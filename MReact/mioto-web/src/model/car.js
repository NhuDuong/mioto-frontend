import axios, { post } from "axios"

import { devMode } from "./base"
import { arrayToString, arrayObjToString } from "../components/common/common"

const baseDomain = `https://m-car${devMode ? "-dev" : ""}.mioto.vn`
const baseUploadDomain = `https://f1-up${devMode ? "-dev" : ""}.mioto.vn`
const baseGPSDomain = `https://m-gps${devMode ? "-dev" : ""}.mioto.vn`

export function init() {
    return axios({
        withCredentials: true,
        method: "GET",
        url: `${baseDomain}/init/app`
    })
}

export function searchCarForList(filter) {
    const instantBooking = filter.instantBooking ? "&instant=true" : "";
    const directDelivery = filter.directDelivery ? "&delivery=true" : "";
    const maxPrice = filter.maxPrice > 0 ? `&maxPrice=${filter.maxPrice * 1000}` : "";
    var features = "";
    if (filter.features && filter.features.length > 0) {
        features = `&features=${arrayToString(filter.features)}`;
    }
    const vType = filter.vehicleType > 0 ? `&vType=${filter.vehicleType}` : "";
    const vMake = (filter.vehicleMake && filter.vehicleMake !== "" && filter.vehicleMake > 0) ? `&vMake=${filter.vehicleMake}` : "";
    const vModel = (filter.vehicleModel && filter.vehicleModel !== "" && filter.vehicleModel > 0) ? `&vModel=${filter.vehicleModel}` : "";
    const transmission = filter.transmission > 0 ? `&transmission=${filter.transmission}` : "";

    const limitKM = filter.limitKM >= 0 ? `&limitKM=${filter.limitKM}` : "";
    const maxDPrice = filter.dPrice >= 0 ? `&maxDPrice=${filter.dPrice * 1000}` : "";
    const maxLimitKMPrice = filter.limitKMPrice >= 0 ? `&maxLimitKMPrice=${filter.limitKMPrice * 1000}` : "";
    const pVerified = filter.pVerified ? `&pVerified=${filter.pVerified}` : "";
    const fuel = filter.fuel > 0 ? `&fuel=${filter.fuel}` : "";
    const fuelC = filter.fuelC > 0 ? `&fuelC=${filter.fuelC}` : "";
    const seatFrom = filter.minSeat > 0 ? `&seatFrom=${filter.minSeat}` : "";
    const seatTo = filter.maxSeat > 0 ? `&seatTo=${filter.maxSeat}` : "";
    const yearFrom = filter.minYear > 0 ? `&yearFrom=${filter.minYear}` : "";
    const yearTo = filter.maxYear > 0 ? `&yearTo=${filter.maxYear}` : "";
    const radius = filter.radius > 0 ? `&radius=${filter.radius}` : "";
    const fromCarId = (filter.fromCarId && filter.fromCarId !== "") ? `&fromCarId=${filter.fromCarId}` : "";

    return axios({
        withCredentials: true,
        method: "POST",
        url: `${baseDomain}/car/search/list?lat=${filter.lat}&lon=${filter.lng}&st=${filter.startDate.valueOf()}&et=${filter.endDate.valueOf()}&sort=${filter.sort}${maxPrice}${vType}${vMake}${vModel}${transmission}${features}${instantBooking}${directDelivery}${limitKM}${maxDPrice}${maxLimitKMPrice}${fuel}${fuelC}${pVerified}${seatFrom}${seatTo}${yearFrom}${yearTo}${radius}${fromCarId}&pos=${filter.pos}`
    })
}

export function searchCarForMap(filter) {
    const instantBooking = filter.instantBooking ? "&instant=true" : "";
    const directDelivery = filter.directDelivery ? "&delivery=true" : "";
    const maxPrice = filter.maxPrice > 0 ? `&maxPrice=${filter.maxPrice * 1000}` : "";
    var features = "";
    if (filter.features && filter.features.length > 0) {
        features = `&features=${arrayToString(filter.features)}`;
    }
    const vType = filter.vehicleType > 0 ? `&vType=${filter.vehicleType}` : "";
    const vMake = (filter.vehicleMake && filter.vehicleMake !== "" && filter.vehicleMake > 0) ? `&vMake=${filter.vehicleMake}` : "";
    const vModel = (filter.vehicleModel && filter.vehicleModel !== "" && filter.vehicleModel > 0) ? `&vModel=${filter.vehicleModel}` : "";
    const transmission = filter.transmission > 0 ? `&transmission=${filter.transmission}` : "";
    const limitKM = filter.limitKM >= 0 ? `&limitKM=${filter.limitKM}` : "";
    const maxDPrice = filter.maxDPrice >= 0 ? `&maxDPrice=${filter.maxDPrice * 1000}` : "";
    const maxLimitKMPrice = filter.limitKMPrice >= 0 ? `&maxLimitKMPrice=${filter.limitKMPrice * 1000}` : "";
    const pVerified = filter.pVerified ? `&pVerified=${filter.pVerified}` : "";
    const fuel = filter.fuel > 0 ? `&fuel=${filter.fuel}` : "";
    const fuelC = filter.fuelC > 0 ? `&fuelC=${filter.fuelC}` : "";
    const seatFrom = filter.minSeat > 0 ? `&seatFrom=${filter.minSeat}` : "";
    const seatTo = filter.maxSeat > 0 ? `&seatTo=${filter.maxSeat}` : "";
    const yearFrom = filter.minYear > 0 ? `&yearFrom=${filter.minYear}` : "";
    const yearTo = filter.maxYear > 0 ? `&yearTo=${filter.maxYear}` : "";
    const radius = `&radius=${filter.minMiles * 3 /*buffered*/}`;

    return axios({
        withCredentials: true,
        method: "POST",
        url: `${baseDomain}/car/search/map?lat=${filter.lat}&lon=${filter.lng}${vType}${vMake}${vModel}&st=${filter.startDate.valueOf()}&et=${filter.endDate.valueOf()}&sort=${filter.sort}${maxPrice}${transmission}${features}${instantBooking}${directDelivery}${limitKM}${maxDPrice}${maxLimitKMPrice}${fuel}${fuelC}${pVerified}${seatFrom}${seatTo}${yearFrom}${yearTo}${radius}`,
    })
}

export function searchCarForMapV2(filter) {
    const instantBooking = filter.instantBooking ? "&instant=true" : "";
    const directDelivery = filter.directDelivery ? "&delivery=true" : "";
    const maxPrice = filter.maxPrice > 0 ? `&maxPrice=${filter.maxPrice * 1000}` : "";
    var features = "";
    if (filter.features && filter.features.length > 0) {
        features = `&features=${arrayToString(filter.features)}`;
    }
    const vType = filter.vehicleType > 0 ? `&vType=${filter.vehicleType}` : "";
    const vMake = (filter.vehicleMake && filter.vehicleMake !== "" && filter.vehicleMake > 0) ? `&vMake=${filter.vehicleMake}` : "";
    const vModel = (filter.vehicleModel && filter.vehicleModel !== "" && filter.vehicleModel > 0) ? `&vModel=${filter.vehicleModel}` : "";
    const transmission = filter.transmission > 0 ? `&transmission=${filter.transmission}` : "";
    const limitKM = filter.limitKM >= 0 ? `&limitKM=${filter.limitKM}` : "";
    const maxDPrice = filter.maxDPrice >= 0 ? `&maxDPrice=${filter.maxDPrice * 1000}` : "";
    const maxLimitKMPrice = filter.limitKMPrice >= 0 ? `&maxLimitKMPrice=${filter.limitKMPrice * 1000}` : "";
    const pVerified = filter.pVerified ? `&pVerified=${filter.pVerified}` : "";
    const fuel = filter.fuel > 0 ? `&fuel=${filter.fuel}` : "";
    const fuelC = filter.fuelC > 0 ? `&fuelC=${filter.fuelC}` : "";
    const seatFrom = filter.minSeat > 0 ? `&seatFrom=${filter.minSeat}` : "";
    const seatTo = filter.maxSeat > 0 ? `&seatTo=${filter.maxSeat}` : "";
    const yearFrom = filter.minYear > 0 ? `&yearFrom=${filter.minYear}` : "";
    const yearTo = filter.maxYear > 0 ? `&yearTo=${filter.maxYear}` : "";
    const radius = `&radius=${filter.minMiles}`;
    const radiusBuffer = `&radiusB=${filter.minMiles * 3}`;
    const screenWidth = `&w=${filter.minMiles * 2}`;
    const screenHeight = `&h=${filter.minMiles * 2}`;
    const groupWidth = `&uw=${filter.minMiles / 4}`;
    const groupHeight = `&uh=${filter.minMiles / 4}`;

    return axios({
        withCredentials: true,
        method: "POST",
        url: `${baseDomain}/v2/car/search/map?lat=${filter.lat}&lon=${filter.lng}${vType}${vMake}${vModel}&st=${filter.startDate.valueOf()}&et=${filter.endDate.valueOf()}&sort=${filter.sort}${maxPrice}${transmission}${features}${instantBooking}${directDelivery}${limitKM}${maxDPrice}${maxLimitKMPrice}${fuel}${fuelC}${pVerified}${seatFrom}${seatTo}${yearFrom}${yearTo}${radius}${radiusBuffer}${screenWidth}${screenHeight}${groupWidth}${groupHeight}`,
    })
}

export function getCarDetail(carId, ex) {
    const carLat = ex && ex.lat ? `&lat=${ex.lat}` : "";
    const carLng = ex && ex.lng ? `&lon=${ex.lng}` : "";
    const st = ex && ex.st ? `&st=${ex.st}` : "";
    const et = ex && ex.et ? `&et=${ex.et}` : "";
    const pVw = ex && ex.vw ? `&vw=${ex.vw}` : "";

    return axios({
        withCredentials: true,
        method: "GET",
        url: `${baseDomain}/car/detail?carId=${carId}${carLat}${carLng}${pVw}${st}${et}`
    })
}

export function getCarPrice(carId, st, et, latD, lonD, promo) {
    const pPromo = promo ? `&promo=${promo.code}` : "";
    const pLatD = latD !== 0 ? `&latD=${latD}` : "";
    const pLonD = lonD !== 0 ? `&lonD=${lonD}` : "";
    return axios({
        withCredentials: true,
        method: "GET",
        url: `${baseDomain}/car/detail?carId=${carId}&st=${st.valueOf()}&et=${et.valueOf()}${pLatD}${pLonD}${pPromo}`
    })
}

export function getCarCalendar(carId, vw) {
    return axios({
        withCredentials: true,
        method: "GET",
        url: `${baseDomain}/car/calendar?carId=${carId}&vw=${vw}`,
    })
}

export function bookCar(carId, instant, dateFrom, dateTo, price, limitEnable, limitKM, limitPrice, useCarLocation, dLat, dLon, dAddr, message, promo) {
    const pPromo = promo ? `&promo=${promo.code}` : "";

    return axios({
        method: "POST",
        withCredentials: true,
        url: `${baseDomain}/book/car?instant=${instant}&carId=${carId}&dateFrom=${dateFrom}&dateTo=${dateTo}&price=${price}&limitEnable=${limitEnable}&limitKM=${limitKM}&limitPrice=${limitPrice}&useCarLocation=${useCarLocation}&dLat=${dLat}&dLon=${dLon}&dAddr=${dAddr}&message=${message}${pPromo}`
    })
}

export function getTrips(filterStatus, fromTripId, pos, ver) {
    const pFromTripId = fromTripId !== 0 ? `&fromTripId=${fromTripId}` : "";
    return axios({
        method: "GET",
        withCredentials: true,
        url: `${baseDomain}/traveler/trips?filterStatus=${filterStatus}${pFromTripId}&pos=${pos}&ver=${ver}`
    })
}

export function getOwnerTrips(filterStatus, fromTripId, pos, ver) {
    const pFromTripId = fromTripId !== 0 ? `&fromTripId=${fromTripId}` : "";
    return axios({
        method: "GET",
        withCredentials: true,
        url: `${baseDomain}/owner/trips?filterStatus=${filterStatus}${pFromTripId}&pos=${pos}&ver=${ver}`
    })
}

export function getCarTrips(carId, filterStatus, fromTripId, pos, ver) {
    const pFromTripId = fromTripId !== 0 ? `&fromTripId=${fromTripId}` : "";
    return axios({
        method: "GET",
        withCredentials: true,
        url: `${baseDomain}/car/trips?carId=${carId}&filterStatus=${filterStatus}${pFromTripId}&pos=${pos}&ver=${ver}`
    })
}

export function getUpcommingTrip(fromTripId, pos, ver) {
    const pFromTripId = fromTripId !== 0 ? `&fromTripId=${fromTripId}` : "";
    return axios({
        method: "GET",
        withCredentials: true,
        url: `${baseDomain}/trip/upcoming?&pos=${pos}&ver=$${ver}${pFromTripId}`
    })
}

export function getTripDetail(tripId) {
    return axios({
        method: "GET",
        withCredentials: true,
        url: `${baseDomain}/trip/detail?tripId=${tripId}`
    })
}

export function getCancelTripReasons(tripId) {
    return axios({
        method: "GET",
        withCredentials: true,
        url: `${baseDomain}/traveler/cancel-reason?tripId=${tripId}`
    })
}

export function cancelTrip(tripId, reasonId, comment) {
    return axios({
        method: "POST",
        withCredentials: true,
        url: `${baseDomain}/traveler/cancel-trip?tripId=${tripId}&reason=${reasonId}&comment=${comment}`
    })
}

export function getCancelTripReasonsOwner(tripId) {
    return axios({
        method: "GET",
        withCredentials: true,
        url: `${baseDomain}/owner/cancel-reason?tripId=${tripId}`
    })
}

export function cancelTripOwner(tripId, reasonId, comment) {
    return axios({
        method: "POST",
        withCredentials: true,
        url: `${baseDomain}/owner/cancel-trip?tripId=${tripId}&reason=${reasonId}&comment=${comment}`
    })
}

export function getRejectTripReasonsOwner(tripId) {
    const p = tripId ? `?tripId=${tripId}` : "";
    return axios({
        method: "GET",
        withCredentials: true,
        url: `${baseDomain}/owner/reject-reason${p}`
    })
}

export function rejectTripByOwner(tripId, reasonId, comment) {
    return axios({
        method: "POST",
        withCredentials: true,
        url: `${baseDomain}/owner/approve-trip?tripId=${tripId}&reason=${reasonId}&comment=${comment}&reject=true`
    })
}

export function approveTripByOwner(tripId) {
    return axios({
        method: "POST",
        withCredentials: true,
        url: `${baseDomain}/owner/approve-trip?tripId=${tripId}&reject=false`
    })
}

export function pickupTrip(tripId) {
    return axios({
        method: "POST",
        withCredentials: true,
        url: `${baseDomain}/owner/pickup-trip?tripId=${tripId}`
    })
}

export function returnTrip(tripId) {
    return axios({
        method: "POST",
        withCredentials: true,
        url: `${baseDomain}/owner/return-trip?tripId=${tripId}`
    })
}

export function reviewTripByOwner(tripId, rating, comment) {
    return axios({
        method: "POST",
        withCredentials: true,
        url: `${baseDomain}/owner/review?tripId=${tripId}&rating=${rating}&comment=${comment}`
    })
}

export function removePendingReview(tripId) {
    return axios({
        method: "POST",
        withCredentials: true,
        url: `${baseDomain}/review/remove-pending?tripId=${tripId}`
    })
}

export function reviewTripByTraveler(tripId, rating, comment) {
    return axios({
        method: "POST",
        withCredentials: true,
        url: `${baseDomain}/traveler/review?tripId=${tripId}&rating=${rating}&comment=${comment}`
    })
}

export function getUserCars(userId, filterStatus, fromCarId, pos, ver, all) {
    const pFromCarId = fromCarId !== 0 ? `&fromCarId=${fromCarId}` : "";
    return axios({
        method: "GET",
        withCredentials: true,
        url: `${baseDomain}/owner/cars?&ownerId=${userId}&filterStatus=${filterStatus}${pFromCarId}&pos=${pos}&ver=${ver}&all=${all || false}`
    })
}

export function getOwnerCars(filterStatus, fromCarId, pos, ver, all) {
    const pFromCarId = fromCarId !== 0 ? `&fromCarId=${fromCarId}` : "";
    return axios({
        method: "GET",
        withCredentials: true,
        url: `${baseDomain}/owner/cars?filterStatus=${filterStatus}${pFromCarId}&pos=${pos}&ver=${ver}&all=${all || false}`
    })
}

export function getFavoriteCars(fromCarId, pos, ver) {
    const pFromCarId = fromCarId !== 0 ? `&fromCarId=${fromCarId}` : "";
    return axios({
        method: "GET",
        withCredentials: true,
        url: `${baseDomain}/fav/cars?pos=${pos}&ver=${ver}${pFromCarId}`
    })
}

export function addFavoriteCar(carId) {
    const url = `${baseDomain}/fav/add-car`;
    const params = {
        carId: carId
    }

    return axios({
        method: "POST",
        withCredentials: true,
        url: url,
        params: params
    })
}

export function removeFavoriteCar(carId) {
    return axios({
        method: "POST",
        withCredentials: true,
        url: `${baseDomain}/fav/remove-car?carId=${carId}`
    })
}

export function getCarReviews(carId, fromReviewId, pos, ver) {
    const pFromReviewId = fromReviewId !== 0 ? `&fromReviewId=${fromReviewId}` : "";
    return axios({
        method: "GET",
        withCredentials: true,
        url: `${baseDomain}/car/reviews?carId=${carId}&pos=${pos}&ver=${ver}${pFromReviewId}`
    })
}

export function getOwnerReviews(userId, fromReviewId, pos, ver) {
    const pFromReviewId = fromReviewId !== 0 ? `&fromReviewId=${fromReviewId}` : "";

    return axios({
        method: "GET",
        withCredentials: true,
        url: `${baseDomain}/traveler/reviews?travelerId=${userId}${pFromReviewId}&pos=${pos}&ver=${ver}`
    })

}

export function getTravelerReviews(userId, fromReviewId, pos, ver) {
    const pFromReviewId = fromReviewId !== 0 ? `&fromReviewId=${fromReviewId}` : "";

    return axios({
        method: "GET",
        withCredentials: true,
        url: `${baseDomain}/owner/reviews?ownerId=${userId}&pos=${pos}&ver=${ver}${pFromReviewId}`
    })
}

export function getCarReportReasons(carId) {
    return axios({
        method: "GET",
        withCredentials: true,
        url: `${baseDomain}/report/car-reason?carId=${carId}`
    })
}

export function reportCar(carId, reasonId, comment) {
    const pComment = comment !== "" ? `&comment=${comment}` : "";
    return axios({
        method: "POST",
        withCredentials: true,
        url: `${baseDomain}/report/car?carId=${carId}&reason=${reasonId}${pComment}`
    })
}

export function getUserReportReasons(userId) {
    return axios({
        method: "GET",
        withCredentials: true,
        url: `${baseDomain}/report/user-reason?userId=${userId}`
    })
}

export function reportUser(userId, reasonId, comment) {
    return axios({
        method: "POST",
        withCredentials: true,
        url: `${baseDomain}/report/user?userId=${userId}&reason=${reasonId}&comment=${comment}`
    })
}

export function getCarSetting(data) {
    var pCarId = "";
    if (data.carId) {
        pCarId = `carId=${data.carId}`;
    }

    var pModelId = "";
    if (data.vehicleModelId) {
        pModelId = `${pCarId !== "" ? "&" : ""}vehicleModelId=${data.vehicleModelId}`
    }

		var pModelYear = "";
    if(data.year) {
        pModelYear = `&year=${data.year}`
    }
	
    return axios({
        method: "GET",
        withCredentials: true,
        url: `${baseDomain}/car/setting?${pCarId}${pModelId}`
    })
}

export function getFilterConfig() {
    return axios({
        method: "GET",
        withCredentials: true,
        url: `${baseDomain}/car/filter`
    })
}

export function getVehicleType() {
    return axios({
        method: "GET",
        withCredentials: true,
        url: `${baseDomain}/vehicle/types`
    })
}

export function getVehicleMake() {
    return axios({
        method: "GET",
        withCredentials: true,
        url: `${baseDomain}/vehicle/makes`
    })
}

export function getVehicleModel(vehicleType, vehicleMakeId) {
    return axios({
        method: "GET",
        withCredentials: true,
        url: `${baseDomain}/vehicle/models?vehicleMakeId=${vehicleMakeId}&vehicleType=${vehicleType}`
    })
}

export function removeAccountPaper(paperId, photoId) {
    return axios({
        method: "POST",
        withCredentials: true,
        url: `${baseDomain}/traveler/remove-paper?paperId=${paperId}&photoIds=${photoId}`
    })
}

export function uploadCarPhoto(carId, photo) {
    const url = `${baseUploadDomain}/car/upload-photo`;
    const formData = new FormData();
    if (carId !== 0) {
        formData.append('carId', carId);
    }
    formData.append('pdata', photo);
    const config = {
        headers: {
            'content-type': 'multipart/form-data'
        },
        withCredentials: true,
        // onUploadProgress: event => {
        //     console.log("Uploaded: " + Math.round(event.loaded / event.total * 100));
        // }
    }
    return post(url, formData, config);
}

export function removeCarPhoto(carId, photoId) {
    return axios({
        method: "POST",
        withCredentials: true,
        url: `${baseDomain}/car/remove-photo?carId=${carId}&photoIds=${photoId}`
    })
}

export function uploadCarPaper(carId, paperId, photo) {
    const url = `${baseUploadDomain}/car/upload-paper`;
    const formData = new FormData();
    formData.append('carId', carId)
    formData.append('paperId', paperId)
    formData.append('pdata', photo)
    const config = {
        withCredentials: true,
        headers: {
            'content-type': 'multipart/form-data'
        }
    }
    return post(url, formData, config);
}

export function removeCarPaper(carId, paperId, photoId) {
    return axios({
        method: "POST",
        withCredentials: true,
        url: `${baseDomain}/car/remove-paper?carId=${carId}&paperId=${paperId}&photoIds=${photoId}`
    })
}

export function uploadInsurancePhoto(tripId, imageId, photo) {
    const url = `${baseUploadDomain}/trip/upload-insurance`;
    const formData = new FormData();
    formData.append('tripId', tripId)
    formData.append('imageId', imageId)
    formData.append('pdata', photo)
    const config = {
        withCredentials: true,
        headers: {
            'content-type': 'multipart/form-data'
        }
    }
    return post(url, formData, config);
}

export function removeInsurancePhoto(tripId, imageId, photoId) {
    return axios({
        method: "POST",
        withCredentials: true,
        url: `${baseDomain}/trip/remove-insurance?tripId=${tripId}&imageId=${imageId}&photoIds=${photoId}`
    })
}
export function activeInsurance(tripId) {
	const url = `${baseDomain}/trip/active-insurance`;
	const params = {
			tripId: tripId
	}

	return axios({
			method: "POST",
			withCredentials: true,
			url: url,
			params: params
	})
}

export function getActiveInsurance(tripId, certNo) {
	return axios({
		method: "GET",
		withCredentials: true,
		url: `${baseDomain}/trip/get-active-insurance?tripId=${tripId}&certNo=${certNo}`
	})
}


export function registerCar(data) {
    const lp = data.lp ? `lp=${data.lp}` : "";
    const vehicleModelId = data.vehicleModelId ? `&vehicleModelId=${data.vehicleModelId}` : "";
    const seat = data.seat ? `&seat=${data.seat}` : "";
    const year = data.year ? `&year=${data.year}` : "";
    const instant = `&instant=${data.instant}`;
    const instantRangeFrom = `&instantRangeFrom=${data.instantRangeFrom}`;
    const instantRangeTo = `&instantRangeTo=${data.instantRangeTo}`;
    const lat = data.lat ? `&lat=${data.lat}` : "";
    const lon = data.lon ? `&lon=${data.lon}` : "";
    const addr = data.addr ? `&addr=${data.addr}` : "";
    const deliveryEnable = `&deliveryEnable=${data.deliveryEnable}`;
    const deliveryRadius = data.deliveryRadius ? `&deliveryRadius=${data.deliveryRadius}` : "";
    const deliveryPrice = data.deliveryPrice ? `&deliveryPrice=${data.deliveryPrice}` : "";
    const limitEnable = `&limitEnable=${data.limitEnable}`;
    const limitKM = data.limitKM ? `&limitKM=${data.limitKM}` : "";
    const limitPrice = data.limitPrice ? `&limitPrice=${data.limitPrice}` : "";
    var photoTmpIds = "";
    if (data.photoTmpIds && data.photoTmpIds.length > 0) {
        photoTmpIds = `&photoTmpIds=${arrayObjToString(data.photoTmpIds, "id")}`
    }
    const transmission = data.transmission ? `&transmission=${data.transmission}` : "";
    const fuel = data.fuel ? `&fuel=${data.fuel}` : "";
    const fuelConsumption = data.fuelConsumption ? `&fuelConsumption=${data.fuelConsumption}` : "";
    var features = "";
    if (data.features && data.features.length > 0) {
        features = `&features=${arrayToString(data.features)}`;
    }
    const desc = data.desc ? `&desc=${data.desc}` : "";
		const notes = data.notes ? `&notes=${data.notes}` : "";

    const requiredPapers = data.requiredPapers ? `&requiredPapers=${data.requiredPapers}` : "";
    const requiredPapersSpecific = data.requiredPapersSpecific ? `&requiredPapersSpecific=${data.requiredPapersSpecific}` : "";
		const requiredPapersOther = data.requiredPapersOther ? `&requiredPapersOther=${data.requiredPapersOther}` : "";
	
		const mortgages = data.mortgages ? `&mortgages=${data.mortgages}` : "";
		const mortgagesSpecific = data.mortgagesSpecific ? `$mortgagesSpecific=${data.mortgagesSpecific}` : "";

    const priceDaily = data.priceDaily ? `&priceDaily=${data.priceDaily}` : "";
    const discountEnable = `&discountEnable=${data.discountEnable}`;
    const discountWeekly = data.discountWeekly ? `&discountWeekly=${data.discountWeekly}` : "";
    const discountMonthly = data.discountMonthly ? `&discountMonthly=${data.discountMonthly}` : "";

    const inviteCode = data.inviteCode ? `&inviteCode=${data.inviteCode}` : "";

    return axios({
        method: "POST",
        withCredentials: true,
        url: `${baseDomain}/owner/add-car?${lp}${vehicleModelId}${seat}${year}${instant}${instantRangeFrom}${instantRangeTo}${lat}${lon}${addr}${deliveryEnable}${deliveryRadius}${deliveryPrice}${limitEnable}${limitKM}${limitPrice}${photoTmpIds}${transmission}${fuel}${fuelConsumption}${features}${desc}${notes}${requiredPapersSpecific}${requiredPapers}${requiredPapersOther}${mortgages}${mortgagesSpecific}${discountEnable}${discountWeekly}${discountMonthly}${priceDaily}${inviteCode}`
    })
}

export function updateCar(carId, data) {
    const instant = data.instant !== undefined ? `&instant=${data.instant}` : "";
    const instantRangeFrom = data.instantRangeFrom !== undefined ? `&instantRangeFrom=${data.instantRangeFrom}` : "";
    const instantRangeTo = data.instantRangeTo !== undefined ? `&instantRangeTo=${data.instantRangeTo}` : "";

    const lat = data.lat !== undefined ? `&lat=${data.lat}` : "";
    const lon = data.lon !== undefined ? `&lon=${data.lon}` : "";
    const addr = data.address !== undefined ? `&addr=${data.address}` : "";

    const deliveryEnable = data.deliveryEnable !== undefined ? `&deliveryEnable=${data.deliveryEnable}` : "";
    const deliveryRadius = data.deliveryRadius !== undefined ? `&deliveryRadius=${data.deliveryRadius}` : "";
    const deliveryPrice = data.deliveryPrice !== undefined ? `&deliveryPrice=${data.deliveryPrice}` : "";

    const limitEnable = data.limitEnable !== undefined ? `&limitEnable=${data.limitEnable}` : "";
    const limitKM = data.limitKM !== undefined ? `&limitKM=${data.limitKM}` : "";
		const limitPrice = data.limitPrice !== undefined ? `&limitPrice=${data.limitPrice}` : "";
	


    var photoTmpIds = "";
    if (data.photoTmpIds && data.photoTmpIds.length > 0) {
        photoTmpIds = `&photoIds=${arrayObjToString(data.photoTmpIds, "id")}`
    }

    const fuelConsumption = data.fuelConsumption !== undefined ? `&fuelConsumption=${data.fuelConsumption}` : "";
    var features = "";
    if (data.features && data.features.length > 0) {
        features = `&features=${arrayToString(data.features)}`;
    }

    const desc = data.desc !== undefined ? `&desc=${data.desc}` : "";
		const notes = data.notes !== undefined ? `&notes=${data.notes}` : "";
		const policies = data.policies !== undefined ? `&policies=${data.policies}` : "";

    const requiredPapers = data.requiredPapers !== undefined ? `&requiredPapers=${data.requiredPapers}` : "";
    const requiredPapersSpecific = data.requiredPapersSpecific !== undefined ? `&requiredPapersSpecific=${data.requiredPapersSpecific}` : "";
		const requiredPapersOther = data.requiredPapersOther !== undefined ? `&requiredPapersOther=${data.requiredPapersOther}` : "";
	
		const mortgages = data.mortgages !== undefined ? `&mortgages=${data.mortgages}` : "";
		const mortgagesSpecific = data.mortgagesSpecific !== undefined ? `&mortgagesSpecific=${data.mortgagesSpecific}` : "";
	
    const priceDaily = data.priceDaily !== undefined ? `&priceDaily=${data.priceDaily}` : "";
    const discountEnable = data.discountEnable !== undefined ? `&discountEnable=${data.discountEnable}` : "";
    const discountWeekly = data.discountWeekly !== undefined ? `&discountWeekly=${data.discountWeekly}` : "";
    const discountMonthly = data.discountMonthly !== undefined ? `&discountMonthly=${data.discountMonthly}` : "";

    var i;
    var unavails = "";
    if (data.unavailDays) {
        if (data.unavailDays.length === 0) {
            unavails = "&unavails=";
        } else {
            for (i = 0; i < data.unavailDays.length; ++i) {
                const startTs = data.unavailDays[i];
                const endTs = startTs + (3600000 * 24) - 1;

                if (unavails === "") {
                    unavails = `&unavails=${startTs}-${endTs}`;
                } else {
                    unavails = unavails + `,${startTs}-${endTs}`;
                }
            }
        }
    }

    var priceSpecific = "";
    if (data.priceSpecifics) {
        if (data.priceSpecifics.length === 0) {
            priceSpecific = "&priceSpecific=";
        } else {
            for (i = 0; i < data.priceSpecifics.length; ++i) {
                const startTs = data.priceSpecifics[i].ts;
                const endTs = startTs + (3600000 * 24) - 1;
                const price = data.priceSpecifics[i].price;

                if (priceSpecific === "") {
                    priceSpecific = `&priceSpecific=${startTs}-${endTs}-${price}`;
                } else {
                    priceSpecific = priceSpecific + `,${startTs}-${endTs}-${price}`;
                }
            }
        }
    }

    var weekday;
    var priceRepeat = "";
    if (data.priceRepeat) {
        if (data.priceRepeat.length === 0) {
            priceRepeat = "&priceRepeat=";
        } else {
            for (i = 0; i < data.priceRepeat.length; ++i) {
                weekday = data.priceRepeat[i].weekday;
                if (weekday === 7) {
                    weekday = 1;
                } else {
                    weekday = weekday + 1;
                }
                const price = data.priceRepeat[i].price;

                if (priceRepeat === "") {
                    priceRepeat = `&priceRepeat=${weekday}-${price}`;
                } else {
                    priceRepeat = priceRepeat + `,${weekday}-${price}`;
                }
            }
        }
    }

    var priceRepeatEndDate = "";
    if (data.priceRepeatEndDate !== undefined) {
        priceRepeatEndDate = `&priceRepeatEndDate=${data.priceRepeatEndDate}`;
    }

    var unavailsRepeat = "";
    if (data.unavailsRepeat) {
        if (data.unavailsRepeat.length === 0) {
            unavailsRepeat = "&unavailsRepeatWeekday=";
        } else {
            for (i = 0; i < data.unavailsRepeat.length; ++i) {
                weekday = data.unavailsRepeat[i].weekday;
                if (weekday === 7) {
                    weekday = 1;
                } else {
                    weekday = weekday + 1;
                }

                if (unavailsRepeat === "") {
                    unavailsRepeat = `&unavailsRepeatWeekday=${weekday}`;
                } else {
                    unavailsRepeat = unavailsRepeat + `,${weekday}`;
                }
            }
        }
    }

    var unavailsRepeatEndDate = "";
    if (data.unavailsRepeatEndDate !== undefined) {
        unavailsRepeatEndDate = `&unavailsRepeatEndDate=${data.unavailsRepeatEndDate}`;
    }

    var unavailsBefore = "";
    if (data.unavailsBefore !== undefined){
        unavailsBefore = `&unavailsBefore=${data.unavailsBefore}`;
    }

    var unavailsAfter = "";
    if (data.unavailsAfter !== undefined){
        unavailsAfter = `&unavailsAfter=${data.unavailsAfter}`;
    }

		const policiesSpecific = data.policiesSpecific !== undefined ? `&policiesSpecific=${data.policiesSpecific}` : "";
	
    return axios({
        method: "POST",
        withCredentials: true,
        url: `${baseDomain}/owner/update-car?carId=${carId}${instant}${instantRangeFrom}${instantRangeTo}${lat}${lon}${addr}${deliveryEnable}${deliveryRadius}${deliveryPrice}${limitEnable}${limitKM}${limitPrice}${photoTmpIds}${fuelConsumption}${features}${desc}${policiesSpecific}${notes}${policies}${requiredPapersSpecific}${requiredPapers}${requiredPapersOther}${mortgages}${mortgagesSpecific}${discountEnable}${discountWeekly}${discountMonthly}${priceDaily}${unavails}${priceSpecific}${priceRepeat}${priceRepeatEndDate}${unavailsRepeat}${unavailsRepeatEndDate}${unavailsBefore}${unavailsAfter}`
    })
}

export function removeCar(carId) {
    return axios({
        method: "POST",
        withCredentials: true,
        url: `${baseDomain}/owner/remove-car?carId=${carId}`
    })
}

export function invisibleCar(carId, isShow) {
    return axios({
        method: "POST",
        withCredentials: true,
        url: `${baseDomain}/owner/invisible-car?carId=${carId}&invisible=${isShow}`
    })
}

export function getCarRequestInfo(reqId) {
    return axios({
        method: "GET",
        withCredentials: true,
        url: `${baseDomain}/request/get-car-request?id=${reqId}`
    })
}

export function responseCarRequest(reqId, accept, msg, rejectReason) {
    const pMsg = msg ? `&msg=${msg}` : "";
    const pRejectReason = rejectReason ? `&rejectReason=${rejectReason}` : "";
    return axios({
        method: "POST",
        withCredentials: true,
        url: `${baseDomain}/request/response?id=${reqId}&accept=${accept}${pMsg}${pRejectReason}`
    })
}

export function getCityReviews(city, fromReviewId, pos, ver) {
    const pFromReviewId = fromReviewId !== 0 ? `&fromReviewId=${fromReviewId}` : "";
    return axios({
        method: "GET",
        withCredentials: true,
        url: `${baseDomain}/city/reviews?city=${city}&pos=${pos}&ver=${ver}${pFromReviewId}`
    })
}

export function getCarLocationHistory(carId, count) {
    return axios({
        method: "GET",
        withCredentials: true,
        url: `${baseGPSDomain}/car/loc?carId=${carId}&count=${count}`
    })
}

export function reserveTrip(params) {
    const { tripId, paymentMed, isCancel, code } = params;
    const pCode = code ? `&code=${code}` : "";
    return axios({
        method: "POST",
        withCredentials: true,
        url: `${baseDomain}/trip/reserve?tripId=${tripId}&med=${paymentMed}${pCode}&cancel=${isCancel}`
    })
}

export function getMyTrips(filter, fromTripId, pos, ver) {
    const sort = filter.sort > 0 ? `sort=${filter.sort}` : "";
    const userType = filter.userType > 0 ? `&userType=${filter.userType}` : "";
    const status = filter.status > 0 ? `&status=${filter.status}` : "";
    const carId = filter.carId ? `&carId=${filter.carId}` : "";
    
    return axios({
        method: "GET",
        withCredentials: true,
        url: `${baseDomain}/trip/mytrips?${sort}${userType}${status}${carId}&fromTripId=${fromTripId}&pos=${pos}&ver=${ver}`
    })
}

export function getTripHistories(filter, fromId, pos, ver){
    const userType = filter.userType > 0 ? `userType=${filter.userType}` : "";
    const status = filter.status > 0 ? `&status=${filter.status}` : "";
    const carId = filter.carId ? `&carId=${filter.carId}` : "";
    const fromTime = filter.fromTime > 0 ? `&fromTime=${filter.fromTime}` : "";
    const toTime = filter.toTime > 0 ? `$toTime=${filter.toTime}` : "";

    return axios({
        method: "GET",
        withCredentials: true,
        url: `${baseDomain}/trip/histories?${userType}${status}${carId}${fromTime}${toTime}&fromId=${fromId}&pos=${pos}&ver=${ver}`
    })
}