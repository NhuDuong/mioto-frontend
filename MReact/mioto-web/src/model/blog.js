import axios from "axios"

import { devMode } from "./base"

const baseDomain = `https://m-blog${devMode ? "-dev" : ""}.mioto.vn`

export function getBlogPage(pageId, city) {
    const pCity = city ? `&city=${city}` : ""
    return axios({
        withCredentials: true,
        method: "GET",
        url: `${baseDomain}/blogs?page=${pageId}${pCity}`
    })
}

export function getBlog(blogId) {
    return axios({
        withCredentials: true,
        method: "GET",
        url: `${baseDomain}/blog?blogId=${blogId}`
    })
}