import axios from "axios"

import { devMode } from "./base"

const baseDomain = `https://m-promo${devMode ? "-dev" : ""}.mioto.vn`

export function getMyPromo(fromId, pos, ver) {
    const pFromId = fromId !== 0 ? `&fromId=${fromId}` : "";
    return axios({
        withCredentials: true,
        method: "GET",
        url: `${baseDomain}/promo/my-promotions?pos=${pos}&ver=${ver}${pFromId}`
    })
}

export function searchPromo(code, carId) {
    const pCarId = carId !== 0 ? `&carId=${carId}` : "";
    return axios({
        withCredentials: true,
        method: "GET",
        url: `${baseDomain}/promo/search-promotions?code=${code}${pCarId}`
    })
}