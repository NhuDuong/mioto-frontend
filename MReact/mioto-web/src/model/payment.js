import axios from "axios"

import { devMode } from "./base"

const baseDomain = `https://payment${devMode ? "-dev" : ""}.mioto.vn`
const baseCallbackDomain = `https://${devMode ? "dev" : "www"}.mioto.vn`



export function requestPayment(tripId) {
    return axios({
        withCredentials: true,
        method: "POST",
        url: `${baseDomain}/vtc/req-payment?tripId=${tripId}&callbackURL=${baseCallbackDomain}/trip/detail/${tripId}`
    })
}

export function requestPaymentWithUrl(url, tripId) {
    return axios({
        withCredentials: true,
        method: "POST",
        url: `${url}?tripId=${tripId}&callbackURL=${baseCallbackDomain}/trip/detail/${tripId}`
    })
}

export function getListPaymentProvider() {
    return axios({
        withCredentials: true,
        method: "GET",
        url: `${baseDomain}/mipay/get-list-payment-provider`
    })
}

export function getListPaymentMethod(tripId) {
    return axios({
        withCredentials: true,
        method: "GET",
        url: `${baseDomain}/mipay/get-list-payment-method?tripId=${tripId}`
    })
}

export function requestPaymentWithUrlNew(url, tripId) {
    return axios({
        withCredentials: true,
        method: "POST",
        url: `${url}&tripId=${tripId}&callbackURL=${baseCallbackDomain}/trip/detail/${tripId}`
    })
}


export function requestPaymentWithMyCard(tripId, cardId) {
    return axios({
        withCredentials: true,
        method: "POST",
        url: `${baseDomain}/alepay/req-payment?tripId=${tripId}&cardId=${cardId}&callbackURL=${baseCallbackDomain}/trip/detail/${tripId}`
    })
}

export function getListCard() {
	return axios({
		withCredentials: true,
		method: "GET",
		url: `${baseDomain}/alepay/get-list-card`
	})
} 

export function removeCard(cardId) {
	return axios({
			method: "POST",
			withCredentials: true,
			url: `${baseDomain}/alepay/remove-card?cardId=${cardId}`
	})
}

export function requestAddCard(callbackPaths) {
	return axios({
			withCredentials: true,
			method: "POST",
			url: `${baseDomain}/alepay/req-add-card?callbackURL=${baseCallbackDomain}/${callbackPaths}`
	})
}


