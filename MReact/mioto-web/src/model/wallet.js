import axios from "axios"

import { devMode } from "./base"

const baseDomain = `https://m-wallet${devMode ? "-dev" : ""}.mioto.vn`

export function getBalance() {
    return axios({
        withCredentials: true,
        method: "GET",
        url: `${baseDomain}/owner/balance`
    })
}

export function getInvoiceByMonth(month) {
    const monthParams = month !== 0 ? `month=${month}` : "";
    return axios({
        withCredentials: true,
        method: "GET",
        url: `${baseDomain}/owner/invoice?${monthParams}`
    })
}

export function withdrawMoney(cancel, withdrawInfo){
    const isCancel = cancel !== undefined ? `cancel=${cancel}` : 0;
    const amount = withdrawInfo && withdrawInfo.amount !== undefined ? `&amount=${withdrawInfo.amount}` : "";
    const accountName = withdrawInfo && withdrawInfo.accountName !== undefined ? `&accountName=${withdrawInfo.accountName}` : "";
    const accountNumber = withdrawInfo && withdrawInfo.accountNumber !== undefined ? `&accountNumber=${withdrawInfo.accountNumber}` : "";
    const bankName = withdrawInfo && withdrawInfo.bankName !== undefined ? `&bankName=${withdrawInfo.bankName}` : "";
    const bankProvince = withdrawInfo &&  withdrawInfo.bankProvince !== undefined ? `&bankProvince=${withdrawInfo.bankProvince}` : "";
    const bankBranch = withdrawInfo && withdrawInfo.bankBranch !== undefined ? `&bankBranch=${withdrawInfo.bankBranch}` : "";
    return axios({
        withCredentials: true,
        method: "POST",
        url: `${baseDomain}/owner/request-withdraw?${isCancel}${amount}${accountName}${accountNumber}${bankName}${bankProvince}${bankBranch}`
    })
}