import axios from "axios"

import { devMode } from "./base"
const baseDomain = `https://m-car${devMode ? "-dev" : ""}.mioto.vn`
const commonDomain = `https://m-common${devMode ? "-dev" : ""}.mioto.vn`

export function getHomePage() {
    return axios({
        withCredentials: true,
        method: "GET",
        url: `${baseDomain}/homepage`
    })
}

export function getCity(city) {
    return axios({
        withCredentials: true,
        method: "GET",
        url: `${baseDomain}/city/detail?city=${city}`
    })
}

export function submitFormOwnerRegister(data){
    return axios({
        method: "POST",
        url: `${commonDomain}/owner/register?name=${data.name}&phoneNumber=${data.phoneNumber}&city=${data.city}&email=${data.email}&isRental=${data.isRental}&reference=${data.reference}&carName=${data.carName}`
    })
}

