import axios, { post } from "axios"

import { devMode } from "./base"

const baseDomain = `https://accounts${devMode ? "-dev" : ""}.mioto.vn`
const baseDomainProfile = `https://m-profile${devMode ? "-dev" : ""}.mioto.vn`
const baseUploadDomain = `https://f1-up${devMode ? "-dev" : ""}.mioto.vn`
const baseNotifyDomain = `https://m-notify${devMode ? "-dev" : ""}.mioto.vn`

export function quickSignup(name, displayName, password) {
    return axios({
        method: "POST",
        withCredentials: true,
        url: `${baseDomain}/mapi/sign-up?name=${name}&displayName=${displayName}&pwd=${password}&gender=&dob=`
    })
}

export function signup(name, displayName, password, gender, dob) {
    return axios({
        method: "POST",
        withCredentials: true,
        url: `${baseDomain}/mapi/sign-up?name=${name}&displayName=${displayName}&pwd=${password}&gender=${gender}&dob=${dob}`
    })
}

export function updateInfo(displayName, gender, dob) {
    return axios({
        method: "POST",
        withCredentials: true,
        url: `${baseDomain}/mapi/profile/update?displayName=${displayName}&gender=${gender}&dob=${dob}`
    })
}

export function updatePassword(oldPassword, newPassword) {
    return axios({
        method: "POST",
        withCredentials: true,
        url: `${baseDomain}/mapi/profile/change-pwd?oldPwd=${oldPassword}&newPwd=${newPassword}`
    })
}

export function updatePhone(newPhone) {
    return axios({
        method: "POST",
        withCredentials: true,
        url: `${baseDomain}/mapi/profile/update-phone?newPhone=${newPhone}`
    })
}

export function updateEmail(newEmail) {
    return axios({
        method: "POST",
        withCredentials: true,
        url: `${baseDomain}/mapi/profile/update-email?newEmail=${newEmail}`
    })
}

export function updateConfigPapers(data){
	const requiredCarPapers = data.requiredCarPapers !== undefined ? `&requiredCarPapers=${data.requiredCarPapers}` : "";
	const requiredCarPapersOther = data.requiredCarPapersOther !== undefined ? `&requiredCarPapersOther=${data.requiredCarPapersOther}` : "";
	const mortgages = data.mortgages !== undefined ? `&mortgages=${data.mortgages}` : "";
	const policies = data.policies !== undefined ? `&policies=${data.policies}` : "";

	return axios({
			method: "POST",
			withCredentials: true,
			url: `${baseDomain}/mapi/profile/update?${requiredCarPapers}${requiredCarPapersOther}${mortgages}${policies}`
	})
}

export function login(username, password) {
    return axios({
        method: "POST",
        withCredentials: true,
        url: `${baseDomain}/mapi/login/pwd?name=${username}&pwd=${password}`
    })
}

export function logout() {
    return axios({
        method: "POST",
        withCredentials: true,
        url: `${baseDomain}/mapi/logout`
    })
}

export function getLoggedProfile() {
    return axios({
        withCredentials: true,
        method: "GET",
        url: `${baseDomain}/mapi/profile/get`
    })
}

export function unLinkFb() {
    return axios({
        method: "POST",
        withCredentials: true,
        url: `${baseDomain}/mapi/link/fb?unlink=true`
    })
}

export function unLinkGg() {
    return axios({
        method: "POST",
        withCredentials: true,
        url: `${baseDomain}/mapi/link/gg?unlink=true`
    })
}

export function verifyPhone(phone, token, otpType) {
    return axios({
        method: "POST",
        withCredentials: true,
        url: `${baseDomain}/mapi/profile/phone/verify?phone=${phone}&code=${token}&otpType=${otpType}`
    })
}

export function verifyEmail(email, token, otp) {
    return axios({
        method: "POST",
        withCredentials: true,
        url: `${baseDomain}/mapi/profile/email/verify?email=${email}&code=${token}&otp=${otp}`
    })
}

export function generateEmailOtp(email) {
    return axios({
        method: "POST",
        withCredentials: true,
        url: `${baseDomain}/mapi/profile/email/verify/generate-otp?email=${email}`
    })
}

export function generateEmailOtpForgot(email) {
    return axios({
        method: "POST",
        withCredentials: true,
        url: `${baseDomain}/mapi/email/forgot/generate-otp?email=${email}`
    })
}

export function checkPhone(phone) {
    return axios({
        method: "GET",
        withCredentials: true,
        url: `${baseDomain}/mapi/check-phone?phone=${phone}`
    })
}

export function checkEmail(email) {
    return axios({
        method: "GET",
        withCredentials: true,
        url: `${baseDomain}/mapi/check-email?email=${email}`
    })
}

export function getProfile(userId, vw) {
    var pVw = "";
    if (vw) {
        pVw = `&vw=${vw}`
    }
    return axios({
        method: "GET",
        withCredentials: true,
        url: `${baseDomainProfile}/profile/detail?userId=${userId}${pVw}`
    })
}

export function uploadAvatar(photo) {
    const url = `${baseUploadDomain}/profile/upload-avatar`;
    const formData = new FormData();
    formData.append('pdata', photo)
    const config = {
        withCredentials: true,
        headers: {
            'content-type': 'multipart/form-data'
        }
    }
    return post(url, formData, config);
}

export function uploadPaper(id, photo) {
    const url = `${baseUploadDomain}/traveler/upload-paper`;
    const formData = new FormData();
    formData.append('paperId', id)
    formData.append('pdata', photo)
    const config = {
        withCredentials: true,
        headers: {
            'content-type': 'multipart/form-data'
        }
    }
    return post(url, formData, config);
}

export function resetPasswordByPhone(phone, token, password) {
    return axios({
        method: "POST",
        withCredentials: true,
        url: `${baseDomain}/mapi/phone/forgot/reset-pwd?phone=${phone}&code=${token}&otpType=1&newPwd=${password}`
    })
}

export function resetPasswordByEmail(email, token, otp, password) {
    return axios({
        method: "POST",
        withCredentials: true,
        url: `${baseDomain}/mapi/email/forgot/reset-pwd?email=${email}&code=${token}&otp=${otp}&newPwd=${password}`
    })
}

export function getUserNotify(fromId, pos, ver) {
    const pFromId = fromId !== 0 ? `fromNotificationId=${fromId}` : "";
    return axios({
        method: "GET",
        withCredentials: true,
        url: `${baseNotifyDomain}/user/notifications?${pFromId}&pos=${pos}&ver=${ver}`
    })
}

export function getUserNotifyDetail(id) {
    return axios({
        method: "GET",
        withCredentials: true,
        url: `${baseNotifyDomain}/user/notification?notificationId=${id}`
    })
}

export function getUserNotifyCouter() {
    return axios({
        method: "GET",
        withCredentials: true,
        url: `${baseNotifyDomain}/user/notification-counter`
    })
}

export function removeUserNotify(id) {
    return axios({
        method: "POST",
        withCredentials: true,
        url: `${baseNotifyDomain}/user/remove-notification?notificationId=${id}`
    })
}

export function seenUserNotify(ids, dCounter) {
    var isDecrease = "";
    if (dCounter) {
        isDecrease = `&dCounter=${dCounter}`
    }
    return axios({
        method: "POST",
        withCredentials: true,
        url: `${baseNotifyDomain}/user/seen-notifications?notificationIds=${ids}${isDecrease}`
    })
}

export function resetUsernotifyCounter() {
    return axios({
        method: "POST",
        withCredentials: true,
        url: `${baseNotifyDomain}/user/reset-notification-counter`
    })
}

export function getPopupNofify(id) {
    return axios({
        method: "GET",
        withCredentials: true,
        url: `${baseNotifyDomain}/snotification/popup-notification?notificationId=${id}`
    })
}

export function seenSystemNotify(id, type) {
    return axios({
        method: "POST",
        withCredentials: true,
        url: `${baseNotifyDomain}/snotification/seen?id=${id}&type=${type}`
    })
}