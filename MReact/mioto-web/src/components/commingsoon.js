import React from "react";

import Header from "./common/header";
import Footer from "./common/footer";

import "../static/css/custom.css";

class CommingSoon extends React.Component {
    render() {
        return (
            <div className="mioto-layout">
                <Header />
                <section className="body">
                    <div className="body-cms">
                        <div className="header-cms">
                            <div className="title-cms">MiOTO</div>
                            <div className="sub-title-cms">Ứng dụng thuê xe tự lái</div>
                            <div className="message-cms">Dự kiến ra mắt <b>28/11/2017</b></div>
                        </div>
                    </div>
                </section>
                <Footer />
            </div>
        );
    }
}

export default CommingSoon;