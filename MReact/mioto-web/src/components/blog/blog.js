import React from "react"
import moment from "moment"
import HtmlToReactParser from "html-to-react"

import { getBlog } from "../../model/blog"
import { commonErr } from "../common/errors"
import Header from "../common/header"
import Footer from "../common/footer"
import { MessagePage } from "../common/messagebox"
import { LoadingPage } from "../common/loading"
import { formatTitleInUrl } from "../common/common"

import bg from "../../static/images/background/bg-blog-1.jpg"

export default class Article extends React.Component {
    constructor() {
        super();

        this.state = {
            err: commonErr.INNIT,
            errMsg: "",
            blog: null
        }
    }

    componentDidMount() {
        const blogId = this.props.match.params.blogId;
        if (blogId) {
            getBlog(blogId).then(resp => {
                this.setState({
                    err: resp.data.error,
                    errMsg: resp.data.errorMessage,
                    blog: resp.data.data
                });
            });
        }
    }

    render() {
        var content;
        if (this.state.err === commonErr.INNIT || this.state.err === commonErr.LOADING) {
            content = <LoadingPage />
        } else if (this.state.blog && this.state.blog.blog) {
            const blog = this.state.blog.blog;
            const relatedBlog = this.state.blog.relateBlogs;

            var htmlToReactParser = new HtmlToReactParser.Parser();
            var blogContent = htmlToReactParser.parse(blog.content);

            content = <div className="blog-detail__sect">
                <div className="blog-detail__title" style={{ backgroundImage: `url(${bg})` }}>
                    <div className="m-container">
                        <h4 className="entry-title">{blog.title}</h4>
                    </div>
                </div>
                <div className="blog-detail__wrap">
                    <div className="b-container">
                        <div className="time-post"> <i className="ic ic-sm-time-post"></i><span>{moment(blog.timeCreated).fromNow()}</span></div>
                        <div className="content-container">
                            <div className="entry-content">
                                {blogContent}
                            </div>
                        </div>
                        <hr className="block-separator" />
                        {relatedBlog && <div className="relevant-blog">
                            <h4 className="title">Các bài viết liên quan</h4>
                            <ul className="relevant-list">
                                {relatedBlog.map(a => <li key={a.id}>
                                    <div className="post-list">
                                        <div className="post-img">
                                        <div className="fix-img">
                                            <a activeClassName="active" href={`/blog/${formatTitleInUrl(a.title)}/${a.id}`}><img src={a.thumb} /></a>
                                            </div>
                                        </div>
                                        <div className="post-title">
                                            <a activeClassName="active" href={`/blog/${formatTitleInUrl(a.title)}/${a.id}`}><h4>{a.title}</h4></a>
                                        </div>
                                    </div>
                                </li>)}
                            </ul>
                        </div>}
                    </div>
                </div>
            </div>
        } else {
            content = <MessagePage message={"Không tìm thấy nội dung."} />
        }

        return <div className="mioto-layout">
            <Header />
            <section className="body">
                {content}
            </section>
            <Footer />
        </div>
    }
}