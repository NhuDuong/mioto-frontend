import React from "react"
import moment from "moment"

import { getBlogPage } from "../../model/blog"
import { commonErr } from "../common/errors"
import Header from "../common/header"
import Footer from "../common/footer"
import { MessagePage } from "../common/messagebox"
import { LoadingPage } from "../common/loading"
import { formatTitleInUrl } from "../common/common"

import defThumb1 from "../../static/images/homev2/blog-1.jpg"
import bg from "../../static/images/background/bg-blog-1.jpg"

export default class Articles extends React.Component {
    constructor() {
        super();

        this.state = {
            err: commonErr.INNIT,
            errMsg: ""
        }
    }

    componentDidMount() {
        getBlogPage(0).then(resp => {
            this.setState({
                err: resp.data.error,
                errMsg: resp.data.errorMessage,
                blogs: resp.data.data,
            });
        });
    }

    next() {
        window.scrollTo(0, 0);
        getBlogPage(this.state.blogs.curPage + 1).then(resp => {
            this.setState({
                err: resp.data.error,
                errMsg: resp.data.errorMessage,
                blogs: resp.data.data
            });
        });
    }

    pre() {
        window.scrollTo(0, 0);
        getBlogPage(this.state.blogs.curPage - 1).then(resp => {
            this.setState({
                err: resp.data.error,
                errMsg: resp.data.errorMessage,
                blogs: resp.data.data
            });
        });
    }

    go(page) {
        window.scrollTo(0, 0);
        getBlogPage(page.number).then(resp => {
            this.setState({
                err: resp.data.error,
                errMsg: resp.data.errorMessage,
                blogs: resp.data.data
            });
        });
    }

    render() {
        var content;
        if (this.state.err === commonErr.INNIT || this.state.err === commonErr.LOADING) {
            content = <LoadingPage />
        } else if (this.state.blogs && this.state.blogs.blogs) {
            const blogs = this.state.blogs.blogs;
            const curPage = this.state.blogs.curPage;
            const maxPage = this.state.blogs.totalPages;
            const pagination = [];
            for (var i = 0; i < maxPage; ++i) {
                const page = { number: i };
                pagination.push(<li>
                    <a className={`${curPage === i ? "active" : ""}`} onClick={() => this.go(page)}>{i + 1}</a>
                </li >);
            }

            content = <div class="m-container">
                <div class="blog-list__wrap">
                    {blogs.map(blog => <div key={blog.id} class="blog-list__item">
                        <a href={`/blog/${formatTitleInUrl(blog.title)}/${blog.id}`}>
                            <div class="blog-list-img">
                                <div class="fix-img"><img src={blog.thumb ? blog.thumb : defThumb1} /></div>
                            </div>
                            <div class="blog-list-detail">
                                <div class="title-blog">
                                    <h3>{blog.title}</h3>
                                </div>
                                <div class="bar-line" />
                                <p class="short-desc">{blog.desc}</p>
                                <div class="group-info">
                                    <div class="time-post"><i class="ic ic-sm-time-post"></i><span>{moment(blog.timeCreated).fromNow()}</span></div>
                                    <div class="s-all">
                                        <a href={`/blog/${formatTitleInUrl(blog.title)}/${blog.id}`}><p class="see-all">Xem tiếp <i class="ic ic-chevron-right"></i></p></a>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>)}
                </div>
                <div class="wrap-btn has-2btn">
                    <ul class="pagination-blog">
                        <li>
                            <a className={`prev btn ${curPage == 0 ? "disabled" : ""}`} onClick={this.pre.bind(this)} title="Trang Trước">Trang trước </a>
                        </li>
                        {pagination}
                        <li>
                            <a className={`next btn ${curPage == maxPage - 1 ? "disabled" : ""}`} onClick={this.next.bind(this)} title="Trang Kế"> Trang kế </a>
                        </li>
                    </ul>
                </div>
            </div>
        } else {
            content = <MessagePage message={"Không tìm thấy nội dung."} />
        }

        return <div className="mioto-layout">
            <Header />
            <section className="body">
                <div class="blog-list__sect">
                    <div class="blog-list__title" style={{ backgroundImage: `url(${bg})` }}>
                        <div class="m-container">
                            <h4 class="blog-title">MIOTO'S BLOG</h4>
                        </div>
                    </div>
                    {content}
                </div>
            </section>
            <Footer />
        </div>
    }
}