import React, { Component } from 'react';
import { connect } from "react-redux"
import NumberFormat from "react-number-format";
import { StickyContainer, Sticky } from 'react-sticky';
import moment from 'moment';

import { logout, getLoggedProfile } from "../../actions/sessionAct";

import { getBalance, getInvoiceByMonth } from '../../model/wallet';
import { getProfile } from "../../model/profile";

import Header from "../common/header"; 
import { MessageLine } from "../common/messagebox";
import { commonErr } from "../common/errors";
import { LoadingPage, LoadingInline } from "../common/loading";


class MyMonthlyReport extends Component {
	constructor(props) {
        super(props);
				this.state = {
					invalidWidth: false,
				}

		
		this.getTotalTrasactionAmount = this.getTotalTrasactionAmount.bind(this);
		this.getProfile = this.getProfile.bind(this);
	}

	componentDidMount() {	
		if (!this.props.session.profile.info || !this.props.session.profile.info.uid) {
            this.getProfile();
        }
		const monthInMiliseconds = moment(this.props.match.params.month, ["MM-YYYY"]).valueOf();
		this.getWalletInfo(monthInMiliseconds);	

	}


	getProfile() {
        this.props.dispatch(getLoggedProfile());
    }

	getTotalTrasactionAmount(listTransaction){
		var total = 0;
		var i;
		if(listTransaction && listTransaction.length > 0) {
			for (i = 0; i < listTransaction.length; ++i) {
				var transaction = listTransaction[i];
				total += transaction.amount;
			}
		}
		return total;
	}


	getWalletInfo(monthInMilliseconds){
		var month = monthInMilliseconds > 0 ? monthInMilliseconds : moment().startOf('month').valueOf();
		const self = this;
		getBalance().then(function (resp){
			if (resp.data.error === commonErr.SUCCESS) {
				self.setState({					
					balance: resp.data.data.balance
				})
			} else {
				self.setState({
					err: resp.data.error,
				})
			}
		})
		getInvoiceByMonth(month).then(function (resp) {
			if (resp.data.error === commonErr.SUCCESS){
				self.setState({
					startDate: moment(month).startOf('month').format("DD-MM-YYYY"),
					endDate: moment(month).endOf('month').format("DD-MM-YYYY"),
					transactionsFinished: resp.data.data.transactionsFinished,
					transactionsCanceled: resp.data.data.transactionsCanceled,
					transactionsOther: resp.data.data.transactionsOther,
					totalTripFinished: resp.data.data.totalTripFinished,
					rating: resp.data.data.rating.avg,
					responseRate: resp.data.data.responseRate,
					responseTime: resp.data.data.responseTime,
					acceptRate: resp.data.data.acceptRate,	
					openingBalance: resp.data.data.openingBalance,
					endingBalance: resp.data.data.endingBalance			
				})
			} else {
				self.setState({
					err: resp.data.error,
					data: null
				})
			}
		})
	}


	render() {
		var content;
		if (this.state.invalidWidth) {
			content = <section className="body">
				<div className="sidebar-control z-2">
					<div className="sidebar-settings general-settings">
						<ul>
							<li> <a href="/mycars"><i className="ic ic-cars"></i><span className="label">Danh sách xe</span></a></li>
							<li> <a href="/calendars"><i className="ic ic-setting-calendar"></i><span className="label">Lịch chung</span></a></li>
							<li> <a href="/gps"><i className="ic ic-gps"></i><span className="label">GPS</span><sup class="new-func">BETA</sup></a></li>
							<li> <a href="/commonsetting"><i className="ic ic-setting-rent"> </i><span className="label">Cài đặt chung</span></a></li>
							<li> <a href="/carregister"><i className="ic ic-plus-black"></i><span className="label">Đăng ký xe </span></a></li>
							<li> <a href="/mywallet"><i className="ic ic-owner-wallet"></i><span className="label">Số dư: <span className="fontWeight-6 font-16"><NumberFormat value={this.state.balance} displayType={'text'} thousandSeparator={" "} decimalSeparator={","} suffix={"đ"} /></span></span></a></li>	
						</ul>
					</div>
				</div>
				<MessageLine message="Bảng Sao kê chi tiết giao dịch chỉ hỗ trợ xem trên màn hình PC, tablet (từ 1024px trở lên). Vui lòng xoay ngang màn hình hoặc sử dụng màn hình lớn hơn." />
			</section>
		} else if (this.state.err === commonErr.LOADING) {
            content = <LoadingPage />
		} else { 
			content = <section className="body">
			<div className="sidebar-control z-2">
				<div className="sidebar-settings general-settings">
					<ul>
						<li> <a href="/mycars"><i className="ic ic-cars"></i><span className="label">Danh sách xe</span></a></li>
						<li> <a href="/calendars"><i className="ic ic-setting-calendar"></i><span className="label">Lịch chung</span></a></li>
						<li> <a href="/gps"><i className="ic ic-gps"></i><span className="label">GPS</span><sup class="new-func">BETA</sup></a></li>
						<li> <a href="/commonsetting"><i className="ic ic-setting-rent"> </i><span className="label">Cài đặt chung</span></a></li>
						<li> <a href="/carregister"><i className="ic ic-plus-black"></i><span className="label">Đăng ký xe </span></a></li>
						<li> <a href="/mywallet"><i className="ic ic-owner-wallet"></i><span className="label">Số dư: <span className="fontWeight-6 font-16"><NumberFormat value={this.state.balance} displayType={'text'} thousandSeparator={" "} decimalSeparator={","} suffix={"đ"} /></span></span></a></li>	
					</ul>
				</div>
			</div>
			<div className="owner-transaction__wrap">
				<div className="f-container">
					<div className="main-title"> 
						<a className="btn-back-list" href="/mywallet" title="Quay lại"><i className="i-arr-left-dark"></i></a>
						<h3 className="n-title">SAO KÊ CHI TIẾT GIAO DỊCH</h3>
						<p className="date">Từ ngày <span>{this.state.startDate} </span>Đến ngày <span>{this.state.endDate}</span></p>
					</div>
					<div className="info-owner__box">
						<div className="info-owner">
							<p>Chủ xe </p>
							<p>Mã số</p>
						</div>
						<div className="info-owner">
							<p>{this.props.session.profile.info && this.props.session.profile.info.name}</p>
							<p>{this.props.session.profile.info && this.props.session.profile.info.uid}</p>
						</div>
					</div>
					<div className="trip-completed__wrap"> 
						<h4>Chuyến đi hoàn thành trong kì</h4>
						<div className="trip-completed__table"> 
							<table> 
								<thead>
									<tr> 
										<th colSpan="4">Thời gian</th>
										<th colSpan="3">Thông tin chuyến đi</th>
										<th colSpan="2">Thanh toán</th>
										<th rowSpan="2"> Mioto KM </th>
										<th rowSpan="2"> Phí vận hành</th>
										<th rowSpan="2"> Thay đổi số dư</th>
									</tr>
									<tr> 
										<th>Mã chuyến đi</th>
										<th>Ngày đi</th>
										<th>Ngày về </th>
										<th>Ngày đặt xe</th>
										<th>Khách hàng</th>
										<th>Xe thuê</th>
										<th>Đơn giá</th>
										<th>Đặt cọc tại Mioto</th>
										<th>Thanh toán chủ xe</th>
									</tr>
								</thead>
								<tbody>
									{this.state.transactionsFinished && this.state.transactionsFinished.map(transaction => <tr> 
										<td><a style={{color: '#00a550'}} href={`/trip/detail/${transaction.trip.id}`} target="_blank">{transaction.trip.id}</a></td>
										<td>{moment(transaction.trip.tripDateFrom).format("DD/MM/YYYY")}</td>
										<td>{moment(transaction.trip.tripDateFrom).format("DD/MM/YYYY")}</td>
										<td>{moment(transaction.trip.timeBooked).format("DD/MM/YYYY")}</td>
										<td>{transaction.trip.travelerName}</td>
										<td>{transaction.trip.carName}</td>
										<td><NumberFormat value={transaction.trip.price} displayType={'text'} thousandSeparator={" "} decimalSeparator={","} suffix={"đ"} /></td>
										<td><NumberFormat value={transaction.trip.deposit} displayType={'text'} thousandSeparator={" "} decimalSeparator={","} suffix={"đ"} /></td>
										<td><NumberFormat value={transaction.trip.payAfter} displayType={'text'} thousandSeparator={" "} decimalSeparator={","} suffix={"đ"} /></td>
										<td><NumberFormat value={transaction.trip.promotion} displayType={'text'} thousandSeparator={" "} decimalSeparator={","} suffix={"đ"} /></td>
										<td><NumberFormat value={transaction.trip.fee} displayType={'text'} thousandSeparator={" "} decimalSeparator={","} suffix={"đ"} /></td>
										<td><NumberFormat value={transaction.amount} displayType={'text'} thousandSeparator={" "} decimalSeparator={","} suffix={"đ"} /></td>
									</tr> )}			
								</tbody>
								<tfoot>
									<tr> 
										<th colSpan="11">Tổng thay đổi - Chuyến đi hoàn thành</th>
											<th><NumberFormat value={this.getTotalTrasactionAmount(this.state.transactionsFinished)} displayType={'text'} thousandSeparator={" "} decimalSeparator={","} suffix={"đ"}/></th>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>
					<div className="transaction-in-month__wrap"> 
						<h4>Giao dịch rút/nộp tiền trong kì</h4>
						<div className="transaction-in-month__table"> 
							<table> 
								<thead> 
									<tr> 
										<th>Ngày giao dịch </th>
										<th>Nội dung </th>
										<th>Thay đổi số dư </th>
									</tr>
								</thead>
								<tbody>{this.state.transactionsOther && this.state.transactionsOther.map(transaction => <tr>
										<td>{moment(transaction.timeCreated).format("DD/MM/YYYY")}</td>
										<td>{transaction.comment}</td>
										<td><NumberFormat value={transaction.amount} displayType={'text'} thousandSeparator={" "} decimalSeparator={","} suffix={"đ"} /></td>
									</tr>)}
								</tbody>
								<tfoot> 
									<tr>
										<th colSpan="2">Tổng thay đổi - Giao dịch rút/nộp tiền</th>
										<th><NumberFormat value={this.getTotalTrasactionAmount(this.state.transactionsOther)} displayType={'text'} thousandSeparator={" "} decimalSeparator={","} suffix={"đ"} /></th>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>
					<div className="cancel-transaction__wrap"> 
						<h4>Giao dịch hủy chuyến trong kì</h4>
						<div className="cancel-transaction__table"> 
							<table> 
								<thead>
									<tr> 
										<th colSpan="4">Thời gian</th>
										<th colSpan="3">Thông tin chuyến đi</th>
										<th colSpan="2">Thanh toán</th>
										<th rowSpan="2"> Nội dung hủy chuyến</th>
										<th rowSpan="2"> Thay đổi số dư</th>
									</tr>
									<tr> 
										<th>Mã chuyến đi</th>
										<th>Ngày đi</th>
										<th>Ngày về </th>
										<th>Ngày hủy chuyến</th>
										<th>Khách hàng</th>
										<th>Xe thuê</th>
										<th>Đơn giá</th>
										<th>Đặt cọc tại Mioto</th>
										<th>Thanh toán chủ xe</th>
									</tr>
								</thead>
									<tbody>{this.state.transactionsCanceled && this.state.transactionsCanceled.map(transaction => <tr> 
											<td><a style={{color: '#00a550'}} href={`/trip/detail/${transaction.trip.id}`} target="_blank">{transaction.trip.id}</a></td>
											<td>{moment(transaction.trip.tripDateFrom).format("DD/MM/YYYY")}</td>
											<td>{moment(transaction.trip.tripDateFrom).format("DD/MM/YYYY")}</td>
											<td>{moment(transaction.trip.timeBooked).format("DD/MM/YYYY")}</td>
											<td>{transaction.trip.travelerName}</td>
											<td>{transaction.trip.carName}</td>
											<td><NumberFormat value={transaction.trip.price} displayType={'text'} thousandSeparator={" "} decimalSeparator={","} suffix={"đ"} /></td>
											<td><NumberFormat value={transaction.trip.deposit} displayType={'text'} thousandSeparator={" "} decimalSeparator={","} suffix={"đ"} /></td>
											<td><NumberFormat value={transaction.trip.payAfter} displayType={'text'} thousandSeparator={" "} decimalSeparator={","} suffix={"đ"} /></td>
											<td>{transaction.trip.reasonCanceled}</td>
											<td><NumberFormat value={transaction.amount} displayType={'text'} thousandSeparator={" "} decimalSeparator={","} suffix={"đ"} /></td>
									</tr>)}
								</tbody>
								<tfoot>
									<tr> 
										<th colSpan="10">Tổng thay đổi - Giao dịch hủy chuyến</th>
										<th><NumberFormat value={this.getTotalTrasactionAmount(this.state.transactionsCanceled)} displayType={'text'} thousandSeparator={" "} decimalSeparator={","} suffix={"đ"} /></th>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>
					<div className="owner-report">
						<div className="group-summary">
							<div className="transaction-summary">
								<p className="text-sm">TỔNG CỘNG THAY ĐỔI TRONG KÌ</p>
								<p className="normal"><NumberFormat value={this.state.endingBalance - this.state.openingBalance} displayType={'text'} thousandSeparator={" "} decimalSeparator={","} suffix={"đ"} /></p>
							</div>
							<div className="transaction-summary">
								<p className="text-sm">TIỀN ĐẦU KÌ</p>
								<p className="normal"><NumberFormat value={this.state.openingBalance} displayType={'text'} thousandSeparator={" "} decimalSeparator={","} suffix={"đ"} /></p>
							</div>
							<div className="transaction-summary">
								<p className="text-sm">TIỀN CUỐI KÌ</p>
								<p className="sum-price"><NumberFormat value={this.state.endingBalance} displayType={'text'} thousandSeparator={" "} decimalSeparator={","} suffix={"đ"} /></p>
							</div>
						</div>
					</div>
					<p className="note">Ghi chú: Mọi vấn đề thắc mắc về thông tin ghi nhận trên bản sao kê chi tiết giao dịch, Quý đối tác vui lòng liên hệ với bộ phận Chăm Sóc Khách Hàng của Mioto tại 19009217 để biết thêm chi tiết. Xin cám ơn!</p>
				</div>
			</div>
		</section>

		}
		return 	<div className="mioto-layout">
							<Header />
							{content}
						</div>
	}
}

function mapState(state) {
    return {
        session: state.session
    }
}

MyMonthlyReport = connect(mapState)(MyMonthlyReport)

export default MyMonthlyReport;