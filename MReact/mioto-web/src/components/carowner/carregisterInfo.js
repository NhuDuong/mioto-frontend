import React from "react"
import moment from 'moment'

export default class CarRegisterInfo extends React.Component {
    constructor(props) {
        super();

        var i;
        const seats = [];
        for (i = 4; i <= 20; ++i) {
            seats.push(i);
        }

        const years = [];
        const curYear = moment().year();
        for (i = curYear; i >= 1960; --i) {
            years.push(i);
        }

        this.state = {
            seats: seats,
            years: years
        }
    }

    componentDidMount() {
        window.scrollTo(0, 0);
    }

    onInviteCodeChange(event) {
        this.props.onInviteCodeChange(event);
    }

    onLicensePlateChange(event) {
        this.props.onLicensePlateChange(event);
    }

    onVehicleTypeChange(event) {
        this.props.onVehicleTypeChange(event);
    }

    onVehicleMakeChange(event) {
        this.props.onVehicleMakeChange(event);
    }

    onVehicleModelChange(event) {
        this.props.onVehicleModelChange(event);
    }

    onYearChange(event) {
        this.props.onYearChange(event);
    }

    onSeatChange(event) {
        this.props.onSeatChange(event);
    }

    onTransmisionChange(event) {
        this.props.onTransmisionChange(event);
    }

    onFuelChange(event) {
        this.props.onFuelChange(event);
    }

    onFuelRateChange(event) {
        this.props.onFuelRateChange(event);
    }

    onDescChange(event) {
        this.props.onDescChange(event);
    }

    onFeaturesChange(event) {
        this.props.onFeaturesChange(event);
    }

    render() {
        return <div className="group form-default">
            <h6>Mã giới thiệu</h6>
            <p className="fl"><span className="note">Nếu bạn nhận được mã giới thiệu để đăng kí xe, vui lòng nhập vào ô bên dưới.</span></p>
            <div className="space m"/>
            <div className="space m"/>
            <div className="col-left">
                <div className="line-form">
                    <div className="wrap-input">
                        <input type="text" onChange={this.onInviteCodeChange.bind(this)} value={this.props.inviteCode} />
                    </div>
                </div>
            </div>
            <div className="space m clear"/>
            <div className="clear"/>
            <h6>Biển số xe</h6>
            <p className="fl"><span className="note" style={{ color: "red" }}>Lưu ý: Biển số sẽ không thể thay đổi sau khi đăng kí.</span></p>
            <div className="space m"/>
            <div className="space m"/>
            <div className="col-left">
                <div className="line-form">
                    <div className="wrap-input">
                        <input type="text" onChange={this.onLicensePlateChange.bind(this)} value={this.props.licensePlate} />
                    </div>       
                </div>
            </div>
            <div className="space m clear"/>
            <div className="clear"></div>
            <h6>Thông tin cơ bản</h6>
            <p className="fl"><span className="note" style={{ color: "red" }}>Lưu ý: Các thông tin cơ bản sẽ không thể thay đổi sau khi đăng kí.</span></p>
            <div className="space m"></div>
            <div className="col-left">
                <div className="line-form">
                    <label className="label">Loại xe</label>
                    <span className="wrap-select">
                        <select onChange={this.onVehicleTypeChange.bind(this)} value={this.props.vehicleType}>
                            <option value="0">Chưa chọn</option>
                            {this.props.vehicleTypes && this.props.vehicleTypes.map(option => <option key={option.id} value={option.id}>{option.name}</option>)}
                        </select>
                    </span>
                </div>
            </div>
            <div className="col-right">
                <div className="line-form">
                    <label className="label">Hãng xe</label>
                    <span className="wrap-select">
                        <select disabled={!this.props.vehicleType || this.props.vehicleType === 0} onChange={this.onVehicleMakeChange.bind(this)} value={this.props.vehicleMake}>
                            <option value="0">{(!this.props.vehicleType || this.props.vehicleType === 0) ? "Chọn loại xe trước" : "Chưa chọn"}</option>
                            {this.props.vehicleMakes && this.props.vehicleMakes.map(option => <option key={option.id} value={option.id}>{option.name}</option>)}
                        </select>
                    </span>
                </div>
            </div>
            <div className="col-left">
                <div className="line-form">
                    <label className="label">Mẫu xe</label>
                    <span className="wrap-select">
                        <select disabled={!this.props.vehicleMake || this.props.vehicleMake === 0} onChange={this.onVehicleModelChange.bind(this)} value={this.props.vehicleModel}>
                            <option value="0">{!this.props.vehicleMake || this.props.vehicleMake === 0 ? "Chọn hãng xe trước" : "Chưa chọn"}</option>
                            {this.props.vehicleModels && this.props.vehicleModels.map(option => <option key={option.id} value={option.id}>{option.name}</option>)}
                        </select>
                    </span>
                </div>
            </div>
            <div className="space clear"/>
            <div className="col-left">
                <div className="line-form">
                    <label className="label">Số ghế</label><span className="wrap-select">
                        <select onChange={this.onSeatChange.bind(this)} value={this.props.seat}>
                            {this.state.seats.map(seat => <option key={seat} value={seat}>{seat}</option>)}
                        </select>
                    </span>
                </div>
            </div>
            <div className="col-right">
                <div className="line-form">
                    <label className="label">Năm sản xuất</label>
                    <span className="wrap-select">
                        <select onChange={this.onYearChange.bind(this)} value={this.props.year}>
                            {this.state.years.map(year => <option key={year} value={year}>{year}</option>)}
                        </select>
                    </span>
                </div>
            </div>
            <div className="space clear"/>
            <div className="col-left">
                <div className="line-form">
                    <label className="label">Truyền động</label><span className="wrap-select">
                        <select onChange={this.onTransmisionChange.bind(this)} value={this.props.transmision}>
                            <option value="1">Số tự động</option>
                            <option value="2">Số sàn</option>
                        </select>
                    </span>
                </div>
            </div>
            <div className="col-right">
                <div className="line-form">
                    <label className="label">Loại nhiên liệu</label>
                    <span className="wrap-select">
                        <select onChange={this.onFuelChange.bind(this)} value={this.props.fuel}>
                            <option value="1">Xăng</option>
                            <option value="2">Dầu diesel</option>
                        </select>
                    </span>
                </div>
            </div>
            <div className="space m"/>
            <div className="space clear"/>
            <h6>Mức tiêu thụ nhiên liệu</h6>
            <p className="fl"><span className="note">Số lít nhiên liệu cho quãng đường 100km.</span></p>
            <div className="clear"/>
            <div className="col-left">
                <div className="line-form">
                    <label className="label"/>
                    <div className="wrap-input">
                        <input type="text" placeholder="" onChange={this.onFuelRateChange.bind(this)} value={this.props.fuelRate} />
                    </div>
                </div>
            </div>
            <div className="space clear"/>
            <h6>Mô tả</h6>
            <textarea className="textarea" onChange={this.onDescChange.bind(this)} value={this.props.desc} placeholder="Mô tả ngắn gọn về xe của bạn."></textarea>
            <div className="space m"/>
            <h6>Tính năng</h6>
            <div className="list-features">
                <ul>
                    {this.props.featuresAll && this.props.featuresAll.map(option => <li key={option.id}>
                        <div className="squaredFour have-label">
                            <input id={`${option.id}`} type="checkbox" checked={option.checked ? true : false} value={option.id} onChange={this.onFeaturesChange.bind(this)} />
                            <label htmlFor={`${option.id}`}> <img style={{ width: "16px", height: "16px" }} className="img-ico" src={option.logo} alt="Mioto - Thuê xe tự lái" /> {option.name}</label>
                        </div>
                    </li>)}
                </ul>
            </div>
        </div>
    }
} 