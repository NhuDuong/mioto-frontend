import React from "react"
import moment from 'moment'
import { Link } from 'react-router-dom'
import InfiniteScroll from "react-infinite-scroller"

import { formatPrice, getTripStatus } from "../common/common"
import { getCarTrips } from "../../model/car"
import { commonErr } from "../common/errors"
import { MessagePage } from "../common/messagebox"
import { LoadingInline } from "../common/loading"

import avatar_default from "../../static/images/avatar_default.png"

class TripItem extends React.Component {
    render() {
        const trip = this.props.trip;
        const renter = this.props.renter;
        const status = getTripStatus(trip.status);

        return <div className="trip-box">
            <div className="trip-header"></div>
            <div className="space m"></div>
            <div className="trip-body">
                <div className="left">
                    <p> Bắt đầu: {moment(trip.tripDateFrom).format("HH:mm dddd, DD/MM/YYYY")}</p>
                    <p> Kết thúc: {moment(trip.tripDateTo).format("HH:mm dddd, DD/MM/YYYY")}</p>
                    <p className="total-price">
                        <i className="ic ic-total-price"></i>Tổng tiền {formatPrice(trip.priceSummary.priceTotal)}
                    </p>
                </div>
                <div className="right">
                    <div className="avatar">
                        <div className="avatar-img">
                            <Link to={`/profile/${renter.uid}`}>
                                <img src={`${(renter.avatar && renter.avatar.thumbUrl) ? renter.avatar.thumbUrl : avatar_default}`} alt="Mioto - Thuê xe tự lái" />
                            </Link>
                        </div>
                    </div>
                    <div className="lstitle">{renter.name}</div>
                </div>
            </div>
            <Link to={{ pathname: `/trip/detail/${trip.id}` }}>
                <div className="trip-footer">
                    <div className="status-trips">
                        <p><span className={status.class} /> {status.name}</p>
                    </div>
                    <div className="time">
                        <p>{moment(trip.timeBooked).fromNow()}</p>
                    </div>
                </div>
            </Link>
        </div>
    }
}

export default class CarSettingTrips extends React.Component {
    constructor() {
        super();
        this.state = {
            err: commonErr.INNIT,

            filterStatus: 0,
            trips: [],
            cars: [],
            profiles: []
        }
    }

    getTrips(carId, pos) {
        getCarTrips(carId, this.state.filterStatus, 0, pos, 0).then(resp => {
            if (resp.data.error >= commonErr.SUCCESS) {
                this.setState({
                    trips: resp.data.data.trips,
                    cars: resp.data.data.cars,
                    profiles: resp.data.data.profiles,
                    more: resp.data.data.more,
                    err: resp.data.error
                });
            } else {
                this.setState({
                    err: resp.data.error
                });
            }
        });
    }

    init(car) {
        this.setState({
            car: car
        });
        this.getTrips(car.id, 0);
    }

    componentDidMount() {
        const car = this.props.car;
        this.init(car);
    }

    componentWillReceiveProps(props) {
        const car = props.car;
        this.init(car);
    }

    getTripsMore() {
        const carId = this.props.car.id;
        const nextPos = this.state.trips.length + 10;
        getCarTrips(carId, this.state.filterStatus, 0, nextPos, 0).then(resp => {
            if (resp.data.error >= commonErr.SUCCESS && resp.data.data.trips) {
                this.setState({
                    trips: this.state.trips.concat(resp.data.data.trips),
                    profiles: this.state.profiles.concat(resp.data.data.profiles),
                    more: resp.data.data.more,
                    err: resp.data.error
                });
            } else {
                this.setState({
                    err: resp.data.error
                });
            }
        });
    }

    onStatusFilterChange(event) {
        this.setState({
            filterStatus: event.target.value
        });
    }

    onStatusFilterReset() {
        this.setState({
            filterStatus: 0
        });
    }

    render() {
        var content;
        if (this.state.err === commonErr.INNIT) {
            content = <LoadingInline />
        } else {
            var bodyContent;
            if (this.state.trips && this.state.profiles) {
                const trips = this.state.trips;
                const profiles = this.state.profiles;
                var i, j;
                if (trips.length > 0 && profiles.length > 0) {
                    var subContent = [];
                    for (i = 0; i < trips.length; ++i) {
                        const trip = trips[i];
                        for (j = 0; j < profiles.length; ++j) {
                            const profile = profiles[j];
                            if (profile.uid === trip.travelerId) {
                                subContent.push(<TripItem key={trip.id} trip={trip} renter={profile} />);
                            }
                            break;
                        }
                    }

                    bodyContent = <div className="content-container">
                        <InfiniteScroll pageStart={0}
                            loadMore={this.getTripsMore.bind(this)}
                            hasMore={this.state.more === 1}
                            loader={<ul><LoadingInline /></ul>}>
                            {subContent}
                        </InfiniteScroll>
                        <div className="space m"></div>
                        <div className="space m"></div>
                        <div className="space m"></div>
                    </div>
                } else {
                    bodyContent = <MessagePage message={"Không tìm thấy chuyến nào."} />
                }
            } else {
                bodyContent = <MessagePage message={"Không tìm thấy chuyến nào."} />
            }

            content = <div className="content">
                {bodyContent}
            </div>
        }

        return content;
    }
}