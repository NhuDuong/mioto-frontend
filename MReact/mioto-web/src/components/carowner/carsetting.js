import React from "react"
import { StickyContainer, Sticky } from "react-sticky"
import { connect } from "react-redux"
import NumberFormat from "react-number-format";

import Header from "../common/header"
import Footer from "../common/footer"
import CarSettingHeader from "./carsettingheader"
import CarSettingSideBar from "./carsettingsidebar"
import CarSettingSideBarMobile from "./carsettingsidebarmobile"
import CarSettingInfo from "./carsettinginfo"
import CarSettingCalendar from "./carsettingcalendar"
import CarSettingPapers from "./carsettingpapers"
import CarSettingPhotos from "./carsettingphotos"
import CarSettingRenting from "./carsettingrenting"
import CarSettingPrice from "./carsettingprice"
import CarSettingTrips from "./carsettingtrips"
import CarSettingGPS from "./carsettinggps"
import CarSettingMortgages from "./carsettingmortgages"
import { getCarDetail } from "../../model/car"
import { commonErr } from "../common/errors"
import { getOwnerCars } from "../../model/car"
import { getBalance } from '../../model/wallet';
import Session from "../common/session"

class CarSetting extends React.Component {
    constructor(props) {
        super();

        this.state = {
            err: commonErr.INNIT,
            errMsg: "",
            handler: ""
        }
    }

    componentDidMount() {
        const carId = this.props.match.params.carId;
        getCarDetail(carId).then(carResp => {
            if (carResp.data.error >= commonErr.SUCCESS && carResp.data.data) {
                getOwnerCars(0, 0, 0, 0, true).then((resp) => {
                    if (resp.data.error >= commonErr.SUCCESS && resp.data.data.cars) {
                        var cars = resp.data.data.cars;
                        this.setState({
                            cars: cars,
                            car: carResp.data.data.car
                        });
                    } else {
                        this.setState({
                            err: resp.data.error
                        });
                    }
                });
            } else {
                this.setState({
                    err: carResp.data.error
                });
            }
        });
        getBalance().then((resp) => {
			if (resp.data.error === commonErr.SUCCESS) {
				this.setState({					
					balance: resp.data.data.balance
				})
			} else {
				this.setState({
					err: resp.data.error,
				})
			}
		})
    }

    componentWillReceiveProps(props) {
        const carId = props.match.params.carId;
        if (carId !== this.props.match.params.carId) {
            getCarDetail(carId).then(resp => {
                if (resp.data.error >= commonErr.SUCCESS && resp.data.data) {
                    this.setState({
                        car: resp.data.data.car
                    });
                }
            });
        }
    }

    getCar() {
        const carId = this.props.match.params.carId;
        getCarDetail(carId).then(carResp => {
            this.setState({
                car: carResp.data.data.car
            });
        });
    }

    render() {
        const carId = this.props.match.params.carId;;
        const handler = this.props.location.hash;
        const cars = this.state.cars;
        const car = this.state.car;
        const profile = this.props.session.profile.info;
        var content;

        if (profile && profile.uid) {
            if (car) {
                switch (handler) {
                    case "#gpssetting":
                        content = <CarSettingGPS car={car} />
                        break;
                    case "#calendarsetting":
                        content = <CarSettingCalendar car={car} />
                        break;
                    case "#paperssetting":
                        content = <CarSettingPapers car={car} />
                        break;
                    case "#photossetting":
                        content = <CarSettingPhotos car={car} />
                        break;
                    case "#pricesetting":
                        content = <CarSettingPrice car={car} />
                        break;
                    case "#rentingsetting":
                        content = <CarSettingRenting car={car} />
                        break;
                    case "#tripssetting":
                        content = <CarSettingTrips car={car} />
                        break;
                    case "#infosetting":
                        content = <CarSettingInfo car={car} />
												break;
										case "#mortgagessetting":
												content = <CarSettingMortgages car={car} />
												break;
                    default:
                        content = <CarSettingInfo car={car} />
                }
            }
        }

        return <div canvas="container">
            <div className="mioto-layout">
                <Session />
                <Header />
                <section className="body">
                    <div className="sidebar-control z-2">
                        <div className="sidebar-settings general-settings">
                            <ul>
                                <li> <a href="/mycars"><i className="ic ic-cars"></i><span className="label">Danh sách xe</span></a></li>
                                <li> <a href="/calendars"><i className="ic ic-setting-calendar"></i><span className="label">Lịch chung</span></a></li>
                                <li> <a href="/gps"><i className="ic ic-gps"></i><span className="label">GPS</span><sup class="new-func">BETA</sup></a></li>
                                <li> <a href="/commonsetting"><i className="ic ic-setting-rent"> </i><span className="label">Cài đặt chung</span></a></li>
                                <li> <a href="/carregister"><i className="ic ic-plus-black"></i><span className="label">Đăng ký xe </span></a></li>
                                <li> <a href="/mywallet"><i className="ic ic-owner-wallet"></i><span className="label">Số dư: <span className="fontWeight-6 font-16"><NumberFormat value={this.state.balance} displayType={'text'} thousandSeparator={" "} decimalSeparator={","} suffix={"đ"} /></span></span></a></li>	
                            </ul>
                        </div>
                    </div>
                    <CarSettingSideBarMobile carId={carId} handler={handler} />
                    {car && cars && <CarSettingHeader car={car} cars={cars} handler={handler} getCar={this.getCar.bind(this)} />}
                    <div className="body-container settings min-height-no-footer">
                        <CarSettingSideBar carId={carId} handler={handler} />
                        {content}
                    </div>
                </section>
                <Footer />
            </div>
        </div>
    }
}

function mapState(state) {
    return {
        session: state.session
    }
}

CarSetting = connect(mapState)(CarSetting)

export default CarSetting;