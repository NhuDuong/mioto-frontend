import React from "react"

import { getCarSetting, updateCar } from "../../model/car"
import { getLoggedProfile} from "../../model/profile"
import { commonErr } from "../common/errors"
import { LoadingInline, LoadingOverlay } from "../common/loading"
import { MessageBox, MessageLine } from "../common/messagebox"

class CarSettingMortgages extends React.Component {
	constructor(){
		super();
		this.state = {
				err: commonErr.INNIT,
				errMsg: "",
				updateErrMsg: "",
		}
		this.toggleMortgagesSpecific = this.toggleMortgagesSpecific.bind(this); 
		this.updateCarInfo = this.updateCarInfo.bind(this);
	}
	
	init(car) {
		getCarSetting({ carId: car.id }).then((settingResp) => {
				const setting = settingResp.data.data;
				var i, j;
				if (setting) {
						const mortgages = [];
					
					if (setting.mortgagesSpecific === 0) {
						getLoggedProfile().then((profileResp) => {
							const profile = profileResp.data.data.profile;
									if (profile.mortgages) {
										for (j = 0; j < profile.mortgages.length; ++j) {
											var mortgage = {
												id: profile.mortgages[j].id,
												name: profile.mortgages[j].name,
												groupId: profile.mortgages[j].groupId,
												edit: profile.mortgages[j].edit === 1,
												groupRequired: profile.mortgages[j].groupRequired === 1,
												checked: profile.mortgages[j].val === 1
											}
											mortgages.push(mortgage)
										}
										
									}
									if (profile) {
												this.setState({
														err: profileResp.data.error,
														errMsg: profileResp.data.errorMessage,
														car: car,
														mortgagesSpecific: setting.mortgagesSpecific === 1,
														mortgages: mortgages,
														setting: setting
												})
										}
								});
						} else {
								if (setting.mortgages) {
										for (i = 0; i < setting.mortgages.length; ++i) {
												var mortgage = {
														id: setting.mortgages[i].id,
														name: setting.mortgages[i].name,
														groupId: setting.mortgages[i].groupId,
														edit: setting.mortgages[i].edit === 1,
														groupRequired: setting.mortgages[i].groupRequired === 1,
														checked: setting.mortgages[i].val === 1
												}
												mortgages.push(mortgage)
										}
										for (i = 0; i < mortgages.length; ++i) {
												for (j = 0; j < car.mortgages.length; ++j) {
														if (mortgages[i].id === car.mortgages[j].id && car.mortgages[j].val === 1) {
															mortgages[i].checked = true;
														}
												}
										}
								}
								this.setState({
										err: settingResp.data.error,
										errMsg: settingResp.data.errorMessage,
										car: car,
										mortgagesSpecific: setting.mortgagesSpecific === 1,
										mortgages: mortgages,
										setting: setting
								})
						}
				} else {
						this.setState({
								err: settingResp.data.error,
								errMsg: settingResp.data.errorMessage
						})
			}
	
		})
	}

	componentDidMount() {
		const car = this.props.car;
		this.init(car);
	}

	componentWillReceiveProps(props) {
		const car = props.car;
		this.init(car);
	}
	
	toggleMortgagesSpecific() {
		const required = !this.state.mortgagesSpecific;
		
			var mortgages = [];
			if (required) {
				getCarSetting({ carId: this.state.car.id }).then((settingResp) => {
					const setting = settingResp.data.data;
					var i, j;
					if (setting) {
						if (setting.mortgages) {
							for (var i = 0; i < setting.mortgages.length; ++i){
								var mortgage = {
									id: setting.mortgages[i].id,
									name: setting.mortgages[i].name,
									groupId: setting.mortgages[i].groupId,
									edit: setting.mortgages[i].edit === 1,
									groupRequired: setting.mortgages[i].groupRequired === 1,
									checked: setting.mortgages[i].val === 1
								}
								mortgages.push(mortgage)
							}
						}
					}
					if (this.state.car.mortgages) {
						for (var i = 0; i < mortgages.length; ++i)
							for (var j = 0; j < this.state.car.mortgages.length; ++j) {
								if (mortgages[i].id === this.state.car.mortgages[j].id && this.state.car.mortgages[j].val === 1) {
									mortgages[i].checked = true;
									break;
							}
						}		
					}
					this.setState({
							mortgagesSpecific: required,
							mortgages: mortgages,
					});
				})          
			} else {
					getLoggedProfile().then(profileResp => {
							const profile = profileResp.data.data.profile;
							if (profile.mortgages) {
								
											for (var j = 0; j < profile.mortgages.length; ++j) {
												var mortgage = {
													id: profile.mortgages[j].id,
													name: profile.mortgages[j].name,
																								
													groupId: profile.mortgages[j].groupId,
													edit: profile.mortgages[j].edit === 1,
													groupRequired: profile.mortgages[j].groupRequired === 1,
													checked: profile.mortgages[j].val === 1
												}
												mortgages.push(mortgage)
											}
									
							}
							this.setState({
									mortgagesSpecific: required,
									mortgages: mortgages,
							});
					});
			}
	}

	onMortgagesChange(event) {
		const mortgages = this.state.mortgages.slice();
		for (var i = 0; i < mortgages.length; ++i) {
				if(!mortgages[i].edit){
						continue;
				}
				if(mortgages[i].groupRequired && mortgages[i].groupId === event.target.name){
						if(mortgages[i].id !== event.target.value){
								mortgages[i].checked = false;
						}
				}
				if (mortgages[i].id === event.target.value) {
						mortgages[i].checked = event.target.checked;
				}
		}

		this.setState({
				mortgages: mortgages
		});
	}

	hideMessageBox() {
		this.setState({
				updateErrMsg: "",
		});
	}


	updateCarInfo() {
	

			const mortgagesSpecific = this.state.mortgagesSpecific;

			var mortgages = "";

			if (mortgagesSpecific) {
					for (var i = 0; i < this.state.mortgages.length; ++i) {
							const mortgage = this.state.mortgages[i];
							if (mortgage.checked === true) {
									if (mortgages === "") {
											mortgages = mortgage.id;
									} else {
											mortgages += ";" + mortgage.id;
									}
							}
					}
					
			}
	
			this.setState({
					err: commonErr.LOADING
			});
			updateCar(this.state.car.id, {
				mortgagesSpecific: mortgagesSpecific,
				mortgages: mortgages,
			}).then(resp => {
				console.log("success" + mortgages )
					this.setState({
							err: resp.data.error,
							updateErrMsg: resp.data.errorMessage,
					});
			});
	
}

	render() {
		var content;
	
		if (this.state.err === commonErr.INNIT) {
			content = <LoadingInline />
		} else if (!this.state.car || !this.state.setting) {
			content = <MessageLine message="Không tìm thấy thông tin." />
		} else { 
			content = <div className="content">
			<div className="content-container">
				<div className="form-default">
				<h3 className="title">Tài sản thế chấp</h3>
					<p className="position-relative pos-1">
							<div className="switch-on-off" style={{ right: "0", position: "absolute", bottom: "10px" }}  >
									<input className="switch-input" id="cb-mortgages-spec" type="checkbox" checked={!this.state.mortgagesSpecific} onChange={this.toggleMortgagesSpecific} />
									<label className="switch-label" htmlFor="cb-mortgages-spec"></label>
							</div>
					</p>
					<p>Thiết lập tài sản thế chấp mà khách bắt buộc phải có khi thuê xe.</p>
					{!this.state.mortgagesSpecific && <span className="note">Lưu ý: Xe đang áp dụng thiết lập chung tài sản thế chấp cho tất cả các xe. Tắt thiết lập chung nếu muốn tuỳ chỉnh riêng cho xe này</span>}
					<div className="space m"></div>
					<div className="list-features">
							<ul>
									{this.state.mortgages && this.state.mortgages.map(mortgage => <li key={mortgage.id}>                               
											{ mortgage.groupRequired ? 
											<div className="line-radio">
													<label className="custom-radio custom-control">
															<input className="custom-control-input" type="radio" id={`mortgage_${mortgage.id}`}
																	disabled={!this.state.mortgagesSpecific || !mortgage.edit}
																	name={mortgage.groupId}
																	value={mortgage.id}
														checked={mortgage.checked}
														onChange={this.onMortgagesChange.bind(this)}
																	/>
															<span className="custom-control-indicator"></span>
															<span className="custom-control-description">{mortgage.name}</span>
													</label>
											</div> : 
											<div className="squaredFour have-label">
													<input type="checkbox" id={`mortgage_${mortgage.id}`} 
															disabled={!this.state.mortgagesSpecific || !mortgage.edit} 
															name={mortgage.groupRequired ? mortgage.groupId : ""} 
															checked={mortgage.checked} 
															value={mortgage.id} 
															onChange={this.onMortgagesChange.bind(this)}
															 />
													<label htmlFor={`mortgage_${mortgage.id}`}>{mortgage.name}</label>
											</div> }
									</li>)}
							</ul>
						</div>
						<div className="line"></div>
					<a className="btn btn-primary btn--m" onClick={this.updateCarInfo}>Lưu thay đổi</a>
				</div>
				</div>
				<MessageBox show={this.state.err <= commonErr.SUCCESS && this.state.updateErrMsg !== ""} error={this.state.err} hideModal={this.hideMessageBox.bind(this)} message={this.state.errMsg} />
		</div>
		}
		return content;
	}
}

export default CarSettingMortgages