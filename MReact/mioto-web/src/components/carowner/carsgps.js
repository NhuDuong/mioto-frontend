import React from "react"
import moment from 'moment'
import ReactInterval from "react-interval"
import NumberFormat from "react-number-format"

import Header from "../common/header"

import { getOwnerCars, getCarLocationHistory } from "../../model/car"
import { commonErr } from "../common/errors"
import { LoadingInline } from "../common/loading"
import { MessagePage } from "../common/messagebox"
import { getBalance } from '../../model/wallet'

const MIN_TIMER = 10000;
const MIN_MAP_RADIUS = 2000;
const MAX_LAT_LNG_PER_TIME = 2;

function formatTimeLabel(ts) {
    if (ts <= 0) {
        return {
            label: "Không tín hiệu",
            class: "gray-dot"
        }
    }
    var deltaTime = moment().valueOf() - ts;
    if (deltaTime < 30000) {
        return {
            label: "Vừa cập nhật",
            class: "green-dot"
        }
    } else if (deltaTime < 30000 * 60){
        return {
            label: moment(ts).fromNow(),
            class: "orange-dot"
        }
    } else {
        return {
            label: moment(ts).fromNow(),
            class: "red-dot"
        }
    }
}

var CarPopup;
/** Defines the CarPopup class. */
function defineCarPopupClass() {
    /**
     * A customized popup on the map.
     * @param {!google.maps.LatLng} position
     * @param {!Element} content
     * @constructor
     * @extends {google.maps.OverlayView}
     */
    CarPopup = function (car, position, type, ts) {
        this.id = car.id;
        this.name = car.name;
        this.position = position;

        var contentBox = document.createElement('div');
        contentBox.classList.add('popup-bubble-anchor', 'car-gps');
        const content = document.createElement('div');
			content.classList.add('popup-bubble-content', 'popup-car-gps');

        const tsLabel = formatTimeLabel(ts);
        const html = '<div class="gps-box"> <div class="box-text"> <span class="license-plate">' + car.licensePlate + '</span></div></div><div class="gps-box-time"><div class="div"> <span class="time"> <span class="status ' + tsLabel.class + '"> </span>' + tsLabel.label + '</span></div></div>';
        content.innerHTML = html;
        
        contentBox.appendChild(content);

        this.anchor = document.createElement('div');
        this.anchor.classList.add('popup-tip-anchor');
        this.anchor.appendChild(contentBox);
    };

    // NOTE: google.maps.OverlayView is only defined once the Maps API has
    // loaded. That is why CarPopup is defined inside initMap().
    CarPopup.prototype = Object.create(window.google.maps.OverlayView.prototype);

    /** Called when the popup is added to the map. */
    CarPopup.prototype.onAdd = function () {
        this.getPanes().floatPane.appendChild(this.anchor);
    };

    /** Called when the popup is removed from the map. */
    CarPopup.prototype.onRemove = function () {
        if (this.anchor.parentElement) {
            this.anchor.parentElement.removeChild(this.anchor);
        }
    };

    /** Called when the popup needs to draw itself. */
    CarPopup.prototype.draw = function () {
        var divPosition = this.getProjection().fromLatLngToDivPixel(this.position);
        // Hide the popup when it is far out of view.
        var display =
            Math.abs(divPosition.x) < 4000 && Math.abs(divPosition.y) < 4000 ?
                'block' :
                'none';

        if (display === 'block') {
            this.anchor.style.left = divPosition.x - 30 + 'px';
            this.anchor.style.top = divPosition.y - 7 + 'px';
        }
        if (this.anchor.style.display !== display) {
            this.anchor.style.display = display;
        }
    };

    /** Stops clicks/drags from bubbling up to the map. */
    CarPopup.prototype.stopEventPropagation = function () {
        var anchor = this.anchor;
        anchor.style.cursor = 'auto';

        ['click', 'dblclick', 'contextmenu', 'wheel', 'mousedown', 'touchstart',
            'pointerdown']
            .forEach(function (event) {
                anchor.addEventListener(event, function (e) {
                    e.stopPropagation();
                }, { 
                    passive: true 
                });
            });
    };
}

class CarMapLocation extends React.Component {
    pMarkers = [];
    pPopups = [];
    pPopupTss = [];

    componentDidMount() {
        defineCarPopupClass();
        this.initMap(this.refs, this.props.cars, this.props.markers);
    }

    buildIcon(lat, lng, preLat, preLng) {
        const pre = new window.google.maps.LatLng(preLat, preLng);
        const cur = new window.google.maps.LatLng(lat, lng);
        var rotation = 0;
        if (preLat && preLng) {
            rotation = window.google.maps.geometry.spherical.computeHeading(pre, cur) - 90;
        }
        var scale = .5;

        return {
            path: "M63.656,43.791c-0.695-4.486-1.998-7.645-3.874-9.387c-1.422-1.271-3.185-0.922-3.22-0.913 c-0.375,0.005-37.69,0.501-40.094,0.572C10.641,34.235,7.685,39.597,7.685,50c0,10.402,2.956,15.765,8.784,15.937   c2.403,0.07,39.719,0.565,40.054,0.565c0.026,0.006,0.275,0.056,0.651,0.056c0.659,0,1.704-0.152,2.61-0.966   c1.874-1.738,3.177-4.896,3.872-9.384c0.512-3.311,0.486-6.156,0.485-6.208C64.143,49.948,64.168,47.103,63.656,43.791z    M16.195,50.755c-0.729,0-1.321-0.338-1.321-0.755s0.592-0.756,1.321-0.756s1.322,0.339,1.322,0.756S16.925,50.755,16.195,50.755z M94.822,33.357l-0.387-0.482c-2.192-2.729-3.924-4.886-9.569-4.886H64.785c-0.266,0-1.062,0.01-1.502,0.016   c0.591-1.85,0.316-3.496-0.755-4.444c-0.933-0.825-2.263-0.925-3.558-0.265c-0.292,0.148-0.242,0.663,0.113,2.63   c0.135,0.743,0.314,1.736,0.291,2.063c-3.052,0-7.566,0.033-12.343,0.066c-11.478,0.082-25.762,0.184-30.114-0.066   c-5.299-0.305-8.856,0.534-10.885,2.563C4.381,32.205,1.61,36.926,1.61,50c0,13.072,2.771,17.795,4.423,19.447   c2.029,2.028,5.589,2.868,10.885,2.562c4.353-0.25,18.637-0.148,30.114-0.067c4.776,0.035,9.291,0.067,12.343,0.067   c0.023,0.326-0.156,1.321-0.291,2.064c-0.355,1.966-0.405,2.481-0.113,2.629c0.566,0.29,1.144,0.434,1.688,0.434   c0.697,0,1.347-0.235,1.869-0.697c1.071-0.949,1.346-2.595,0.755-4.445c0.44,0.007,1.236,0.016,1.502,0.016h20.081   c5.646,0,7.377-2.156,9.569-4.886l0.387-0.481c3.584-4.425,3.766-14.64,3.766-16.643S98.406,37.782,94.822,33.357z M89.902,30.071   c0,0,1.478,0.938,2.575,2.312s1.927,2.88,1.927,2.88c-1.225,0.997-3.063,0.884-4.465-0.841   C88.537,32.697,88.678,31.066,89.902,30.071z M96.228,41.042c0.042,0.272,0.427,3.491,0.439,3.662   c-0.019-0.205-12.272-2.258-17.295-2.876c0.011,0.078-0.429-2.706-0.847-4.127C78.525,37.701,90.791,39.231,96.228,41.042z    M75.119,66.329c-1.944,3.381-5.636,3.466-8.604,3.532l-0.225,0.005c-0.4,0.01-0.965,0.014-1.663,0.014   c-4.574,0-14.829-0.191-21.433-0.442c-7.387-0.281-24.802-0.791-28.287-0.714c-3.361,0.07-5.387,0.111-7.834-3.818   C4.681,61.06,3.897,52.513,3.897,50s0.783-11.06,3.177-14.906c2.447-3.93,4.477-3.887,7.834-3.817   c3.48,0.072,20.9-0.434,28.287-0.715c7.609-0.289,20.064-0.499,23.096-0.428l0.225,0.004c2.968,0.067,6.659,0.15,8.604,3.533   c2.12,3.692,3.387,9.797,3.387,16.329C78.506,56.531,77.239,62.636,75.119,66.329z M92.478,67.616   c-1.098,1.374-2.575,2.312-2.575,2.312c-1.225-0.996-1.365-2.625,0.037-4.351c1.401-1.727,3.24-1.838,4.465-0.843   C94.404,64.734,93.575,66.241,92.478,67.616z M96.228,58.956c-5.437,1.812-17.702,3.342-17.702,3.342   c0.418-1.42,0.857-4.205,0.847-4.127c5.022-0.619,17.276-2.67,17.295-2.875C96.654,55.466,96.27,58.686,96.228,58.956z",
            fillColor: '#000',
            fillOpacity: 1,
            strokeWeight: 0,
            scale: scale,
            anchor: new window.google.maps.Point(0, 39),
            rotation: rotation
        }
    }

    componentWillUpdate(props) {
        const oMarkers = this.props.markers;
        const { cars, markers, isUpdateBounds } = props;

        for (var i = 0; i < cars.length; ++i) {
            const carId = cars[i].id;
            const marker = markers[carId];
            const oMarker = oMarkers[carId];

            if (oMarker && (marker.lat !== oMarker.lat || marker.lng !== oMarker.lng || marker.ts !== oMarker.ts)) {
                const pMarker = this.pMarkers[carId];
                pMarker.setMap(null);
                const pPopup = this.pPopups[carId];
                pPopup.setMap(null);
                // const pPopupTs = this.pPopupTss[carId];
                // pPopupTs.setMap(null);

                var newPMarker = new window.google.maps.Marker({
                    position: new window.google.maps.LatLng(marker.lat, marker.lng),
                    icon: this.buildIcon(marker.lat, marker.lng, marker.preLat, marker.preLng),
                    map: this.map,
                });

                const newPPopup = new CarPopup(marker.car, new window.google.maps.LatLng(marker.lat, marker.lng), 1, marker.ts);
                newPPopup.setMap(this.map);

                // const newPPopupTs = new CarPopup(marker.car, new window.google.maps.LatLng(marker.lat, marker.lng), 2, marker.ts);
                // newPPopupTs.setMap(this.map);

                this.pMarkers[carId] = newPMarker;
                this.pPopups[carId] = newPPopup;
                // this.pPopupTss[carId] = newPPopupTs;
            }
        }

        if (isUpdateBounds) {
            this.fitBounds(this.map, cars, markers);
        }
    }

    fitBounds(map, cars, markers) {
        var bounds = new window.google.maps.LatLngBounds();

        for (var i = 0; i < cars.length; ++i) {
            const marker = markers[cars[i].id];
            bounds.extend(new window.google.maps.LatLng(marker.lat, marker.lng));
        }

        const ne = bounds.getNorthEast();
        const center = bounds.getCenter();
        const mapRadius = window.google.maps.geometry.spherical.computeDistanceBetween(ne, center);

        if (mapRadius < MIN_MAP_RADIUS) {
            const circle = new window.google.maps.Circle({
                center: center,
                radius: MIN_MAP_RADIUS
            });
            map.fitBounds(circle.getBounds());

        } else {
            map.fitBounds(bounds);
        }
    }

    initMap(refs, cars, markers) {
        const map = this.map = new window.google.maps.Map(refs.map, {
            mapTypeControl: false,
            scrollwheel: false,
            clickableIcons: false,
            streetViewControl: false,
            zoomControl: true,
            zoomControlOptions: {
                style: window.google.maps.ZoomControlStyle.SMALL
            },
        })

        for (var i = 0; i < cars.length; ++i) {
            const marker = markers[cars[i].id];

            var pMarker = new window.google.maps.Marker({
                position: new window.google.maps.LatLng(marker.lat, marker.lng),
                icon: this.buildIcon(marker.lat, marker.lng),
                map: map
            });

            const pPopup = new CarPopup(marker.car, new window.google.maps.LatLng(marker.lat, marker.lng), 1, marker.ts);
            pPopup.setMap(map);

            // const pPopupTs = new CarPopup(marker.car, new window.google.maps.LatLng(marker.lat, marker.lng), 2, marker.ts);
            // pPopupTs.setMap(map);

            this.pMarkers[marker.car.id] = pMarker;
            this.pPopups[marker.car.id] = pPopup;
            // this.pPopupTss[marker.car.id] = pPopupTs;
        }       

        this.fitBounds(map, cars, markers);
    }

    render() {
        return <div ref="map" className="map-container" />
    }
}

export default class CarsGPS extends React.Component {
    constructor() {
        super();
        this.state = {
            err: commonErr.INNIT,
            isUpdateBounds: true
        }
    }

    getCarsLocation(cars, isUpdateBounds) {
        const markers = [];
        var counter = 0;
        for (var i = 0; i < cars.length; ++i) {
            const car = cars[i];
            getCarLocationHistory(car.id, MAX_LAT_LNG_PER_TIME).then(resp => {
                if (resp.data.error >= commonErr.SUCCESS) {
                    if (resp.data.data && resp.data.data.loc) {
                        const { lat, lon, ts } = resp.data.data.loc;
                        markers[car.id] = {
                            car: car,
                            ts: ts[0] * 1000,
                            lat: lat[lat.length - 1],
                            lng: lon[lon.length - 1],
                            preLat: lat[lat.length - 2],
                            preLng: lon[lon.length - 2]
                        }
                    } else {
                        markers[car.id] = {
                            car: car,
                            ts: 0,
                            lat: car.location.lat,
                            lng: car.location.lon
                        }
                    }

                    ++counter;

                    if (counter === cars.length) {
                        this.setState({
                            isUpdateBounds: isUpdateBounds,
                            markers: markers
                        })
                    }
                }
            });
        }
    }

    componentDidMount() {
        getOwnerCars(0, 0, 0, 0, true).then(resp => {
            if (resp.data.data) {
                const cars = resp.data.data.cars.filter(car => car.location != null);
                const markers = [];
                if (cars !== null) {
                    for (var i = 0; i < cars.length; ++i) {
                        markers[cars[i].id] = {
                            car: cars[i],
                            lat: cars[i].location.lat,
                            lng: cars[i].location.lon,
                            ts: 0
                        };
                    }

                    this.setState({
                        err: commonErr.SUCCESS,
                        cars: cars,
                        markers: markers
                    })
                    this.getCarsLocation(cars, true /*update bounds of map at first time*/);
                } else {
                    this.setState({
                        err: commonErr.FAIL,
                    })
                }
            } else {
                this.setState({
                    err: commonErr.FAIL,
                })
            }
        })
        getBalance().then((resp) => {
			if (resp.data.error === commonErr.SUCCESS) {
				this.setState({
					balance: resp.data.data.balance
				})
			} else {
				this.setState({
					err: resp.data.error,
				})
			}
		})
    }

    render() {
        var content;
        if (this.state.err === commonErr.INNIT) {
            content = <LoadingInline />
        } if (this.state.err <= commonErr.FAIL) {
            content = <MessagePage message="Không tìm thấy thông tin" />
        } else {
            const { cars, markers, isUpdateBounds } = this.state;
            var bodyContent = <div className="tracking_cars--wrap">
                {markers && <CarMapLocation cars={cars} markers={markers} isUpdateBounds={isUpdateBounds} />}
                <ReactInterval timeout={MIN_TIMER} enabled={true} callback={() => this.getCarsLocation(cars, false /*DO NO update bounds of map when auto*/)} />
            </div>

            content = <div className="content">
                {bodyContent}
            </div>
        }

        return <div className="mioto-layout bg-gray">
            <Header />
            <section className="body">
                <div className="sidebar-control z-2">
                    <div className="sidebar-settings general-settings">
                        <ul>
                            <li> <a href="/mycars"><i className="ic ic-cars"></i><span className="label">Danh sách xe</span></a></li>
                            <li> <a href="/calendars"><i className="ic ic-setting-calendar"></i><span className="label">Lịch chung</span></a></li>
                            <li> <a className="active" href="/gps"><i className="ic ic-gps"></i><span className="label">GPS</span><sup class="new-func">BETA</sup></a></li>
                            <li> <a href="/commonsetting"><i className="ic ic-setting-rent"> </i><span className="label">Cài đặt chung</span></a></li>
                            <li> <a href="/carregister"><i className="ic ic-plus-black"></i><span className="label">Đăng ký xe </span></a></li>
                            <li> <a href="/mywallet"><i className="ic ic-owner-wallet"></i><span className="label">Số dư: <span className="fontWeight-6 font-16"><NumberFormat value={this.state.balance} displayType={'text'} thousandSeparator={" "} decimalSeparator={","} suffix={"đ"} /></span></span></a></li>
                        </ul>
                    </div>
                </div>
                {content}
            </section>
        </div>
    }
}