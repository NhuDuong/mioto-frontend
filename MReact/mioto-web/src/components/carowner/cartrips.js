import React from "react"
import { connect } from "react-redux"
import moment from 'moment'
import { Link } from 'react-router-dom'

import Header from "../common/header"
import Footer from "../common/footer"
import { LoadingPage } from "../common/loading"
import { commonErr } from "../common/errors"
import { getLoggedProfile } from "../../actions/sessionAct"
import { getCarTrips } from "../../model/car"
import { MessageLine } from "../common/messagebox"

class TripItem extends React.Component {
    render() {
        const car = this.props.car;
        const trip = this.props.trip;
        const renter = this.props.renter;

        var status;
        if (trip.status >= 25) {
            status = <p><span className="status refuse"> </span>Đã kết thúc</p>
        } else if (trip.status >= 20) {
            status = <p><span className="status cancel"> </span>Đã huỷ</p>
        } else if (trip.status >= 5) {
            status = <p><span className="status approved"></span>Đang thuê</p>
        } else if (trip.status >= 4) {
            status = <p><span className="status approved"></span>Đã cọc</p>
        } else if (trip.status >= 2) {
            status = <p><span className="status deposit"> </span>Đã duyệt</p>
        } else {
            status = <p><span className="status completed"></span>Chờ duyệt</p>
        }

        return <div className="trip-box">
            <div className="trip-header">
                <div className="left">
                    <div className="car-img">
                        <Link to={{ pathname: `/trip/detail/${trip.id}` }}>
                            <div className="fix-img"> <img src={`${car.photos ? car.photos[0].thumbUrl : ""}`} alt="Mioto - Thuê xe tự lái" /></div>
                        </Link>
                    </div>
                </div>
                <div className="right">
                    <h4>{car.name}</h4>
                </div>
            </div>
            <div className="space m"></div>
            <div className="trip-body">
                <div className="left">
                    <p className="total-price">
                        <i className="ic ic-total-price"></i>Tổng tiền {formatPrice(trip.priceSummary.priceTotal)}</p>
                    <p> Bắt đầu: {moment(trip.tripDateFrom).format("HH:mm dddd, DD/MM/YYYY")}</p>
                    <p> Kết thúc: {moment(trip.tripDateTo).format("HH:mm dddd, DD/MM/YYYY")}</p>
                </div>
                <div className="right">
                    <div className="avatar">
                        <div className="avatar-img">
                            <Link to={`/profile/${renter.uid}`}>
                                <img src={`${(renter.avatar && renter.avatar.thumbUrl) ? renter.avatar.thumbUrl : avatar_default}`} />
                            </Link>
                        </div>
                    </div>
                    <div className="lstitle">{renter.name}</div>
                </div>
            </div>
            <div className="trip-footer">
                <div className="status-trips">
                    {status}
                </div>
                <div className="time">
                    <p>{moment(trip.timeBooked).fromNow()}</p>
                </div>
            </div>
        </div>
    }
}

class CarTrips extends React.Component {
    constructor() {
        super();
        this.state = {
            err: commonErr.INNIT,
            filterStatus: 0,
            trips: [],
            cars: [],
            profiles: []
        }
    }

    getTrips(pos) {
        const self = this;
        getCarTrips(this.state.filterStatus, 0, pos, 0).then(function (resp) {
            if (resp.data.error >= commonErr.SUCCESS && resp.data.data.trips) {
                self.setState({
                    trips: resp.data.data.trips,
                    cars: resp.data.data.cars,
                    profiles: resp.data.data.profiles,
                    more: resp.data.data.more,
                    err: resp.data.error
                });
            } else {
                self.setState({
                    err: resp.data.error
                });
            }
        });
    }

    componentDidMount() {
        window.scrollTo(0, 0);
        //
        const self = this;
        self.setState({
            err: commonErr.LOADING
        });
        this.getTrips(0);
    }

    getTripsMore() {
        const self = this;
        const nextPos = this.state.trips.length + 10;
        getCarTrips(this.state.filterStatus, 0, nextPos, 0).then(function (resp) {
            if (resp.data.error >= commonErr.SUCCESS && resp.data.data.trips) {
                self.setState({
                    trips: self.state.trips.concat(resp.data.data.trips),
                    cars: self.state.cars.concat(resp.data.data.cars),
                    profiles: self.state.profiles.concat(resp.data.data.profiles),
                    more: resp.data.data.more,
                    err: resp.data.error
                });
            } else {
                self.setState({
                    err: resp.data.error
                });
            }
        });
    }

    onStatusFilterChange(event) {
        this.setState({
            filterStatus: event.target.value
        });
    }

    onStatusFilterReset() {
        this.setState({
            filterStatus: 0
        });
    }

    render() {
        var content;
        if (this.state.err === commonErr.LOADING) {
            content = <LoadingPage />
        } else if (this.state.err >= commonErr.SUCCESS && this.state.trips && this.state.cars && this.state.profiles) {
            var validTrips = this.state.trips.filter(function (trip) {
                // return (trip.status === 1 || trip.status === 3 || trip.status === 4);
                return true;
            });
            if (validTrips.length > 0) {
                var subContent = validTrips.map(trip => {
                    var car;
                    this.state.cars.map(carInfo => {
                        if (carInfo.id === trip.carId) {
                            car = carInfo;
                        }
                    });
                    var renter;
                    this.state.profiles.map(profile => {
                        if (profile.uid === trip.travelerId) {
                            renter = profile;
                        }
                    });
                    if (car && renter) {
                        return <TripItem trip={trip} car={car} renter={renter} />
                    }
                });
                content = <InfiniteScroll pageStart={0}
                    loadMore={this.getTripsMore.bind(this)}
                    hasMore={this.state.more === 1}
                    loader={<ul><LoadingInline /></ul>}>
                    {subContent}
                </InfiniteScroll>

            } else {
                content = <MessagePage message={"Không tìm thấy chuyến nào."} />
            }
        } else {
            content = <MessagePage message={"Không tìm thấy chuyến nào."} />
        }

        return <div className="mioto-layout bg-gray">
            <Header />
            <section className="body has-filter">
                <div className="module-map module-trips">{content}
                    <div className="space m"></div>
                    <div className="space m"></div>
                    <div className="space m"></div>
                </div>
            </section>
            <Footer />
        </div>
    }
}

export default CarTrips;