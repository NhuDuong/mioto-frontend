import React from "react"
import { connect } from "react-redux"
import NumberFormat from "react-number-format"

import Header from "../common/header"
import Footer from "../common/footer"
import { commonErr } from "../common/errors"
import { MessagePage, MessageBox } from "../common/messagebox"
import { LoadingPage } from "../common/loading"
import { getLoggedProfile, updateConfigPapers} from "../../model/profile"
import { getBalance } from '../../model/wallet'


class CommonSetting extends React.Component {
    constructor() {
        super();

        this.state = {
            err: commonErr.INNIT,
            errMsg: ""
			}
				this.onPolicyChange = this.onPolicyChange.bind(this)
    }

    componentDidMount() {
        window.scrollTo(0, 0);
        //
        const self = this;
        getLoggedProfile().then(function (resp) {
					if (resp.data.data) {
						const profile = resp.data.data.profile;
						const papers = [];
						// const papersOther = profile.requiredCarPapersOther || "";
						
						const mortgages = [];
						const policies = profile.policies

						if (profile.requiredCarPapers) {
							for (var j = 0; j < profile.requiredCarPapers.length; ++j) {
								var requiredPaper = {
									id: profile.requiredCarPapers[j].id,
									name: profile.requiredCarPapers[j].name,
									logo: profile.requiredCarPapers[j].logo,
									groupId: profile.requiredCarPapers[j].groupId,
									edit: profile.requiredCarPapers[j].edit === 1,
									groupRequired: profile.requiredCarPapers[j].groupRequired === 1,
									checked: profile.requiredCarPapers[j].val === 1
								}
								papers.push(requiredPaper)
							}
                    
						}
						if (profile.mortgages) {
							for (var i = 0; i < profile.mortgages.length; ++i) {
								var mortgage = {
									id: profile.mortgages[i].id, 
									name: profile.mortgages[i].name, 
									groupId: profile.mortgages[i].groupId, 
									edit: profile.mortgages[i].edit === 1, 
									groupRequired: profile.mortgages[i].groupRequired === 1, 
									checked: profile.mortgages[i].val === 1
								}
								mortgages.push(mortgage)
							}
							
						}
                self.setState({
                  papers: papers,
									// papersOther: papersOther, 
									mortgages: mortgages,
									policies: policies,
                });
            } else {
                self.setState({
                    err: resp.data.error,
                    errMsg: resp.data.errorMessage
                });
            }
        });
        getBalance().then(function (resp){
					if (resp.data.error === commonErr.SUCCESS) {
						self.setState({
							balance: resp.data.data.balance
						})
					} else {
						self.setState({
							err: resp.data.error,
						})
					}
				})
    }

    onPapersChange(event) {
        const papers = this.state.papers.slice();
        for (var i = 0; i < papers.length; ++i) {
            if(!papers[i].edit){
                continue;
            }
            if(papers[i].groupRequired && papers[i].groupId === event.target.name){
                if(papers[i].id !== event.target.value){
                    papers[i].checked = false;
                }
            }
            if (papers[i].id === event.target.value) {
                papers[i].checked = event.target.checked;
            }
        } 
        this.setState({
            papers: papers
        });
    }

    // onPapersOtherChange(event) {
    //     this.setState({
    //         papersOther: event.target.value
    //     });
    // }

		onMortgagesChange(event) {
			const mortgages = this.state.mortgages.slice();
			for (var i = 0; i < mortgages.length; ++i) {
					if(!mortgages[i].edit){
							continue;
					}
					if(mortgages[i].groupRequired && mortgages[i].groupId === event.target.name){
							if(mortgages[i].id !== event.target.value){
									mortgages[i].checked = false;
							}
					}
					if (mortgages[i].id === event.target.value) {
							mortgages[i].checked = event.target.checked;
					}
			} 
			this.setState({
					mortgages: mortgages
			});
	}
		onPolicyChange(event) {
				this.setState({
						policies: event.target.value
				});
		}
		
    update() {
        var papers = "";
        for (var i = 0; i < this.state.papers.length; ++i) {
            const paper = this.state.papers[i];
            if (paper.checked === true) {
                if (papers === "") {
                    papers = paper.id;
                } else {
                    papers += ";" + paper.id;
                }
            }
        }

				// const papersOther = encodeURI(this.state.papersOther);
				
				
				var mortgages = "";
        for (var i = 0; i < this.state.mortgages.length; ++i) {
            const mortgage = this.state.mortgages[i];
            if (mortgage.checked === true) {
                if (mortgages === "") {
                    mortgages = mortgage.id;
                } else {
                    mortgages += ";" + mortgage.id;
                }
            }
				}
				
				const policies = encodeURI(this.state.policies);
			
        updateConfigPapers({requiredCarPapers: papers, mortgages: mortgages, policies: policies}).then(resp => {
            this.setState({
                err: resp.data.error,
                errMsg: resp.data.errorMessage
            });
        });
    }

    hideMessageBox() {
        this.setState({
            err: commonErr.INNIT,
            errMsg: ""
        });
    }

    render() {
        var content;

        const profile = this.props.session.profile.info;
        const papers = this.state.papers;
        // const papersOther = this.state.papersOther;

			
        if (this.props.session.profile.err === commonErr.LOADING) {
            content = <LoadingPage />
        } else if (profile && profile.uid) {
            content = <div className="module-register">
                <div className="register-container">
                    <div className="content-register">
                        <MessageBox show={this.state.errMsg !== ""} hideModal={this.hideMessageBox.bind(this)} error={this.state.err} message={this.state.errMsg} />
                        <div className="group form-default">
														<h6>Giấy tờ thuê xe</h6>
														<p>Thiết lập các giấy tờ khách bắt buộc phải có (bản gốc) khi thuê xe của bạn.</p>
														<div className="space"></div>
                            <div className="list-features">
                                <ul>
                                    {papers && papers.map(paper => <li key={paper.id}>
                                        { paper.groupRequired ?
                                            <label className="custom-radio custom-control">
                                                <input disabled={!paper.edit}
                                                    className="custom-control-input" 
                                                    type="radio"
                                                    name={paper.groupId}
                                                    value={paper.id}
                                                    checked={paper.checked}
                                                    onChange={this.onPapersChange.bind(this)} />
                                                <span className="custom-control-indicator"></span>
                                                <span className="custom-control-description">{paper.name}</span>
                                            </label> :
                                            <div className="squaredFour have-label">
                                                <input disabled={paper.edit} id={`paper_${paper.id}`} type="checkbox" checked={paper.checked} value={paper.id} onClick={this.onPapersChange.bind(this)} />
                                                <label htmlFor={`paper_${paper.id}`}> <img style={{ width: "20px", height: "20px", display: "inline" }} src={paper.logo} alt="Mioto - Thuê xe tự lái" /> {paper.name}</label>
                                            </div>}
                                    </li>)}
                                </ul>
                            </div>
                            {/* <h6>Giấy tờ đặc biệt khác</h6>
                            <textarea className="textarea" value={papersOther} onChange={this.onPapersOtherChange.bind(this)}></textarea>
                            <p><span className="note">Liệt kê các loại giấy tờ đặc biệt khách thuê phải cung cấp khi thuê xe.</span></p> */}
														<div className="line"></div>
														<div className="space"></div>
														<h6>Tài sản thế chấp khi thuê xe</h6>
														<p>Thiết lập các tài sản thế chấp khách phải có khi thuê xe của bạn</p>
														<div className="space"></div>
														<div className="list-features">
															<ul>
																	{this.state.mortgages && this.state.mortgages.map(mortgage => <li key={mortgage.id}>                               
																			{ mortgage.groupRequired ? 
																			<div className="line-radio">
																					<label className="custom-radio custom-control">
																							<input className="custom-control-input" type="radio" id={`mortgage_${mortgage.id}`}
																								
																									name={mortgage.groupId}
																									value={mortgage.id}
																									checked={mortgage.checked}
																									onChange={this.onMortgagesChange.bind(this)}
																									/>
																							<span className="custom-control-indicator"></span>
																							<span className="custom-control-description">{mortgage.name}</span>
																					</label>
																			</div> : 
																			<div className="squaredFour have-label">
																					<input type="checkbox" id={`mortgage_${mortgage.id}`} 
																							name={mortgage.groupRequired ? mortgage.groupId : ""} 
																							checked={mortgage.checked} 
																							value={mortgage.id} 
																							onChange={this.onMortgagesChange.bind(this)}
																							/>
																					<label htmlFor={`mortgage_${mortgage.id}`}>{mortgage.name}</label>
																			</div> }
																	</li>)}
															</ul>
												</div>
														<div className="line"></div>
														<div className="space"></div>
														<h6>Điều khoản chung khi thuê xe</h6>
														<p>Ghi rõ các yêu cầu thuê xe</p> 
													
														<div className="form-default">
																<div className="line-form end">
																		<textarea className="textarea" onChange={this.onPolicyChange} value={this.state.policies}></textarea>
																</div>
														</div>
														<div className="space"></div>
                            <a className="btn btn-primary btn--m" onClick={this.update.bind(this)}>Lưu thay đổi</a>
                        </div>
                      
                    </div>
                </div>
            </div>
        } else {
            content = <MessagePage message="Vui lòng đăng nhập để sử dụng tính năng này." />
        }

        return <div className="mioto-layout">
            <Header />
            <section className="body">
                <div className="sidebar-control z-2">
                    <div className="sidebar-settings general-settings">
                        <ul>
                            <li> <a href="/mycars"><i className="ic ic-cars"></i><span className="label">Danh sách xe</span></a></li>
                            <li> <a href="/calendars"><i className="ic ic-setting-calendar"></i><span className="label">Lịch chung</span></a></li>
                            <li> <a href="/gps"><i className="ic ic-gps"></i><span className="label">GPS</span><sup class="new-func">BETA</sup></a></li>
                            <li> <a className="active" href="/commonsetting"><i className="ic ic-setting-rent"> </i><span className="label">Cài đặt chung</span></a></li>
                            <li> <a href="/carregister"><i className="ic ic-plus-black"></i><span className="label">Đăng ký xe </span></a></li>
                            <li> <a href="/mywallet"><i className="ic ic-owner-wallet"></i><span className="label">Số dư: <span className="fontWeight-6 font-16"><NumberFormat value={this.state.balance} displayType={'text'} thousandSeparator={" "} decimalSeparator={","} suffix={"đ"} /></span></span></a></li>
                        </ul>
                    </div>
                </div>
                {content}
            </section>
            <Footer />
        </div>
    }
}

function mapState(state) {
    return {
        session: state.session
    }
}

CommonSetting = connect(mapState)(CommonSetting)

export default CommonSetting;