import React, { Component } from 'react'; 
import Header from "../common/header"; 
import NumberFormat from "react-number-format"
import { Link } from "react-router-dom"
import moment from 'moment'
import { commonErr } from "../common/errors"
import { MessageBox } from "../common/messagebox"
import { LoadingPage, LoadingInline } from "../common/loading"
import { StickyContainer, Sticky } from 'react-sticky';
import { getBalance, getInvoiceByMonth, withdrawMoney } from '../../model/wallet'
import { Modal } from "react-bootstrap"

class WalletItem extends React.Component {
	constructor(props) {
				super(props);
		this.getTotalTrasactionAmount = this.getTotalTrasactionAmount.bind(this);
	}

	getTotalTrasactionAmount(listTransaction){
		var total = 0;
		var i;
		if(listTransaction && listTransaction.length > 0) {
			for (i = 0; i < listTransaction.length; ++i) {
				var transaction = listTransaction[i];
				total += transaction.amount;
			}
		}
		return total;
	}

	render() {
		const data = this.props.data;
		var rating = parseFloat(data.rating.avg).toFixed(1) ;
		// var n = parseFloat(x); x = Math.round(n * 1000) / 1000; alert(x)
		return <div>
			<div className="info-display">
				<div className="info-display--item"> 
					<div className="group-ratings"><i className="ic ic-star"></i><span className="highlight">{rating}</span></div>
					<p className="desc">Đánh giá</p>
				</div>
				<div className="info-display--item"> <span className="highlight">{data.totalTripFinished} </span>
					<p className="desc">Chuyến đi thành công</p>
				</div>
				<div className="info-display--item d-flex">
					<div className="response-desc"> <span className="highlight">{data.responseRate} </span>
						<p className="desc">Tỉ lệ phản hồi </p>
					</div>
					<div className="response-desc"> <span className="highlight">{data.responseTime} </span>
						<p className="desc">Thời gian phản hồi  </p>
					</div>
					<div className="response-desc"> <span className="highlight">{data.acceptRate} </span>
						<p className="desc">Tỉ lệ đồng ý</p>
					</div>
				</div>
			</div>
			<div className="transaction-detail__wrap">
				<div className="transaction-detail__table">
					<div className="row-transaction main-row">												
						<div className="desc">
							<p>Mã chuyến đi</p>
						</div>
						<div className="desc">
							<p>Ngày đi</p>
						</div>
						<div className="desc">
							<p>Ngày về</p>
						</div>
						<div className="desc">
							<p>Đơn giá</p>
						</div>
						<div className="desc">
							<p>Khuyến mãi</p>
						</div>
						<div className="desc">
							<p>Đặt cọc tại Mioto</p>
						</div>
						<div className="desc">
							<p>Phí Vận hành</p>
						</div>
						<div className="desc">
							<p>Thay đổi số dư</p>
						</div>
					</div>												
					{ data.transactionsFinished && data.transactionsFinished.length > 0 && data.transactionsFinished.map(transaction =>
					<div className="row-transaction">
						<div className="desc"> 
							<p><a href={`/trip/detail/${transaction.trip.id}`} target="_blank">{transaction.trip.id}</a></p>
						</div>
						<div className="desc"> 
							<p>{moment(transaction.trip.tripDateFrom).format("DD/MM/YYYY")}</p>
						</div>
						<div className="desc"> 
							<p>{moment(transaction.trip.tripDateTo).format("DD/MM/YYYY")}</p>
						</div>
						<div className="desc"> 
							<p><NumberFormat value={transaction.trip.price} displayType={'text'} thousandSeparator={" "} decimalSeparator={","} suffix={"đ"} /></p>
						</div>
						<div className="desc"> 
							<p><NumberFormat value={transaction.trip.promotion} displayType={'text'} thousandSeparator={" "} decimalSeparator={","} suffix={"đ"} /></p>
						</div>
						<div className="desc"> 
							<p><NumberFormat value={transaction.trip.deposit} displayType={'text'} thousandSeparator={" "} decimalSeparator={","} suffix={"đ"} /></p>
						</div>
						<div className="desc"> 
							<p><NumberFormat value={transaction.trip.fee} displayType={'text'} thousandSeparator={" "} decimalSeparator={","} suffix={"đ"} /></p>
						</div>
						<div className="desc"> 
							<p><NumberFormat value={transaction.amount} displayType={'text'} thousandSeparator={" "} decimalSeparator={","} suffix={"đ"} /></p>
						</div> 														
					</div>) }
			</div>
			<div className="summary-change">
				<p>Tổng thay đổi - Chuyến đi hoàn thành</p>
				<p><NumberFormat value={this.getTotalTrasactionAmount(data.transactionsFinished)} displayType={'text'} thousandSeparator={" "} decimalSeparator={","} suffix={"đ"} /></p>
			</div>
			<div className="summary-change">
				<p>Tổng thay đổi - Giao dịch rút/nộp tiền</p>
				<p><NumberFormat value={this.getTotalTrasactionAmount(data.transactionsOther)} displayType={'text'} thousandSeparator={" "} decimalSeparator={","} suffix={"đ"} /></p>
			</div>
			<div className="summary-change">
				<p>Tổng thay đổi - Giao dịch hủy chuyến</p>
				<p><NumberFormat value={this.getTotalTrasactionAmount(data.transactionsCanceled)} displayType={'text'} thousandSeparator={" "} decimalSeparator={","} suffix={"đ"} /></p>
			</div>
			<div className="group-summary">
					<div className="transaction-summary">
						<p>TỔNG CỘNG THAY ĐỔI TRONG KÌ</p>
						<p><NumberFormat value={data.endingBalance - data.openingBalance} displayType={'text'} thousandSeparator={" "} decimalSeparator={","} suffix={"đ"} /></p>
					</div>
					<div className="transaction-summary">
						<p>TIỀN ĐẦU KÌ</p>
						<p><NumberFormat value={data.openingBalance} displayType={'text'} thousandSeparator={" "} decimalSeparator={","} suffix={"đ"} /></p>
					</div>
					<div className="transaction-summary">
						<p>TIỀN CUỐI KÌ</p>
						<p className="sum-price"><NumberFormat value={data.endingBalance} displayType={'text'} thousandSeparator={" "} decimalSeparator={","} suffix={"đ"} /></p>
					</div>
				</div>
			</div>
		</div>
	}
}

class MyWallet extends Component {
	constructor(props) {
		super(props);
		this.state = { 
			err: commonErr.INNIT,
			errMsg: "",
			monthInMilliseconds: 0,
			showWithdrawRequest: false,
			amount: 0,
			accountNumber: "",
			accountName: "",
			bankName: "",
			bankProvince: "",
			bankBranch: "",			
			alertMsg: "",
		}
		
		this.initMonthData = this.initMonthData.bind(this);
		this.handleInputChange = this.handleInputChange.bind(this);
		this.close = this.close.bind(this);
	}

	componentDidMount() {
		this.refreshCheckFilled();
		this.getWalletInfo(0);
	}

	initMonthData(){
		var monthData = [];
		for (var m = moment().startOf('month'); m.isAfter(moment("01/01/2019")); m.subtract(1, 'month')){
			monthData.push({monthInMilliseconds: m.valueOf(), monthInText: m.format("DD-MM-YYYY")});
		}
		monthData.push({monthInMilliseconds: moment("01/01/2019").valueOf(), monthInText: moment("01/01/2019").format("DD-MM-YYYY")});
		return monthData;
	}

	getWalletInfo(monthInMilliseconds){
		var month = monthInMilliseconds > 0 ? monthInMilliseconds : moment().startOf('month').valueOf();
		const self = this;
		getBalance().then(function (resp){
			if (resp.data.error === commonErr.SUCCESS) {
				self.setState({
					balance: resp.data.data.balance,
					amount: resp.data.data.balance,
				})
			} else {
				self.setState({
					err: resp.data.error,
				})
			}
		})
		getInvoiceByMonth(month).then(function (resp) {
			if (resp.data.error === commonErr.SUCCESS){
				self.setState({
					monthInMilliseconds: month,
					data: resp.data.data,
					timeRequested: resp.data.data.timeRequested,
					accountNumber: resp.data.data.accountNumber && resp.data.data.accountNumber,
					accountName: resp.data.data.accountName && resp.data.data.accountName,
					bankName: resp.data.data.bankName && resp.data.data.bankName,
					bankProvince: resp.data.data.bankProvince && resp.data.data.bankProvince,
					bankBranch: resp.data.data.bankBranch && resp.data.data.bankBranch,
				})
			} else {
				self.setState({
					err: resp.data.error,
					data: null
				})
			}
		})
	}

	onSelectMonthChange(event){
		var monthInMilliseconds = event.target.value;
		this.setState({
			monthInMilliseconds: monthInMilliseconds
		})
		this.getWalletInfo(monthInMilliseconds);
	}

	openWithdrawRequest() {
		this.setState({
			showWithdrawRequest: true
		});
	}

	confirmCancelWithdrawRequest() {
		this.setState({
			showConfirmCancelWithdraw: true,
		})
	}

	cancelWithdrawRequest() {
		this.setState({ err: commonErr.LOADING });
		withdrawMoney(true, undefined).then(resp => {
			this.setState({
				err: resp.data.error,
				errMsg: resp.data.errorMessage,
				showConfirmCancelWithdraw: false
			})
		}) 
		this.getWalletInfo(0);
	}

	closeWithdrawRequest() {
		this.setState({
			showWithdrawRequest: false
		});
	}

	handleInputChange(event) {
		this.setState({
			[event.target.name]: event.target.value
		}, () => this.isValidInfo());
	}

	amountChangedHandler = values => {
		this.setState({ amount: values.value }, () => this.isValidInfo());
	}
	
	refreshCheckFilled(){
		this.setState({
				isAccountNameValid: true,
				isAccountNumberValid: true,
				isAmountValid: true,
				isBankBranchValid: true,
				isBankNameValid: true,
				isBankProvinceValid: true,
				alertMsg: "",
		})
	}
	
	isValidInfo(){
		this.refreshCheckFilled();
		if (this.state.amount <= 0 || !this.state.amount) {
			this.setState({
				isAmountValid: false,
				alertMsg: "Bạn chưa điền số tiền cần rút"
			})
			return "Bạn chưa điền số tiền cần rút"
		}

		if (this.state.amount	> this.state.balance) {
			this.setState({
				isAmountValid: false,
				alertMsg: "Số tiền cần rút vượt quá số dư hiện có"
			})
			return "Số tiền cần rút vượt quá số dư hiện có"
		}

		if (this.state.accountNumber === "" || !this.state.accountNumber){
						this.setState({
							isAccountNumberValid: false,
							alertMsg: "Bạn chưa điền số tài khoản"
						})
						return "Bạn chưa điền số tài khoản"
				}

		if (this.state.accountName === "" || !this.state.accountName){
				this.setState({
					isAccountNameValid: false,
					alertMsg: "Bạn chưa điền tên người thụ hưởng"
				})
				return "Bạn chưa điền tên người thụ hưởng"
		}
		
		if (this.state.bankName === "" || !this.state.bankName){
			this.setState({
				isBankNameValid: false,
				alertMsg: "Bạn chưa điền tên ngân hàng thụ hưởng"
			})
			return "Bạn chưa điền tên ngân hàng thụ hưởng"
		}

		if (this.state.bankProvince === "" || !this.state.bankProvince){
			this.setState({
				isBankProvinceValid: false,
				alertMsg: "Bạn chưa điền Tỉnh/Thành phố"
			})
			return "Bạn chưa điền Tỉnh/Thành phố"
		}

		if (this.state.bankBranch === "" || !this.state.bankBranch){
			this.setState({
				isBankBranchValid: false,
				alertMsg: "Bạn chưa điền tên chi nhánh"
			})
			return "Bạn chưa điền tên chi nhánh"
		}
		return "";
	
	}
	

	
	onRequestWithdrawClick(){
		const message = this.isValidInfo();
		if (message !== "") {
						this.setState({
								err: commonErr.FAIL,
								errMsg: message
						})
				} else {
			const data = {
							amount: this.state.amount,
							accountName: this.state.accountName,
							accountNumber: this.state.accountNumber,
							bankName: this.state.bankName,
							bankProvince: this.state.bankProvince,
							bankBranch: this.state.bankBranch,
						}            
						var self = this;
						this.setState({ err: commonErr.LOADING });
						withdrawMoney(false, data).then(resp => {
								self.setState({
										err: resp.data.error,
										errMsg: resp.data.errorMessage,
										showWithdrawRequest: false
							});
								if(resp.data.error == commonErr.SUCCESS){
										self.setState({
												errMsg: this.renderMessage(),
										});
								}
								else {
										self.setState({
												isSubmittedSuccess: false
										});
								}
			});  
			this.getWalletInfo(0);
		}
	}

	renderMessage() {
				return (
						<div className='message-box'>  
								Gửi yêu cầu thành công
								<p>Yêu cầu rút tiền đã được gửi đi.</p>
								<p>Bộ phận kế toán sẽ tiến hành xử lý yêu cầu rút tiền của bạn trong vòng 3 ngày làm việc tiếp theo (không kể Thứ 7 - Chủ nhật và ngày lễ Tết) kể từ khi yêu cầu được gửi</p>
								<p>Để được hỗ trợ, vui lòng gọi hotline 19009217 (T2-T7, 9AM-6PM).</p>
								<p>Xin cám ơn,</p>
								<p>MIOTO TEAM</p>
						</div>
				)
	}
	
	hideMessageBox() {
				this.setState({
						err: commonErr.INNIT,
						errMsg: ""
				});
	}
	
	close(){
				this.setState({
						showConfirmCancelWithdraw: false
				})
		}
	
	render() {
	
		var monthData = this.initMonthData();	
		var content;
		var timeRequestContent = "";
		var btnWithdrawContent = <a onClick={this.openWithdrawRequest.bind(this)} className="btn btn--m btn-primary">Gửi yêu cầu rút tiền</a>;
		if (this.state.err === commonErr.LOADING) {
				content = <LoadingPage />
		} else {
			if(this.state.timeRequested && this.state.timeRequested > 0) {
				btnWithdrawContent = <a onClick={this.confirmCancelWithdrawRequest.bind(this)} className="btn btn--m btn-red">Hủy yêu cầu rút tiền</a>;
				timeRequestContent = <p>Thời gian yêu cầu rút tiền: {moment(this.state.timeRequested).format("DD-MM-YYYY HH:mm")}</p>
			}
			content = <div className="module-register">
				<div className="register-container owner-report">
					<div className="owner-report__wrap">
						<div className="owner-report--heading"> 
							<h4 className="title">Bảng tổng hợp giao dịch</h4>
							<div className="line-form"> 
								<div className="wrap-select custom-select--dark">
									<select value={this.state.monthInMilliseconds ? this.state.monthInMilliseconds : monthData[0].monthInMilliseconds} onChange={this.onSelectMonthChange.bind(this)}>
									{ monthData.map(monthObj =>(<option key={monthObj.monthInMilliseconds} value={monthObj.monthInMilliseconds}>Tháng {monthObj.monthInText.slice(3)}</option>)) } 									
									</select>
								</div>
							</div>
						</div>
						<div className="owner-report--body textAlign-center">
							<div className="balance-summary">
								<h3><NumberFormat value={this.state.balance} displayType={'text'} thousandSeparator={" "} decimalSeparator={","} suffix={"đ"} /></h3>
								<p>Số dư hiện tại </p>
							</div>						
							{this.state.data && <WalletItem data={this.state.data} monthInMilliseconds={this.state.monthInMilliseconds}></WalletItem>}
						</div>
						<div className="down-report">
							{/* <label>Tải sao kê chi tiết giao dịch</label>
							<div className="d-flex">
								<button className="btn btn--s btn-default">PDF</button>
								<button className="btn btn--s btn-default">Excel</button>
							</div> */}
							{timeRequestContent}
						</div>
					</div>
		
					</div>
					<div className="wrap-btn report-btn d-flex">
						{btnWithdrawContent}
						<Link className="btn btn--m btn-secondary" to={{ pathname: `/mymonthlyreport/${moment(Number(this.state.monthInMilliseconds)).format("MM-YYYY")}` }}>
							Xem Sao kê chi tiết giao dịch
						</Link>
					</div>
			</div>
		}
		
		return <div className="mioto-layout">
			<Header />
			<StickyContainer>
				<section className="body">
					<div className="sidebar-control z-2">
							<div className="sidebar-settings general-settings">
								<ul>
									<li> <a href="/mycars"><i className="ic ic-cars"></i><span className="label">Danh sách xe</span></a></li>
									<li> <a href="/calendars"><i className="ic ic-setting-calendar"></i><span className="label">Lịch chung</span></a></li>
									<li> <a href="/gps"><i className="ic ic-gps"></i><span className="label">GPS</span><sup class="new-func">BETA</sup></a></li>
									<li> <a href="/commonsetting"><i className="ic ic-setting-rent"> </i><span className="label">Cài đặt chung</span></a></li>
									<li> <a href="/carregister"><i className="ic ic-plus-black"></i><span className="label">Đăng ký xe </span></a></li>
									<li> <a href="/mywallet"><i className="ic ic-owner-wallet"></i><span className="label">Số dư: <span className="fontWeight-6 font-16"><NumberFormat value={this.state.balance} displayType={'text'} thousandSeparator={" "} decimalSeparator={","} suffix={"đ"} /></span></span></a></li>	
								</ul>
							</div>
					</div>
					{content}
					<Modal show={this.state.showWithdrawRequest}
						onHide={this.closeWithdrawRequest.bind(this)}
						dialogClassName="modal-sm modal-dialog modal-withdrawal-req">
							<Modal.Header closeButton={true} closeLabel={""}>
									<Modal.Title>Yêu cầu rút tiền</Modal.Title>
							</Modal.Header>
								<Modal.Body>
								<div className="request-container">
									<p>Với trách nhiệm thuộc về Tôi/Chúng tôi, đề nghị Quý Công ty ghi nợ tài khoản của Tôi/Chúng tôi để thực hiện chuyển tiền theo nội dung sau:</p>
									{this.state.alertMsg !== "" && <div className="message-valid-box">
										<p>{this.state.alertMsg}</p>
									</div>}
									<div className="form-request form-default">
										<div className="group">
											<span className="lstitle">SỐ TIỀN (*)</span>
											<div className="line-form">
												<div className={this.state.isAmountValid ? "wrap-input" : "wrap-input has-error-form"}> 
												<NumberFormat value={this.state.amount} onValueChange={this.amountChangedHandler} placeholder="Nhập số tiền cần rút" name="amount" thousandSeparator={","}/>
												</div>
											</div>
										</div>
										<div className="group">
											<span className="lstitle">NGƯỜI THỤ HƯỞNG</span>
											<div className="line-form">
												<label className="label">Số tài khoản (*)</label>
												<div className={this.state.isAccountNumberValid ? "wrap-input" : "wrap-input has-error-form"}> 
													<input placeholder="1247563759505" type="text" name="accountNumber" value={this.state.accountNumber} onChange={this.handleInputChange}/>
												</div>
											</div>
											<div className="line-form">
												<label className="label">Tên người thụ hưởng (*)</label>
												<div className={this.state.isAccountNameValid ? "wrap-input" : "wrap-input has-error-form"}> 
													<input placeholder="Nguyễn Văn A" type="text" name="accountName" value={this.state.accountName} onChange={this.handleInputChange}/>
												</div>
											</div>
										</div>
										<div className="group">
											<span className="lstitle">NGÂN HÀNG THỤ HƯỞNG</span>
											<div className="line-form">
												<label className="label">Ngân hàng thụ hưởng (*)</label>
												<div className={this.state.isBankNameValid ? "wrap-input" : "wrap-input has-error-form"}> 
													<input placeholder="Ngân hàng Sacombank" type="text" name="bankName" value={this.state.bankName} onChange={this.handleInputChange}/>
												</div>
											</div>
											<div className="line-form">
												<label className="label">Tỉnh/Thành phố (*)</label>
												<div className={this.state.isBankProvinceValid ? "wrap-input" : "wrap-input has-error-form"}> 
													<input placeholder="Hồ Chí Minh" type="text" name="bankProvince" value={this.state.bankProvince} onChange={this.handleInputChange}/>
												</div>
											</div>
											<div className="line-form">
												<label className="label">Chi nhánh (*)</label>
												<div className={this.state.isBankBranchValid ? "wrap-input" : "wrap-input has-error-form"}> 
													<input placeholder="Quận 4" type="text" name="bankBranch" value={this.state.bankBranch} onChange={this.handleInputChange}/>
												</div>
											</div>
										</div>
										<div className="group group-inline">
											<span className="lstitle">PHÍ CHUYỂN TIỀN (11,000VND)</span>
										</div>
										<div className="group">
											<a className="btn btn--m btn-request btn-primary" onClick={this.onRequestWithdrawClick.bind(this)}>Gửi yêu cầu</a>
										</div>
									</div>
								</div>
							</Modal.Body>
						</Modal>
						<MessageBox show={this.state.err <= commonErr.SUCCESS} hideModal={this.hideMessageBox.bind(this)} message={this.state.errMsg} error={this.state.err} />
						<Modal
							show={this.state.showConfirmCancelWithdraw}
							onHide={this.close}
							dialogClassName="modal-sm modal-dialog">
							<Modal.Header closeButton={true} closeLabel={""}>
								<Modal.Title>Thông báo</Modal.Title>
							</Modal.Header>
							<Modal.Body><div className="module-register" style={{ background: "none", padding: "0" }}>
								<div className="form-default form-s">
									<div className="textAlign-center">
										<p style={{ color: '#141414' }}>Bạn muốn hủy yêu cầu rút tiền?</p>
									</div>
									<div className="space m" />
									<div className="space m" />
									<div className="wrap-btn has-2btn">
										<div className="wr-btn">
											<a className="btn btn-secondary btn--m" onClick={this.close}>Bỏ qua</a>
										</div>
										<div className="wr-btn">
											<a className="btn btn-primary btn--m" onClick={this.cancelWithdrawRequest.bind(this)}>Đồng ý</a>
										</div>
									</div>
								</div>
							</div></Modal.Body>
						</Modal>
					</section>
				</StickyContainer>
			</div>
	}
}

export default MyWallet;