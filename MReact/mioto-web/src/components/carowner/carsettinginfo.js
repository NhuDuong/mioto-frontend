import React from "react"

import { commonErr } from "../common/errors"
import { LoadingInline, LoadingOverlay } from "../common/loading"
import { CarTransmission, CarFuel } from "../common/common"
import { getFilterConfig, updateCar } from "../../model/car"
import { MessageBox } from "../common/messagebox"

export default class CarSettingInfo extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            err: commonErr.INNIT,
            errMsg: "",
            updateErrMsg: ""
        }
    }

    init(car) {
        getFilterConfig().then(resp => {
            if (resp.data.data) {
                const featuresAll = resp.data.data.allFeatures;
                const features = car.features;
                for (var i = 0; i < featuresAll.length; ++i) {
                    for (var j = 0; j < features.length; ++j) {
                        if (featuresAll[i].id === features[j].id) {
                            featuresAll[i].checked = true;
                            break;
                        }
                    }
                }
                this.setState({
                    err: resp.data.error,
                    errMsg: resp.data.errorMessage,
                    featuresAll: resp.data.data.allFeatures,
                    car: car,
                    desc: car.desc,
                    fuelConsumption: car.optionsFuelConsumption
                })
            } else {
                this.setState({
                    err: resp.data.error,
                    errMsg: resp.data.errorMessage
                })
            }
        })
    }

    componentDidMount() {
        const car = this.props.car;
        this.init(car);
    }

    componentWillReceiveProps(props) {
        const car = props.car;
        this.init(car);
    }

    onFuelRateChange(event) {
        this.setState({
            fuelConsumption: event.target.value
        });
    }

    onDescChange(event) {
        this.setState({
            desc: event.target.value
        });
    }

    onFeaturesChange(event) {
        const featuresAll = this.state.featuresAll.slice();
        for (var i = 0; i < featuresAll.length; ++i) {
            const feature = featuresAll[i];
            if (event.target.value === feature.id) {
                feature.checked = event.target.checked
            }
        }
        this.setState({
            featuresAll: featuresAll
        });
    }

    hideMessageBox() {
        this.setState({
            updateErrMsg: ""
        });
    }

    updateCarInfo() {
        const fuelConsumption = this.state.fuelConsumption;
        const desc = this.state.desc;
        const featuresAll = this.state.featuresAll;
        const features = [];
        for (var i = 0; i < featuresAll.length; ++i) {
            const feature = featuresAll[i];
            if (feature.checked === true) {
                features.push(feature.id);
            }
        }

        this.setState({
            err: commonErr.LOADING
        });
        updateCar(this.state.car.id, {
            fuelConsumption: fuelConsumption,
            desc: encodeURI(desc),
            features: features
        }).then(resp => {
            this.setState({
                err: resp.data.error,
                updateErrMsg: resp.data.errorMessage
            });
        });
    }

    render() {
        var content;
        if (this.state.err === commonErr.INNIT) {
            content = <LoadingInline />
        } else {
            const car = this.state.car;
            const featuresAll = this.state.featuresAll;

            content = <div className="content">
                {this.state.err === commonErr.LOADING && <LoadingOverlay />}
                {this.state.updateErrMsg && this.state.updateErrMsg !== "" && <MessageBox error={this.state.err} message={this.state.updateErrMsg} show={this.state.updateErrMsg && this.state.updateErrMsg !== ""} hideModal={this.hideMessageBox.bind(this)} />}
                <div className="content-container">
                    <div className="col-1">
                        <h3>Biển số</h3>
                        <div className="form-default">
                            <div className="line-form">
                                <div className="wrap-input">
                                    <input type="text" value={car.licensePlate} disabled={true} />
                                </div>
                                <span className="note">Thông tin không thể thay đổi.</span>
                            </div>
                        </div>
                    </div>
                    <div className="space m clear"></div>
                    <h3>Thông tin cơ bản</h3>
                    <div className="form-default">
                        <div className="col-1 clear">
                            <div className="line-form">
                                <label className="label">Số ghế</label>
                                <div className="wrap-input">
                                    <input type="text" value={car.seat} disabled={true} />
                                </div>
                            </div>
                        </div>
                        <div className="col-2">
                            <div className="line-form">
                                <label className="label">Truyền động</label>
                                <div className="wrap-input">
                                    <input type="text" value={CarTransmission[car.optionsTransmission]} disabled={true} />
                                </div>
                            </div>
                        </div>
                        <div className="col-1 clear">
                            <div className="line-form">
                                <label className="label">Loại nhiên liệu</label>
                                <div className="wrap-input">
                                    <input type="text" value={CarFuel[car.optionsFuel]} disabled={true} />
                                </div>
                            </div>
                        </div>
                        <div className="col-2">
                            <div className="line-form">
                                <label className="label">Mức tiêu thụ nhiên liệu (lít/100km)</label>
                                <div className="wrap-input">
                                    <input type="text" value={this.state.fuelConsumption} onChange={this.onFuelRateChange.bind(this)} />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="space m clear"></div>
                    <h3>Mô tả</h3>
                    <div className="form-default">
                        <div className="line-form end">
                            <textarea className="textarea" wrap="hard" onChange={this.onDescChange.bind(this)} value={this.state.desc} />
                        </div>
                    </div>
                    <div className="space m clear"></div>
                    <h3>Tính năng</h3>
                    <div className="list-features">
                        <ul>
                            {featuresAll && featuresAll.map(option => <li key={option.id}>
                                <div className="squaredFour have-label">
                                    <input id={`${option.id}`} type="checkbox" checked={option.checked ? true : false} value={option.id} onChange={this.onFeaturesChange.bind(this)} />
																<label className="d-flex" htmlFor={`${option.id}`}> <img style={{ float: "left", width: "16px", height: "16px", marginRight: "5px" }} className="img-ico" src={option.logo} alt="Mioto - Thuê xe tự lái" /> {option.name}</label>
                                </div>
                            </li>)}
                        </ul>
                    </div>
                    <div className="space m"></div>
                    <div className="line"></div>
                    <a className="btn btn-primary btn--m" onClick={this.updateCarInfo.bind(this)}>Lưu thay đổi</a>
                </div>
            </div>
        }

        return content;
    }
} 