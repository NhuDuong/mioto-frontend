import React from "react"
import moment from "moment"
import { Modal } from "react-bootstrap"
import DatePicker from "react-datepicker"

import "react-datepicker/dist/react-datepicker-cssmodules.css"

import { commonErr } from "../common/errors"
import { LoadingInline } from "../common/loading"
import { getCarCalendar, getCarSetting, updateCar, getTripDetail } from "../../model/car"
import { formatPrice, getTripStatus, listAvailFrom, listAvailTo, DEFAULT_AVAIL_FROM, DEFAULT_AVAIL_TO } from "../common/common"
import { MessageLine, MessageBox } from "../common/messagebox"

import avatar_default from "../../static/images/avatar_default.png"

const settingModes = {
    price: 0,
    calendar: 1
}

class TripDetailBox extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            err: commonErr.INNIT,
            errMsg: "",
        }
    }

    getTrips(day) {
        this.setState({
            err: commonErr.LOADING
        });

        var i;
        const tripsId = [];
        if (day.isBooked && day.bookedTripId !== "") {
            tripsId.push(day.bookedTripId);
        } else if (day.tripsReq && day.tripsReq.length > 0) {
            for (i = 0; i < day.tripsReq.length; ++i) {
                tripsId.push(day.tripsReq[i].tripInfo.tripId);
            }
        }
        if (tripsId.length > 0) {
            var tripsInfo = [];
            var profilesInfo = [];

            for (i = 0; i < tripsId.length; ++i) {
                getTripDetail(tripsId[i]).then(resp => {
                    if (resp.data.error >= commonErr.SUCCESS && resp.data.data.trip) {
                        tripsInfo = tripsInfo.concat(resp.data.data.trip);
                        profilesInfo = profilesInfo.concat(resp.data.data.profiles);
                        if (tripsInfo.length === tripsId.length) {
                            this.setState({
                                err: commonErr.SUCCESS,
                                trips: tripsInfo,
                                profiles: profilesInfo
                            });
                        }
                    } else {
                        this.setState({
                            err: resp.data.error
                        });
                    }
                });
            }
        } else {
            this.setState({
                err: commonErr.SUCCESS
            });
        }
    }

    componentDidMount() {
        this.getTrips(this.props.activeDay);
    }

    componentWillReceiveProps(props) {
        if (this.props.activeCar !== props.activeCar
            || this.props.activeDay !== props.activeDay) {
            this.getTrips(props.activeDay);
        }
    }

    render() {
        const activeTs = this.props.activeDay.ts;
        const tripsDetail = [];

        if (this.state.trips) {
            const trips = this.state.trips.filter(function (trip) {
                return (moment(activeTs).isSame(moment(trip.tripDateFrom).startOf("day"))
                    || moment(activeTs).isSame(moment(trip.tripDateTo).startOf("day"))
                    || moment(activeTs).isBetween(moment(trip.tripDateFrom).startOf("day"), moment(trip.tripDateTo).endOf("day")))
                    && trip.status < 20
            });
            if (trips && trips.length > 0) {
                for (var i = 0; i < trips.length; ++i) {
                    const trip = trips[i];
                    const status = getTripStatus(trip.status);
                    var profile;
                    if (this.state.profiles) {
                        const profiles = this.state.profiles.filter(function (item) {
                            return item.uid === trip.travelerId;
                        });
                        if (profiles && profiles.length > 0) {
                            profile = profiles[0];
                        }
                    }
                    if (trip && profile && status) {
                        tripsDetail.push({
                            trip: trip,
                            profile: profile,
                            status: status
                        });
                    }
                }
            }
        }

        if (this.state.err === commonErr.INNIT || this.state.err === commonErr.LOADING) {
            return <LoadingInline />
        } else if (tripsDetail.length === 0) {
            return <MessageLine message="Không tìm thấy thông tin chuyến." />
        } else {
            return tripsDetail.map((tripDetail) => {
                const trip = tripDetail.trip;
                const profile = tripDetail.profile;
                const status = tripDetail.status;

                return <div key={trip.id} className="form-default">
                    <div className="trip-box">
                        <div className="trip-footer" style={{ paddingBottom: "0px" }}>
                            <div className="status-trips">
                                <p><span className={status.class} /> {status.name}</p>
                            </div>
                            <div className="time">
                                <p>{moment(trip.timeBooked).fromNow()}</p>
                            </div>
                        </div>
                        <div className="trip-body">
                            <div className="left">
                                <div className="space m"></div>
                                <div>Bắt đầu: {moment(trip.tripDateFrom).format("HH:mm dddd, DD/MM/YYYY")}</div>
                                <div>Kết thúc: {moment(trip.tripDateTo).format("HH:mm dddd, DD/MM/YYYY")}</div>
                                <div className="space m"></div>
                                <div className="total-price">Tổng tiền: {formatPrice(trip.priceSummary.priceTotal)}</div>
                                <div className="space m"></div>
                                <div><a target="_blank" href={`/trip/detail/${trip.id}`}><i className="ic ic-info"></i> Xem chi tiết</a></div>
                            </div>
                            <div className="right">
                                <div className="space m"></div>
                                <div className="avatar">
                                    <div className="avatar-img">
                                        <a target="_blank" href={`/profile/${profile.uid}`}>
                                            <img src={`${(profile.avatar && profile.avatar.thumbUrl) ? profile.avatar.thumbUrl : avatar_default}`} alt="Mioto - Thuê xe tự lái" />
                                        </a>
                                    </div>
                                </div>
                                <div className="lstitle">{profile.name}</div>
                            </div>
                        </div>
                    </div>
                </div>
            })
        }
    }
}

export default class CarSettingCalendar extends React.Component {
    constructor() {
        super();
        this.state = {
            err: commonErr.INNIT,
            errMsg: "",
            isShowMsgBox: false,
            updateErrMsg: "",
            settingMode: settingModes.price,
            isShowPriceSettingBox: false,
            customPrice: 0,
            activeTs: 0,
            isShowPriceRepeatSettingBox: false,
            isShowUnavailsRepeatSettingBox: false,
            unavailsBefore: DEFAULT_AVAIL_FROM,
            unavailsAfter: DEFAULT_AVAIL_TO,
            isTurnOnUnavailsRange: false
        }
    }

    init(car) {
        const self = this;
        getCarCalendar(car.id, "cs").then(function (calendarResp) {
            if (calendarResp.data.error >= commonErr.SUCCESS) {              
                getCarSetting({
                    carId: car.id
                }).then(function (settingResp) {
                    if (settingResp.data.error >= commonErr.SUCCESS) {
                        const unavailDays = [];
                        const bookedDays = [];
                        const priceSpecifics = [];
                        const priceRepeat = [];
                        const unavailsRepeat = [];
                        const instantDays = [];

                        const calendar = calendarResp.data.data;
                        const setting = settingResp.data.data;

                        if (calendar) {
                            var i, j;
                            if(car.instantEnable){                                
                                const start = car.instantRangeFrom === 0 ? moment(calendar.startDate).startOf('day').valueOf() : moment(moment().valueOf() + car.instantRangeFrom * 1000).startOf('day').valueOf();
                                const end = car.instantRangeTo === 0 ? moment(calendar.endDate).endOf('month').valueOf() : moment(moment().valueOf() + car.instantRangeTo * 1000).startOf('day').valueOf();
                                for(i = start; i <= end; i += (3600000 * 24)){
                                    instantDays.push(i);
                                }
                            }
                            if (calendar.unavails) {
                                for (i = 0; i < calendar.unavails.length; ++i) {
                                    const start = moment(calendar.unavails[i].startTime).startOf('day').valueOf();
                                    const end = moment(calendar.unavails[i].endTime).startOf('day').valueOf();
                                    for (j = start; j <= end; j += (3600000 * 24)) {
                                        unavailDays.push(j);
                                    }
                                }
                            }

                            if (calendar.bookings) {
                                for (i = 0; i < calendar.bookings.length; ++i) {
                                    const start = moment(calendar.bookings[i].startTime).startOf('day').valueOf();
                                    const end = moment(calendar.bookings[i].endTime).startOf('day').valueOf();
                                    for (j = start; j <= end; j += (3600000 * 24)) {
                                        bookedDays.push({
                                            ts: j,
                                            startTs: calendar.bookings[i].startTime,
                                            endTs: calendar.bookings[i].endTime
                                        });
                                    }
                                }
                            }

                            if (calendar.priceSpecifics) {
                                for (i = 0; i < calendar.priceSpecifics.length; ++i) {
                                    const start = moment(calendar.priceSpecifics[i].startTime).startOf('day').valueOf();
                                    const end = moment(calendar.priceSpecifics[i].endTime).startOf('day').valueOf();
                                    for (j = start; j <= end; j += (3600000 * 24)) {
                                        priceSpecifics.push({
                                            ts: j,
                                            price: calendar.priceSpecifics[i].price
                                        });
                                    }
                                }
                            }

                            for (i = 1; i <= 7; ++i) {
                                const weekday = {
                                    weekday: i,
                                    price: 0
                                };

                                if (calendar.priceRepeat) {
                                    for (j = 0; j < calendar.priceRepeat.length; ++j) {
                                        if (i === 7 && calendar.priceRepeat[j].weekday === 1) {
                                            weekday.price = calendar.priceRepeat[j].price;
                                            break;
                                        } else if ((i + 1) === calendar.priceRepeat[j].weekday) {
                                            weekday.price = calendar.priceRepeat[j].price;
                                            break;
                                        }
                                    }
                                }

                                priceRepeat.push(weekday);
                            }

                            for (i = 1; i <= 7; ++i) {
                                const weekday = {
                                    weekday: i,
                                    unavail: false
                                };

                                if (calendar.unavailsRepeatWeekday) {
                                    for (j = 0; j < calendar.unavailsRepeatWeekday.length; ++j) {
                                        if (i === 7 && calendar.unavailsRepeatWeekday[j] === 1) {
                                            weekday.unavail = true;
                                            break;
                                        } else if ((i + 1) === calendar.unavailsRepeatWeekday[j]) {
                                            weekday.unavail = true;
                                            break;
                                        }
                                    }
                                }

                                unavailsRepeat.push(weekday);
                            }

                            self.setState({
                                err: settingResp.data.error,
                                errMsg: settingResp.data.errorMessage,
                                car: car,
                                time: moment(),
                                priceDaily: calendar.priceDaily,
                                customPrice: calendar.priceDaily,
                                customRepeatPrice: calendar.priceDaily,
                                setting: setting,
                                instantDays : instantDays,
                                unavailDays: unavailDays,
                                bookedDays: bookedDays,
                                priceSpecifics: priceSpecifics,
                                priceRepeat: priceRepeat,
                                isLimitRepeatPrice: calendar.priceRepeatEndDate > 0,
                                priceRepeatEndDate: calendar.priceRepeatEndDate > 0 ? calendar.priceRepeatEndDate : moment().add(1, 'month').valueOf(),
                                unavailsRepeat: unavailsRepeat,
                                isLimitRepeatUnavails: calendar.unavailsRepeatEndDate > 0,
                                unavailsRepeatEndDate: calendar.unavailsRepeatEndDate > 0 ? calendar.unavailsRepeatEndDate : moment().add(1, 'month').valueOf(),
                                unavailsBefore: calendar.unavailsBefore,
                                unavailsAfter: calendar.unavailsAfter > 0 ? calendar.unavailsAfter : DEFAULT_AVAIL_TO,
                            });
                        }
                    }
                });
            }
        });
    }

    componentDidMount() {
        const car = this.props.car;
        this.init(car);
    }

    componentWillReceiveProps(props) {
        const car = props.car;
        this.init(car);
    }

    setSettingMode(mode) {
        this.setState({
            settingMode: mode
        });
    }

    toggleUnAvailsRange(){
        var isTurnOnUnavailsRange = !this.state.isTurnOnUnavailsRange;
        this.setState({
            isTurnOnUnavailsRange: isTurnOnUnavailsRange,
        })
        if(isTurnOnUnavailsRange){
            this.setState({
                unavailsBefore: DEFAULT_AVAIL_FROM,
                unavailsAfter: DEFAULT_AVAIL_TO,
            })
        } else {
            this.updateCarAvails(0, 0);
        }
    }

    onAvailFromChange(event){
        this.setState({
            unavailsBefore: event.target.value
        })
    }

    onAvailToChange(event){
        this.setState({
            unavailsAfter: event.target.value
        })
    }

    onUpdateAvailsClick(){
        if(this.state.unavailsAfter && this.state.unavailsAfter > this.state.unavailsBefore){
            this.updateCarAvails(this.state.unavailsBefore, this.state.unavailsAfter);
        } 
    }
    

    setAvailDay(ts) {
        const unavailDays = this.state.unavailDays.filter(function (day) {
            return ts !== day;
        });

        this.setState({
            unavailDays: unavailDays
        });

        this.updateCarCalendar(unavailDays);
    }

    setUnavailDay(ts) {
        const unavailDays = this.state.unavailDays.slice();
        unavailDays.push(ts);

        this.setState({
            unavailDays: unavailDays
        });

        this.updateCarCalendar(unavailDays);
    }

    onDayItemClick(day) {
        if (this.state.settingMode === settingModes.calendar) {
            if (day.isUnavai) {
                this.setAvailDay(day.ts);
            } else {
                this.setUnavailDay(day.ts);
            }
        } else {
            this.setPrice(day);
        }
    }

    hidePriceSettingBox() {
        this.setState({
            isShowPriceSettingBox: false
        });
    }

    confirmPriceSetting() {
        const ts = this.state.activeTs;
        var customPrice;
        if (this.state.customPrice === undefined || this.state.customPrice === 0) {
            customPrice = this.state.priceDaily;
        } else {
            customPrice = this.state.customPrice;
            if (customPrice < this.state.setting.priceDailyMin || customPrice > this.state.setting.priceDailyMax) {
                const err = commonErr.FAIL;
                const updateErrMsg = `Giá chỉ cho phép trong khoản từ ${formatPrice(this.state.setting.priceDailyMin)} đến ${formatPrice(this.state.setting.priceDailyMax)}.`;
                this.setState({
                    err: err,
                    updateErrMsg: updateErrMsg
                });
                return;
            }
        }

        var priceSpecifics = this.state.priceSpecifics.filter(function (day) {
            return ts !== day.ts;
        });

        priceSpecifics.push({
            ts: ts,
            price: customPrice
        });

        this.setState({
            priceSpecifics: priceSpecifics,
            isShowPriceSettingBox: false
        });

        this.updateCarPrice(priceSpecifics);
    }

    onCustomPriceRangeChange(event) {
        if (isNaN(event.target.value)) {
            return;
        }

        const price = event.target.value * 1000;
        this.setState({
            err: commonErr.SUCCESS,
            updateErrMsg: "",
            customPrice: price
        });
    }

    setPrice(day) {
        this.setState({
            isShowPriceSettingBox: true,
            activeTs: day.ts,
            activeDay: day,
            customPrice: day.price
        });
    }

    updateCarAvails(unavailsBefore, unavailsAfter) {
        updateCar(this.state.car.id, {
            unavailsBefore: unavailsBefore,
            unavailsAfter: unavailsAfter
        }).then(resp => {
            this.setState({
                err: resp.data.error,
                errMsg: resp.data.errorMessage,
                updateErrMsg: resp.data.errorMessage,
                isShowMsgBox: true,
            });
        });
    }

    updateCarCalendar(unavailDays) {
        updateCar(this.state.car.id, {
            unavailDays: unavailDays,
        }).then(resp => {
            this.setState({
                err: resp.data.error,
                errMsg: resp.data.errorMessage,
                updateErrMsg: resp.data.errorMessage
            });
        });
    }

    updateCarPrice(priceSpecifics) {
        updateCar(this.state.car.id, {
            priceSpecifics: priceSpecifics
        }).then(resp => {
            this.setState({
                err: resp.data.error,
                errMsg: resp.data.errorMessage,
                updateErrMsg: resp.data.errorMessage
            });
        });
    }

    setRepeat() {
        if (this.state.settingMode === settingModes.price) {
            this.setPriceRepeat();
        } else {
            this.setUnavailsRepeat();
        }
    }

    //price repeat
    hidePriceRepeatSettingBox() {
        this.setState({
            isShowPriceRepeatSettingBox: false
        });
    }

    confirmPriceRepeatSetting() {
        const self = this;
        const invailidPrice = this.state.priceRepeat.filter(function (weekday) {
            return (weekday.price !== 0 && weekday.price !== self.state.priceDaily && (weekday.price < self.state.setting.priceDailyMin || weekday.price > self.state.setting.priceDailyMax))
        });

        if (invailidPrice && invailidPrice.length > 0) {
            const err = commonErr.FAIL;
            const updateErrMsg = `Giá chỉ cho phép trong khoản từ ${formatPrice(this.state.setting.priceDailyMin)} đến ${formatPrice(this.state.setting.priceDailyMax)}.`;
            this.setState({
                err: err,
                updateErrMsg: updateErrMsg
            });
            return;
        }

        const priceRepeat = this.state.priceRepeat.filter(function (weekday) {
            return (weekday.price && weekday.price > 0);
        });

        const priceRepeatEndDate = this.state.isLimitRepeatPrice === true ? this.state.priceRepeatEndDate : 0;

        this.updateCarPriceRepeat(priceRepeat, priceRepeatEndDate);
    }

    onCustomPriceRepeatChange(event) {
        const price = event.target.value * 1000;

        if (isNaN(price)) {
            return;
        }

        const priceRepeat = this.state.priceRepeat.slice();
        for (var i = 0; i < priceRepeat.length; ++i) {
            if (`priceRepeat_${i}` === event.target.id) {
                priceRepeat[i].price = price;
                break;
            }
        }

        this.setState({
            err: commonErr.SUCCESS,
            updateErrMsg: "",
            priceRepeat: priceRepeat
        });
    }

    setPriceRepeat() {
        this.setState({
            isShowPriceRepeatSettingBox: true,
        });
    }

    onLimitRepeatPriceChange(event) {
        this.setState({
            isLimitRepeatPrice: event.target.checked
        });
    }

    onPriceRepeatEndDayChange(date) {
        this.setState({
            priceRepeatEndDate: date.valueOf()
        });
    }

    updateCarPriceRepeat(priceRepeat, priceRepeatEndDate) {
        const self = this;

        updateCar(this.state.car.id, {
            priceRepeat: priceRepeat,
            priceRepeatEndDate: priceRepeatEndDate
        }).then(function (resp) {
            self.setState({
                err: resp.data.error,
                updateErrMsg: resp.data.errorMessage,
                isShowPriceRepeatSettingBox: false
            });
        });
    }

    //unavails repeat
    hideUnavailsRepeatSettingBox() {
        this.setState({
            isShowUnavailsRepeatSettingBox: false
        });
    }

    confirmUnavailsRepeatSetting() {
        const unavailsRepeat = this.state.unavailsRepeat.filter(function (weekday) {
            return (weekday.unavail === true);
        });

        const unavailsRepeatEndDate = this.state.isLimitRepeatUnavails === true ? this.state.unavailsRepeatEndDate : 0;
        this.updateCarUnavailsRepeat(unavailsRepeat, unavailsRepeatEndDate);
    }

    onUnavailsRepeatChange(event) {
        const unavail = event.target.checked;
        const unavailsRepeat = this.state.unavailsRepeat.slice();
        for (var i = 0; i < unavailsRepeat.length; ++i) {
            if (`unavailsRepeat_${i}` === event.target.id) {
                unavailsRepeat[i].unavail = unavail;
                break;
            }
        }
        this.setState({
            unavailsRepeat: unavailsRepeat
        });
    }

    setUnavailsRepeat() {
        this.setState({
            isShowUnavailsRepeatSettingBox: true,
        });
    }

    onLimitUnavailsChange(event) {
        this.setState({
            isLimitRepeatUnavails: event.target.checked
        });
    }

    onUnavailsRepeatEndDayChange(date) {
        this.setState({
            unavailsRepeatEndDate: date.valueOf()
        });
    }

    updateCarUnavailsRepeat(unavailsRepeat, unavailsRepeatEndDate) {
        updateCar(this.state.car.id, {
            unavailsRepeat: unavailsRepeat,
            unavailsRepeatEndDate: unavailsRepeatEndDate
        }).then(resp => {
            this.setState({
                err: resp.data.error,
                updateErrMsg: resp.data.errorMessage,
                isShowUnavailsRepeatSettingBox: false
            });
        });
    }

    buildMonthCalendar(time) {
        const days = [];
        const mapTripsMaxLevel = {};
        const setting = this.state.setting;

        const start = moment(time);
        const end = moment(time);
        const startOfMonth = start.startOf('month');
        const endOfMonth = end.endOf('month');

        const startDayOfWeek = startOfMonth.isoWeekday();
        const endDayOfWeek = endOfMonth.isoWeekday();
        const startOffset = startDayOfWeek - 1;
        const endOffset = 7 - endDayOfWeek;
        const startOffCalendar = moment(startOfMonth.valueOf()).subtract(startOffset, 'days');
        const endOfCalendar = moment(endOfMonth.valueOf()).add(endOffset, 'days');

        const unavailDays = this.state.unavailDays;
        const bookedDays = this.state.bookedDays;
        const instantDays = this.state.instantDays;
        const priceSpecifics = this.state.priceSpecifics;
        const priceRepeat = this.state.priceRepeat;
        const priceRepeatEndDate = this.state.priceRepeatEndDate;
        const isLimitRepeatPrice = this.state.isLimitRepeatPrice;
        const unavailsRepeat = this.state.unavailsRepeat;
        const unavailsRepeatEndDate = this.state.unavailsRepeatEndDate;
        const isLimitRepeatUnavails = this.state.isLimitRepeatUnavails;
        const unavailsBefore = this.state.unavailsBefore;
        const unavailsAfter = this.state.unavailsAfter;

        var i, j;
        const _1dayInMSec = 24 * 3600000;
        for (i = startOffCalendar.valueOf(); i <= endOfCalendar.valueOf(); i = i + (3600000 * 24)) {
            var isToday = moment(i).isSame(moment(), 'day');
            var isPast = moment(i).isBefore(moment(), 'day');
            var isOutside = moment(i).isBefore(startOfMonth, 'day') || endOfMonth.isBefore(moment(i), 'day');
            var isBooked = false;
            var isInstant = false;
            var bookedTripId = "";
            var bookedWidth;
            var bookedLeft;
            var isUnavai = false;
            var price = this.state.priceDaily;
            var isUnavailRepeat = false;
            var isPriceRepeat = false;

            var trips = [];
            var tripsReq = [];

            for (j = 0; j < unavailDays.length; ++j) {
                if (i === unavailDays[j]) {
                    isUnavai = true;
                    break;
                }
            }

            for (j = 0; j < instantDays.length; ++j){
                if (i === instantDays[j]){
                    isInstant = true;
                    break;
                }
            }

            if (unavailsAfter && unavailsAfter > unavailsBefore){
                if (i < moment(unavailsBefore * 1000 + moment().valueOf()).startOf('day').valueOf() || i > unavailsAfter * 1000 + moment().valueOf()){
                    isUnavai = true;
                }
            }

            for (j = 0; j < bookedDays.length; ++j) {
                const trip = bookedDays[j];
                if (i === trip.ts) {
                    isBooked = true;
                    bookedTripId = trip.tripId;
                    bookedWidth = 100;
                    bookedLeft = 0;
                    if (moment(trip.startTs).isSame(moment(trip.endTs), "day")) {
                        bookedWidth = (trip.endTs - trip.startTs) * 100 / _1dayInMSec
                        bookedLeft = 100 - (i + _1dayInMSec - trip.startTs) * 100 / _1dayInMSec
                    } else if (moment(trip.endTs).isBetween(moment(i), moment(i + _1dayInMSec))) {
                        bookedWidth = (trip.endTs - i) * 100 / _1dayInMSec
                    } else if (moment(trip.startTs).isBetween(moment(i), moment(i + _1dayInMSec))) {
                        bookedWidth = (i + _1dayInMSec - trip.startTs) * 100 / _1dayInMSec
                        bookedLeft = 100 - bookedWidth
                    }
                }
            }

            for (j = 0; j < priceRepeat.length; ++j) {
                const weekday = priceRepeat[j];
                if (weekday.price > 0
                    && moment(i).isoWeekday() === weekday.weekday
                    && (!isLimitRepeatPrice || !priceRepeatEndDate || priceRepeatEndDate === 0 || i <= priceRepeatEndDate)) {
                    price = weekday.price;
                    isPriceRepeat = true;
                    break;
                }
            }

            for (j = 0; j < priceSpecifics.length; ++j) {
                if (i === priceSpecifics[j].ts) {
                    price = priceSpecifics[j].price
                    break;
                }
            }

            for (j = 0; j < unavailsRepeat.length; ++j) {
                const weekday = unavailsRepeat[j];
                if (moment(i).isoWeekday() === weekday.weekday
                    && weekday.unavail === true
                    && (!isLimitRepeatUnavails || !unavailsRepeatEndDate || unavailsRepeatEndDate === 0 || i <= unavailsRepeatEndDate)) {
                    isUnavai = true;
                    isUnavailRepeat = true;
                    break;
                }
            }

            if (setting.waitApproves) {
                for (j = 0; j < setting.waitApproves.length; ++j) {
                    trips.push({ ...setting.waitApproves[j], isApproved: false });
                }
            }
            if (setting.waitDeposits) {
                for (j = 0; j < setting.waitDeposits.length; ++j) {
                    trips.push({ ...setting.waitDeposits[j], isApproved: true });
                }
            }
            trips = trips.sort(function (trip1, trip2) {
                return trip1.startTime - trip2.startTime;
            });

            for (j = 0; j < trips.length; ++j) {
                const trip = trips[j];
                if (moment(i).isSame(moment(trip.startTime).startOf("day"))
                    || moment(i).isSame(moment(trip.endTime).startOf("day"))
                    || moment(i).isBetween(moment(trip.startTime).startOf("day"), moment(trip.endTime).endOf("day"))) {

                    const isStart = moment(i).isSame(moment(trip.startTime), "day");
                    var width = 100;
                    var left = 0;

                    if (moment(trip.startTime).isSame(moment(trip.endTime), "day")) {
                        width = (trip.endTime - trip.startTime) * 100 / (24 * 3600000)
                        left = 100 - (i + 24 * 3600000 - trip.startTime) * 100 / (24 * 3600000)
                    } else if (moment(trip.endTime).isBetween(moment(i), moment(i + 24 * 3600000))) {
                        width = (trip.endTime - i) * 100 / (24 * 3600000)
                    } else if (moment(trip.startTime).isBetween(moment(i), moment(i + 24 * 3600000))) {
                        width = (i + 24 * 3600000 - trip.startTime) * 100 / (24 * 3600000)
                        left = 100 - width
                    }

                    tripsReq.push({
                        tripInfo: trip,
                        width: width,
                        left: left,
                        isStart: isStart
                    });

                    var level = 1;
                    for (var k = 0; k < j; ++k) {
                        const stackedTrip = trips[k];
                        if (trip.startTime <= stackedTrip.endTime) {
                            if (mapTripsMaxLevel[stackedTrip.tripId]) {
                                level = mapTripsMaxLevel[stackedTrip.tripId] + 1;
                            } else {
                                level = level + 1;
                            }
                        }
                    }

                    if (!mapTripsMaxLevel[trip.tripId] || level > mapTripsMaxLevel[trip.tripId]) {
                        mapTripsMaxLevel[trip.tripId] = level;
                    }
                }
            }

            days.push({
                ts: i,
                isToday: isToday,
                isPast: isPast,
                isOutside: isOutside,
                isBooked: isBooked,
                bookedTripId: bookedTripId,
                bookedLeft: bookedLeft,
                bookedWidth: bookedWidth,
                isInstant: isInstant,
                isUnavai: isUnavai,
                price: price,
                isPriceRepeat: isPriceRepeat,
                isUnavailRepeat: isUnavailRepeat,
                tripsReq: tripsReq,
                mapTripsMaxLevel: mapTripsMaxLevel
            })
        }

        return days;
    }

    toggleTripInfo(isShow) {
        this.setState({
            isShowTripInfo: isShow
        });
    }

    hideMessageBox() {
        this.setState({
            errMsg: "",
            isShowMsgBox: false
        });
    }

    render() {
        var content;
        var settingUnavailsRangeContent;
        if (this.state.err === commonErr.INNIT) {
            content = <LoadingInline />
        } else {
            const months = [];

            const setting = this.state.setting;
            const startDate = moment(setting.startDate);
            const endDate = moment(setting.endDate);
            for (var m = startDate; m.isBefore(endDate, "month"); m = m.add(1, "months")) {
                months.push({
                    t: moment(m),
                    days: this.buildMonthCalendar(m)
                });
            }

            const isShowTripInfo = this.state.isShowTripInfo;
            
            settingUnavailsRangeContent = <div><div className="form-default form-instant-booking">
                <div className="line-form">
                    <label className="label">Bắt đầu từ</label>
                    <div className="wrap-select">
                        <select disabled="disabled" onChange={this.onAvailFromChange.bind(this)} value={this.state.unavailsBefore ? this.state.unavailsBefore : DEFAULT_AVAIL_FROM}> 
                        {listAvailFrom && listAvailFrom.map(option => <option key={option.key} value={option.key}>{option.value}</option>)}
                        </select>
                    </div>
                </div>
                <div className="line-form">
                    <label className="label">Cho đến</label>
                    <div className="wrap-select">
                        <select onChange={this.onAvailToChange.bind(this)} value={this.state.unavailsAfter ? this.state.unavailsAfter : DEFAULT_AVAIL_TO}>
                        {listAvailTo && listAvailTo.map(option => <option key={option.key} value={option.key}>{option.value}</option>)}
                        </select>
                    </div>
                </div>
                </div>
                <div className="space m clear" />
                <a className="btn btn-primary btn--m" onClick={this.onUpdateAvailsClick.bind(this)}>Lưu thay đổi</a>
                </div>
            content = <div className="content">
                <Modal
                    show={this.state.isShowPriceSettingBox}
                    onHide={this.hidePriceSettingBox.bind(this)}
                    dialogClassName="modal-sm modal-dialog"
                >
                    <Modal.Header closeButton={true} closeLabel={""}>
                        <Modal.Title>{!isShowTripInfo ? "Tuỳ chỉnh giá" : "Thông tin chuyến"}</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <div className="form-default form-s">
                            <label className="custom-radio custom-control">
                                <input className="custom-control-input" type="radio"
                                    name="setting-mode-in-price"
                                    value={"price"}
                                    checked={!isShowTripInfo}
                                    onChange={() => this.toggleTripInfo(false)} />
                                <span className="custom-control-indicator"></span>
                                <span className="custom-control-description">Tùy chỉnh giá</span>
                            </label>
                            <label className="custom-radio custom-control">
                                <input className="custom-control-input" type="radio"
                                    name="setting-mode-in-price"
                                    value={"trip"}
                                    checked={isShowTripInfo}
                                    onChange={() => this.toggleTripInfo(true)} />
                                <span className="custom-control-indicator"></span>
                                <span className="custom-control-description">Thông tin chuyến</span>
                            </label>
                        </div>
                        <div className="space m" />
                        {!isShowTripInfo && <div className="form-default form-s">
                            <p className="notes">Nhập giá tuỳ chỉnh cho ngày này. Nhập 0 nếu muốn dùng giá mặc định.</p>
                            <div className="line-form">
                                <div className="wrap-input-label d-flex">
                                    <div className="wrap-input">
                                        <input type="text" value={this.state.customPrice / 1000} onChange={this.onCustomPriceRangeChange.bind(this)} />
                                    </div>
                                    <span className="phay"> K</span>
                                </div>
                            </div>
                            <p className="notes">Giá đề xuất: {formatPrice(this.state.setting.priceDailyRecommend)}</p>
                            {this.state.err < commonErr.SUCCESS && this.state.updateErrMsg !== "" && <p style={{ color: "red" }}>{this.state.updateErrMsg}</p>}
                            <div className="clear" />
                            <div className="space m" />
                            <p className="textAlign-center has-more-btn">
                                <a className="btn btn-primary btn--m" type="button" onClick={this.confirmPriceSetting.bind(this)}>Xác nhận</a>
                            </p>
                        </div>}
                        {isShowTripInfo && <div className="form-default form-s">
                            <TripDetailBox
                                activeCar={this.state.car}
                                activeDay={this.state.activeDay} />
                        </div>}
                    </Modal.Body>
                </Modal>
                <Modal
                    show={this.state.isShowPriceRepeatSettingBox}
                    onHide={this.hidePriceRepeatSettingBox.bind(this)}
                    dialogClassName="modal-sm modal-dialog"
                >
                    <Modal.Header closeButton={true} closeLabel={""}>
                        <Modal.Title>Tuỳ chỉnh giá lặp lại</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <div className="form-default form-s">
                            <p className="notes">Nhập giá vào ngày trong tuần tương ứng. Nhập 0 nếu muốn dùng giá mặc định.</p>
                            <p className="notes">Giá đề xuất: {formatPrice(this.state.setting.priceDailyRecommend)}</p>
                            {this.state.priceRepeat.map((weekday, i) => <div key={weekday.weekday}>
                                <div className="space m" />
                                <div className="line-form">
                                    <div className="wrap-input-label d-flex">
                                        <label className="label-inline"> {weekday.weekday === 7 ? "CN" : `Thứ ${weekday.weekday + 1}`}</label>
                                        <div className="wrap-input">
                                            <input type="text" placeholder="Dùng giá mặc định" id={`priceRepeat_${i}`} value={weekday.price / 1000} onChange={this.onCustomPriceRepeatChange.bind(this)} />
                                        </div>
                                        <span className="phay"> K</span>
                                    </div>
                                </div>
                            </div>)}
                            {this.state.err < commonErr.SUCCESS && this.state.updateErrMsg !== "" && <p style={{ color: "red" }}>{this.state.updateErrMsg}</p>}
                            <div className="line" />
                            <p className="notes">Không chọn nếu muốn chu kì lặp không giới hạn</p>
                            <div className="squaredFour have-label">
                                <input id="is-limit-repeat-price" type="checkbox" checked={this.state.isLimitRepeatPrice} onChange={this.onLimitRepeatPriceChange.bind(this)} />
                                <label htmlFor="is-limit-repeat-price"> Giới hạn thời gian lặp lại</label>
                            </div>
                            <div className="space m" />
                            {this.state.isLimitRepeatPrice && <div className="line-form">
                                <div className="wrap-input datepicker">
                                    <DatePicker
                                        selected={moment(this.state.priceRepeatEndDate)}
                                        timeFormat="HH:mm"
                                        dateFormat="DD/MM/YYYY"
                                        onChange={this.onPriceRepeatEndDayChange.bind(this)} />
                                </div>
                            </div>}
                            <div className="clear" />
                            <div className="space m" />
                            <p className="textAlign-center has-more-btn">
                                <a className="btn btn-primary btn--m" disabled={this.state.err < commonErr.SUCCESS ? true : false} type="button" onClick={this.confirmPriceRepeatSetting.bind(this)}>Xác nhận</a>
                            </p>
                        </div>
                    </Modal.Body>
                </Modal>
                <Modal
                    show={this.state.isShowUnavailsRepeatSettingBox}
                    onHide={this.hideUnavailsRepeatSettingBox.bind(this)}
                    dialogClassName="modal-sm modal-dialog"
                >
                    <Modal.Header closeButton={true} closeLabel={""}>
                        <Modal.Title>Tuỳ chỉnh lịch bận lặp lại</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <div className="form-default form-s">
                            <p className="notes">Đánh dấu vào ngày bận</p>
                            {this.state.unavailsRepeat.map((weekday, i) => <div key={weekday.weekday}>
                                <div className="squaredFour have-label">
                                    <input id={`unavailsRepeat_${i}`} type="checkbox" checked={weekday.unavail === true} onChange={this.onUnavailsRepeatChange.bind(this)} />
                                    <label htmlFor={`unavailsRepeat_${i}`}> {weekday.weekday === 7 ? "CN" : `Thứ ${weekday.weekday + 1}`}</label>
                                </div>
                                <div className="space m" />
                            </div>)}
                            <div className="line"></div>
                            <p className="notes">Không chọn nếu muốn chu kì lặp không giới hạn</p>
                            <div className="squaredFour have-label">
                                <input id="is-limit-repeat-unavails" type="checkbox" checked={this.state.isLimitRepeatUnavails} onChange={this.onLimitUnavailsChange.bind(this)} />
                                <label htmlFor="is-limit-repeat-unavails"> Giới hạn thời gian lặp lại</label>
                            </div>
                            <div className="space m" />
                            {this.state.isLimitRepeatUnavails && <div className="line-form">
                                <div className="wrap-input datepicker">
                                    <DatePicker
                                        selected={moment(this.state.unavailsRepeatEndDate)}
                                        timeFormat="HH:mm"
                                        dateFormat="DD/MM/YYYY"
                                        onChange={this.onUnavailsRepeatEndDayChange.bind(this)} />
                                </div>
                            </div>}
                            <div className="clear" />
                            <div className="space m" />
                            <p className="textAlign-center has-more-btn">
                                <a className="btn btn-primary btn--m" type="button" onClick={this.confirmUnavailsRepeatSetting.bind(this)}>Xác nhận</a>
                            </p>
                        </div>
                    </Modal.Body>
                </Modal>
                
                <MessageBox show={this.state.err <= commonErr.SUCCESS && this.state.errMsg !== "" && this.state.isShowMsgBox} error={this.state.err} hideModal={this.hideMessageBox.bind(this)} message={this.state.errMsg} />
                <div className="content-container">
                    <h3 className="title" style={{display: 'inline-block'}}>Thiết lập thời gian cho thuê </h3>
                    {settingUnavailsRangeContent}                       
                                    
                    <div className="line"></div>
                    <h3 className="title">Lịch xe </h3>
                    <div className="line-form line-radio">
                        <div className="wrap-input">
                            <label className="custom-radio custom-control">
                                <input className="custom-control-input" type="radio"
                                    name="setting-mode"
                                    value={settingModes.price}
                                    checked={this.state.settingMode === settingModes.price}
                                    onChange={() => this.setSettingMode(settingModes.price)} />
                                <span className="custom-control-indicator"></span>
                                <span className="custom-control-description">Tùy chỉnh giá</span>
                            </label>
                            <i className="ic ic-clock-repeat" onClick={() => this.setPriceRepeat()}></i>
                        </div>
                        <div className="wrap-input">
                            <label className="custom-radio custom-control">
                                <input className="custom-control-input" type="radio"
                                    name="setting-mode"
                                    value={settingModes.calendar}
                                    checked={this.state.settingMode === settingModes.calendar}
                                    onChange={() => this.setSettingMode(settingModes.calendar)} />
                                <span className="custom-control-indicator"></span>
                                <span className="custom-control-description">Thiết lập lịch bận </span>
                            </label>
                            <i className="ic ic-clock-repeat" onClick={() => this.setUnavailsRepeat()}></i>
                        </div>
                    </div>
                    <div className="space m clear" />
                    <ul className="list-status">
                        <li className="status-3"><span>Giá mặc định</span></li>
                        <li className="status-6"><span>Lặp lại tuần</span></li>
                        <li className="status-1"><span>Ngày trống</span></li>
                        <li className="status-9"><span>Chờ đặt cọc</span></li>
                        <li className="status-4"><span>Giá tùy chỉnh</span></li>
                        <li className="status-5"><span>Xe đã đặt</span></li>
                        <li className="status-2"><span>Ngày bận</span></li>
                        <li className="status-8"><span>Chờ duyệt</span></li>
                    </ul>
                    {months.map((m, i) => {
                        return <div key={i} className="calendar-container">
                            <div className="title-calendar">
                                <span className="t-ins">{`${moment(m.t).format("MMMM")}`}</span>
                            </div>
                            <ol className="day-names">
                                <li>Thứ 2</li>
                                <li>Thứ 3</li>
                                <li>Thứ 4</li>
                                <li>Thứ 5</li>
                                <li>Thứ 6</li>
                                <li>Thứ 7</li>
                                <li><strong>CN&nbsp;</strong></li>
                            </ol>
                            <ol className="days">
                                {m.days.map((day, index) => {
                                    if (day.isOutside) {
                                        return <li key={index} className="outside "></li>
                                    } if (day.isPast) {
                                        return <li key={index} className={`outside`}>
                                            <div className="date">{moment(day.ts).format("DD")}</div>
                                        </li>
                                    } else {
                                        var isShowPrice = !day.isUnavail && day.bookedWidth !== 100;
                                        var tripsReqCont;
                                        const tripsReq = day.tripsReq;
                                        if (tripsReq.length > 0) {
                                            tripsReqCont = tripsReq.map((trip, i) => {
                                                const className = `${trip.tripInfo.isApproved ? "deposit" : "req"}-${day.mapTripsMaxLevel[trip.tripInfo.tripId]}`;
                                                const style = { width: `${trip.width}%`, left: `${trip.left}%` }
                                                return <span key={i} className={className} style={style}>{trip.isStart && <i className="ic ic-sm-flat"></i>}</span>;
                                            })
                                        }
                                        return <li key={index} className={`${day.isToday ? "current-date " : ""}${day.isUnavai ? "gray " : ""}`} onClick={() => this.onDayItemClick(day)}>
                                            <div className="date">{moment(day.ts).format("DD")}</div>
                                            {(isShowPrice && day.price !== this.state.priceDaily) && <div className="event">{formatPrice(day.price)}</div>}
                                            {(isShowPrice && day.price === this.state.priceDaily) && <div className="price-default">{formatPrice(this.state.priceDaily)}</div>}
                                            {tripsReqCont}
                                            {day.isBooked && <span className="full-in-calendar" style={{ width: `${day.bookedWidth}%`, left: `${day.bookedLeft}%` }} />}
                                            <div className="label-in-calendar">
                                                {day.isInstant && <i className="ic ic-clock-repeat"></i>}
                                                {day.isInstant && <i className="ic ic-sm-thunderbolt"></i>}
                                            </div>
                                        </li>
                                    }
                                })}
                            </ol>
                            <div className="space m" />
                        </div>
                    })}
                </div>
            </div>
        }
        return content;
    }
} 