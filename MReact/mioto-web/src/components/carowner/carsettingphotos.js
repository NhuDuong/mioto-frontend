import React from "react"
import DragSortableList from 'react-drag-sortable'
import ReactCrop, { makeAspectCrop } from 'react-image-crop'
import { Modal } from "react-bootstrap"

import { getCarDetail, uploadCarPhoto, removeCarPhoto, updateCar } from "../../model/car"
import { commonErr } from "../common/errors"
import ImageUpload from "../common/imageupload"
import { LoadingInline } from "../common/loading"
import { MessageBox } from "../common/messagebox"

import 'react-image-crop/dist/ReactCrop.css'

export default class CarSettingPhotos extends React.Component {
    constructor() {
        super();
        this.state = {
            err: commonErr.INNIT,
            errMsg: "",
            isDragToSort: false,

            //crop
            crop: {
                x: 0,
                y: 0
            },
            maxHeight: 300
        }

        this.removeCarPhoto = this.removeCarPhoto.bind(this);
    }

    init(car) {
        getCarDetail(car.id).then(resp => {
            const photos = car.photos;
            this.setState({
                err: resp.data.error,
                errMsg: resp.data.errorMessage,
                car: car,
                photos: photos
            })
        })
    }

    componentDidMount() {
        const car = this.props.car;
        this.init(car);
    }

    componentWillReceiveProps(props) {
        const car = props.car;
        this.init(car);
    }

    hideMessageBox() {
        this.setState({
            uploadErr: commonErr.INNIT,
            uploadErrMsg: ""
        });
    }

    showResizeBox() {
        this.setState({
            isShowResizeBox: true
        });
    }

    hideResizeBox() {
        this.setState({
            isShowResizeBox: false
        });
    }

    hanldePhotoInputChange(photos) {
        const reader = new FileReader();

        reader.onload = (e2) => {
            this.setState({
                dataUrl: e2.target.result,
                isShowResizeBox: true
            });
        };

        reader.readAsDataURL(photos[0]);
    }

    dataURItoBlob(dataURI) {
        // convert base64/URLEncoded data component to raw binary data held in a string
        var byteString;
        if (dataURI.split(',')[0].indexOf('base64') >= 0)
            byteString = atob(dataURI.split(',')[1]);
        else
            byteString = unescape(dataURI.split(',')[1]);
        // separate out the mime component
        var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

        // write the bytes of the string to a typed array
        var ia = new Uint8Array(byteString.length);
        for (var i = 0; i < byteString.length; i++) {
            ia[i] = byteString.charCodeAt(i);
        }

        return new Blob([ia], { type: mimeString });
    }

    onUploadBtnClick() {
        this.setState({
            uploadErr: commonErr.LOADING,
            isShowResizeBox: false
        });

        const photo = this.dataURItoBlob(this.state.resizeImage);

        uploadCarPhoto(this.state.car.id, photo).then(resp => {
            if (resp.data.data && resp.data.data.id) {
                var photos = [];
                if (this.state.photos) {
                    photos = photos.concat(this.state.photos);
                }
                photos = photos.concat(resp.data.data);
                this.setState({
                    uploadErr: resp.data.error,
                    photos: photos
                });
            } else {
                this.setState({
                    uploadErr: resp.data.error,
                    uploadErrMsg: resp.data.errorMessage
                });
            }
        });
    }

    removeCarPhoto(photo) {
        var photos = [];
        if (this.state.photos) {
            photos = photos.concat(this.state.photos);
        }

        photos = photos.filter(p => {
            return p.id !== photo.id
        });

        if (photos.length >= 1) {
            this.setState({
                photos: photos
            });
            removeCarPhoto(this.state.car.id, photo.id);
        } else {
            this.setState({
                uploadErr: commonErr.FAIL,
                uploadErrMsg: "Xe phải có ít nhất 1 hình."
            });
        }
    }

    onSort(sortedList) {
        sortedList = sortedList.filter(function (item) {
            return item.id !== 0
        })

        updateCar(this.state.car.id, {
            photoTmpIds: sortedList.slice().reverse()
        })

        const photos = []
        for (var i = 0; i < sortedList.length; ++i) {
            for (var j = 0; j < this.state.photos.length; ++j) {
                if (sortedList[i].id === this.state.photos[j].id) {
                    photos.push(this.state.photos[j]);
                    break;
                }
            }
        }

        this.setState({
            photos: photos
        });
    }

    onImageLoaded = (image) => {
        const crop = makeAspectCrop({
            x: 0,
            y: 0,
            aspect: 4 / 3,
            width: 80,
        }, image.naturalWidth / image.naturalHeight);

        var bufferCanvas = document.createElement('canvas');
        var bufferContext = bufferCanvas.getContext('2d');
        bufferCanvas.width = image.naturalWidth;
        bufferCanvas.height = image.naturalHeight;
        bufferContext.drawImage(image, 0, 0);

        const width = crop.width * image.naturalWidth / 100;
        const height = crop.height * image.naturalHeight / 100;

        var tnCanvas = document.createElement('canvas');
        var tnCanvasContext = tnCanvas.getContext('2d');
        tnCanvas.width = width;
        tnCanvas.height = height;

        tnCanvasContext.drawImage(bufferCanvas, crop.x, crop.y, width, height, 0, 0, width, height);

        this.setState({
            crop: crop,
            image,
            resizeImage: tnCanvas.toDataURL("image/jpeg")
        });
    }

    onCropComplete = (crop, pixelCrop) => {
        const image = this.state.image;

        var bufferCanvas = document.createElement('canvas');
        var bufferContext = bufferCanvas.getContext('2d');
        bufferCanvas.width = image.naturalWidth;
        bufferCanvas.height = image.naturalHeight;
        bufferContext.drawImage(image, 0, 0);

        var tnCanvas = document.createElement('canvas');
        var tnCanvasContext = tnCanvas.getContext('2d');
        tnCanvas.width = pixelCrop.width;
        tnCanvas.height = pixelCrop.height;

        tnCanvasContext.drawImage(bufferCanvas, pixelCrop.x, pixelCrop.y, pixelCrop.width, pixelCrop.height, 0, 0, pixelCrop.width, pixelCrop.height);

        this.setState({
            resizeImage: tnCanvas.toDataURL("image/jpeg")
        });
    }

    onCropChange = (crop) => {
        if (crop.width < 10) {
            return;
        }

        this.setState({ crop });
    }

    toggleDragToSort() {
        this.setState({
            isDragToSort: !this.state.isDragToSort
        })
    }

    render() {
        var content;
        if (this.state.err === commonErr.INNIT) {
            content = <LoadingInline />
        } else {
            var listGrid = [{
                content: <div className="obj-photo">
                    <div className="fix-img">
                        {!this.state.isShowResizeBox && this.state.uploadErr !== commonErr.LOADING && <ImageUpload withPreview={false} withIcon={false} onChange={this.hanldePhotoInputChange.bind(this)} className="form-control" />}
                        {this.state.uploadErr === commonErr.LOADING && <LoadingInline />}
                    </div>
                </div>,
                id: 0
            }];

            const photos = this.state.photos;
            if (photos) {
                for (var i = 0; i < photos.length; ++i) {
                    const photo = photos[i];
                    listGrid.push({
                        content: <div key={photo.id} className="obj-photo">
                            <div className="fix-img">
                                <img src={photo.thumbUrl} alt="Mioto - Thuê xe tự lái" />
                            </div>
                            <a className="func-remove" onClick={() => this.removeCarPhoto(photo)} onTouchEnd={() => this.removeCarPhoto(photo)}><i className="ic ic-remove"></i></a>
                        </div>,
                        id: photo.id
                    });
                }
            }

            content = <div className="content">
                <div className="content-container">
                    <p className="position-relative pos-1">
                        <div className="switch-on-off">
                            <input className="switch-input" id="cb-instant-booking" type="checkbox" checked={this.state.isDragToSort} onClick={this.toggleDragToSort.bind(this)} />
                            <label className="switch-label" for="cb-instant-booking"></label>
                            <span className="note">Cho phép kéo thả để sắp xếp thứ tự hình ảnh.</span>
                        </div>
                    </p>
                    <div className="space m"></div>
                    <div className="list-photos settings">
                        {this.state.isDragToSort && <div className="list-thumb">
                            <DragSortableList items={listGrid} dropBackTransitionDuration={1} onSort={this.onSort.bind(this)} type="grid" />
                        </div>}
                        {!this.state.isDragToSort && <div className="list-thumb">
                            <div className="draggable">
                                <div className="obj-photo">
                                    <div className="fix-img no-drag">
                                        {!this.state.isShowResizeBox && this.state.uploadErr !== commonErr.LOADING && <ImageUpload withPreview={false} withIcon={false} onChange={this.hanldePhotoInputChange.bind(this)} className="form-control" />}
                                        {this.state.uploadErr === commonErr.LOADING && <LoadingInline />}
                                    </div>
                                </div>
                            </div>
                            {this.state.photos.map(photo => <div key={photo.id} className="draggable">
                                <div className="obj-photo">
                                    <div className="fix-img no-drag">
                                        <img src={photo.thumbUrl} alt="Mioto - Thuê xe tự lái" />
                                    </div>
                                    <a className="func-remove" onClick={() => this.removeCarPhoto(photo)} onTouchEnd={() => this.removeCarPhoto(photo)}><i className="ic ic-remove"></i></a>
                                </div>
                            </div>)}
                        </div>}
                        <MessageBox show={this.state.uploadErr < commonErr.SUCCESS} hideModal={this.hideMessageBox.bind(this)} message={this.state.uploadErrMsg} />
                    </div>
                    <Modal show={this.state.isShowResizeBox}
                        onHide={this.hideResizeBox.bind(this)}
                        dialogClassName="modal-sm modal-dialog">
                        <Modal.Header closeButton={true} closeLabel={""}>
                            <Modal.Title>Chuẩn hoá kích thước hình</Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                            <div className="form-default form-s" style={{ textAlign: "center" }}>
                                <p className="notes">Mioto chỉ chấp nhận hình với tỉ lệ 4:3. Vui lòng điều chỉnh để có được chất lượng hiển thị tốt nhất.</p>
                                {this.state.dataUrl && <ReactCrop
                                    {...this.state}
                                    src={this.state.dataUrl}
                                    onImageLoaded={this.onImageLoaded}
                                    onComplete={this.onCropComplete}
                                    onChange={this.onCropChange}
                                />}
                                {this.state.resizeImage && <div className="hide-on-med-and-down">
                                    <div className="space m"></div>
                                    <div className="space m"></div>
                                    <div className="line"></div>
                                    <div className="space m"></div>
                                    <div className="ReactCrop">
                                        <img className="ReactCrop__image" src={this.state.resizeImage} alt="Mioto - Thuê xe tự lái" />
                                    </div>
                                </div>}
                                <div className="space m"></div>
                                <div className="space m"></div>
                                <a className="btn btn-primary btn--m" onClick={this.onUploadBtnClick.bind(this)}>Đăng hình</a>
                            </div>
                        </Modal.Body>
                    </Modal>
                </div>
            </div>
        }
        return content;
    }
} 