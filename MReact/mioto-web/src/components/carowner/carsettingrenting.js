import React from "react"

import { getCarSetting, updateCar } from "../../model/car"
import { getLoggedProfile } from "../../model/profile"
import { commonErr } from "../common/errors"
import { Modal } from "react-bootstrap"
import { formatPrice, DEFAULT_INSTANT_RANGE_FROM, DEFAULT_INSTANT_RANGE_TO, listInstantRangeFrom, listInstantRangeTo, convertSecondToTime } from "../common/common"
import { MessageBox, MessageLine } from "../common/messagebox"
import ReactSimpleRange from "react-simple-range"
import { LoadingInline, LoadingOverlay } from "../common/loading"
import CarMapLocation from "../common/carmaplocation"
import LocationPikcer from "../common/locationpicker"

export default class CarSettingRenting extends React.Component {
    constructor() {
        super();

        this.state = {
            err: commonErr.INNIT,
            errMsg: "",
            updateErrMsg: "",
						isShowConfirmInstant: false,
        }

        this.close = this.close.bind(this);
    }

    init(car) {
        getCarSetting({ carId: car.id }).then((settingResp) => {
            const setting = settingResp.data.data;
            var i, j;
            if (setting) {
                const papers = [];
							
							if (car.requiredPapersSpecific === 0) {
								getLoggedProfile().then((profileResp) => {
									const profile = profileResp.data.data.profile;
											if (profile.requiredCarPapers) {
												for (j = 0; j < profile.requiredCarPapers.length; ++j) {
													var requiredPaper = {
														id: profile.requiredCarPapers[j].id,
														name: profile.requiredCarPapers[j].name,
														logo: profile.requiredCarPapers[j].logo,
														groupId: profile.requiredCarPapers[j].groupId,
														edit: profile.requiredCarPapers[j].edit === 1,
														groupRequired: profile.requiredCarPapers[j].groupRequired === 1,
														checked: profile.requiredCarPapers[j].val === 1
													}
													papers.push(requiredPaper)
												}
												
											}
								
											
                        if (profile) {
                            this.setState({
                                err: profileResp.data.error,
                                errMsg: profileResp.data.errorMessage,
                                car: car,
                                isInstantly: car.instantEnable === 1,
                                instantRangeFrom: car.instantRangeFrom > 0 ? car.instantRangeFrom : DEFAULT_INSTANT_RANGE_FROM,
                                instantRangeTo: car.instantRangeTo > 0 ? car.instantRangeTo : DEFAULT_INSTANT_RANGE_TO,
                                address: car.locationAddr,
                                lat: car.location.lat,
                                lng: car.location.lon,
                                isDelivery: car.deliveryEnable === 1,
                                deliveryLimited: car.deliveryRadius,
                                deliveryPrice: car.deliveryPrice,
                                isLimited: car.limitEnable === 1,
                                limited: car.limitKM,
                                limitedPrice: car.limitPrice,
                                notes:  setting.policiesSpecific === 1 ? setting.policies : profile.policies,
                                requiredPapersSpecific: car.requiredPapersSpecific === 1,
                                requiredPapers: papers,
                                // requiredPapersOther: profile.requiredCarPapersOther,
																setting: setting,
																isPoliciesSpecific: setting.policiesSpecific === 1
                            })
                        }
                    });
                } else {
                    if (setting.allRequiredPapers) {
                        for (i = 0; i < setting.allRequiredPapers.length; ++i) {
                            var requiredPaper = {
                                id: setting.allRequiredPapers[i].id,
                                name: setting.allRequiredPapers[i].name,
                                logo: setting.allRequiredPapers[i].logo,
                                groupId: setting.allRequiredPapers[i].groupId,
                                edit: setting.allRequiredPapers[i].edit === 1,
                                groupRequired: setting.allRequiredPapers[i].groupRequired === 1,
                                checked: setting.allRequiredPapers[i].val === 1
                            }
                            papers.push(requiredPaper)
                        }
                        for (i = 0; i < papers.length; ++i) {
                            for (j = 0; j < car.requiredPapers.length; ++j) {
                                if (papers[i].id === car.requiredPapers[j].id && car.requiredPapers[j].val === 1) {
                                    papers[i].checked = true;
                                }
                            }
                        }
                    }
                    this.setState({
                        err: settingResp.data.error,
                        errMsg: settingResp.data.errorMessage,
                        car: car,
                        isInstantly: car.instantEnable === 1,
                        instantRangeFrom: car.instantRangeFrom > 0 ? car.instantRangeFrom : DEFAULT_INSTANT_RANGE_FROM,
                        instantRangeTo: car.instantRangeTo > 0 ? car.instantRangeTo : DEFAULT_INSTANT_RANGE_TO,
                        address: car.locationAddr,
                        lat: car.location.lat,
                        lng: car.location.lon,
                        isDelivery: car.deliveryEnable === 1,
                        deliveryLimited: car.deliveryRadius,
                        deliveryPrice: car.deliveryPrice,
                        isLimited: car.limitEnable === 1,
                        limited: car.limitKM,
                        limitedPrice: car.limitPrice,
                        notes: setting.policiesSpecific === 1 ? setting.policies : car.notes,
                        requiredPapersSpecific: car.requiredPapersSpecific === 1,
                        requiredPapers: papers,
                        // requiredPapersOther: car.requiredPapersOther,
												setting: setting,
												isPoliciesSpecific: setting.policiesSpecific === 1
                    })
                }
            } else {
                this.setState({
                    err: settingResp.data.error,
                    errMsg: settingResp.data.errorMessage
                })
            }
        })
    }

    componentDidMount() {
        const car = this.props.car;
        this.init(car);
    }

    componentWillReceiveProps(props) {
        const car = props.car;
        this.init(car);
    }

    onAddressLocationUpdate(address, location) {
        this.setState({
            address: address,
            lat: location.lat,
            lng: location.lng
        });
    }

    onAddressFocus(event) {
        if (!this.state.address || this.state.address === "") {
            this.toogleLocationPicker();
            event.target.blur();
        }
    }

    onAddressChange(event) {
        this.setState({
            address: event.target.value
        });
        if (!event.target.value || event.target.value === "") {
            this.toogleLocationPicker();
            event.target.blur();
        }
    }

    toogleLocationPicker() {
        this.setState({
            isShowLocationPicker: !this.state.isShowLocationPicker
        });
    }

    toggleInstantly() {
        var instant = !this.state.isInstantly;
        this.setState({
            isInstantly: instant
        });
        if(instant){
            this.setState({
                instantRangeFrom: (!this.state.instantRangeFrom  || this.state.instantRangeFrom === 0) ? DEFAULT_INSTANT_RANGE_FROM : this.state.instantRangeFrom,
                instantRangeTo: (!this.state.instantRangeTo  || this.state.instantRangeTo === 0) ? DEFAULT_INSTANT_RANGE_TO : this.state.instantRangeTo,
            });
        }
    }

    onInstantRangeFromChange(event){
        this.setState({
            instantRangeFrom: event.target.value
        });
    }

    onInstantRangeToChange(event){
        this.setState({
            instantRangeTo: event.target.value
        });
    }

    toggleDelivery() {
        this.setState({
            isDelivery: !this.state.isDelivery
        });
    }

    onDeliveryLimitedChange(event) {
        this.setState({
            deliveryLimited: event.target.value
        });
    }

    onDeliveryPriceRangeChange(range) {
        this.setState({
            deliveryPrice: range.value
        });
    }

    toggleLimited() {
        this.setState({
            isLimited: !this.state.isLimited
        });
    }

    onLimitedChange(event) {
        this.setState({
            limited: event.target.value
        });
    }

    onLimitedPriceRangeChange(range) {
        this.setState({
            limitedPrice: range.value
        });
    }

    onPolicyChange(event) {
        this.setState({
            notes: event.target.value
        });
		}
	
		togglePoliciesSpecific(){
			const policiesSpecific = !this.state.isPoliciesSpecific;

			if(policiesSpecific){
					getCarSetting({ carId: this.state.car.id }).then((settingResp) => {
							const setting = settingResp.data.data;
							this.setState({
									notes: setting.policies,
									isPoliciesSpecific: policiesSpecific
							})
					})
			} else {
					getLoggedProfile().then(profileResp => {
							const profile = profileResp.data.data.profile;
							this.setState({
									notes: profile.policies,
									isPoliciesSpecific: policiesSpecific
							})
					})
			}
	}

    toggleRequiredPapersSpecific() {
			const required = !this.state.requiredPapersSpecific;
		
        var papers = [];
				if (required) {
				getCarSetting({ carId: this.state.car.id }).then((settingResp) => {
					const setting = settingResp.data.data;
					var i, j;
					if (setting) {
						if (setting.allRequiredPapers) {
							for (var i = 0; i < setting.allRequiredPapers.length; ++i){
								var requiredPaper = {
									id: setting.allRequiredPapers[i].id,
									name: setting.allRequiredPapers[i].name,
                                    logo: setting.allRequiredPapers[i].logo,
                                    groupId: setting.allRequiredPapers[i].groupId,
                                    edit: setting.allRequiredPapers[i].edit === 1,
                                    groupRequired: setting.allRequiredPapers[i].groupRequired === 1,
									checked: setting.allRequiredPapers[i].val === 1
								}
								papers.push(requiredPaper)
							}
						}
					}
					if (this.state.car.requiredPapers) {
						for (var i = 0; i < papers.length; ++i)
							for (var j = 0; j < this.state.car.requiredPapers.length; ++j) {
								if (papers[i].id === this.state.car.requiredPapers[j].id && this.state.car.requiredPapers[j].val === 1) {
									papers[i].checked = true;
									break;
							}
						}		
					}
					this.setState({
							requiredPapersSpecific: required,
							requiredPapers: papers,
							requiredPapersOther: this.state.car.requiredPapersOther
					});
				})          
			} else {
            getLoggedProfile().then(profileResp => {
                const profile = profileResp.data.data.profile;
                if (profile.requiredCarPapers) {
                   
                        for (var j = 0; j < profile.requiredCarPapers.length; ++j) {
													var requiredPaper = {
														id: profile.requiredCarPapers[j].id,
														name: profile.requiredCarPapers[j].name,
                                                        logo: profile.requiredCarPapers[j].logo,
                                                        groupId: profile.requiredCarPapers[j].groupId,
                                                        edit: profile.requiredCarPapers[j].edit === 1,
                                                        groupRequired: profile.requiredCarPapers[j].groupRequired === 1,
														checked: profile.requiredCarPapers[j].val === 1
													}
													papers.push(requiredPaper)
                        }
                    
                }
                this.setState({
                    requiredPapersSpecific: required,
                    requiredPapers: papers,
                    requiredPapersOther: profile.requiredCarPapersOther
                });
            });
        }
    }

    onRequiredPapersChange(event) {
        const papers = this.state.requiredPapers.slice();
        for (var i = 0; i < papers.length; ++i) {
            if(!papers[i].edit){
                continue;
            }
            if(papers[i].groupRequired && papers[i].groupId === event.target.name){
                if(papers[i].id !== event.target.value){
                    papers[i].checked = false;
                }
            }
            if (papers[i].id === event.target.value) {
                papers[i].checked = event.target.checked;
            }
        }
        // if(event.target.value === "hk" && event.target.checked){
        //     for (var i = 0; i < papers.length; ++i) {
        //         if (papers[i].id === "pp") {
        //             papers[i].checked = false;
        //             break;
        //         }
        //     }
        // } else if(event.target.value === "pp" && event.target.checked){
        //     for (var i = 0; i < papers.length; ++i) {
        //         if (papers[i].id === "hk") {
        //             papers[i].checked = false;
        //             break;
        //         }
        //     }
        // } 
        this.setState({
            requiredPapers: papers
        });
    }

    // onRequiredPapersOtherChange(event) {
    //     this.setState({
    //         requiredPapersOther: event.target.value
    //     });
    // }


	
    hideMessageBox() {
        this.setState({
            updateErrMsg: ""
        });
    }

    callingUpdateCarInfo(){
        if(this.state.isInstantly){
            this.setState({
                isShowConfirmInstant : true,
            })
        } else {
            this.updateCarInfo();
        }
    }

    close(){
        this.setState({
            isShowConfirmInstant: false
        })
    }

    updateCarInfo() {
        const address = this.state.address;
        const lat = this.state.lat;
        const lng = this.state.lng;

        const instant = this.state.isInstantly;
        const instantRangeFrom = this.state.instantRangeFrom;
        const instantRangeTo = this.state.instantRangeTo;

        const deliveryEnable = this.state.isDelivery;
        const deliveryRadius = this.state.deliveryLimited;
        const deliveryPrice = this.state.deliveryPrice;

        const limitEnable = this.state.isLimited;
        const limitKM = this.state.limited;
        const limitPrice = this.state.limitedPrice;

        const notes = this.state.notes;
				const requiredPapersSpecific = this.state.requiredPapersSpecific;
			
				const policiesSpecific = this.state.isPoliciesSpecific;
			

        var requiredPapers = "";
        var requiredPapersOther = "";
        if (requiredPapersSpecific) {
            for (var i = 0; i < this.state.requiredPapers.length; ++i) {
                const paper = this.state.requiredPapers[i];
                if (paper.checked === true) {
                    if (requiredPapers === "") {
                        requiredPapers = paper.id;
                    } else {
                        requiredPapers += ";" + paper.id;
                    }
                }
            }
            requiredPapersOther = this.state.requiredPapersOther;
        }

        if (!address || address === "" || !lat || lat === 0 || !lng || lng === 0) {
            this.setState({
                err: commonErr.FAIL,
                updateErrMsg: "Địa chỉ không hợp lệ."
            });
        } else {
            this.setState({
                err: commonErr.LOADING
            });
            updateCar(this.state.car.id, {
                instant: instant,
                instantRangeFrom: instantRangeFrom,
                instantRangeTo: instantRangeTo,
                address: address,
                lat: lat,
                lon: lng,
                deliveryEnable: deliveryEnable,
                deliveryRadius: deliveryRadius,
                deliveryPrice: deliveryPrice,
                limitEnable: limitEnable,
                limitKM: limitKM,
                limitPrice: limitPrice,
                notes: encodeURI(notes),
                requiredPapersSpecific: requiredPapersSpecific,
                requiredPapers: requiredPapers,
								requiredPapersOther: encodeURI(requiredPapersOther),
								policiesSpecific: policiesSpecific,
            }).then(resp => {
                this.setState({
                    err: resp.data.error,
                    updateErrMsg: resp.data.errorMessage,
										isShowConfirmInstant: false, 
										isPoliciesSpecific: this.state.isPoliciesSpecific
                });
            });
        }
    }

    render() {
				var content;
        if (this.state.err === commonErr.INNIT) {
            content = <LoadingInline />
        } else if (!this.state.car || !this.state.setting) {
            content = <MessageLine message="Không tìm thấy thông tin." />
        } else {
            const deliveryRadiuses = [];
            const limiteds = [];
            var i;

            if (this.state.setting) {
                for (i = 5; i <= this.state.setting.deliveryRadiusMax; i = i + 5) {
                    deliveryRadiuses.push(i);
                }
                for (i = this.state.setting.limitKMMin; i <= this.state.setting.limitKMMax; i = i + 50) {
                    limiteds.push(i);
                }
            }

            var selectInstantRangeContent = <div className="form-default form-instant-booking">
                    <div className="line-form">
                        <label className="label">Giới hạn từ</label>
                        <div className="wrap-select">
                            <select disabled="disabled" value={!this.state.instantRangeFrom ? DEFAULT_INSTANT_RANGE_FROM : this.state.instantRangeFrom} onChange={this.onInstantRangeFromChange.bind(this)}>                           
                                {listInstantRangeFrom && listInstantRangeFrom.map(option => <option key={option.key} value={option.key}>{option.value}</option>)}
                            </select>
                        </div>
                    </div>
                    <div className="line-form">
                        <label className="label">Cho đến</label>
                        <div className="wrap-select">
                            <select value={!this.state.instantRangeTo ? DEFAULT_INSTANT_RANGE_TO : this.state.instantRangeTo} onChange={this.onInstantRangeToChange.bind(this)}>
                                {listInstantRangeTo && listInstantRangeTo.map(option => <option key={option.key} value={option.key}>{option.value}</option>)}
                            </select>
                        </div>
                    </div>
                </div>

            content = <div className="content">
                <Modal
                    show={this.state.isShowConfirmInstant}
                    onHide={this.close}
                    dialogClassName="modal-sm modal-dialog"
                >
                    <Modal.Header closeButton={true} closeLabel={""}>
                        <Modal.Title>Thông báo</Modal.Title>
                    </Modal.Header>
                    <Modal.Body><div className="module-register" style={{ background: "none", padding: "0" }}>
                        <div className="form-default form-s">
                            <div className="textAlign-center">
                                <p style={{ color: '#141414' }}>Bạn có chắc chắn rằng xe của bạn luôn sẵn sàng cho thuê trong khoảng thời gian trên?</p>
                                <p style={{fontSize: '12px', color: '#141414' }}>Bạn cần đảm bảo việc cần cập nhật lịch bận xe thường xuyên, trường hợp hủy chuyến sau khi khách đặt cọc vì chưa cập nhật lịch bận sẽ áp dụng phí hủy chuyến (100% tiền đặt cọc)</p>
                            </div>
                            <div className="space m" />
                            <div className="space m" />
                            <div className="wrap-btn has-2btn">
                                <div className="wr-btn">
                                    <a className="btn btn-secondary btn--m" onClick={this.close}>Bỏ qua</a>
                                </div>
                                <div className="wr-btn">
                                    <a className="btn btn-primary btn--m" onClick={this.updateCarInfo.bind(this)}>Tôi chắc chắn</a>
                                </div>
                            </div>
                        </div>
                    </div></Modal.Body>
                </Modal>
                {this.state.err === commonErr.LOADING && <LoadingOverlay />}
                {this.state.updateErrMsg && this.state.updateErrMsg !== "" && <MessageBox error={this.state.err} message={this.state.updateErrMsg} show={this.state.updateErrMsg && this.state.updateErrMsg !== ""} hideModal={this.hideMessageBox.bind(this)} />}
                <div className="content-container">
                    <div className="form-default">
                        <h3 className="title text-inline">Đặt xe nhanh</h3>
                        <div className="tooltip"> 
                            <i className="ic ic-question-mark"></i>
                            <div className="tooltip-text">Tính năng cho phép khách hàng đặt xe ngay lập tức không cần chủ xe phê duyệt <span> (Phù hợp với các chủ xe không thường xuyên online và kiểm tra điện thoại.) </span>
                            </div>
                        </div>
                        <p className="position-relative pos-1">
                            <div className="switch-on-off" style={{ right: "0", position: "absolute", bottom: "10px" }}>
                                <input className="switch-input" id="cb-instant-booking" type="checkbox" checked={this.state.isInstantly} onClick={this.toggleInstantly.bind(this)} />
                                <label className="switch-label" for="cb-instant-booking"></label>
                            </div>
                        </p>
                        <span className="note marginBottom-s">Tự động đồng ý đối với tất cả yêu cầu thuê xe trong khoảng thời gian cài đặt</span>                       
                        {this.state.isInstantly && selectInstantRangeContent}
                        <div className="line"></div>
                        <div className="space m"></div>
                        <p className="position-relative pos-1">
                            <div className="line-form">
                                <h3 className="title">Địa chỉ xe</h3>
                                <LocationPikcer show={this.state.isShowLocationPicker} onHide={this.toogleLocationPicker.bind(this)} onUpdate={this.onAddressLocationUpdate.bind(this)} address={this.state.address} location={{ lat: this.state.lat, lng: this.state.lng }} />
                                <div className="wrap-input has-ico-search"><i onClick={this.toogleLocationPicker.bind(this)} className="ic ic-map"></i>
                                    <input type="text" value={this.state.address}
                                        onChange={this.onAddressChange.bind(this)}
                                        onFocus={this.onAddressFocus.bind(this)}
                                        placeholder={"Địa chỉ mặc định để giao nhận xe."} />
                                </div>
                            </div>
                            {this.state.address !== "" && this.state.lat !== 0 && this.state.lng !== 0 && <CarMapLocation address={this.state.address} location={{ lat: this.state.lat, lng: this.state.lng }} />}
                        </p>
                        <div className="space m clear "></div>
                        <h3 className="title">Giao xe tận nơi</h3>
                        <p className="position-relative pos-1">
                            <div className="switch-on-off" style={{ right: "0", position: "absolute", bottom: "10px" }}>
                                <input className="switch-input" id="cb-instant-delivery" type="checkbox" checked={this.state.isDelivery} onClick={this.toggleDelivery.bind(this)} />
                                <label className="switch-label" for="cb-instant-delivery"></label>
                            </div>
                        </p>
                        {this.state.isDelivery && <div className="col-1">
                            <div className="line-form end">
                                <label className="label">Quảng đường giao xe tối đa</label>
                                <span className="wrap-select">
                                    <select onChange={this.onDeliveryLimitedChange.bind(this)} value={this.state.deliveryLimited}>
                                        {deliveryRadiuses.map(radius => <option key={radius} value={radius}>{radius}km</option>)}
                                    </select>
                                </span>
                                <span className="note">Quảng đường đề xuất: {this.state.setting.deliveryRadiusRecommend} km</span>
                            </div>
                        </div>}
                        {this.state.isDelivery && <div className="col-2">
                            <div className="line-form end">
                                <label className="label">Phí giao nhận xe cho mỗi km</label>
                                <div className="range-slider">
                                    <ReactSimpleRange
                                        step={1000}
                                        min={0}
                                        max={this.state.setting.deliveryPriceMax}
                                        value={this.state.deliveryPrice}
                                        sliderSize={14}
                                        thumbSize={16}
                                        label={false}
                                        trackColor={"#00a550"}
                                        thumbColor={"#141414"}
                                        onChange={this.onDeliveryPriceRangeChange.bind(this)}
                                    />
                                </div>
                                <span style={{ float: "right", fontSize: "14px" }}>{`${this.state.deliveryPrice > 0 ? `${formatPrice(this.state.deliveryPrice)}` : "Miễn phí"}`}</span>
                                <span className="note">Phí đề xuất: {formatPrice(this.state.setting.deliveryPriceRecommend)}</span>
                            </div>
                        </div>}
                        <div className="space m clear"></div>
                        <div className="line"></div>
                        <h3 className="title">Giới hạn quãng đường</h3>
                        <p className="position-relative pos-1">
                            <div className="switch-on-off" style={{ right: "0", position: "absolute", bottom: "10px" }}>
                                <input className="switch-input" id="cb-instant-limited" type="checkbox" checked={this.state.isLimited} onClick={this.toggleLimited.bind(this)} />
                                <label className="switch-label" for="cb-instant-limited"></label>
                            </div>
                        </p>
                        {this.state.isLimited && <div className="col-1">
                            <div className="line-form end">
                                <label className="label">Quảng đường tối đa (trong 1 ngày)</label>
                                <span className="wrap-select">
                                    <select onChange={this.onLimitedChange.bind(this)} value={this.state.limited}>
                                        {limiteds.map(limited => <option key={limited} value={limited}>{limited}km</option>)}
                                    </select>
                                </span>
                                <span className="note">Quảng đường đề xuất: {this.state.setting.limitKMRecommend} km</span>
                            </div>
                        </div>}
                        {this.state.isLimited && <div className="col-2">
                            <div className="line-form end">
                                <label className="label">Phí vượt giới hạn (tính mỗi km)</label>
                                <ReactSimpleRange
                                    step={1000}
                                    min={1000}
                                    max={this.state.setting.limitPriceMax}
                                    value={this.state.limitedPrice}
                                    sliderSize={14}
                                    thumbSize={16}
                                    label={false}
                                    trackColor={"#00a550"}
                                    thumbColor={"#141414"}
                                    onChange={this.onLimitedPriceRangeChange.bind(this)}
                                />
                                <span style={{ float: "right", fontSize: "14px" }}>{formatPrice(this.state.limitedPrice)}</span>
                                <span className="note">Phí đề xuất: {formatPrice(this.state.setting.limitPriceRecommend)}</span>
                            </div>
                        </div>}
                        <div className="space m clear"></div>
                        <div className="space m"></div>
                    </div>
                    <div className="line"></div>
											<h3 className="title">Điều khoản thuê xe</h3>
											<p className="position-relative pos-1">
                        <div className="switch-on-off" style={{ right: "0", position: "absolute", bottom: "10px" }}>
                            <input className="switch-input" id="cb-policies-spec" type="checkbox" checked={!this.state.isPoliciesSpecific} onClick={this.togglePoliciesSpecific.bind(this)} />
                            <label className="switch-label" for="cb-policies-spec"></label>
                        </div>
                        </p>
                        <p>Thiết lập các yêu cầu khi thuê xe {this.state.car.name}.</p>
                        
                    {!this.state.isPoliciesSpecific && <span className="note">Lưu ý: Xe đang áp dụng thiết lập điều khoản chung. Tắt thiết lập chung nếu muốn tuỳ chỉnh riêng cho xe này</span>}
										{!this.state.isPoliciesSpecific && <div className="space m"></div> }
										<div className="form-default">
                        <div className="line-form end">
                            <textarea className="textarea" disabled={!this.state.isPoliciesSpecific} onChange={this.onPolicyChange.bind(this)} value={this.state.notes}></textarea>
                          
														{/* <p><span className="note">Ghi rõ các yêu cầu thuê xe.</span></p> */}
                        </div>
                    </div>
                    <div className="space m"></div>
                    <h3 className="title">Giấy tờ thuê xe</h3>
                    <p className="position-relative pos-1">
                        <div className="switch-on-off" style={{ right: "0", position: "absolute", bottom: "10px" }}>
                            <input className="switch-input" id="cb-required-papers-spec" type="checkbox" checked={!this.state.requiredPapersSpecific} onClick={this.toggleRequiredPapersSpecific.bind(this)} />
                            <label className="switch-label" for="cb-required-papers-spec"></label>
                        </div>
										</p>
										<p>Thiết lập các giấy tờ khách bắt buộc phải có (bản gốc) khi thuê xe {this.state.car.name}.</p>
										{/* <div className="space"></div> */}
                    {!this.state.requiredPapersSpecific && <span className="note">Lưu ý: Xe đang áp dụng thiết lập giấy tờ chung. Tắt thiết lập chung nếu muốn tuỳ chỉnh riêng cho xe này</span>}
                    <div className="space m"></div>
                    <div className="list-features">
                        <ul>
                            {this.state.requiredPapers && this.state.requiredPapers.map(paper => <li key={paper.id}>                               
                                { paper.groupRequired ? 
                                <div className="line-radio">
                                    <label className="custom-radio custom-control">
                                        <input className="custom-control-input" type="radio" id={`paper_${paper.id}`}
                                            disabled={!this.state.requiredPapersSpecific || !paper.edit}
                                            name={paper.groupId}
                                            value={paper.id}
                                            checked={paper.checked}
                                            onChange={this.onRequiredPapersChange.bind(this)} />
                                        <span className="custom-control-indicator"></span>
                                        <span className="custom-control-description">{paper.name}</span>
                                    </label>
                                </div> : 
                                <div className="squaredFour have-label">
                                    <input type="checkbox" id={`paper_${paper.id}`} 
                                        disabled={!this.state.requiredPapersSpecific || !paper.edit} 
                                        name={paper.groupRequired ? paper.groupId : ""} 
                                        checked={paper.checked} 
                                        value={paper.id} 
                                        onClick={this.onRequiredPapersChange.bind(this)} />
                                    <label htmlFor={`paper_${paper.id}`}> <img style={{ width: "20px", height: "20px", display: "inline" }} src={paper.logo} alt="Mioto - Thuê xe tự lái" /> {paper.name}</label>
                                </div> }
                            </li>)}
                        </ul>
                    </div>
                    {/* <label className="label">Giấy tờ khác</label>
                    <div className="space m"></div>
                    <div className="form-default">
                        <div className="line-form end">
                            <textarea className="textarea" disabled={!this.state.requiredPapersSpecific} value={this.state.requiredPapersOther} onChange={this.onRequiredPapersOtherChange.bind(this)}></textarea>
                            <p><span className="note">Ghi rõ các giấy tờ khác (nếu có).</span></p>
                        </div>
                    </div> */}
                    <div className="space m"></div>
                    <div className="line"></div>
                    <a className="btn btn-primary btn--m" onClick={this.callingUpdateCarInfo.bind(this)}>Lưu thay đổi</a>
                </div>
            </div>
        }

        return content;
    }
} 