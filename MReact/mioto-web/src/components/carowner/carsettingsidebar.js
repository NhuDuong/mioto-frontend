import React from "react"
import { NavLink } from "react-router-dom"

export default class CarSettingSidebar extends React.Component {
    render() {
        return <div className="sidebar sidebar-settings hide-on-med-and-down">
            <ul>
                <li><NavLink className={this.props.handler === "#infosetting" ? "active" : ""} to={`/carsetting/${this.props.carId}#infosetting`}><i className="ic ic-infomation"></i> Thông tin</NavLink></li>
                <li><NavLink className={this.props.handler === "#photossetting" ? "active" : ""} to={`/carsetting/${this.props.carId}#photossetting`}><i className="ic ic-photo"></i> Hình ảnh</NavLink></li>
								<li><NavLink className={this.props.handler === "#paperssetting" ? "active" : ""} to={`/carsetting/${this.props.carId}#paperssetting`}><i className="ic ic-license"></i> Giấy tờ xe</NavLink></li>
								<li><NavLink className={this.props.handler === "#mortgagessetting" ? "active" : ""} to={`/carsetting/${this.props.carId}#mortgagessetting`}><i className="ic ic-mortgages"></i>Tài sản thế chấp</NavLink></li>
                <li><NavLink className={this.props.handler === "#rentingsetting" ? "active" : ""} to={`/carsetting/${this.props.carId}#rentingsetting`}><i className="ic ic-setting-rent"></i> Cài đặt cho thuê</NavLink></li>
                <li><NavLink className={this.props.handler === "#pricesetting" ? "active" : ""} to={`/carsetting/${this.props.carId}#pricesetting`}><i className="ic ic-setting-price"></i> Cài đặt giá</NavLink></li>
                <li><NavLink className={this.props.handler === "#calendarsetting" ? "active" : ""} to={`/carsetting/${this.props.carId}#calendarsetting`}><i className="ic ic-setting-calendar"></i> Cài đặt lịch</NavLink></li>
                <li><NavLink className={this.props.handler === "#tripssetting" ? "active" : ""} to={`/carsetting/${this.props.carId}#tripssetting`}><i className="ic ic-management"></i> Quản lý chuyến</NavLink></li>
                <li><NavLink className={this.props.handler === "#gpssetting" ? "active" : ""} to={`/carsetting/${this.props.carId}#gpssetting`}><i className="ic ic-gps"></i> GPS</NavLink></li>
            </ul>
        </div>
    }
} 