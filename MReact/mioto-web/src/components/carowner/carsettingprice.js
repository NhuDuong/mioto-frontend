import React from "react"
import moment from 'moment'

import { formatPrice } from "../common/common"
import { commonErr } from "../common/errors"
import ReactSimpleRange from "react-simple-range"
import { LoadingInline, LoadingOverlay } from "../common/loading"
import { getCarSetting, updateCar, getCarCalendar } from "../../model/car"
import { MessageBox, MessageLine } from "../common/messagebox"

export default class CarSettingPrice extends React.Component {
    constructor() {
        super();
        this.state = {
            err: commonErr.INNIT,
            errMsg: "",
            updateErrMsg: "",
            isShowPriceSettingBox: false,
            customPrice: 0,
            activeTs: 0
        }
    }

    init(car) {
        getCarCalendar(car.id).then(calendarResp => {
            if (calendarResp.data.error >= commonErr.SUCCESS) {
                getCarSetting({
                    carId: car.id
                }).then(settingResp => {
                    const setting = settingResp.data.data;
                    const calendar = calendarResp.data.data;

                    if (calendar) {
                        this.setState({
                            err: settingResp.data.error,
                            errMsg: settingResp.data.errorMessage,
                            car: car,
                            price: calendar.priceDaily,
                            isDiscount: car.discountEnable === 1,
                            discountWeek: car.discountWeekly,
                            discountMonth: car.discountMonthly,
                            time: moment(),
                            setting: setting
                        })
                    }
                })
            }
        });
    }

    componentDidMount() {
        const car = this.props.car;
        this.init(car);
    }

    componentWillReceiveProps(props) {
        const car = props.car;
        this.init(car);
    }

    onPriceRangeChange(event) {
        if (isNaN(event.target.value)) {
            return;
        }
        this.setState({
            price: event.target.value * 1000
        });
    }

    toggleDiscount() {
        this.setState({
            isDiscount: !this.state.isDiscount
        });
    }

    onDiscountWeekRangeChange(range) {
        this.setState({
            discountWeek: range.value
        });
    }

    onDiscountMonthRangeChange(range) {
        this.setState({
            discountMonth: range.value
        });
    }

    nextMonth() {
        const nextMonth = this.state.time.add(1, "months");
        this.setState({
            time: nextMonth
        });
    }

    preMonth() {
        const preMonth = this.state.time.subtract(1, "month");
        this.setState({
            time: preMonth
        });
    }

    hideMessageBox() {
        this.setState({
            updateErrMsg: ""
        });
    }

    isValid() {
        var errMsg = ""
        if (!this.state.price || this.state.price === "") {
            errMsg = "Bạn chưa nhập giá"
        } else if (this.state.price < this.state.setting.priceDailyMin || this.state.price > this.state.setting.priceDailyMax) {
            errMsg = `Giá không hợp lệ. Giá cho phép trong khoản từ ${formatPrice(this.state.setting.priceDailyMin)} đến ${formatPrice(this.state.setting.priceDailyMax)}.`
        }
        return errMsg;
    }

    updateCarInfo() {
        const errMsg = this.isValid();
        if (errMsg !== "") {
            this.setState({
                err: commonErr.FAIL,
                updateErrMsg: errMsg
            });
            return;
        }

        const priceDaily = this.state.price;
        const discountEnable = this.state.isDiscount;
        const discountWeekly = this.state.discountWeek;
        const discountMonthly = this.state.discountMonth;

        this.setState({
            err: commonErr.LOADING
        });
        updateCar(this.state.car.id, {
            priceDaily: priceDaily,
            discountEnable: discountEnable,
            discountWeekly: discountWeekly,
            discountMonthly: discountMonthly
        }).then(resp => {
            this.setState({
                err: resp.data.error,
                updateErrMsg: resp.data.errorMessage
            });
        });
    }

    render() {
        var content;
        if (this.state.err === commonErr.INNIT) {
            content = <LoadingInline />
        } else if (!this.state.car || !this.state.setting) {
            content = <MessageLine message="Không tìm thấy thông tin." />
        } else {
            content = <div className="content">
                <div className="content-container">
                    {this.state.err === commonErr.LOADING && <LoadingOverlay />}
                    {this.state.updateErrMsg && this.state.updateErrMsg !== "" && <MessageBox error={this.state.err} message={this.state.updateErrMsg} show={this.state.updateErrMsg && this.state.updateErrMsg !== ""} hideModal={this.hideMessageBox.bind(this)} />}
                    <h3>Đơn giá thuê mặc định</h3>
                    <p className="fl">
                        <span className="note">Đơn giá thuê mặc định được áp dụng nếu ngày đó không có tuỳ chỉnh khác về giá.</span>
                    </p>
                    <div className="space m"></div>
                    <div className="space m"></div>
                    <div className="space m"></div>
                    <div className="col-1">
                        <div className="form-default">
                            <div className="line-form">
                                <div className="wrap-input-label d-flex">
                                    <div className="wrap-input">
                                        <input type="text" value={this.state.price / 1000} onChange={this.onPriceRangeChange.bind(this)} />
                                    </div>
                                    <span className="phay"> K</span>
                                </div>
                            </div>
                            <span className="note">Giá đề xuất: {formatPrice(this.state.setting.priceDailyRecommend)}</span>
                        </div>
                    </div>
                    <div className="clear"></div>
                    <div className="space m clear"></div>
                    <div className="form-default">
                        <h3>Giảm giá</h3>
                        <p className="position-relative pos-1">
                            <div className="switch-on-off" style={{
                                right: "0",
                                position: "absolute",
                                bottom: "10px"
                            }}>
                                <input className="switch-input" id="cb-discount" type="checkbox" checked={this.state.isDiscount} onChange={this.toggleDiscount.bind(this)} />
                                <label className="switch-label" htmlFor="cb-discount"></label>
                            </div>
                        </p>
                        {this.state.isDiscount && <div className="col-1">
                            <div className="form-default form-s">
                                <div className="line-form">
                                    <label className="label">Giảm giá thuê tuần (% trên đơn giá)</label>
                                    <div className="range-slider">
                                        <ReactSimpleRange
                                            step={1}
                                            min={1}
                                            max={this.state.setting.discountMax}
                                            value={this.state.discountWeek}
                                            sliderSize={14}
                                            thumbSize={16}
                                            label={false}
                                            trackColor={"#00a550"}
                                            thumbColor={"#141414"}
                                            onChange={this.onDiscountWeekRangeChange.bind(this)}
                                        />
                                    </div>
                                    <span style={{
                                        float: "right",
                                        fontSize: "14px"
                                    }}>{`${this.state.discountWeek}%`}</span>
                                    <span className="note">Giảm đề xuất: {this.state.setting.discountWeeklyRecommend}%</span>
                                </div>
                            </div>
                        </div>}
                        {this.state.isDiscount && <div className="col-2">
                            <div className="form-default form-s">
                                <div className="line-form">
                                    <label className="label">Giảm giá thuê tháng (% trên đơn giá)</label>
                                    <div className="range-slider">
                                        <ReactSimpleRange
                                            step={1}
                                            min={1}
                                            max={this.state.setting.discountMax}
                                            value={this.state.discountMonth}
                                            sliderSize={14}
                                            thumbSize={16}
                                            label={false}
                                            trackColor={"#00a550"}
                                            thumbColor={"#141414"}
                                            onChange={this.onDiscountMonthRangeChange.bind(this)}
                                        />
                                    </div>
                                    <span style={{ float: "right", fontSize: "14px" }}>{`${this.state.discountMonth}%`}</span>
                                    <span className="note">Giảm đề xuất: {this.state.setting.discountMonthlyRecommend}%</span>
                                </div>
                            </div>
                        </div>}
                        <div className="space m clear"></div>
                    </div>
                    <a className="link-calendar" onClick={this.updateCarInfo.bind(this)} href={`/carsetting/${this.state.car.id}#calendarsetting`}><i className="ic ic-setting-calendar"></i>Tùy chỉnh giá theo lịch </a>
                    <div className="line"></div>
                    <a className="btn btn-primary btn--m" onClick={this.updateCarInfo.bind(this)}>Lưu thay đổi</a>
                </div>
            </div>
        }

        return content;
    }
} 