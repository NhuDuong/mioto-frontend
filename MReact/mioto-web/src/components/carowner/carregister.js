import React from "react"
import { connect } from "react-redux"
import { Modal } from "react-bootstrap"
import NumberFormat from "react-number-format"

import { getFilterConfig, getCarSetting, getVehicleType, getVehicleMake, getVehicleModel, registerCar } from "../../model/car"
import { getLoggedProfile as baseGetProfile } from "../../model/profile"
import { getBalance } from '../../model/wallet'
import { getLoggedProfile } from "../../actions/sessionAct"
import Login from "../login/login"
import Header from "../common/header"
import Footer from "../common/footer"
import CarRegisterInfo from "./carregisterInfo"
import CarRegisterPhotos from "./carregisterPhotos"
import CarRegisterRenting from "./carregisterRenting"
import ChangePhone from "../account/changephone"
import VerifyPhone from "../account/verifyphone"
import { commonErr } from "../common/errors"
import { MessageBox } from "../common/messagebox"
import { LoadingOverlay } from "../common/loading"
import { formatPrice, DEFAULT_INSTANT_RANGE_FROM, DEFAULT_INSTANT_RANGE_TO, convertSecondToTime } from "../common/common"

class CarRegister extends React.Component {
    constructor() {
        super();
        this.state = {
            minStep: 1,
            maxStep: 3,
            step: 1,
            err: commonErr.INNIT,
            errMsg: "",

            //info
            inviteCode: "",
            licensePlate: "",
            address: "",
            lat: 0,
            lng: 0,
            vehicleType: 0,
            vehicleMake: 0,
            vehicleModel: 0,
            year: 2017,
            seat: 4,
            transmission: 1,
            fuel: 1,
            fuelRate: 10,
            desc: "",
            features: [],

            //photos
            photos: [],

            //renting
            price: 1000000,
            isDiscount: true,
            isInstantly: false,
            instantRangeFrom: DEFAULT_INSTANT_RANGE_FROM,
            instantRangeTo: DEFAULT_INSTANT_RANGE_TO,
            isDelivery: true,
            isLimited: true,
            policy: "",
            discountWeek: 5,
            discountMonth: 10,
            deliveryLimited: 30,
            deliveryPrice: 20000,
            limited: 400,
            limitedPrice: 20000,
            isShowConfirmInstant: false,
        }

        this.goNext = this.goNext.bind(this);
        this.goPrev = this.goPrev.bind(this);
        this.register = this.register.bind(this);
        this.getVehicleModelInfo = this.getVehicleModelInfo.bind(this);
    }

    componentDidMount() {
        baseGetProfile().then(resp => {
            if (resp.data.data && resp.data.data.profile) {
                const profile = resp.data.data.profile;
                if (!profile.phoneNumber || profile.phoneNumber === "") {
                    this.setState({
                        showChangePhone: true
                    });
                } else if (!profile.phoneVerified || profile.phoneVerified === 0) {
                    this.setState({
                        showVerifyPhone: true,
                        profile: profile
                    });
                }
            } else {
                this.setState({
                    showLoginForm: true
                });
            }
        });

        getFilterConfig().then(resp => {
            if (resp.data.data) {
                this.setState({
                    featuresAll: resp.data.data.allFeatures
                });
            }
        });

        getVehicleType().then(resp => {
            if (resp.data.data) {
                this.setState({
                    vehicleTypes: resp.data.data.vehicleTypes
                });
            }
        });

        getBalance().then((resp) => {
			if (resp.data.error === commonErr.SUCCESS) {
				this.setState({
					balance: resp.data.data.balance
				})
			} else {
				this.setState({
					err: resp.data.error,
				})
			}
		})

        window.scrollTo(0, 0);
    }

    //session
    openLoginForm() {
        this.setState({
            showLoginForm: true
        });
    }

    showLoginForm() {
        this.setState({
            showLoginForm: true
        })
    }

    closeLoginForm() {
        this.setState({
            showLoginForm: false
        })
    }

    getLoggedProfile() {
        this.props.dispatch(getLoggedProfile());
    }

    goNext() {
        const message = this.isValidNext();
        if (message === "") {
            this.setState({
                step: this.state.step + 1
            })
        } else {
            this.setState({
                err: commonErr.FAIL,
                errMsg: message
            })
        }
    }

    goPrev() {
        this.setState({
            step: this.state.step - 1
        });
    }

    reset() {
        this.setState({
            step: 1,
            err: commonErr.INNIT,
            errMsg: ""
        });
    }

    hideMessageBox() {
        this.setState({
            err: commonErr.INNIT,
            errMsg: ""
        });
    }

    hideWarningBox() {
        this.setState({
            warn: commonErr.INNIT,
            warnMessage: ""
        });
    }

    isValidInfo() {
        var message = "";
        if (!this.state.licensePlate || this.state.licensePlate === "") {
            message = "Vui lòng nhập biển số.";
        } else if (!this.state.vehicleType || this.state.vehicleType <= 0) {
            message = "Bạn chưa chọn loại xe.";
        } else if (!this.state.vehicleMake || this.state.vehicleMake <= 0) {
            message = "Bạn chưa chọn hãng xe.";
        } else if (!this.state.vehicleModel || this.state.vehicleModel <= 0) {
            message = "Bạn chưa chọn mẫu xe.";
        } else if (!this.state.year || this.state.year <= 0) {
            message = "Bạn chưa chọn năm sản xuất.";
        } else if (!this.state.seat || this.state.seat <= 0) {
            message = "Bạn chưa chọn số ghế.";
        } else if (!this.state.transmission || this.state.transmission <= 0) {
            message = "Bạn chưa chọn truyền động.";
        } else if (!this.state.fuel || this.state.fuel <= 0) {
            message = "Bạn chưa chọn loại nhiên liệu.";
        } else if (!this.state.fuelRate || this.state.fuelRate <= 0) {
            message = "Bạn chưa chọn mức tiêu thụ nhiên liệu.";
        }
        return message;
    }

    isValidPhotos() {
        var message = "";
        if (!this.state.photos || this.state.photos.length <= 0) {
            message = "Bạn chưa upload hình cho xe";
        }
        return message;
    }

    isValidPapers() {
        return "";
    }

    isValidRenting() {
        var message = "";
        if (!this.state.price || this.state.price <= 0) {
            message = "Bạn chưa chọn giá cho xe."
        } else if (this.state.price > this.state.setting.priceDailyMax || this.state.price < this.state.setting.priceDailyMin) {
            message = `Giá không hợp lệ. Giá cho phép trong khoản từ ${formatPrice(this.state.setting.priceDailyMin)} đến ${formatPrice(this.state.setting.priceDailyMax)}.`
        } else if (!this.state.address || this.state.address === "") {
            message = "Bạn chưa chọn địa chỉ xe."
        } else if (this.state.lat === 0 || this.state.lng === 0) {
            message = "Địa chỉ không hợp lệ.";
        }
        return message;
    }

    isValidNext() {
        if (this.state.step === 1) {
            return this.isValidInfo();
        } else if (this.state.step === 2) {
            return this.isValidRenting();
        } else if (this.state.step === 3) {
            return this.isValidPhotos();
        }
    }

    register() {
        const message = this.isValidNext();
        if (message !== "") {
            this.setState({
                err: commonErr.FAIL,
                errMsg: message
            })
        } else {
            const data = {
                inviteCode: this.state.inviteCode,
                lp: this.state.licensePlate,
                vehicleModelId: this.state.vehicleModel,
                seat: this.state.seat,
                year: this.state.year,
                instant: this.state.isInstantly,
                instantRangeFrom: this.state.instantRangeFrom,
                instantRangeTo: this.state.instantRangeTo,
                lat: this.state.lat,
                lon: this.state.lng,
                addr: this.state.address,
                deliveryEnable: this.state.isDelivery,
                deliveryRadius: this.state.deliveryLimited,
                deliveryPrice: this.state.deliveryPrice,
                limitEnable: this.state.isLimited,
                limitKM: this.state.limited,
                limitPrice: this.state.limitedPrice,
                photoTmpIds: this.state.photos,
                transmission: this.state.transmission,
                fuel: this.state.fuel,
                fuelConsumption: this.state.fuelRate,
                features: this.state.features,
                desc: encodeURI(this.state.desc),
                notes: encodeURI(this.state.policy),
                requiredPapers: "",
                requiredPapersSpecific: "",
                requiredPapersOther: "",
                priceDaily: this.state.price,
                discountEnable: this.state.isDiscount,
                discountWeekly: this.state.discountWeek,
                discountMonthly: this.state.discountMonth
            }

            this.setState({
                err: commonErr.LOADING
            })

            registerCar(data).then(resp => {
                if (resp.data.error >= 0) {
                    window.location.href = `/mycars`;
                } else {
                    this.setState({
                        err: resp.data.error,
                        errMsg: resp.data.errorMessage
                    })
                }
            })
        }
    }

    //info
    onInviteCodeChange(event) {
        this.setState({
            inviteCode: event.target.value
        });
    }

    onLicensePlateChange(event) {
        this.setState({
            licensePlate: event.target.value
        });
    }

    onAddressChange(event) {
        this.setState({
            address: event.target.value
        });
    }

    onAddressLocationUpdate(address, location) {
        this.setState({
            address: address,
            lat: location.lat,
            lng: location.lng
        });
    }

    onVehicleTypeChange(event) {
        const vehicleType = event.target.value;
        if (vehicleType > 0) {
            getVehicleMake(vehicleType).then(resp => {
                if (resp.data.data) {
                    this.setState({
                        vehicleType: vehicleType,
                        vehicleMakes: resp.data.data.vehicleMakes
                    });
                }
            });
        } else {
            this.setState({
                vehicleType: 0,
                vehicleMake: 0,
                vehicleModel: 0
            });
        }
    }

    onVehicleMakeChange(event) {
        const vehicleMake = event.target.value;
        if (vehicleMake > 0) {
            const vehicleType = this.state.vehicleType;
            getVehicleModel(vehicleType, vehicleMake).then(resp => {
                if (resp.data.data) {
                    this.setState({
                        vehicleMake: vehicleMake,
                        vehicleModels: resp.data.data.vehicleModels
                    });
                }
            });
        } else {
            this.setState({
                vehicleMake: 0,
                vehicleModel: 0
            });
        }
    }

    getVehicleModelInfo(vehicleModel, year){
        if (vehicleModel !== 0) {
            getCarSetting({ vehicleModelId: vehicleModel, year: year }).then(resp => {
                const data = resp.data.data;
                if (data) {
                    this.setState({
                        price: data.priceDailyRecommend,
                        discountWeekly: data.discountWeeklyRecommend,
                        discountMonthly: data.discountMonthlyRecommend,
                        deliveryLimited: data.deliveryRadiusRecommend,
                        deliveryPrice: data.deliveryPriceRecommend,
                        limitKM: data.limitKMRecommend,
                        limitedPrice: data.limitPriceRecommend,
                        //
                        setting: data
                    });
                }
            });
        }
    }

    onVehicleModelChange(event) {
        const vehicleModel = event.target.value;
        this.setState({
            vehicleModel: vehicleModel
        });

        this.getVehicleModelInfo(vehicleModel, this.state.year);
    }

    onYearChange(event) {
        const year = event.target.value;
        this.setState({
            year: year
        });

        this.getVehicleModelInfo(this.state.vehicleModel, year);
    }

    onSeatChange(event) {
        const seat = event.target.value;
        this.setState({
            seat: seat
        });
    }

    onTransmisionChange(event) {
        const transmission = event.target.value;
        this.setState({
            transmission: transmission
        });
    }

    onFuelChange(event) {
        const fuel = event.target.value;
        this.setState({
            fuel: fuel
        });
    }

    onFuelRateChange(event) {
        const fuelRate = event.target.value;
        this.setState({
            fuelRate: fuelRate
        });
    }

    onDescChange(event) {
        const desc = event.target.value;
        this.setState({
            desc: desc
        });
    }

    onFeaturesChange(event) {
        var i;
        var features = [];
        if (this.state.features) {
            features = features.concat(this.state.features);
        }

        const feature = event.target.value;
        if (event.target.checked) {
            features.push(feature);
        } else {
            for (i = features.length - 1; i >= 0; --i) {
                if (features[i] === feature) {
                    features.splice(i, 1);
                }
            }
        }

        const featuresAll = [];
        for (i = 0; i < this.state.featuresAll.length; ++i) {
            const feature = this.state.featuresAll[i];
            if (event.target.value === feature.id) {
                feature.checked = event.target.checked
            }
            featuresAll.push(feature);
        }

        this.setState({
            features: features,
            featuresAll: featuresAll
        });
    }

    //photos
    hanldePhotoInputChange(photo) {
        var photos = [];
        if (this.state.photos) {
            photos = photos.concat(this.state.photos);
        }
        photos.push(photo);

        this.setState({
            photos: photos
        });
    }

    removeCarPhoto(photo) {
        var photos = [];
        if (this.state.photos) {
            photos = photos.concat(this.state.photos);
        }
        photos = photos.filter(function (p) {
            return p.id !== photo.id
        });

        this.setState({
            photos: photos
        });
    }

    //renting
    onPriceRangeChange(event) {
        if (isNaN(event.target.value)) {
            return;
        }
        this.setState({
            price: event.target.value * 1000
        });
    }

    toggleDiscount() {
        this.setState({
            isDiscount: !this.state.isDiscount
        });
    }

    onDiscountWeekRangeChange(range) {
        this.setState({
            discountWeek: range.value
        });
    }

    onDiscountMonthRangeChange(range) {
        this.setState({
            discountMonth: range.value
        });
    }

    onCloseModal(){
        this.setState({
            isShowConfirmInstant: false
        });
    }

    toggleInstantly() {
        var instant = !this.state.isInstantly;
        this.setState({
            isInstantly: instant,
            isShowConfirmInstant: instant
        });
        if(instant){
            this.setState({
                instantRangeFrom: (!this.state.instantRangeFrom  || this.state.instantRangeFrom === 0) ? DEFAULT_INSTANT_RANGE_FROM : this.state.instantRangeFrom,
                instantRangeTo: (!this.state.instantRangeTo  || this.state.instantRangeTo === 0) ? DEFAULT_INSTANT_RANGE_TO : this.state.instantRangeTo,
            });
        }
    }

    onInstantRangeFromChange(event){
        this.setState({
            instantRangeFrom: event.target.value
        });
    }

    onInstantRangeToChange(event){
        this.setState({
            instantRangeTo: event.target.value
        });
    }

    toggleDelivery() {
        this.setState({
            isDelivery: !this.state.isDelivery
        });
    }

    onDeliveryLimitedChange(event) {
        this.setState({
            deliveryLimited: event.target.value
        });
    }

    onDeliveryPriceRangeChange(range) {
        this.setState({
            deliveryPrice: range.value
        });
    }

    toggleLimited() {
        this.setState({
            isLimited: !this.state.isLimited
        });
    }

    onLimitedChange(event) {
        this.setState({
            limited: event.target.value
        });
    }

    onLimitedPriceRangeChange(range) {
        this.setState({
            limitedPrice: range.value
        });
    }

    onPolicyChange(event) {
        this.setState({
            policy: event.target.value
        });
    }

    hideChangePhone() {
        this.setState({
            showChangePhone: false
        });
    }

    hideVerifyPhone() {
        this.setState({
            showVerifyPhone: false
        });
    }

    getProfile() {
        this.props.dispatch(getLoggedProfile());
    }

    render() {
        return <div className="mioto-layout">
            <Header />
            <section className="body">
                <div className="sidebar-control z-2">
                    <div className="sidebar-settings general-settings">
                        <ul>
                            <li> <a href="/mycars"><i className="ic ic-cars"></i><span className="label">Danh sách xe</span></a></li>
                            <li> <a href="/calendars"><i className="ic ic-setting-calendar"></i><span className="label">Lịch chung</span></a></li>
                            <li> <a href="/gps"><i className="ic ic-gps"></i><span className="label">GPS</span><sup class="new-func">BETA</sup></a></li>
                            <li> <a href="/commonsetting"><i className="ic ic-setting-rent"> </i><span className="label">Cài đặt chung</span></a></li>
                            <li> <a className="active" href="/carregister"><i className="ic ic-plus-black"></i><span className="label">Đăng ký xe </span></a></li>
                            <li> <a href="/mywallet"><i className="ic ic-owner-wallet"></i><span className="label">Số dư: <span className="fontWeight-6 font-16"><NumberFormat value={this.state.balance} displayType={'text'} thousandSeparator={" "} decimalSeparator={","} suffix={"đ"} /></span></span></a></li>
                        </ul>
                    </div>
                </div>
                <div className="module-register">
                    <div className="register-container">
                        <div className="content-register">
                            <div className="stepbystep">
                                <ul>
                                    <li className={this.state.step === 1 ? "active" : ""}><span className="nu">1</span><span className="value">Thông tin</span></li>
                                    <li className={this.state.step === 2 ? "active" : ""}><span className="nu">2</span><span className="value">Cho thuê</span></li>
                                    <li className={this.state.step === 3 ? "active" : ""}><span className="nu">3</span><span className="value">Hình ảnh</span></li>
                                </ul>
                            </div>
                            {this.state.step === 1 && <CarRegisterInfo
                                inviteCode={this.state.inviteCode}
                                licensePlate={this.state.licensePlate}
                                vehicleType={this.state.vehicleType}
                                vehicleTypes={this.state.vehicleTypes}
                                vehicleMake={this.state.vehicleMake}
                                vehicleMakes={this.state.vehicleMakes}
                                vehicleModels={this.state.vehicleModels}
                                vehicleModel={this.state.vehicleModel}
                                year={this.state.year}
                                seat={this.state.seat}
                                transmission={this.state.transmission}
                                fuel={this.state.fuel}
                                fuelRate={this.state.fuelRate}
                                desc={this.state.desc}
                                features={this.state.features}
                                featuresAll={this.state.featuresAll}
                                onInviteCodeChange={this.onInviteCodeChange.bind(this)}
                                onLicensePlateChange={this.onLicensePlateChange.bind(this)}
                                onVehicleTypeChange={this.onVehicleTypeChange.bind(this)}
                                onVehicleMakeChange={this.onVehicleMakeChange.bind(this)}
                                onVehicleModelChange={this.onVehicleModelChange.bind(this)}
                                onYearChange={this.onYearChange.bind(this)}
                                onSeatChange={this.onSeatChange.bind(this)}
                                onTransmisionChange={this.onTransmisionChange.bind(this)}
                                onFuelChange={this.onFuelChange.bind(this)}
                                onFuelRateChange={this.onFuelRateChange.bind(this)}
                                onDescChange={this.onDescChange.bind(this)}
                                onFeaturesChange={this.onFeaturesChange.bind(this)}
                            />}

                            {this.state.step === 2 && <CarRegisterRenting
                                price={this.state.price}
                                isDiscount={this.state.isDiscount}
                                discountWeek={this.state.discountWeek}
                                discountMonth={this.state.discountMonth}
                                address={this.state.address}
                                lat={this.state.lat}
                                lng={this.state.lng}
                                isInstantly={this.state.isInstantly}
                                instantRangeFrom={this.state.instantRangeFrom}
                                instantRangeTo={this.state.instantRangeTo}
                                isDelivery={this.state.isDelivery}
                                deliveryLimited={this.state.deliveryLimited}
                                deliveryPrice={this.state.deliveryPrice}
                                isLimited={this.state.isLimited}
                                limited={this.state.limited}
                                limitedPrice={this.state.limitedPrice}
                                policy={this.state.policy}
                                setting={this.state.setting}
                                onPriceRangeChange={this.onPriceRangeChange.bind(this)}
                                toggleDiscount={this.toggleDiscount.bind(this)}
                                onDiscountWeekRangeChange={this.onDiscountWeekRangeChange.bind(this)}
                                onDiscountMonthRangeChange={this.onDiscountMonthRangeChange.bind(this)}
                                onAddressChange={this.onAddressChange.bind(this)}
                                onAddressLocationUpdate={this.onAddressLocationUpdate.bind(this)}
                                toggleInstantly={this.toggleInstantly.bind(this)}
                                onCloseModal={this.onCloseModal.bind(this)}
                                onInstantRangeFromChange={this.onInstantRangeFromChange.bind(this)}
                                onInstantRangeToChange={this.onInstantRangeToChange.bind(this)}
                                toggleDelivery={this.toggleDelivery.bind(this)}
                                onDeliveryLimitedChange={this.onDeliveryLimitedChange.bind(this)}
                                onDeliveryPriceRangeChange={this.onDeliveryPriceRangeChange.bind(this)}
                                toggleLimited={this.toggleLimited.bind(this)}
                                onLimitedChange={this.onLimitedChange.bind(this)}
                                onLimitedPriceRangeChange={this.onLimitedPriceRangeChange.bind(this)}
                                onPolicyChange={this.onPolicyChange.bind(this)}
                            />}
                            {this.state.step === 3 && <CarRegisterPhotos
                                photos={this.state.photos}
                                hanldePhotoInputChange={this.hanldePhotoInputChange.bind(this)}
                                removeCarPhoto={this.removeCarPhoto.bind(this)}
                            />}
                            {this.state.err === commonErr.LOADING && <LoadingOverlay />}
                            <MessageBox show={this.state.err < commonErr.SUCCESS} hideModal={this.hideMessageBox.bind(this)} message={this.state.errMsg} />
                            <Login show={this.state.showLoginForm} showModal={this.openLoginForm.bind(this)} hideModal={this.closeLoginForm.bind(this)} getLoggedProfile={this.getLoggedProfile.bind(this)} />
                            <Modal
                                show={this.state.isShowConfirmInstant}
                                onHide={this.onCloseModal.bind(this)}
                                dialogClassName="modal-sm modal-dialog"
                            >
                                <Modal.Header closeButton={true} closeLabel={""}>
                                    <Modal.Title>Thông báo</Modal.Title>
                                </Modal.Header>
                                <Modal.Body><div className="module-register" style={{ background: "none", padding: "0" }}>
                                    <div className="form-default form-s">
                                        <div className="textAlign-center">
                                            <p style={{ color: '#141414' }}>Bạn có chắc chắn rằng xe của bạn luôn sẵn sàng cho thuê?</p>
                                            <p style={{fontSize: '12px', color: '#141414' }}>Bạn cần đảm bảo việc cần cập nhật lịch bận xe thường xuyên, trường hợp hủy chuyến sau khi khách đặt cọc vì chưa cập nhật lịch bận sẽ áp dụng phí hủy chuyến (100% tiền đặt cọc)</p>
                                            </div>
                                        <div className="space m" />
                                        <div className="space m" />
                                        <div className="wrap-btn has-2btn">
                                            <div className="wr-btn">
                                                <a className="btn btn-secondary btn--m" onClick={this.toggleInstantly.bind(this)}>Bỏ qua</a>
                                            </div>
                                            <div className="wr-btn">
                                                <a className="btn btn-primary btn--m" onClick={this.onCloseModal.bind(this)}>Tôi chắc chắn</a>
                                            </div>
                                        </div>
                                    </div>
                                </div></Modal.Body>
                            </Modal>    
                            <Modal
                                show={this.state.warn < commonErr.SUCCESS}
                                onHide={this.hideWarningBox.bind(this)}
                                dialogClassName="modal-sm modal-dialog"
                            >
                                <Modal.Header closeButton={true} closeLabel={""}>
                                    <Modal.Title>Thông báo</Modal.Title>
                                </Modal.Header>
                                <Modal.Body>
                                    <div className="form-default form-s">
                                        <div className="textAlign-center"><i className="ic ic-error"></i> {this.state.warnMessage}</div>
                                        <div className="space m"></div>
                                        <div className="clear"></div>
                                        <a className="btn btn-primary btn--m" href="/account" target="_blank">Cập nhật ngay</a>
                                    </div>
                                </Modal.Body>
                            </Modal>
                        </div>
                    </div>
                    <div className="wrap-btn has-2btn">
                        <div className="wr-btn"><a className="btn btn-secondary btn--m" disabled={this.state.step === this.state.minStep} onClick={this.goPrev}>Quay lại</a></div>
                        {this.state.step < this.state.maxStep && <div className="wr-btn"><a className="btn btn-primary btn--m" onClick={this.goNext}>Kế tiếp</a></div>}
                        {this.state.step === this.state.maxStep && <div className="wr-btn"><a className="btn btn-primary btn--m" onClick={this.register}>Đăng kí</a></div>}
                    </div>
                </div>
                <ChangePhone show={this.state.showChangePhone} hideModal={this.hideChangePhone.bind(this)} getProfile={this.getProfile.bind(this)} />
                {this.state.profile && <VerifyPhone show={this.state.showVerifyPhone} hideModal={this.hideVerifyPhone.bind(this)} profile={this.state.profile} getProfile={this.getProfile.bind(this)} />}
            </section>
            <Footer />
        </div>
    }
}

function mapSessionState(state) {
    return {
        session: state.session
    }
}

CarRegister = connect(mapSessionState)(CarRegister);

export default CarRegister;