import React from "react"

import { uploadCarPaper } from "../../model/car"
import { commonErr } from "../common/errors"

export default class CarRegisterPapers extends React.Component {
    constructor() {
        super();
        this.state = {
            cavet: {
                err: commonErr.INNIT
            },
            dangkiem: {
                err: commonErr.INNIT
            },
            baohiem: {
                err: commonErr.INNIT
            }
        }
    }

    componentDidMount() {
        window.scrollTo(0, 0);
    }

    hanldeCavetFileInputChange(photos) {
        const self = this;
        self.setState({
            cavet: {
                ...self.state.cavet,
                err: commonErr.LOADING
            }
        });
        uploadCarPaper(0, "cv", photos[0]).then(function (resp) {
            var photos = [];
            if (self.state.cavet.photos) {
                photos = photos.concat(self.state.cavet.photos);
            }
            if (resp.data.data && resp.data.data.id) {
                photos.push(resp.data.data);
            }
            self.setState({
                cavet: {
                    ...self.state.cavet,
                    err: resp.data.error,
                    errMsg: resp.data.errorMessage,
                    photos: photos
                }
            });
        });
    }

    hanldeDangkiemFileInputChange(photos) {
        const self = this;
        self.setState({
            dangkiem: {
                ...self.state.dangkiem,
                err: commonErr.LOADING
            }
        });
        uploadCarPaper(0, "dk", photos[0]).then(function (resp) {
            var photos = [];
            if (self.state.dangkiem.photos) {
                photos = photos.concat(self.state.dangkiem.photos);
            }
            if (resp.data.data && resp.data.data.id) {
                photos.push(resp.data.data);
            }
            self.setState({
                dangkiem: {
                    ...self.state.dangkiem,
                    err: resp.data.error,
                    errMsg: resp.data.errorMessage,
                    photos: photos
                }
            });
        });
    }

    hanldeBaohiemFileInputChange(photos) {
        const self = this;
        self.setState({
            baohiem: {
                ...self.state.baohiem,
                err: commonErr.LOADING
            }
        });

        uploadCarPaper(0, "bh", photos[0]).then(function (resp) {
            var photos = [];
            if (self.state.baohiem.photos) {
                photos = photos.concat(self.state.baohiem.photos);
            }
            if (resp.data.data && resp.data.data.id) {
                photos.push(resp.data.data);
            }
            self.setState({
                baohiem: {
                    ...self.state.baohiem,
                    err: resp.data.error,
                    errMsg: resp.data.errorMessage,
                    photos: photos
                }
            });
        });
    }

    render() {
        return <div className="group form-default">
            <p>Giấy tờ xe là thông tin bắt buộc và được bảo mật tuyệt đối theo chính sách của Mioto. Vui lòng cập nhật giấy tờ trong mục quản lý xe sau khi hoàn tất việc đăng kí xe.</p>
            {/* <div className="content-container">
                <h6>Cà vẹt</h6>
                <div className="space m"></div>
                <div className="list-photos">
                    <ul>
                        {this.state.cavet.photos && this.state.cavet.photos.map(photo => <li>
                            <div className="obj-photo" style={{ backgroundImage: `url(${photo.url})` }}><a className="func-remove" href="#!"><i className="ic ic-remove"></i></a></div>
                        </li>)}
                        <li>
                            <a className="obj-photo">
                                <span className="ins">
                                    {this.state.cavet.err !== commonErr.LOADING && <ImageUpload withPreview={false} onChange={this.hanldeCavetFileInputChange.bind(this)} className="form-control" />}
                                    {this.state.cavet.err === commonErr.LOADING && <LoadingInline />}
                                </span>
                            </a>
                        </li>
                    </ul>
                </div>
                <div className="space m"></div>
                <div className="space m"></div>
            </div>
            <div className="content-container">
                <h6>Đăng kiểm</h6>
                <div className="space m"></div>
                <div className="list-photos">
                    <ul>
                        {this.state.dangkiem.photos && this.state.dangkiem.photos.map(photo => <li>
                            <div className="obj-photo" style={{ backgroundImage: `url(${photo.url})` }}><a className="func-remove" href="#!"><i className="ic ic-remove"></i></a></div>
                        </li>)}
                        <li>
                            <a className="obj-photo">
                                <span className="ins">
                                    {this.state.dangkiem.err !== commonErr.LOADING && <ImageUpload withPreview={false} onChange={this.hanldeDangkiemFileInputChange.bind(this)} className="form-control" />}
                                    {this.state.dangkiem.err === commonErr.LOADING && <LoadingInline />}
                                </span>
                            </a>
                        </li>
                    </ul>
                </div>
                <div className="space m"></div>
                <div className="space m"></div>
            </div>
            <div className="content-container">
                <h6>Bảo hiểm dân sự</h6>
                <div className="space m"></div>
                <div className="list-photos">
                    <ul>
                        {this.state.baohiem.photos && this.state.baohiem.photos.map(photo => <li>
                            <div className="obj-photo" style={{ backgroundImage: `url(${photo.url})` }}><a className="func-remove" href="#!"><i className="ic ic-remove"></i></a></div>
                        </li>)}
                        <li>
                            <a className="obj-photo">
                                <span className="ins">
                                    {this.state.baohiem.err !== commonErr.LOADING && <ImageUpload withPreview={false} onChange={this.hanldeBaohiemFileInputChange.bind(this)} className="form-control" />}
                                    {this.state.baohiem.err === commonErr.LOADING && <LoadingInline />}
                                </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div> */}
        </div>
    }
} 