import React from "react"
import StarRatings from "react-star-ratings"
import { Link } from "react-router-dom"
import { Modal } from "react-bootstrap"

import { invisibleCar } from "../../model/car"
import { CarStatus, CarStatusColor, formatTitleInUrl } from "../common/common"

import car_photo from "../../static/images/upload/car_1.png"

export default class CarSettingHeader extends React.Component {
    constructor(props) {
        super();
        this.state = {
            isShowVisibleWarn: false,
            isActive: props.car.status !== 5,
        }
    }

    componentWillReceiveProps(props) {
        this.setState({
            isActive: props.car.status !== 5
        });
    }

    showVisibleWarn() {
        this.setState({
            isShowVisibleWarn: true
        });
    }

    hideVisibleWarn() {
        this.setState({
            isShowVisibleWarn: false
        });
    }

    visibleCar() {
        const self = this;
        const car = this.props.car;
        const isActive = this.state.isActive;

        if (isActive) {
            self.showVisibleWarn();
        } else {
            invisibleCar(car.id, false).then(function (resp) {
                self.props.getCar();
            });
        }
    }

    onConfirmVisibleCar() {
        const self = this;
        const car = this.props.car;
        invisibleCar(car.id, true).then(function (resp) {
            self.setState({
                isShowVisibleWarn: false
            });
            self.props.getCar();
        });
    }

    render() {
        const car = this.props.car;
        const handler = this.props.handler;
        const cars = this.props.cars;

        var nextCar;
        var prevCar;

        if (cars && cars.length >= 2) {
            for (var i = 0; i < cars.length; ++i) {
                if (cars[i].id === car.id) {
                    var nextIndex = i + 1;
                    if (nextIndex >= cars.length) {
                        nextIndex = 0;
                    }
                    nextCar = cars[nextIndex];
                    var prevIndex = i - 1;
                    if (prevIndex < 0) {
                        prevIndex = cars.length - 1;
                    }
                    prevCar = cars[prevIndex];
                    break;
                }
            }
        }

        return <div className="module-settings">
            <div className="info-car status-trips">
                {nextCar && <Link to={`/carsetting/${nextCar.id}${handler}`}><div className="swiper-button-next"><i className="i-arr"></i></div></Link>}
                {prevCar && <Link to={`/carsetting/${prevCar.id}${handler}`}><div className="swiper-button-prev"><i className="i-arr"></i></div></Link>}
                <div className="info-img">
                    <div className="fix-img">
                        <Link to={{ pathname: `/car/${formatTitleInUrl(car.name)}/${car.id}` }}>
                            <img src={car.photos ? car.photos[0].thumbUrl : car_photo} alt={`Cho thuê xe tự lái ${car.name}`} />
                        </Link>
                    </div>
                    <p className="status">
                        <span className={`status ${CarStatusColor[car.status]}-dot`}></span>{CarStatus[car.status]}
                    </p>
                    {car.status >= 2 && <a className="btn btn-primary btn--m show-on-small" onClick={this.visibleCar.bind(this)}>{this.state.isActive ? "Ẩn xe" : "Hiện xe"}</a>}
                    <div className="info-desc show-on-small">
                        <div className="group-name"><span className="lstitle">DÒNG XE</span>
                            <h1 className="title-car">{car.name}</h1>
                            <StarRatings
                                rating={car.rating.avg}
                                starRatedColor="#00a550"
                                starDimension="17px"
                                starSpacing="1px"
                            />
                        </div>
                    </div>
                </div>
                <div className="info-desc hide-on-small">
                    <div className="group-name"><span className="lstitle">DÒNG XE</span>
                        <h1 className="title-car">{car.name}</h1>
                        <StarRatings
                            rating={car.rating.avg}
                            starRatedColor="#00a550"
                            starDimension="17px"
                            starSpacing="1px"
                        />
                    </div>
                    <div className="space m"></div>
                    {car.status >= 2 && <a className="btn btn-primary btn--m hide-on-small" onClick={this.visibleCar.bind(this)}>{this.state.isActive ? "Ẩn xe" : "Hiện xe"}</a>}
                </div>
            </div>
            <div className="clear"></div>
            <Modal
                show={this.state.isShowVisibleWarn}
                onHide={this.hideVisibleWarn.bind(this)}
                dialogClassName="modal-sm modal-dialog"
            >
                <Modal.Header closeButton={true} closeLabel={""}>
                    <Modal.Title>Tạm ngưng hoạt động</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <div className="form-default form-s">
                        <div className="textAlign-center"><i className="ic ic-error"></i> Xe của bạn sẽ chuyển sang trạng thái tạm ngưng hoạt động và không được tìm thấy trên hệ thống. <span style={{ color: "red" }}>Lưu ý: Toàn bộ các yêu cầu đặt xe này (nếu có) sẽ được huỷ.</span></div>
                        <div className="space m"></div>
                        <div className="space m"></div>
                        <div className="wr-btn"><a className="btn btn-primary btn--m" onClick={this.onConfirmVisibleCar.bind(this)}>Xác nhận</a></div>
                    </div>
                </Modal.Body>
            </Modal>
        </div>
    }
} 