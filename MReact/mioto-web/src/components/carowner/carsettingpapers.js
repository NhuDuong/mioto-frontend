import React from "react"

import { getCarSetting, uploadCarPaper, removeCarPaper } from "../../model/car"
import { commonErr } from "../common/errors"
import ImageUpload from "../common/imageupload"
import { LoadingInline } from "../common/loading"
import { MessageBox } from "../common/messagebox"

export default class CarSettingPapers extends React.Component {
    constructor() {
        super();
        this.state = {
            err: commonErr.INNIT,
            errMsg: "",

            cavet: {
                err: commonErr.INNIT
            },
            dangkiem: {
                err: commonErr.INNIT
            },
            baohiem: {
                err: commonErr.INNIT
            },
      
        }
    }

    init(car) {
        getCarSetting({
            carId: car.id
        }).then(settingResp => {
            if (settingResp.data.error >= commonErr.SUCCESS) {
                const cavet = settingResp.data.data.papers.filter(paper => {
                    return paper.id === "cv"
                })[0];
                const dangkiem = settingResp.data.data.papers.filter(paper => {
                    return paper.id === "dk"
                })[0];
                const baohiem = settingResp.data.data.papers.filter(paper => {
                    return paper.id === "ds"
                })[0];
           
                this.setState({
                    err: settingResp.data.error,
                    errMsg: settingResp.data.errorMessage,
                    car: car,
                    cavet: {
                        ...this.state.cavet,
                        photos: cavet.photos
                    },
                    dangkiem: {
                        ...this.state.dangkiem,
                        photos: dangkiem.photos
                    },
                    baohiem: {
                        ...this.state.baohiem,
                        photos: baohiem.photos
                    },
                  
                })
            } else {
                this.setState({
                    err: settingResp.data.error,
                    errMsg: settingResp.data.errorMessage,
                })
            }
        });
    }

    componentDidMount() {
        const car = this.props.car;
        this.init(car);
    }

    componentWillReceiveProps(props) {
        const car = props.car;
        this.init(car);
    }

    hanldeCavetFileInputChange(photos) {
        this.setState({
            cavet: {
                ...this.state.cavet,
                err: commonErr.LOADING
            }
        });
        uploadCarPaper(this.state.car.id, "cv", photos[0]).then(resp => {
            var photos = [];
            if (this.state.cavet.photos) {
                photos = photos.concat(this.state.cavet.photos);
            }
            if (resp.data.data && resp.data.data.id) {
                photos.push(resp.data.data);
            }
            this.setState({
                cavet: {
                    ...this.state.cavet,
                    err: resp.data.error,
                    errMsg: resp.data.errorMessage,
                    photos: photos
                }
            });
        });
    }

    hanldeDangkiemFileInputChange(photos) {
        this.setState({
            dangkiem: {
                ...this.state.dangkiem,
                err: commonErr.LOADING
            }
        });
        uploadCarPaper(this.state.car.id, "dk", photos[0]).then(resp => {
            var photos = [];
            if (this.state.dangkiem.photos) {
                photos = photos.concat(this.state.dangkiem.photos);
            }
            if (resp.data.data && resp.data.data.id) {
                photos.push(resp.data.data);
            }
            this.setState({
                dangkiem: {
                    ...this.state.dangkiem,
                    err: resp.data.error,
                    errMsg: resp.data.errorMessage,
                    photos: photos
                }
            });
        });
    }

    hanldeBaohiemFileInputChange(photos) {
        const self = this;
        self.setState({
            baohiem: {
                ...self.state.baohiem,
                err: commonErr.LOADING
            }
        });
        uploadCarPaper(self.state.car.id, "ds", photos[0]).then(function (resp) {
            var photos = [];
            if (self.state.baohiem.photos) {
                photos = photos.concat(self.state.baohiem.photos);
            }
            if (resp.data.data && resp.data.data.id) {
                photos.push(resp.data.data);
            }
            self.setState({
                baohiem: {
                    ...self.state.baohiem,
                    err: resp.data.error,
                    errMsg: resp.data.errorMessage,
                    photos: photos
                }
            });
        });
    }

    // hanldeSokhungFileInputChange(photos) {
    //     const self = this;
    //     self.setState({
    //         sokhung: {
    //             ...self.state.sokhung,
    //             err: commonErr.LOADING
    //         }
    //     });
    //     uploadCarPaper(self.state.car.id, "sk", photos[0]).then(function (resp) {
    //         var photos = [];
    //         if (self.state.sokhung.photos) {
    //             photos = photos.concat(self.state.sokhung.photos);
    //         }
    //         if (resp.data.data && resp.data.data.id) {
    //             photos.push(resp.data.data);
    //         }
    //         self.setState({
    //             sokhung: {
    //                 ...self.state.sokhung,
    //                 err: resp.data.error,
    //                 errMsg: resp.data.errorMessage,
    //                 photos: photos
    //             }
    //         });
    //     });
    // }

    removeCarPaper(typeId, photo) {
        var photos = [];

        if (typeId === "cv") {
            if (this.state.cavet.photos) {
                photos = photos.concat(this.state.cavet.photos);
            }
            photos = photos.filter(p => {
                return p.id !== photo.id
            });
            this.setState({
                cavet: {
                    ...this.state.cavet,
                    photos: photos
                }
            });
        } else if (typeId === "dk") {
            if (this.state.dangkiem.photos) {
                photos = photos.concat(this.state.dangkiem.photos);
            }
            photos = photos.filter(p => {
                return p.id !== photo.id
            });
            this.setState({
                dangkiem: {
                    ...this.state.dangkiem,
                    photos: photos
                }
            });
        } else if (typeId === "ds") {
            if (this.state.baohiem.photos) {
                photos = photos.concat(this.state.baohiem.photos);
            }
            photos = photos.filter(function (p) {
                return p.id !== photo.id
            });
            this.setState({
                baohiem: {
                    ...this.state.baohiem,
                    photos: photos
                }
            });
        }

        removeCarPaper(this.state.car.id, typeId, photo.id);
    }

    hideCavetMessageBox() {
        this.setState({
            cavet: {
                err: commonErr.INNIT,
                errMsg: ""
            }
        });
    }

    hideDangkiemMessageBox() {
        this.setState({
            dangkiem: {
                err: commonErr.INNIT,
                errMsg: ""
            }
        });
    }

    hideBaohiemMessageBox() {
        this.setState({
            baohiem: {
                err: commonErr.INNIT,
                errMsg: ""
            }
        });
    }

    // hideSokhungMessageBox() {
    //     this.setState({
    //         sokhung: {
    //             err: commonErr.INNIT,
    //             errMsg: ""
    //         }
    //     });
    // }

    render() {
        var content;
        if (this.state.err === commonErr.INNIT) {
            content = <LoadingInline />
        } else {
            const cavet = this.state.cavet;
            const dangkiem = this.state.dangkiem;
            const baohiem = this.state.baohiem;
            // const sokhung = this.state.sokhung; 
            
            content = <div className="content">
                <div className="content-container">
                    <h3 className="title">Giấy tờ xe</h3>
                    <p>Giấy tờ dùng cho mục đích quản lý. Thông tin này tuyệt đối được bảo mật.</p>
                    <div className="space m"></div>
                    <h3 className="title">Cà vẹt</h3>
                    <div className="space m"></div>
                    <div className="list-photos">
                        <ul>
                            <li>
                                <a className="obj-photo">
                                    <span className="ins">
                                        {this.state.cavet.err !== commonErr.LOADING && <ImageUpload withPreview={false} onChange={this.hanldeCavetFileInputChange.bind(this)} className="form-control" />}
                                        {this.state.cavet.err === commonErr.LOADING && <LoadingInline />}
                                        <MessageBox show={this.state.cavet.err < commonErr.SUCCESS} hideModal={this.hideCavetMessageBox.bind(this)} message={this.state.cavet.errMsg} />
                                    </span>
                                </a>
                            </li>
                            {cavet.photos && cavet.photos.map(photo => <li key={photo.id}>
                                <div className="obj-photo" style={{ backgroundImage: `url(${photo.url})` }}><a className="func-remove" onClick={() => this.removeCarPaper("cv", photo)}><i className="ic ic-remove"></i></a></div>
                            </li>)}
                        </ul>
                        <div className="line"></div>
                    </div>
                    <div className="space m"></div>
                    <h3 className="title">Đăng kiểm</h3>
                    <div className="space m"></div>
                    <div className="list-photos">
                        <ul>
                            <li>
                                <a className="obj-photo">
                                    <span className="ins">
                                        {this.state.dangkiem.err !== commonErr.LOADING && <ImageUpload withPreview={false} onChange={this.hanldeDangkiemFileInputChange.bind(this)} className="form-control" />}
                                        {this.state.dangkiem.err === commonErr.LOADING && <LoadingInline />}
                                        <MessageBox show={this.state.dangkiem.err < commonErr.SUCCESS} hideModal={this.hideDangkiemMessageBox.bind(this)} message={this.state.dangkiem.errMsg} />
                                    </span>
                                </a>
                            </li>
                            {dangkiem.photos && dangkiem.photos.map(photo => <li key={photo.id}>
                                <div className="obj-photo" style={{ backgroundImage: `url(${photo.url})` }}><a className="func-remove" onClick={() => this.removeCarPaper("dk", photo)}><i className="ic ic-remove"></i></a></div>
                            </li>)}
                        </ul>
                        <div className="line"></div>
                    </div>
                    <div className="space m"></div>
                    <h3 className="title">Bảo hiểm dân sự</h3>
                    <div className="space m"></div>
                    <div className="list-photos">
                        <ul>
                            <li>
                                <a className="obj-photo">
                                    <span className="ins">
                                        {this.state.baohiem.err !== commonErr.LOADING && <ImageUpload withPreview={false} onChange={this.hanldeBaohiemFileInputChange.bind(this)} className="form-control" />}
                                        {this.state.baohiem.err === commonErr.LOADING && <LoadingInline />}
                                        <MessageBox show={this.state.baohiem.err < commonErr.SUCCESS} hideModal={this.hideBaohiemMessageBox.bind(this)} message={this.state.baohiem.errMsg} />
                                    </span>
                                </a>
                            </li>
                            {baohiem.photos && baohiem.photos.map(photo => <li key={photo.id}>
                                <div className="obj-photo" style={{ backgroundImage: `url(${photo.url})` }}><a className="func-remove" onClick={() => this.removeCarPaper("ds", photo)}><i className="ic ic-remove"></i></a></div>
                            </li>)}
                        </ul>
                    </div>
                    <div className="space m"></div>
                    {/* <h3 className="title">Số khung</h3>
                    <div className="space m"></div>
                    <div className="list-photos">
                        <ul>
                            <li>
                                <a className="obj-photo">
                                    <span className="ins">
                                        {this.state.sokhung.err !== commonErr.LOADING && <ImageUpload withPreview={false} onChange={this.hanldeSokhungFileInputChange.bind(this)} className="form-control" />}
                                        {this.state.sokhung.err === commonErr.LOADING && <LoadingInline />}
                                        <MessageBox show={this.state.sokhung.err < commonErr.SUCCESS} hideModal={this.hideSokhungMessageBox.bind(this)} message={this.state.sokhung.errMsg} />
                                    </span>
                                </a>
                            </li>
                            {sokhung.photos && sokhung.photos.map(photo => <li key={photo.id}>
                                <div className="obj-photo" style={{ backgroundImage: `url(${photo.url})` }}><a className="func-remove" onClick={() => this.removeCarPaper("sk", photo)}><i className="ic ic-remove"></i></a></div>
                            </li>)}
                        </ul>
                    </div> */}
                </div>
            </div>
        }

        return content;
    }
} 