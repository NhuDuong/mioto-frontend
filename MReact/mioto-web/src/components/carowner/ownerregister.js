import React from "react"
import moment from 'moment'
import ReactInterval from "react-interval"
import Header from "../common/header"
import Footer from "../common/footer"

import { MessageBox } from "../common/messagebox"
import { LoadingOverlay } from "../common/loading"
import { commonErr } from "../common/errors"
import { LoadingInline } from "../common/loading"
import { MessagePage } from "../common/messagebox"
import { VNCitiesMap, isPhoneValid, isEmailValid } from "../common/common"
import { submitFormOwnerRegister } from "../../model/common"

import bg_main from "../../static/images/background/bg-owner-register.jpg"
import bg_secondary from "../../static/images/background/bg-cta-register.jpg"

export default class OwnerRegister extends React.Component {
    constructor(props) {
        super(props);
        this.state = { 
            err: commonErr.INNIT,
            errMsg: "",
            isSubmittedSuccess: false,
            name: "",
            phoneNumber: "",
            city: "",
            email: "",
            isRental: 0,
            reference: "",
            carName: ""
        }  
        
        this.handleInputChange = this.handleInputChange.bind(this);
    }

    handleInputChange(event) {
		this.setState({
			[event.target.name]: event.target.value
        });
    }
    
    refreshCheckFilled(){
        this.setState({
            isNameFilled: true,
            isPhoneFilled: true,
            isCityFilled: true,
            isEmailFilled: true,
        })
    }

    componentDidMount() {
        window.scrollTo(0, 0);
        this.refreshCheckFilled();
    }

    hideMessageBox() {
        this.setState({
            err: commonErr.INNIT,
            errMsg: ""
        });
    }

    isValidInfo(){
        this.refreshCheckFilled();
        if(this.state.city === "" || !this.state.city){
            this.setState({
                isCityFilled: false,
            })
            return "Bạn chưa chọn thành phố."
        }
        if(this.state.name === "" || !this.state.name){
            this.setState({
                isNameFilled: false,
            })
            return "Bạn chưa điền tên."
        }
        if(this.state.phoneNumber === "" || !this.state.phoneNumber){
            this.setState({
                isPhoneFilled: false,
            })
            return "Bạn chưa điền số điện thoại."
        }         
        if(!isPhoneValid(this.state.phoneNumber)){
            this.setState({
                isPhoneFilled: false,
            })
            return "Số điện thoại không hợp lệ."
        } 
        if(this.state.email !== ""){
            if(!isEmailValid(this.state.email)){
                this.setState({
                    isEmailFilled: false,
                })
                return "Email không hợp lệ."
            }
        }
        return "";
    }

    onSubmitFormRegister = (event) => {
        const message = this.isValidInfo();
        if (message !== "") {
            this.setState({
                err: commonErr.FAIL,
                errMsg: message
            })
        } else {
            const data = {
                name: this.state.name,
                phoneNumber: this.state.phoneNumber,
                city: this.state.city,
                email: this.state.email,
                isRental: this.state.isRental,
                reference: this.state.reference,
                carName: this.state.carName
            }            
        
            var self = this;
            this.setState({ err: commonErr.LOADING });
            submitFormOwnerRegister(data).then(resp => {
                self.setState({
                    err: resp.data.error,
                    errMsg: resp.data.errorMessage,
                });
                if(resp.data.error == commonErr.SUCCESS){
                    self.setState({
                        isSubmittedSuccess: true,
                        errMsg: this.renderMessage(),
                    });
                }
                else {
                    self.setState({
                        isSubmittedSuccess: false
                    });
                }
            });       
        }
    }

    renderMessage() {
        return (
            <div className='message-box'>  
                Gửi yêu cầu thành công
                <p>Yêu cầu đăng kí cho thuê xe của bạn đã được gửi.</p>
                <p>Bộ phận phát triển kinh doanh của Mioto sẽ liên hệ bạn để tư vấn quy trình và hướng dẫn đăng xe trong thời gian sớm nhất. 
                Bạn cũng có thể đăng xe trực tiếp bằng cái tải ứng dụng Mioto và đăng xe theo hướng dẫn tại mục "Xe của tôi".</p>
                <p>Để được hỗ trợ, vui lòng gọi hotline 19009217 (T2-T7, 9AM-6PM).</p>
                <p>Xin cám ơn,</p>
                <p>MIOTO TEAM</p>
            </div>
        )
    }

    render() { 
        var content;
        if (this.state.err === commonErr.LOADING) {
            content = <LoadingOverlay />
        } else {
            const { isNameFilled, isPhoneFilled, isCityFilled, isEmailFilled} = this.state;
            content = <section className="body">
                <div className="banner-home__sect owner-register__wrap" id="ownerRegister" style={{ backgroundImage: `url(${bg_main})` }}>
                    <div className="reg-container">
                    <div className="main-title"> 
                        <h2 className="reg-tt">Đăng ký cho thuê trên Mioto ngay để gia tăng thu nhập hấp dẫn!</h2>
                        <p>Hotline: 1900 9217 (T2-T7 9AM-6PM)</p>
                        <p>Hoặc để lại tin nhắn cho Mioto qua <a href="https://www.facebook.com/mioto.vn" target="_blank">Fanpage</a></p>
                        <div className="space m hide-on-med"></div>
                        <p>Mioto không thu phí khi bạn đăng xe. Chúng tôi chỉ thu phí vận hành khi có giao dịch.</p>
                    </div>
                    <div className="box-register">
                        <div className="form-register form-default"> 
                        <h4 className="title">Tư vấn đăng ký xe ngay </h4>
                        <p>Vui lòng điền thông tin đầy đủ và chính xác để Mioto liên hệ bạn sớm nhất </p>
                        <div className="form-group"> 
                            <div className="line-form"> 
                                <div className={isCityFilled ? "wrap-select" : "wrap-select has-error-form"}> 
                                <select required onChange={this.handleInputChange} name="city" value={this.state.city}>
                                    <option value="" hidden disabled>Thành phố đăng ký hoạt động với Mioto </option>
                                    {VNCitiesMap && VNCitiesMap.map(option => <option key={option.key} value={option.key}>{option.value}</option>)}
                                </select>
                                </div>
                            </div>
                            <div className="line-form w-60"> 
                                <div className={isNameFilled ? "wrap-input" : "wrap-input has-error-form"}>
                                <input type="text" placeholder="Họ và tên*" name="name" value={this.state.name} onChange={this.handleInputChange}/>
                                </div>
                            </div>
                            <div className="line-form w-40"> 
                                <div className={isPhoneFilled ? "wrap-input" : "wrap-input has-error-form"}>
                                <input type="text" placeholder="Số di động*" name="phoneNumber" value={this.state.phoneNumber} onChange={this.handleInputChange}/>
                                </div>
                            </div>
                            <div className="line-form"> 
                                <div className={isEmailFilled ? "wrap-input" : "wrap-input has-error-form"}>
                                <input type="text" placeholder="Địa chỉ email (nếu có)" name="email" value={this.state.email} onChange={this.handleInputChange}/>
                                </div>
                            </div>
                            <div className="line-form"> 
                                <div className="wrap-input">
                                    <input type="text" placeholder="Xe đăng ký cho thuê" name="carName" value={this.state.carName} onChange={this.handleInputChange}/>
                                </div>
                            </div>
                            <div className="line-form"> 
                                <div className="wrap-select">
                                <select required name="reference" onChange={this.handleInputChange} value={this.state.reference}>
                                    <option value="" hidden disabled>Nguồn giới thiệu </option>
                                    <option value="1">Chủ xe Mioto</option>
                                    <option value="2">Bạn bè</option>
                                    <option value="3">Báo chí</option>
                                    <option value="4">Diễn đàn/Blog/Mạng xã hội</option>
                                    <option value="5">Quảng cáo/Tìm kiếm</option>
                                    <option value="6">Khác</option>
                                </select>
                                </div>
                            </div>
                            <div className="line-form local"> 
                            <label>Đã từng cho thuê xe chưa? </label>
                            <div className="line-radio">
                            <label className="custom-radio custom-control">
                                <input className="custom-control-input" type="radio" name="isRental" value="1" onChange={this.handleInputChange} checked={this.state.isRental == 1}/><span className="custom-control-indicator"></span><span className="custom-control-description">Đã từng</span>
                            </label>
                            <label className="custom-radio custom-control">
                                <input className="custom-control-input" type="radio" name="isRental" value="0" onChange={this.handleInputChange} checked={this.state.isRental == 0 || !this.state.isRental}/><span className="custom-control-indicator"></span><span className="custom-control-description">Chưa từng</span>
                            </label>
                            </div>
                        </div>
                        </div>
                        <p className="read-paper">
                            Khi tiếp tục, tôi đồng ý Mioto được phép thu thập, sử dụng và tiết lộ thông tin được tôi cung cấp theo <a href="https://www.mioto.vn/regu" target="_blank">Quy chế hoạt động</a> của Mioto mà tôi đã đọc và hiểu</p><a className="btn btn--m btn-primary" onClick={this.onSubmitFormRegister.bind(this)}>Đăng ký </a>
                        </div>
                    </div>
                    </div>
                </div>
                <div className="reg-tutorial__sect">
                    <div className="ex-container"> 
                    <h3 className="n-title">Đăng ký cho thuê trên Mioto trong 3 bước </h3>
                    <div className="reg-step__wrap"> 
                        <div className="reg-step__item"> 
                            <div className="ict-large ict-register"></div>
                            <div className="step-detail"> 
                            <h4>Bước 1</h4>
                            <div className="dt-step"> <span>Cách 1</span>
                                <p className="dt">Trực tiếp tải ứng dụng Mioto và đăng xe tại mục "Xe của tôi" </p>
                            </div>
                            <div className="dt-step"> <span>Cách 2</span>
                                <p className="dt">Đăng kí thông tin đầy đủ để Mioto hỗ trợ đăng xe giúp bạn</p>
                            </div>
                            </div>
                        </div>
                        <div className="reg-step__item"> 
                            <div className="ict-large ict-checked"></div>
                            <div className="step-detail"> 
                            <h4>Bước 2</h4>
                            <p className="dt">Nhân viên Mioto sẽ kiểm tra thông tin, liên hệ chủ xe để tư vấn thủ tục và quy trình cho thuê xe</p>
                            </div>
                        </div>
                        <div className="reg-step__item"> 
                            <div className="ict-large ict-accept"></div>
                            <div className="step-detail"> 
                            <h4>Bước 3</h4>
                            <p className="dt">Bắt đầu cho thuê trên Mioto ngay khi nhận thông báo xe được phê duyệt từ Mioto</p>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
                <div className="reg-cact__sect" style={{ backgroundImage: `url(${bg_secondary})` }}>
                    <div className="inside ex-container">
                    <h3 className="n-title">Tham gia Mioto ngay hôm nay! </h3>
                        <p>Gia nhập hệ thống chủ xe của Mioto để kiếm thêm thu nhập từ chính xe của bạn một cách chủ động </p>
                        <a href="#ownerRegister" className="btn btn--l btn-primary">Đăng ký ngay</a>
                    </div>
                </div>
                <MessageBox show={this.state.err <= commonErr.SUCCESS} hideModal={this.hideMessageBox.bind(this)} message={this.state.errMsg} error={this.state.err} />
            </section>
        }

        return <div className="mioto-layout">
            <Header />
            {content}
            <Footer />
        </div>
    }
}