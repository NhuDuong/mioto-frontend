import React from "react"
import moment from "moment"
import { connect } from "react-redux"
import DatePicker from "react-datepicker"
import "react-datepicker/dist/react-datepicker-cssmodules.css"

import { StickyContainer, Sticky } from "react-sticky"
import { Modal } from "react-bootstrap"
import { Link } from "react-router-dom"
import NumberFormat from "react-number-format"

import Header from "../common/header"
import { commonErr } from "../common/errors"
import { buildMonthCalendar, formatPrice, getTripStatus } from "../common/common"
import { getCarSetting, updateCar, getCarCalendar, getOwnerCars, getTripDetail } from "../../model/car"
import { getBalance } from '../../model/wallet'
import { MessageLine } from "../common/messagebox"
import { LoadingInline } from "../common/loading"
import CarItem from "../common/caritem"

import car_default from "../../static/images/upload/car_1.png"
import avatar_default from "../../static/images/avatar_default.png"

const settingModes = {
    price: 0,
    calendar: 1,
    trip: 2
}

//children components
const FilterControl = (props) => {
    const filterText = props.filterText;
    const onFilterTextChange = props.onFilterTextChange;
    const sortType = props.sortType;
    const onSortTypeChange = props.onSortTypeChange;
    const isDesSort = props.isDesSort;
    const onSortWayToggle = props.onSortWayToggle;

    return <div className="filter-map-container form-default">
        <div className="switch-sort">
            <a className="btn btn--m" onClick={onSortWayToggle}>{isDesSort ? <i className="ic ic-sort-increase"> </i> : <i className="ic ic-sort-descend"> </i>}</a>
        </div>
        <div className="col-1">
            <div className="line-form">
                <div className="wrap-input has-ico-search"><i className="ic ic-search"></i>
                    <input type="text" placeholder="Lọc theo tên hoặc biển số" value={filterText} onChange={onFilterTextChange} />
                </div>
            </div>
        </div>
        <div className="col-2">
            <div className="line-form">
                <div className="wrap-select">
                    <select value={sortType} onChange={onSortTypeChange}>
                        <option value="st_name">Tên xe</option>
                        <option value="st_price">Giá mặc định</option>
                        <option value="st_lblade">Biển số</option>
                    </select>
                </div>
            </div>
        </div>
    </div>
}

const SettingModeControl = (props) => {
    const settingMode = props.settingMode;
    const setSettingMode = props.setSettingMode;

    return <div className="textAlign-center">
        <label className="custom-radio custom-control">
            <input className="custom-control-input" type="radio"
                name="setting-mode"
                value={settingModes.price}
                checked={settingMode === settingModes.price}
                onChange={() => setSettingMode(settingModes.price)} />
            <span className="custom-control-indicator"></span>
            <span className="custom-control-description">Tùy chỉnh giá</span>
        </label>

        <label className="custom-radio custom-control">
            <input className="custom-control-input" type="radio"
                name="setting-mode"
                value={settingModes.calendar}
                checked={settingMode === settingModes.calendar}
                onChange={() => setSettingMode(settingModes.calendar)} />
            <span className="custom-control-indicator"></span>
            <span className="custom-control-description">Thiết lập lịch bận</span>
        </label>

        <label className="custom-radio custom-control">
            <input className="custom-control-input" type="radio"
                name="setting-mode"
                value={settingModes.trip}
                checked={settingMode === settingModes.trip}
                onChange={() => setSettingMode(settingModes.trip)} />
            <span className="custom-control-indicator"></span>
            <span className="custom-control-description">Thông tin chuyến</span>
        </label>
    </div>
}

const CarDetailBox = (props) => {
    const isShowCarDetailBox = props.isShowCarDetailBox;
    const hideCarDetailBox = props.hideCarDetailBox;
    const activeCar = props.activeCar;

    return <Modal
        show={isShowCarDetailBox}
        onHide={hideCarDetailBox}
        dialogClassName="modal-sm modal-dialog"
    >
        <Modal.Header closeButton={true} closeLabel={""}>
            <Modal.Title>Thông tin xe</Modal.Title>
        </Modal.Header>
        {activeCar && <Modal.Body>
            <CarItem car={activeCar} />
            <div className="clear"></div>
            <div className="space m"></div>
            <p className="textAlign-center">
                <Link className="btn btn-primary btn--m" to={`/carsetting/${activeCar.id}#infosetting`}>Quản lý xe</Link>
            </p>
        </Modal.Body>}
    </Modal>
}

const DefaultPriceSettingBox = (props) => {
    const isShowDefaultPriceSettingBox = props.isShowDefaultPriceSettingBox;
    const hideDefaultPriceSettingBox = props.hideDefaultPriceSettingBox;
    const activeCar = props.activeCar;
    const err = props.err;
    const updateErrMsg = props.updateErrMsg;
    const defaultPrice = props.defaultPrice;
    const onDefaultPriceChange = props.onDefaultPriceChange;
    const isShowMultiApply = props.isShowMultiApply;
    const isMultiApply = props.isMultiApply;
    const onIsMultiApplyChange = props.onIsMultiApplyChange;
    const confirmDefaultPriceSetting = props.confirmDefaultPriceSetting;

    return <Modal
        show={isShowDefaultPriceSettingBox}
        onHide={hideDefaultPriceSettingBox}
        dialogClassName="modal-sm modal-dialog"
    >
        <Modal.Header closeButton={true} closeLabel={""}>
            <Modal.Title>Tuỳ chỉnh giá mặc định</Modal.Title>
        </Modal.Header>
        {activeCar && <Modal.Body>
            <div className="form-default form-s">
                <p className="notes">Giá Mioto đề xuất: {formatPrice(activeCar.setting.priceDailyRecommend)}</p>
                <div className="line-form">
                    <div className="wrap-input-label d-flex">
                        <div className="wrap-input">
                            <input type="text" value={defaultPrice / 1000} onChange={onDefaultPriceChange} />
                        </div>
                        <span className="phay"> K</span>
                    </div>
                </div>
                {err < commonErr.SUCCESS && updateErrMsg !== "" && <p style={{ color: "red" }}><i className="ic ic-error"></i> {updateErrMsg}</p>}
                <div className="space m"></div>
                <div className="line"></div>
                {isShowMultiApply && <div className="squaredFour have-label">
                    <input id="is-multi-apply-default-price" type="checkbox" checked={isMultiApply} onChange={onIsMultiApplyChange} />
                    <label htmlFor="is-multi-apply-default-price" style={{ color: "red" }}> Áp dụng chung cho tất cả các xe có đánh dấu.</label>
                    <div className="space m"></div>
                </div>}
                <div className="clear"></div>
                <div className="space m"></div>
                <p className="textAlign-center has-more-btn">
                    <a className="btn btn-primary btn--m" type="button" onClick={confirmDefaultPriceSetting}>Xác nhận</a>
                </p>
            </div>
        </Modal.Body>}
    </Modal>
}

const PriceSettingBox = (props) => {
    const err = props.err;
    const updateErrMsg = props.updateErrMsg;
    const activeCar = props.activeCar;
    const customPrice = props.customPrice;
    const onCustomPriceRangeChange = props.onCustomPriceRangeChange;
    const isRepeatPrice = props.isRepeatPrice;
    const onIsRepeatPriceChange = props.onIsRepeatPriceChange;
    const isLimitRepeatPrice = props.isLimitRepeatPrice;
    const onIsLimitRepeatPriceChange = props.onIsLimitRepeatPriceChange;
    const priceRepeatEndDate = props.priceRepeatEndDate;
    const onPriceRepeatEndDateChange = props.onPriceRepeatEndDateChange;
    const isShowMultiApply = props.isShowMultiApply;
    const isMultiApply = props.isMultiApply;
    const onIsMultiApplyChange = props.onIsMultiApplyChange;
    const confirmCustomPriceSetting = props.confirmCustomPriceSetting;

    return <div className="form-default form-s">
        <p className="notes">Nhập giá tuỳ chỉnh cho ngày này hoặc nhập 0 nếu muốn dùng giá mặc định. Giá Mioto đề xuất: {formatPrice(activeCar.setting.priceDailyRecommend)}</p>
        <div className="line-form">
            <div className="wrap-input-label d-flex">
                <div className="wrap-input">
                    <input type="text" value={customPrice / 1000} onChange={onCustomPriceRangeChange} />
                </div>
                <span className="phay"> K</span>
            </div>
        </div>
        {err < commonErr.SUCCESS && updateErrMsg !== "" && <p style={{ color: "red" }}><i className="ic ic-error"></i> {updateErrMsg}</p>}
        <div className="squaredFour have-label">
            <input id="is-repeat-price" type="checkbox" checked={isRepeatPrice} onChange={onIsRepeatPriceChange} />
            <label htmlFor="is-repeat-price"> Lặp lại hàng tuần</label>
        </div>
        <div className="space m"></div>
        {isRepeatPrice && <div className="squaredFour have-label">
            <input id="is-limit-repeat-price" type="checkbox" checked={isLimitRepeatPrice} onChange={onIsLimitRepeatPriceChange} />
            <label htmlFor="is-limit-repeat-price"> Giới hạn thời gian lặp lại</label>
            <div className="space m"></div>
        </div>}
        {isLimitRepeatPrice && <div className="line-form">
            <div className="wrap-input datepicker">
                <DatePicker
                    selected={moment(priceRepeatEndDate)}
                    timeFormat="HH:mm"
                    dateFormat="DD/MM/YYYY"
                    onChange={onPriceRepeatEndDateChange} />
            </div>
        </div>}
        <div className="line"></div>
        {isShowMultiApply && <div className="squaredFour have-label">
            <input id="is-multi-apply-unavails-price" type="checkbox" checked={isMultiApply} onChange={onIsMultiApplyChange} />
            <label htmlFor="is-multi-apply-unavails-price" style={{ color: "red" }}> Áp dụng chung cho tất cả các xe có đánh dấu.</label>
            <div className="space m"></div>
        </div>}
        <div className="clear"></div>
        <div className="space m"></div>
        <p className="textAlign-center has-more-btn">
            <a className="btn btn-primary btn--m" type="button" onClick={confirmCustomPriceSetting}>Xác nhận</a>
        </p>
    </div>
}

const CalendarSettingBox = (props) => {
    const isUnavail = props.isUnavail;
    const onIsUnavailChange = props.onIsUnavailChange;
    const isUnavailsRepeat = props.isUnavailsRepeat;
    const onIsUnavailsRepeatChange = props.onIsUnavailsRepeatChange;
    const isLimitUnavailsRepeat = props.isLimitUnavailsRepeat;
    const onIsLimitUnavailsRepeatChange = props.onIsLimitUnavailsRepeatChange;
    const unavailsRepeatEndDate = props.unavailsRepeatEndDate;
    const onUnavailRepeatEndDateChange = props.onUnavailRepeatEndDateChange;
    const isShowMultiApply = props.isShowMultiApply;
    const isMultiApply = props.isMultiApply;
    const onIsMultiApplyChange = props.onIsMultiApplyChange;
    const confirmCalendarSetting = props.confirmCalendarSetting;

    return <div className="form-default form-s">
        <div className="squaredFour have-label">
            <input id="is-unavail" type="checkbox" checked={isUnavail} onChange={onIsUnavailChange} />
            <label htmlFor="is-unavail"> Bận</label>
        </div>
        <div className="space m"></div>
        {isUnavail && <div className="squaredFour have-label">
            <input id="is-repeat-price" type="checkbox" checked={isUnavailsRepeat} onChange={onIsUnavailsRepeatChange} />
            <label htmlFor="is-repeat-price"> Lặp lại hàng tuần</label>
        </div>}
        <div className="space m"></div>
        {(isUnavail && isUnavailsRepeat) && <div className="squaredFour have-label">
            <input id="is-limit-repeat-price" type="checkbox" checked={isLimitUnavailsRepeat} onChange={onIsLimitUnavailsRepeatChange} />
            <label htmlFor="is-limit-repeat-price"> Giới hạn thời gian lặp lại</label>
            <div className="space m"></div>
        </div>}
        {(isUnavail && isLimitUnavailsRepeat) && <div className="line-form">
            <div className="wrap-input datepicker">
                <DatePicker
                    selected={moment(unavailsRepeatEndDate)}
                    timeFormat="HH:mm"
                    dateFormat="DD/MM/YYYY"
                    onChange={onUnavailRepeatEndDateChange} />
            </div>
        </div>}
        <div className="line"></div>
        {isShowMultiApply && <div className="squaredFour have-label">
            <input id="is-multi-apply-unavails" type="checkbox" checked={isMultiApply} onChange={onIsMultiApplyChange} />
            <label htmlFor="is-multi-apply-unavails" style={{ color: "red" }}> Áp dụng chung cho tất cả các xe có đánh dấu</label>
            <div className="space m"></div>
        </div>}
        <div className="clear"></div>
        <div className="space m"></div>
        <p className="textAlign-center has-more-btn">
            <a className="btn btn-primary btn--m" type="button" onClick={confirmCalendarSetting}>Xác nhận</a>
        </p>
    </div>
}

class TripDetailBox extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            err: commonErr.INNIT,
            errMsg: "",
        }
    }

    getTrips(day) {
        this.setState({
            err: commonErr.LOADING
        });

        const tripsId = [];
        var i;
        if (day.isBooked && day.bookedTripId !== "") {
            tripsId.push(day.bookedTripId);
        } else if (day.tripsReq && day.tripsReq.length > 0) {
            for (i = 0; i < day.tripsReq.length; ++i) {
                tripsId.push(day.tripsReq[i].tripInfo.tripId);
            }
        }
        if (tripsId.length > 0) {
            var tripsInfo = [];
            var profilesInfo = [];
            for (i = 0; i < tripsId.length; ++i) {
                getTripDetail(tripsId[i]).then((resp) => {
                    if (resp.data.error >= commonErr.SUCCESS && resp.data.data.trip) {
                        tripsInfo = tripsInfo.concat(resp.data.data.trip);
                        profilesInfo = profilesInfo.concat(resp.data.data.profiles);
                        if (tripsInfo.length === tripsId.length) {
                            this.setState({
                                err: commonErr.SUCCESS,
                                trips: tripsInfo,
                                profiles: profilesInfo
                            });
                        }
                    } else {
                        this.setState({
                            err: resp.data.error
                        });
                    }
                });
            }
        } else {
            this.setState({
                err: commonErr.SUCCESS
            });
        }
    }

    componentDidMount() {
        this.getTrips(this.props.activeDay);
    }

    componentWillReceiveProps(props) {
        if (this.props.activeCar !== props.activeCar
            || this.props.activeDay !== props.activeDay) {
            this.getTrips(props.activeDay);
        }
    }

    render() {
        const activeTs = this.props.activeTs;
        const tripsDetail = [];

        if (this.state.trips) {
            const trips = this.state.trips.filter(function (trip) {
                return (moment(activeTs).isSame(moment(trip.tripDateFrom).startOf("day"))
                    || moment(activeTs).isSame(moment(trip.tripDateTo).startOf("day"))
                    || moment(activeTs).isBetween(moment(trip.tripDateFrom).startOf("day"), moment(trip.tripDateTo).endOf("day")))
                    && trip.status < 20
            });
            if (trips && trips.length > 0) {
                for (var i = 0; i < trips.length; ++i) {
                    const trip = trips[i];
                    const status = getTripStatus(trip.status);
                    var profile;
                    if (this.state.profiles) {
                        const profiles = this.state.profiles.filter(function (item) {
                            return item.uid === trip.travelerId;
                        });
                        if (profiles && profiles.length > 0) {
                            profile = profiles[0];
                        }
                    }
                    if (trip && profile && status) {
                        tripsDetail.push({
                            trip: trip,
                            profile: profile,
                            status: status
                        });
                    }
                }
            }
        }

        if (this.state.err === commonErr.INNIT || this.state.err === commonErr.LOADING) {
            return <LoadingInline />
        } else if (tripsDetail.length === 0) {
            return <MessageLine message="Không tìm thấy thông tin chuyến." />
        } else {
            return tripsDetail.map((tripDetail) => {
                const trip = tripDetail.trip;
                const profile = tripDetail.profile;
                const status = tripDetail.status;

                return <div key={trip.id} className="form-default">
                    <div className="trip-box">
                        <div className="trip-footer" style={{ paddingBottom: "0px" }}>
                            <div className="status-trips">
                                <p><span className={status.class} /> {status.name}</p>
                            </div>
                            <div className="time">
                                <p>{moment(trip.timeBooked).fromNow()}</p>
                            </div>
                        </div>
                        <div className="trip-body">
                            <div className="left">
                                <div className="space m"></div>
                                <div>Bắt đầu: {moment(trip.tripDateFrom).format("HH:mm dddd, DD/MM/YYYY")}</div>
                                <div>Kết thúc: {moment(trip.tripDateTo).format("HH:mm dddd, DD/MM/YYYY")}</div>
                                <div className="space m"></div>
                                <div className="total-price">Tổng tiền: {formatPrice(trip.priceSummary.priceTotal)}</div>
                                <div className="space m"></div>
                                <div><a target="_blank" href={`/trip/detail/${trip.id}`}><i className="ic ic-info"></i> Xem chi tiết</a></div>
                            </div>
                            <div className="right">
                                <div className="space m"></div>
                                <div className="avatar">
                                    <div className="avatar-img">
                                        <a target="_blank" href={`/profile/${profile.uid}`}>
                                            <img src={`${(profile.avatar && profile.avatar.thumbUrl) ? profile.avatar.thumbUrl : avatar_default}`} alt="Mioto - Thuê xe tự lái" />
                                        </a>
                                    </div>
                                </div>
                                <div className="lstitle">{profile.name}</div>
                            </div>
                        </div>
                    </div>
                </div>
            })
        }
    }
}

const DaySettingBox = (props) => {
    const isShowDaySettingBox = props.isShowDaySettingBox;
    const hideDaySettingBox = props.hideDaySettingBox;
    const settingMode = props.settingMode;
    const setSettingMode = props.setSettingMode;

    var content;
    switch (settingMode) {
        case settingModes.trip:
            content = <TripDetailBox
                activeCar={props.activeCar}
                activeTs={props.activeTs}
                activeDay={props.activeDay} />
            break;
        case settingModes.calendar:
            content = <CalendarSettingBox
                err={props.err}
                updateErrMsg={props.updateErrMsg}
                activeCar={props.activeCar}
                isMultiApply={props.isMultiApply}
                onIsMultiApplyChange={props.onIsMultiApplyChange}
                confirmCalendarSetting={props.confirmCalendarSetting}

                isUnavail={props.isUnavail}
                onIsUnavailChange={props.onIsUnavailChange}
                isUnavailsRepeat={props.isUnavailsRepeat}
                onIsUnavailsRepeatChange={props.onIsUnavailsRepeatChange}
                isLimitUnavailsRepeat={props.isLimitUnavailsRepeat}
                onIsLimitUnavailsRepeatChange={props.onIsLimitUnavailsRepeatChange}
                unavailsRepeatEndDate={props.unavailsRepeatEndDate}
                onUnavailRepeatEndDateChange={props.onUnavailRepeatEndDateChange}
                isShowMultiApply={props.isShowMultiApply} />
            break;
        default:
            content = <PriceSettingBox
                err={props.err}
                updateErrMsg={props.updateErrMsg}
                activeCar={props.activeCar}
                isMultiApply={props.isMultiApply}
                onIsMultiApplyChange={props.onIsMultiApplyChange}
                confirmCustomPriceSetting={props.confirmCustomPriceSetting}

                customPrice={props.customPrice}
                onCustomPriceRangeChange={props.onCustomPriceRangeChange}
                isRepeatPrice={props.isRepeatPrice}
                onIsRepeatPriceChange={props.onIsRepeatPriceChange}
                isLimitRepeatPrice={props.isLimitRepeatPrice}
                onIsLimitRepeatPriceChange={props.onIsLimitRepeatPriceChange}
                priceRepeatEndDate={props.priceRepeatEndDate}
                onPriceRepeatEndDateChange={props.onPriceRepeatEndDateChange}
                isShowMultiApply={props.isShowMultiApply} />
    }

    return <Modal
        show={isShowDaySettingBox}
        onHide={hideDaySettingBox}
        dialogClassName="modal-sm modal-dialog"
    >
        <Modal.Header closeButton={true} closeLabel={""}>
            <Modal.Title>{moment(props.activeTs).format("dddd, ngày DD/MM")}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
            <SettingModeControl
                settingMode={settingMode}
                setSettingMode={setSettingMode} />
            <div className="space m"></div>
            <div className="space m"></div>
            {content}
        </Modal.Body>
    </Modal>
}

//main component
class Calendars extends React.Component {
    constructor() {
        super();

        this.state = {
            err: commonErr.INNIT,
            errMsg: "",
            cars: [],
            time: moment().startOf("week"),
            maxDay: 14,
            settingMode: settingModes.price,
            defaultPrice: 0,
            customPrice: 0,
            isMultiApply: false,
            isShowMultiApply: false,
            filterText: "",
            filterStatus: 0,
            sortType: "st_name",
            isDesSort: true,
            isHiddenPrev: true,
            isHiddenNext: false
        }

        this.onFilterTextChange = this.onFilterTextChange.bind(this);
        this.onSortTypeChange = this.onSortTypeChange.bind(this);
        this.onSortWayToggle = this.onSortWayToggle.bind(this);
        this.hideMessageBox = this.hideMessageBox.bind(this);
        this.prevTime = this.prevTime.bind(this);
        this.nextTime = this.nextTime.bind(this);
        this.setSettingMode = this.setSettingMode.bind(this);
        this.onCarCheckboxClick = this.onCarCheckboxClick.bind(this);
        this.onDayItemClick = this.onDayItemClick.bind(this);
        this.onIsMultiApplyChange = this.onIsMultiApplyChange.bind(this);

        this.showCarDetailBox = this.showCarDetailBox.bind(this);
        this.hideCarDetailBox = this.hideCarDetailBox.bind(this);
        this.showTripDetailBox = this.showTripDetailBox.bind(this);

        this.showDaySettingBox = this.showDaySettingBox.bind(this);
        this.hideDaySettingBox = this.hideDaySettingBox.bind(this);
        this.hideDefaultPriceSettingBox = this.hideDefaultPriceSettingBox.bind(this);
        this.onDefaultPriceChange = this.onDefaultPriceChange.bind(this);
        this.setDefaultPrice = this.setDefaultPrice.bind(this);
        this.confirmDefaultPriceSetting = this.confirmDefaultPriceSetting.bind(this);
        this.updateCarDefaultPrice = this.updateCarDefaultPrice.bind(this);

        this.setCustomPrice = this.setCustomPrice.bind(this);
        this.onCustomPriceRangeChange = this.onCustomPriceRangeChange.bind(this);
        this.confirmCustomPriceSetting = this.confirmCustomPriceSetting.bind(this);
        this.updateCarCustomPrice = this.updateCarCustomPrice.bind(this);
        this.onIsRepeatPriceChange = this.onIsRepeatPriceChange.bind(this);
        this.onIsLimitRepeatPriceChange = this.onIsLimitRepeatPriceChange.bind(this);
        this.onPriceRepeatEndDateChange = this.onPriceRepeatEndDateChange.bind(this);

        this.setCalendar = this.setCalendar.bind(this);
        this.updateCarCalendar = this.updateCarCalendar.bind(this);
        this.confirmCalendarSetting = this.confirmCalendarSetting.bind(this);
        this.onIsUnavailChange = this.onIsUnavailChange.bind(this);
        this.onIsUnavailsRepeatChange = this.onIsUnavailsRepeatChange.bind(this);
        this.onIsLimitUnavailsRepeatChange = this.onIsLimitUnavailsRepeatChange.bind(this);
        this.onUnavailRepeatEndDateChange = this.onUnavailRepeatEndDateChange.bind(this);
    }

    componentDidMount() {
        const self = this;

        getOwnerCars(0, 0, 0, 0, true).then(function (resp) {
            if (resp.data.data) {
                const cars = resp.data.data.cars;
                if (resp.data.error >= commonErr.SUCCESS && cars) {
                    for (var i = 0; i < cars.length; ++i) {
                        const car = cars[i];
                        getCarCalendar(car.id, "cs").then(function (calendarResp) {
                            const calendar = calendarResp.data.data;
                            if (calendar) {
                                getCarSetting({ carId: car.id }).then(function (settingResp) {
                                    const setting = settingResp.data.data;
                                    if (setting) {
                                        const unavailDays = [];
                                        const bookedDays = [];
                                        const priceSpecifics = [];
                                        const priceRepeat = [];
                                        const unavailsRepeat = [];

                                        if (calendar) {
                                            var i, j;

                                            if (calendar.unavailsAfter && calendar.unavailsAfter > calendar.unavailsBefore) {
                                                const start = moment(calendar.startDate).startOf('day').valueOf();
                                                const end = moment(calendar.endDate).endOf('month').valueOf();
                                                for (i = start; i < moment(calendar.unavailsBefore * 1000 + moment().valueOf()).startOf('day').valueOf(); i += (3600000 * 24)) {
                                                    unavailDays.push(i);
                                                }
                                                const endOfAvailsDay = calendar.unavailsAfter * 1000 + moment().valueOf();
                                                for (i = moment(endOfAvailsDay + 24 * 3600 * 1000).startOf('day').valueOf(); i <= end; i += (3600000 * 24)) {
                                                    unavailDays.push(i);
                                                }
                                            }

                                            if (calendar.unavails) {
                                                for (i = 0; i < calendar.unavails.length; ++i) {
                                                    const start = moment(calendar.unavails[i].startTime).startOf('day').valueOf();
                                                    const end = moment(calendar.unavails[i].endTime).startOf('day').valueOf();
                                                    for (j = start; j <= end; j += (3600000 * 24)) {
                                                        unavailDays.push(j);
                                                    }
                                                }
                                            }

                                            if (calendar.bookings) {
                                                for (i = 0; i < calendar.bookings.length; ++i) {
                                                    const start = moment(calendar.bookings[i].startTime).startOf('day').valueOf();
                                                    const end = moment(calendar.bookings[i].endTime).startOf('day').valueOf();
                                                    for (j = start; j <= end; j += (3600000 * 24)) {
                                                        bookedDays.push({
                                                            ts: j,
                                                            tripId: calendar.bookings[i].tripId,
                                                            startTs: calendar.bookings[i].startTime,
                                                            endTs: calendar.bookings[i].endTime
                                                        });
                                                    }
                                                }
                                            }

                                            if (calendar.priceSpecifics) {
                                                for (i = 0; i < calendar.priceSpecifics.length; ++i) {
                                                    const start = moment(calendar.priceSpecifics[i].startTime).startOf('day').valueOf();
                                                    const end = moment(calendar.priceSpecifics[i].endTime).startOf('day').valueOf();
                                                    for (j = start; j <= end; j += (3600000 * 24)) {
                                                        priceSpecifics.push({
                                                            ts: j,
                                                            price: calendar.priceSpecifics[i].price
                                                        });
                                                    }
                                                }
                                            }

                                            for (i = 1; i <= 7; ++i) {
                                                const weekday = {
                                                    weekday: i,
                                                    price: 0
                                                };

                                                if (calendar.priceRepeat) {
                                                    for (j = 0; j < calendar.priceRepeat.length; ++j) {
                                                        if (i === 7 && calendar.priceRepeat[j].weekday === 1) {
                                                            weekday.price = calendar.priceRepeat[j].price;
                                                            break;
                                                        } else if ((i + 1) === calendar.priceRepeat[j].weekday) {
                                                            weekday.price = calendar.priceRepeat[j].price;
                                                            break;
                                                        }
                                                    }
                                                }

                                                priceRepeat.push(weekday);
                                            }

                                            for (i = 1; i <= 7; ++i) {
                                                const weekday = {
                                                    weekday: i,
                                                    unavail: false
                                                };

                                                if (calendar.unavailsRepeatWeekday) {
                                                    for (j = 0; j < calendar.unavailsRepeatWeekday.length; ++j) {
                                                        if (i === 7 && calendar.unavailsRepeatWeekday[j] === 1) {
                                                            weekday.unavail = true;
                                                            break;
                                                        } else if ((i + 1) === calendar.unavailsRepeatWeekday[j]) {
                                                            weekday.unavail = true;
                                                            break;
                                                        }
                                                    }
                                                }

                                                unavailsRepeat.push(weekday);
                                            }
                                        }

                                        const isLimitRepeatPrice = calendar.priceRepeatEndDate > 0;
                                        const priceRepeatEndDate = calendar.priceRepeatEndDate > 0 ? calendar.priceRepeatEndDate : moment().add(1, 'month').valueOf();
                                        const isLimitUnavailsRepeat = calendar.unavailsRepeatEndDate > 0;
                                        const unavailsRepeatEndDate = calendar.unavailsRepeatEndDate > 0 ? calendar.unavailsRepeatEndDate : moment().add(1, 'month').valueOf();

                                        car.checked = false;
                                        car.calendar = calendar;
                                        car.setting = setting;
                                        car.priceSpecifics = priceSpecifics;
                                        car.unavailDays = unavailDays;
                                        car.bookedDays = bookedDays;
                                        car.priceRepeat = priceRepeat;
                                        car.priceRepeatEndDate = priceRepeatEndDate;
                                        car.isLimitRepeatPrice = isLimitRepeatPrice;
                                        car.unavailsRepeat = unavailsRepeat;
                                        car.unavailsRepeatEndDate = unavailsRepeatEndDate;
                                        car.isLimitUnavailsRepeat = isLimitUnavailsRepeat;

                                        self.setState({
                                            cars: self.state.cars.concat(car)
                                        });
                                    }
                                });
                            }
                        });
                    }
                }
            } else {
                self.setState({
                    err: resp.data.error,
                    errMsg: resp.data.errorMessage
                });
            }
        });
        
        getBalance().then(function (resp){
			if (resp.data.error === commonErr.SUCCESS) {
				self.setState({
					balance: resp.data.data.balance
				})
			} else {
				self.setState({
					err: resp.data.error,
				})
			}
		})

        window.addEventListener("resize", this.updateDimensions.bind(this));

        this.widthToMaxDay();
    }

    //common
    updateDimensions() {
        this.widthToMaxDay();
    }

    hideMessageBox() {
        this.setState({
            err: commonErr.INNIT,
            errMsg: ""
        });
    }

    widthToMaxDay() {
        if (window.innerWidth <= 500) {
            this.setState({
                invalidWidth: true,
                maxDay: 0
            });
        } else {
            var newMaxDay;
            if (window.innerWidth <= 991) {
                newMaxDay = 7;
            } else if (window.innerWidth <= 1680) {
                newMaxDay = 14;
            } else {
                const delta = window.innerWidth - 1680;
                newMaxDay = 14 + (7 * ((delta - (delta % 500)) / 500));
            }
            if (newMaxDay !== this.state.maxDay) {
                this.setState({
                    maxDay: newMaxDay,
                    invalidWidth: false
                })
            }
        }
    }

    prevTime() {
        const time = moment(this.state.time).subtract(7, "days");
        this.setState({
            time: time,
            isHiddenPrev: moment(time).startOf("week").isBefore(moment(), "day"),
            isHiddenNext: false
        });
    }

    nextTime() {
        const time = moment(this.state.time).add(7, "days")
        this.setState({
            time: time,
            isHiddenNext: moment(time).endOf("week").isAfter(moment().add(5, "months"), "day"),
            isHiddenPrev: false
        });
    }

    onFilterTextChange(event) {
        this.setState({
            filterText: event.target.value
        });
    }

    onSortTypeChange(event) {
        this.setState({
            sortType: event.target.value
        });
    }

    onFilterStatusChange(event) {
        this.setState({
            filterStatus: event.target.value
        });
    }

    onSortWayToggle() {
        this.setState(oldState => ({
            isDesSort: !oldState.isDesSort
        }))
    }

    setSettingMode(mode) {
        this.setState({
            settingMode: mode
        });
    }

    onCarCheckboxClick(event) {
        const cars = this.state.cars;
        for (var i = 0; i < cars.length; ++i) {
            const carId = event.target.id.replace("cb_", "");
            if (carId === cars[i].id) {
                cars[i].checked = event.target.checked
            }
        }
        const checkedCars = cars.filter(function (car) {
            return car.checked === true;
        });

        this.setState({
            cars: cars,
            isShowMultiApply: (checkedCars.length >= 2)
        });
    }

    onDayItemClick(car, day) {
        this.showDaySettingBox(car, day);
    }

    onIsMultiApplyChange(event) {
        this.setState({
            isMultiApply: event.target.checked
        });
    }

    //car detail
    showCarDetailBox(car) {
        this.setState({
            isShowCarDetailBox: true,
            activeCar: car
        });
    }

    hideCarDetailBox() {
        this.setState({
            isShowCarDetailBox: false
        });
    }

    //trip detail
    showTripDetailBox(car, day) {
        this.setState({
            isShowDaySettingBox: true,
            settingMode: settingModes.trip,
            activeCar: car,
            activeTs: day.ts,
            activeDay: day
        });
    }

    //default price
    hideDefaultPriceSettingBox() {
        this.setState({
            isShowDefaultPriceSettingBox: false,
            err: commonErr.INNIT,
            errMsg: ""
        });
    }

    onDefaultPriceChange(event) {
        if (isNaN(event.target.value)) {
            return;
        }

        const price = event.target.value * 1000;
        this.setState({
            err: commonErr.SUCCESS,
            updateErrMsg: "",
            defaultPrice: price
        });
    }

    setDefaultPrice(car, price) {
        this.setState({
            isShowDefaultPriceSettingBox: true,
            activeCar: car,
            defaultPrice: price
        });
    }

    confirmDefaultPriceSetting() {
        const isMultiApply = this.state.isMultiApply;
        const activeCar = this.state.activeCar;
        const cars = this.state.cars.slice();
        var i;

        //check valid price
        for (i = 0; i < cars.length; ++i) {
            const car = cars[i];
            if (car.id === activeCar.id || (isMultiApply && car.checked === true)) {
                const setting = car.setting;
                const defaultPrice = this.state.defaultPrice;
                if (defaultPrice < setting.priceDailyMin || defaultPrice > setting.priceDailyMax) {
                    const err = commonErr.FAIL;
                    const updateErrMsg = `Không thể cập nhật. Giá mặc định của xe ${car.name} chỉ được phép trong khoản từ ${formatPrice(setting.priceDailyMin)} đến ${formatPrice(setting.priceDailyMax)}.`;
                    this.setState({
                        err: err,
                        updateErrMsg: updateErrMsg
                    });
                    return;
                }
            }
        }

        for (i = 0; i < cars.length; ++i) {
            const car = cars[i];
            if (car.id === activeCar.id || (isMultiApply && car.checked === true)) {
                const defaultPrice = this.state.defaultPrice;
                car.calendar.priceDaily = defaultPrice;
                this.updateCarDefaultPrice(car);
            }
        }

        this.setState({
            cars: cars,
            isShowDefaultPriceSettingBox: false
        });
    }

    updateCarDefaultPrice(car) {
        updateCar(car.id, {
            priceDaily: car.calendar.priceDaily
        }).then((resp) => {
            this.setState({
                err: resp.data.error,
                updateErrMsg: resp.data.errorMessage
            });
        });
    }

    //day setting
    showDaySettingBox(car, day) {
        if (day.isBooked && !day.isLeftHalfBooked && !day.isRightHalfBooked) {
            this.showTripDetailBox(car, day);
        } else if (day.isUnavail) {
            this.setCalendar(car, day);
        } else {
            this.setCustomPrice(car, day);
        }
    }

    hideDaySettingBox() {
        this.setState({
            isShowDaySettingBox: false,
            err: commonErr.INNIT,
            errMsg: ""
        });
    }

    //custom price
    setCustomPrice(car, day) {
        var isRepeatPrice = false;
        var isLimitRepeatPrice = false;
        var priceRepeatEndDate = moment().add(2, "months").valueOf();

        if (car.priceRepeat) {
            const priceRepeat = car.priceRepeat.filter(function (weekday) {
                return (moment(day.ts).isoWeekday() === weekday.weekday && weekday.price > 0
                    && (car.priceRepeatEndDate === 0 || day.ts <= car.priceRepeatEndDate))
            });
            if (priceRepeat && priceRepeat.length > 0) {
                isRepeatPrice = true;
                isLimitRepeatPrice = car.isLimitRepeatPrice;
                if (car.priceRepeatEndDate > 0) {
                    priceRepeatEndDate = car.priceRepeatEndDate;
                }
            }
        }

        this.setState({
            isShowDaySettingBox: true,
            activeTs: day.ts,
            customPrice: day.price,
            activeDay: day,
            isRepeatPrice: isRepeatPrice,
            isLimitRepeatPrice: isLimitRepeatPrice,
            priceRepeatEndDate: priceRepeatEndDate,
            activeCar: car
        });
    }

    onCustomPriceRangeChange(event) {
        if (isNaN(event.target.value)) {
            return;
        }

        const price = event.target.value * 1000;
        this.setState({
            err: commonErr.SUCCESS,
            updateErrMsg: "",
            customPrice: price
        });
    }

    confirmCustomPriceSetting() {
        const ts = this.state.activeTs;
        const isRepeatPrice = this.state.isRepeatPrice;
        const isLimitRepeatPrice = this.state.isLimitRepeatPrice;
        const priceRepeatEndDate = isLimitRepeatPrice === true ? this.state.priceRepeatEndDate : 0;
        const isMultiApply = this.state.isMultiApply;
        const activeCar = this.state.activeCar;
        const cars = this.state.cars.slice();
        var customPrice;
        var i;

        //check valid price
        for (i = 0; i < cars.length; ++i) {
            const car = cars[i];
            if (car.id === activeCar.id || (isMultiApply && car.checked === true)) {
                const setting = car.setting;
                customPrice = car.calendar.priceDaily;
                if (this.state.customPrice !== undefined && this.state.customPrice > 0) {
                    customPrice = this.state.customPrice;
                }
                if (customPrice < setting.priceDailyMin || customPrice > setting.priceDailyMax) {
                    const err = commonErr.FAIL;
                    const updateErrMsg = `Không thể cập nhật. Giá tuỳ chỉnh của xe ${car.name} chỉ được phép trong khoản từ ${formatPrice(setting.priceDailyMin)} đến ${formatPrice(setting.priceDailyMax)}.`;
                    this.setState({
                        err: err,
                        updateErrMsg: updateErrMsg
                    });
                    return;
                }
            }
        }

        for (i = 0; i < cars.length; ++i) {
            const car = cars[i];
            if (car.id === activeCar.id || (isMultiApply && car.checked === true)) {
                const priceSpecifics = car.priceSpecifics.filter(function (day) {
                    return ts !== day.ts;
                });
                customPrice = car.calendar.priceDaily;
                if (this.state.customPrice !== undefined && this.state.customPrice > 0) {
                    customPrice = this.state.customPrice;
                    if (!isRepeatPrice) {
                        priceSpecifics.push({
                            ts: ts,
                            price: customPrice
                        });
                    }
                }

                const priceRepeat = car.priceRepeat.filter(function (weekday) {
                    return (moment(ts).isoWeekday() !== weekday.weekday);
                });

                priceRepeat.push({
                    weekday: moment(ts).isoWeekday(),
                    price: isRepeatPrice ? customPrice : 0
                });

                car.priceSpecifics = priceSpecifics;
                car.priceRepeat = priceRepeat;
                car.priceRepeatEndDate = priceRepeatEndDate;
                car.isLimitRepeatPrice = isLimitRepeatPrice;

                this.updateCarCustomPrice(car);
            }
        }

        this.setState({
            cars: cars,
            isShowDaySettingBox: false
        });
    }

    updateCarCustomPrice(car) {
        const self = this;
        updateCar(car.id, {
            priceSpecifics: car.priceSpecifics,
            priceRepeat: car.priceRepeat,
            priceRepeatEndDate: car.priceRepeatEndDate
        }).then(function (resp) {
            self.setState({
                err: resp.data.error,
                updateErrMsg: resp.data.errorMessage
            });
        });
    }

    onIsRepeatPriceChange(event) {
        this.setState({
            isRepeatPrice: event.target.checked,
            isLimitRepeatPrice: (!event.target.checked ? false : this.state.isLimitRepeatPrice)
        });
    }

    onIsLimitRepeatPriceChange(event) {
        this.setState({
            isLimitRepeatPrice: event.target.checked
        });
    }

    onPriceRepeatEndDateChange(date) {
        this.setState({
            priceRepeatEndDate: date.valueOf()
        });
    }

    //calendar
    setCalendar(car, day) {
        var isUnavailsRepeat = false;
        var isLimitUnavailsRepeat = false;
        var unavailsRepeatEndDate = moment().add(2, "months").valueOf();

        if (car.unavailsRepeat) {
            const unavailsRepeat = car.unavailsRepeat.filter(function (weekday) {
                return (moment(day.ts).isoWeekday() === weekday.weekday && weekday.unavail === true
                    && (car.unavailsRepeatEndDate === 0 || day.ts <= car.unavailsRepeatEndDate))
            });

            if (unavailsRepeat && unavailsRepeat.length > 0) {
                isUnavailsRepeat = true;
                isLimitUnavailsRepeat = car.isLimitUnavailsRepeat;
                if (car.unavailsRepeatEndDate > 0) {
                    unavailsRepeatEndDate = car.unavailsRepeatEndDate;
                }
            }
        }

        this.setState({
            isShowDaySettingBox: true,
            settingMode: settingModes.calendar,
            activeTs: day.ts,
            activeDay: day,
            isUnavail: day.isUnavail,
            isUnavailsRepeat: isUnavailsRepeat,
            isLimitUnavailsRepeat: isLimitUnavailsRepeat,
            unavailsRepeatEndDate: unavailsRepeatEndDate,
            activeCar: car
        });
    }

    updateCarCalendar(car) {
        const self = this;
        const unavailsRepeat = car.unavailsRepeat.filter(function (weekday) {
            return (weekday.unavail === true);
        });
        updateCar(car.id, {
            unavailDays: car.unavailDays,
            unavailsRepeat: unavailsRepeat,
            unavailsRepeatEndDate: car.unavailsRepeatEndDate
        }).then(function (resp) {
            self.setState({
                err: resp.data.error,
                updateErrMsg: resp.data.errorMessage
            });
        });
    }

    confirmCalendarSetting() {
        const ts = this.state.activeTs;
        const activeCar = this.state.activeCar;
        const isUnavail = this.state.isUnavail;
        const isUnavailsRepeat = this.state.isUnavailsRepeat;
        const isLimitUnavailsRepeat = this.state.isLimitUnavailsRepeat;
        const unavailsRepeatEndDate = isLimitUnavailsRepeat === true ? this.state.unavailsRepeatEndDate : 0;
        const isMultiApply = this.state.isMultiApply;
        const cars = this.state.cars.slice();

        for (var i = 0; i < cars.length; ++i) {
            const car = cars[i];
            if (car.id === activeCar.id || (isMultiApply && car.checked === true)) {
                const unavailDays = car.unavailDays.filter(function (day) {
                    return ts !== day;
                });

                if (!isUnavailsRepeat && isUnavail) {
                    unavailDays.push(ts);
                }

                const unavailsRepeat = car.unavailsRepeat.filter(function (weekday) {
                    return (moment(ts).isoWeekday() !== weekday.weekday);
                });

                unavailsRepeat.push({
                    weekday: moment(ts).isoWeekday(),
                    unavail: isUnavailsRepeat && isUnavail
                });

                car.unavailDays = unavailDays;
                car.unavailsRepeat = unavailsRepeat;
                car.unavailsRepeatEndDate = unavailsRepeatEndDate;
                car.isLimitUnavailsRepeat = isLimitUnavailsRepeat;

                this.updateCarCalendar(car);
            }
        }

        this.setState({
            cars: cars,
            isShowDaySettingBox: false
        });
    }

    onIsUnavailChange(event) {
        this.setState({
            isUnavail: event.target.checked
        });
    }

    onIsUnavailsRepeatChange(event) {
        this.setState({
            isUnavailsRepeat: event.target.checked
        });
    }

    onIsLimitUnavailsRepeatChange(event) {
        this.setState({
            isLimitUnavailsRepeat: event.target.checked
        });
    }

    onUnavailRepeatEndDateChange(date) {
        this.setState({
            unavailsRepeatEndDate: date.valueOf()
        });
    }

    render() {
        var content;
        if (this.state.invalidWidth) {
            content = <section className="body">
                <div className="sidebar-control z-2">
                    <div className="sidebar-settings general-settings">
                        <ul>
                            <li> <a href="/mycars"><i className="ic ic-cars"></i><span className="label">Danh sách xe</span></a></li>
                            <li> <a className="active" href="/calendars"><i className="ic ic-setting-calendar"></i><span className="label">Lịch chung</span></a></li>
                            <li> <a href="/gps"><i className="ic ic-gps"></i><span className="label">GPS</span><sup class="new-func">BETA</sup></a></li>
                            <li> <a href="/commonsetting"><i className="ic ic-setting-rent"> </i><span className="label">Cài đặt chung</span></a></li>
                            <li> <a href="/carregister"><i className="ic ic-plus-black"></i><span className="label">Đăng ký xe </span></a></li>
                        </ul>
                    </div>
                </div>
                <MessageLine message="Tính năng chỉ hỗ trợ cho màn hình có kích thước lớn hơn 500px. Vui lòng xoay ngang màn hình hoặc sử dụng màn hình lớn hơn." />
            </section>
        } else {
            const allCars = this.state.cars;
            var cars = [];
            var mainDays = [];
            const filterText = this.state.filterText;
            const sortType = this.state.sortType;
            const isDesSort = this.state.isDesSort;
            const time = this.state.time;
            const start = moment(time);
            const end = moment(time).add(this.state.maxDay - 1, "days");

            if (allCars.length > 0) {
                if (filterText && filterText !== "") {
                    cars = allCars.filter(function (car) {
                        return car.name.toLowerCase().indexOf(filterText.trim().toLowerCase()) !== -1
                            || car.licensePlate.toLowerCase().indexOf(filterText.trim().toLowerCase()) !== -1
                    });
                } else {
                    cars = allCars.slice();
                }
                if (cars.length > 0) {
                    if (sortType !== "") {
                        cars = cars.sort(function (car1, car2) {
                            if (sortType === "st_price") {
                                return car1.setting.priceDaily - car2.setting.priceDaily
                            } else if (sortType === "st_name") {
                                return car1.name.localeCompare(car2.name)
                            } else {
                                return car1.licensePlate.localeCompare(car2.licensePlate)
                            }
                        });
                    }
                    if (!isDesSort) {
                        cars = cars.reverse();
                    }

                    mainDays = buildMonthCalendar(start, end, cars[0].calendar.priceDaily, [], cars[0].unavailDays, cars[0].bookedDays,
                        cars[0].priceSpecifics, cars[0].priceRepeat, cars[0].priceRepeatEndDate, cars[0].isLimitRepeatPrice, cars[0].unavailsRepeat,
                        cars[0].unavailsRepeatEndDate, cars[0].isLimitUnavailsRepeat, cars[0].setting);
                }
            }

            content = <section className="body">
                <div className="sidebar-control z-2">
                    <div className="sidebar-settings general-settings">
                        <ul>
                            <li> <a href="/mycars"><i className="ic ic-cars"></i><span className="label">Danh sách xe</span></a></li>
                            <li> <a className="active" href="/calendars"><i className="ic ic-setting-calendar"></i><span className="label">Lịch chung</span></a></li>
                            <li> <a href="/gps"><i className="ic ic-gps"></i><span className="label">GPS</span><sup class="new-func">BETA</sup></a></li>
                            <li> <a href="/commonsetting"><i className="ic ic-setting-rent"> </i><span className="label">Cài đặt chung</span></a></li>
                            <li> <a href="/carregister"><i className="ic ic-plus-black"></i><span className="label">Đăng ký xe </span></a></li>
                            <li> <a href="/mywallet"><i className="ic ic-owner-wallet"></i><span className="label">Số dư: <span className="fontWeight-6 font-16"><NumberFormat value={this.state.balance} displayType={'text'} thousandSeparator={" "} decimalSeparator={","} suffix={"đ"} /></span></span></a></li>
                        </ul>
                    </div>
                </div>
                <CarDetailBox
                    isShowCarDetailBox={this.state.isShowCarDetailBox}
                    hideCarDetailBox={this.hideCarDetailBox}
                    activeCar={this.state.activeCar} />

                <DefaultPriceSettingBox
                    isShowDefaultPriceSettingBox={this.state.isShowDefaultPriceSettingBox}
                    hideDefaultPriceSettingBox={this.hideDefaultPriceSettingBox}
                    err={this.state.err}
                    updateErrMsg={this.state.updateErrMsg}
                    activeCar={this.state.activeCar}
                    defaultPrice={this.state.defaultPrice}
                    onDefaultPriceChange={this.onDefaultPriceChange}
                    isShowMultiApply={this.state.isShowMultiApply}
                    isMultiApply={this.state.isMultiApply}
                    onIsMultiApplyChange={this.onIsMultiApplyChange}
                    confirmDefaultPriceSetting={this.confirmDefaultPriceSetting} />

                {this.state.activeCar && <DaySettingBox
                    isShowDaySettingBox={this.state.isShowDaySettingBox}
                    hideDaySettingBox={this.hideDaySettingBox}
                    settingMode={this.state.settingMode}
                    setSettingMode={this.setSettingMode}

                    err={this.state.err}
                    updateErrMsg={this.state.updateErrMsg}
                    activeCar={this.state.activeCar}
                    isShowMultiApply={this.state.isShowMultiApply}
                    isMultiApply={this.state.isMultiApply}
                    onIsMultiApplyChange={this.onIsMultiApplyChange}
                    activeTs={this.state.activeTs}
                    activeDay={this.state.activeDay}

                    customPrice={this.state.customPrice}
                    onCustomPriceRangeChange={this.onCustomPriceRangeChange}
                    isRepeatPrice={this.state.isRepeatPrice}
                    onIsRepeatPriceChange={this.onIsRepeatPriceChange}
                    isLimitRepeatPrice={this.state.isLimitRepeatPrice}
                    onIsLimitRepeatPriceChange={this.onIsLimitRepeatPriceChange}
                    priceRepeatEndDate={this.state.priceRepeatEndDate}
                    onPriceRepeatEndDateChange={this.onPriceRepeatEndDateChange}
                    confirmCustomPriceSetting={this.confirmCustomPriceSetting}

                    isUnavail={this.state.isUnavail}
                    onIsUnavailChange={this.onIsUnavailChange}
                    isUnavailsRepeat={this.state.isUnavailsRepeat}
                    onIsUnavailsRepeatChange={this.onIsUnavailsRepeatChange}
                    isLimitUnavailsRepeat={this.state.isLimitUnavailsRepeat}
                    onIsLimitUnavailsRepeatChange={this.onIsLimitUnavailsRepeatChange}
                    unavailsRepeatEndDate={this.state.unavailsRepeatEndDate}
                    onUnavailRepeatEndDateChange={this.onUnavailRepeatEndDateChange}
                    confirmCalendarSetting={this.confirmCalendarSetting}
                />}

                <div className="section_manage">
                    <div className="main-title">
                        <h3 className="tt">Quản lý lịch chung</h3>
                    </div>
                    <div className="space m"></div>
                    <div className="content-container">
                        <div className="space-control"></div>
                        <FilterControl
                            filterText={this.state.filterText}
                            onFilterTextChange={this.onFilterTextChange}
                            sortType={this.state.sortType}
                            onSortTypeChange={this.onSortTypeChange}
                            isDesSort={this.state.isDesSort}
                            onSortWayToggle={this.onSortWayToggle} />
                        <div className="calendar-container">
                            <ul className="list-status">
                                <li className="status-3"><span>Giá mặc định</span></li>
                                <li className="status-6"><span>Lặp lại tuần</span></li>
                                <li className="status-5"><span>Ngày đã đặt</span></li>
                                <li className="status-9"><span>Chờ đặt cọc</span></li>
                                <li className="status-4"><span>Giá tùy chỉnh</span></li>
                                <li className="status-1"><span>Ngày trống </span></li>
                                <li className="status-2"><span>Ngày bận</span></li>
                                <li className="status-8"><span>Chờ duyệt </span></li>
                            </ul>
                        </div>
                    </div>
                    <div className="space m"></div>
                    <StickyContainer>
                        <div className="manage_wrap">
                            <div className="manage-calendar">
                                <div className="row-day heading-wrap">
                                    <div className={`swiper-button-next ${this.state.isHiddenNext ? "hidden" : ""}`} onClick={this.nextTime}><i className="i-arr"></i></div>
                                    <div className={`swiper-button-prev ${this.state.isHiddenPrev ? "hidden" : ""}`} onClick={this.prevTime}><i className="i-arr"></i></div>
                                    <div className="column-car"></div>
                                    {mainDays.length > 0 && mainDays.map((day, index) => {
                                        return <div key={index} className={`desc ${moment(day.ts).isoWeekday() === 7 ? "weekend" : ""}`}>
                                            <p>{moment(day.ts).format("dd")}</p>
                                            <p>{moment(day.ts).format("DD/MM")}</p>
                                        </div>
                                    })}
                                </div>
                                {/* <Sticky disableCompensation>
                                    {({
                                        isSticky,
                                        wasSticky,
                                        style,
                                        distanceFromTop,
                                        distanceFromBottom,
                                        calculatedHeight
                                    }) => {
                                        const sStyle = { ...style, width: '100vw' };
                                        if (isSticky) {
                                            sStyle.borderBottom = "1px solid #eee";
                                        }
                                        return <div className="row-day heading-wrap" style={sStyle}>
                                            <div className="column-car"></div>
                                            {mainDays.length > 0 && mainDays.map((day, index) => {
                                                return <div key={index} className={`desc ${moment(day.ts).isoWeekday() === 7 ? "weekend" : ""}`}>
                                                    <p>{moment(day.ts).format("dd")}</p>
                                                    <p>{moment(day.ts).format("DD/MM")}</p>
                                                </div>
                                            })}
                                        </div>
                                    }}
                                </Sticky> */}
                                {cars.length > 0 && cars.map(car => {
                                    const calendar = car.calendar;
                                    const unavailDays = car.unavailDays;
                                    const bookedDays = car.bookedDays;
                                    const priceSpecifics = car.priceSpecifics;
                                    const priceRepeat = car.priceRepeat;
                                    const priceRepeatEndDate = car.priceRepeatEndDate;
                                    const isLimitRepeatPrice = car.isLimitRepeatPrice;
                                    const unavailsRepeat = car.unavailsRepeat;
                                    const instantDays = [];
                                    const unavailsRepeatEndDate = car.unavailsRepeatEndDate;
                                    const isLimitUnavailsRepeat = car.isLimitUnavailsRepeat;
                                    const setting = car.setting;
                                    if(car.instantEnable){                                
                                        const start = car.instantRangeFrom === 0 ? moment(calendar.startDate).startOf('day').valueOf() : moment(moment().valueOf() + car.instantRangeFrom * 1000).startOf('day').valueOf();
                                        const end = car.instantRangeTo === 0 ? moment(calendar.endDate).endOf('month').valueOf() : moment(moment().valueOf() + car.instantRangeTo * 1000).startOf('day').valueOf();
                                        for(var i = start; i <= end; i += (3600000 * 24)){
                                            instantDays.push(i);
                                        }
                                    }

                                    const days = buildMonthCalendar(start, end, calendar.priceDaily, instantDays, unavailDays, bookedDays, priceSpecifics, priceRepeat, priceRepeatEndDate,
                                        isLimitRepeatPrice, unavailsRepeat, unavailsRepeatEndDate, isLimitUnavailsRepeat, setting);

                                    return <div key={car.id} className="row-day">
                                        <div className="column-car">
                                            {/* <i className="ic ic-info-wh" onClick={() => this.showCarDetailBox(car)}> </i> */}
                                            <span className="license-plate">{car.licensePlate}</span>
                                            <span className="price" onClick={() => this.setDefaultPrice(car, car.calendar.priceDaily)}>{formatPrice(car.calendar.priceDaily)}</span>
                                            <div className="img-car" onClick={() => this.showCarDetailBox(car)}>
                                                <img src={car.photos ? car.photos[0].thumbUrl : car_default} alt={`Cho thuê xe tự lái ${car.name}`} />
                                            </div>
                                            <div className="squaredFour car-cell">
                                                <input id={`cb_${car.id}`} type="checkbox" checked={car.checked} onChange={this.onCarCheckboxClick} />
                                                <label htmlFor={`cb_${car.id}`}></label>
                                            </div>
                                        </div>
                                        {days && days.map((day, index) => {
                                            if (day.isPast) {
                                                return <div key={index} className="desc outside">
                                                    {day.price !== car.calendar.priceDaily && <p className="event">{formatPrice(day.price)}</p>}
                                                    {day.price === car.calendar.priceDaily && <p>{formatPrice(car.calendar.priceDaily)}</p>}
                                                </div>
                                            } else {
                                                var isShowPrice = !day.isUnavail && day.bookedWidth !== 100;

                                                var tripsReqCont;
                                                const tripsReq = day.tripsReq;
                                                if (tripsReq.length > 0) {
                                                    tripsReqCont = tripsReq.map((trip, i) => {
                                                        const className = `${trip.tripInfo.isApproved ? "deposit" : "req"}-${day.mapTripsMaxLevel[trip.tripInfo.tripId]}`;
                                                        const style = { width: `${trip.width}%`, left: `${trip.left}%` }
                                                        return <span key={i} className={className} style={style}>{trip.isStart && <i className="ic ic-sm-flat"></i>}</span>;
                                                    })
                                                }

                                                return <div key={index} className={`desc available ${day.isUnavail ? "gray " : ""} ${day.isWeekend ? "mn-weekend" : ""}`} onClick={() => this.onDayItemClick(car, day)}>
                                                    {(isShowPrice && day.price !== car.calendar.priceDaily) && <p className="event">{formatPrice(day.price)}</p>}
                                                    {(isShowPrice && day.price === car.calendar.priceDaily) && <p className="price-default">{formatPrice(car.calendar.priceDaily)}</p>}
                                                    {tripsReqCont}
                                                    {day.isBooked && <span className="full-in-calendar" style={{ width: `${day.bookedWidth}%`, left: `${day.bookedLeft}%` }} />}
                                                    <div className="label-in-calendar">
                                                        {(day.isInstant && <i className="ic ic-clock-repeat"></i>)}
                                                        {(day.isInstant && <i className="ic ic-sm-thunderbolt"></i>)}
                                                    </div>
                                                </div>
                                            }
                                        })}
                                    </div>
                                })}
                                {(!cars.length || cars.length === 0) && <MessageLine message="Không tìm thấy xe nào." />}
                            </div>
                        </div>
                    </StickyContainer>
                </div>
            </section>
        }

        return <div className="mioto-layout">
            <Header />
            {content}
        </div>
    }
}

function mapState(state) {
    return {
        session: state.session
    }
}

Calendars = connect(mapState)(Calendars)

export default Calendars;