import React from "react"
import StarRatings from "react-star-ratings"

import { commonErr } from "../common/errors"
import { getOwnerCars } from "../../model/car"
import { MessageLine } from "../common/messagebox"
import { formatPrice } from "../common/common"

import carPhotoDefault from "../../static/images/upload/car_1.png"

function CarItem(props) {
    var car = props.featureCar;
    return <div className="item-car">
        <a href={`/infosetting/${car.id}`}>
            <span className="img-car" style={{ backgroundImage: `url(${car.photos ? car.photos[0].thumbUrl : carPhotoDefault})` }}></span>
            <div className="desc-car">
                <StarRatings
                    rating={car.rating.avg || 0}
                    starRatedColor="#00a550"
                    starDimension="17px"
                    starSpacing="1px"
                />
                <h2>{car.name}</h2>
                <p>
                    <span><i className="ic ic-clock"></i> {car.totalTrips} chuyến</span>
                    <span><i className="ic ic-tag"></i> {formatPrice(car.price)}</span>
                </p>
            </div>
        </a>
    </div>
}

class RelatedCars extends React.Component {
    componentDidMount() {
        this.swiper = new window.Swiper(this.refs.swiperRelatedCars, {
            slidesPerView: 4.2,
            spaceBetween: 24,
            threshold: 15,
            speed: 600,
            nextButton: '.swiper-button-next-related-cars',
            prevButton: '.swiper-button-prev-related-cars',
            slidesOffsetBefore: 0,
            slidesOffsetAfter: 0,
            breakpoints: {
                992: {
                    slidesPerView: 3.2,
                    spaceBetween: 30
                },
                768: {
                    slidesPerView: 3.2,
                    spaceBetween: 30
                },
                480: {
                    slidesPerView: 1.2,
                    spaceBetween: 15
                }
            },
        });
    }

    render() {
        return <div className="module-deal">
            <h4 className="title">Xe khác của tôi</h4>
            <div className="swiper-button-next swiper-button-next-related-cars">
                <i className="i-arr"></i>
            </div>
            <div className="swiper-button-prev swiper-button-prev-related-cars">
                <i className="i-arr"></i>
            </div>
            <div ref="swiperRelatedCars" className="swiper-container swiper-similar">
                <div className="swiper-wrapper">
                    {this.props.featureCars.map(function (featureCar, i) {
                        return <div className="swiper-slide" key={i}>
                            <CarItem featureCar={featureCar} />
                        </div>
                    })}
                </div>
            </div>
        </div>
    }
}

class MyCarsBox extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            cars: null,
            err: commonErr.INNIT
        }
    }

    componentDidMount() {
        const self = this;
        this.setState({
            err: commonErr.LOADING
        });
        getOwnerCars(0, 0, 0, 0, true).then(function (resp) {
            if (resp.data.error >= commonErr.SUCCESS && resp.data.data.cars) {
                self.setState({
                    cars: resp.data.data.cars,
                    err: resp.data.error
                });
            } else {
                self.setState({
                    err: resp.data.error
                });
            }
        });
    }

    render() {
        var content;
        var cars = this.state.cars;
        if (cars) {
            cars = cars.filter(car => {
                return car.id !== this.props.activedCar.id;
            });
        }
        if (cars && cars.length > 0) {
            content = <RelatedCars cars={cars} />
        } else {
            content = <MessageLine message={"Không tìm thấy xe nào khác."} />
        }
        return content;
    }
}

export default MyCarsBox;