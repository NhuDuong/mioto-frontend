import React from "react"
import ReactCrop, { makeAspectCrop } from 'react-image-crop'
import { Modal } from "react-bootstrap"

import { uploadCarPhoto } from "../../model/car"
import { commonErr } from "../common/errors"
import ImageUpload from "../common/imageupload"
import { LoadingInline } from "../common/loading"
import { MessageBox } from "../common/messagebox"

export default class CarRegisterPhotos extends React.Component {
    constructor() {
        super();

        this.state = {
            err: commonErr.INNIT,

            //crop
            crop: {
                x: 0,
                y: 0
            },
            maxHeight: 300
        }
    }

    componentDidMount() {
        window.scrollTo(0, 0);
    }

    hideMessageBox() {
        this.setState({
            err: commonErr.INNIT,
            errMsg: ""
        });
    }

    hideResizeBox() {
        this.setState({
            isShowResizeBox: false
        });
    }

    hanldePhotoInputChange(photos) {
        const reader = new FileReader();

        reader.onload = (e2) => {
            this.setState({
                dataUrl: e2.target.result,
                isShowResizeBox: true
            });
        };

        reader.readAsDataURL(photos[0]);
    }

    removeCarPhoto(photo) {
        const self = this;
        self.props.removeCarPhoto(photo);
    }

    dataURItoBlob(dataURI) {
        // convert base64/URLEncoded data component to raw binary data held in a string
        var byteString;
        if (dataURI.split(',')[0].indexOf('base64') >= 0)
            byteString = atob(dataURI.split(',')[1]);
        else
            byteString = unescape(dataURI.split(',')[1]);
        // separate out the mime component
        var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

        // write the bytes of the string to a typed array
        var ia = new Uint8Array(byteString.length);
        for (var i = 0; i < byteString.length; i++) {
            ia[i] = byteString.charCodeAt(i);
        }

        return new Blob([ia], { type: mimeString });
    }

    onUploadBtnClick() {
        const self = this;
        self.setState({
            err: commonErr.LOADING,
            isShowResizeBox: false
        });

        const photo = this.dataURItoBlob(this.state.resizeImage);

        uploadCarPhoto(0, photo).then(function (resp) {
            if (resp.data.data && resp.data.data.id) {
                self.props.hanldePhotoInputChange(resp.data.data);
            }
            self.setState({
                err: resp.data.error,
                errMsg: resp.data.errorMessage,
            });
        });
    }

    onImageLoaded = (image) => {
        const crop = makeAspectCrop({
            x: 0,
            y: 0,
            aspect: 4 / 3,
            width: 80,
        }, image.naturalWidth / image.naturalHeight);

        var bufferCanvas = document.createElement('canvas');
        var bufferContext = bufferCanvas.getContext('2d');
        bufferCanvas.width = image.naturalWidth;
        bufferCanvas.height = image.naturalHeight;
        bufferContext.drawImage(image, 0, 0);

        const width = crop.width * image.naturalWidth / 100;
        const height = crop.height * image.naturalHeight / 100;

        var tnCanvas = document.createElement('canvas');
        var tnCanvasContext = tnCanvas.getContext('2d');
        tnCanvas.width = width;
        tnCanvas.height = height;

        tnCanvasContext.drawImage(bufferCanvas, crop.x, crop.y, width, height, 0, 0, width, height);

        this.setState({
            crop: crop,
            image,
            resizeImage: tnCanvas.toDataURL("image/jpeg")
        });
    }

    onCropComplete = (crop, pixelCrop) => {
        const image = this.state.image;

        var bufferCanvas = document.createElement('canvas');
        var bufferContext = bufferCanvas.getContext('2d');
        bufferCanvas.width = image.naturalWidth;
        bufferCanvas.height = image.naturalHeight;
        bufferContext.drawImage(image, 0, 0);

        var tnCanvas = document.createElement('canvas');
        var tnCanvasContext = tnCanvas.getContext('2d');
        tnCanvas.width = pixelCrop.width;
        tnCanvas.height = pixelCrop.height;

        tnCanvasContext.drawImage(bufferCanvas, pixelCrop.x, pixelCrop.y, pixelCrop.width, pixelCrop.height, 0, 0, pixelCrop.width, pixelCrop.height);

        this.setState({
            resizeImage: tnCanvas.toDataURL("image/jpeg")
        });
    }

    onCropChange = (crop) => {
        if (crop.width < 30) {
            return;
        }
        this.setState({ crop });
    }

    render() {
        var photos = this.props.photos.slice().reverse();

        return <div className="group form-default">
            <h6>Hình ảnh</h6>
            <p className="summary">Đăng nhiều hình ở các góc độ khác nhau để tăng thông tin cho xe của bạn.</p>
            <div className="list-photos">
                <ul>
                    <li>
                        <a className="obj-photo">
                            <span className="ins">
                                {!this.state.isShowResizeBox && this.state.err !== commonErr.LOADING && <ImageUpload withPreview={false} onChange={this.hanldePhotoInputChange.bind(this)} className="form-control" />}
                                {this.state.err === commonErr.LOADING && <LoadingInline />}
                                <MessageBox show={this.state.err < commonErr.SUCCESS} hideModal={this.hideMessageBox.bind(this)} message={this.state.errMsg} />
                            </span>
                        </a>
                    </li>
                    {photos && photos.map(photo => <li key={photo.id}>
                        <div className="obj-photo" style={{ backgroundImage: `url(${photo.thumbUrl})` }}><a className="func-remove" onClick={() => this.removeCarPhoto(photo)}><i className="ic ic-remove"></i></a></div>
                    </li>)}
                </ul>
            </div>
            <Modal
                show={this.state.isShowResizeBox}
                onHide={this.hideResizeBox.bind(this)}
                dialogClassName="modal-sm modal-dialog"
            >
                <Modal.Header closeButton={true} closeLabel={""}>
                    <Modal.Title>Chuẩn hoá hình</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <div className="form-default form-s" style={{ textAlign: "center" }}>
                        <p className="notes">Mioto chỉ chấp nhận hình với tỉ lệ 4:3. Vui lòng điều chỉnh để có được chất lượng hiển thị tốt nhất.</p>
                        {this.state.dataUrl && <ReactCrop
                            {...this.state}
                            src={this.state.dataUrl}
                            onImageLoaded={this.onImageLoaded}
                            onComplete={this.onCropComplete}
                            onChange={this.onCropChange}
                        />}

                        {this.state.resizeImage && <div className="hide-on-med-and-down">
                            <div className="space m"></div>
                            <div className="space m"></div>
                            <div className="line"></div>
                            <div className="space m"></div>
                            <div className="ReactCrop">
                                <img className="ReactCrop__image" src={this.state.resizeImage} alt="Mioto - Thuê xe tự lái"/>
                            </div>
                        </div>}
                        <div className="space m"></div>
                        <div className="space m"></div>
                        <a className="btn btn-primary btn--m" onClick={this.onUploadBtnClick.bind(this)}>Đăng hình</a>
                    </div>
                </Modal.Body>
            </Modal>
        </div>
    }
} 