import React from "react"
import ReactSimpleRange from "react-simple-range"

import { formatPrice, listInstantRangeFrom, listInstantRangeTo } from "../common/common"
import CarMapLocation from "../common/carmaplocation"
import LocationPikcer from "../common/locationpicker"

export default class CarRegisterRenting extends React.Component {
    constructor() {
        super();
        this.state = {
        }

        this.toogleLocationPicker = this.toogleLocationPicker.bind(this);
        this.onAddressLocationUpdate = this.onAddressLocationUpdate.bind(this);
    }

    toogleLocationPicker() {
        this.setState({
            isShowLocationPicker: !this.state.isShowLocationPicker
        });
    }

    onAddressLocationUpdate(address, location) {
        this.props.onAddressLocationUpdate(address, location);
    }

    onPriceRangeChange(event) {
        this.props.onPriceRangeChange(event);
    }

    toggleDiscount() {
        this.props.toggleDiscount();
    }

    onDiscountWeekRangeChange(range) {
        this.props.onDiscountWeekRangeChange(range);
    }

    onDiscountMonthRangeChange(range) {
        this.props.onDiscountMonthRangeChange(range);
    }

    onAddressFocus(event) {
        if (!this.props.address || this.props.address === "") {
            this.toogleLocationPicker();
            event.target.blur();
        }
    }

    onAddressChange(event) {
        this.props.onAddressChange(event);
        if (!event.target.value || event.target.value === "") {
            this.toogleLocationPicker();
            event.target.blur();
        }
    }

    toggleInstantly() {
        this.props.toggleInstantly();
    }

    onInstantRangeFromChange(event){
        this.props.onInstantRangeFromChange(event);
    }

    onInstantRangeToChange(event){
        this.props.onInstantRangeToChange(event);
    }

    toggleDelivery() {
        this.props.toggleDelivery();
    }

    onDeliveryPriceRangeChange(range) {
        this.props.onDeliveryPriceRangeChange(range);
    }

    onDeliveryLimitedChange(event) {
        this.props.onDeliveryLimitedChange(event);
    }

    toggleLimited() {
        this.props.toggleLimited();
    }

    onLimitedPriceRangeChange(range) {
        this.props.onLimitedPriceRangeChange(range);
    }

    onLimitedChange(event) {
        this.props.onLimitedChange(event);
    }

    onPolicyChange(event) {
        this.props.onPolicyChange(event);
    }

    render() {
        var i;
        const deliveryRadiuses = [];
        for (i = 5; i <= this.props.setting.deliveryRadiusMax; i = i + 5) {
            deliveryRadiuses.push(i);
        }

        const limiteds = [];
        for (i = this.props.setting.limitKMMin; i <= this.props.setting.limitKMMax; i = i + 50) {
            limiteds.push(i);
        }
        return <div className="group form-default">
            <h6>Đơn giá thuê mặc định</h6>
            <p className="fl"><span className="note">Đơn giá áp dụng cho tất cả các ngày. Bạn có thuể tuỳ chỉnh giá khác cho các ngày đặc biệt (cuối tuần, lễ, tết...) trong mục quản lý xe sau khi đăng kí.</span></p>
            <div className="space m"></div>
            <div className="space m"></div>
            <div className="space m"></div>

            <div className="col-left">
                <div className="form-default">
                    <div className="line-form">
                        <p className="pl">
                            <span className="note">Giá đề xuất: {formatPrice(this.props.setting.priceDailyRecommend)}</span>
                        </p>
                        <div className="wrap-input-label d-flex">
                            <div className="wrap-input">
                                <input type="text" value={this.props.price / 1000} onChange={this.onPriceRangeChange.bind(this)} />
                            </div>
                            <span className="phay"> K</span>
                        </div>
                    </div>
                </div>
            </div>
            <div className="space m clear" />
            <div className="form-default">
                <div className="group-inline d-flex">
                    <h6>Giảm giá</h6>
                    <div className="switch-on-off">
                        <input className="switch-input" id="cb-discount" type="checkbox" checked={this.props.isDiscount} onChange={this.toggleDiscount.bind(this)} />
                        <label className="switch-label" htmlFor="cb-discount"></label>
                    </div>
                </div>
                {this.props.isDiscount && <div className="col-left">
                    <div className="line-form end">
                        <label className="label">Giảm giá thuê tuần (% trên đơn giá)</label>
                        <div className="range-slider">
                            <ReactSimpleRange
                                step={1}
                                min={1}
                                max={this.props.setting.discountMax}
                                value={this.props.discountWeek}
                                sliderSize={14}
                                thumbSize={16}
                                label={false}
                                trackColor={"#00a550"}
                                thumbColor={"#141414"}
                                onChange={this.onDiscountWeekRangeChange.bind(this)}
                            />
                        </div>
                        <span style={{ float: "right", fontSize: "14px" }}>{`${this.props.discountWeek}%`}</span>
                        <div className="space m" />
                        <p className="pl">
                            <span className="note">Giảm đề xuất: {this.props.setting.discountWeeklyRecommend}%</span>
                        </p>
                    </div>
                </div>}
                {this.props.isDiscount && <div className="col-right">
                    <div className="line-form end">
                        <label className="label">Giảm giá thuê tháng (% trên đơn giá)</label>
                        <div className="range-slider">
                            <ReactSimpleRange
                                step={1}
                                min={1}
                                max={this.props.setting.discountMax}
                                value={this.props.discountMonth}
                                sliderSize={14}
                                thumbSize={16}
                                label={false}
                                trackColor={"#00a550"}
                                thumbColor={"#141414"}
                                onChange={this.onDiscountMonthRangeChange.bind(this)}
                            />
                        </div>
                        <span style={{ float: "right", fontSize: "14px" }}>{`${this.props.discountMonth}%`}</span>
                        <div className="space m" />
                        <p className="pl">
                            <span className="note">Giảm đề xuất: {this.props.setting.discountMonthlyRecommend}%</span>
                        </p>
                    </div>
                </div>}
                <div className="space m clear" />
                <div className="line" />
            </div>
            <div className="form-default">
                <div className="group-inline d-flex">
                    <h6>Đặt xe nhanh</h6>
                    <div className="switch-on-off">
                        <input className="switch-input" id="cb-instantly" type="checkbox" checked={this.props.isInstantly} onChange={this.toggleInstantly.bind(this)} />
                        <label className="switch-label" htmlFor="cb-instantly" />
                    </div>
                </div>
                <div className="space m" />
                <span className="note marginBottom-xs" style={{ color: "red" }}>Bật tính năng cho phép khách hàng đặt xe ngay lập tức không cần chủ xe phê duyệt. (Phù hợp với chủ xe không thường xuyên online hoặc kiểm tra điện thoại)</span>
                {this.props.isInstantly && <div className="col-left">
                    <div className="line-form">
                        <label className="label">Giới hạn từ</label>
                        <span className="wrap-select">
                            <select disabled="disabled" onChange={this.onInstantRangeFromChange.bind(this)} value={this.props.instantRangeFrom}>
                                {listInstantRangeFrom && listInstantRangeFrom.map(option => <option key={option.key} value={option.key}>{option.value}</option>)}
                            </select>
                        </span>
                    </div>
                </div>}
                {this.props.isInstantly && <div className="col-right">
                        <div className="line-form">
                            <label className="label">Cho đến</label>
                            <span className="wrap-select">
                                <select onChange={this.onInstantRangeToChange.bind(this)} value={this.props.instantRangeTo}>
                                    {listInstantRangeTo && listInstantRangeTo.map(option => <option key={option.key} value={option.key}>{option.value}</option>)}
                                </select>
                            </span>
                        </div>
                    </div>
                }
                <div className="space m" />
                <h6>Địa chỉ xe</h6>
                <div className="line-form">
                    <LocationPikcer show={this.state.isShowLocationPicker} onHide={this.toogleLocationPicker.bind(this)} onUpdate={this.onAddressLocationUpdate} address={this.props.address} location={{ lat: this.props.lat, lng: this.props.lng }} />
                    <div className="wrap-input has-ico-search">
                        <i onClick={this.toogleLocationPicker.bind(this)} className="ic ic-map"></i>
                        <input type="text" value={this.props.address}
                            onChange={this.onAddressChange.bind(this)}
                            onFocus={this.onAddressFocus.bind(this)}
                            placeholder={"Địa chỉ mặc định để giao nhận xe."} />
                    </div>
                </div>
                {this.props.address !== "" && this.props.lat !== 0 && this.props.lng !== 0 && <CarMapLocation address={this.props.address} location={{ lat: this.props.lat, lng: this.props.lng }} />}
                <div className="space m clear" />
                <div className="group-inline d-flex">
                    <h6>Giao xe tận nơi</h6>
                    <div className="switch-on-off">
                        <input className="switch-input" id="cb-delivery" type="checkbox" checked={this.props.isDelivery} onChange={this.toggleDelivery.bind(this)} />
                        <label className="switch-label" htmlFor="cb-delivery" />
                    </div>
                </div>
                {this.props.isDelivery && <div className="col-left">
                    <div className="line-form end">
                        <label className="label">Quảng đường giao xe tối đa</label>
                        <span className="wrap-select">
                            <select onChange={this.onDeliveryLimitedChange.bind(this)} value={this.props.deliveryLimited}>
                                {deliveryRadiuses.map(radius => <option key={radius} value={radius}>{radius}km</option>)}
                            </select>
                        </span>
                        <div className="space m" />
                        <p className="pl">
                            <span className="note">Quảng đường đề xuất: {this.props.setting.deliveryRadiusRecommend} km</span>
                        </p>
                    </div>
                </div>}
                {this.props.isDelivery && <div className="col-right">
                    <div className="line-form end">
                        <label className="label">Phí giao nhận xe cho mỗi km</label>
                        <div className="range-slider">
                            <ReactSimpleRange
                                step={1000}
                                min={1000}
                                max={this.props.setting.deliveryPriceMax}
                                value={this.props.deliveryPrice}
                                sliderSize={14}
                                thumbSize={16}
                                label={false}
                                trackColor={"#00a550"}
                                thumbColor={"#141414"}
                                onChange={this.onDeliveryPriceRangeChange.bind(this)}
                            />
                        </div>
                        <span style={{ float: "right", fontSize: "14px" }}>{`${this.props.deliveryPrice > 0 ? `${formatPrice(this.props.deliveryPrice)}` : "Miễn phí"}`}</span>
                        <div className="space m" />
                        <p className="pl">
                            <span className="note">Phí đề xuất: {formatPrice(this.props.setting.deliveryPriceRecommend)}</span>
                        </p>
                    </div>
                </div>}
                <div className="space m clear" />
                <div className="group-inline d-flex">
                    <h6>Giới hạn quãng đường</h6>
                    <div className="switch-on-off">
                        <input className="switch-input" id="cb-limited" type="checkbox" checked={this.props.isLimited} onChange={this.toggleLimited.bind(this)} />
                        <label className="switch-label" htmlFor="cb-limited" />
                    </div>
                </div>
                {this.props.isLimited && <div className="col-left">
                    <div className="line-form end">
                        <label className="label">Quảng đường tối đa trong 1 ngày</label>
                        <span className="wrap-select">
                            <select onChange={this.onLimitedChange.bind(this)} value={this.props.limited}>
                                {limiteds.map(limited => <option key={limited} value={limited}>{limited}km</option>)}
                            </select>
                        </span>
                        <div className="space m" />
                        <p className="pl">
                            <span className="note">Quảng đường đề xuất: {this.props.setting.limitKMRecommend} km</span>
                        </p>
                    </div>
                </div>}
                {this.props.isLimited && <div className="col-right">
                    <div className="line-form end">
                        <label className="label">Phí vượt giới hạn tính mỗi km</label>
                        <ReactSimpleRange
                            step={1000}
                            min={1000}
                            max={this.props.setting.limitPriceMax}
                            value={this.props.limitedPrice}
                            sliderSize={14}
                            thumbSize={16}
                            label={false}
                            trackColor={"#00a550"}
                            thumbColor={"#141414"}
                            onChange={this.onLimitedPriceRangeChange.bind(this)}
                        />
                        <span style={{ float: "right", fontSize: "14px" }}>{`${this.props.limitedPrice > 0 ? `${formatPrice(this.props.limitedPrice)}` : "Miễn phí"}`}</span>
                        <div className="space m" />
                        <p className="pl">
                            <span className="note">Phí đề xuất: {formatPrice(this.props.setting.limitPriceRecommend)}</span>
                        </p>
                    </div>
                </div>}
                <div className="space m clear" />
                <div className="line" />
            </div>
            <div className="space m" />
            <h6>Điều khoản thuê xe</h6>
            <p><span className="note">Ghi rõ các yêu cầu để khách có thể thuê xe.</span></p>
            <div className="line-form end">
                <textarea className="textarea" onChange={this.onPolicyChange.bind(this)} value={this.props.policy} />
            </div>
            <div className="space m" />
        </div>
    }
} 