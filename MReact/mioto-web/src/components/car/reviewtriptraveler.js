import React from "react"
import { Modal } from "react-bootstrap"
import ReactStars from "react-stars"

import { commonErr } from "../common/errors"
import { reviewTripByTraveler, removePendingReview } from "../../model/car"

export default class ReviewTripTraveler extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            err: commonErr.INNIT,
            step: 1, //1: review 2: finished
            errMsg: "",
            rating: 5,
            comment: ""
        }
    }

    onRatingChange(rating) {
        this.setState({
            rating: rating
        });
    }

    onCommentChange(e) {
        this.setState({
            comment: e.target.value
        });
    }

    goToReview() {
        this.setState({
            err: commonErr.INNIT,
            step: 1,
            errMsg: "",
        });
    }

    review() {
        reviewTripByTraveler(this.props.trip.id, this.state.rating, this.state.comment).then(resp => {
            if (resp.data.error >= commonErr.SUCCESS) {
                this.props.getTrip(this.props.trip.id);
            }
            this.setState({
                step: 2,
                err: resp.data.error,
                errMsg: resp.data.errorMessage
            });
        });
    }

    finish() {
        removePendingReview(this.props.trip.id).then(resp => {
            if (resp.data.error >= commonErr.SUCCESS) {
                this.props.getTrip(this.props.trip.id);
            };
        });
        this.props.hideModal();
        this.setState({
            err: commonErr.INNIT,
            step: 1,
            errMsg: "",
            rating: 0,
            comment: ""
        });
    }

    render() {
        var content;
        if (this.state.step === 2) {
            if (this.state.err >= commonErr.SUCCESS) {
                content = <div className="form-default form-s">
                    <div className="line-form">
                        <div className="textAlign-center"><i className="ic ic-verify"></i> Đánh giá thành công.</div>
                    </div>
                    <div className="clear"></div>
                    <div className="space m"></div>
                    <button className="btn btn-primary btn--m" type="button" onClick={this.finish.bind(this)}>Hoàn tất</button>
                </div>
            } else {
                content = <div className="form-default form-s">
                    <div className="line-form">
                        <div className="textAlign-center"><i className="ic ic-error"></i> {this.state.errMsg}</div>
                    </div>
                    <div className="clear"></div>
                    <div className="space m"></div>
                    <p className="textAlign-center has-more-btn">
                        <button className="btn btn-primary btn--m" type="button" onClick={this.goToReview.bind(this)}>Thử lại</button>
                    </p>
                </div>
            }
        } else if (this.state.step === 1) {
            content = <div className="module-register" style={{ background: "none", padding: "5px" }}>
                <div className="form-default form-s" style={{ width: "160px", margin: "auto" }}>
                    <ReactStars
                        count={5}
                        size={30}
                        value={this.state.rating}
                        half={false}
                        onChange={this.onRatingChange.bind(this)}
                        color1={'#efefef'}
                        color2={'#00a550'} />
                </div>
                <div className="form-default form-s">
                    <div className="space m"></div>
                    <div className="line-form">
                        <div className="wrap-input has-ico">
                            <textarea className="textarea" onChange={this.onCommentChange.bind(this)} placeholder="Nhập nội dung đánh giá" value={this.state.comment}></textarea>
                        </div>
                    </div>
                    <div className="clear"></div>
                    <div className="space m"></div>
                    <div className="space m"></div>
                    <div className="space m"></div>
                    <div className="space m"></div>
                    <div className="wrap-btn has-2btn">
                        <div className="wr-btn">
                            <a className="btn btn-secondary btn--m" type="button" onClick={this.finish.bind(this)}>Bỏ qua</a>
                        </div>
                        <div className="wr-btn">
                            <a className="btn btn-primary btn--m" type="button" onClick={this.review.bind(this)}>Đánh giá</a>
                        </div>
                    </div>
                </div>
            </div>
        }

        return <Modal
            show={this.props.show}
            onHide={this.finish.bind(this)}
            dialogClassName="modal-sm modal-dialog"
        >
            <Modal.Header closeButton={true} closeLabel={""}>
                <Modal.Title>Đánh giá chuyến</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                {content}
            </Modal.Body>
        </Modal>
    }
}