import React from "react"
import { connect } from "react-redux"
import { geocodeByAddress, getLatLng } from 'react-places-autocomplete'
import NumberFormat from "react-number-format"
import { Modal } from "react-bootstrap"
import cookie from 'react-cookies'
import StarRatings from "react-star-ratings"

import Login from "../login/login"
import { commonErr } from "../common/errors"
import { LoadingPage } from "../common/loading"
import { getLoggedProfile } from "../../actions/sessionAct"
import { bookCar } from "../../model/car"
import { formatPrice } from "../common/common"

import car_photo from "../../static/images/upload/car_1.png"
import avatar_default from "../../static/images/avatar_default.png"

class CarMapLocation extends React.Component {
		componentDidMount() {
				if (this.props.location) {
						setTimeout(() => {
								this.initMap(this.refs, this.props.location.lat, this.props.location.lon);
						}, 100)
				}
		}

		initMap(refs, lat, lng) {
				const map = new window.google.maps.Map(refs.map, {
						center: {
								lat: lat,
								lng: lng
						},
						zoom: 14,
						mapTypeControl: false,
						scrollwheel: false,
						clickableIcons: false,
						streetViewControl: false,
						zoomControl: true,
						zoomControlOptions: {
								position: window.google.maps.ControlPosition.RIGHT_BOTTOM
						}
				})

				this.marker = new window.google.maps.Marker({
						map: map,
						icon: {
								path: window.google.maps.SymbolPath.CIRCLE,
								scale: map.getZoom() * 3,
								strokeColor: '#141414',
								strokeOpacity: 0.8,
								strokeWeight: 1,
								fillColor: '#666',
								fillOpacity: 0.35
						},
						position: new window.google.maps.LatLng(this.props.location.lat, this.props.location.lon)
				});

				map.addListener('zoom_changed', () => {
						this.marker.setMap(null);

						var scale = map.getZoom() * 3;
						var bounds = map.getBounds();
						var center = map.getCenter();
						if (bounds && center) {
								var ne = bounds.getNorthEast();
								var radius = window.google.maps.geometry.spherical.computeDistanceBetween(center, ne);
								scale = 100000 / radius;
						}

						this.marker = new window.google.maps.Marker({
								map: map,
								icon: {
										path: window.google.maps.SymbolPath.CIRCLE,
										scale: scale,
										strokeColor: '#141414',
										strokeOpacity: 0.8,
										strokeWeight: 1,
										fillColor: '#666',
										fillOpacity: 0.35
								},
								position: new window.google.maps.LatLng(this.props.location.lat, this.props.location.lon)
						});
				});
		}

		render() {
				return <div ref="map" className="fix-map"></div>
		}
}

class BookingResult extends React.Component {
		render() {
				const { err, errMsg, car, trip } = this.props;

				if (err >= commonErr.SUCCESS) {
						if (car.instant) {
								return <div className="form-default form-s">
										<div className="textAlign-center">
												<i className="ic ic-verify" /> Yêu cầu đặt xe của bạn đã được chủ xe đồng ý. Bạn cần đặt cọc để hoàn tất việc đặt xe.
												<div className="space m" />
												<div className="wrap-btn">
														<a href={`/trip/detail/${trip.id}`} className="btn btn-primary btn--m" >Tiếp tục</a>
												</div>
										</div>
								</div>
						} else {
								return <div className="form-default form-s">
										<div className="textAlign-center">
												<i className="ic ic-verify" /> Yêu cầu đặt xe của bạn đã được gởi đến chủ xe. Vui lòng chờ chủ xe xác nhận sau đó tiến hành đặt cọc để hoàn tất việc đặt xe.
												<div className="space m" />
												<div className="wrap-btn">
														<a href={`/trip/detail/${trip.id}`} className="btn btn-primary btn--m" >Xem chi tiết</a>
												</div>
										</div>
								</div>
						}
				} else {
						return <div className="form-default form-s">
								<div className="textAlign-center">
										<i className="ic ic-error" /> Đặt xe thất bại. {errMsg}.
								</div>
						</div>
				}
		}
}

class BookingConfirm extends React.Component {
		constructor(props) {
				super(props);
				this.state = {
						err: commonErr.INNIT,
						errMsg: "",

						showLoginForm: false,
						showWarningForm: false,
						priceSummary: null,
						messageToOwner: ""
				}

				this.openLoginForm = this.openLoginForm.bind(this);
				this.closeLoginForm = this.closeLoginForm.bind(this);
				this.openWarningForm = this.openWarningForm.bind(this);
				this.closeWarningForm = this.closeWarningForm.bind(this);
				this.getLoggedProfile = this.getLoggedProfile.bind(this);
				this.onMessageToOwnerChange = this.onMessageToOwnerChange.bind(this);
		}

		componentDidMount() {
				window.scrollTo(0, 0);
		}

		openLoginForm() {
				this.setState({
						showLoginForm: true
				});
		}

		closeLoginForm() {
				this.setState({
						showLoginForm: false
				});
		}

		openWarningForm() {
				this.setState({
						showWarningForm: true
				});
		}

		closeWarningForm() {
				this.setState({
						showWarningForm: false
				});
		}

		closeMessageBox() {
				this.setState({
						errMsg: ""
				});
		}

		getLoggedProfile() {
				this.props.dispatch(getLoggedProfile());
		}

		onMessageToOwnerChange(event) {
				this.setState({
						messageToOwner: event.target.value
				});
		}

		readPaperChange(event) {
				this.setState({
						readPaper: event.target.checked
				});
		}

		readPolicyChange(event) {
				this.setState({
						readPolicy: event.target.checked
				});
		}

		isValidBooking() {
				if (!this.state.readPolicy) {
						return false;
				}

				const car = this.props.car;
				if (((car.requiredPapers && car.requiredPapers.length !== 0)
						|| (car.requiredPapersOther && car.requiredPapersOther !== ""))
						&& !this.state.readPaper) {
						return false;
				}

				return true;
		}

		booking() {
				if (!this.props.session.profile || !this.props.session.profile.info || !this.props.session.profile.info.uid) {
						this.openLoginForm();
				} else {
						this.setState({
								err: commonErr.LOADING
						});

						const car = this.props.car;
						const startDate = this.props.startDate.valueOf();
						const endDate = this.props.endDate.valueOf();
						const useCarLocation = this.props.selfPickup;
						const dAddr = this.props.fromAddress;
						const price = JSON.stringify(car.priceSummary);
						var dLat = car.location.lat;
						var dLon = car.location.lon;
						const message = this.state.messageToOwner;
						const promoCode = this.props.promoCode;

						if (useCarLocation) {
								bookCar(car.id, car.instant, startDate, endDate, price, car.limitEnable, car.limitKM, car.limitPrice, useCarLocation, dLat, dLon, dAddr, message, promoCode).then(resp => {
										if (resp.data.error >= commonErr.SUCCESS) {
												// if (car.instant) {
												//     requestPayment(resp.data.data.trip.id).then(paymentResp => {
												//         if (paymentResp.data.data) {
												//             window.location.assign(paymentResp.data.data.payUrl);
												//         } else {
												//             this.setState({
												//                 err: paymentResp.data.error,
												//                 errMsg: paymentResp.data.errorMessage
												//             });
												//         }
												//     });
												// } else {
												//     this.props.goToResult(resp.data.data.trip);
												// }
												this.props.goToResult(resp.data.data.trip);
										} else {
												// this.setState({
												//     err: resp.data.error,
												//     errMsg: resp.data.errorMessage
												// });
												this.props.goToError(resp.data.error, resp.data.errorMessage);
										}
								});
						} else {
								geocodeByAddress(dAddr)
										.then(results => getLatLng(results[0]))
										.then(latLng => {
												dLat = latLng.lat;
												dLon = latLng.lng;
												bookCar(car.id, car.instant, startDate, endDate, price, car.limitEnable, car.limitKM, car.limitPrice, useCarLocation, dLat, dLon, dAddr, message, promoCode).then(resp => {
														if (resp.data.error >= commonErr.SUCCESS) {
																// if (car.instant) {
																//     requestPayment(resp.data.data.trip.id).then(paymentResp => {
																//         if (paymentResp.data.data) {
																//             window.location.assign(paymentResp.data.data.payUrl);
																//         } else {
																//             this.setState({
																//                 err: paymentResp.data.error,
																//                 errMsg: paymentResp.data.errorMessage
																//             });
																//         }
																//     });
																// } else {
																//     this.props.goToResult(resp.data.data.trip);
																// }
																this.props.goToResult(resp.data.data.trip);
														} else {
																// this.setState({
																//     err: resp.data.error,
																//     errMsg: resp.data.errorMessage
																// });
																this.props.goToError(resp.data.error, resp.data.errorMessage);
														}
												});
										})
						}

						//GA
						const utmSrc = cookie.load("_utm_src") || "web_directly";
						const utmExt = cookie.load("_utm_ext") || "no_label";
						const action = car.instant ? "CAR_BOOKING_INSTANTLY" : "CAR_BOOKING"
						window.ga('send', 'event', utmSrc, action, utmExt);
				}
		}

		render() {
				const car = this.props.car;
				const owner = this.props.owner;
				const priceSummary = car.priceSummary;
				const selfPickup = this.props.selfPickup;
				const fromAddress = this.props.fromAddress;
				const startDate = this.props.startDate;
				const endDate = this.props.endDate;
				const promoCode = this.props.promoCode;
				const backToSetup = this.props.backToSetup;

				var content;
				if (this.state.err === commonErr.LOADING) {
						content = <LoadingPage />
				} else {
						content = <div className="module-register" style={{ background: "none", padding: "0" }}>
								<div className="register-container">
										<div className="content-register">
												<div className="content-booking" style={{ padding: "0" }}>
														<div className="info-car--more" style={{ padding: "0" }}>
																<div className="group-1">
																		<h6 className="name">{car.name}</h6>
																		<div className="bar-line"></div>
																		<StarRatings
																				rating={car.rating.avg}
																				starRatedColor="#00a550"
																				starDimension="17px"
																				starSpacing="1px"
																		/>
																		<hr className="line-name" />
																</div>
																<div className="group-2">
																		<div className="car-img">
																				<div className="fix-img"> <img src={car.photos ? car.photos[0].thumbUrl : car_photo} alt="Mioto - Thuê xe tự lái" /></div>
																		</div>
																</div>
																<div className="group-3">
																		<div className="group-info">
																				<h6>Thời gian thuê xe</h6>
																				<div className="form-default grid-form">
																						<div className="line-form has-timer">
																								<p>Bắt đầu: {startDate.format("HH:mm - DD/MM/YYYY")}</p>
																						</div>
																						<div className="line-form has-timer">
																								<p>Kết thúc: {endDate.format("HH:mm - DD/MM/YYYY")}</p>
																						</div>
																				</div>
																		</div>
																		<div className="group-info">
																				<h6>Địa điểm giao nhận xe</h6>
																				<div className="address-car">
																						{selfPickup && <div className="line-form">
																								<p>{car.locationAddr} (địa chỉ của xe).</p>
																						</div>}
																						{!selfPickup && <div className="line-form">
																								<a target="_blank" href={`https://www.google.com/maps/search/?api=1&query=${fromAddress}`}><span className="value">{fromAddress} <i className="ic ic-map" /></span></a>
																						</div>}
																				</div>
																		</div>
																</div>
																<div className="group-4">
																		<div className="profile-mini-v2">
																				<div className="avatar avatar-new" title="title name">
																						<div className="avatar-img" style={{ backgroundImage: `url(${(owner && owner.avatar && owner.avatar.fullUrl) ? owner.avatar.fullUrl : avatar_default})` }}></div>
																				</div>
																				<div className="desc-v2">
																						<h2><a href="#!">{owner.name} </a></h2>
																						<StarRatings
																								rating={owner.owner.rating.avg}
																								starRatedColor="#00a550"
																								starDimension="17px"
																								starSpacing="1px"
																						/>
																				</div>
																		</div>
																</div>
																<div className="group-5">
																		<div className="space m"></div>
																		<div className="group-info mb-4">
																				<h6>Số điện thoại</h6>
																				<p>***********</p>
																				<span>Lưu ý: Số điện thoại của chủ xe sẽ được hiển thị sau khi đặt cọc.</span>
																		</div>
																		<div className="group-info">
																				<h6>Lời nhắn</h6>
																				<textarea className="textarea" onChange={this.onMessageToOwnerChange} placeholder="Gợi ý: Chào anh chủ xe, tôi cần thuê xe đi du lịch Vũng Tàu cùng gia đình với thời gian như trên. Tôi có đầy đủ giấy tờ và tài sản đặt cọc như yêu cầu (hoặc tôi không có HK TPHCM nhưng có HK Đồng Nai thay thế..). Anh vui lòng sắp xếp báo giúp tình trạng xe sớm nhé. Cảm ơn!" value={this.state.messageToOwner}></textarea>
																		</div>
																</div>
																<div className="group-6">
																		<div className="space m"></div>
																		<div className="address-car">
																				<h6>Vị trí xe </h6>
																				<span className="value">{car.locationAddr} (địa chỉ cụ thể sẽ được hiển thị sau khi đặt cọc).</span>
																				<div className="space m"></div>
																				<CarMapLocation location={car.location} />
																		</div>
																		<div className="limited">
																				<div className="space m"></div>
																				<h6>Giới hạn quãng đường </h6>
																				{car.limitEnable === 1 && <div className="note"><p>Tối đa <strong>{car.limitKM}</strong> km/ngày. Phí <strong>{formatPrice(car.limitPrice)}</strong>/km vượt giới hạn.</p></div>}
																				{car.limitEnable !== 1 && <div className="note"><p>Không giới hạn quãng đường.</p></div>}
																		</div>
																</div>
															
																<div className="group-7">
																		<div className="space m"></div>
																		<h6>Bảng giá</h6>
																		<div className="form-default">
																				<div className="line-form has-timer">
																						<div style={{ width: "50%" }} className="wrap-input has-dropdown date has-value"><span className="value">Đơn giá thuê</span></div>
																						<div style={{ width: "50%" }} className="wrap-input has-dropdown time has-value"><span className="value"><NumberFormat value={priceSummary.priceSpecial} displayType={'text'} thousandSeparator={" "} decimalSeparator={","} suffix={" / ngày"} /></span></div>
																						{priceSummary.discountPercent > 0 && <div style={{ width: "50%" }} className="wrap-input has-dropdown date has-value long-term"><span className="value">{priceSummary.discountType === 0 ? <span>Đơn giá thuê {priceSummary.discountPercent > 0 && <span> (giảm {priceSummary.discountPercent}%)</span>}</span> : <span>Thuê{priceSummary.discountType == 1 && <span> tuần</span>}{priceSummary.discountType === 2 && <span> tháng</span>} {priceSummary.discountPercent > 0 && <span> (giảm {priceSummary.discountPercent}%)</span>}</span>}</span></div>}
																						{priceSummary.discountPercent > 0 && <div style={{ width: "50%" }} className="wrap-input has-dropdown time has-value long-term"><span className="value line-through"><NumberFormat value={priceSummary.priceOrigin} displayType={'text'} thousandSeparator={" "} decimalSeparator={","} prefix={"Giá gốc "} suffix={" / ngày"} /></span></div>}
																						{priceSummary.specialDiscount > 0 && <div style={{ width: "50%" }} className="wrap-input has-dropdown date has-value long-term"><span className="value">Chương trình giảm giá<div className="tooltip"><i className="ic ic-question-mark" /><div className="tooltip-text">Không áp dụng đồng thời với mã khuyến mãi.</div></div>
																						</span></div>}
																						{priceSummary.specialDiscount > 0 && <div style={{ width: "50%" }} className="wrap-input has-dropdown time has-value long-term"><span className="value line-through">{priceSummary.priceOrigin === priceSummary.price && <span>Giá gốc </span>}<NumberFormat value={priceSummary.price} displayType={'text'} thousandSeparator={" "} decimalSeparator={","} suffix={" / ngày"} /></span></div>}
																						{priceSummary.tripFee > 0 && <div style={{ width: "50%" }} className="wrap-input has-dropdown date has-value"><span className="value">Phí dịch vụ <div className="tooltip"><i className="ic ic-question-mark" /><div className="tooltip-text">Phí dịch vụ nhằm hỗ trợ Mioto duy trì nền tảng ứng dụng và các hoạt động chăm sóc khách hàng một cách tốt nhất.</div></div></span>
																						</div>}
																						{priceSummary.tripFee > 0 && <div style={{ width: "50%" }} className="wrap-input has-dropdown time has-value"><span className="value"><NumberFormat value={priceSummary.tripFee} displayType={'text'} thousandSeparator={" "} decimalSeparator={","} suffix={" / ngày"} /></span></div>}
																						{priceSummary.insuranceFee > 0 && <div style={{ width: "50%" }} className="wrap-input has-dropdown date has-value"><span className="value">Phí bảo hiểm <div className="tooltip"><i className="ic ic-question-mark" /><div className="tooltip-text">Phí dịch vụ nhằm hỗ trợ Mioto duy trì nền tảng ứng dụng và các hoạt động chăm sóc khách hàng một cách tốt nhất.</div></div></span>
																						</div>}
																						{priceSummary.insuranceFee > 0 && <div style={{ width: "50%" }} className="wrap-input has-dropdown time has-value"><span className="value"><NumberFormat value={priceSummary.insuranceFee} displayType={'text'} thousandSeparator={" "} decimalSeparator={","} suffix={" / ngày"}/></span>
																						</div>}
																						<div style={{ width: "50%" }} className="wrap-input has-dropdown date has-value line-gray"><span className="value">Tổng phí thuê xe</span></div>
																						<div style={{ width: "50%" }} className="wrap-input has-dropdown time has-value line-gray"><span className="value"><NumberFormat value={priceSummary.totalPerDay} displayType={'text'} thousandSeparator={" "} decimalSeparator={","} /> x <strong>{priceSummary.totalDays} ngày</strong></span></div>
																						{priceSummary.deliveryFee > 0 && <div style={{ width: "50%" }} className="wrap-input has-dropdown date has-value"><span className="value">Phí giao nhận xe (2 chiều)</span></div>}
																						{priceSummary.deliveryFee > 0 && <div style={{ width: "50%" }} className="wrap-input has-dropdown time has-value"><span className="value"><NumberFormat value={priceSummary.deliveryFee > 0 ? priceSummary.deliveryFee : 0} displayType={'text'} thousandSeparator={" "} decimalSeparator={","} /> {(priceSummary.deliveryDistance && priceSummary.deliveryDistance) > 0 && <NumberFormat value={priceSummary.deliveryDistance} displayType={'text'} thousandSeparator={" "} decimalSeparator={","} prefix={" ("} suffix={" km)"} decimalScale={1} />}</span></div>}
																						{priceSummary.promotionDiscount > 0 && <div style={{ width: "50%" }} className="wrap-input has-dropdown date has-value"><span className="value">Khuyễn mãi mã <strong>{promoCode.code}</strong></span></div>}
																						{priceSummary.promotionDiscount > 0 && <div style={{ width: "50%" }} className="wrap-input has-dropdown time has-value"><span className="value">- <NumberFormat value={priceSummary.promotionDiscount > 0 ? priceSummary.promotionDiscount : 0} displayType={'text'} thousandSeparator={" "} decimalSeparator={","} /></span></div>}
																						<div style={{ width: "50%" }} className="wrap-input has-dropdown date has-value line-gray"><span className="value"><h6>Tổng cộng</h6></span></div>
																						<div style={{ width: "50%" }} className="wrap-input has-dropdown time has-value line-gray"><span className="value"><h6><NumberFormat value={priceSummary.priceTotal} displayType={'text'} thousandSeparator={" "} decimalSeparator={","} suffix="đ" /></h6></span></div>
																				</div>
																		</div>
																</div>
																<div className="payment">
																		<div className="space m"></div>
																		<ul className="lp lp-confirm">
																				<li className="lp-li">
																						<div className="item-lp"><span className="title-lp">TIỀN CỌC</span><span className="value-lp value-total"><span><NumberFormat value={priceSummary.deposit} displayType={'text'} thousandSeparator={" "} decimalSeparator={","} suffix={"đ"} /></span></span></div>
																				</li>
																				<li className="lp-li">
																						<div className="item-lp"><span className="title-lp">TIỀN TRẢ SAU</span><span className="value-lp"><span><NumberFormat value={priceSummary.priceTotal - priceSummary.deposit} displayType={'text'} thousandSeparator={" "} decimalSeparator={","} suffix={"đ"} /></span></span></div>
																				</li>
																		</ul>
																</div>
															
																<div className="group-6">
																		<div className="require-paper">
																				<h6>Giấy tờ thuê xe (bắt buộc)</h6>
																				{car.requiredPapers && car.requiredPapers.length > 0 && <div className="ctn-desc-new">
																						<ul className="required">
																								{car.requiredPapers.map(paper => <li key={paper.id}> <img style={{ width: "20px", height: "20px" }} className="img-ico" src={paper.logo} alt="Mioto - Thuê xe tự lái" /> {paper.name}</li>)}
																						</ul>
																				</div>}
																				{(!car.requiredPapers || car.requiredPapers.length === 0) && <p>Không yêu cầu giấy tờ</p>}
																		</div>
																		{/* {car.requiredPapersOther && car.requiredPapersOther !== "" && <div className="require-paper">
																				<div className="space m" />
																				<h6>Các yêu cầu khác</h6>
																				<div className="ctn-desc-new">
																						<p>{car.requiredPapersOther}</p>
																				</div>
																		</div>} */}
																	
																</div>
																
																<div className="group-6">
																	<div className="space m"></div>
																			<div className="require-paper">
																					<h6>Tài sản thế chấp</h6>
																					{car.mortgages && car.mortgages.length > 0 && <div className="ctn-desc-new">
																								<ul className="required">
																										{car.mortgages.map(mortgage => <li key={mortgage.id}> {mortgage.name}</li>)}
																								</ul>
																						</div>}
																				</div>
																		</div>
																		<div className="group-6">
																			<div className="space m"></div>
																			<div className="require-paper">
																					<h6>Điều khoản thuê xe</h6>
																					{car.notes && car.notes !== "" && <div className="ctn-desc-new clause">
																							<p>{car.notes}</p>
																						</div>}
																				</div>
																		</div>
																<div className="full-wrapper">
																		<div className="space m" />
																		<h6><a href="/privacy#canceltrip" target="_blank">Chính sách hủy chuyến</a></h6>
																		{/* <div className="line-form">
																				<span className="value">- Thời điểm hủy chuyến <strong>&lt;= 7</strong> ngày trước khởi hành, phí hủy chuyến <strong>100%</strong> tiền cọc, hoàn trả <strong>0%</strong> tiền đã cọc.</span>
																				<br />
																				<span className="value">- Thời điểm hủy chuyến <strong>> 7</strong> ngày trước khởi hành, phí hủy chuyến <strong>0%</strong> tiền cọc, hoàn trả <strong>100%</strong> tiền đã cọc.</span>
																				<br />
																				<span className="value" style={{ display: "block" , color: "blue", fontSize: "10px" }}>* Tất cả các trường hợp hủy chuyến trong vòng <strong>1 giờ</strong> kể từ thời điểm đặt cọc thành công sẽ không áp dụng việc tính Phí hủy chuyến</span>
																		</div> */}
																		<table className="cancel-privacy">
																			<thead>
																					<tr>
																							<td>Thời điểm hủy chuyến</td>
																							<td>Phí hủy chuyến</td>
																							<td>Số tiền cọc hoàn trả</td>
																					</tr>
																			</thead>
																			<tbody>
																					<tr>
																							<td>Trong vòng 1 giờ sau khi đặt cọc</td>
																							<td>0% Tiền cọc</td>
																							<td>100% Tiền cọc</td>
																					</tr>
																					<tr>
																							<td>&gt; 7 ngày trước khởi hành</td>
																							<td>30% Tiền cọc</td>
																							<td>70% Tiền cọc</td>
																					</tr>
																					<tr>
																							<td>&lt;= 7 ngày trước khởi hành</td>
																							<td>100% Tiền cọc</td>
																							<td>0% Tiền cọc</td>
																					</tr>
																			</tbody>
																		</table>
																		<p className="text-danger font-13">*** Tiền cọc sẽ được hoàn trả trong vòng 1 - 3 ngày làm việc</p>
																		<div className="space m" />
																		{((car.requiredPapersOther && car.requiredPapersOther !== "") || (car.requiredPapers && car.requiredPapers.length > 0)) && <div className="squaredFour have-label">
																				<input id="read_paper" type="checkbox" onChange={this.readPaperChange.bind(this)} />
																				<label className="label" style={{ color: "red" }} htmlFor="read_paper">Tôi đã có đầy đủ giấy tờ chủ xe yêu cầu.</label>
																		</div>}
																		<div className="squaredFour have-label">
																				<input id="read_policy" type="checkbox" onChange={this.readPolicyChange.bind(this)} />
																				<label className="label" style={{ color: "red" }} htmlFor="read_policy">Tôi đã hiểu và đồng ý với chính sách hủy chuyến của Mioto.</label>
																		</div>
																		<div className="space m" />
																		<div className="wrap-btn has-2btn">
																				<div className="wr-btn"><a className="btn btn-secondary btn--m" onClick={backToSetup}>Huỷ</a></div>
																				<div className="wr-btn"><a className="btn btn-primary btn--m" onClick={this.booking.bind(this)} disabled={(this.isValidBooking() === true) ? false : true}>Đặt xe</a></div>
																		</div>
																		<div className="space m" />
																		<div className="space m" />
																		<Login show={this.state.showLoginForm} showModal={this.openLoginForm.bind(this)} getLoggedProfile={this.getLoggedProfile} hideModal={this.closeLoginForm} />
																</div>
														</div>
												</div>
										</div>
								</div>
						</div>
				}
				return (content);
		}
}

class CarBooking extends React.Component {
		constructor(props) {
				super(props);

				this.state = {
						err: commonErr.INNIT,
						errMsg: "",

						step: 1,
						trip: null
				}

				this.goToResult = this.goToResult.bind(this);
				this.goToError = this.goToError.bind(this);
				this.close = this.close.bind(this);
		}

		componentDidMount() {
				window.scrollTo(0, 0);
		}

		goToResult(trip) {
				this.setState({
						step: 2,
						trip: trip
				});
		}

		goToError(err, errMsg) {
				this.setState({
						step: 2,
						err: err,
						errMsg: errMsg
				});
		}

		close() {
				this.setState({
						step: 1,
						err: commonErr.INNIT,
						trip: null
				})
				this.props.hideModal();
		}

		render() {
				var title;
				var content;
				var modalClass;
				if (this.state.err === commonErr.LOADING) {
						content = <section className="body">
								<LoadingPage />
						</section>
				} else {
						const err = this.state.err;
						const errMsg = this.state.errMsg;
						const car = this.props.car;
						const owner = this.props.owner;
						const selfPickup = this.props.selfPickup;
						const fromAddress = this.props.fromAddress;
						const startDate = this.props.startDate;
						const endDate = this.props.endDate;
						const promoCode = this.props.promoCode;

						switch (this.state.step) {
								case 2:
										content = <BookingResult
												err={err}
												errMsg={errMsg}
												car={car}
												close={this.close}
												trip={this.state.trip} />
										title = "Thông báo"
										modalClass = "modal-sm"
										break;
								default:
										content = <BookingConfirm
												car={car}
												promoCode={promoCode}
												owner={owner}
												selfPickup={selfPickup}
												fromAddress={fromAddress}
												startDate={startDate}
												endDate={endDate}
												backToSetup={this.close}
												goToResult={this.goToResult}
												goToError={this.goToError}
										/>
										title = "Xác nhận đặt xe"
										modalClass = "modal-lg modal-confirm"
						}
				}

				return <Modal
						show={this.props.show}
						onHide={this.close}
						dialogClassName={`modal-dialog ${modalClass}`}
				>
						<Modal.Header closeButton={true} closeLabel={""}>
								<Modal.Title>{title}</Modal.Title>
						</Modal.Header>
						<Modal.Body>
								{content}
						</Modal.Body>
				</Modal>
		}
}

function mapSessionState(state) {
		return {
				session: state.session
		}
}

BookingConfirm = connect(mapSessionState)(BookingConfirm);

function mapDetailState(state) {
		return {
				carDetail: state.carDetail
		}
}

CarBooking = connect(mapDetailState)(CarBooking);

export default CarBooking;