import React from "react"
import { Modal } from "react-bootstrap"

import { commonErr } from "../common/errors"
import { getRejectTripReasonsOwner, rejectTripByOwner } from "../../model/car"

export default class RejectTripOwner extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            err: commonErr.INNIT,
            step: 0, //0 confirm 1: cancel trip 2: done
            errMsg: "",
            cancelReasons: null,
            ip_cancel_reason: "r0",
            ip_comment: ""
        }
    }

    componentDidMount() {
        getRejectTripReasonsOwner(this.props.trip.id).then(resp => {
            if (resp.data.error >= commonErr.SUCCESS) {
                this.setState({
                    err: resp.data.error,
                    errMsg: resp.data.errorMessage,
                    cancelReasons: resp.data.data.reasons
                });
            } else {
                this.setState({
                    err: resp.data.error,
                    errMsg: resp.data.errorMessage
                });
            }
        });
    }

    handleInputChange = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        });
    }

    goToCancelTrip() {
        this.setState({
            step: 1
        });
    }

    finishCancel() {
        this.props.hideModal();
        this.setState({
            step: 0,
            err: commonErr.INNIT
        });
        this.props.getTrip(this.props.trip.id);
    }

    isValidCancelTrip() {
        if (!this.state.ip_cancel_reason || this.state.ip_cancel_reason === "" || this.state.ip_cancel_reason === "0") {
            return false;
        }
        if (this.state.ip_cancel_reason === "r0" && (!this.state.ip_comment || this.state.ip_comment === "")) {
            return false;
        }
        return true;
    }

    cancelTrip() {
        if (!this.isValidCancelTrip()) {
            this.setState({
                err: commonErr.FAIL,
                errMsg: "Vui lòng chọn lý do hoặc ghi rõ lý do của bạn."
            });
        } else {
            rejectTripByOwner(this.props.trip.id, this.state.ip_cancel_reason, this.state.ip_comment).then(resp => {
                if (resp.data.error >= commonErr.SUCCESS) {
                    this.setState({
                        step: 2,
                        err: resp.data.error,
                        errMsg: resp.data.errorMessage
                    });
                } else {
                    this.setState({
                        step: 2,
                        err: resp.data.error,
                        errMsg: resp.data.errorMessage
                    });
                }
            });
        }
    }

    render() {
        var content;
        if (this.state.step === 2) {
            if (this.state.err >= commonErr.SUCCESS) {
                content = <div className="module-register" style={{ background: "none", padding: "5px" }}>
                    <div className="form-default form-s">
                        <div className="line-form">
                            <div className="textAlign-center"><i className="ic ic-verify"></i> Từ chối chuyến thành công</div>
                        </div>
                        <div className="clear" />
                        <div className="space m" />
                        {this.state.ip_cancel_reason === "r6" && <div className="wrap-btn has-2btn">
                            <div className="wr-btn">
                                <a className="btn btn-secondary btn--m" type="button" href={`/carsetting/${this.props.trip.carId}#calendarsetting`}>Cập nhật lịch</a>
                            </div>
                            <div className="wr-btn">
                                <a className="btn btn-primary btn--m" onClick={this.finishCancel.bind(this)}>Hoàn tất</a>
                            </div>
                        </div>}
                        {this.state.ip_cancel_reason !== "r6" && <div className="wrap-btn">
                            <a className="btn btn-primary btn--m" onClick={this.finishCancel.bind(this)}>Hoàn tất</a>
                        </div>}
                    </div>
                </div>
            } else {
                content = <div className="form-default form-s">
                    <div className="line-form">
                        <div className="textAlign-center"><i className="ic ic-error"></i> Từ chối chuyến thất bại. {this.state.errMsg}</div>
                    </div>
                    <div className="clear" />
                    <div className="space m" />
                    <p className="textAlign-center has-more-btn">
                        <button className="btn btn-primary btn--m" type="button" onClick={this.goToCancelTrip.bind(this)}>Thử lại</button>
                    </p>
                </div>
            }
        } else if (this.state.step === 1) {
            content = <div className="form-default form-s">
                <div className="line-form">
                    <div className="wrap-select">
                        <select name="ip_cancel_reason" onChange={this.handleInputChange.bind(this)}>
                            {this.state.cancelReasons && this.state.cancelReasons.map(
                                reason => <option value={reason.reason}>{reason.desc}</option>
                            )}
                        </select>
                    </div>
                </div>
                <div className="line-form">
                    <div className="wrap-input has-ico">
                        <input type="text" placeholder="Vui lòng nhập lý do hoặc lời nhắn" name="ip_comment" onChange={this.handleInputChange.bind(this)} />
                    </div>
                </div>
                <div className="clear" />
                <button className="btn btn-primary btn--m" type="button" name="cancelTripBtn" disabled={!this.isValidCancelTrip()} onClick={this.cancelTrip.bind(this)}>Huỷ chuyến</button>
            </div>
        } else {
            content = <div className="form-default form-s">
                <div className="line-form textAlignCenter">
                    <div className="textAlign-center">Bạn có chắc muốn từ chối yêu cầu thuê xe này?</div>
                    <div className="space m" />
                </div>
                <div className="clear" />
                <button className="btn btn-primary btn--m" type="button" onClick={this.goToCancelTrip.bind(this)}>Tiếp tục</button>
            </div>
        }

        return <Modal
            show={this.props.show}
            onHide={this.finishCancel.bind(this)}
            dialogClassName="modal-sm modal-dialog"
        >
            <Modal.Header closeButton={true} closeLabel={""}>
                <Modal.Title>Từ chối yêu cầu đặt xe</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                {content}
            </Modal.Body>
        </Modal>
    }
}