import React from "react"
import StarRatings from "react-star-ratings"
import { connect } from "react-redux"
import { geocodeByAddress, getLatLng } from 'react-places-autocomplete'
import cookie from 'react-cookies'

import { getLoggedProfile } from "../../actions/sessionAct"
import Login from "../login/login"
import { commonErr } from "../common/errors"
import { CarTransmission, CarFuel } from "../common/common"
import { LoadingPage } from "../common/loading"
import FavoriteButton from "../common/favoritebutton"
import { getCarDetail } from "../../model/car"
import ImageLightBox from "../common/imagelightbox"
import { MessagePage } from "../common/messagebox"
import BookingSideBar from "./bookingsidebar"
import CarReviews from "./carreviews"
import CarReport from "./carreport"
import RelatedCars from "./relatedcars"

import cover_default from "../../static/images/img_landscape_2.jpg"
import avatar_default from "../../static/images/avatar_default.png"
import discount from "../../static/images/percentage.png"

const Header = (props) => {
    const car = props.car;

    return <div className="modal-header">
        <div className="info-car">
            <h1 className="title-car">{car.name}</h1>
            <div className="group-line">
                <StarRatings
                    rating={car.rating.avg || 0}
                    starRatedColor="#00a550"
                    starDimension="17px"
                    starSpacing="1px"
                />
                <div className="bar-line" />
                <p> <span className="value">{car.totalTrips} </span>chuyến </p>
            </div>
        </div>
    </div>
}

class Cover extends React.Component {
    constructor() {
        super();
        this.state = {
            isShowLightBox: false,
            isShowQrLightBox: false,
            activeIndex: 1
        }

        this.showLightBox = this.showLightBox.bind(this);
        this.hideLightBox = this.hideLightBox.bind(this);
        this.showQrLightBox = this.showQrLightBox.bind(this);
        this.hideQrLightBox = this.hideQrLightBox.bind(this);
    }

    showLightBox(i) {
        this.setState({
            isShowLightBox: true,
            photoIndex: i
        })
    }

    hideLightBox() {
        this.setState({
            isShowLightBox: false
        })
    }

    showQrLightBox(i) {
        this.setState({
            isShowQrLightBox: true
        })
    }

    hideQrLightBox() {
        this.setState({
            isShowQrLightBox: false
        })
    }

    render() {
        const car = this.props.car;

        return <div className="info-car--desc no-pd cover-car">
            <div className="car-img">
                <div className="fix-img" onClick={() => this.showLightBox(0)}><img src={car.photos ? car.photos[0].fullUrl : cover_default} alt="Mioto - Thuê xe tự lái" /></div>
            </div>
            <span className="label-pos">
                {car.totalDiscountPercent > 0 && <span className="discount">Giảm {car.totalDiscountPercent}%</span>}
								{car.instant > 0 && <span className="rent"><i className="ic ic-sm-thunderbolt-wh" />Đặt xe nhanh</span>}
								{car.pp === 1 && <span className="free"><i className="ic ic-passport" />Chấp nhận passport</span>}
                {/* {car.deliveryEnable === 1 && car.deliveryPrice === 0 && <span className="free">Miễn phí giao nhận xe</span>} */}
            </span>
            <a className="qr-car" onClick={this.showQrLightBox}> <i className="ic ic-qr-code"></i></a>
            {car.photos && <div className="status-verify left" onClick={() => this.showLightBox(0)}>{car.photos.length} hình</div>}
            {/* {car.photosVerified === 1 && <div className="status-verify"><i className="ic ic-verify-stroke"></i> Ảnh đã xác thực</div>} */}
            <ImageLightBox isOpen={this.state.isShowLightBox} photoIndex={this.state.photoIndex} images={car.photos} hideLightBox={this.hideLightBox} />
            <ImageLightBox isOpen={this.state.isShowQrLightBox} images={[{ id: 0, thumbUrl: car.qr, fullUrl: car.qr }]} hideLightBox={this.hideQrLightBox} />
        </div>
    }
}

const Info = (props) => {
    const car = props.car;

    return <div className="info-car--desc">
        <div className="group"><span className="lstitle-new">ĐẶC ĐIỂM</span>
            <div className="ctn-desc-new">
                <ul className="features">
                    <li><i className="ic ic-chair" /> Số ghế: {car.seat}</li>
                    <li><i className="ic ic-trans" /> Truyền động: {CarTransmission[car.optionsTransmission]}</li>
                    <li><i className="ic ic-diesel" /> Nhiên liệu: {CarFuel[car.optionsFuel]}</li>
                    {car.optionsFuelConsumption > 0 && <li><i className="ic ic-fuel"> </i> Mức tiêu thụ nhiêu liệu: {car.optionsFuelConsumption} lít/100km</li>}
                </ul>
            </div>
        </div>
        {car.desc && car.desc !== "" && <div className="group">
            <span className="lstitle-new">MÔ TẢ</span>
            <div className="ctn-desc-new">
                <pre>{car.desc}</pre>
            </div>
        </div>}
        {car.features && car.features.length > 0 && <div className="group"><span className="lstitle-new">TÍNH NĂNG</span>
            <div className="ctn-desc-new">
                <ul className="accessories">
                    {car.features.map(function (feature, i) {
                        return <li key={i}> <img style={{ width: "20px", height: "20px" }} className="img-ico" src={feature.logo} alt="Mioto - Thuê xe tự lái" /> {feature.name}</li>
                    })}
                </ul>
            </div>
        </div>}
    </div>
}

const Papers = (props) => {
    const car = props.car;

    return <div className="info-car--desc">
       
        <div className="group">
            <span className="lstitle-new">GIẤY TỜ THUÊ XE (BẮT BUỘC) </span>
            <div className="ctn-desc-new">
                {car.requiredPapers && car.requiredPapers.length > 0 && <ul className="required two-opt">
                    {car.requiredPapers.map(paper => <li key={paper.id}> <img style={{ width: "20px", height: "20px" }} className="img-ico" src={paper.logo} alt="Mioto - Thuê xe tự lái" /> {paper.name}</li>)}
                </ul>}
                {(!car.requiredPapers || car.requiredPapers.length === 0) && <p>Không yêu cầu giấy tờ</p>}
            </div>
			</div>
			{car.mortgages && car.mortgages.length > 0 && <div className="group">
                            <span className="lstitle-new">TÀI SẢN THẾ CHẤP</span>
                            <div className="ctn-desc-new">
															<ul className="required two-opt">
                                    {car.mortgages.map(mortgage => <li key={mortgage.id}>{mortgage.name}</li>)}
                                </ul>
                            </div>
												</div>}
				{car.notes && car.notes !== "" && <div className="group">
            <span className="lstitle-new">ĐIỀU KHOẢN</span>
            <div className="ctn-desc-new clause">
                <pre>{car.notes}</pre>
            </div>
        </div>}
        {/* {car.requiredPapersOther && car.requiredPapersOther !== "" && <div className="group">
            <span className="lstitle-new">CÁC YÊU CẦU KHÁC</span>
            <div className="ctn-desc-new">
                <pre>{car.requiredPapersOther}</pre>
            </div>
        </div>} */}
    </div>
}

class LocationMap extends React.Component {
    componentDidMount() {
        const location = this.props.car.location;
        if (location) {
            // const map = this.map = new window.google.maps.Map(this.refs.map, {
            //     center: {
            //         lat: location.lat,
            //         lng: location.lon
            //     },
            //     zoom: 14,
            //     mapTypeControl: false,
            //     scrollwheel: false,
            //     clickableIcons: false,
            //     streetViewControl: false,
            //     zoomControl: true,
            //     zoomControlOptions: {
            //         position: window.google.maps.ControlPosition.RIGHT_BOTTOM
            //     }
            // })

            // const marker = new window.google.maps.Marker({
            //     map: map,
            //     icon: {
            //         path: window.google.maps.SymbolPath.CIRCLE,
            //         scale: map.getZoom() * 3,
            //         strokeColor: '#141414',
            //         strokeOpacity: 0.8,
            //         strokeWeight: 1,
            //         fillColor: '#666',
            //         fillOpacity: 0.35
            //     },
            //     position: new window.google.maps.LatLng(location.lat, location.lon)
            // });

            // this.marker = marker;

            // map.addListener('zoom_changed', () => {
            //     this.marker.setMap(null);

            //     var scale = map.getZoom() * 3;
            //     var bounds = map.getBounds();
            //     var center = map.getCenter();
            //     if (bounds && center) {
            //         var ne = bounds.getNorthEast();
            //         var radius = window.google.maps.geometry.spherical.computeDistanceBetween(center, ne);
            //         scale = 100000 / radius;
            //     }

            //     const marker = new window.google.maps.Marker({
            //         map: map,
            //         icon: {
            //             path: window.google.maps.SymbolPath.CIRCLE,
            //             scale: scale,
            //             strokeColor: '#141414',
            //             strokeOpacity: 0.8,
            //             strokeWeight: 1,
            //             fillColor: '#666',
            //             fillOpacity: 0.35
            //         },
            //         position: new window.google.maps.LatLng(location.lat, location.lon)
            //     });

            //     this.marker = marker;
            // });

            function addCircleToMap(map){
                map.addObject(new window.H.map.Circle(
                  // The central point of the circle
                  {lat: location.lat, lng: location.lon},
                  // The radius of the circle in meters
                  400,
                  {
                    style: {
                      strokeColor: 'gray', // Color of the perimeter
                      lineWidth: 2,
                      fillColor: 'rgba(0, 0, 0, 0.3)',  // Color of the circle
                      opacity: 0.3
                    }
                  }
                ));
            }

              /**
               * Boilerplate map initialization code starts below:
               */
              
              //Step 1: initialize communication with the platform
            var platform = new window.H.service.Platform({
                app_id: 'Oxoz6Kc6mhPKq4qP3wsM',
                app_code: '4JCjxp1mfXEXuD5E02yP2Q',
                useHTTPS: true,
                useCIT: true
            });
            var pixelRatio = window.devicePixelRatio || 1;
            var defaultLayers = platform.createDefaultLayers({
                tileSize: pixelRatio === 1 ? 256 : 512,
                ppi: pixelRatio === 1 ? undefined : 320
            });
            
            //Step 2: initialize a map - this map is centered over New Delhi
            var map = new window.H.Map(document.getElementById('map'),
                defaultLayers.normal.map,{
                center: {lat: location.lat, lng: location.lon},
                zoom: 14,
                pixelRatio: pixelRatio
            });
            
            //Step 3: make the map interactive
            // MapEvents enables the event system
            // Behavior implements default interactions for pan/zoom (also on mobile touch environments)
            var behavior = new window.H.mapevents.Behavior(new window.H.mapevents.MapEvents(map));
            behavior.disable(window.H.mapevents.Behavior.WHEELZOOM);

            // Create the default UI components
            var ui = window.H.ui.UI.createDefault(map, defaultLayers);
            
            // Now use the map as required...
            addCircleToMap(map);
        }
    }

    render() {
        return <div className="info-car--desc info--map">
            <div className="group">
                <span className="lstitle-new">VỊ TRÍ </span>
                <div className="ctn-desc-new">
                    <div ref="map" id="map" style={{width: '100%', height: '300px', background: 'grey' }}></div>        {/* add className="fix-map" for google maps*/}  
                </div>
                <span style={{ padding: "7px" }}><i className="ic ic-location-f" /> {this.props.car.locationAddr} </span>
            </div>
        </div>
    }
}

const Owner = (props) => {
    const owner = props.owner;

    return <div className="info-car--desc">
        <div className="group group-owner">
            <div className="profile-mini">
                <div className="avatar avatar-new" title="title name">
                    <a href={`/profile/${owner.uid}`} target="_blank">
                        <div className="avatar-img" style={{ backgroundImage: `url(${(owner.avatar && owner.avatar.thumbUrl) ? owner.avatar.thumbUrl : avatar_default})` }}></div>
                    </a>
                </div>
                <div className="desc">
                    <a href={`/profile/${owner.uid}`} target="_blank">
                        <span className="lstitle-new" style={{ width: "100px" }}>Chủ xe</span>
                        <h2>{owner.name}</h2>
                        <StarRatings
                            rating={owner.owner.rating.avg}
                            starRatedColor="#00a550"
                            starDimension="17px"
                            starSpacing="1px"
                        />
                    </a>
                </div>
            </div>
            <div className="owner-response">
                <div className="response-desc">
                    <p>Tỉ lệ phản hồi</p><span className="rate">{owner.owner.responseRate}</span>
                </div>
                <div className="response-desc">
                    <p>Thời gian phản hồi</p><span className="rate">{owner.owner.responseTime}</span>
                </div>
                <div className="response-desc">
                    <p>Tỉ lệ đồng ý</p><span className="rate">{owner.owner.acceptRate}</span>
                </div>
            </div>
        </div>
    </div>
}


class CarDetailPopup extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            err: commonErr.INNIT,
            errMsg: ""
        }

        this.getCarDetail = this.getCarDetail.bind(this);
        this.showLoginForm = this.showLoginForm.bind(this);
        this.closeLoginForm = this.closeLoginForm.bind(this);
        this.showReportForm = this.showReportForm.bind(this);
        this.closeReportForm = this.closeReportForm.bind(this);
        this.getLoggedProfile = this.getLoggedProfile.bind(this);
    }

    componentDidMount() {
        const carId = this.props.carId;
        const filter = this.props.carFinding.filter;

        if (filter.lat && filter.lng) {
            this.getCarDetail(carId, filter.lat, filter.lng, filter.startDate.valueOf(), filter.endDate.valueOf());
        } else if (filter.address && filter.address !== "") {
            geocodeByAddress(filter.address)
                .then(results => getLatLng(results[0]))
                .then(latLng => {
                    this.getCarDetail(carId, latLng.lat, latLng.lng, filter.startDate.valueOf(), filter.endDate.valueOf());
                })
        } else {
            this.getCarDetail(carId);
        }

        //GA
        const utmSrc = cookie.load("_utm_src") || "web_directly";
        const utmExt = cookie.load("_utm_ext") || "no_label";
        window.ga('send', 'event', utmSrc, "VIEW_CAR_DETAIL", utmExt);
    }

    componentWillReceiveProps(props) {
        const filter = props.carFinding.filter;
        const carId = props.carId;

        if (carId !== this.props.carId
            || filter.address !== this.props.carFinding.filter.address
            || filter.lat !== this.props.carFinding.filter.lat
            || filter.lng !== this.props.carFinding.filter.lng) {
            if (filter.lat && filter.lng) {
                this.getCarDetail(carId, filter.lat, filter.lng, filter.startDate.valueOf(), filter.endDate.valueOf());
            } else if (filter && filter.address !== "") {
                geocodeByAddress(filter.address)
                    .then(results => getLatLng(results[0]))
                    .then(latLng => {
                        this.getCarDetail(carId, latLng.lat, latLng.lng, filter.startDate.valueOf(), filter.endDate.valueOf());
                    });
            } else {
                this.getCarDetail(carId);
            }

            //GA
            const utmSrc = cookie.load("_utm_src") || "web_directly";
            const utmExt = cookie.load("_utm_ext") || "no_label";
            window.ga('send', 'event', utmSrc, "VIEW_CAR_DETAIL", utmExt);
        }
    }

    getCarDetail(carId, lat, lng, st, et) {
        this.setState({
            err: commonErr.LOADING
        })

        getCarDetail(carId, {
            lat: lat,
            lng: lng,
            st: st,
            et: et,
            vw: "cd"
        }).then(resp => {
            if (resp.data.error >= commonErr.SUCCESS) {
                this.setState({
                    err: resp.data.error,
                    errMsg: resp.data.errorMessage,
                    carDetail: {
                        info: resp.data.data.car,
                        priceOrigin: resp.data.data.car.priceOrigin,
                        profiles: resp.data.data.profiles,
                        relatedCars: resp.data.data.cars,
                        reviews: resp.data.data.reviews,
                        moreReview: resp.data.data.moreReview
                    }
                })
            } else {
                this.setState({
                    err: resp.data.error,
                    errMsg: resp.data.errorMessage
                })
            }
        })
    }

    isLogged() {
        return this.props.session.profile
            && this.props.session.profile.info
            && this.props.session.profile.info.uid;
    }

    showLoginForm() {
        this.setState({
            showLoginForm: true
        })
    }

    openLoginForm() {
        this.setState({
            showLoginForm: true
        });
    }

    closeLoginForm() {
        this.setState({
            showLoginForm: false
        })
    }

    showReportForm() {
        this.setState({
            showReportForm: true
        })
    }

    closeReportForm() {
        this.setState({
            showReportForm: false
        })
    }

    getLoggedProfile() {
        this.props.dispatch(getLoggedProfile());
    }

    reportCarBtnClick() {
        if (!this.isLogged()) {
            this.showLoginForm();
        } else {
            this.showReportForm();
        }
    }

    updatePrice(data) {
        this.setState({
            carDetail : {
                info: data.car,
                priceOrigin: data.car.priceOrigin,
                profiles: data.profiles,
                relatedCars: data.cars,
                reviews: data.reviews,
                moreReview: data.moreReview
            }
        });
    }

    render() {
        var content;
        if (this.state.err === commonErr.INNIT || this.state.err === commonErr.LOADING) {
            content = <LoadingPage />
        } else if (this.state.err >= commonErr.SUCCESS) {
            const loggedProfile = this.props.session.profile.info;
            const car = this.state.carDetail.info;
            const profiles = this.state.carDetail.profiles;
            const relatedCars = this.state.carDetail.relatedCars;
            var owner;
            if (profiles && profiles.length > 0) {
                for (var i = 0; i < profiles.length; ++i) {
                    if (profiles[i].uid === car.ownerId) {
                        owner = profiles[i];
                        break;
                    }
                }
            }

            content = <div className="modal-content" role="document">
                <div className="module-detail dt__wrapper">
                    <Header car={car} />
                    <div className="modal-body detail-container">
                        <div className="content-detail">
                            <Cover car={this.state.carDetail.info} />
                        </div>
                        <BookingSideBar detail={this.state.carDetail} owner={owner} showLoginForm={this.showLoginForm} loggedProfile={loggedProfile} updatePrice={this.updatePrice.bind(this)} />
                        <div className="content-detail">
                            <Info car={car} />
                            <Papers car={car} />
                            <Owner owner={owner} />
                            <LocationMap car={car} />
                            <CarReviews detail={this.state.carDetail} />
                            <div className="info-car--desc info--transparent">
                                <div className="wr-wrap-btn">
                                    <div className="wrap-btn">
                                        <a className="btn btn-default btn--m" onClick={this.reportCarBtnClick.bind(this)}>Báo xấu</a>
                                    </div>
                                    <div className="wrap-btn">
                                        <FavoriteButton car={car} showLoginForm={this.showLoginForm} isLogged={this.isLogged()} />
                                    </div>
                                </div>
                            </div>
                        </div>
                        {this.isLogged() && <CarReport show={this.state.showReportForm} hideModal={this.closeReportForm.bind(this)} car={car} />}
                        <Login show={this.state.showLoginForm} showModal={this.openLoginForm.bind(this)} hideModal={this.closeLoginForm} getLoggedProfile={this.getLoggedProfile} />
                    </div>
                    {relatedCars && <RelatedCars titleClass={"title--wh"} isNewTab={true} cars={relatedCars} tooglePopup={this.props.tooglePopup} />}
                </div>
            </div>
        } else {
            content = <MessagePage message={"Không tìm thấy thông tin."} />
        }

        return <div role="dialog">
            <button className="func-remove-x" onClick={this.props.tooglePopup}>
                <i className="ic ic-remove-wh" />
            </button>
            <div className="modal-backdrop dt-backdrop fade in"></div>
            <div className="fade modal in" role="dialog" tabIndex="-1" style={{ display: "block" }}>
                <div className="modal-dialog modal-details">
                    {content}
                </div>
            </div>
        </div>
    }
}

function mapFindingState(state) {
    return {
        carFinding: state.carFinding
    }
}

function mapSessionState(state) {
    return {
        session: state.session
    }
}

CarDetailPopup = connect(mapSessionState)(CarDetailPopup);
CarDetailPopup = connect(mapFindingState)(CarDetailPopup);

export default CarDetailPopup;