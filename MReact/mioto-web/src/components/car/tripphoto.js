import React from 'react'

import { Modal } from "react-bootstrap" 

import { commonErr } from "../common/errors"
import ImageUpload from "../common/imageupload"
import { LoadingInline } from "../common/loading"
import { MessageBox } from "../common/messagebox"

class ImgCarSlider extends React.Component {
	constructor(props) {
		super(props);

		this.initSwiper = this.initSwiper.bind(this);
	}

	componentDidMount() {
		this.initSwiper();
	}

	componentWillReceiveProps(props) {
		this.initSwiper();
	}

	initSwiper() {
		this.swiper = new window.Swiper(this.refs.swiperFrontCar, {
			slidesPerView: 4,
			spaceBetween: 24,
			threshold: 15,
			speed: 500,
			loop: false,
			slidesOffsetBefore: 0,
			slidesOffsetAfter: 0,
			paginationClickable: true,
			breakpoints: {
					992: {
							slidesPerView: 2.4,
						
					},
					768: {
							slidesPerView: 2,
						
					},
					480: {
							slidesPerView: 1,
						
					}
			}
		})
		this.swiper = new window.Swiper(this.refs.swiperBehindCar, {
			slidesPerView: 4,
			spaceBetween: 24,
			threshold: 15,
			speed: 500,
			loop: false,
			slidesOffsetBefore: 0,
			slidesOffsetAfter: 0,
			paginationClickable: true,
			breakpoints: {
					992: {
							slidesPerView: 2.4,
						
					},
					768: {
							slidesPerView: 2,
						
					},
					480: {
							slidesPerView: 1,
						
					}
			}
		})
		this.swiper = new window.Swiper(this.refs.swiperLeftCar, {
			slidesPerView: 4,
			spaceBetween: 24,
			threshold: 15,
			speed: 500,
			loop: false,
			slidesOffsetBefore: 0,
			slidesOffsetAfter: 0,
			paginationClickable: true,
			breakpoints: {
					992: {
							slidesPerView: 2.4,
						
					},
					768: {
							slidesPerView: 2,
						
					},
					480: {
							slidesPerView: 1,
						
					}
			}
		})
		this.swiper = new window.Swiper(this.refs.swiperRightCar, {
			slidesPerView: 4,
			spaceBetween: 24,
			threshold: 15,
			speed: 500,
			loop: false,
			slidesOffsetBefore: 0,
			slidesOffsetAfter: 0,
			paginationClickable: true,
			breakpoints: {
					992: {
							slidesPerView: 2.4,
						
					},
					768: {
							slidesPerView: 2,
						
					},
					480: {
							slidesPerView: 1,
						
					}
			}
		})
	}

	render() {
		const insPhotos = this.props.photos;
		const imageId = this.props.imageId;
		const trip = this.props.trip;
		return <div ref={this.props.refs} className="swiper-container swiper-trip-photo">
		<div className="swiper-wrapper box-photo__wrap">
				<div className="box-item swiper-slide">
					<div className="fix-img add-box">
						<div className="add-photo"><i className="ic ic-dark-plus"></i>
						{insPhotos.err !== commonErr.LOADING && <ImageUpload withPreview={false} onChange={this.props.handleInsPhotoChange} />}
            {insPhotos.err === commonErr.LOADING && <LoadingInline />}</div>
						<img src={insPhotos.tip}/></div>
				</div>
				{insPhotos.photos && insPhotos.photos.map(photo => <div className="box-item swiper-slide" key={photo.id}>
							{!trip.insActiveInfo && <a className="func-remove" onClick={() => this.props.removeInsPhoto(imageId, photo.id)}><i className="ic ic-remove" ></i></a>}
							<div className="fix-img">
								<img src={photo.url} />	
							</div>
						</div>)
				}
				
				{/* <div className="box-item swiper-slide">
					<div className="fix-img"><img src="https://n1-pstg.mioto.vn/cho_thue_xe_tu_lai_tphcm/hyundai_elantra_2017/p/g/2018/06/11/18/G291aLE79LkcVjL9stZUqw.jpg" /></div>
				</div> */}
			</div>
		</div>
	}
}

export default class TripPhoto extends React.Component {
	constructor(props) {
		super(props); 
		// this.state = {
		// 	frInsPhoto: props.frInsPhoto,
		// 	baInsPhoto: props.baInsPhoto
		// }
	}
	componentDidMount() {
		// parents update list photos
		// photos: this.props.photos

	}

	handleFrInsFileInputChange(img) {
		this.props.handleFrInsInputChange(img)
	}
	handleBaInsFileInputChange(img) {
		this.props.handleBaInsInputChange(img)
	}
	handleLeInsFileInputChange(img) {
		this.props.handleLeInsInputChange(img)
	}
	handleRiInsFileInputChange(img) {
		this.props.handleRiInsInputChange(img)
	}
	render() {
		return <Modal
					show={this.props.show}
					onHide={this.props.close}
					dialogClassName={`modal-lg`}
					>
					<Modal.Header closeButton={true} closeLabel={""}>
							<Modal.Title>Hình ảnh chuyến</Modal.Title>
					</Modal.Header>
					<Modal.Body>
							<p>Ảnh phía trước xe</p>
					<ImgCarSlider
							imageId={"fr"}
							refs={"swiperFrontCar"} 
							photos={this.props.frInsPhoto}
							handleInsPhotoChange={this.handleFrInsFileInputChange.bind(this)}
							removeInsPhoto={this.props.removeInsPhoto}
							trip={this.props.trip}
							/>
							<div className="space m"></div>
							<p>Ảnh phía sau xe</p>
							<ImgCarSlider imageId={"ba"} removeInsPhoto={this.props.removeInsPhoto} refs={"swiperBehindCar"} photos={this.props.baInsPhoto} handleInsPhotoChange={this.handleBaInsFileInputChange.bind(this)} trip={this.props.trip}/>
							<div className="space m"></div>
							<p>Ảnh bên trái xe</p>
							<ImgCarSlider imageId={"le"} removeInsPhoto={this.props.removeInsPhoto} refs={"swiperLeftCar"} photos={this.props.leInsPhoto} handleInsPhotoChange={this.handleLeInsFileInputChange.bind(this)} trip={this.props.trip}/>
							<div className="space m"></div>
							<p>Ảnh bên phải xe</p>
							<ImgCarSlider imageId={"ri"} removeInsPhoto={this.props.removeInsPhoto} refs={"swiperRightCar"} photos={this.props.riInsPhoto} handleInsPhotoChange={this.handleRiInsFileInputChange.bind(this)} trip={this.props.trip}/>
					</Modal.Body>
			</Modal>
	}
}