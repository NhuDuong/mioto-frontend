import React from "react"
import ReactSimpleRange from "react-simple-range"
import { Range } from 'rc-slider'

import CheckBoxList from "../common/checkboxlist"
import { getVehicleModel } from "../../model/car"

import 'rc-slider/assets/index.css'

class Filter extends React.Component {
    constructor(props) {
        super(props);
        this.state = {

        }
    }

    componentDidMount() {
        const vehicleType = this.props.filter.vehicleType;
        const vehicleMake = this.props.filter.vehicleMake;
        if (vehicleMake > 0) {
            getVehicleModel(vehicleType, vehicleMake).then(resp => {
                if (resp.data.data) {
                    this.setState({
                        vehicleModels: resp.data.data.vehicleModels
                    });
                }
            });
			}
			
			if (this.props.customFilterConfig) {
				const featuresAll = this.props.customFilterConfig.allFeatures;
        const features = this.props.filter.features;
        for (var i = 0; i < featuresAll.length; ++i) {
            var checked = false;
            if (features) {
                for (var j = 0; j < features.length; ++j) {
                    if (featuresAll[i].id === features[j]) {
                        checked = true;
                        break;
                    }
                }
            }

            featuresAll[i].checked = checked;
        }
			}		
    }

    resetFilter() {
        this.props.resetFilter();
    }

    onSortChange(event) {
        const filter = {
            ...this.props.filter,
            sort: event.target.value
        }
        this.props.setFilter(filter);
        this.props.searchCar(filter);
    }

    onPriceRangeChange(range) {
        const filter = {
            ...this.props.filter,
            maxPrice: range.value
        }
        this.props.setFilter(filter);
    }

    onPriceRangeChangeComplete(range) {
        const filter = {
            ...this.props.filter,
            maxPrice: range.value
        }
        this.props.setFilter(filter);
        this.props.searchCar(filter);
    }

    onLimitKMRangeChange(range) {
        const filter = {
            ...this.props.filter,
            limitKM: range.value
        }
        this.props.setFilter(filter);
    }

    onLimitKMRangeChangeComplete(range) {
        const filter = {
            ...this.props.filter,
            limitKM: range.value
        }
        this.props.setFilter(filter);
        this.props.searchCar(filter);
    }

    onLimitKMPriceRangeChange(range) {
        const filter = {
            ...this.props.filter,
            limitKMPrice: range.value
        }
        this.props.setFilter(filter);
    }

    onLimitKMPriceRangeChangeComplete(range) {
        const filter = {
            ...this.props.filter,
            limitKMPrice: range.value
        }
        this.props.setFilter(filter);
        this.props.searchCar(filter);
    }

    onDPriceRangeChange(range) {
        const filter = {
            ...this.props.filter,
            dPrice: range.value
        }
        this.props.setFilter(filter);
    }

    onDPriceRangeChangeComplete(range) {
        const filter = {
            ...this.props.filter,
            dPrice: range.value
        }
        this.props.setFilter(filter);
        this.props.searchCar(filter);
    }

    onInstantBookingChange(event) {
        const filter = {
            ...this.props.filter,
            instantBooking: event.target.checked,
        }
        this.props.setFilter(filter);
        this.props.searchCar(filter);
    }

    onPVerifiedChange(event) {
        const filter = {
            ...this.props.filter,
            pVerified: event.target.checked,
        }
        this.props.setFilter(filter);
        this.props.searchCar(filter);
    }

    onDirectDeliveryChange(event) {
        const filter = {
            ...this.props.filter,
            directDelivery: event.target.checked,
        }
        this.props.setFilter(filter);
        this.props.searchCar(filter);
    }

    onVTypeChange(event) {
        const vehicleType = event.target.value;
        const vehicleMake = this.props.filter.vehicleMake;
        if (vehicleMake > 0) {
            getVehicleModel(vehicleType, vehicleMake).then(resp => {
                if (resp.data.data) {
                    this.setState({
                        vehicleModels: resp.data.data.vehicleModels
                    });
                    const filter = {
                        ...this.props.filter,
                        vehicleType: vehicleType,
                        vehicleModel: 0
                    }
                    this.props.setFilter(filter);
                    this.props.searchCar(filter);
                }
            });
        } else {
            this.setState({
                vehicleModels: null
            });
            const filter = {
                ...this.props.filter,
                vehicleType: vehicleType,
                vehicleModel: 0
            }
            this.props.setFilter(filter);
            this.props.searchCar(filter);
        }
    }

    onVMakeChange(event) {
        const vehicleMake = event.target.value;
        if (vehicleMake > 0) {
            const vehicleType = this.props.filter.vehicleType;
            getVehicleModel(vehicleType, vehicleMake).then(resp => {
                if (resp.data.data) {
                    this.setState({
                        vehicleModels: resp.data.data.vehicleModels
                    });
                    const filter = {
                        ...this.props.filter,
                        vehicleMake: vehicleMake,
                        vehicleModel: 0
                    }
                    this.props.setFilter(filter);
                    this.props.searchCar(filter);
                }
            });
        } else {
            this.setState({
                vehicleModels: null
            });
            const filter = {
                ...this.props.filter,
                vehicleMake: 0,
                vehicleModel: 0
            }
            this.props.setFilter(filter);
            this.props.searchCar(filter);
        }
    }

    onVModelChange(event) {
        const filter = {
            ...this.props.filter,
            vehicleModel: event.target.value
        }
        this.props.setFilter(filter);
        this.props.searchCar(filter);
    }

    onTransmissionChange(event) {
        const filter = {
            ...this.props.filter,
            transmission: event.target.value
        }
        this.props.setFilter(filter);
        this.props.searchCar(filter);
    }

    onRadiusRangeChange(range) {
        const filter = {
            ...this.props.filter,
            radius: range.value / 1.60934
        }
        this.props.setFilter(filter);
    }

    onRadiusRangeChangeComplete(range) {
        const filter = {
            ...this.props.filter,
            radius: range.value / 1.60934
        }
        this.props.setFilter(filter);
        this.props.searchCar(filter);
    }

    onFuelChange(event) {
        const filter = {
            ...this.props.filter,
            fuel: event.target.value
        }
        this.props.setFilter(filter);
        this.props.searchCar(filter);
    }

    onFuelCRangeChange(range) {
        const filter = {
            ...this.props.filter,
            fuelC: range.value
        }
        this.props.setFilter(filter);
    }

    onFuelCRangeChangeComplete(range) {
        const filter = {
            ...this.props.filter,
            fuelC: range.value
        }
        this.props.setFilter(filter);
        this.props.searchCar(filter);
    }

    onYearRangeChange(range) {
        const filter = {
            ...this.props.filter,
            minYear: range[0],
            maxYear: range[1]
        }
			this.props.setFilter(filter);
    }

    onYearRangeAfterChange(range) {
        const filter = {
            ...this.props.filter,
            minYear: range[0],
            maxYear: range[1]
			}
			
        this.props.setFilter(filter);
        this.props.searchCar(filter);
    }

	onSeatRangeChange(range) {
      const filter = {
            ...this.props.filter,
            minSeat: range[0],
            maxSeat: range[1]
			}
        this.props.setFilter(filter);
    }

	onSeatRangeAfterChange(range) {
        const filter = {
            ...this.props.filter,
            minSeat: range[0],
            maxSeat: range[1]
        }
        this.props.setFilter(filter);
        this.props.searchCar(filter);
    }

    onFeaturesChange(event) {
			var features = [];
			if (this.props.filter.features) {
					features = this.props.filter.features.slice();
			}

			const featureId = event.target.value;
			if (event.target.checked) {
					features.push(featureId)
			} else {
					features = features.filter(function (feature) {
							return feature !== featureId;
					});
			}

			const featuresAll = this.props.customFilterConfig.allFeatures;
			for (var i = 0; i < featuresAll.length; ++i) {
					var checked = false;
					if (features) {
							for (var j = 0; j < features.length; ++j) {
									if (featuresAll[i].id === features[j]) {
											checked = true;
											break;
									}
							}
					}

					featuresAll[i].checked = checked;
			}

			const filter = {
					...this.props.filter,
					features: features
			}
			
			this.props.setFilter(filter);
			this.props.searchCar(filter);
	}


	render() {

        const viewModes = this.props.viewModes;
        const defPriceRange = this.props.defPriceRange;
        const defLimitKMRange = this.props.defLimitKMRange;
        const defLimitKMPriceRange = this.props.defLimitKMPriceRange;
        const defDPriceRange = this.props.defDPriceRange;
        const defFuelCRange = this.props.defFuelCRange;
        const defSeatRange = this.props.defSeatRange;
        const defYearRange = this.props.defYearRange;
        const defRadiusRange = this.props.defRadiusRange;
        const mode = this.props.viewMode;
        const filter = this.props.filter;
        const counters = this.props.counters;
        const customFilterConfig = this.props.customFilterConfig;
        const vehicleTypes = this.props.vehicleTypes;
        const vehicleMakes = this.props.vehicleMakes;
				const vehicleModels = this.state.vehicleModels;

        var i, j, counter;

        if (counters) {
            if (vehicleTypes) {
                for (i = 0; i < vehicleTypes.length; ++i) {
                    counter = 0;
                    if (counters.vehicleTypes) {
                        for (j = 0; j < counters.vehicleTypes.length; ++j) {
                            if (vehicleTypes[i].id === counters.vehicleTypes[j].id) {
                                counter = counters.vehicleTypes[j].counter;
                                break;
                            }
                        }
                    }
                    vehicleTypes[i].counter = counter;
                }
            }
            if (vehicleMakes) {
                for (i = 0; i < vehicleMakes.length; ++i) {
                    counter = 0;
                    if (counters.vehicleMakes) {
                        for (j = 0; j < counters.vehicleMakes.length; ++j) {
                            if (vehicleMakes[i].id === counters.vehicleMakes[j].id) {
                                counter = counters.vehicleMakes[j].counter;
                                break;
                            }
                        }
                    }
                    vehicleMakes[i].counter = counter;
                }
            }
            if (vehicleModels) {
                for (i = 0; i < vehicleModels.length; ++i) {
                    counter = 0;
                    if (counters.vehicleModels) {
                        for (j = 0; j < counters.vehicleModels.length; ++j) {
                            if (vehicleModels[i].id === counters.vehicleModels[j].id) {
                                counter = counters.vehicleModels[j].counter;
                                break;
                            }
                        }
                    }
                    vehicleModels[i].counter = counter;
                }
            }
        }

        const sortValue = (customFilterConfig && customFilterConfig.sort) ? customFilterConfig.sort : "op";
        const minPrice = (customFilterConfig && customFilterConfig.minPrice) ? customFilterConfig.minPrice / 1000 : defPriceRange.min;
        const maxPrice = (customFilterConfig && customFilterConfig.maxPrice) ? customFilterConfig.maxPrice / 1000 : defPriceRange.max;
        const minLimitKM = defLimitKMRange.min;
        const maxLimitKM = (customFilterConfig && customFilterConfig.maxLimitKM) ? customFilterConfig.maxLimitKM + 50 : defLimitKMRange.max;
        const minLimitKMPrice = defLimitKMPriceRange.min;
        const maxLimitKMPrice = (customFilterConfig && customFilterConfig.maxLimitKMPrice) ? customFilterConfig.maxLimitKMPrice / 1000 + 5: defLimitKMPriceRange.max;
        const minDPrice = defDPriceRange.min;
        const maxDPrice = (customFilterConfig && customFilterConfig.maxDPrice) ? customFilterConfig.maxDPrice / 1000 : defDPriceRange.max;
        const minFuelC = (customFilterConfig && customFilterConfig.minFuelC) ? customFilterConfig.minFuelC : defFuelCRange.min;
        const maxFuelC = (customFilterConfig && customFilterConfig.maxFuelC) ? customFilterConfig.maxFuelC : defFuelCRange.max;
        const minSeat = (customFilterConfig && customFilterConfig.seatMin) ? customFilterConfig.seatMin : defSeatRange.min;
        const maxSeat = (customFilterConfig && customFilterConfig.seatMax) ? customFilterConfig.seatMax : defSeatRange.max;
        const minYear = (customFilterConfig && customFilterConfig.yearMin) ? customFilterConfig.yearMin : defYearRange.min;
        const maxYear = (customFilterConfig && customFilterConfig.yearMax) ? customFilterConfig.yearMax : defYearRange.max;
        const minRadius = (customFilterConfig && customFilterConfig.minRadius) ? customFilterConfig.minRadius : defRadiusRange.min;
        const maxRadius = (customFilterConfig && customFilterConfig.maxRadius) ? customFilterConfig.maxRadius : defRadiusRange.max;

        const hasFilter = filter.maxPrice < maxPrice
            || filter.vehicleType != "0" || filter.vehicleMake != "0" || filter.vehicleModel != "0"
            || filter.minSeat > minSeat || filter.maxSeat < maxSeat
            || filter.minYear > minYear || filter.maxYear < maxYear
            || filter.instantBooking === true || filter.directDelivery === true
            || filter.dPrice < maxDPrice
            || filter.transmission != "0"
            || filter.features.length > 0
            || filter.fuel != "0" || filter.fuelC > minFuelC
            || filter.limitKM < maxLimitKM
            || filter.limitKMPrice < maxLimitKMPrice
            || filter.radius * 1.60934 < maxRadius
            || filter.pVerified === true
            || filter.sort != sortValue[0].id
			
        return <div className="filter-container">
            <div className="content-filter">
                <div className="rent-car has-scroll filter-body">
                    <div className="scroll-inner">
                        {mode === viewModes.LIST && <span className="slstitle">Sắp xếp</span>}
                        {mode === viewModes.LIST && <div className="line-form">
                            <div className="wrap-select">
                                <select onChange={this.onSortChange.bind(this)} value={filter.sort}>
                                    {customFilterConfig && customFilterConfig.sort.map(option => <option key={option.id} value={option.id}>{option.name}</option>)}
                                </select>
                            </div>
                        </div>}
                        <span className="slstitle">Mức giá</span>
                        <div className="line-form">
                            <div className="range-slider">
                                <ReactSimpleRange
                                    step={defPriceRange.step}
                                    min={minPrice}
                                    max={maxPrice}
                                    value={filter.maxPrice}
                                    sliderSize={6}
                                    thumbSize={13}
                                    label={false}
                                    trackColor={"#00a550"}
                                    thumbColor={"#141414"}
                                    onChange={this.onPriceRangeChange.bind(this)}
                                    onChangeComplete={this.onPriceRangeChangeComplete.bind(this)}
                                />
                                <span className="range-slider__value2" style={{ float: "right", fontSize: "12px" }}>{filter.maxPrice < maxPrice ? `Từ dưới ${filter.maxPrice}K/ngày` : "Tất cả giá"}</span>
                            </div>
                        </div>
                        <span className="slstitle">Loại xe</span>
                        <div className="line-form">
                            <div className="wrap-select">
                                <select onChange={this.onVTypeChange.bind(this)} value={filter.vehicleType}>
                                    <option value="0">Tất cả</option>
                                    {vehicleTypes && vehicleTypes.map(option => <option key={option.id} value={option.id}>{option.name} {option.counter > 0 ? `(${option.counter} xe)` : ""}</option>)}
                                </select>
                            </div>
                        </div>
                        <span className="slstitle">Hãng xe</span>
                        <div className="line-form">
                            <div className="wrap-select">
                                <select onChange={this.onVMakeChange.bind(this)} value={filter.vehicleMake}>
                                    <option value="0">Tất cả</option>
                                    {vehicleMakes && vehicleMakes.map(option => <option key={option.id} value={option.id}>{option.name} {option.counter > 0 ? `(${option.counter} xe)` : ""}</option>)}
                                </select>
                            </div>
                        </div>
                        {filter.vehicleMake !== 0 && <span className="slstitle">Dòng xe</span>}
                        {filter.vehicleMake !== 0 && <div className="line-form">
                            <div className="wrap-select">
                                <select onChange={this.onVModelChange.bind(this)} value={filter.vehicleModel}>
                                    <option value="0">Tất cả</option>
                                    {vehicleModels && vehicleModels.map(option => <option key={option.id} value={option.id}>{option.name} {option.counter > 0 ? `(${option.counter} xe)` : ""}</option>)}
                                </select>
                            </div>
                        </div>}
                        <div className="line-form">
                            <div className="squaredFour have-label">
                                <input id="ip_instant_booking" type="checkbox" checked={filter.instantBooking === true} onChange={this.onInstantBookingChange.bind(this)} />
                                <label htmlFor="ip_instant_booking">Đặt xe nhanh <div className="tooltip"> <i className="ic ic-question-mark"></i>
                                    <div className="tooltip-text">Lọc những xe có thể đặt ngay lập tức mà không cần chủ xe phê duyệt</div>
                                </div></label>
                            </div>
                        </div>
                        {this.props.isShowAdvance && <a className="func-more block" onClick={this.props.hideAdvance}>Cơ bản <i className="i-arrow-up"></i></a>}
                        {!this.props.isShowAdvance && <a className="func-more block" onClick={this.props.showAdvance}>Nâng cao <i className="i-arrow-down"></i></a>}
                        <div className={`advance-filter ${this.props.isShowAdvance ? "" : "hidden"}`}>
                            <span className="slstitle">Số chỗ</span>
                            <div className="line-form">
                                <div className="range-slider">
                                    <Range
                                        min={minSeat}
                                        max={maxSeat}
                                        value={[filter.minSeat, filter.maxSeat]}
                                        allowCross={false}
                                        pushable={true}
                                        handleStyle={[{ background: "#141414", border: "none", cursor: "pointer", boxShadow: "none" }, { background: "#141414", border: "none", cursor: "pointer", boxShadow: "none" }]}
                                        trackStyle={[{ background: "#00a550", height: "6px", cursor: "pointer" }]}
                                        railStyle={{ background: "rgb(185, 185, 185)", height: "6px", borderRadius: "0px", cursor: "pointer" }}
                                        onChange={this.onSeatRangeChange.bind(this)}
                                        onAfterChange={this.onSeatRangeAfterChange.bind(this)}
                                    />
                                    <span className="range-slider__value2"
                                        style={{ paddingTop: "5px", float: "right", fontSize: "12px" }}>{filter.minSeat > minSeat && filter.maxSeat < maxSeat && `Từ ${filter.minSeat} đến ${filter.maxSeat}`}{filter.minSeat <= minSeat && filter.maxSeat < maxSeat && `Dưới ${filter.maxSeat}`}{filter.minSeat > minSeat && filter.maxSeat >= maxSeat && `Trên ${filter.minSeat}`}{filter.minSeat <= minSeat && filter.maxSeat >= maxSeat && `Bất kì`}</span>
                                </div>
                            </div>
                            <span className="slstitle">Năm sản xuất</span>
                            <div className="line-form">
                                <div className="range-slider">
                                    <Range
                                        min={minYear}
                                        max={maxYear}
                                        value={[filter.minYear, filter.maxYear]}
                                        allowCross={false}
                                        pushable={true}
                                        handleStyle={[{ background: "#141414", border: "none", cursor: "pointer", boxShadow: "none" }, { background: "#141414", border: "none", cursor: "pointer", boxShadow: "none" }]}
                                        trackStyle={[{ background: "#00a550", height: "6px", cursor: "pointer" }]}
                                        railStyle={{ background: "rgb(185, 185, 185)", height: "6px", borderRadius: "0px", cursor: "pointer" }}
                                        onChange={this.onYearRangeChange.bind(this)}
                                        onAfterChange={this.onYearRangeAfterChange.bind(this)}
                                    />
                                    <span className="range-slider__value2"
                                        style={{ paddingTop: "5px", float: "right", fontSize: "12px" }}>{filter.minYear > minYear && filter.maxYear < maxYear && `Từ ${filter.minYear} đến ${filter.maxYear}`}{filter.minYear <= minYear && filter.maxYear < maxYear && `Trước ${filter.maxYear}`}{filter.minYear > minYear && filter.maxYear >= maxYear && `Sau ${filter.minYear}`}{filter.minYear <= minYear && filter.maxYear >= maxYear && `Bất kì`}</span>
                                </div>
                            </div>
                            <span className="slstitle">Giao dịch</span>
                            <div className="line-form">                                
                                <div className="squaredFour have-label">
                                    <input id="ip_direct_delivery" type="checkbox" checked={filter.directDelivery === true} onChange={this.onDirectDeliveryChange.bind(this)} />
                                    <label htmlFor="ip_direct_delivery">Giao xe tận nơi</label>
                                </div>
                            </div>
                            {filter.directDelivery && <span className="slstitle">Phí giao nhận xe</span>}
                            {filter.directDelivery && <div className="line-form">
                                <div className="range-slider">
                                    <ReactSimpleRange
                                        step={defDPriceRange.step}
                                        min={minDPrice}
                                        max={maxDPrice}
                                        value={filter.dPrice}
                                        sliderSize={6}
                                        thumbSize={13}
                                        label={false}
                                        trackColor={"#00a550"}
                                        thumbColor={"#141414"}
                                        onChange={this.onDPriceRangeChange.bind(this)}
                                        onChangeComplete={this.onDPriceRangeChangeComplete.bind(this)}
                                    />
                                    <span className="range-slider__value2" style={{ float: "right", fontSize: "12px" }}>{filter.dPrice < maxDPrice && filter.dPrice > minDPrice && `Từ dưới ${filter.dPrice}K/km`}{filter.dPrice <= minDPrice && `Miễn phí`}{filter.dPrice >= maxDPrice && `Bất kì`}</span>
                                </div>
                            </div>}
                            <span className="slstitle">Truyền động</span>
                            <div className="line-form">
                                <div className="wrap-select">
                                    <select onChange={this.onTransmissionChange.bind(this)} value={filter.transmission}>
                                        <option value="0">Tất cả</option>
                                        <option value="1">Số tự động</option>
                                        <option value="2">Số sàn</option>
                                    </select>
                                </div>
                            </div>
                            <span className="slstitle">Nhiên liệu</span>
                            <div className="line-form">
                                <div className="wrap-select">
                                    <select onChange={this.onFuelChange.bind(this)} value={filter.fuel}>
                                        <option value="0">Tất cả</option>
                                        <option value="1">Xăng</option>
                                        <option value="2">Dầu</option>
                                    </select>
                                </div>
                            </div>
                            <span className="slstitle">Mức tiêu thụ nhiên liệu</span>
                            <div className="line-form">
                                <div className="range-slider">
                                    <ReactSimpleRange
                                        step={defFuelCRange.step}
                                        min={minFuelC}
                                        max={maxFuelC}
                                        value={filter.fuelC}
                                        sliderSize={6}
                                        thumbSize={13}
                                        label={false}
                                        trackColor={"#00a550"}
                                        thumbColor={"#141414"}
                                        onChange={this.onFuelCRangeChange.bind(this)}
                                        onChangeComplete={this.onFuelCRangeChangeComplete.bind(this)}
                                    />
                                    <span className="range-slider__value2" style={{ float: "right", fontSize: "12px" }}>{filter.fuelC <= maxFuelC && filter.fuelC > minFuelC && `Từ dưới ${filter.fuelC}l/100km`}{filter.fuelC <= minFuelC && `Bất kì`}</span>
                                </div>
                            </div>
                            <span className="slstitle">Tính năng</span>
                            <div className="line-form">
																{customFilterConfig && <CheckBoxList id="cb-feature-sb" options={customFilterConfig.allFeatures} onOptionChange={this.onFeaturesChange.bind(this)} />}
                            </div>
                            <span className="slstitle">Giới hạn quãng đường</span>
                            <div className="line-form">
                                <div className="range-slider">
                                    <ReactSimpleRange
                                        step={defLimitKMRange.step}
                                        min={minLimitKM}
                                        max={maxLimitKM}
                                        value={filter.limitKM}
                                        sliderSize={6}
                                        thumbSize={13}
                                        label={false}
                                        trackColor={"#00a550"}
                                        thumbColor={"#141414"}
                                        onChange={this.onLimitKMRangeChange.bind(this)}
                                        onChangeComplete={this.onLimitKMRangeChangeComplete.bind(this)}
                                    />
                                    <span className="range-slider__value2" style={{ float: "right", fontSize: "12px" }}>{filter.limitKM < maxLimitKM && filter.limitKM > minLimitKM && `Trên ${filter.limitKM}km/ngày`}{filter.limitKM <= minLimitKM && `Không giới hạn`}{filter.limitKM >= maxLimitKM && `Bất kì`}</span>
                                </div>
                            </div>
                            <span className="slstitle">Phí vượt giới hạn</span>
                            {filter.limitKM > minLimitKM && <div className="line-form">
                                <div className="range-slider">
                                    <ReactSimpleRange
                                        step={defLimitKMPriceRange.step}
                                        min={minLimitKMPrice}
                                        max={maxLimitKMPrice}
                                        value={filter.limitKMPrice}
                                        sliderSize={6}
                                        thumbSize={13}
                                        label={false}
                                        trackColor={"#00a550"}
                                        thumbColor={"#141414"}
                                        onChange={this.onLimitKMPriceRangeChange.bind(this)}
                                        onChangeComplete={this.onLimitKMPriceRangeChangeComplete.bind(this)}
                                    />
                                    <span className="range-slider__value2" style={{ float: "right", fontSize: "12px" }}>{filter.limitKMPrice < maxLimitKMPrice && filter.limitKMPrice > minLimitKMPrice && `Từ dưới ${filter.limitKMPrice}K/km`}{filter.limitKMPrice <= minLimitKMPrice && `Miễn phí`}{filter.limitKMPrice >= maxLimitKMPrice && `Bất kì`}</span>
                                </div>
                            </div>}
                            {filter.limitKM <= minLimitKM && <div className="line-form">
                                <div className="range-slider">
                                    <ReactSimpleRange
                                        step={defLimitKMPriceRange.step}
                                        min={minLimitKMPrice}
                                        max={maxLimitKMPrice}
                                        value={maxLimitKMPrice}
                                        sliderSize={6}
                                        label={false}
                                        disableThumb={true}
                                        trackColor={"rgb(185, 185, 185)"}
                                    />
                                    <span className="range-slider__value2" style={{ float: "right", fontSize: "12px" }}>Bất kì</span>
                                </div>
                            </div>}
                            {mode === viewModes.LIST && <span className="slstitle">Khoảng cách</span>}
                            {mode === viewModes.LIST && <div className="line-form">
                                <div className="range-slider">
                                    <ReactSimpleRange
                                        step={defRadiusRange.step}
                                        min={minRadius}
                                        max={maxRadius}
                                        value={filter.radius * 1.60934 <= maxRadius ? filter.radius * 1.60934 : maxRadius}
                                        sliderSize={6}
                                        thumbSize={13}
                                        label={false}
                                        trackColor={"#00a550"}
                                        thumbColor={"#141414"}
                                        onChange={this.onRadiusRangeChange.bind(this)}
                                        onChangeComplete={this.onRadiusRangeChangeComplete.bind(this)}
                                    />
                                    <span className="range-slider__value2" style={{ float: "right", fontSize: "12px" }}>{(filter.radius * 1.60934 < maxRadius) ? `Trong vòng ${Math.round(filter.radius * 1.60934)} km` : "Bất kì"}</span>
                                </div>
                            </div>}
                            <span className="slstitle">Hình ảnh</span>
                            <div className="line-form">
                                <div className="squaredFour have-label">
                                    <input id="ip_photos_verified" type="checkbox" checked={filter.pVerified === true} onChange={this.onPVerifiedChange.bind(this)} />
                                    <label htmlFor="ip_photos_verified">Đã xác thực</label>
                                </div>
                            </div>
                        </div>
                        <div className="space m"></div>
                        <a className={`btn btn-default btn--m btn-reset hide-on-med-and-down ${hasFilter ? "has-dot-red" : ""}`} style={{ width: "100%" }} onClick={this.resetFilter.bind(this)}><i className="ic ic-reset"></i> Bỏ lọc</a>
                    </div>
                </div>
            </div>
        </div >
    }
}

export default Filter;