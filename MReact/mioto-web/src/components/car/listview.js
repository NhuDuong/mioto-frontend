import React from "react"
import InfiniteScroll from "react-infinite-scroller"
import { connect } from "react-redux"

import CarItem from "../common/caritem"
import { LoadingPage } from "../common/loading"
import { commonErr } from "../common/errors"
import { MessageLine } from "../common/messagebox"

class ListView extends React.Component {
    searchCarListMore() {
        this.props.searchCarListMore({
            ...this.props.carFinding.filter,
            pos: this.props.carFinding.cars.length,
            fromCarId: this.props.carFinding.cars[this.props.carFinding.cars.length - 1].id
        });
    }

    render() {
        const searchData = this.props.carFinding;
        if (searchData.err === commonErr.INNIT || searchData.err === commonErr.LOADING) {
            return <div className="module-map">
                <div className="map-container">
                    <LoadingPage />
                </div>
            </div>
        } else if (searchData.cars) {
            return <div className="module-map">
                <div className="has-list">
                    <div className="map-container">
                        <InfiniteScroll pageStart={0}
                            loadMore={this.searchCarListMore.bind(this)}
                            hasMore={searchData.more === 1}
                            loader={<div key={0}></div>}
                            useWindow={false}>
                            <div className="listing-car">
                                <ul>
                                    {searchData.cars.map(car => <li key={car.id}>
                                        <CarItem car={car} tooglePopup={this.props.tooglePopup} />
                                    </li>)}
                                    <div className="space m" />
                                    <div className="space m" />
                                </ul>
                            </div>
                        </InfiniteScroll>
                    </div>
                </div>
            </div>
        } else {
            return <div className="module-map">
                <div className="map-container"><MessageLine message="Không tìm thấy xe nào." /></div>
            </div>
        }
    }
}

function mapState(state) {
    return {
        carFinding: state.carFinding
    }
}

ListView = connect(mapState)(ListView);

export default ListView;

