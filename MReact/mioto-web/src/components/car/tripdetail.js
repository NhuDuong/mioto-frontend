import React from "react"
import { connect } from "react-redux"
import { Redirect } from "react-router-dom"
import moment from 'moment'
import NumberFormat from "react-number-format"
import StarRatings from "react-star-ratings"
import cookie from 'react-cookies'
import { Modal } from "react-bootstrap"
import HtmlToReactParser from "html-to-react"

import Header from "../common/header"
import Footer from "../common/footer"
import Login from "../login/login"
import { commonErr } from "../common/errors"
import {getCountDownTime, convertToDateTimeText, convertToDateTimeObj} from "../common/common"
import { LoadingPage, LoadingOverlay, LoadingInline } from "../common/loading"
import { getLoggedProfile } from "../../actions/sessionAct"
import { getTripDetail, uploadInsurancePhoto, removeInsurancePhoto, activeInsurance, getActiveInsurance} from "../../model/car"
import { requestPaymentWithUrl, getListPaymentProvider } from "../../model/payment"
import CancelTrip from "./canceltrip"
import CancelTripOwner from "./canceltripowner"
import ApproveTripOwner from "./approvetripowner"
import RejectTripOwner from "./rejecttripowner"
import PickupTrip from "./pickuptrip"
import ReturnTrip from "./returntrip"
import ReviewTripTraveler from "./reviewtriptraveler"
import { formatPrice, formatTitleInUrl } from "../common/common"
import TripPhoto from "./tripphoto"
import ImageUpload from "../common/imageupload"

import { MessageBox } from "../common/messagebox"


import avatar_default from "../../static/images/avatar_default.png"
import car_icon from "../../static/images/marker_car.png"

import mic from "../../static/images/homev2/mic.png"
import global_care from "../../static/images/global-care.png"

class CarMapLocation extends React.Component {
    initMap(refs, lat, lng) {
        const map = this.map = new window.google.maps.Map(refs.map, {
            center: {
                lat: lat,
                lng: lng
            },
            zoom: 14,
            mapTypeControl: false,
            scrollwheel: false,
            clickableIcons: false,
            streetViewControl: false,
            zoomControl: true,
            zoomControlOptions: {
                position: window.google.maps.ControlPosition.RIGHT_BOTTOM
            }
        })

        this.pMarker = new window.google.maps.Marker({
            icon: {
                url: car_icon,
                scaledSize: new window.google.maps.Size(34, 44)
            },
            position: new window.google.maps.LatLng(this.props.location.lat, this.props.location.lon)
        });

        this.rMarker = new window.google.maps.Marker({
            icon: {
                path: window.google.maps.SymbolPath.CIRCLE,
                scale: map.getZoom() * 3,
                strokeColor: '#141414',
                strokeOpacity: 0.8,
                strokeWeight: 1,
                fillColor: '#666',
                fillOpacity: 0.35
            },
            position: new window.google.maps.LatLng(this.props.location.lat, this.props.location.lon)
        });

        if (this.props.status === 4 || this.props.status === 5) {
            this.pMarker.setMap(map);
        } else {
            this.rMarker.setMap(map);
        }

        map.addListener('zoom_changed', () => {
            if (this.props.status !== 4 && this.props.status !== 5) {
                this.rMarker.setMap(null);

                var scale = map.getZoom() * 3;
                var bounds = map.getBounds();
                var center = map.getCenter();
                if (bounds && center) {
                    var ne = bounds.getNorthEast();
                    var radius = window.google.maps.geometry.spherical.computeDistanceBetween(center, ne);
                    scale = 140000 / radius;
                }

                const rMarker = new window.google.maps.Marker({
                    map: map,
                    icon: {
                        path: window.google.maps.SymbolPath.CIRCLE,
                        scale: scale,
                        strokeColor: '#141414',
                        strokeOpacity: 0.8,
                        strokeWeight: 1,
                        fillColor: '#666',
                        fillOpacity: 0.35
                    },
                    position: new window.google.maps.LatLng(this.props.location.lat, this.props.location.lon)
                });

                this.rMarker = rMarker;
            }
        });
    }

    componentDidMount() {
        const { location } = this.props;
        if (this.props.location) {
            setTimeout(() => {
                this.initMap(this.refs, location.lat, location.lon);
            }, 100)
        }
    }

    componentWillUpdate() {
        if (this.props.status === 4 || this.props.status === 5) {
            if (this.rMarker) {
                this.rMarker.setMap(null);
            }
            if (this.pMarker) {
                this.pMarker.setMap(this.map);
            }
        } else {
            if (this.rMarker) {
                this.rMarker.setMap(this.map);
            }
            if (this.pMarker) {
                this.pMarker.setMap(null);
            }
        }
    }

    render() {
        return <div ref="map" className="fix-map" />
    }
}

class PaymentBtn extends React.Component {
    constructor() {
        super();
        this.state = {
            step: 1,
            err: commonErr.INNIT,
            errMsg: "",

            isShowMethodBox: false,
            methods: [],
            activeMethod: "ttlh"
        }

        this.onDepositBtnClick = this.onDepositBtnClick.bind(this);
        this.onContinueBtnClick = this.onContinueBtnClick.bind(this);
        this.onFinishBtnClick = this.onFinishBtnClick.bind(this);
        this.onBackBtnClick = this.onBackBtnClick.bind(this);
        this.closeMethodBox = this.closeMethodBox.bind(this);
			this.onMethodChange = this.onMethodChange.bind(this);

    }

    componentDidMount() {
        this.setState({
            err: commonErr.LOADING
        });
        getListPaymentProvider().then(resp => {
            if (resp.data.data && resp.data.data.length > 0) {
                this.setState({
                    methods: resp.data.data,
                    activeMethod: resp.data.data[0].connectId,
                    err: resp.error
                })
            }
        })
    }

    onDepositBtnClick() {
        this.setState({
            isShowMethodBox: true
        });
    }

    closeMethodBox() {
        this.setState({
            isShowMethodBox: false,
            err: commonErr.INNIT,
            errMsg: ""
        });
    }

    onContinueBtnClick() {
        this.setState({
            step: 2
        });
    }

    onBackBtnClick() {
        this.setState({
            step: 1,
            err: commonErr.INNIT,
            errMsg: ""
        })
    }

    onFinishBtnClick(method) {
        //GA
        const utmSrc = cookie.load("_utm_src") || "web_directly";
        const utmExt = cookie.load("_utm_ext") || "no_label";
        window.ga('send', 'event', utmSrc, "CAR_DEPOSIT", utmExt);

        if (method.connectId === "ttlh") {
            this.setState({
                step: 1,
                isShowMethodBox: false
            });
        } else {
            this.setState({
                err: commonErr.LOADING,
            });
            requestPaymentWithUrl(method.linkPayment, this.props.tripId).then(paymentResp => {
                if (paymentResp.data.data) {
                    window.location.assign(paymentResp.data.data.payUrl);
                } else {
                    this.setState({
                        err: paymentResp.data.error,
                        errMsg: paymentResp.data.errorMessage
                    });
                }
            });
        }
    }

    onMethodChange(id) {
        this.setState({
            activeMethod: id
        })
    }

    render() {
        if (this.state.err === commonErr.LOADING) {
            return <LoadingOverlay />
        } else {
            var title;
            var content;
            if (this.state.err < commonErr.SUCCESS) {
                title = "Thông báo"
                content = <div className="module-register" style={{ background: "none", padding: "5px" }}>
                    <div className="form-default form-s">
                        <div className="line-form">
                            <div className="textAlign-center">Rất tiếc, đã có lỗi xảy ra!</div>
                        </div>
                        <div className="space m" />
                        <div className="wr-btn">
                            <a className="btn btn-primary btn--m" type="button" onClick={this.onBackBtnClick}>Quay lại</a>
                        </div>
                    </div>
                </div>
            } else if (this.state.methods && this.state.methods.length > 0) {
                const activeMethod = this.state.methods.filter(method => {
                    return method.connectId === this.state.activeMethod
                })[0];
                const step = this.state.step;
                switch (step) {
                    case 2:
                        title = "Thông báo"
                        var message;
                        var btn;
                        if (activeMethod.connectId === "ttlh") {
                            title = activeMethod.name;
                            const htmlToReactParser = new HtmlToReactParser.Parser();
                            message = htmlToReactParser.parse(activeMethod.description);
                            btn = btn = <div className="wrap-btn has-2btn">
                                <div className="wr-btn">
                                    <a className="btn btn-secondary btn--m" type="button" onClick={this.onBackBtnClick}>Quay lại</a>
                                </div>
                                <div className="wr-btn">
                                    <a className="btn btn-primary btn--m" onClick={() => this.onFinishBtnClick(activeMethod)}>Đóng</a>
                                </div>
                            </div>
                        } else {
                            title = activeMethod.name;
                            message = `Bạn sẽ được chuyển hướng đến trang thanh toán của nhà cung cấp ${activeMethod.name}`
                            btn = <div className="wrap-btn has-2btn">
                                <div className="wr-btn">
                                    <a className="btn btn-secondary btn--m" type="button" onClick={this.onBackBtnClick}>Quay lại</a>
                                </div>
                                <div className="wr-btn">
                                    <a className="btn btn-primary btn--m" onClick={() => this.onFinishBtnClick(activeMethod)}>Tiếp tục</a>
                                </div>
                            </div>
                        }
                        content = <div className="module-register" style={{ background: "none", padding: "5px" }}>
                            <div className="form-default form-s">
                                <div className="line-form">
                                    {message}
                                </div>
                                <div className="space m" />
                                {btn}
                            </div>
                        </div>
                        break;
                    default:
                        title = "Chọn phương thức"
                        content = <div className="form-default form-s">
                            <div className="line-form">
                                <div className="body-promo">
                                    {this.state.methods.map((method, i) => <div key={i} className="box-promo">
                                        <div className="left">
                                            <img className="img-promo" src={method.logo} alt="Mioto - Thuê xe tự lái" />
                                        </div>
                                        <div className="center center-payment">
                                            <p className="code">{method.name}</p>
                                            <p class="desc">{method.caption}</p>
                                        </div>
                                        <div className="right right-payment">
                                            <label className="custom-radio custom-control">
                                                <input className="custom-control-input" type="radio"
                                                    name={method.connectId}
                                                    checked={activeMethod.connectId === method.connectId}
                                                    onChange={() => this.onMethodChange(method.connectId)} />
                                                <span className="custom-control-indicator" />
                                            </label>
                                        </div>
                                    </div>)}
                                </div>
                            </div>
                            {activeMethod.connectId === "ttlh" && <a className="btn btn-primary btn--m" onClick={() => this.onContinueBtnClick(activeMethod)}>Tiếp tục</a>}
                            {activeMethod.connectId !== "ttlh" && <a className="btn btn-primary btn--m" onClick={() => this.onFinishBtnClick(activeMethod)}>Tiếp tục</a>}
                        </div >
                }
            }

            return <div className="wr-btn">
                <a className="btn btn-primary btn--m" onClick={this.onDepositBtnClick.bind(this)}>Đặt cọc</a>
                <Modal
                    show={this.state.isShowMethodBox}
                    onHide={this.closeMethodBox}
                    dialogClassName={`modal-dialog modal-sm`}
                >
                    <Modal.Header closeButton={true} closeLabel={""}>
                        <Modal.Title>{title}</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        {content}
                    </Modal.Body>
                </Modal>
            </div>
        }
    }
}

class TripPhotoSlider extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			frInsPhoto: this.props.frInsPhoto,
			baInsPhoto: this.props.baInsPhoto,
			leInsPhoto: this.props.leInsPhoto,
			riInsPhoto: this.props.riInsPhoto,
			trip: this.props.trip
					
		}
		this.isEnabledUploadIns = this.isEnabledUploadIns.bind(this)
	}
	componentDidMount() {
		this.swiper = new window.Swiper(this.refs.swiperTripPhoto, {
			slidesPerView: 4,
			spaceBetween: 24,
			threshold: 15,
			speed: 500,
			loop: false,
			slidesOffsetBefore: 0,
			slidesOffsetAfter: 0,
			paginationClickable: true,
			breakpoints: {
				992: {
					slidesPerView: 3.2,
						
				},
				768: {
					slidesPerView: 2.4,
						
				},
				480: {
					slidesPerView: 1.5,
						
				}
			}
		})
	}
    
	componentWillReceiveProps(props) {
		this.setState({
			frInsPhoto: props.frInsPhoto,
			baInsPhoto: props.baInsPhoto,
			leInsPhoto: props.leInsPhoto,
			riInsPhoto: props.riInsPhoto,

		})
	}
	isEnabledUploadIns() {
		return moment().valueOf() + this.state.trip.insUpThr >= this.state.trip.tripDateFrom
		
	}

	render() {
		
		return <div ref={"swiperTripPhoto"} className="swiper-container swiper-trip-photo">
			    <div className="swiper-wrapper box-photo__wrap">
                    <div className="box-item swiper-slide">
						<a onClick={this.isEnabledUploadIns() ? this.props.showTripPhoto : this.props.showMessageUpload }>
							<div className={`fix-img ${this.state.frInsPhoto.photos && this.state.frInsPhoto.photos.length > 0 ? "" : "add-box"}`}>
								<img src={this.state.frInsPhoto ? (this.state.frInsPhoto.photos && this.state.frInsPhoto.photos.length > 0 ? this.state.frInsPhoto.photos[(this.state.frInsPhoto.photos.length - 1)].url : this.state.frInsPhoto.tip) : ""} />
								{!this.state.frInsPhoto.photos && <div className="add-photo"><i className="ic ic-dark-plus"></i></div>}
							</div>
						</a>
					</div>
                    <div className="box-item swiper-slide">
						<a onClick={this.isEnabledUploadIns() ? this.props.showTripPhoto : this.props.showMessageUpload}>
						<div className={`fix-img ${this.state.baInsPhoto.photos && this.state.baInsPhoto.photos.length > 0 ? "" : "add-box"}`}>
							<img src={this.state.baInsPhoto ? (this.state.baInsPhoto.photos && this.state.baInsPhoto.photos.length > 0 ? this.state.baInsPhoto.photos[(this.state.baInsPhoto.photos.length - 1)].url : this.state.baInsPhoto.tip) : ""} />
							{!this.state.baInsPhoto.photos && <div className="add-photo"><i className="ic ic-dark-plus"></i></div>}
						</div>
						</a>
					</div>
                    <div className="box-item swiper-slide">
						<a onClick={this.isEnabledUploadIns() ? this.props.showTripPhoto : this.props.showMessageUpload}>
						<div className={`fix-img ${this.state.leInsPhoto.photos && this.state.leInsPhoto.photos.length > 0 ? "" : "add-box"}`}>
							<img src={this.state.leInsPhoto ? (this.state.leInsPhoto.photos && this.state.leInsPhoto.photos.length > 0 ? this.state.leInsPhoto.photos[(this.state.leInsPhoto.photos.length - 1)].url : this.state.leInsPhoto.tip) : ""} />
							{!this.state.leInsPhoto.photos && <div className="add-photo"><i className="ic ic-dark-plus"></i></div>}
						</div>
						</a>
					</div>
          <div className="box-item swiper-slide">
						<a onClick={this.isEnabledUploadIns() ? this.props.showTripPhoto : this.props.showMessageUpload}>
						<div className={`fix-img ${this.state.riInsPhoto.photos && this.state.riInsPhoto.photos.length > 0 ? "" : "add-box"}`}>
							<img src={this.state.riInsPhoto ? (this.state.riInsPhoto.photos && this.state.riInsPhoto.photos.length > 0 ? this.state.riInsPhoto.photos[(this.state.riInsPhoto.photos.length - 1)].url : this.state.riInsPhoto.tip) : ""} />
							{!this.state.riInsPhoto.photos && <div className="add-photo"><i className="ic ic-dark-plus"></i></div>}
						</div>
						</a>
					</div>
				</div>
			</div>
            
	}
}

class TripDetail extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showLoginForm: false,
            showCancelForm: false,
            showApproveForm: false,
            showRejectForm: false,
            showPickupForm: false,
            showReturnForm: false,
            showCancelOwnerForm: false,
            showTripPhoto: false,  						
						showMessageUpload: false,  
						showInsuranceForm: false,
						isShowTravelerReviewForm: false,
            err: commonErr.INNIT,
            errMsg: "",
            car: null,
            trip: null,
            profiles: null,
            paymentUrl: "",
            frInsPhoto: {
                err: commonErr.INNIT
            }, 
            baInsPhoto: {
                err: commonErr.INNIT
            },
            leInsPhoto: {
                err: commonErr.INNIT	
            }, 
            riInsPhoto: {
                err: commonErr.INNIT	
            },
        }

        this.showLoginForm = this.showLoginForm.bind(this);
        this.closeLoginForm = this.closeLoginForm.bind(this);
        this.getLoggedProfile = this.getLoggedProfile.bind(this);
        this.getTrip = this.getTrip.bind(this);
        this.showTripPhoto = this.showTripPhoto.bind(this);
        this.closeTripPhoto = this.closeTripPhoto.bind(this);
        this.showMessageUpload = this.showMessageUpload.bind(this);
        this.closeMessageUpload = this.closeMessageUpload.bind(this);
        this.handleFrInsFileInputChange = this.handleFrInsFileInputChange.bind(this);
        this.handleBaInsFileInputChange = this.handleBaInsFileInputChange.bind(this);
        this.handleLeInsFileInputChange = this.handleLeInsFileInputChange.bind(this);
        this.handleRiInsFileInputChange = this.handleRiInsFileInputChange.bind(this);
        this.removeInsurancePhoto = this.removeInsurancePhoto.bind(this);
        this.reRenderTripPhoto = this.reRenderTripPhoto.bind(this);
        this.hideMessageBox = this.hideMessageBox.bind(this);
		
    }

    componentDidMount() {
        window.scrollTo(0, 0);

        const tripId = this.props.match.params.tripId;
        this.getTrip(tripId);

    }

    getTrip(tripId) {
        getTripDetail(tripId).then(resp => {
            if (resp.data.error >= commonErr.SUCCESS && resp.data.data) {
                var frInsPhoto;
                var baInsPhoto;
                var leInsPhoto;
                var riInsPhoto;
                var self = this;
                if(resp.data.data.trip.insPhotos){
                    frInsPhoto = resp.data.data.trip.insPhotos.filter(ins => {
                        return ins.id === "fr"
                    })[0];
                
                    baInsPhoto = resp.data.data.trip.insPhotos.filter(ins => {
                            return ins.id === "ba"
                    })[0];
        
                    leInsPhoto = resp.data.data.trip.insPhotos.filter(ins => {
                            return ins.id === "le"
                    })[0];
        
                    riInsPhoto = resp.data.data.trip.insPhotos.filter(ins => {
                            return ins.id === "ri"
                    })[0];
                    self.setState({
                        frInsPhoto: {
                            ...self.state.frInsPhoto,
                            photos: frInsPhoto.photos, 
                            tip: frInsPhoto.tip
                        },
                        baInsPhoto: {
                            ...self.state.baInsPhoto,
                            photos: baInsPhoto.photos,
                            tip: baInsPhoto.tip
                        },
                        leInsPhoto: {
                            ...self.state.leInsPhoto,
                            photos: leInsPhoto.photos,
                            tip: leInsPhoto.tip
                        },
                        riInsPhoto: {
                            ...self.state.riInsPhoto,
                            photos: riInsPhoto.photos,
                            tip: riInsPhoto.tip
                        },
                    })
                }	
                this.setState({
                    err: resp.data.error,
                    car: resp.data.data.car,
                    trip: resp.data.data.trip,
                    profiles: resp.data.data.profiles                  
                });

                if (resp.data.data.trip.timeExpired > 0) {
                    var countDownDate = resp.data.data.trip.timeExpired;

                    // Update the count down every 1 second
                    var x = setInterval(() => {

                        // Get todays date and time
                        var timeRemain = getCountDownTime(countDownDate);

                        // Display the result in the element with id="demo"
                        this.setState({
                            countdown: convertToDateTimeText(timeRemain)
                        })

                        // If the count down is finished, write some text 
                        if (timeRemain < 0) {
                            clearInterval(x);
                            this.setState({
                                countdown: convertToDateTimeText(timeRemain)
                            })
                        }
                    }, 1000);
                } else {
                    this.setState({
                        countdown: ""
                    })
                }
				
            } else {
                this.setState({
                    err: resp.data.error,
                    errMsg: resp.data.errorMessage
                });
            }
        });
    }

    showLoginForm() {
        this.setState({
            showLoginForm: true
        });
    }

    openLoginForm() {
        this.setState({
            showLoginForm: true
        });
    }

    closeLoginForm() {
        this.setState({
            showLoginForm: false
        });
    }

    cancelTrip() {
        this.showCancelForm();
    }

    showCancelForm() {
        this.setState({
            showCancelForm: true
        });
    }

    closeCancelForm() {
        this.setState({
            showCancelForm: false
        });
    }

    showApproveForm() {
        this.setState({
            showApproveForm: true
        });
    }

    closeApproveForm() {
        this.setState({
            showApproveForm: false
        });
    }

    showRejectForm() {
        this.setState({
            showRejectForm: true
        });
    }

    closeRejectForm() {
        this.setState({
            showRejectForm: false
        });
    }

    showPickupForm() {
        this.setState({
            showPickupForm: true
        });
    }

    closePickupForm() {
        this.setState({
            showPickupForm: false
        });
    }

    showReturnForm() {
        this.setState({
            showReturnForm: true
        });
    }

    closeReturnForm() {
        this.setState({
            showReturnForm: false
        });
    }

    showCancelOwnerForm() {
        this.setState({
            showCancelOwnerForm: true
        });
    }

    closeCancelOwnerForm() {
        this.setState({
            showCancelOwnerForm: false
        });
    }

    showTravelerReviewForm() {
        this.setState({
            isShowTravelerReviewForm: true
        });
    }

    closeTravelerReviewForm() {
        this.setState({
            isShowTravelerReviewForm: false
        });
    }

		showTripPhoto() {
			this.setState({
				showTripPhoto: true
			});
		}
	
		closeTripPhoto() {
			this.setState({
				showTripPhoto: false
			});
		}
		showInsuranceForm() {
				this.setState({
					showInsuranceForm: true
				});
		}
	
		closeInsuranceForm() {
			this.setState({
				showTripPhoto: false
			})

			const tripId = this.props.match.params.tripId;
			this.getTrip(tripId);
		}
	
    travelerReview() {
        this.showTravelerReviewForm();
    }

    getLoggedProfile() {
        this.props.dispatch(getLoggedProfile());
    }

    buildOwnerTripStatusBar(trip) {
        if (trip.status >= 25)
            return <div className="status-wrapper">
                <div className="content-register d-shadow">
                    <div className="stepbystep">
                        <ul>
                            <li className="active"><span className="nu">1</span><span className="value">Duyệt yêu cầu</span></li>
                            <li className="active"><span className="nu">2</span><span className="value">Thanh toán cọc</span></li>
                            <li className="active"><span className="nu">3</span><span className="value">Khởi hành</span></li>
                            <li className="active"><span className="nu">4</span><span className="value">Kết thúc</span></li>
                        </ul>
                    </div>
                </div>
                <div className="booking-status status-trips arrow-5">
                    <p><span className="status black-dot"> </span><span>Chuyến đã kết thúc. {trip.status === 25 && trip.needReview === 1 && "Bạn hãy để lại nhận xét cho chuyến của mình."}</span></p>
                </div>
            </div>

        if (trip.status >= 23)
            return <div className="status-wrapper">
                <div className="content-register d-shadow">
                    <div className="stepbystep">
                        <ul>
                            <li><span className="nu">1</span><span className="value">Duyệt yêu cầu</span></li>
                            <li><span className="nu">2</span><span className="value">Thanh toán cọc</span></li>
                            <li><span className="nu">3</span><span className="value">Khởi hành</span></li>
                            <li><span className="nu">4</span><span className="value">Kết thúc</span></li>
                        </ul>
                    </div>
                </div>
                <div className="booking-status status-trips reject-trip">
                    <p><span className="status red-dot"></span><span>Chuyến đã bị huỷ. {(trip.cancelReason && trip.cancelReason !== "") ? `Lý do: ${trip.cancelReason}` : ""}</span></p>
                </div>
            </div>

        if (trip.status >= 21)
            return <div className="status-wrapper">
                <div className="content-register d-shadow">
                    <div className="stepbystep">
                        <ul>
                            <li><span className="nu">1</span><span className="value">Duyệt yêu cầu</span></li>
                            <li><span className="nu">2</span><span className="value">Thanh toán cọc</span></li>
                            <li><span className="nu">3</span><span className="value">Khởi hành</span></li>
                            <li><span className="nu">4</span><span className="value">Kết thúc</span></li>
                        </ul>
                    </div>
                </div>
                <div className="booking-status status-trips reject-trip">
                    <p><span className="status red-dot"></span><span>Chuyến đã bị huỷ. {(trip.cancelReason && trip.cancelReason !== "") ? `Lý do: ${trip.cancelReason}` : ""}</span></p>
                </div>
            </div>

        if (trip.status >= 20)
            return <div className="status-wrapper">
                <div className="content-register d-shadow">
                    <div className="stepbystep">
                        <ul>
                            <li><span className="nu">1</span><span className="value">Duyệt yêu cầu</span></li>
                            <li><span className="nu">2</span><span className="value">Thanh toán cọc</span></li>
                            <li><span className="nu">3</span><span className="value">Khởi hành</span></li>
                            <li><span className="nu">4</span><span className="value">Kết thúc</span></li>
                        </ul>
                    </div>
                </div>
                <div className="booking-status status-trips reject-trip">
                    <p><span className="status red-dot"></span><span>Bạn đã từ chối chuyến. {(trip.rejectReason && trip.rejectReason !== "") ? `Lý do: ${trip.rejectReason}` : ""}</span></p>
                </div>
            </div>

        if (trip.status >= 5)
            return <div className="status-wrapper">
                <div className="content-register d-shadow">
                    <div className="stepbystep">
                        <ul>
                            <li className="active"><span className="nu">1</span><span className="value">Duyệt yêu cầu</span></li>
                            <li className="active"><span className="nu">2</span><span className="value">Thanh toán cọc</span></li>
                            <li className="active"><span className="nu">3</span><span className="value">Khởi hành</span></li>
                            <li className="next-step"><span className="nu">4</span><span className="value">Kết thúc</span></li>
                        </ul>
                    </div>
                </div>
                <div className="booking-status status-trips arrow-4">
                    <p><span className="status green-dot"> </span><span>Chuyến cho thuê của bạn sẽ kết thúc lúc {moment(trip.tripDateTo).format('HH:mm ngày DD/MM/YYYY')}</span></p>
                </div>
            </div>

        if (trip.status >= 4)
            return <div className="status-wrapper">
                <div className="content-register d-shadow">
                    <div className="stepbystep">
                        <ul>
                            <li className="active"><span className="nu">1</span><span className="value">Duyệt yêu cầu</span></li>
                            <li className="active"><span className="nu">2</span><span className="value">Thanh toán cọc</span></li>
                            <li className="next-step"><span className="nu">3</span><span className="value">Khởi hành</span></li>
                            <li><span className="nu">4</span><span className="value">Kết thúc</span></li>
                        </ul>
                    </div>
                </div>
                <div className="booking-status status-trips arrow-3">
                    <p><span className="status green-dot"> </span><span>Chuyến cho thuê của bạn sẽ bắt đầu lúc {moment(trip.tripDateFrom).format('HH:mm ngày DD/MM/YYYY')}</span></p>
                </div>
            </div>

        if (trip.status >= 2)
            return <div className="status-wrapper">
                <div className="content-register d-shadow">
                    <div className="stepbystep">
                        <ul>
                            <li className="active"><span className="nu">1</span><span className="value">Duyệt yêu cầu</span></li>
                            <li className="nex-step"><span className="nu">2</span><span className="value">Thanh toán cọc</span></li>
                            <li><span className="nu">3</span><span className="value">Khởi hành</span></li>
                            <li><span className="nu">4</span><span className="value">Kết thúc</span></li>
                        </ul>
                    </div>
                </div>
                <div className="booking-status status-trips arrow-2">
                    <p><span className="status orange-dot"> </span><span>Đang chờ khách thuê đặt cọc.</span></p>
                    {this.state.countdown && this.state.countdown !== "" && <span className="countdown">{this.state.countdown}</span>}
                </div>
            </div>


        if (trip.status >= 1)
            return <div className="status-wrapper">
                <div className="content-register d-shadow">
                    <div className="stepbystep">
                        <ul>
                            <li className="nex-step"><span className="nu">1</span><span className="value">Duyệt yêu cầu</span></li>
                            <li><span className="nu">2</span><span className="value">Thanh toán cọc</span></li>
                            <li><span className="nu">3</span><span className="value">Khởi hành</span></li>
                            <li><span className="nu">4</span><span className="value">Kết thúc</span></li>
                        </ul>
                    </div>
                </div>
                <div className="booking-status status-trips arrow-1">
                    <p><span className="status orange-dot"> </span><span>Bạn cần duyệt yêu cầu đặt xe trước thời hạn, nếu không hệ thống sẽ tự động hủy chuyến.</span></p>
                    {this.state.countdown && this.state.countdown !== "" && <span className="countdown">{this.state.countdown}</span>}
                </div>
            </div>

        return null;
    }

    buildTravelerTripStatusBar(trip, isInstant) {
        if (trip.status >= 25)
            return <div className="status-wrapper">
                <div className="content-register d-shadow">
                    <div className="stepbystep">
                        <ul>
                            <li className="active"><span className="nu">1</span><span className="value">Duyệt yêu cầu</span></li>
                            <li className="active"><span className="nu">2</span><span className="value">Thanh toán cọc</span></li>
                            <li className="active"><span className="nu">3</span><span className="value">Khởi hành</span></li>
                            <li className="active"><span className="nu">4</span><span className="value">Kết thúc</span></li>
                        </ul>
                    </div>
                </div>
                <div className="booking-status status-trips arrow-5">
                    <p><span className="status black-dot"> </span><span>Chuyến đã kết thúc. {trip.status === 25 && trip.needReview === 1 && "Bạn hãy để lại nhận xét cho chuyến của mình."}</span></p>
                </div>
            </div>

        if (trip.status >= 23)
            return <div className="status-wrapper">
                <div className="content-register d-shadow">
                    <div className="stepbystep">
                        <ul>
                            <li><span className="nu">1</span><span className="value">Duyệt yêu cầu</span></li>
                            <li><span className="nu">2</span><span className="value">Thanh toán cọc</span></li>
                            <li><span className="nu">3</span><span className="value">Khởi hành</span></li>
                            <li><span className="nu">4</span><span className="value">Kết thúc</span></li>
                        </ul>
                    </div>
                </div>
                <div className="booking-status status-trips reject-trip">
                    <p><span className="status red-dot"></span><span>Chuyến đã bị huỷ. {(trip.cancelReason && trip.cancelReason !== "") ? `Lý do: ${trip.cancelReason}` : ""}</span></p>
                </div>
            </div>

        if (trip.status >= 21)
            return <div className="status-wrapper">
                <div className="content-register d-shadow">
                    <div className="stepbystep">
                        <ul>
                            <li><span className="nu">1</span><span className="value">Duyệt yêu cầu</span></li>
                            <li><span className="nu">2</span><span className="value">Thanh toán cọc</span></li>
                            <li><span className="nu">3</span><span className="value">Khởi hành</span></li>
                            <li><span className="nu">4</span><span className="value">Kết thúc</span></li>
                        </ul>
                    </div>
                </div>
                <div className="booking-status status-trips reject-trip">
                    <p><span className="status red-dot"></span><span>Chuyến đã bị huỷ. {(trip.cancelReason && trip.cancelReason !== "") ? `Lý do: ${trip.cancelReason}` : ""}</span></p>
                </div>
            </div>

        if (trip.status >= 20)
            return <div className="status-wrapper">
                <div className="content-register d-shadow">
                    <div className="stepbystep">
                        <ul>
                            <li><span className="nu">1</span><span className="value">Duyệt yêu cầu</span></li>
                            <li><span className="nu">2</span><span className="value">Thanh toán cọc</span></li>
                            <li><span className="nu">3</span><span className="value">Khởi hành</span></li>
                            <li><span className="nu">4</span><span className="value">Kết thúc</span></li>
                        </ul>
                    </div>
                </div>
                <div className="booking-status status-trips reject-trip">
                    <p><span className="status red-dot"></span><span>Chủ xe đã từ chối. {(trip.rejectReason && trip.rejectReason !== "") ? `Lý do: ${trip.rejectReason}` : ""}</span></p>
                </div>
            </div>

        if (trip.status >= 5)
            return <div className="status-wrapper">
                <div className="content-register d-shadow">
                    <div className="stepbystep">
                        <ul>
                            <li className="active"><span className="nu">1</span><span className="value">Duyệt yêu cầu</span></li>
                            <li className="active"><span className="nu">2</span><span className="value">Thanh toán cọc</span></li>
                            <li className="active"><span className="nu">3</span><span className="value">Khởi hành</span></li>
                            <li className="next-step"><span className="nu">4</span><span className="value">Kết thúc</span></li>
                        </ul>
                    </div>
                </div>
                <div className="booking-status status-trips arrow-4">
                    <p><span className="status green-dot"> </span><span>Chuyến thuê của bạn sẽ kết thúc lúc {moment(trip.tripDateTo).format('HH:mm ngày DD/MM/YYYY')}</span></p>
                </div>
            </div>

        if (trip.status >= 4)
            return <div className="status-wrapper">
                <div className="content-register d-shadow">
                    <div className="stepbystep">
                        <ul>
                            <li className="active"><span className="nu">1</span><span className="value">Duyệt yêu cầu</span></li>
                            <li className="active"><span className="nu">2</span><span className="value">Thanh toán cọc</span></li>
                            <li className="next-step"><span className="nu">3</span><span className="value">Khởi hành</span></li>
                            <li><span className="nu">4</span><span className="value">Kết thúc</span></li>
                        </ul>
                    </div>
                </div>
                <div className="booking-status status-trips arrow-3">
                    <p><span className="status green-dot"></span><span>Chuyến thuê của bạn sẽ bắt đầu lúc {moment(trip.tripDateFrom).format('HH:mm ngày DD/MM/YYYY')}</span></p>
                </div>
            </div>

        if (trip.status >= 2)
            return <div className="status-wrapper">
                <div className="content-register d-shadow">
                    <div className="stepbystep">
                        <ul>
                            <li className="active"><span className="nu">1</span><span className="value">Duyệt yêu cầu</span></li>
                            <li className="nex-step"><span className="nu">2</span><span className="value">Thanh toán cọc</span></li>
                            <li><span className="nu">3</span><span className="value">Khởi hành</span></li>
                            <li><span className="nu">4</span><span className="value">Kết thúc</span></li>
                        </ul>
                    </div>
                </div>
                <div className="booking-status status-trips arrow-2">
                    <p><span className="status orange-dot"></span><span>Bạn cần đặt cọc trước thời hạn, nếu không hệ thống sẽ tự động huỷ chuyến.</span></p>
                    {this.state.countdown && this.state.countdown !== "" && <span className="countdown">{this.state.countdown}</span>}
                </div>
            </div>

        if (trip.status >= 1)
            return <div className="status-wrapper">
                <div className="content-register d-shadow">
                    <div className="stepbystep">
                        <ul>
                            <li className="nex-step"><span className="nu">1</span><span className="value">Duyệt yêu cầu</span></li>
                            <li><span className="nu">2</span><span className="value">Thanh toán cọc</span></li>
                            <li><span className="nu">3</span><span className="value">Khởi hành</span></li>
                            <li><span className="nu">4</span><span className="value">Kết thúc</span></li>
                        </ul>
                    </div>
                </div>
                <div className="booking-status status-trips arrow-1">
                    <p><span className="status orange-dot"> </span><span>Đang chờ chủ xe duyệt.</span></p>
                    {this.state.countdown && this.state.countdown !== "" && <span className="countdown">{this.state.countdown}</span>}
                </div>
            </div>

        return null;
    }

    reRenderTripPhoto(){
        this.closeTripPhoto();
        this.showTripPhoto()
    }
	
	handleFrInsFileInputChange(photos) {
			this.setState({
					frInsPhoto: {
							...this.state.frInsPhoto,
							err: commonErr.LOADING
					}
			});
			uploadInsurancePhoto(this.state.trip.id, "fr", photos[0]).then(resp => {
                if(resp.data.error !== commonErr.SUCCESS){
                    this.setState({
                        actErr: resp.data.error,
                        errMsg: resp.data.errorMessage
                    })
                } else {
					var photos = [];
					if (this.state.frInsPhoto.photos) {
							photos = photos.concat(this.state.frInsPhoto.photos);
					}
					if (resp.data.data && resp.data.data.id) {
							photos.push(resp.data.data);
					}
					this.setState({
						frInsPhoto: {
									...this.state.frInsPhoto,
									err: resp.data.error,
									errMsg: resp.data.errorMessage,
									photos: photos
							}
					});

					if (this.state.showTripPhoto) {
                        this.reRenderTripPhoto();
                    }
                }
			});
			
		
	
		}

    handleBaInsFileInputChange(photos) {
			this.setState({
                baInsPhoto: {
                    ...this.state.baInsPhoto,
                    err: commonErr.LOADING
                }
			});
			uploadInsurancePhoto(this.state.trip.id, "ba", photos[0]).then(resp => {
                if(resp.data.error !== commonErr.SUCCESS){
                    this.setState({
                        actErr: resp.data.error,
                        errMsg: resp.data.errorMessage
                    })
                } else {
                    var photos = [];
					if (this.state.baInsPhoto.photos) {
							photos = photos.concat(this.state.baInsPhoto.photos);
					}
					if (resp.data.data && resp.data.data.id) {
							photos.push(resp.data.data);
					}
					this.setState({
						baInsPhoto: {
									...this.state.baInsPhoto,
									err: resp.data.error,
									errMsg: resp.data.errorMessage,
									photos: photos
							}
					});
					if (this.state.showTripPhoto) {
						this.reRenderTripPhoto();
                    }
                }
			});
		}
    handleLeInsFileInputChange(photos) {
			this.setState({
                leInsPhoto: {
                        ...this.state.leInsPhoto,
                        err: commonErr.LOADING
                }
			});
			uploadInsurancePhoto(this.state.trip.id, "le", photos[0]).then(resp => {
                if(resp.data.error !== commonErr.SUCCESS){
                    this.setState({
                        actErr: resp.data.error,
                        errMsg: resp.data.errorMessage
                    })
                } else {
                    var photos = [];
                    if (this.state.leInsPhoto.photos) {
                            photos = photos.concat(this.state.leInsPhoto.photos);
                    }
                    if (resp.data.data && resp.data.data.id) {
                            photos.push(resp.data.data);
                    }
                    this.setState({
                        leInsPhoto: {
                                    ...this.state.leInsPhoto,
                                    err: resp.data.error,
                                    errMsg: resp.data.errorMessage,
                                    photos: photos
                            }
                    });
                    if (this.state.showTripPhoto) {
                        this.reRenderTripPhoto();
                    }
                }
            });
            
		}
    handleRiInsFileInputChange(photos) {
			this.setState({
                riInsPhoto: {
                        ...this.state.riInsPhoto,
                        err: commonErr.LOADING
                }
			});
			uploadInsurancePhoto(this.state.trip.id, "ri", photos[0]).then(resp => {
                var photos = [];
                if(resp.data.error !== commonErr.SUCCESS){
                    this.setState({
                        actErr: resp.data.error,
                        errMsg: resp.data.errorMessage
                    })
                } else {
                    if (this.state.riInsPhoto.photos) {
                        photos = photos.concat(this.state.riInsPhoto.photos);
                    }
                    if (resp.data.data && resp.data.data.id) {
                            photos.push(resp.data.data);
                    }
                    this.setState({
                        riInsPhoto: {
                                    ...this.state.riInsPhoto,
                                    err: resp.data.error,
                                    errMsg: resp.data.errorMessage,
                                    photos: photos
                            }
                    });
                    if (this.state.showTripPhoto) {
                        this.reRenderTripPhoto();
                    }
                }					
			});
		}
	
		removeInsurancePhoto(imageId, photoId) {
			removeInsurancePhoto(this.state.trip.id, imageId, photoId).then(resp => {
				if (resp.data.error == commonErr.SUCCESS) {
					var photos = [];
					if (imageId === "fr") {
							if (this.state.frInsPhoto.photos) {
									photos = photos.concat(this.state.frInsPhoto.photos);
                            }
							photos = photos.filter(p => {
									return p.id !== photoId
                            });
							this.setState({
									frInsPhoto: {
											...this.state.frInsPhoto,
											photos: photos
									}
                            });
						    return;
					} else if (imageId === "ba") {
							if (this.state.baInsPhoto.photos) {
									photos = photos.concat(this.state.baInsPhoto.photos);
							}
							photos = photos.filter(p => {
									return p.id !== photoId
							});
							this.setState({
									baInsPhoto: {
											...this.state.baInsPhoto,
											photos: photos
									}
                            });
                            return;
					} else if (imageId === "le") {
						if (this.state.leInsPhoto.photos) {
								photos = photos.concat(this.state.leInsPhoto.photos);
						}
						photos = photos.filter(p => {
								return p.id !== photoId
						});
						this.setState({
								leInsPhoto: {
										...this.state.leInsPhoto,
										photos: photos
								}
                        });	
                        return;			
					} else if (imageId === "ri") {
						if (this.state.riInsPhoto.photos) {
							photos = photos.concat(this.state.riInsPhoto.photos);
						}
						photos = photos.filter(p => {
								return p.id !== photoId
						});
						this.setState({
								riInsPhoto: {
										...this.state.riInsPhoto,
										photos: photos
								}
                        });
                        return;				
					}
					} else {
                        this.setState({
                            actErr: resp.data.err,
                            errMsg: resp.data.errorMessage
                        })
                    }
				});
		}
		
		activeInsurance() {
			activeInsurance(this.state.trip.id).then(resp => {
                this.setState({
                    actErr: resp.data.error,
                    errMsg: resp.data.errorMessage
                })				
			})
		}


        
	showMessageUpload() {
		this.setState({
			showMessageUpload: true
		})
	}
	closeMessageUpload() {
		this.setState({
			showMessageUpload: false
		})
	}

	hideMessageBox() {
		this.setState({
            errMsg: ""
        })
        this.getTrip(this.props.match.params.tripId)
    }

			
    render() {
        var content;
        if (this.state.err === commonErr.INNIT
            || this.state.err === commonErr.LOADING) {
            content = <LoadingPage />
        } else if (this.props.session.profile.err !== commonErr.INNIT
            && this.props.session.profile.err !== commonErr.LOADING
            && (!this.props.session.profile.info || !this.props.session.profile.info.uid)) {
            content = <Redirect to="/" />
        } else if (this.state.err >= commonErr.SUCCESS) {
            const car = this.state.car;
            const trip = this.state.trip;
            var loginId = this.props.session.profile.info.uid;
            var isOwner = (loginId === trip.ownerId);
            var profile;
            for (var i = 0; i < this.state.profiles.length; ++i) {
                if (isOwner) {
                    if (this.state.profiles[i].uid === trip.travelerId) {
                        profile = this.state.profiles[i];
                    }
                } else {
                    if (this.state.profiles[i].uid === trip.ownerId) {
                        profile = this.state.profiles[i];
                    }
                }
            }
            if (car && trip && profile) {
                var header;
                if (isOwner) {
                    header = this.buildOwnerTripStatusBar(trip);
                } else {
                    header = this.buildTravelerTripStatusBar(trip);
                }

                var footer;
                if (isOwner) {
                    if (trip.status <= 1) {
                        footer = <div className="wrap-btn dt-trip-btn has-2btn">
                            <div className="wr-btn" onClick={this.showRejectForm.bind(this)}><a className="btn btn-secondary btn--m">Từ chối yêu cầu</a></div>
                            <div className="wr-btn" onClick={this.showApproveForm.bind(this)}><a className="btn btn-primary btn--m" >Đồng ý yêu cầu</a></div>
                            <ApproveTripOwner getTrip={this.getTrip} trip={trip} show={this.state.showApproveForm} hideModal={this.closeApproveForm.bind(this)} />
                            <RejectTripOwner getTrip={this.getTrip} trip={trip} show={this.state.showRejectForm} hideModal={this.closeRejectForm.bind(this)} />
                        </div>
                    } else if (trip.status < 20) {
                        if (trip.status === 4) {
                            footer = <div className="wrap-btn dt-trip-btn has-2btn">
                                <div className="wr-btn"><a className="btn btn-red btn--m" onClick={this.showCancelOwnerForm.bind(this)}>Huỷ chuyến</a></div>
                                <div className="wr-btn" onClick={this.showPickupForm.bind(this)}><a className="btn btn-primary btn--m" >Xác nhận giao xe</a></div>
                                <PickupTrip getTrip={this.getTrip} trip={trip} show={this.state.showPickupForm} hideModal={this.closePickupForm.bind(this)} />
                                <CancelTripOwner getTrip={this.getTrip} trip={trip} show={this.state.showCancelOwnerForm} hideModal={this.closeCancelOwnerForm.bind(this)} />
                            </div>
                        } else if (trip.status === 5) {
                            footer = <div className="wrap-btn dt-trip-btn">
                                <div className="wr-btn" onClick={this.showReturnForm.bind(this)}>
                                    <a className="btn btn-primary btn--m" >Xác nhận khách trả xe</a>
                                </div>
                                <ReturnTrip getTrip={this.getTrip} trip={trip} step={0} show={this.state.showReturnForm} hideModal={this.closeReturnForm.bind(this)} />
                            </div>
                        } else {
                            footer = <div className="wrap-btn dt-trip-btn">
                                <div className="wr-btn">
                                    <a className="btn btn-red btn--m" onClick={this.showCancelOwnerForm.bind(this)}>Huỷ chuyến</a>
                                </div>
                                <CancelTripOwner getTrip={this.getTrip} trip={trip} show={this.state.showCancelOwnerForm} hideModal={this.closeCancelOwnerForm.bind(this)} />
                            </div>
                        }
                    } else if (trip.status === 25 && trip.needReview === 1) {
                        footer = <div className="wrap-btn dt-trip-btn">
                            <div className="wr-btn" onClick={this.showReturnForm.bind(this)}>
                                <a className="btn btn-primary btn--m" >Đánh giá chuyến</a>
                            </div>
                            <ReturnTrip getTrip={this.getTrip} trip={trip} step={2} show={this.state.showReturnForm} hideModal={this.closeReturnForm.bind(this)} />
                        </div>
                    }
                } else {
                    if (trip.status <= 1) {
                        footer = <div className="wrap-btn dt-trip-btn">
                            <div className="wr-btn"><a className="btn btn-red btn--m" onClick={this.cancelTrip.bind(this)}>Huỷ chuyến</a></div>
                            <CancelTrip getTrip={this.getTrip} trip={trip} show={this.state.showCancelForm} hideModal={this.closeCancelForm.bind(this)} />
                        </div>
                    } else if (trip.status <= 3) {
                        footer = <div className="wrap-btn dt-trip-btn has-2btn">
                            <div className="wr-btn"><a className="btn btn-red btn--m" onClick={this.cancelTrip.bind(this)}>Huỷ chuyến</a></div>
                            {/* <PaymentBtn tripId={trip.id} /> */}
                            <div className="wr-btn">
                                <a className="btn btn-primary btn--m" href={`/payment/${trip.id}?deposit=${trip.priceSummary.deposit}&timeExpired=${trip.timeExpired}`}>Thanh toán</a>
                            </div>
                            <CancelTrip getTrip={this.getTrip} trip={trip} show={this.state.showCancelForm} hideModal={this.closeCancelForm.bind(this)} />
                        </div>
                    } else if (trip.status < 5) {
                        footer = <div className="wrap-btn dt-trip-btn">
                            <div className="wr-btn"><a className="btn btn-red btn--m" onClick={this.cancelTrip.bind(this)}>Huỷ chuyến</a></div>
                            <CancelTrip getTrip={this.getTrip} trip={trip} show={this.state.showCancelForm} hideModal={this.closeCancelForm.bind(this)} />
                        </div>
                    } else if (trip.needReview === 1) {
                        footer = <div className="wrap-btn dt-trip-btn">
                            <div className="wr-btn"><a className="btn btn-primary btn--m" onClick={this.travelerReview.bind(this)}>Đánh giá chuyến</a></div>
                            <ReviewTripTraveler getTrip={this.getTrip} trip={trip} show={this.state.isShowTravelerReviewForm} hideModal={this.closeTravelerReviewForm.bind(this)} />
                        </div>
                    }
                }
							const timeEnabledUpload = convertToDateTimeObj(trip.insUpThr);
							
                var subContent = <section className="body">
                    <div className="module-register">
										<div className="register-container trip-container">
											{trip.status >= 4 && trip.status < 20 && trip.insSupport === 1 && trip.insActiveInfo !== null && <div className="status-wrapper">
											
												<div className="content-trip insurance">
												
													<p className="status-insurance"><i className="ic ic-act-insurance"></i>Gói Bảo hiểm đã được kích hoạt</p>
													
													</div>
								
											</div>}
											{trip.status >= 4 && trip.status < 20 && trip.insSupport === 1 && trip.insActiveInfo === null && <div className="status-wrapper">
												<div className="content-trip insurance deactive">
												<p className="status-insurance"><i className="ic ic-deact-insurance"></i>Gói Bảo hiểm chưa được kích hoạt</p>
												</div>
											</div>}
                            {header}
                            <div className="content-register content-trip">
                                <div className="content-booking">
                                    <div className="info-trip--more info-car--more">
                                        <div className="group-1">
                                            <div className="car-img">
                                                <div className="fix-img"><a href={`/car/${formatTitleInUrl(car.name)}/${car.id}`} target="_blank"><img src={car.photos ? car.photos[0].thumbUrl : ""} alt={`Cho thuê xe tự lái ${car.name}`} /></a></div>
                                            </div>
                                        </div>
                                        <div className="group-2">
                                            <div className="group-info">
                                                <h6 className="name">{car.name}</h6>
                                                <StarRatings
                                                    rating={car.rating.avg}
                                                    starRatedColor="#00a550"
                                                    starDimension="17px"
                                                    starSpacing="1px"
                                                />
                                            </div>
                                        </div>
                                        <div className="group-3">
                                            <div className="profile-mini-v2">
                                                <div className="avatar avatar-new" title="title name">
                                                    <a href={`/profile/${profile.uid}`}><div className="avatar-img" style={{ backgroundImage: `url(${(profile.avatar && profile.avatar.thumbUrl) ? profile.avatar.thumbUrl : avatar_default})` }}></div></a>
                                                </div>
                                                <div className="desc-v2">
                                                    <h2><span className="lstitle">{isOwner ? "Khách thuê" : "Chủ xe"} </span>
                                                        {profile.name}</h2>
                                                    <StarRatings
                                                        rating={profile.owner.rating.avg}
                                                        starRatedColor="#00a550"
                                                        starDimension="17px"
                                                        starSpacing="1px"
                                                    />
                                                </div>
                                            </div>
                                        </div>
                                        <div className="group-4">
                                            <div className="group-info">
                                                <h6>Thời gian thuê xe</h6>
                                                <div className="form-default grid-form">
                                                    <div className="line-form has-timer">
                                                        <p>Bắt đầu: {moment(trip.tripDateFrom).format("DD/MM/YYYY HH:mm")}</p>
                                                    </div>
                                                    <div className="line-form has-timer">
                                                        <p>Kết thúc: {moment(trip.tripDateTo).format("DD/MM/YYYY HH:mm")}</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="group-info">
                                                <h6>Địa điểm giao nhận xe</h6>
                                                <div className="address-car">
                                                    {trip.dAddr && trip.dAddr !== "" && <label className="value">{trip.dAddr}</label>}
                                                    {(!trip.dAddr || trip.dAddr === "") && <div className="line-form">
                                                        <span className="value">{car.locationAddr} (địa chỉ của xe).</span>
                                                    </div>}
                                                </div>
                                            </div>
                                        </div>
                                        <div className="group-5">
                                            <div className="group-info mb-4">
                                                <h6>Số điện thoại</h6>
                                                {trip.status >= 4 && isOwner && <span>{trip.travelerPhoneNumber || '***********'}</span>}
                                                {trip.status >= 4 && !isOwner && <span>{trip.ownerPhoneNumber || '***********'}</span>}
                                                {trip.status < 4 && <p>***********</p>}
                                                {trip.status < 4 && <span>Số điện thoại liên hệ sẽ được hiển thị sau khi đặt cọc.</span>}
                                            </div>
                                            <div className="group-info">
                                                <h6>Lời nhắn</h6>
                                                {trip.messageFromTraveller && trip.messageFromTraveller !== "" ? <span>{trip.messageFromTraveller}</span> : <span>Không có lời nhắn</span>}
																						</div>
																					</div>
																					<div className="group-6">
                                            <div className="group-info">
                                                <h6>Vị trí xe </h6>
                                                <span className="value">{car.locationAddr}{(this.props.status === 4 || this.props.status === 5) && <span>(địa chỉ cụ thể sẽ được hiển thị sau khi đặt cọc)</span>}.</span>
                                                <div className="space m"></div>
                                                <CarMapLocation location={car.location} status={trip.status} />
                                            </div>
                                            <div className="group-info">
                                                <h6>Giới hạn quãng đường</h6>
                                                {car.limitEnable === 1 && <div className="note"><p>Tối đa <strong>{car.limitKM}</strong> km/ngày. Phí <strong>{formatPrice(car.limitPrice)}</strong>/km vượt giới hạn.</p></div>}
                                                {car.limitEnable !== 1 && <div className="note"><p>Không giới hạn quãng đường.</p></div>}
                                            </div>
                                       		 </div>
																					{(trip.status >= 4) && trip.status < 20 && trip.insSupport === 1 && trip.insPhotos && <div className="group-tphoto">
																						<div className="group-info">
																						<h6>Bảo hiểm vật chất xe ô tô</h6>
																						{/* {trip.insActiveInfo !== null && <div><p>Hợp đồng BH số: 
																							<span></span>
																						</p>
																						<p>Nếu xe xảy ra sự cố trên đường, vui lòng gọi 1900 55 88 91, đọc số Hợp đồng BH và làm theo hướng dẫn của tổng đài bảo hiểm MIC.</p></div>} */}
																						<TripPhotoSlider
																							trip={trip}
																							showTripPhoto={this.showTripPhoto}
																							showMessageUpload={this.showMessageUpload}
																							frInsPhoto={this.state.frInsPhoto}
																							baInsPhoto={this.state.baInsPhoto}
																							leInsPhoto={this.state.leInsPhoto}
																							riInsPhoto={this.state.riInsPhoto}
																						/>
																					{trip.insActiveInfo === null && <div className="ins-notice">
																						<p onClick={this.showTripPhoto} className="note text-danger">Cung cấp thêm hình ảnh để kích hoạt gói Bảo hiểm</p>
																					</div>}
																					{trip.insReady === 1 && trip.insActiveInfo === null && <div className="ins-notice">
																						<p className="note text-primary">Gói bảo hiểm chưa được kích hoạt</p>
																						<a onClick={this.activeInsurance.bind(this)} className="btn btn--s btn-default" href="#!"><i className="ic ic-retry-insurance"></i><span>Kích hoạt ngay</span></a></div>}
																					{trip.insActiveInfo !== null && <div className="ins-notice">
																						<p className="note text-primary">Gói Bảo hiểm đã được kích hoạt</p>
																						<a className="btn btn--s btn-primary" href="#!"><i className="ic ic-pdf"></i><span>Xem thông tin Bảo hiểm</span></a>
																					</div>}												
																<MessageBox show={this.state.errMsg !== ""} error={this.state.actErr} message={this.state.errMsg} hideModal={this.hideMessageBox} />
																</div> </div>}
                                            
                                      
																				<div className="group-tphoto ">
                                            <div className="group-info required-paper">
                                                <h6>Giấy tờ thuê xe (bắt buộc)</h6>
																								{car.requiredPapers && car.requiredPapers.length > 0 && <div className="ctn-desc-new">
																										<ul className="required">
																												{car.requiredPapers.map(paper => <li key={paper.id}> <img style={{ width: "20px", height: "20px", marginRight: "10px" }} className="img-ico" src={paper.logo} alt="Mioto - Thuê xe tự lái" /> {paper.name}</li>)}
																										</ul>
																								</div>}
                                            </div>
                                            <div className="group-info mortgages">
                                                <h6>Tài sản thế chấp</h6>
																								{car.mortgages && car.mortgages.length > 0 && <div className="ctn-desc-new">
																								<ul className="required">
																										{car.mortgages.map(mortgage => <li key={mortgage.id}>{mortgage.name}</li>)}
																								</ul>
																								</div>}
                                            </div>
																						{car.notes && car.notes !== "" && <div className="group-info policies">
																							<h6>Điều khoản thuê xe</h6>
																							<div className="ctn-desc-new clause">
																								<p>{car.notes}</p>
																							</div>
																						</div>}
                                           
                                        </div>
                                        <div className="group-7">
                                            <h6>Bảng tính giá</h6>
                                            <div className="form-default">
                                                <div className="line-form has-timer">
                                                    <div style={{ width: "50%" }} className="wrap-input has-dropdown date has-value"><span className="value">Đơn giá thuê</span></div>
                                                    <div style={{ width: "50%" }} className="wrap-input has-dropdown time has-value"><span className="value"><NumberFormat value={trip.priceSummary.priceSpecial} displayType={'text'} thousandSeparator={" "} decimalSeparator={","} suffix={" / ngày"} /></span></div>
                                                    {trip.priceSummary.discountPercent > 0 && <div style={{ width: "50%" }} className="wrap-input has-dropdown date has-value long-term"><span className="value">{trip.priceSummary.discountType === 0 ? <span>Đơn giá thuê {trip.priceSummary.discountPercent > 0 && <span> (giảm {trip.priceSummary.discountPercent}%)</span>}</span> : <span>Thuê{trip.priceSummary.discountType == 1 && <span> tuần</span>}{trip.priceSummary.discountType === 2 && <span> tháng</span>} {trip.priceSummary.discountPercent > 0 && <span> (giảm {trip.priceSummary.discountPercent}%)</span>}</span>}</span></div>}
                                                    {trip.priceSummary.discountPercent > 0 && <div style={{ width: "50%" }} className="wrap-input has-dropdown time has-value long-term"><span className="value line-through"><NumberFormat value={trip.priceSummary.priceOrigin} displayType={'text'} thousandSeparator={" "} decimalSeparator={","} prefix={"Giá gốc "} suffix={" / ngày"} /></span></div>}
                                                    {trip.priceSummary.specialDiscount > 0 && <div style={{ width: "50%" }} className="wrap-input has-dropdown date has-value long-term"><span className="value">Chương trình giảm giá<div className="tooltip"><i className="ic ic-question-mark" />
                                                        {!isOwner && <div className="tooltip-text">Không áp dụng đồng thời với mã khuyến mãi.</div>}
                                                        {isOwner && <div className="tooltip-text">Mioto sẽ hoàn lại cho chủ xe số tiền giảm này</div>}
                                                    </div></span></div>}
																										{trip.priceSummary.specialDiscount > 0 && <div style={{ width: "50%" }} className="wrap-input has-dropdown time has-value long-term"><span className="value line-through">{trip.priceSummary.priceOrigin === trip.priceSummary.price && <span>Giá gốc </span>}<NumberFormat value={trip.priceSummary.price} displayType={'text'} thousandSeparator={" "} decimalSeparator={","} suffix={" / ngày"} /></span></div>}
																										{trip.priceSummary.tripFee > 0 && <div style={{ width: "50%" }} className="wrap-input has-dropdown date has-value"><span className="value">Phí dịch vụ <div className="tooltip"><i className="ic ic-question-mark" /><div className="tooltip-text">Phí dịch vụ nhằm hỗ trợ Mioto duy trì nền tảng ứng dụng và các hoạt động chăm sóc khách hàng một cách tốt nhất.</div></div></span>
																										</div>}
																										{trip.priceSummary.tripFee > 0 && <div style={{ width: "50%" }} className="wrap-input has-dropdown time has-value"><span className="value"><NumberFormat value={trip.priceSummary.tripFee} displayType={'text'} thousandSeparator={" "} decimalSeparator={","} suffix={" / ngày"}/></span>
																										</div>}
																										{trip.priceSummary.insuranceFee > 0 && <div style={{ width: "50%" }} className="wrap-input has-dropdown date has-value"><span className="value">Phí bảo hiểm<div className="tooltip"><i className="ic ic-question-mark" /><div className="tooltip-text">Chuyến đi của bạn được mua gói bảo hiểm vật chất xe ô tô từ nhà bảo hiểm MIC. Trường hơp có sự cố ngoài ý muốn (trong phạm vi bảo hiểm), số tiền thanh toán tối đa là 2,000,000VND/vụ.</div></div></span>
																										</div>}
																										{trip.priceSummary.insuranceFee > 0 && <div style={{ width: "50%" }} className="wrap-input has-dropdown time has-value"><span className="value"><NumberFormat value={trip.priceSummary.insuranceFee} displayType={'text'} thousandSeparator={" "} decimalSeparator={","} suffix={" / ngày"}/></span>
																										</div>}
                                                    <div style={{ width: "50%" }} className="wrap-input has-dropdown date has-value line-gray"><span className="value">Tổng phí thuê xe</span></div>
                                                    <div style={{ width: "50%" }} className="wrap-input has-dropdown time has-value line-gray"><span className="value"><NumberFormat value={trip.priceSummary.totalPerDay} displayType={'text'} thousandSeparator={" "} decimalSeparator={","} /> x <strong>{trip.priceSummary.totalDays} ngày</strong></span></div>
                                                    {trip.priceSummary.deliveryFee > 0 && <div>
                                                        <div style={{ width: "50%" }} className="wrap-input has-dropdown date has-value"><span className="value">Phí giao nhận xe (2 chiều)</span></div>
                                                        <div style={{ width: "50%" }} className="wrap-input has-dropdown time has-value"><span className="value"><NumberFormat value={trip.priceSummary.deliveryFee > 0 ? trip.priceSummary.deliveryFee : 0} displayType={'text'} thousandSeparator={" "} decimalSeparator={","} /> {(trip.priceSummary.deliveryDistance && trip.priceSummary.deliveryDistance) > 0 && <NumberFormat value={trip.priceSummary.deliveryDistance} displayType={'text'} thousandSeparator={" "} decimalSeparator={","} prefix={" ("} suffix={" km)"} decimalScale={1} />}</span></div>
                                                    </div>}
                                                    {trip.priceSummary.promotionDiscount > 0 && <div>
                                                        <div style={{ width: "50%" }} className="wrap-input has-dropdown date has-value"><span className="value">Khuyến mãi mã <strong>{trip.promo}</strong> {isOwner && <div className="tooltip"> <i className="ic ic-question-mark"></i>
                                                            <div className="tooltip-text">Mioto sẽ hoàn lại cho chủ xe số tiền giảm này</div></div>}</span></div>
                                                        <div style={{ width: "50%" }} className="wrap-input has-dropdown time has-value"><span className="value">- <NumberFormat value={trip.priceSummary.promotionDiscount} displayType={'text'} thousandSeparator={" "} decimalSeparator={","} /></span></div>
                                                    </div>}
                                                    <div style={{ width: "50%" }} className="wrap-input has-dropdown date has-value line-gray"><span className="value"><h6>Tổng cộng</h6></span></div>
                                                    <div style={{ width: "50%" }} className="wrap-input has-dropdown time has-value line-gray"><span className="value"><h6><NumberFormat value={trip.priceSummary.priceTotal} displayType={'text'} thousandSeparator={" "} decimalSeparator={","} suffix={"đ"} /></h6></span></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="group-8">
                                            <ul className="lp lp-confirm">
                                                <li className="lp-li">
                                                    <div className="item-lp"><span className="title-lp">TIỀN{trip.status >= 4 && trip.status <= 20 && <span> ĐÃ</span>} CỌC</span><span className="value-lp value-total"><NumberFormat value={trip.priceSummary.deposit} displayType={'text'} thousandSeparator={" "} decimalSeparator={","} suffix={"đ"} /></span></div>
                                                </li>
                                                <li className="lp-li">
                                                    <div className="item-lp"><span className="title-lp">TIỀN TRẢ SAU</span><span className="value-lp"><NumberFormat value={trip.priceSummary.priceTotal - trip.priceSummary.deposit} displayType={'text'} thousandSeparator={" "} decimalSeparator={","} suffix={"đ"} /></span></div>
                                                </li>
                                            </ul>
                                        </div>
                                        {trip.status <= 4 && <div className="group-9">
                                            <h6><a href="/privacy#canceltrip" target="_blank">Chính sách hủy chuyến</a></h6>
                                            {isOwner && <div><table className="cancel-privacy">
																								<thead>
																										<tr>
																												<td>Thời điểm hủy chuyến</td>
																												<td>Phí hủy chuyến</td>
																												<td>Đánh giá hệ thống</td>
																										</tr>
																								</thead>
																								<tbody>
																										<tr>
																												<td>Trong vòng 1 giờ sau khi đặt cọc</td>
																												<td>0% Tiền cọc</td>
																												<td>3*</td>
																										</tr>
																										<tr>
																												<td>&gt; 7 ngày trước khởi hành</td>
																												<td>30% Tiền cọc</td>
																												<td>3*</td>
																										</tr>
																										<tr>
																												<td>&lt;= 7 ngày trước khởi hành</td>
																												<td>100% Tiền cọc</td>
																												<td>2* (hoặc 1* nếu &lt;6 tiếng)</td>
																										</tr>
																								</tbody>
																							</table>
																							<p className="text-danger font-13">*** Không đánh giá hệ thống đối với trường hợp chủ xe hủy chuyến vì lí do từ phía khách thuê</p>
																						</div>}
                                            {!isOwner && <div>
																								<table className="cancel-privacy">
																									<thead>
																											<tr>
																													<td>Thời điểm hủy chuyến</td>
																													<td>Phí hủy chuyến</td>
																													<td>Số tiền cọc hoàn trả</td>
																											</tr>
																									</thead>
																									<tbody>
																											<tr>
																													<td>Trong vòng 1 giờ sau khi đặt cọc</td>
																													<td>0% Tiền cọc</td>
																													<td>100% Tiền cọc</td>
																											</tr>
																											<tr>
																													<td>&gt; 7 ngày trước khởi hành</td>
																													<td>30% Tiền cọc</td>
																													<td>70% Tiền cọc</td>
																											</tr>
																											<tr>
																													<td>&lt;= 7 ngày trước khởi hành</td>
																													<td>100% Tiền cọc</td>
																													<td>0% Tiền cọc</td>
																											</tr>
																									</tbody>
																								</table>
																								<p className="text-danger font-13">*** Tiền cọc sẽ được hoàn trả trong vòng 1 - 3 ngày làm việc</p>
                                            </div>}
                                        </div>}
                                    </div>
                                </div>
                            </div>
                        </div>
                        {footer}
                        <Login show={this.state.showLoginForm} showModal={this.openLoginForm.bind(this)} getLoggedProfile={this.getLoggedProfile} hideModal={this.closeLoginForm} />
												<TripPhoto
													show={this.state.showTripPhoto}
													close={this.closeTripPhoto}
													trip={trip}
													frInsPhoto={this.state.frInsPhoto}
													baInsPhoto={this.state.baInsPhoto}
													leInsPhoto={this.state.leInsPhoto}
													riInsPhoto={this.state.riInsPhoto}
													handleFrInsInputChange={this.handleFrInsFileInputChange}
													handleBaInsInputChange={this.handleBaInsFileInputChange}
													handleLeInsInputChange={this.handleLeInsFileInputChange}
													handleRiInsInputChange={this.handleRiInsFileInputChange}
													removeInsPhoto={this.removeInsurancePhoto}
											
										/>						
											<Modal
                            show={this.state.showMessageUpload}
                            onHide={this.closeMessageUpload.bind(this)}
                            dialogClassName="modal-sm modal-dialog"
                        >
                            <Modal.Header closeButton={true} closeLabel={""}>
                                <Modal.Title>Thông báo</Modal.Title>
                            </Modal.Header>
                            <Modal.Body>
                                <div className="form-default form-s">
                                    <div className="textAlign-center"><i className="ic ic-error" /> Bạn chỉ được phép đăng hình ảnh chuyến trước chuyến đi {timeEnabledUpload.days > 0 ? `${timeEnabledUpload.days} ngày` : `${timeEnabledUpload.hours} tiếng`}.</div>
                                </div>
                            </Modal.Body>
										</Modal>
									
											<Modal
                            show={this.state.showInsuranceForm}
                            onHide={this.closeInsuranceForm.bind(this)}
                            dialogClassName="modal-lg modal-ins-form modal-confirm"
                        >
                            <Modal.Header closeButton={true} closeLabel={""}>
                            </Modal.Header>
                            <Modal.Body>
														<div className="module-register">
																<div className="register-container">
																	<div className="content-register">
																		<div className="ins-form-logo">
																			<div className="left">
																				<p>Đơn vị bảo hiểm</p>
																				<img className="img-fluid" src={mic} />
																			</div>
																			<div className="right">
																				<p>Phân phối bởi</p>
																				<img className="img-fluid" src={global_care} />
																			</div>
																		</div>
																		<div className="ins-form-title">
																			<h3>CHỨNG NHẬN BẢO HIỂM VẬT CHẤT XE Ô TÔ</h3>
																			<p className="cert-no">Số:<span>   5d022343047b0</span></p>
																		</div>
																		<div className="ins-form-body" style={{backgroundImage: 'url(../images/background/bg-ins-form.png)'}}>
																			<div className="ins-info">
																				<h6>THÔNG TIN YÊU CẦU BẢO HIỂM</h6>
																				<div className="form-info">
																					<p>Loại xe:
																						<span>  	TOYOTA CAMRY 2017</span>
																					</p>
																					<p>Năm SX:
																							<span>		2018</span>
																					</p>
																					<p>Biển số xe:
																							<span>		51G-30021</span>
																					</p>
																				</div>
																			</div>
																			<div className="ins-info">
																				<h6>CÁC PHẠM VI BẢO HIỂM CHÍNH:</h6>
																				<p>- Bảo hiểm vật chất xe: đâm va, hỏa hoạn, cháy nổ</p>
																				<p>- Bảo hiểm mất cắp, mất nguyên chiếc.</p>
																				<p>- Miễn phí cứu hộ tối đa 70 km/vụ.</p>
																				<p>- Mức khấu trừ: 2.000.000 VND/vụ.</p>
																			</div>
																			<div className="ins-info">
																				<h6>THỜI HẠN BẢO HIỂM</h6>
																				<p>Từ 00:00 6/12/1019 - Đến 00:00 6/12/2019</p>
																				<p>Chương trình bảo hiểm này là sản phẩm bảo hiểm của MIC được phân phối tại Global Care</p>
																				<p>Xảy ra sự cố, vui lòng gọi <span className="text-primary fontWeight-5">1900 55 88 91</span>, đọc số Hợp đồng BH và làm theo hướng dẫn của tổng đài bảo hiểm MIC!</p>
																			</div>
																			<div className="wrap-btn"><a className="btn btn--m btn-down-ins btn-primary" href="#!"><i className="ic ic-pdf" />Xem chi tiết</a></div>
																		</div>
																	</div>
																</div>
															</div>
                            </Modal.Body>
										</Modal>
									
									</div>
                </section>
                content = <div className="mioto-layout">
                    <Header />
                    {subContent}
                    <Footer />
                </div>
            } else {
                content = <Redirect to="/notfound" />
            }
        } else {
            content = <Redirect to="/notfound" />
        }

        return (content);
    }
}

function mapSession(state) {
    return {
        session: state.session
    }
}

TripDetail = connect(mapSession)(TripDetail);

export default TripDetail;