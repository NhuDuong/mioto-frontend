import React from "react"
import { Modal } from "react-bootstrap"

import { commonErr } from "../common/errors"
import { approveTripByOwner } from "../../model/car"

export default class ApproveTripOwner extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            err: commonErr.INNIT,
            step: 0, //0 confirm 1: approve trip
            errMsg: "",
            cancelReasons: null,
            ip_cancel_reason: "",
            ip_comment: ""
        }
    }

    handleInputChange = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        });
    }

    goToConfirm() {
        this.setState({
            step: 0,
            err: commonErr.INNIT
        });
    }

    finishApprove() {
        this.props.hideModal();
        this.setState({
            step: 0,
            err: commonErr.INNIT
        });
        this.getTrip();

    }

    getTrip() {
        this.props.getTrip(this.props.trip.id);
    }

    approveTrip() {
        approveTripByOwner(this.props.trip.id).then(resp => {
            if (resp.data.error >= commonErr.SUCCESS) {
                this.setState({
                    step: 1,
                    err: resp.data.error,
                    errMsg: resp.data.errorMessage
                });
            } else {
                this.setState({
                    step: 1,
                    err: resp.data.error,
                    errMsg: resp.data.errorMessage
                });
            }
        });
    }

    render() {
        var content;
        if (this.state.step === 1) {
            if (this.state.err >= commonErr.SUCCESS) {
                content = <div className="form-default form-s">
                    <div className="line-form">
                        <div className="textAlign-center"><i className="ic ic-verify"></i> Bạn đã đồng ý cho thuê xe.</div>
                    </div>
                    <div className="clear"></div>
                    <div className="space m"></div>
                    <button className="btn btn-primary btn--m" type="button" onClick={this.finishApprove.bind(this)}>Hoàn tất</button>
                </div>
            } else {
                content = <div className="form-default form-s">
                    <div className="line-form">
                        <div className="textAlign-center"><i className="ic ic-error"></i> {this.state.errMsg}</div>
                    </div>
                    <div className="clear"></div>
                    <div className="space m"></div>
                    <p className="textAlign-center has-more-btn">
                        <button className="btn btn-primary btn--m" type="button" onClick={this.goToConfirm.bind(this)}>Thử lại</button>
                    </p>
                </div>
            }
        } else {
            content = <div className="form-default form-s">
                <div className="line-form textAlignCenter">
                    <div className="textAlign-center">Bạn chấp nhận yêu cầu thuê xe này?</div>
                    <div className="space m"></div>
                </div>
                <div className="clear"></div>
                <button className="btn btn-primary btn--m" type="button" onClick={this.approveTrip.bind(this)}>Đồng ý</button>
            </div>
        }

        return <Modal
            show={this.props.show}
            onHide={this.finishApprove.bind(this)}
            dialogClassName="modal-sm modal-dialog"
        >
            <Modal.Header closeButton={true} closeLabel={""}>
                <Modal.Title>Duyệt yêu cầu đặt xe</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                {content}
            </Modal.Body>
        </Modal>
    }
}