import React from "react"
import StarRatings from "react-star-ratings"
import { Link } from "react-router-dom"
import { connect } from "react-redux"
import { geocodeByAddress, getLatLng } from 'react-places-autocomplete'
import cookie from 'react-cookies'

import { getLoggedProfile } from "../../actions/sessionAct"
import Login from "../login/login"
import Header from "../common/header"
import Footer from "../common/footer"
import { commonErr } from "../common/errors"
import { formatPrice, CarTransmission, CarFuel } from "../common/common"
import { LoadingPage } from "../common/loading"
import FavoriteButton from "../common/favoritebutton"
import { getCarDetail } from "../../model/car"
import ImageLightBox from "../common/imagelightbox"
import { MessagePage } from "../common/messagebox"
import RelatedCars from "./relatedcars"
import BookingSideBar from "./bookingsidebar"
import CarReport from "./carreport"
import CarReviews from "./carreviews"

import cover_default from "../../static/images/img_landscape_2.jpg"
import avatar_default from "../../static/images/avatar_default.png"
import mic from "../../static/images/logomic.png"

class CarMapLocation extends React.Component {
    componentDidMount() {
        if (this.props.location) {
            // const map = this.map = new window.google.maps.Map(this.refs.map, {
            //     center: {
            //         lat: this.props.location.lat,
            //         lng: this.props.location.lon
            //     },
            //     zoom: 14,
            //     mapTypeControl: false,
            //     scrollwheel: false,
            //     clickableIcons: false,
            //     streetViewControl: false,
            //     zoomControl: true,
            //     zoomControlOptions: {
            //         position: window.google.maps.ControlPosition.RIGHT_BOTTOM
            //     }
            // })

            // this.marker = new window.google.maps.Marker({
            //     map: map,
            //     icon: {
            //         path: window.google.maps.SymbolPath.CIRCLE,
            //         scale: map.getZoom() * 3,
            //         strokeColor: '#141414',
            //         strokeOpacity: 0.8,
            //         strokeWeight: 1,
            //         fillColor: '#666',
            //         fillOpacity: 0.35
            //     },
            //     position: new window.google.maps.LatLng(this.props.location.lat, this.props.location.lon)
            // });

            // map.addListener('zoom_changed', () => {
            //     this.marker.setMap(null);

            //     var scale = map.getZoom() * 3;
            //     var bounds = map.getBounds();
            //     var center = map.getCenter();
            //     if (bounds && center) {
            //         var ne = bounds.getNorthEast();
            //         var radius = window.google.maps.geometry.spherical.computeDistanceBetween(center, ne);
            //         scale = 100000 / radius;
            //     }

            //     this.marker = new window.google.maps.Marker({
            //         map: map,
            //         icon: {
            //             path: window.google.maps.SymbolPath.CIRCLE,
            //             scale: scale,
            //             strokeColor: '#141414',
            //             strokeOpacity: 0.8,
            //             strokeWeight: 1,
            //             fillColor: '#666',
            //             fillOpacity: 0.35
            //         },
            //         position: new window.google.maps.LatLng(this.props.location.lat, this.props.location.lon)
            //     });
            // });
            
            const lat = this.props.location.lat;
            const lon = this.props.location.lon;

            function addCircleToMap(map){
                map.addObject(new window.H.map.Circle(
                  // The central point of the circle
                  {lat:lat, lng:lon},
                  // The radius of the circle in meters
                  400,
                  {
                    style: {
                      strokeColor: 'gray', // Color of the perimeter
                      lineWidth: 2,
                      fillColor: 'rgba(0, 0, 0, 0.3)',  // Color of the circle
                      opacity: 0.3
                    }
                  }
                ));
            }

              /**
               * Boilerplate map initialization code starts below:
               */
              
              //Step 1: initialize communication with the platform
            var platform = new window.H.service.Platform({
                app_id: 'Oxoz6Kc6mhPKq4qP3wsM',
                app_code: '4JCjxp1mfXEXuD5E02yP2Q',
                useHTTPS: true,
                useCIT: true
            });
            var pixelRatio = window.devicePixelRatio || 1;
            var defaultLayers = platform.createDefaultLayers({
                tileSize: pixelRatio === 1 ? 256 : 512,
                ppi: pixelRatio === 1 ? undefined : 320
            });
            
            //Step 2: initialize a map - this map is centered over New Delhi
            var map = new window.H.Map(document.getElementById('map'),
                defaultLayers.normal.map,{
                center: {lat:lat, lng:lon},
                zoom: 14,
                pixelRatio: pixelRatio
            });
            
            //Step 3: make the map interactive
            // MapEvents enables the event system
            // Behavior implements default interactions for pan/zoom (also on mobile touch environments)
            var behavior = new window.H.mapevents.Behavior(new window.H.mapevents.MapEvents(map));
            behavior.disable(window.H.mapevents.Behavior.WHEELZOOM);

            // Create the default UI components
            var ui = window.H.ui.UI.createDefault(map, defaultLayers);
            
            // Now use the map as required...
            addCircleToMap(map);


        }
    }

    render() {
        return <div ref="map" id="map" style={{width: '100%', height: '300px', background: 'grey' }}/>          // add className="fix-map" for google maps
    }
}

class CarDetailInfo extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            err: commonErr.INNIT,
            errMsg: "",
            price: props.detail.info.price,
            priceOrigin: props.detail.info.priceOrigin
        }

        this.showLoginForm = this.showLoginForm.bind(this);
        this.closeLoginForm = this.closeLoginForm.bind(this);
        this.showReportForm = this.showReportForm.bind(this);
        this.closeReportForm = this.closeReportForm.bind(this);
        this.getLoggedProfile = this.getLoggedProfile.bind(this);
    }

    isLogged() {
        const { profile } = this.props.session;
        return profile
            && profile.info
            && profile.info.uid;
    }

    showLoginForm() {
        this.setState({
            showLoginForm: true
        })
    }

    openLoginForm() {
        this.setState({
            showLoginForm: true
        });
    }

    closeLoginForm() {
        this.setState({
            showLoginForm: false
        })
    }

    showReportForm() {
        this.setState({
            showReportForm: true
        })
    }

    closeReportForm() {
        this.setState({
            showReportForm: false
        })
    }

    getLoggedProfile() {
        this.props.dispatch(getLoggedProfile());
    }

    reportCarBtnClick() {
        if (!this.isLogged()) {
            this.showLoginForm();
        } else {
            this.showReportForm();
        }
    }

    updatePrice(car) {
        this.props.updatePrice(car)
    }

    render() {
        const props = this.props;
        const detail = this.props.detail;
        const info = this.props.detail.info;
        const profiles = this.props.detail.profiles;
        const priceOrigin = this.state.priceOrigin;
        const price = this.state.price;

        var ownerProfile;
        if (profiles && profiles.length > 0) {
            for (var i = 0; i < profiles.length; ++i) {
                if (profiles[i].uid === props.detail.info.ownerId) {
                    ownerProfile = profiles[i];
                    break;
                }
            }
        }

        const loggedProfile = this.props.session.profile.info;
        const relatedCars = this.props.detail.relatedCars;

        return <div className="module-detail dt__wrapper">
            <div className="detail-container">
                <div className="content-detail">
                    <div className="info-car">
                        <div className="group-name">
                            <h1 className="title-car">{info.name}</h1>
                            <p className="price">
                                {priceOrigin !== price && <span className="real">{formatPrice(priceOrigin)}</span>}
                                <span className="special">{formatPrice(price)}</span> <span> /ngày</span>
                            </p>
                        </div>
                        <div className="group-line">
                            <StarRatings
                                rating={(info && info.rating) ? info.rating.avg : 0}
                                starRatedColor="#00a550"
                                starDimension="17px"
                                starSpacing="1px"
                            />
                            <div className="bar-line"> </div>
                            <p>{info.totalTrips > 0 ? <span className="value">{info.totalTrips} chuyến</span> : `chưa có chuyến nào`}</p>
                        </div>
												<div className="group-label">
													{info.deliveryEnable === 1 && <span>Giao xe tận nơi</span>}
													{info.deliveryEnable === 1 && info.deliveryPrice === 0 && <span>Miễn phí giao xe</span>}
												</div>
										</div>
                </div>
                <BookingSideBar detail={detail} owner={ownerProfile} showLoginForm={this.showLoginForm} loggedProfile={loggedProfile} updatePrice={this.updatePrice.bind(this)} />
                <div className="content-detail">
                    <div className="info-car--desc">
                        <div className="group">
                            <span className="lstitle-new">ĐẶC ĐIỂM</span>
                            <div className="ctn-desc-new">
                                <ul className="features">
                                    <li><i className="ic ic-chair" /> Số ghế: {info.seat}</li>
                                    <li><i className="ic ic-trans" /> Truyền động: {CarTransmission[info.optionsTransmission]}</li>
                                    <li><i className="ic ic-diesel" /> Nhiên liệu: {CarFuel[info.optionsFuel]}</li>
                                    {(info.optionsFuelConsumption > 0) && <li><i className="ic ic-fuel-consumption" /> Mức tiêu thụ nhiêu liệu: {info.optionsFuelConsumption} lít/100km</li>}
                                </ul>
                            </div>
                        </div>
                        {info.desc && info.desc !== "" && <div className="group">
                            <span className="lstitle-new">MÔ TẢ</span>
                            <div className="ctn-desc-new">
                                <pre>{info.desc}</pre>
                            </div>
                        </div>}
                        {info.features && info.features.length > 0 && <div className="group">
                            <span className="lstitle-new">TÍNH NĂNG</span>
                            <div className="ctn-desc-new">
                                <ul className="accessories">
                                    {info.features.map(function (feature, i) {
                                        return <li key={i}> <img style={{ width: "20px", height: "20px", marginRight: "10px" }} className="img-ico" src={feature.logo} alt="Mioto - Thuê xe tự lái" /> {feature.name}</li>
                                    })}
                                </ul>
                            </div>
                        </div>}
                       
                        <div className="group">
                            <span className="lstitle-new">GIẤY TỜ THUÊ XE (BẢN GỐC)</span>
                            {info.requiredPapers && info.requiredPapers.length > 0 && <div className="ctn-desc-new">
                                <ul className="required">
                                    {info.requiredPapers.map(paper => <li key={paper.id}> <img style={{ width: "20px", height: "20px", marginRight: "10px" }} className="img-ico" src={paper.logo} alt="Mioto - Thuê xe tự lái" /> {paper.name}</li>)}
                                </ul>
                            </div>}
                            {(!info.requiredPapers || info.requiredPapers.length === 0) && <div className="ctn-desc-new"><p>Không yêu cầu giấy tờ</p></div>}
                        </div>
                        {/* {info.requiredPapersOther && info.requiredPapersOther !== "" && <div className="group">
                            <span className="lstitle-new">CÁC GIẤY TỜ KHÁC</span>
                            <div className="ctn-desc-new">
                                <pre>{info.requiredPapersOther}</pre>
                            </div>
												</div>} */}
												{info.mortgages && info.mortgages.length > 0 && <div className="group">
                            <span className="lstitle-new">TÀI SẢN THẾ CHẤP</span>
                            <div className="ctn-desc-new">
															<ul className="required">
                                    {info.mortgages.map(mortgage => <li key={mortgage.id}>{mortgage.name}</li>)}
                                </ul>
                            </div>
												</div>}
												{info.notes && info.notes !== "" && <div className="group">
                            <span className="lstitle-new">ĐIỀU KHOẢN</span>
                            <div className="ctn-desc-new clause">
                                <pre>{info.notes}</pre>
                            </div>
                        </div>}
                        <div className="group">
                            <span className="lstitle-new">CHỦ XE</span>
                            <div className="profile-mini">
                                <div className="avatar avatar-new" title="title name">
                                    <Link to={`/profile/${ownerProfile.uid}`}>
                                        <div className="avatar-img" style={{ backgroundImage: `url(${(ownerProfile.avatar && ownerProfile.avatar.thumbUrl) ? ownerProfile.avatar.thumbUrl : avatar_default})` }}></div>
                                    </Link>
                                </div>
                                <div className="desc">
                                    <Link to={`/profile/${ownerProfile.uid}`}>
                                        <h2>{ownerProfile.name}</h2>
                                        <StarRatings
                                            rating={ownerProfile.owner.rating.avg}
                                            starRatedColor="#00a550"
                                            starDimension="17px"
                                            starSpacing="1px"
                                        />
                                    </Link>
                                </div>
                                <div className="owner-response">
                                    <div className="response-desc">
                                        <p>Tỉ lệ phản hồi</p><span className="rate">{ownerProfile.owner.responseRate}</span>
                                    </div>
                                    <div className="response-desc">
                                        <p>Thời gian phản hồi</p><span className="rate">{ownerProfile.owner.responseTime}</span>
                                    </div>
                                    <div className="response-desc">
                                        <p>Tỉ lệ đồng ý</p><span className="rate">{ownerProfile.owner.acceptRate}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="group">
                            <span className="lstitle-new">VỊ TRÍ</span>
                            <div className="ctn-desc-new">
                                <CarMapLocation location={info.location} />
                                <div className="space m" />
                                <span><i className="ic ic-location-f" /> {info.locationAddr} </span>
                                <div className="space m" />
                            </div>
                        </div>
                        <div className="wr-wrap-btn">
                            <div className="wrap-btn">
                                <a className="btn btn-default btn--m" onClick={this.reportCarBtnClick.bind(this)}>Báo xấu</a>
                                {this.isLogged() && <CarReport show={this.state.showReportForm} hideModal={this.closeReportForm.bind(this)} car={info} />}
                            </div>
                            <div className="wrap-btn">
                                <FavoriteButton car={info} showLoginForm={this.showLoginForm} isLogged={this.isLogged()} />
                            </div>
                        </div>
                    </div>
                    <CarReviews detail={detail} />
                </div>
            </div>
            <div className="clear" />
            {relatedCars && <RelatedCars cars={relatedCars} />}
            <Login show={this.state.showLoginForm} showModal={this.openLoginForm.bind(this)} hideModal={this.closeLoginForm} getLoggedProfile={this.getLoggedProfile} />
        </div>
    }
}

class CarCoverPhoto extends React.Component {
    constructor() {
        super();
        this.state = {
            isShowLightBox: false,
            isShowQrLightBox: false,
            activeIndex: 1
        }

        this.showLightBox = this.showLightBox.bind(this);
        this.hideLightBox = this.hideLightBox.bind(this);
        this.showQrLightBox = this.showQrLightBox.bind(this);
        this.hideQrLightBox = this.hideQrLightBox.bind(this);
    }

    componentDidMount() {
        this.swiperCover = new window.Swiper(this.refs.swiperCover, {
            slidesPerView: 3,
            spaceBetween: 0,
            threshold: 15,
            speed: 400,
            loop: true,
            pagination: {
                el: '.swiper-pagination',
            },
            navigation: {
                nextEl: '.swiper-button-next-car-cover',
                prevEl: '.swiper-button-prev-car-cover',
            },
            slidesOffsetBefore: 250,
            slidesOffsetAfter: 0,
            preventClicks: false,
            preventClicksPropagation: false,
            paginationClickable: true,
            watchSlidesVisibility: true,
            breakpoints: {
                992: {
                    slidesPerView: 2.4,
                    slidesOffsetBefore: 250
                },
                768: {
                    slidesPerView: 2,
                    slidesOffsetBefore: 180,
                },
                480: {
                    slidesPerView: 1,
                    slidesOffsetBefore: 0
                }
            }
        })
    }

    coverNext() {
        var activeIndex = this.state.activeIndex;
        const photos = this.props.detail.info.photos;
        if (activeIndex === photos.length) {
            activeIndex = 1;
        } else {
            activeIndex = activeIndex + 1;
        }
        this.setState({
            activeIndex: activeIndex
        })
    }

    coverPrev() {
        const photos = this.props.detail.info.photos;
        var activeIndex = this.state.activeIndex;
        if (activeIndex === 1) {
            activeIndex = photos.length;
        } else {
            activeIndex = activeIndex - 1;
        }
        this.setState({
            activeIndex: activeIndex
        })
    }

    showLightBox(i) {
        this.setState({
            isShowLightBox: true,
            photoIndex: i
        })
    }

    hideLightBox() {
        this.setState({
            isShowLightBox: false
        })
    }

    showQrLightBox(i) {
        this.setState({
            isShowQrLightBox: true
        })
    }

    hideQrLightBox() {
        this.setState({
            isShowQrLightBox: false
        })
    }

    slideNext() {
        this.swiperCover.appendSlide('<a class="swiper-slide"><div class="img-cover fit-img"><img src="https://n1-pstg-dev.mioto.vn/cho_thue_xe_tu_lai_tphcm/mitsubishi_triton_2017/p/g/2018/02/15/18/1018.jpg" alt="Cho thuê xe tự lái" /></div></a>');
        this.swiperCover.update();
    }

    render() {
        var covers = [];
        const info = this.props.detail.info;
        if (info.photos) {
            if (info.photos.length < 3) {
                covers = covers.concat(info.photos);
                covers = covers.concat(info.photos);
                covers = covers.concat(info.photos);
            } else {
                covers = covers.concat(info.photos);
            }
        } else {
            covers.push({
                id: 0,
                thumbUrl: cover_default,
                fullUrl: cover_default
            })
        }

        return <div className="cover-car">
					{/* {info.photosVerified === 1 && <div className="status-verify left"><i className="ic ic-verify-stroke"></i> Ảnh đã xác thực</div>} */}
						{info.insSupport === 1 && <div className="status-verify status-ins">
							<span>Bảo hiểm bởi</span>
							<img className="img-fluid logo-mic" src={mic} />
						</div>}
            <div ref="swiperCover" className="swiper-container swiper-car">
                <div className="swiper-wrapper" onClick={() => this.showLightBox(0)}>
                    {covers.map(function (cover, i) {
                        return <a key={i} className="swiper-slide">
                            <div className="img-cover fit-img"><img src={cover.fullUrl} alt={`Cho thuê xe tự lái ${info.name}`} /></div>
                        </a>
                    })}
                </div>
                <div className="swiper-pagination"></div>
                <div className="swiper-button-next swiper-button-next-car-cover"><i className="i-arr"></i></div>
                <div className="swiper-button-prev swiper-button-prev-car-cover"><i className="i-arr"></i></div>
            </div>
            <span className="label-pos">
                {info.totalDiscountPercent > 0 && <span className="discount">Giảm {info.totalDiscountPercent}%</span>}
                {info.instant > 0 && <span className="rent"><i className="ic ic-sm-thunderbolt-wh" />Đặt xe nhanh</span>}
                {info.pp === 1 && <span className="free"><i className="ic ic-passport" />Chấp nhận passport</span>}
            </span>
            <div className="qr-car" onClick={this.showQrLightBox}><i className="ic ic-qr-code"></i></div>
            <ImageLightBox isOpen={this.state.isShowLightBox} photoIndex={this.state.photoIndex} images={covers} hideLightBox={this.hideLightBox} />
            <ImageLightBox isOpen={this.state.isShowQrLightBox} images={[{ id: 0, thumbUrl: info.qr, fullUrl: info.qr }]} hideLightBox={this.hideQrLightBox} />
        </div>
    }
}

class CarDetail extends React.Component {
    constructor() {
        super();
        this.state = {
            err: commonErr.LOADING,
            errMsg: ""
        }

        this.getCarDetail = this.getCarDetail.bind(this);
    }

    componentDidMount() {
        const filter = this.props.carFinding.filter;
        const carId = this.props.match.params.carId;

        if (filter.address !== "") {
            var lat = filter.lat;
            var lng = filter.lng;
            var st = filter.startDate.valueOf();
            var et = filter.endDate.valueOf();
            if (lat && lng) {
                this.getCarDetail(carId, lat, lng, st, et);
            } else {
                geocodeByAddress(filter.address)
                    .then(results => getLatLng(results[0]))
                    .then(latLng => {
                        lat = latLng.lat;
                        lng = latLng.lng;
                        this.getCarDetail(carId, lat, lng, st, et);
                    })
            }
        } else {
            this.getCarDetail(carId);
        }

        //GA
        const utmSrc = cookie.load("_utm_src") || "web_directly";
        const utmExt = cookie.load("_utm_ext") || "no_label";
        window.ga('send', 'event', utmSrc, "VIEW_CAR_DETAIL", utmExt);
    }

    componentWillReceiveProps(props) {
        const filter = props.carFinding.filter;
        const carId = props.match.params.carId;

        if (carId !== this.props.match.params.carId
            || filter.address !== this.props.carFinding.filter.address
            || filter.lat !== this.props.carFinding.filter.lat
            || filter.lng !== this.props.carFinding.filter.lng) {
            if (filter.address !== "") {
                var lat = filter.lat;
                var lng = filter.lng;
                var st = filter.startDate.valueOf();
                var et = filter.endDate.valueOf();
                if (lat && lng) {
                    this.getCarDetail(carId, lat, lng, st, et);
                } else {
                    geocodeByAddress(filter.address)
                        .then(results => getLatLng(results[0]))
                        .then(latLng => {
                            lat = latLng.lat;
                            lng = latLng.lng;
                            this.getCarDetail(carId, lat, lng, st, et);
                        });
                }
            } else {
                this.getCarDetail(carId);
            }

            //GA
            const utmSrc = cookie.load("_utm_src") || "web_directly";
            const utmExt = cookie.load("_utm_ext") || "no_label";
            window.ga('send', 'event', utmSrc, "VIEW_CAR_DETAIL", utmExt);
        }
    }

    updatePrice(data) {
        this.setState({
            carDetail : {
                info: data.car,
                profiles: data.profiles,
                relatedCars: data.cars,
                reviews: data.reviews,
                moreReview: data.moreReview
            }
        });
    }

    getCarDetail(carId, lat, lng, st, et) {
        this.setState({
            err: commonErr.LOADING
        })
        getCarDetail(carId, {
            lat: lat,
            lng: lng,
            st: st,
            et: et,
            vw: "cd"
        }).then(resp => {
            if (resp.data.error >= commonErr.SUCCESS) {
                this.setState({
                    err: resp.data.error,
                    errMsg: resp.data.errorMessage,
                    carDetail: {
                        info: resp.data.data.car,
                        profiles: resp.data.data.profiles,
                        relatedCars: resp.data.data.cars,
                        reviews: resp.data.data.reviews,
                        moreReview: resp.data.data.moreReview
                    }
                })
            } else {
                this.setState({
                    err: resp.data.error,
                    errMsg: resp.data.errorMessage
                })
            }
        })
    }

    render() {
        var content;
        if (this.state.err === commonErr.LOADING) {
            content = <section className="body"><LoadingPage /></section>
        } else if (this.state.err >= commonErr.SUCCESS) {
            content = <section className="body">
                <CarCoverPhoto detail={this.state.carDetail} />
                <CarDetailInfo detail={this.state.carDetail} updatePrice={this.updatePrice.bind(this)}/>
            </section >
        } else {
            content = <MessagePage message={"Không tìm thấy thông tin."} />
        }

        return <div className="mioto-layout">
            <Header />
            {content}
            <Footer />
        </div>
    }
}

function mapFindingState(state) {
    return {
        carFinding: state.carFinding
    }
}

function mapSessionState(state) {
    return {
        session: state.session
    }
}

CarDetailInfo = connect(mapSessionState)(CarDetailInfo);
CarDetail = connect(mapFindingState)(CarDetail);

export default CarDetail;