import React from "react"
import StarRatings from "react-star-ratings"

import { formatPrice, formatTitleInUrl } from "../common/common"

import carPhotoDefault from "../../static/images/upload/car_1.png"

function CarItem(props) {
    var car = props.featureCar;
    if (props.isNewTab) {
        return <a href={`/car/${formatTitleInUrl(car.name)}/${car.id}`} onClick={(e) => { e.preventDefault(); return false; }}>
            <div className="item-car" onClick={() => props.tooglePopup(car, true)}>
                <span className="img-car">
                    <div className="fix-img">
                        <img src={car.photos ? car.photos[0].thumbUrl : carPhotoDefault} alt={`Cho thuê xe tự lái ${car.name}`} />
                    </div>
                    <span className="label-pos">
                        {car.totalDiscountPercent > 0 && <span className="discount">Giảm {car.totalDiscountPercent}%</span>}
												{car.instant > 0 && <span className="rent"><i className="ic ic-sm-thunderbolt-wh" /> Đặt xe nhanh</span>}
												{car.pp === 1 && <span className="free"><i className="ic ic-passport" />Chấp nhận passport</span>}
                        {/* {car.deliveryEnable === 1 && car.deliveryPrice === 0 && <span className="free">Miễn phí giao nhận xe</span>} */}
                    </span>
                </span>
                <div className="desc-car">
                    <div className="group">
                        <span className="star">
                            <StarRatings
                                rating={car.rating.avg || 0}
                                starRatedColor="#00a550"
                                starDimension="17px"
                                starSpacing="1px"
                            />
                        </span>
                        <span className="price">
                            {car.priceOrigin !== car.price && <span className="real">{formatPrice(car.priceOrigin)}</span>}
                            {formatPrice(car.price)}
                        </span>
                    </div>
                    <h2>{car.name}</h2>
                    <p>
                        <span><i className="ic ic-map"></i> {car.distance}</span>
                        <span><i className="ic ic-clock"></i> {car.totalTrips} chuyến</span>
										</p>
										<div className={`group-label marginTop-s ${car.deliveryEnable !== 1 ? "item-min-height" : ""}`}>
											{car.deliveryEnable === 1 && <span>Giao xe tận nơi</span>}
											{car.deliveryEnable === 1 && car.deliveryPrice === 1 && <span>Miễn phí giao xe</span>}
										</div>
                </div>
            </div>
        </a>
    } else
        return <div className="item-car">
            <a href={`/car/${formatTitleInUrl(car.name)}/${car.id}`}>
                <span className="img-car">
                    <div className="fix-img">
                        <img src={car.photos ? car.photos[0].thumbUrl : carPhotoDefault} alt={`Cho thuê xe tự lái ${car.name}`} />
                    </div>
                    <span className="label-pos">
                        {car.totalDiscountPercent > 0 && <span className="discount">Giảm {car.totalDiscountPercent}%</span>}
												{car.instant > 0 && <span className="rent"><i className="ic ic-sm-thunderbolt-wh" /> Đặt xe nhanh</span>}
												{car.pp === 1 && <span className="free"><i className="ic ic-passport" />Chấp nhận passport</span>}
                        {/* {car.deliveryEnable === 1 && car.deliveryPrice === 0 && <span className="free">Miễn phí giao nhận xe</span>} */}
                    </span>
                </span>
                <div className="desc-car">
                    <div className="group">
                        <span className="star">
                            <StarRatings
                                rating={car.rating.avg || 0}
                                starRatedColor="#00a550"
                                starDimension="17px"
                                starSpacing="1px"
                            />
                        </span>
                        <span className="price">
                            {car.priceOrigin !== car.price && <span className="real">{formatPrice(car.priceOrigin)}</span>}
                            {formatPrice(car.price)}
                        </span>
                    </div>
                    <h2>{car.name}</h2>
                    <p>
                        {car.distance && <span><i className="ic ic-map" /> {car.distance}</span>}
                        <span><i className="ic ic-clock" /> {car.totalTrips} chuyến</span>
										</p>
										<div className={`group-label marginTop-s ${car.deliveryEnable !== 1 ? "item-min-height" : ""}`}>
											{car.deliveryEnable === 1 && <span>Giao xe tận nơi</span>}
											{car.deliveryEnable === 1 && car.deliveryPrice === 1 && <span>Miễn phí giao xe</span>}
										</div>
                </div>
            </a>
        </div>
}

class RelatedCars extends React.Component {
    componentDidMount() {
        this.swiper = new window.Swiper(this.refs.swiperRelatedCars, {
            slidesPerView: 4.2,
            spaceBetween: 24,
            threshold: 15,
            speed: 600,
            navigation: {
                nextEl: '.swiper-button-next-related-cars',
                prevEl: '.swiper-button-prev-related-cars',
            },
            slidesOffsetBefore: 0,
            slidesOffsetAfter: 0,
            breakpoints: {
                992: {
                    slidesPerView: 3.2,
                    spaceBetween: 30
                },
                768: {
                    slidesPerView: 3.2,
                    spaceBetween: 30
                },
                480: {
                    slidesPerView: 1.2,
                    spaceBetween: 15
                }
            },
        });
    }

    render() {
        return <div className="module-deal">
            <div className="swiper-button-next swiper-button-next-related-cars">
                <i className="i-arr" />
            </div>
            <div className="swiper-button-prev swiper-button-prev-related-cars">
                <i className="i-arr" />
            </div>
            <div ref="swiperRelatedCars" className="swiper-container swiper-similar">
                <h4 className={`title ${this.props.titleClass || ""}`}>Xe liên quan</h4>
                <div className="swiper-wrapper">
                    {this.props.cars.map((featureCar, i) => {
                        return <div className="swiper-slide" key={i}>
                            <CarItem isNewTab={this.props.isNewTab} featureCar={featureCar} tooglePopup={this.props.tooglePopup} />
                        </div>
                    })}
                </div>
            </div>
        </div>
    }
}

export default RelatedCars;