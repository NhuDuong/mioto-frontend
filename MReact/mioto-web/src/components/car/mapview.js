import React from "react"
import { connect } from "react-redux"

import { Stars } from "../common/stars"
import { setFilter } from "../../actions/carFindingAct"
import { commonErr } from "../common/errors"
import { formatPrice } from "../common/common"
import { renderToString } from 'react-dom/server'

import avatar_default from "../../static/images/avatar_default.png"

const mapStyle = {
    width: "100%",
    height: "100%",
    position: "absolute"
};

const maxRadius = 2000; //mile

var CarPopup;
/** Defines the CarPopup class. */
function defineCarPopupClass() {
    /**
     * A customized popup on the map.
     * @param {!google.maps.LatLng} position
     * @param {!Element} content
     * @constructor
     * @extends {google.maps.OverlayView}
     */
    CarPopup = function (car, position, textContent, onClick) {
        this.id = car.id;
        this.name = car.name;
        this.position = position;

        var contentBox = document.createElement('div');
        contentBox.classList.add('popup-bubble-content');

        if (car.instant) {
            var iconBox = document.createElement('i');
            iconBox.classList.add('ic', 'ic-sm-thunderbolt');
            contentBox.appendChild(iconBox);
        }

        var content = document.createTextNode(textContent);
        contentBox.appendChild(content);

        var pixelOffset = document.createElement('div');
        pixelOffset.classList.add('popup-bubble-anchor');
        pixelOffset.appendChild(contentBox);

        this.anchor = document.createElement('div');
        this.anchor.classList.add('popup-tip-anchor');
        this.anchor.appendChild(pixelOffset);

        //event
        this.anchor.addEventListener('click', () => onClick(car.id));
        this.anchor.addEventListener('click', () => {
            this.anchor.classList.add('active');
        });

        // Optionally stop clicks, etc., from bubbling up to the map.
        // this.stopEventPropagation();
    };
    // NOTE: google.maps.OverlayView is only defined once the Maps API has
    // loaded. That is why CarPopup is defined inside initMap().
    CarPopup.prototype = Object.create(window.google.maps.OverlayView.prototype);

    /** Called when the popup is added to the map. */
    CarPopup.prototype.onAdd = function () {
        this.getPanes().floatPane.appendChild(this.anchor);
    };

    /** Called when the popup is removed from the map. */
    CarPopup.prototype.onRemove = function () {
        if (this.anchor.parentElement) {
            this.anchor.parentElement.removeChild(this.anchor);
        }
    };

    /** Called when the popup needs to draw itself. */
    CarPopup.prototype.draw = function () {
        var divPosition = this.getProjection().fromLatLngToDivPixel(this.position);
        // Hide the popup when it is far out of view.
        var display =
            Math.abs(divPosition.x) < 4000 && Math.abs(divPosition.y) < 4000 ?
                'block' :
                'none';

        if (display === 'block') {
            this.anchor.style.left = divPosition.x + 'px';
            this.anchor.style.top = divPosition.y + 'px';
        }
        if (this.anchor.style.display !== display) {
            this.anchor.style.display = display;
        }
    };

    /** Stops clicks/drags from bubbling up to the map. */
    CarPopup.prototype.stopEventPropagation = function () {
        var anchor = this.anchor;
        anchor.style.cursor = 'auto';

        ['click', 'dblclick', 'contextmenu', 'wheel', 'mousedown', 'touchstart',
            'pointerdown']
            .forEach(function (event) {
                anchor.addEventListener(event, function (e) {
                    e.stopPropagation();
                }, { passive: true });
            });
    };

    CarPopup.prototype.setActive = function () {
        this.anchor.classList.add('active');
    };

    CarPopup.prototype.removeActive = function () {
        this.anchor.classList.remove('active');
    };
}

var ZoomPopup;
function defineZoomPopupClass() {
    ZoomPopup = function (zoom, position, textContent, onClick) {
        this.position = position;

        var contentBox = document.createElement('div');
        contentBox.classList.add('popup-bubble-content');

        var iconBox = document.createElement('i');
        iconBox.classList.add('ic', 'ic-sm-cars');
        contentBox.appendChild(iconBox);

        var content = document.createTextNode(" " + textContent);
        contentBox.appendChild(content);

        var pixelOffset = document.createElement('div');
        pixelOffset.classList.add('popup-bubble-anchor');
        pixelOffset.appendChild(contentBox);

        this.anchor = document.createElement('div');
        this.anchor.classList.add('popup-tip-anchor');
        this.anchor.appendChild(pixelOffset);

        this.anchor.addEventListener('click', () => onClick(zoom));

        // this.stopEventPropagation();
    };
    ZoomPopup.prototype = Object.create(window.google.maps.OverlayView.prototype);

    ZoomPopup.prototype.onAdd = function () {
        this.getPanes().floatPane.appendChild(this.anchor);
    };

    ZoomPopup.prototype.onRemove = function () {
        if (this.anchor.parentElement) {
            this.anchor.parentElement.removeChild(this.anchor);
        }
    };

    ZoomPopup.prototype.draw = function () {
        var divPosition = this.getProjection().fromLatLngToDivPixel(this.position);
        var display =
            Math.abs(divPosition.x) < 4000 && Math.abs(divPosition.y) < 4000 ?
                'block' :
                'none';

        if (display === 'block') {
            this.anchor.style.left = divPosition.x + 'px';
            this.anchor.style.top = divPosition.y + 'px';
        }
        if (this.anchor.style.display !== display) {
            this.anchor.style.display = display;
        }
    };

    ZoomPopup.prototype.stopEventPropagation = function () {
        var anchor = this.anchor;
        anchor.style.cursor = 'auto';

        ['click', 'dblclick', 'contextmenu', 'wheel', 'mousedown', 'touchstart',
            'pointerdown']
            .forEach(function (event) {
                anchor.addEventListener(event, function (e) {
                    e.stopPropagation();
                }, { passive: true });
            });
    };
}

var GroupPopup;
function defineGroupPopupClass() {
    GroupPopup = function (group, position, textContent, onClick) {
        this.group = group;
        this.position = position;

        var contentBox = document.createElement('div');
        contentBox.classList.add('popup-bubble-content');

        var iconBox = document.createElement('i');
        iconBox.classList.add('ic', 'ic-sm-cars');
        contentBox.appendChild(iconBox);

        var content = document.createTextNode(" " + textContent);
        contentBox.appendChild(content);

        var pixelOffset = document.createElement('div');
        pixelOffset.classList.add('popup-bubble-anchor');
        pixelOffset.appendChild(contentBox);

        this.anchor = document.createElement('div');
        this.anchor.classList.add('popup-tip-anchor');
        this.anchor.appendChild(pixelOffset);

        //event
        this.anchor.addEventListener('click', () => onClick(group));
        this.anchor.addEventListener('click', () => {
            this.anchor.classList.add('active');
        });

        // Optionally stop clicks, etc., from bubbling up to the map.
        // this.stopEventPropagation();
    };
    GroupPopup.prototype = Object.create(window.google.maps.OverlayView.prototype);

    GroupPopup.prototype.onAdd = function () {
        this.getPanes().floatPane.appendChild(this.anchor);
    };

    GroupPopup.prototype.onRemove = function () {
        if (this.anchor.parentElement) {
            this.anchor.parentElement.removeChild(this.anchor);
        }
    };

    GroupPopup.prototype.draw = function () {
        var divPosition = this.getProjection().fromLatLngToDivPixel(this.position);
        var display =
            Math.abs(divPosition.x) < 4000 && Math.abs(divPosition.y) < 4000 ?
                'block' :
                'none';

        if (display === 'block') {
            this.anchor.style.left = divPosition.x + 'px';
            this.anchor.style.top = divPosition.y + 'px';
        }
        if (this.anchor.style.display !== display) {
            this.anchor.style.display = display;
        }
    };

    GroupPopup.prototype.stopEventPropagation = function () {
        var anchor = this.anchor;
        anchor.style.cursor = 'auto';

        ['click', 'dblclick', 'contextmenu', 'wheel', 'mousedown', 'touchstart',
            'pointerdown']
            .forEach(function (event) {
                anchor.addEventListener(event, function (e) {
                    e.stopPropagation();
                }, { passive: true });
            });
    };

    GroupPopup.prototype.setActive = function () {
        this.anchor.classList.add('active');
    };

    GroupPopup.prototype.removeActive = function () {
        this.anchor.classList.remove('active');
    };
}

var PosPopup;
function definePosPopupClass() {
    PosPopup = function (position) {
        this.position = position;

        var contentBox = document.createElement('div');
        contentBox.classList.add('popup-pos-content');

        var iconBox = document.createElement('i');
        iconBox.classList.add('ic', 'ic-pin');
        contentBox.appendChild(iconBox);

        this.anchor = document.createElement('div');
        this.anchor.classList.add('popup-pos-anchor', 'popup-tip-anchor');
        this.anchor.appendChild(contentBox);

        // this.stopEventPropagation();
    };

    PosPopup.prototype = Object.create(window.google.maps.OverlayView.prototype);

    PosPopup.prototype.onAdd = function () {
        this.getPanes().floatPane.appendChild(this.anchor);
    };

    PosPopup.prototype.onRemove = function () {
        if (this.anchor.parentElement) {
            this.anchor.parentElement.removeChild(this.anchor);
        }
    };

    PosPopup.prototype.draw = function () {
        var divPosition = this.getProjection().fromLatLngToDivPixel(this.position);
        var display =
            Math.abs(divPosition.x) < 4000 && Math.abs(divPosition.y) < 4000 ?
                'block' :
                'none';

        if (display === 'block') {
            this.anchor.style.left = divPosition.x + 'px';
            this.anchor.style.top = divPosition.y + 'px';
        }
        if (this.anchor.style.display !== display) {
            this.anchor.style.display = display;
        }
    };

    PosPopup.prototype.stopEventPropagation = function () {
        var anchor = this.anchor;
        anchor.style.cursor = 'auto';

        ['click', 'dblclick', 'contextmenu', 'wheel', 'mousedown', 'touchstart',
            'pointerdown']
            .forEach(function (event) {
                anchor.addEventListener(event, function (e) {
                    e.stopPropagation();
                }, { passive: true });
            });
    };

    PosPopup.prototype.setPosition = function (position) {
        this.position = position;
    };
}

const CarSwiperListItem = (props) => {
    const car = props.car;
    return <div key={car.id} className="car-item-inmap swiper-slide">
        <div className="car-item-wrapper">
            <div className="car-item-body">
                <div className="car-item-img">
                    <div className="img-car">
                        <div className="fix-img"> <img src={car.photos ? car.photos[0].thumbUrl : avatar_default} alt="Mioto - Thuê xe tự lái." /></div>
                    </div>
                </div>
                <div className="car-item-info">
                    <div className="rating">
                        <Stars avrg={car.rating.avg} />
                    </div>
                    <div className="car-name">
                        <p>{car.name}</p>
                    </div>
                    <div className="features">
                        <p><span><i className="ic ic-tag"></i> {formatPrice(car.price)}</span><span><i className="ic ic-clock"></i> {car.totalTrips} chuyến</span></p>
                    </div>
                </div>
                {car.instant ? <div className="is-instant-book"><i className="ic ic-sm-thunderbolt"></i></div> : ""}
            </div>
        </div>
    </div>
}

class MapViewCont extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            hasActiveCar: false,
            hasActiveGroup: false,
            lastBufferLocation: new window.google.maps.LatLng(props.carFinding.filter.lat, props.carFinding.filter.lng)
        }

        this.markersCar = [];
        this.indexToId = {};
        this.idToIndex = {};

        this.markersGroup = [];
        this.indexToIdGroup = {};

        this.markerZooms = [];

        this.setFilter = this.setFilter.bind(this);
    }

    setFilter(filter) {
        this.props.dispatch(setFilter(filter));
    }

    resetActive() {
        this.setState({
            hasActiveCar: false,
            hasActiveGroup: false
        });
    }

    setCenter(lat, lng) {
        this.map.setCenter(new window.google.maps.LatLng(lat, lng));
    }

    fitBounds(radius, lat, lng) {
        if (radius && lat && lng) {
            const centerSfo = new window.google.maps.LatLng(lat, lng);
            const circle = new window.google.maps.Circle({
                radius: radius * 1609.34 / 2,
                center: centerSfo
            });

            this.map.fitBounds(circle.getBounds());
        }
    }

    addMarker() {
        //update map center
        // this.curPosMarker.setPosition(new window.google.maps.LatLng(this.props.carFinding.filter.lat, this.props.carFinding.filter.lng));

        var cars = this.props.carFinding.carsInMap;

        //filter cars in view port
        var bounds = this.map.getBounds();

        if (cars) {
            cars = cars.filter(c => {
                return bounds.contains(new window.google.maps.LatLng(c.location.lat, c.location.lon));
            });
        }

        var totalCar = cars ? cars.length : 0;
        var oldMarkers = this.markersCar;
        var markersCar = [];
        var removeSlides = [];

        var i, j, k, isExist;
        for (i = 0; i < oldMarkers.length; ++i) {
            isExist = false;
            for (j = 0; j < totalCar; ++j) {
                if (oldMarkers[i].id === cars[j].id) {
                    isExist = true;
                    break;
                }
            }
            if (!isExist) {
                removeSlides.push(i);
                oldMarkers[i].setMap(null);
            } else {
                oldMarkers[i].removeActive();
                markersCar.push(oldMarkers[i]);
                this.indexToId[markersCar.length - 1] = oldMarkers[i];
                this.idToIndex[oldMarkers[i].id] = markersCar.length - 1;
            }
        }

        var totalRemainSlide = markersCar.length;

        if (removeSlides.length > 0) {
            //clean swiper list
            this.swiperCar.removeSlide(removeSlides);
            this.swiperCar.update();
        }

        //add new marker
        for (i = 0; i < totalCar; ++i) {
            var car = cars[i];
            isExist = false;

            for (j = 0; j < markersCar.length; ++j) {
                if (car.id === markersCar[j].id) {
                    isExist = true;
                    break;
                }
            }

            if (!isExist) {
                //new marker (custom popup)
                const marker = new CarPopup(car, new window.google.maps.LatLng(car.location.lat, car.location.lon), formatPrice(car.price), (id) => {
                    for (k = 0; k < this.markersCar.length; ++k) {
                        this.markersCar[k].removeActive();
                    }
                    for (k = 0; k < this.markersGroup.length; ++k) {
                        this.markersGroup[k].removeActive();
                    }
                    const activeIndex = this.idToIndex[id];
                    this.swiperCar.slideTo(activeIndex, 200, true);
                    //
                    this.setState({
                        hasActiveCar: true,
                        hasActiveGroup: false
                    });
                });
                marker.setMap(this.map);

                //store new marker
                markersCar.push(marker);

                //add new swiper list item
                this.swiperCar.addSlide(i + totalRemainSlide, renderToString(<CarSwiperListItem car={car} />));
                this.swiperCar.update();

                //mapping index and id
                this.indexToId[i + totalRemainSlide] = car;
                this.idToIndex[car.id] = i + totalRemainSlide;
            }
        }

        //update state
        this.markersCar = markersCar;
    }

    addMarkerV2() {
        var i, j, k, isExist;
        var icons = this.props.carFinding.carsInMapV2;
        var cars = [];
        var zooms = [];
        var groups = [];

        //filter cars in view port
        var bounds = this.map.getBounds();

        if (icons) {
            icons = icons.filter(c => {
                if (c.t === 0) {
                    return bounds.contains(new window.google.maps.LatLng(c.ic.location.lat, c.ic.location.lon));
                }
                if (c.t === 1) {
                    return bounds.contains(new window.google.maps.LatLng(c.ig.la, c.ig.lo));
                }
                if (c.t === 2) {
                    return bounds.contains(new window.google.maps.LatLng(c.iz.la, c.iz.lo));
                }
                return false;
            });

            for (i = 0; i < icons.length; ++i) {
                const icon = icons[i];
                if (icon.t === 0) {
                    cars.push(icon.ic);
                }
                if (icon.t === 1) {
                    groups.push(icon.ig);
                }
                if (icon.t === 2) {
                    zooms.push(icon.iz);
                }
            }
        }

        ///------------CAR ICON---------------///
        var totalCar = cars ? cars.length : 0;
        var oldMarkers = this.markersCar;
        var markersCar = [];
        var removeSlides = [];

        for (i = 0; i < oldMarkers.length; ++i) {
            isExist = false;
            for (j = 0; j < totalCar; ++j) {
                if (oldMarkers[i].id === cars[j].id) {
                    isExist = true;
                    break;
                }
            }
            if (!isExist) {
                removeSlides.push(i);
                oldMarkers[i].setMap(null);
            } else {
                oldMarkers[i].removeActive();
                markersCar.push(oldMarkers[i]);
                this.indexToId[markersCar.length - 1] = oldMarkers[i];
                this.idToIndex[oldMarkers[i].id] = markersCar.length - 1;
            }
        }

        var totalRemainSlide = markersCar.length;

        if (removeSlides.length > 0) {
            //clean swiper list
            this.swiperCar.removeSlide(removeSlides);
            this.swiperCar.update();
        }

        //add new marker
        for (i = 0; i < totalCar; ++i) {
            var car = cars[i];
            isExist = false;

            for (j = 0; j < markersCar.length; ++j) {
                if (car.id === markersCar[j].id) {
                    isExist = true;
                    break;
                }
            }

            if (!isExist) {
                //new marker (custom popup)
                const marker = new CarPopup(car, new window.google.maps.LatLng(car.location.lat, car.location.lon), formatPrice(car.price), (id) => {
                    for (k = 0; k < this.markersCar.length; ++k) {
                        this.markersCar[k].removeActive();
                    }
                    for (k = 0; k < this.markersGroup.length; ++k) {
                        this.markersGroup[k].removeActive();
                    }
                    const activeIndex = this.idToIndex[id];
                    this.swiperCar.slideTo(activeIndex, 200, true);
                    //
                    this.setState({
                        hasActiveCar: true,
                        hasActiveGroup: false
                    });
                });
                marker.setMap(this.map);

                //store new marker
                markersCar.push(marker);

                //add new swiper list item
                this.swiperCar.addSlide(i + totalRemainSlide, renderToString(<CarSwiperListItem car={car} />));
                this.swiperCar.update();

                //mapping index and id
                this.indexToId[i + totalRemainSlide] = car;
                this.idToIndex[car.id] = i + totalRemainSlide;
            }
        }

        this.markersCar = markersCar;

        ///------------GROUP ICON---------------///
        var totalGroup = groups ? groups.length : 0;
        var oldMarkerGroups = this.markersGroup;
        var markersGroup = [];

        for (i = 0; i < oldMarkerGroups.length; ++i) {
            isExist = false;
            for (j = 0; j < totalGroup; ++j) {
                if (oldMarkerGroups[i].group.la === groups[j].la && oldMarkerGroups[i].group.lo === groups[j].lo) {
                    isExist = true;
                    break;
                }
            }
            if (!isExist) {
                oldMarkerGroups[i].setMap(null);
            } else {
                oldMarkerGroups[i].removeActive();
                markersGroup.push(oldMarkerGroups[i]);
            }
        }

        for (i = 0; i < totalGroup; ++i) {
            var group = groups[i];
            isExist = false;

            for (j = 0; j < markersGroup.length; ++j) {
                if (group.la === markersGroup[j].group.la && group.lo === markersGroup[j].group.lo) {
                    isExist = true;
                    break;
                }
            }

            if (!isExist) {
                const groupPopup = new GroupPopup(group, new window.google.maps.LatLng(group.la, group.lo), group.ti, (g) => {
                    for (j = 0; j < this.markersGroup.length; ++j) {
                        this.markersGroup[j].removeActive();
                    }
                    for (j = 0; j < this.markersCar.length; ++j) {
                        this.markersCar[j].removeActive();
                    }
                    this.swiperGroup.removeAllSlides();
                    for (j = 0; j < g.ca.length; ++j) {
                        this.swiperGroup.addSlide(j, renderToString(<CarSwiperListItem car={g.ca[j]} />));
                        this.indexToIdGroup[j] = g.ca[j];
                    }
                    this.swiperGroup.update();
                    //
                    this.setState({
                        hasActiveCar: false,
                        hasActiveGroup: true
                    });
                });
                groupPopup.setMap(this.map);
                //
                markersGroup.push(groupPopup);
            }
        }

        this.markersGroup = markersGroup;

        ///------------ZOOM ICON---------------///
        var oldMarkerZooms = this.markerZooms;
        var markerZooms = [];

        for (i = 0; i < oldMarkerZooms.length; ++i) {
            oldMarkerZooms[i].setMap(null);
        }

        if (zooms) {
            for (i = 0; i < zooms.length; ++i) {
                const zoom = zooms[i];
                const zoomPopup = new ZoomPopup(zoom, new window.google.maps.LatLng(zoom.la, zoom.lo), zoom.ti, (z) => {
                    const filter = {
                        ...this.props.carFinding.filter,
                        lat: z.la,
                        lng: z.lo,
                        address: ""
                    }

                    this.setFilter(filter);
                    this.props.setAddressPlaceHolder("Vị trí trên bản đồ.");


                    var bounds = this.map.getBounds();
                    var center = this.map.getCenter();
                    var ne = bounds.getNorthEast();
                    var mapRadius = mapRadius = window.google.maps.geometry.spherical.computeDistanceBetween(center, ne) / 1609.34 / Math.sqrt(2);

                    var deltaZoom = 0;
                    while (mapRadius > z.ra) {
                        mapRadius = mapRadius / 2;
                        deltaZoom = deltaZoom + 1;
                    }

                    this.map.setCenter(new window.google.maps.LatLng(z.la, z.lo));
                    if (deltaZoom > 1) {
                        deltaZoom -= 1;
                    }

                    this.map.setZoom(this.map.getZoom() + deltaZoom);
                });
                zoomPopup.setMap(this.map);
                //
                markerZooms.push(zoomPopup);
            }
        }

        this.markerZooms = markerZooms;
    }

    componentDidMount() {
        //init custom popup
        defineCarPopupClass();
        definePosPopupClass();
        defineGroupPopupClass();
        defineZoomPopupClass();

        //init map
        const map = this.map = new window.google.maps.Map(this.refs.map, {
            center: {
                lat: this.props.carFinding.filter.lat,
                lng: this.props.carFinding.filter.lng
            },
            mapTypeControl: false,
            scrollwheel: false,
            clickableIcons: false,
            streetViewControl: false,
            minZoom: 4,
            zoomControlOptions: {
                position: window.google.maps.ControlPosition.RIGHT_CENTER
            },
            styles: [
                {
                    "featureType": "administrative.country",
                    "elementType": "geometry.fill",
                    "stylers": [
                        {
                            "color": "#ff0000"
                        }
                    ]
                },
                {
                    "featureType": "administrative.province",
                    "elementType": "geometry.fill",
                    "stylers": [
                        {
                            "color": "#7b3535"
                        }
                    ]
                },
                {
                    "featureType": "administrative.locality",
                    "elementType": "geometry.fill",
                    "stylers": [
                        {
                            "color": "#794c4c"
                        }
                    ]
                },
                {
                    "featureType": "administrative.neighborhood",
                    "elementType": "geometry.fill",
                    "stylers": [
                        {
                            "color": "#531a1a"
                        }
                    ]
                },
                {
                    "featureType": "administrative.neighborhood",
                    "elementType": "labels",
                    "stylers": [
                        {
                            "visibility": "on"
                        }
                    ]
                },
                {
                    "featureType": "administrative.neighborhood",
                    "elementType": "labels.text",
                    "stylers": [
                        {
                            "visibility": "on"
                        }
                    ]
                },
                {
                    "featureType": "administrative.land_parcel",
                    "elementType": "geometry.fill",
                    "stylers": [
                        {
                            "color": "#381313"
                        }
                    ]
                },
                {
                    "featureType": "landscape.man_made",
                    "elementType": "geometry.fill",
                    "stylers": [
                        {
                            "color": "#e6e6e6"
                        }
                    ]
                },
                {
                    "featureType": "road.arterial",
                    "elementType": "geometry.fill",
                    "stylers": [
                        {
                            "visibility": "on"
                        }
                    ]
                },
                {
                    "featureType": "road.arterial",
                    "elementType": "geometry.stroke",
                    "stylers": [
                        {
                            "visibility": "on"
                        }
                    ]
                },
                {
                    "featureType": "road.local",
                    "elementType": "geometry.fill",
                    "stylers": [
                        {
                            "visibility": "on"
                        },
                        {
                            "weight": "0.5"
                        },
                        {
                            "gamma": "1.65"
                        }
                    ]
                },
                // {
                //     "featureType": "road.local",
                //     "elementType": "labels",
                //     "stylers": [
                //         {
                //             "visibility": "off"
                //         }
                //     ]
                // },
                {
                    "featureType": "water",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#add4dd"
                        },
                        {
                            "visibility": "simplified"
                        },
                        {
                            "weight": "1.00"
                        }
                    ]
                },
                {
                    "featureType": "water",
                    "elementType": "geometry.fill",
                    "stylers": [
                        {
                            "saturation": "48"
                        },
                        {
                            "visibility": "on"
                        }
                    ]
                },
                {
                    "featureType": "water",
                    "elementType": "labels",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "water",
                    "elementType": "labels.text.fill",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    featureType: 'poi',
                    stylers: [{ visibility: 'off' }]
                },
                {
                    featureType: 'administrative.locality',
                    stylers: [{ visibility: 'off' }]
                },
                {
                    featureType: 'road.highway',
                    elementType: 'labels.icon',
                    stylers: [{ visibility: 'off' }]
                },
                {
                    featureType: 'transit',
                    stylers: [{ visibility: 'off' }]
                }
            ]
        });

        //map event
        // map.addListener('drag', () => {
        //     this.curPosMarker.setPosition(map.getCenter());
        // });

        map.addListener('dragend', () => {
            var lastBufferLocation = this.state.lastBufferLocation;
            const center = map.getCenter();
            const move = window.google.maps.geometry.spherical.computeDistanceBetween(center, lastBufferLocation) / 1609.34;
            const filter = {
                ...this.props.carFinding.filter,
                lat: center.lat(),
                lng: center.lng(),
                address: "",
                isOpenSearching: false
            }

            if (move >= this.props.carFinding.filter.minMiles) {
                lastBufferLocation = new window.google.maps.LatLng(center.lat(), center.lng());
                this.props.searchCarMapMore(filter);
            }

            this.setState({
                lastBufferLocation: lastBufferLocation
            });
            this.resetActive();
            this.setFilter(filter);
            this.props.setAddressPlaceHolder("Vị trí trên bản đồ.");
            // this.curPosMarker.setPosition(new window.google.maps.LatLng(this.props.carFinding.filter.lat, this.props.carFinding.filter.lng));
        });

        map.addListener('zoom_changed', () => {
            // calculate radius (in miles).
            var bounds = map.getBounds();
            var center = map.getCenter();
            var ne = bounds.getNorthEast();

            var radius = window.google.maps.geometry.spherical.computeDistanceBetween(center, ne) / 1609.34;

            if (radius > maxRadius) {
                return;
            }

            const filter = {
                ...this.props.carFinding.filter,
                minMiles: radius,
                isOpenSearching: false
            }

            this.setState({
                lastBufferLocation: new window.google.maps.LatLng(center.lat(), center.lng())
            });
            this.setFilter(filter);
            this.props.searchCarMapMore(filter);
            this.resetActive();
        });

        //fit to input radius
        this.fitBounds(this.props.carFinding.filter.minMiles, this.props.carFinding.filter.lat, this.props.carFinding.filter.lng);

        //test marker
        // const curPosMarker1 = new PosPopup(new window.google.maps.LatLng(10.733671232036789, 106.6330009719561));
        // curPosMarker1.setMap(map);
        // const curPosMarker2 = new PosPopup(new window.google.maps.LatLng(10.73291663098958, 106.63292782515569));
        // curPosMarker2.setMap(map);

        //swiper car
        this.swiperCar = new window.Swiper(this.refs.swiperCar, {
            slidesPerView: 3,
            spaceBetween: 0,
            threshold: 15,
            speed: 300,
            centeredSlides: true,
            iOSEdgeSwipeDetection: true,
            touchEventsTarget: 'wrapper',
            touchReleaseOnEdges: true,
            navigation: {
                nextEl: '.swiper-car-button-next',
                prevEl: '.swiper-car-button-prev'
            },
            breakpoints: {
                992: {
                    slidesPerView: 3
                },
                768: {
                    slidesPerView: 2.6
                },
                480: {
									slidesPerView: 1.4,
									centeredSlides: false
                }
            }
        })

        this.swiperCar.on('slideChange', () => {
            if (!this.markersCar || this.markersCar.length === 0) {
                return;
            }
            var activeIndex = this.swiperCar.activeIndex;
            var activeId = this.indexToId[activeIndex].id;
            for (var i = 0; i < this.markersCar.length; ++i) {
                const marker = this.markersCar[i];
                if (this.state.hasActiveCar && marker.id === activeId) {
                    marker.setActive();
                } else {
                    marker.removeActive();
                }
            }
        });

        this.swiperCar.on('tap', (t) => {
            if (t.path) {
                for (var i = 0; i < t.path.length; ++i) {
                    if (t.path[i].classList) {
                        if (t.path[i].classList.contains('swiper-slide-active')) {
                            this.props.tooglePopup(this.indexToId[this.swiperCar.activeIndex]);
                            return;
                        } else if (t.path[i].classList.contains('swiper-slide-prev')) {
                            this.swiperCar.slidePrev(300, true);
                        } else if (t.path[i].classList.contains('swiper-slide-next')) {
                            this.swiperCar.slideNext(300, true);
                        }
                    }
                }
            } else {
                this.props.tooglePopup(this.indexToId[this.swiperCar.activeIndex]);
                return;
            }
        });

        //swiper group
        this.swiperGroup = new window.Swiper(this.refs.swiperGroup, {
            slidesPerView: 3,
            spaceBetween: 0,
            threshold: 15,
            speed: 300,
            centeredSlides: true,
            iOSEdgeSwipeDetection: true,
            touchEventsTarget: 'wrapper',
            touchReleaseOnEdges: true,
            navigation: {
                nextEl: '.swiper-group-button-next',
                prevEl: '.swiper-group-button-prev'
            },
            breakpoints: {
                992: {
                    slidesPerView: 3
                },
                768: {
                    slidesPerView: 2.4
                },
                480: {
                    slidesPerView: 1.1
                }
            }
        })

        this.swiperGroup.on('tap', (t) => {
            if (t.path) {
                for (var i = 0; i < t.path.length; ++i) {
                    if (t.path[i].classList) {
                        if (t.path[i].classList.contains('swiper-slide-active')) {
                            this.props.tooglePopup(this.indexToIdGroup[this.swiperGroup.activeIndex]);
                            return;
                        } else if (t.path[i].classList.contains('swiper-slide-prev')) {
                            this.swiperGroup.slidePrev(300, true);
                        } else if (t.path[i].classList.contains('swiper-slide-next')) {
                            this.swiperGroup.slideNext(300, true);
                        }
                    }
                }
            } else {
                this.props.tooglePopup(this.indexToIdGroup[this.swiperGroup.activeIndex]);
                return;
            }
        });
    }

    componentWillReceiveProps(props) {
        //the address has changed
        if (props.carFinding.filter.address !== ""
            && props.carFinding.filter.address !== this.props.carFinding.filter.address
            && (props.carFinding.filter.lat !== this.props.carFinding.filter.lat
                || props.carFinding.filter.lng !== this.props.carFinding.filter.lng)) {
            this.setCenter(props.carFinding.filter.lat, props.carFinding.filter.lng);
            this.setState({
                lastBufferLocation: new window.google.maps.LatLng(props.carFinding.filter.lat, props.carFinding.filter.lng)
            });
        }
    }

    render() {
        if (this.map) {
            this.addMarkerV2();
        }

        return <div className="module-map">
            <div className="map-container d-shadow">
                <div ref="map" style={mapStyle} className="map-container d-shadow" />
            </div>
            {this.props.carFinding.err === commonErr.LOADING && <div className="loading-bar">
                <div className="peg" />
            </div>}
            <div ref="swiperCar" className="swiper-container swiper-car-inmap">
                <div className={`list-map-wrapper swiper-wrapper ${this.state.hasActiveCar ? "" : "hidden"}`} />
                <div className="swiper-button-prev swiper-car-button-prev" />
                <div className="swiper-button-next swiper-car-button-next" />
            </div>
            <div ref="swiperGroup" className="swiper-container swiper-car-inmap">
                <div className={`list-map-wrapper swiper-wrapper ${this.state.hasActiveGroup ? "" : "hidden"}`} />
                <div className="swiper-button-prev swiper-group-button-prev" />
                <div className="swiper-button-next swiper-group-button-next" />
            </div>
        </div>
    }
}

function mapState(state) {
    return {
        carFinding: state.carFinding
    }
}

MapViewCont = connect(mapState)(MapViewCont);

class MapView extends React.Component {
    componentDidMount() {
        window.scrollTo(0, 0);
    }

    render() {
        return <MapViewCont searchCarMapMore={this.props.searchCarMapMore} setAddressPlaceHolder={this.props.setAddressPlaceHolder} tooglePopup={this.props.tooglePopup} />
    }
}

export default MapView;