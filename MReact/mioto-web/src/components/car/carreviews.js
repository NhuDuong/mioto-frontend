import React from "react"
import StarRatings from "react-star-ratings"
import moment from 'moment'

import { getCarReviews } from "../../model/car"
import { commonErr } from "../common/errors"

import avatar_default from "../../static/images/avatar_default.png"

export default class CarReviews extends React.Component {
    constructor() {
        super();
        this.state = {
            err: commonErr.INNIT,
            reviews: [],
            profiles: [],
            more: 0,
            totalReviews: 0
        }

        this.getMoreReviews = this.getMoreReviews.bind(this);
    }

    componentDidMount() {
        const reviews = this.props.detail.reviews;
        const profiles = this.props.detail.profiles;
        const totalReviews = this.props.detail.info.totalReviews;
        const more = this.props.detail.moreReview;

        if (!reviews || !profiles) {
            if (totalReviews > 0) {
                getCarReviews(this.props.detail.info.id, 0, 0, 0).then(resp => {
                    if (resp.data.data && resp.data.data.reviews && resp.data.data.profiles) {
                        this.setState({
                            err: resp.data.error,
                            reviews: this.state.reviews.concat(resp.data.data.reviews),
                            profiles: this.stata.profiles.concat(resp.data.data.profiles),
                            more: resp.data.data.more,
                            totalReviews: resp.data.data.totalReviews
                        })
                    }
                })
            }
        } else {
            this.setState({
                reviews: this.state.reviews.concat(reviews),
                profiles: this.state.reviews.concat(profiles),
                more: more,
                totalReviews: totalReviews
            })
        }
    }

    getMoreReviews() {
        getCarReviews(this.props.detail.info.id, 0, this.state.reviews.length, 0).then(resp => {
            if (resp.data.data && resp.data.data.reviews && resp.data.data.profiles) {
                this.setState({
                    err: resp.data.error,
                    reviews: this.state.reviews.concat(resp.data.data.reviews),
                    profiles: this.state.profiles.concat(resp.data.data.profiles),
                    more: resp.data.data.more,
                    totalReviews: resp.data.data.totalReviews
                })
            }
        })
    }

    render() {
        const car = this.props.detail.info;

        return <div className="info-car--desc">
            <div className="review">
                <h4 className="title">ĐÁNH GIÁ</h4>
                <div className="group-review">
                    <StarRatings
                        rating={car.rating ? car.rating.avg : 0}
                        starRatedColor="#00a550"
                        starDimension="17px"
                        starSpacing="1px"
                    />
                    <div className="bar-line" />
                    <p>{car.totalReviews > 0 ? <span className="value">{car.totalReviews} đánh giá</span> : `chưa có đánh giá nào`}</p>
                </div>
                <hr className="line" />
                {this.state.reviews && this.state.reviews.map(review => {
                    var reviewProfile;
                    if (this.state.profiles.length > 0) {
                        for (var i = 0; i < this.state.profiles.length; ++i) {
                            if (this.state.profiles[i].uid === review.uid) {
                                reviewProfile = this.state.profiles[i];
                                break;
                            }
                        }
                    }

                    return <div className="list-comments" key={review.id}>
                        <div className="left">
                            <div className="fix-avatar">
                                {!review.avt && reviewProfile && <a href={`/profile/${reviewProfile.uid}`} target="_blank">
                                    <img src={(reviewProfile.avatar && reviewProfile.avatar.thumbUrl) ? reviewProfile.avatar.thumbUrl : avatar_default} alt="Mioto - Thuê xe tự lái" />
                                </a>}
                                {review.avt && review.avt !== "" && <img src={review.avt} alt="Mioto - Thuê xe tự lái" />}
                            </div>
                        </div>
                        <div className="right">
                            {!review.name && reviewProfile && <a href={`/profile/${reviewProfile.uid}`} target="_blank">
                                <h4 className="name">{reviewProfile.name}</h4>
                            </a>}
                            {review.name && review.name !== "" && <h4 className="name">{review.name}</h4>}
                            <div className="cmt-box">
                                <div className="group">
                                    <StarRatings
                                        rating={review.rating}
                                        starRatedColor="#00a550"
                                        starDimension="17px"
                                        starSpacing="1px"
                                    />
                                    <p className="date">{moment(review.timeCreated).fromNow()}</p>
                                </div>
                                <p className="desc">{review.comment}</p>
                            </div>
                        </div>
                    </div>
                })}
                {this.state.more == true && <div className="s-all">
                    <a onClick={this.getMoreReviews} className="see-all">Xem thêm<i className="ic ic-chevron-up"></i></a>
                </div>}
            </div>
        </div>
    }
}