import React from "react"

import { Modal } from "react-bootstrap"
import HtmlToReactParser from "html-to-react"
import moment from 'moment'

import { commonErr } from "../common/errors"
import { formatPrice } from "../common/common"
import { getMyPromo, searchPromo } from "../../model/promo"
import { LoadingInline } from "../common/loading"

import promo_cover from "../../static/images/img_car.jpg"

export default class PromoBox extends React.Component {
    constructor() {
        super();
        this.state = {
            err: commonErr.INNIT,
            searchCode: ""
        }

        this.onPromoCodeClick = this.onPromoCodeClick.bind(this);
    }

    componentDidMount() {
        this.setState({
            err: commonErr.LOADING
        });
        getMyPromo(0, 0, 0).then(resp => {
            if (resp.data.error >= commonErr.SUCCESS) {
                this.setState({
                    err: resp.data.error,
                    myCodes: resp.data.data.codes
                });
            } else {
                this.setState({
                    err: resp.data.error,
                    myCodes: null
                });
            }
        });
    }

    onSearchCodeChange(event) {
        clearTimeout(this.timer);
        const searchCode = event.target.value;
        this.setState({
            searchCode: searchCode
        });
        this.timer = setTimeout(() => this.searchCode(searchCode), 500);
    }

    searchCode(searchCode) {
        if (!searchCode || searchCode === "") {
            return;
        }

        this.setState({
            err: commonErr.LOADING
        });
        searchPromo(searchCode, this.props.carId).then(resp => {
            if (resp.data.error >= commonErr.SUCCESS) {
                this.setState({
                    searchCode: searchCode,
                    err: resp.data.error,
                    codes: resp.data.data.codes
                });
            } else {
                this.setState({
                    searchCode: searchCode,
                    err: resp.data.error,
                    codes: null
                });
            }
        });
    }

    onPromoCodeClick(code) {
        this.props.onPromoCodeClick(code);
        this.props.onHide();
    }

    tooglePromoCodeDetail(code) {
        if (code.isShowDetail) {
            code.isShowDetail = false
        } else {
            code.isShowDetail = true
        }
        this.setState({
            codes: this.state.codes,
            myCodes: this.state.myCodes
        });
    }

    render() {
        var content;
        var htmlToReactParser = new HtmlToReactParser.Parser();
        if (this.state.err === commonErr.LOADING) {
            content = <LoadingInline />
        } else if (!this.state.searchCode || this.state.searchCode === "") {
            if (!this.state.myCodes || this.state.myCodes.length === 0) {
                if (this.state.err !== commonErr.INNIT) {
                    content = <p>Không tìm thấy mã khuyến mãi.</p>
                }
            } else {
                content = this.state.myCodes.map(code => <div key={code.id}>
                    <div className="box-promo">
                        <div className="left"><img className="img-promo" src={code.thumb} alt="Mioto - Thuê xe tự lái" /></div>
                        <div className="center">
                            <p className="code">{code.code}</p>
                            {code.discountPercent > 0 ? <p className="desc">Giảm <span>{code.discountPercent}%. </span>(tối đa<span> {formatPrice(code.discountMax)}</span>)</p> : <p className="desc">Giảm <span>{formatPrice(code.discountMax)}</span></p>}
                            {!code.isShowDetail && <a onClick={() => this.tooglePromoCodeDetail(code)} className="link-desc chevron-up">Chi tiết</a>}
                            {code.isShowDetail && <a onClick={() => this.tooglePromoCodeDetail(code)} className="link-desc chevron-up">Ẩn chi tiết</a>}
                        </div>
                        <div className="right"><a className="btn btn--m btn-primary btn-sm" onClick={() => this.onPromoCodeClick(code)}>Áp dụng</a></div>
                    </div>
                    {code.isShowDetail && code.desc && <div className="promo-details">
                        <div className="promo-header">
                            <div className="cover-promo" style={{ backgroundImage: `url(${promo_cover})` }}> </div>
                            <div className="cover-detail">
                                <div className="img-promo"><img src={code.thumb} alt="Mioto - Thuê xe tự lái" /></div>
                                <div className="desc">
                                    <h4>{code.code}</h4>
                                    <p> Áp dụng từ <span className="green">{moment(code.validDateFrom).format("DD/MM/YYYY")} </span>đến<span className="green">{moment(code.validDateTo).format("DD/MM/YYYY")} </span></p>
                                </div>
                            </div>
                        </div>
                        <div className="promo-body">
                            {htmlToReactParser.parse(code.desc)}
                        </div>
                    </div>}
                    {code.isShowDetail && !code.desc && <div>Không có mô tả</div>}
                </div>)
            }
        } else {
            if (!this.state.codes || this.state.codes.length === 0) {
                if (this.state.err !== commonErr.INNIT) {
                    content = <p>Không tìm thấy mã khuyến mãi.</p>
                }
            } else {
                content = this.state.codes.map(code => <div key={code.id}>
                    <div className="box-promo">
                        <div className="left">
                            <img className="img-promo" src={code.thumb} alt="Mioto - Thuê xe tự lái" />
                        </div>
                        <div className="center">
                            <p className="code">{code.code}</p>
                            {code.discountPercent > 0 ? <p className="desc">Giảm<span> {code.discountPercent}%</span> (tối đa<span> {formatPrice(code.discountMax)}</span>)</p>
                                : <p className="desc">Giảm<span> {formatPrice(code.discountMax)}</span></p>}
                            {!code.isShowDetail && <a onClick={() => this.tooglePromoCodeDetail(code)} className="link-desc chevron-down">Chi tiết</a>}
                            {code.isShowDetail && <a onClick={() => this.tooglePromoCodeDetail(code)} className="link-desc chevron-up">Ẩn chi tiết</a>}
                        </div>
                        <div className="right">
                            <a className="btn btn--m btn-primary btn-sm" onClick={() => this.onPromoCodeClick(code)}>Áp dụng</a>
                        </div>
                    </div>
                    <div className="space m"></div>
                    {code.isShowDetail && code.desc && <div className="promo-details">
                        <div className="promo-header">
                            <div class="cover-promo" style={{ backgroundImage: `url(${promo_cover})` }}> </div>
                            <div className="cover-detail">
                                <div className="img-promo">
                                    <img src={code.thumb} alt="Mioto - Thuê xe tự lái" />
                                </div>
                                <div className="desc">
                                    <h4>{code.code}</h4>
                                    <p> Áp dụng từ <span className="green">{moment(code.validDateFrom).format("DD/MM/YYYY")} </span>đến <span className="green">{moment(code.validDateTo).format("DD/MM/YYYY")} </span></p>
                                </div>
                            </div>
                        </div>
                        <div className="promo-body">
                            {htmlToReactParser.parse(code.desc)}
                        </div>
                    </div>}
                    {code.isShowDetail && !code.desc && <div>Không có mô tả</div>}
                    <div className="space m"></div>
                </div>)
            }
        }

        return <Modal
            show={this.props.show}
            onHide={this.props.onHide}
            dialogClassName="modal-sm modal-dialog"
        >
            <Modal.Header closeButton={true} closeLabel={""}>
                <Modal.Title>Sử dụng mã khuyến mãi</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <div className="form-default">
                    <div className="line-form">
                        <div className="wrap-input has-ico-search"> <i className="ic ic-search"></i>
                            <input type="text" placeholder="Nhập mã khuyến mãi" onChange={this.onSearchCodeChange.bind(this)} style={{ textTransform: "uppercase" }} />
                        </div>
                    </div>
                </div>
                <div className="body-promo">
                    {content}
                </div>
            </Modal.Body>
        </Modal>
    }
}