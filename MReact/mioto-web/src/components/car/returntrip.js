import React from "react"
import { Modal } from "react-bootstrap"
import ReactStars from "react-stars"

import { commonErr } from "../common/errors"
import { returnTrip, reviewTripByOwner } from "../../model/car"

export default class ReturnTrip extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            err: commonErr.INNIT,
            step: props.step || 0, //0: confirm, 1: return trip, 2: review form, 3: review
            errMsg: "",
            rating: 5,
            comment: ""
        }
    }

    getTrip() {
        this.props.getTrip(this.props.trip.id);
    }

    onRatingChange(rating) {
        this.setState({
            rating: rating
        });
    }

    onCommentChange(e) {
        this.setState({
            comment: e.target.value
        });
    }

    finish() {
        this.props.hideModal();
        this.setState({
            step: this.props.step || 0,
            err: commonErr.INNIT
        });
        this.getTrip();
    }

    goToReturn() {
        this.setState({
            step: 0,
            err: commonErr.INNIT,
            errorMessage: ""
        });
    }

    returnTrip() {
        returnTrip(this.props.trip.id).then(resp => {
            this.setState({
                step: 1,
                err: resp.data.error,
                errMsg: resp.data.errorMessage
            });
        });
    }

    goToReview() {
        this.setState({
            step: 2,
            err: commonErr.INNIT
        });
    }

    review() {
        reviewTripByOwner(this.props.trip.id, this.state.rating, this.state.comment).then(resp => {
            this.setState({
                step: 3,
                err: resp.data.error,
                errMsg: resp.data.errorMessage
            });
        });
    }

    render() {
        var content;
        if (this.state.step === 3) {
            if (this.state.err >= commonErr.SUCCESS) {
                content = <div className="form-default form-s">
                    <div className="line-form">
                        <div className="textAlign-center"><i className="ic ic-verify"></i> Đánh giá thành công</div>
                    </div>
                    <div className="clear"></div>
                    <div className="space m"></div>
                    <button className="btn btn-primary btn--m" type="button" onClick={this.finish.bind(this)}>Thoát</button>
                </div>
            } else {
                content = <div className="form-default form-s">
                    <div className="line-form">
                        <div className="textAlign-center"><i className="ic ic-error"></i> {this.state.errMsg}</div>
                    </div>
                    <div className="clear" />
                    <div className="space m" />
                    <p className="textAlign-center has-more-btn">
                        <button className="btn btn-primary btn--m" type="button" onClick={this.goToReview.bind(this)}>Thử lại</button>
                    </p>
                </div>
            }
        } else if (this.state.step === 2) {
            content = <div className="module-register" style={{ background: "none", padding: "5px" }}>
                <div className="form-default form-s" style={{ width: "160px", margin: "auto" }}>
                    <ReactStars
                        count={5}
                        size={30}
                        value={this.state.rating}
                        half={false}
                        onChange={this.onRatingChange.bind(this)}
                        color1={'#efefef'}
                        color2={'#00a550'} />
                </div>
                <div className="form-default form-s">
                    <div className="space m"></div>
                    <div className="line-form">
                        <div className="wrap-input has-ico">
                            <textarea className="textarea" onChange={this.onCommentChange.bind(this)} placeholder="Nhập nội dung đánh giá" value={this.state.comment}></textarea>
                        </div>
                    </div>
                    <div className="clear" />
                    <div className="space m" />
                    <div className="space m" />
                    <div className="space m" />
                    <div className="space m" />
                    <div className="wrap-btn has-2btn">
                        <div className="wr-btn">
                            <a className="btn btn-secondary btn--m" type="button" onClick={this.finish.bind(this)}>Thoát</a>
                        </div>
                        <div className="wr-btn">
                            <a className="btn btn-primary btn--m" type="button" onClick={this.review.bind(this)}>Đánh giá</a>
                        </div>
                    </div>
                </div>
            </div>
        } else if (this.state.step === 1) {
            if (this.state.err >= commonErr.SUCCESS) {
                content = <div className="module-register" style={{ background: "none", padding: "5px" }}>
                    <div className="form-default form-s">
                        <div className="line-form">
                            <div className="textAlign-center"><i className="ic ic-verify"></i> Hệ thống đã cập nhật</div>
                        </div>
                        <div className="clear" />
                        <div className="space m" />
                        <div className="wrap-btn has-2btn">
                            <div className="wr-btn">
                                <a className="btn btn-secondary btn--m" type="button" onClick={this.finish.bind(this)}>Thoát</a>
                            </div>
                            <div className="wr-btn">
                                <a className="btn btn-primary btn--m" type="button" onClick={this.goToReview.bind(this)}>Đánh giá</a>
                            </div>
                        </div>
                    </div>
                </div>
            } else {
                content = <div className="module-register" style={{ background: "none", padding: "5px" }}>
                    <div className="form-default form-s">
                        <div className="line-form">
                            <div className="textAlign-center"><i className="ic ic-error"></i> {this.state.err === -15000 ? "Không thể trả xe trước thời gian kết thúc 16 tiếng." : this.state.errMsg}</div>
                        </div>
                        <div className="clear" />
                        <div className="space m" />
                        <div className="wrap-btn has-2btn">
                            <div className="wr-btn">
                                <a className="btn btn-secondary btn--m" type="button" onClick={this.finish.bind(this)}>Thoát</a>
                            </div>
                            <div className="wr-btn">
                                <button className="btn btn-primary btn--m" type="button" onClick={this.goToReturn.bind(this)}>Thử lại</button>
                            </div>
                        </div>
                    </div>
                </div>
            }
        } else {
            content = <div className="form-default form-s">
                <div className="line-form textAlignCenter">
                    <div className="textAlign-center">Bạn đã nhận lại xe từ khách hàng?</div>
                    <div className="space m" />
                </div>
                <div className="clear" />
                <button className="btn btn-primary btn--m" type="button" onClick={this.returnTrip.bind(this)}>Xác nhận</button>
            </div>
        }

        return <Modal
            show={this.props.show}
            onHide={this.finish.bind(this)}
            dialogClassName="modal-sm modal-dialog"
        >
            <Modal.Header closeButton={true} closeLabel={""}>
                <Modal.Title>{this.state.step >= 2 ? "Đánh giá chuyến" : "Xác nhận khách trả xe"}</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                {content}
            </Modal.Body>
        </Modal>
    }
}