import React from "react"
import { Modal } from "react-bootstrap"

import { getCarReportReasons, reportCar } from "../../model/car"
import { commonErr } from "../common/errors"

export default class CarReport extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            err: commonErr.INNIT,
            errMsg: "",

            reasons: null,
            ip_reason: "c1",
            ip_comment: ""
        }
    }

    componentDidMount() {
        getCarReportReasons(this.props.car.id).then(resp => {
            if (resp.data.error >= commonErr.SUCCESS) {
                this.setState({
                    err: resp.data.error,
                    reasons: resp.data.data.reasons
                });
            } else {
                this.setState({
                    err: resp.data.error,
                    errMsg: resp.data.errorMessage
                });
            }
        });
    }

    handleInputChange = (event) => {
        this.setState({
            err: commonErr.INNIT,
            errMsg: "",
            [event.target.name]: event.target.value
        });
    }

    hideModal() {
        this.setState({
            err: commonErr.INNIT,
            errMsg: "",

            reasons: null,
            ip_reason: "c1",
            ip_comment: ""
        });

        this.props.hideModal();
    }

    reportCar() {
        if (this.state.ip_reason === "0"
            || (this.state.ip_reason === "c0" && this.state.ip_comment === "")) {
            this.setState({
                err: commonErr.FAIL,
                errMsg: "Vui lòng chọn hoặc ghi rõ lý do của bạn."
            });
        } else {
            reportCar(this.props.car.id, this.state.ip_reason, this.state.ip_comment).then((resp) => {
                if (resp.data.error >= commonErr.SUCCESS) {
                    this.setState({
                        err: resp.data.error,
                        errMsg: resp.data.errorMessage
                    });
                    this.hideModal();
                } else {
                    this.setState({
                        err: resp.data.error,
                        errMsg: resp.data.errorMessage
                    });
                }
            })
        }
    }

    render() {
        return <Modal
            show={this.props.show}
            onHide={this.hideModal.bind(this)}
            dialogClassName="modal-sm modal-dialog"
        >
            <Modal.Header closeButton={true} closeLabel={""}>
                <Modal.Title>Báo xấu</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <div className="form-default form-s">
                    {this.state.err <= commonErr.SUCCESS && this.state.errMsg !== "" && <p className="textAlign-center" style={{ color: "red" }}>{this.state.errMsg}</p>}
                    <div className="line-form">
                        <div className="wrap-select">
                            <select name="ip_reason" onChange={this.handleInputChange.bind(this)} value={this.state.ip_reason}>
                                {this.state.reasons && this.state.reasons.map(
                                    reason => <option key={reason.reason} value={reason.reason}>{reason.desc}</option>
                                )}
                            </select>
                        </div>
                    </div>
                    <div className="line-form">
                        <div className="wrap-input">
                            <input type="text" placeholder="Nhập lời nhắn" defaultValue="" name="ip_comment" onChange={this.handleInputChange.bind(this)} />
                        </div>
                    </div>
                    <div className="clear"></div>
                    <button className="btn btn-primary btn--m" type="button" name="cancelTripBtn" onClick={this.reportCar.bind(this)}>Báo cáo</button>
                </div>
            </Modal.Body>
        </Modal>
    }
}