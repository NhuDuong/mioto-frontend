import React from "react"
import { Modal } from "react-bootstrap"

import { commonErr } from "../common/errors"
import { getCancelTripReasonsOwner, cancelTripOwner } from "../../model/car"

export default class CancelTripOwner extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            err: commonErr.INNIT,
            step: 0, //0 confirm 1: cancel trip 2: done
            errMsg: "",
            cancelReasons: null,
            ip_cancel_reason: "o0",
            ip_comment: ""
        }
    }

    componentDidMount() {
        getCancelTripReasonsOwner(this.props.trip.id).then(resp => {
            if (resp.data.error >= commonErr.SUCCESS) {
                this.setState({
                    err: resp.data.error,
                    errMsg: resp.data.errorMessage,
                    cancelReasons: resp.data.data.reasons
                });
            } else {
                this.setState({
                    err: resp.data.error,
                    errMsg: resp.data.errorMessage
                });
            }
        });
    }

    handleInputChange = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        });
    }

    goToCancelTrip() {
        this.setState({
            step: 1
        });
    }

    finishCancel() {
        this.props.hideModal();
        this.setState({
            step: 0,
            err: commonErr.INNIT
        });
        this.props.getTrip(this.props.trip.id);
    }

    isValidCancelTrip() {
        if (!this.state.ip_cancel_reason || this.state.ip_cancel_reason === "" || this.state.ip_cancel_reason === "0") {
            return false;
        }
        if (this.state.ip_cancel_reason === "o0" && (!this.state.ip_comment || this.state.ip_comment === "")) {
            return false;
        }
        return true;
    }

    cancelTrip() {
        if (!this.isValidCancelTrip()) {
            this.setState({
                err: commonErr.FAIL,
                errMsg: "Vui lòng chọn lý do hoặc ghi rõ lý do của bạn."
            });
        } else {
            cancelTripOwner(this.props.trip.id, this.state.ip_cancel_reason, this.state.ip_comment).then(resp => {
                if (resp.data.error >= commonErr.SUCCESS) {
                    this.setState({
                        step: 2,
                        err: resp.data.error,
                        errMsg: resp.data.errorMessage
                    });
                } else {
                    this.setState({
                        step: 2,
                        err: resp.data.error,
                        errMsg: resp.data.errorMessage
                    });
                }
            });
        }
    }

    render() {
        var content;
        if (this.state.step === 2) {
            if (this.state.err >= commonErr.SUCCESS) {
                content = <div className="form-default form-s">
                    <div className="line-form">
                        <div className="textAlign-center"><i className="ic ic-verify"></i> Huỷ chuyến thành công</div>
                    </div>
                    <div className="clear"></div>
                    <div className="space m"></div>
                    {this.state.ip_cancel_reason === "o6" && <a className="btn btn-primary btn--m" type="button" href={`/carsetting/${this.props.trip.carId}#calendarsetting`}>Cập nhật lịch xe</a>}
                    <div className="space m"></div>
                    <button className="btn btn-primary btn--m" type="button" onClick={this.finishCancel.bind(this)}>Hoàn tất</button>
                </div>
            } else {
                content = <div className="form-default form-s">
                    <div className="line-form">
                        <div className="textAlign-center"><i className="ic ic-error"></i> Huỷ chuyến thất bại</div>
                    </div>
                    <div className="clear"></div>
                    <div className="space m"></div>
                    <p className="textAlign-center has-more-btn">
                        <button className="btn btn-primary btn--m" type="button" onClick={this.goToCancelTrip.bind(this)}>Thử lại</button>
                    </p>
                </div>
            }
        } else if (this.state.step === 1) {
            content = <div className="form-default form-s">
                <div className="line-form">
                    <div className="wrap-select">
                        <select name="ip_cancel_reason" onChange={this.handleInputChange.bind(this)}>
                            {this.state.cancelReasons && this.state.cancelReasons.map(
                                reason => <option value={reason.reason}>{reason.desc}</option>
                            )}
                        </select>
                    </div>
                </div>
                <div className="line-form">
                    <div className="wrap-input has-ico">
                        <input type="text" placeholder="Vui lòng nhập lý do hoặc lời nhắn" name="ip_comment" onChange={this.handleInputChange.bind(this)} />
                    </div>
                </div>
                <div className="clear"></div>
                <button className="btn btn-primary btn--m" type="button" name="cancelTripBtn" disabled={!this.isValidCancelTrip()} onClick={this.cancelTrip.bind(this)}>Huỷ chuyến</button>
            </div>
        } else {
            content = <div className="form-default form-s">
                <div className="line-form textAlignCenter">
                    <div className="textAlign-center">Bạn có chắc muốn huỷ chuyến?</div>
                    <div className="space m"></div>
                    <p className="textAlign-center">
                        Lưu ý: Tuỳ vào thời điểm huỷ chuyến bạn có thể bị mất phí. Vui lòng xem thêm tại <a href="/privacy" target="_blank">chính sách huỷ chuyến</a> của Mioto.
                    </p>
                </div>
                <div className="clear"></div>
                <button className="btn btn-primary btn--m" type="button" onClick={this.goToCancelTrip.bind(this)}>Tiếp tục</button>
            </div>
        }

        return <Modal
            show={this.props.show}
            onHide={this.finishCancel.bind(this)}
            dialogClassName="modal-sm modal-dialog"
        >
            <Modal.Header closeButton={true} closeLabel={""}>
                <Modal.Title>Huỷ chuyến</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                {content}
            </Modal.Body>
        </Modal>
    }
}