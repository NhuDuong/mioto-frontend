import React from "react"
import { connect } from "react-redux"
import { Modal } from "react-bootstrap"
import moment from 'moment'
import { geocodeByAddress, getLatLng } from 'react-places-autocomplete'
import NumberFormat from "react-number-format"
import cookie from 'react-cookies'
import DateRangePicker from 'react-bootstrap-daterangepicker'
import { isIOS } from "react-device-detect"

import CarBooking from "./carbooking"
import { LoadingInline } from "../common/loading"
import { commonErr } from "../common/errors"
import { formatPrice } from "../common/common"
import { init, getCarPrice, getCarCalendar } from "../../model/car"
import PromoBox from "./promobox"
import LocationPikcer from "../common/locationpicker"

import mic from "../../static/images/homev2/mic.png"
import logo_mic from "../../static/images/logomic.png"

const calendarTemplate = '<div class="daterangepicker dropdown-menu">' +
    '<div class="calendar left">' +
    '<div class="daterangepicker_input hidden">' +
    '<input class="input-mini form-control" type="text" name="daterangepicker_start" value="" />' +
    '<i class="fa fa-calendar glyphicon glyphicon-calendar"></i>' +
    '<div class="calendar-time">' +
    '<div></div>' +
    '<i class="fa fa-clock-o glyphicon glyphicon-time"></i>' +
    '</div>' +
    '</div>' +
    '<div class="calendar-table"></div>' +
    '</div>' +
    '</div>' +
    '</div>';

const bookingTimes = [];
for (var i = 0; i < 1440; i = i + 30) {
    const time = i;
    const hour = (time - time % 60) / 60;
    const hourLabel = hour < 10 ? `0${hour}` : `${hour}`;
    const min = time % 60;
    const minLabel = min < 10 ? `0${min}` : `${min}`;
    bookingTimes.push({ value: time, label: `${hourLabel}:${minLabel}` });
}

class BookingSideBar extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            err: commonErr.INNIT,
            errMsg: "",

            car: props.detail.info,
            selfPickup: true,
            promoCode: null,
            fromAddress: "",
            fromAddressLat: 0,
            fromAddressLng: 0,
            dateNotAvail: 0,

            showWarningForm: false,
						isShowBooking: false, 
						showDetailIns: false
        }

        this.openTimeSetting = this.openTimeSetting.bind(this);
        this.closeTimeSetting = this.closeTimeSetting.bind(this);
        this.openBooking = this.openBooking.bind(this);
        this.closeBooking = this.closeBooking.bind(this);
        this.onSelfPickupCheck = this.onSelfPickupCheck.bind(this);
        this.onCustomPickupCheck = this.onCustomPickupCheck.bind(this);
        this.onFromAddressChange = this.onFromAddressChange.bind(this);
        this.onAddressFocus = this.onAddressFocus.bind(this);
        this.onStartTimeChange = this.onStartTimeChange.bind(this);
        this.onEndTimeChange = this.onEndTimeChange.bind(this);
				this.onDateRangeChange = this.onDateRangeChange.bind(this);
				this.openDetailIns = this.openDetailIns.bind(this); 
				this.closeDetailIns = this.closeDetailIns.bind(this);
    }

    getCarPrice(selfPickup, startDate, endDate, fromAddress, fromAddressLat, fromAddressLng, promoCode) {
        if (this.state.err < commonErr.SUCCESS || !startDate || !endDate || !endDate.isValid()) {
            return;
        }

        const { car } = this.state;
       

        this.setState({
            err: commonErr.LOADING
        });

        if (!selfPickup && fromAddress && fromAddress !== "") {
            if (!fromAddressLat || !fromAddressLng || fromAddressLat === 0 || !fromAddressLng === 0) {
                geocodeByAddress(fromAddress)
                    .then(results => getLatLng(results[0]))
                    .then(latLng => {
                        getCarPrice(car.id, startDate, endDate, latLng.lat, latLng.lng, promoCode).then(resp => {
                            if (resp.data.data.car.priceSummary.promotionDiscount <= 0) {
                                promoCode = null;
                                cookie.remove("_promo", { path: '/' });
                            }

                            this.setState({
                                err: resp.data.error,
                                car: resp.data.data.car,
                                dateNotAvail: resp.data.data.dateNotAvail,
                                promoCode: promoCode
                            });

                            if (resp.data.data) {
                                this.props.updatePrice(resp.data.data);
                            }
                        });
                    })
            } else {
                getCarPrice(car.id, startDate, endDate, fromAddressLat, fromAddressLng, promoCode).then(resp => {
                    if (resp.data.data.car.priceSummary.promotionDiscount <= 0) {
                        promoCode = null;
                        cookie.remove("_promo", { path: '/' });
                    }

                    this.setState({
                        err: resp.data.error,
                        car: resp.data.data.car,
                        dateNotAvail: resp.data.data.dateNotAvail,
                        promoCode: promoCode
                    });

                    if (resp.data.data) {
                        this.props.updatePrice(resp.data.data);
                    }
                });
            }
        } else {
            getCarPrice(car.id, startDate, endDate, 0, 0, promoCode).then(resp => {
                if (resp.data.data.car.priceSummary.promotionDiscount <= 0) {
                    promoCode = null;
                    cookie.remove("_promo", { path: '/' });
                }

                this.setState({
                    err: resp.data.error,
                    car: resp.data.data.car,
                    dateNotAvail: resp.data.data.dateNotAvail,
                    promoCode: promoCode
                });

                if (resp.data.data) {
                    this.props.updatePrice(resp.data.data);
                }
            })
        }
    }

    componentDidMount() {
        this.setState({
            isOwner: (this.props.loggedProfile && this.props.loggedProfile.uid
                && this.props.loggedProfile.uid === this.state.car.ownerId)
        });
        // check valid cookie before set
        if(cookie.load("_mstartts") < moment().add(2, "hours").valueOf() || cookie.load("_mendts") < moment().add(2, "hours").valueOf()){
            cookie.remove("_mstartts", { path: '/' });
            cookie.remove("_mendts", { path: '/' });
        }
        var startDate = cookie.load("_mstartts");
        var endDate = cookie.load("_mendts");
        var promoCode = cookie.load("_promo");
        var selfPickup = cookie.load("_slfpk") === 'false' ? false : true;
        if (selfPickup === false && this.state.car.deliveryEnable !== 1) {
            selfPickup = true;
        }
        var fromAddress = "";
        var fromAddressLat = 0;
        var fromAddressLng = 0;
        if (selfPickup === false) {
            var dAddress = cookie.load("_daddr");
            if (dAddress) {
                if (dAddress && dAddress.addr && dAddress.lat && dAddress.lng) {
                    fromAddress = dAddress.addr;
                    fromAddressLat = dAddress.lat;
                    fromAddressLng = dAddress.lng;
                }
            }
        }

        if (!startDate || !endDate) {
            init().then(initResp => {
                if (initResp.data.data) {
                    startDate = moment(initResp.data.data.startTime);
                    endDate = moment(initResp.data.data.endTime);

                    this.setState({
                        startDate: startDate,
                        endDate: endDate,
                        startTime: startDate.get("hour") * 60 + startDate.get("minute"),
                        endTime: endDate.get("hour") * 60 + endDate.get("minute"),
                        selfPickup: selfPickup,
                        fromAddress: fromAddress,
                        fromAddressLat: fromAddressLat,
                        fromAddressLng: fromAddressLng
                    })

                    if (this.props.detail.info.priceSummary.specialDiscount > 0) {
                        this.getCarPrice(selfPickup, startDate, endDate, fromAddress, fromAddressLat, fromAddressLng);
                    } else {
                        this.getCarPrice(selfPickup, startDate, endDate, fromAddress, fromAddressLat, fromAddressLng, promoCode);
                    }
                }
            });
        } else {
            startDate = moment(startDate * 1);
            endDate = moment(endDate * 1);

            this.setState({
                startDate: startDate,
                endDate: endDate,
                startTime: startDate.get("hour") * 60 + startDate.get("minute"),
                endTime: endDate.get("hour") * 60 + endDate.get("minute"),
                selfPickup: selfPickup,
                fromAddress: fromAddress,
                fromAddressLat: fromAddressLat,
                fromAddressLng: fromAddressLng
            })

            if (this.props.detail.info.priceSummary.specialDiscount > 0) {
                this.getCarPrice(selfPickup, startDate, endDate, fromAddress, fromAddressLat, fromAddressLng);
            } else {
                this.getCarPrice(selfPickup, startDate, endDate, fromAddress, fromAddressLat, fromAddressLng, promoCode);
            }
        }

        getCarCalendar(this.state.car.id, "cd").then(resp => {
            const car = this.state.car;
            const calendar = resp.data.data;
            const invalidDays = [];
            const priceSpecifics = [];
            const priceRepeat = [];
            const instantDays = [];
            var i, j;

            if (calendar) {
              
                if(car.instantEnable){                                
                    const start = car.instantRangeFrom === 0 ? moment(calendar.startDate).startOf('day').valueOf() : moment(moment().valueOf() + car.instantRangeFrom * 1000).startOf('day').valueOf();
                    const end = car.instantRangeTo === 0 ? moment(calendar.endDate).endOf('month').valueOf() : moment(moment().valueOf() + car.instantRangeTo * 1000).startOf('day').valueOf();
                    for(i = start; i <= end; i += (3600000 * 24)){
                        instantDays.push(i);
                    }
                }

                if (calendar.unavailsAfter && calendar.unavailsAfter > calendar.unavailsBefore){
                    const start = moment(calendar.startDate).startOf('day').valueOf();
                    const end = moment(calendar.endDate).endOf('month').valueOf();
                    for (i = start; i < calendar.unavailsBefore * 1000 + moment().startOf('day').valueOf(); i += (3600000 * 24)) {
                        invalidDays.push({
                            date: moment(i)
                        });
                    }
                    const endOfAvailsDay = calendar.unavailsAfter * 1000 + moment().valueOf();
                    for (i = moment(endOfAvailsDay + 24 * 3600 * 1000).startOf('day').valueOf(); i <= end; i += (3600000 * 24)) {
                        invalidDays.push({
                            date: moment(i)
                        });
                    }
                }

                if (calendar.unavails) {
                    for (i = 0; i < calendar.unavails.length; ++i) {
                        for (j = calendar.unavails[i].startTime; j <= calendar.unavails[i].endTime; j += (3600000 * 24)) {
                            invalidDays.push({
                                date: moment(j)
                            });
                        }
                    }
                }

                if (calendar.unavailsRepeatWeekday) {
                    for (i = 0; i < calendar.unavailsRepeatWeekday.length; ++i) {
                        const start = moment();
                        const end = moment().add(60, "days");
                        for (j = start; j <= end; j = j.add(1, "days")) {
                            if (calendar.unavailsRepeatWeekday[i] === 1 && j.isoWeekday() === 7) {
                                invalidDays.push({
                                    date: moment(j)
                                });
                            } else if (j.isoWeekday() === calendar.unavailsRepeatWeekday[i] - 1) {
                                invalidDays.push({
                                    date: moment(j)
                                });
                            }
                        }
                    }
                }

                if (calendar.priceSpecifics) {
                    for (i = 0; i < calendar.priceSpecifics.length; ++i) {
                        const start = moment(calendar.priceSpecifics[i].startTime).startOf('day').valueOf();
                        const end = moment(calendar.priceSpecifics[i].endTime).startOf('day').valueOf();
                        for (j = start; j <= end; j += (3600000 * 24)) {
                            priceSpecifics.push({
                                ts: j,
                                price: calendar.priceSpecifics[i].price
                            });
                        }
                    }
                }

                for (i = 1; i <= 7; ++i) {
                    const weekday = {
                        weekday: i,
                        price: 0
                    };

                    if (calendar.priceRepeat) {
                        for (j = 0; j < calendar.priceRepeat.length; ++j) {
                            if (i === 7 && calendar.priceRepeat[j].weekday === 1) {
                                weekday.price = calendar.priceRepeat[j].price;
                                break;
                            } else if ((i + 1) === calendar.priceRepeat[j].weekday) {
                                weekday.price = calendar.priceRepeat[j].price;
                                break;
                            }
                        }
                    }

                    priceRepeat.push(weekday);
                }
            }

            this.setState({
                calendar: calendar,
                priceDaily: calendar.priceDaily,
                invalidDays: invalidDays,
                priceSpecifics: priceSpecifics,
                priceRepeat: priceRepeat,
                instantDays: instantDays,
                isLimitRepeatPrice: calendar.priceRepeatEndDate > 0,
                priceRepeatEndDate: calendar.priceRepeatEndDate > 0 ? calendar.priceRepeatEndDate : moment().add(1, 'month').valueOf(),
            })
        })
    }

    onSelfPickupCheck() {
        this.setState({
            selfPickup: true,
            fromAddress: "",
            fromAddressLat: null,
            fromAddressLng: null
        });

        this.getCarPrice(true, this.state.startDate, this.state.endDate, this.state.fromAddress, this.state.fromAddressLat, this.state.fromAddressLng, this.state.promoCode);

        cookie.save("_slfpk", true, { path: '/' });
    }

    onCustomPickupCheck() {
        var fromAddress = "";
        var fromAddressLat = null;
        var fromAddressLng = null;

        var dAddress = cookie.load("_daddr");
        if (dAddress) {
            if (dAddress && dAddress.addr && dAddress.lat && dAddress.lng) {
                fromAddress = dAddress.addr;
                fromAddressLat = dAddress.lat;
                fromAddressLng = dAddress.lng;
                this.getCarPrice(false, this.state.startDate, this.state.endDate, fromAddress, fromAddressLat, fromAddressLng, this.state.promoCode);
            }
        }
        this.setState({
            selfPickup: false,
            fromAddress: fromAddress,
            fromAddressLat: fromAddressLat,
            fromAddressLng: fromAddressLng
        });

        cookie.save("_slfpk", false, { path: '/' });
    }

    onAddressFocus(event) {
        if (!this.state.fromAddress || this.state.fromAddress === "") {
            this.toogleLocationPicker();
            event.target.blur();
        }
    }

    onFromAddressChange(event) {
        const address = event.target.value;
        this.setState({
            selfPickup: (!address || address === ""),
            fromAddress: address
        });
        if (!event.target.value || event.target.value === "") {
            this.toogleLocationPicker();
            event.target.blur();
        }
    }

    onAddressLocationUpdate(address, location) {
        this.setState({
            fromAddress: address,
            fromAddressLat: location.lat,
            fromAddressLng: location.lng
        });

        if (address && location.lat && location.lng) {
            const dAddress = {
                addr: address,
                lat: location.lat,
                lng: location.lng
            }
            cookie.save("_daddr", JSON.stringify(dAddress), { path: '/' });
        }

        this.getCarPrice(this.state.selfPickup, this.state.startDate, this.state.endDate, address, location.lat, location.lng, this.state.promoCode);
    }

    toogleLocationPicker() {
        this.setState({
            isShowLocationPicker: !this.state.isShowLocationPicker
        });
    }

    onDateRangeChange(event, picker) {
        const startDate = picker.startDate ? moment(picker.startDate).set({
            hour: (this.state.startTime - this.state.startTime % 60) / 60,
            minute: this.state.startTime % 60,
            second: 0
        }) : null;

        const endDate = picker.endDate ? moment(picker.endDate).set({
            hour: (this.state.endTime - this.state.endTime % 60) / 60,
            minute: this.state.endTime % 60,
            second: 0
        }) : null;

        if (startDate && endDate && endDate <= startDate) {
            this.setState({
                startDate: endDate,
                startTime: this.state.endTime,
                endDate: startDate,
                endTime: this.state.startTime,
                err: commonErr.SUCCESS,
                errMsg: ""
            });

            this.getCarPrice(this.state.selfPickup, endDate, startDate, this.state.fromAddress, this.state.fromAddressLat, this.state.fromAddressLng, this.state.promoCode);
        } else {
            this.setState({
                startDate: startDate,
                endDate: endDate,
                err: commonErr.SUCCESS,
                errMsg: ""
            });

            this.getCarPrice(this.state.selfPickup, startDate, endDate, this.state.fromAddress, this.state.fromAddressLat, this.state.fromAddressLng, this.state.promoCode);
        }
    }

    onStartTimeChange(event) {
        const time = event.target.value;
        const hour = (time - time % 60) / 60;
        const min = time % 60;
        const startDate = moment(this.state.startDate).set({
            hour: hour,
            minute: min,
            second: 0
        });

        if (this.state.endDate && this.state.endDate.isValid() && this.state.endDate <= startDate) {
            this.setState({
                startDate: this.state.endDate,
                endDate: startDate,
                startTime: this.state.endTime,
                endTime: time,
                err: commonErr.FAIL,
                errMsg: "Thời gian thuê xe không hợp lệ."
            });

            this.getCarPrice(this.state.selfPickup, this.state.endDate, startDate, this.state.fromAddress, this.state.fromAddressLat, this.state.fromAddressLng, this.state.promoCode);
        } else {
            this.setState({
                startDate: startDate,
                startTime: time,
                err: commonErr.SUCCESS,
                errMsg: ""
            });

            this.getCarPrice(this.state.selfPickup, startDate, this.state.endDate, this.state.fromAddress, this.state.fromAddressLat, this.state.fromAddressLng, this.state.promoCode);
        }
    }

    onEndTimeChange(event) {
        const time = event.target.value;
        if (this.state.endDate && this.state.endDate.isValid()) {
            const hour = (time - time % 60) / 60;
            const min = time % 60;
            const endDate = moment(this.state.endDate).set({
                hour: hour,
                minute: min,
                second: 0
            })
            if (endDate <= this.state.startDate) {
                this.setState({
                    endDate: this.state.startDate,
                    startDate: endDate,
                    endTime: this.state.startTime,
                    startTime: time,
                    err: commonErr.FAIL,
                    errMsg: "Thời gian thuê xe không hợp lệ."
                });

                this.getCarPrice(this.state.selfPickup, endDate, this.state.startDate, this.state.fromAddress, this.state.fromAddressLat, this.state.fromAddressLng, this.state.promoCode);
            } else {
                this.setState({
                    endDate: endDate,
                    endTime: time,
                    err: commonErr.SUCCESS,
                    errMsg: ""
                });

                this.getCarPrice(this.state.selfPickup, this.state.startDate, endDate, this.state.fromAddress, this.state.fromAddressLat, this.state.fromAddressLng, this.state.promoCode);
            }
        } else {
            this.setState({
                endTime: time,
                err: commonErr.SUCCESS,
                errMsg: ""
            });
        }
    }

    onPromoCodeClick(promoCode) {
        this.setState({
            promoCode: promoCode
        });

        this.getCarPrice(this.state.selfPickup, this.state.startDate, this.state.endDate, this.state.fromAddress, this.state.fromAddressLat, this.state.fromAddressLng, promoCode);

        cookie.save("_promo", JSON.stringify(promoCode), { path: '/' });
    }

    removePromoCode() {
        this.setState({
            promoCode: ""
        });

        this.getCarPrice(this.state.selfPickup, this.state.startDate, this.state.endDate, this.state.fromAddress, this.state.fromAddressLat, this.state.fromAddressLng, "");

        cookie.remove("_promo", { path: '/' });
    }

    openTimeSetting() {
        this.setState({
            showTimeSetting: true
        });
    }

    closeTimeSetting() {
        this.setState({
            showTimeSetting: false
        });
    }

    openWarningForm() {
        this.setState({
            showWarningForm: true
        });
    }

    closeWarningForm() {
        this.setState({
            showWarningForm: false
        });
    }

    openPromoBox(e) {
        if (!this.props.loggedProfile || !this.props.loggedProfile.uid) {
            e.preventDefault();
            this.props.showLoginForm();
            return false;
        }
        this.setState({
            showPromoBox: true
        });
    }

    closePromoBox() {
        this.setState({
            showPromoBox: false
        });
		}
		
		openDetailIns() {
			this.setState({
				showDetailIns: true
			})
		}
	
		closeDetailIns() {
			this.setState({
				showDetailIns: false
			})
		}

    rentCarBtnClick(e) {
        if (!this.props.loggedProfile || !this.props.loggedProfile.uid) {
            this.props.showLoginForm();
        } else if (this.props.loggedProfile.phoneVerified !== 1) {
            this.openWarningForm();
        } else if (this.props.loggedProfile.uid === this.state.car.ownerId) {
            this.setState({
                isOwner: true
            });
        } else {
            //GA
            const utmSrc = cookie.load("_utm_src") || "web_directly";
            const utmExt = cookie.load("_utm_ext") || "no_label";
            window.ga('send', 'event', utmSrc, "CONFIRM_BOOKING", utmExt);

            this.openBooking();
        }
    }

    getDayChildContent(day) {
        var child = "";
        var isShowPrice = day.isAfter(moment().subtract(1, "days"), "day");
        const invalidDays = this.state.invalidDays;
        const days = invalidDays.filter(function (invalidDay) {
            return (invalidDay.date.isSame(day, "day") && day.isAfter(moment()));
        });

        if (days.length !== 0) {
            child += `<span class="full-in-calendar unavail-in-calendar"></span>`
            isShowPrice = false
        }

        const calendar = this.state.calendar;
        const _1dayInMSec = 24 * 3600000;
        const minWidth = 15;
        if (calendar) {
            const bookings = calendar.bookings;
            if (bookings) {
                for (i = 0; i < bookings.length; ++i) {
                    const trip = bookings[i];
                    if (day.isSame(moment(trip.startTime), "day")
                        || day.isSame(moment(trip.endTime), "day")
                        || day.isBetween(moment(trip.startTime), moment(trip.endTime))) {
                        var width = 100;
                        var left = 0;

                        if (moment(trip.startTime).isSame(moment(trip.endTime), "day")) {
                            width = (trip.endTime - trip.startTime) * 100 / _1dayInMSec
                            left = 100 - (moment(day).endOf("day").valueOf() - trip.startTime) * 100 / _1dayInMSec
                            if (width < minWidth) {
                                left = left - (minWidth - width)
                                width = minWidth;
                            }
                        } else if (moment(trip.endTime).isSame(day, "day")) {
                            width = (trip.endTime - moment(day).startOf("day").valueOf()) * 100 / _1dayInMSec
                        } else if (moment(trip.startTime).isSame(day, "day")) {
                            left = (trip.startTime - moment(day).startOf("day").valueOf()) * 100 / _1dayInMSec
                            if (left > (100 - minWidth)) {
                                left = (100 - minWidth)
                            }
                            width = 100 - left
                        }

                        child += `<span class="full-in-calendar unavail-in-calendar" style="width: ${width}%; left: ${left}%;"></span>`

                        if (width === 100) {
                            isShowPrice = false
                        }
                    }
                }
            }
        }

        const priceSpecifics = this.state.priceSpecifics;
        const priceRepeat = this.state.priceRepeat;
        const priceRepeatEndDate = this.state.priceRepeatEndDate;
        const isLimitRepeatPrice = this.state.isLimitRepeatPrice;

        var price = this.state.priceDaily;
        var j;

        for (j = 0; j < priceRepeat.length; ++j) {
            const weekday = priceRepeat[j];
            if (weekday.price > 0
                && day.isoWeekday() === weekday.weekday
                && (!isLimitRepeatPrice || !priceRepeatEndDate || priceRepeatEndDate === 0 || day.valueOf() <= priceRepeatEndDate)) {
                price = weekday.price;
                break;
            }
        }

        for (j = 0; j < priceSpecifics.length; ++j) {
            if (day.isSame(moment(priceSpecifics[j].ts), 'day')) {
                price = priceSpecifics[j].price
                break;
            }
        }

        child += `<span class="price-in-calendar">${isShowPrice ? formatPrice(price) : '&nbsp'}</span>`

        //- Instant Booking
        const instantDays = this.state.instantDays;
        for (j = 0; j < instantDays.length; ++j){
            if (day.isSame(moment(instantDays[j]), 'day')) { 
                if (isShowPrice) {
                    child += `<i class="ic ic-xs-thunderbolt instant-in-calendar"></i>`                    
                }
                break;
            }
        }
        return child;
    }

    isValidBooking() {
        return (this.state.car
            && !this.state.isOwner
            && (this.state.dateNotAvail <= 0)
            && (this.state.selfPickup || (this.state.fromAddress && this.state.fromAddress !== ""))
            && this.state.startDate && this.state.endDate && this.state.endDate.isValid()
            && this.state.car.priceSummary && this.state.car.priceSummary.totalDays > 0
            && (!this.state.car.deliveryEnable || this.state.car.priceSummary.deliveryFee >= 0)
            && this.state.err >= commonErr.SUCCESS && this.state.err !== commonErr.INNIT && this.state.err !== commonErr.LOADING);
    }

    openBooking() {
        this.setState({
            isShowBooking: true
        })
    }

    closeBooking() {
        this.setState({
            isShowBooking: false
        })
    }

    hideMessageBox() {
        this.setState({
            err: commonErr.SUCCESS,
            errMsg: ""
        });
    }

    render() {
        var content;
        if (this.state.err === commonErr.INNIT) {
            content = <div className="sidebar-detail">
                <div className="rent-box">
                    <LoadingInline />
                </div>
            </div>
        } else {            
            const car = this.state.car;
            const priceSummary = car.priceSummary;
            const startDate = this.state.startDate ? moment(this.state.startDate) : null;
            const endDate = this.state.endDate ? moment(this.state.endDate) : null;
            const startTime = this.state.startTime;
            const endTime = this.state.endTime;
            const minDate = moment();
            const maxDate = this.state.calendar ? moment(this.state.calendar.endDate).endOf("month") : moment().add(2, "month");
            const dateNotAvail = this.state.dateNotAvail;
            const promoCode = this.state.promoCode;
            const isOwner = this.state.isOwner;
            var errMsg;
            if (this.state.err < commonErr.SUCCESS) {
                errMsg = "Thời gian thuê xe không hợp lệ."
            } else if (dateNotAvail > 0) {
                errMsg = "Xe bận trong khoảng thời gian trên. Vui lòng đặt xe khác hoặc thay đổi lịch trình thích hợp."
            }

            content = <div className="sidebar-detail">
                <div className="rent-box rent-car" id="booking-sidebar">
                    <div className="price">
                        {car.priceOrigin !== priceSummary.priceSpecial && <span className="real">{formatPrice(car.priceOrigin)}</span>}
                        <h3>{formatPrice(priceSummary.priceSpecial)} <span> / ngày</span></h3>
                    </div>
                    <div className="line-form has-timer">
                        <label className="label">Ngày bắt đầu</label>
                        <div className="wrap-input has-dropdown date">
                            <DateRangePicker
                                startDate={startDate}
                                endDate={endDate}
                                minDate={minDate}
                                maxDate={maxDate}
                                timePicker={true}
                                singleDateRangePicker={true}
                                singleSelect={true}
                                autoApply={true}
								isIOs={isIOS}
                                timePickerIncrement={30}
                                linkedCalendars={false}
                                timePicker24Hour={true}
                                onHide={this.onDateRangeChange.bind(this)}
                                getChild={this.getDayChildContent.bind(this)}
                                template={calendarTemplate}
                                buttonClasses={["hidden"]}
                                opens={"center"}
                                parentEl={"#booking-sidebar"}>
                                <span className="value">{startDate ? startDate.format("DD/MM/YYYY") : "Chọn ngày"}</span>
                            </DateRangePicker>
                        </div>
                        <div className="wrap-input has-dropdown time wrap-select">
                            <select onChange={this.onStartTimeChange.bind(this)} value={startTime}>
                                {bookingTimes.map(time => <option key={`s_${time.value}`} value={time.value}>{time.label} </option>)}
                            </select>
                        </div>
                    </div>
                    <div className="line-form has-timer">
                        <label className="label">Ngày kết thúc</label>
                        <div className="wrap-input has-dropdown date">
                            <DateRangePicker
                                startDate={startDate}
                                endDate={endDate}
                                minDate={minDate}
                                maxDate={maxDate}
                                timePicker={true}
                                singleDateRangePicker={true}
                                singleSelect={true}
                                autoApply={true}
                                isIOs={isIOS}
                                buttonClasses={["hidden"]}
                                timePickerIncrement={30}
                                pickingEndDateOnly={true}
                                opens={"center"}
                                linkedCalendars={false}
                                timePicker24Hour={true}
                                onHide={this.onDateRangeChange.bind(this)}
                                getChild={this.getDayChildContent.bind(this)}
                                template={calendarTemplate}
                                parentEl={"#booking-sidebar"}>
                                <span className="value">{(endDate && endDate.isValid()) ? endDate.format("DD/MM/YYYY") : "Chọn ngày"}</span>
                            </DateRangePicker>
                        </div>
                        <div className="wrap-input has-dropdown time wrap-select">
                            <select onChange={this.onEndTimeChange.bind(this)} value={endTime}>
                                {bookingTimes.map(time => <option key={`e_${time.value}`} value={time.value}>{time.label} </option>)}
                            </select>
                        </div>
                    </div>
                    {isOwner && <div className="calendar">
                        <p><span className="ic ic-notice"></span>Bạn không thể thuê xe của mình.</p>
                    </div>}
                    {errMsg && <div className="calendar">
                        <p><span className="ic ic-notice"></span>{errMsg}</p>
                    </div>}
                    {car.discountEnable === 1 && <div className="notif-box">
                        <div className="notif-body">
                            <p> {car.discountWeekly > 0 && <label>Thuê tuần giảm <span>{car.discountWeekly}% </span></label>} {car.discountWeekly > 0 && car.discountMonthly > 0 && " - "} {car.discountMonthly > 0 && <label>Thuê tháng giảm <span>{car.discountMonthly}%</span></label>}</p>
                        </div>
                    </div>}
                    <div className="line-form local">
                        <label>Địa điểm giao nhận xe</label>
                        {car.deliveryEnable === 1 && <div className="line-radio">
                            <label className="custom-radio custom-control">
                                <input className="custom-control-input" type="radio"
                                    name="setting-mode-in-price"
                                    value={"self-pickup"}
                                    checked={this.state.selfPickup}
                                    onChange={this.onSelfPickupCheck} />
                                <span className="custom-control-indicator"></span>
                                <span className="custom-control-description">Địa chỉ xe</span>
                            </label>
                            <label className="custom-radio custom-control">
                                <input className="custom-control-input" type="radio"
                                    name="setting-mode-in-price"
                                    value={"custom-pickup"}
                                    checked={!this.state.selfPickup}
                                    onChange={this.onCustomPickupCheck} />
                                <span className="custom-control-indicator"></span>
                                <span className="custom-control-description">Giao nhận tận nơi</span>
                            </label>
                        </div>}
                        {(car.deliveryEnable !== 1 || this.state.selfPickup) && <div className="note"><p>{car.locationAddr}.</p></div>}
                        {(car.deliveryEnable !== 1 || this.state.selfPickup) && <div className="note"><p style={{ color: "gray" }}>{car.deliveryEnable !== 1 && <span>Không hỗ trợ giao nhận xe tận nơi. </span>}Địa chỉ cụ thể sẽ được hiển thị sau khi đặt cọc.</p></div>}
                        {car.deliveryEnable === 1 && !this.state.selfPickup && <div className="group">
                            <LocationPikcer show={this.state.isShowLocationPicker} onHide={this.toogleLocationPicker.bind(this)} onUpdate={this.onAddressLocationUpdate.bind(this)} address={this.state.fromAddress} location={{ lat: this.state.fromAddressLat, lng: this.state.fromAddressLng }} />
                            <i className="ic ic-location-f" />
                            <div className="wrap-input" onClick={this.toogleLocationPicker.bind(this)}>
                                {this.state.fromAddress !== "" ? <span className="address-pickup">{this.state.fromAddress}</span> : <span className="address-pickup placeholder">Click để chọn địa chỉ</span>}
                            </div>
                        </div>}
                    </div>
									  {this.state.fromAddress && this.state.fromAddress !== "" && car.deliveryEnable === 1 && priceSummary.deliveryFee < 0 && <div className="note">
											<p className="marginBottom-xs">Hỗ trợ giao xe tận nơi trong vòng <strong>{car.deliveryRadius}km</strong>.</p>
											{car.deliveryPrice > 0 ? <p className="marginBottom-xs">Phí giao nhận xe (2 chiều): <strong>{formatPrice(car.deliveryPrice)}/km</strong>.</p> : <p className="marginBottom-xs">Miễn phí giao nhận xe.</p> }
											<p className="calendar text-danger"><span className="ic ic-notice"></span>Địa chỉ giao xe quá xa. Vui lòng chọn địa điểm khác thích hợp hơn</p>
									</div>}
								
										{!this.state.selfPickup && (!this.state.fromAddress || this.state.fromAddress === "") && <div className="note">
											<p className="marginBottom-xs">Hỗ trợ giao xe tận nơi trong vòng <strong>{car.deliveryRadius}km</strong>.</p>
											{car.deliveryPrice > 0 ? <p className="marginBottom-xs">Phí giao nhận xe (2 chiều): <strong>{formatPrice(car.deliveryPrice)}/km</strong>.</p> : <p className="marginBottom-xs">Miễn phí giao nhận xe.</p> }
											<p className="calendar text-danger"><span className="ic ic-notice"></span>Vui lòng chọn địa điểm giao nhận xe.</p></div>
										}
										{(this.state.fromAddress || this.state.fromAddress !== "") && car.deliveryEnable === 1 && !this.state.selfPickup && car.deliveryRadius > 0 &&  car.deliveryPrice > 0 && priceSummary.deliveryFee > 0 && <div className="note">
											<p className="marginBottom-xs">Hỗ trợ giao xe tận nơi trong vòng <strong>{car.deliveryRadius}km</strong>. </p>
											<p>Phí giao nhận xe (2 chiều): <strong>{formatPrice(car.deliveryPrice)}/km</strong>.</p>
											<div className="space m" />
										</div>}
                    {(this.state.fromAddress || this.state.fromAddress !== "") && car.deliveryEnable === 1 && !this.state.selfPickup && car.deliveryRadius > 0 && (!priceSummary.deliveryFee || priceSummary.deliveryFee === 0) && car.deliveryPrice === 0 && <div className="note">
												<p className="marginBottom-xs">Hỗ trợ giao xe tận nơi trong vòng <strong>{car.deliveryRadius}km</strong>.</p>
												<p>Miễn phí giao nhận xe.</p>
                        <div className="space m" />
                    </div>}
                    {this.state.fromAddress && this.state.fromAddress !== "" && car.deliveryEnable === 1 && !this.state.selfPickup && (!car.deliveryRadius || car.deliveryRadius <= 0) && priceSummary.deliveryFee > 0 && <div className="note">
                        <p>Phí giao nhận xe (2 chiều) <strong>{formatPrice(car.deliveryPrice)}/km</strong>.</p>
                        <div className="space m" />
                    </div>}
                    <div className="line-form local">
                        <label>Giới hạn quãng đường</label>
                        <div className="note">
                            {car.limitEnable === 1 && <div className="note"><p>Tối đa <strong>{car.limitKM}</strong> km/ngày. Phí <strong>{formatPrice(car.limitPrice)}</strong>/km vượt giới hạn.</p></div>}
                            {car.limitEnable !== 1 && <div className="note"><p>Không giới hạn quãng đường.</p></div>}
                        </div>
									</div>
									{car.insSupport === 1 && <div className="line-form local">
									<div className="note insurance">
												<label>Bảo hiểm</label>
												<p><a onClick={this.openDetailIns}>Bảo hiểm vật chất xe ô tô từ BH Quân đội
												<img className="img-fluid logo-ins" src={logo_mic} /></a></p>
                    	</div>
								</div>}
								
                    <div className="car-bill">
                        <h4 className="text-center">Chi tiết giá</h4>
                        <div className="bill-wrap">
                            <div className="group">
                                <p>Đơn giá thuê</p>
                                <span><NumberFormat value={priceSummary.priceSpecial} displayType={'text'} thousandSeparator={" "} decimalSeparator={","} suffix={" / ngày"} /></span>
                            </div>
                            {priceSummary.discountPercent > 0 && <div className="group long-term">
                                <p>{priceSummary.discountType === 0 ? <span>Đơn giá thuê (giảm {priceSummary.discountPercent}%)</span> : <span>Thuê{priceSummary.discountType == 1 && <span> tuần</span>}{priceSummary.discountType === 2 && <span> tháng</span>} <span> (giảm {priceSummary.discountPercent}%)</span></span>}</p>
                                <span className="line-through">Giá gốc <NumberFormat value={priceSummary.priceOrigin} displayType={'text'} thousandSeparator={" "} decimalSeparator={","} suffix={" / ngày"} /></span>
                            </div>}
                            {priceSummary.specialDiscount > 0 && <div className="group long-term">
                                <div className="d-flex">
                                    <p>Chương trình giảm giá</p>
                                    <div className="tooltip"><i className="ic ic-question-mark" />
                                        <div className="tooltip-text">Không áp dụng đồng thời với mã khuyến mãi.</div>
                                    </div>
                                </div>
                                <span className="line-through">{priceSummary.priceOrigin === priceSummary.price && <span>Giá gốc </span>}<NumberFormat value={priceSummary.price} displayType={'text'} thousandSeparator={" "} decimalSeparator={","} suffix={" / ngày"} /></span>
                            </div>}
                            {priceSummary.tripFee > 0 && <div className="group">
                                <p>Phí dịch vụ <div className="tooltip"><i className="ic ic-question-mark" /><div className="tooltip-text">Phí dịch vụ nhằm hỗ trợ Mioto duy trì nền tảng ứng dụng và các hoạt động chăm sóc khách hàng một cách tốt nhất.</div></div></p>
                                <span><NumberFormat value={priceSummary.tripFee} displayType={'text'} thousandSeparator={" "} decimalSeparator={","} suffix={" / ngày"}/></span>
														</div>}
														{priceSummary.insuranceFee > 0 && <div className="group">
                                <p>Phí bảo hiểm  <div className="tooltip"><i className="ic ic-question-mark" /><div className="tooltip-text">Chuyến đi của bạn được mua gói bảo hiểm vật chất xe ô tô từ nhà bảo hiểm MIC. Trường hơp có sự cố ngoài ý muốn (trong phạm vi bảo hiểm), số tiền thanh toán tối đa là 2,000,000VND/vụ.</div></div></p>
                                <span><NumberFormat value={priceSummary.insuranceFee} displayType={'text'} thousandSeparator={" "} decimalSeparator={","} suffix={" / ngày"}/></span>
                            </div>}
                            <div className="group has-line">
                                <p>Tổng phí thuê xe</p>
                                <span><NumberFormat value={priceSummary.totalPerDay} displayType={'text'} thousandSeparator={" "} decimalSeparator={","} /> x <strong>{priceSummary.totalDays} ngày</strong></span>
                            </div>
                            
                            {priceSummary.deliveryFee > 0 && <div className="group">
                                <p>Phí giao nhận xe (2 chiều)</p>
                                <span><NumberFormat value={priceSummary.deliveryFee > 0 ? priceSummary.deliveryFee : 0} displayType={'text'} thousandSeparator={" "} decimalSeparator={","} /> {(priceSummary.deliveryDistance && priceSummary.deliveryDistance) > 0 && <NumberFormat value={priceSummary.deliveryDistance} displayType={'text'} thousandSeparator={" "} decimalSeparator={","} prefix={" ("} suffix={" km)"} decimalScale={1} />}</span>
                            </div>}
                            {priceSummary.promotionDiscount > 0 && <div className="group">
                                {promoCode && <p>Khuyến mãi mã <strong>{promoCode.code}</strong><a className="func-remove" onClick={this.removePromoCode.bind(this)}><i className="ic ic-remove-thin"></i></a></p>}
                                <span>- <NumberFormat value={priceSummary.promotionDiscount > 0 ? priceSummary.promotionDiscount : 0} displayType={'text'} thousandSeparator={" "} decimalSeparator={","} /></span>
                            </div>}
                            <div className="group has-line">
                                <p><strong>Tổng cộng</strong></p>
                                <span><strong><NumberFormat value={priceSummary.priceTotal} displayType={'text'} thousandSeparator={" "} decimalSeparator={","} suffix={"đ"} /></strong></span>
                            </div>
                        </div>
                        <div className="space m" />
                        <div className="promotion-code d-flex">
                            <div style={{ cursor: 'pointer' }} className="title-new" onClick={this.openPromoBox.bind(this)}>
                                <h4 className="lstitle-new">Mã khuyến mãi</h4>
                            </div>
                        </div>
                        <div className="space m" />
                        <div className="space m" />
                        <div className="wrap-btn">
                            <a className="btn btn-primary btn--m" disabled={!this.isValidBooking()} onClick={this.rentCarBtnClick.bind(this)}>{car.instant === 1 && <i className="ic ic-thunderbolt-wh" />}ĐẶT XE</a>
                        </div>
                        <Modal
                            show={this.state.showWarningForm}
                            onHide={this.closeWarningForm.bind(this)}
                            dialogClassName="modal-sm modal-dialog"
                        >
                            <Modal.Header closeButton={true} closeLabel={""}>
                                <Modal.Title>Thông báo</Modal.Title>
                            </Modal.Header>
                            <Modal.Body>
                                <div className="form-default form-s">
                                    <div className="textAlign-center"><i className="ic ic-error" /> Bạn cần xác minh số điện thoại mới có thể đặt xe.</div>
                                    <div className="space m" />
                                    <div className="space m" />
                                    <a className="btn btn-primary btn--m" href="/account">Cập nhật</a>
                                </div>
                            </Modal.Body>
                        </Modal>
												<Modal
                            show={this.state.showDetailIns}
                            onHide={this.closeDetailIns}
                            dialogClassName="modal-insurance modal-dialog"
                        >
                            <Modal.Header closeButton={true} closeLabel={""}>
															<Modal.Title>Chuyến đi của bạn được bảo vệ bởi MIC</Modal.Title>
																<div className="insurance-partner">
																	<p className="lstitle">ĐỐI TÁC BẢO HIỂM</p>
																	<img className="img-fluid" src={mic} />
																</div>
                            </Modal.Header>
                            <Modal.Body>
														<div className="module-register" style={{background: 'none', padding: '0px'}}>
                  							<div className="register-container">
																	<div className="content-insurance">
																		<p>Vững tay lái với gói bảo hiểm vật chất xe ô tô từ nhà bảo hiểm MIC dành riêng cho các chuyến đi trên Mioto!</p>
																		<p>Các điều khoản bảo hiểm</p>
																		<ul>
																			<li>- Bảo hiểm vật chất xe: Đâm va, hỏa hoạn, cháy nổ</li>
																			<li>- Bảo hiểm mất cắp, mất nguyên chiếc</li>
																			<li>- Miễn phí cứu hộ tối đa 70km/vụ</li>
																		</ul>
																		<p className="ins-note">* Mức khấu trừ: mọi sự cố ngoài ý muốn trong phạm vi bảo hiểm, số tiền tối đa bạn thanh toán là 2 triệu đồng (không bao gồm chi phí nằm bãi sửa chữa nếu có, và các lí do chủ quan khác nằm ngoài phạm vi bảo hiểm)</p>
																		<div class="space m"></div>
																		<p>Nếu xe xảy ra sự cố trên đường, vui lòng gọi đến số <span className="text-primary fontWeight-6">1900 55 88 91</span>, đọc số Hợp đồng BH và làm theo hướng dẫn của tổng đài bảo hiểm MIC!</p>
																	</div>
																</div>
															</div>
                            </Modal.Body>
                        </Modal>
                        {this.state.showPromoBox && <PromoBox show={this.state.showPromoBox} onHide={this.closePromoBox.bind(this)} carId={car.id} onPromoCodeClick={this.onPromoCodeClick.bind(this)} />}
                        <CarBooking
                            show={this.state.isShowBooking}
                            hideModal={this.closeBooking}
                            car={car}
                            promoCode={promoCode}
                            owner={this.props.owner}
                            startDate={startDate}
                            endDate={endDate}
                            selfPickup={this.state.selfPickup}
                            fromAddress={this.state.fromAddress} />
                    </div>
                </div>
            </div>
        }

        return content;
    }
}

function mapFindingState(state) {
    return {
        carFinding: state.carFinding
    }
}

BookingSideBar = connect(mapFindingState)(BookingSideBar);

export default BookingSideBar;