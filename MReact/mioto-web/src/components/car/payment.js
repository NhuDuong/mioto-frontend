import React from "react"
import cookie from 'react-cookies'
import NumberFormat from "react-number-format"
import moment from 'moment'

import Header from "../common/header"
import Footer from "../common/footer"
import { commonErr } from "../common/errors"
import { getCountDownTime, convertToDateTimeText, convertToDateTimeObj } from "../common/common"
import { LoadingOverlay } from "../common/loading"
import { requestPaymentWithUrl, requestPaymentWithUrlNew, getListPaymentMethod,requestPaymentWithMyCard,  getListCard, requestAddCard } from "../../model/payment"
import { getTripDetail, reserveTrip } from "../../model/car"
import { MessageBox } from "../common/messagebox"

import logo from "../../static/images/logo.png"
import bidv from "../../static/images/BIDV.png"
import vietcombank from "../../static/images/vietcombank.png"


import card_method from "../../static/images/payments/wallet.svg"


import credit_card from "../../static/images/payments/credit-card.svg"
import visa from "../../static/images/payments/visa.svg"
import master from "../../static/images/payments/mastercard.svg"
import jcb from "../../static/images/payments/jcb.svg"


const paymentMethods = {
		MY_CARD: "MyCard",
    INTERNATIONAL_CARD: "InternationalCard",
    DOMESTIC_BANK: "DomesticBank",
    STORE: "Store",
    TRANFER: "Tranfer",
    OFFICE: "Office"
}


class MyCardMethod extends React.Component {
	constructor(props) {
		super();
		this.state = {
			err: commonErr.INNIT,
			errMsg: "",
			cards: props.cards, 
			trip: props.trip,
			activeCard: "",
		}
		this.hideMessageBox = this.hideMessageBox.bind(this)
		this.buildImgPayment = this.buildImgPayment.bind(this);
		this.onDepositBtnClick = this.onDepositBtnClick.bind(this);
		this.onAddCardBtn = this.onAddCardBtn.bind(this);
		//
	}
	componentDidMount() {
		
		if (this.state.cards && this.state.cards.length > 0) {
			this.setState({
				activeCard: this.state.cards[0]
			})
		} else {
			this.setState({
				activeCard: ""
			})
		}
	}
	componentWillReceiveProps(props) {
		this.setState({
			cards: props.cards,
			trip: props.trip
		}, () => {
            if (this.state.cards && this.state.cards.length > 0) {
                this.setState({
                    activeCard: this.state.cards[0]
                })
            } else {
                this.setState({
                    activeCard: ""
                })
            }
        })
	}

	buildImgPayment(card) {
		switch (card.cardBrand) {
			case "VISA":
				return <img src={visa} />
			case "MASTERCARD":
				return <img src={master} />
			case "JCB":
				return <img src={jcb} />
			default:
				return <img src={credit_card} />
		}
	}
	onCardChange(card) {
		this.setState({
			activeCard: card
		})
	}
	onDepositBtnClick(tripId, cardId) {
		//GA

		this.setState({
				err: commonErr.LOADING,
		});

		requestPaymentWithMyCard(tripId, cardId).then(paymentResp => {
				const { data, error, errorMessage } = paymentResp.data
				if (error >= commonErr.SUCCESS && data) {
					window.location.assign(data.payUrl);
				} else {
						this.setState({
								err: error,
								errMsg: errorMessage
						});
				}
		});
	}	

	onAddCardBtn(callbackPaths) {
		requestAddCard(callbackPaths).then(cardResp => {
			const { data, error, errorMessage } = cardResp.data
			if (error >= commonErr.SUCCESS && data) {
				window.location.assign(data.linkUrl);
			} else {
				this.setState({
					err: error,
					errMsg: errorMessage
				});
			}
		});
	}
	
	hideMessageBox() {
		this.setState({
				err: commonErr.INNIT,
				errMsg: ""
		})	
	}



	render() {
		const { trip, cards, activeCard } = this.state;
	
		const { err, errMsg } = this.state;
		var content;
		if (this.state.cards && this.state.cards.length > 0 ) {
			 content = cards.map(card => <label key={card.cardId} className="custom-radio custom-control">
			<input className="custom-control-input" type="radio" onChange={() => this.onCardChange(card)} name={`${card.cardName}_${card.cardId}_radio`} checked={activeCard.cardId === card.cardId} />
			<span className="custom-control-indicator"></span>
			<span className="custom-control-description"></span>
			<div className="box-card__item d-flex">
					<div className="logo-card">
						{this.buildImgPayment(card)}
					</div>
					<div className="info-card">
						<p className="card-number">•••• •••• •••• {card.cardNumber.slice(12)}
							{card.cardType && <span className="card-type" style={{ textTransform: "uppercase" }}>{card.cardType}</span>}
						</p>
						<p className="card-name" style={{textTransform: "uppercase"}}>{card.cardName}</p>
						<p className="valid-card">Hiệu lực thẻ: {card.cardExpiredMonth}/{card.cardExpiredYear}</p>
					</div>
				</div >
			</label>)
		} else {
			content = <div>
				<a className="btn btn--m btn-add-card" onClick={() => this.onAddCardBtn(`payment/${trip.id}?deposit=${trip.priceSummary.deposit}&timeExpired=${trip.timeExpired}`)}><i className="ic ic-plus" /> Thêm thẻ</a>
				<div className="space m"></div>
				</div>
		}
			
	
		return <div className="content-payment">
							<div className="step-by-step">
									<p className="step">1. Lựa chọn thẻ mà bạn muốn thanh toán</p>
									<div className="list-cards">
											{content}
									</div>
									<p className="step">2. Sau khi thanh toán, bạn sẽ nhận được thông báo đặt xe thành công và thông tin chủ xe qua tin nhắn và qua ứng dụng/website Mioto.</p>
							</div>
			
							{trip && this.state.cards && this.state.cards.length > 0 && <div className="summary-bill">
								<div className="summary">
									<h4>TỔNG TIỀN </h4>
									<p><NumberFormat value={trip.priceSummary.deposit} displayType={'text'} thousandSeparator={" "} decimalSeparator={","} suffix={"đ"} /></p>
								</div>
								<div className="link-payment">
									<button className="btn btn--m btn-primary btn-payment" onClick={() => this.onDepositBtnClick(trip.id, activeCard.cardId)}>THANH TOÁN</button>
								</div>
							</div>}
							{err === commonErr.LOADING && <LoadingOverlay />}
							<MessageBox show={errMsg !== ""} error={err} message={errMsg} hideModal={this.hideMessageBox} />
					</div>
	}
}

class InternationalMethod extends React.Component {
    constructor(props) {
        super();
        const { providers } = props.method;
        this.state = {
            err: commonErr.INNIT,
            errMsg: "",

            activeProvider: providers[0]
        }

        this.onProviderChange = this.onProviderChange.bind(this);
        this.hideMessageBox = this.hideMessageBox.bind(this);
        this.onDepositBtnClick = this.onDepositBtnClick.bind(this);
    }

    onProviderChange(provider) {
        this.setState({
            activeProvider: provider
        })
    }

    hideMessageBox() {
        this.setState({
            err: commonErr.INNIT,
            errMsg: ""
        })
    }

    onDepositBtnClick(tripId, linkPayment) {
        //GA
        const utmSrc = cookie.load("_utm_src") || "web_directly";
        const utmExt = cookie.load("_utm_ext") || "no_label";
        window.ga('send', 'event', utmSrc, "CAR_DEPOSIT", utmExt);

        this.setState({
            err: commonErr.LOADING,
        });

        requestPaymentWithUrlNew(linkPayment, tripId).then(paymentResp => {
            const { data, error, errorMessage } = paymentResp.data
            if (error >= commonErr.SUCCESS && data) {
                window.location.assign(data.payUrl);
            } else {
                this.setState({
                    err: error,
                    errMsg: errorMessage
                });
            }
        });
    }

    render() {
        const { trip, method } = this.props;
        const { err, errMsg, activeProvider } = this.state;
		
        return <div className="content-payment">
            <p>Trên thẻ của bạn phải có các ký hiệu Visa, Master để thanh toán được bằng hình thức này.</p>
            <div className="step-by-step">
                <p className="step">1. Lựa chọn cổng thanh toán</p>
                <div className="ccard-group">
                    {method.providers.map((provider, i) => {
                        return <label key={i} className="custom-radio custom-control">
                            <input
                                className="custom-control-input"
                                type="radio"
                                name={`${method.name}_${provider.connectId}_radio`}
                                checked={activeProvider.connectId === provider.connectId}
                                onChange={() => this.onProviderChange(provider)} />
                            <span className="custom-control-indicator"></span>
                            <span className="custom-control-description"></span>
                            <div className="img-card"><img className="img-fluid" src={provider.logo} /></div>
                            <span className="prov">{provider.name}</span>
                        </label>
                    })}
                </div>
                <p className="step">2. Bấm <strong>"THANH TOÁN"</strong> để chuyển hướng về cổng thanh toán và tiến hành đặt cọc.</p>
                <p className="step">3. Nhập thông tin trên thẻ.</p>
                <p className="step">4. Sau khi thanh toán, bạn sẽ nhận được thông báo đặt xe thành công và thông tin chủ xe qua tin nhắn và qua ứng dụng/website Mioto.</p>
            </div>
            <div className="summary-bill">
                <div className="summary">
                    <h4>TỔNG TIỀN </h4>
                    <p><NumberFormat value={trip.priceSummary.deposit} displayType={'text'} thousandSeparator={" "} decimalSeparator={","} suffix={"đ"} /></p>
                </div>
                <div className="link-payment">
                    <button className="btn btn--m btn-primary btn-payment" onClick={() => this.onDepositBtnClick(trip.id, activeProvider.linkPayment)}>THANH TOÁN</button>
                </div>
            </div>
            {err === commonErr.LOADING && <LoadingOverlay />}
            <MessageBox show={errMsg !== ""} error={err} message={errMsg} hideModal={this.hideMessageBox} />
        </div>
    }
}

class DomesticBankMethod extends React.Component {
    constructor(props) {
        super();
        const { providers } = props.method;
        this.state = {
            err: commonErr.INNIT,
            errMsg: "",

            activeProvider: providers[0]
        }

        this.onProviderChange = this.onProviderChange.bind(this);
        this.hideMessageBox = this.hideMessageBox.bind(this);
        this.onDepositBtnClick = this.onDepositBtnClick.bind(this);
    }

    onProviderChange(provider) {
        this.setState({
            activeProvider: provider
        })
    }

    hideMessageBox() {
        this.setState({
            err: commonErr.INNIT,
            errMsg: ""
        })
    }

    onDepositBtnClick(tripId, linkPayment) {
        //GA
        const utmSrc = cookie.load("_utm_src") || "web_directly";
        const utmExt = cookie.load("_utm_ext") || "no_label";
        window.ga('send', 'event', utmSrc, "CAR_DEPOSIT", utmExt);

        this.setState({
            err: commonErr.LOADING,
        });

        requestPaymentWithUrlNew(linkPayment, tripId).then(paymentResp => {
            const { data, error, errorMessage } = paymentResp.data
            if (error >= commonErr.SUCCESS && data) {
                window.location.assign(data.payUrl);
            } else {
                this.setState({
                    err: error,
                    errMsg: errorMessage
                });
            }
        });
    }

    render() {
        const { trip, method } = this.props;
        const { activeProvider, err, errMsg } = this.state;

        return <div className="content-payment">
            <p>Thẻ của bạn phải có đăng ký dịch vụ thanh toán trực tuyến với ngân hàng.</p>
            <div className="step-by-step">
                <p className="step">1. Lựa chọn cổng thanh toán</p>
                <div className="ccard-group">
                    {method.providers.map((provider, i) => {
                        return <label key={i} className="custom-radio custom-control">
                            <input
                                className="custom-control-input"
                                type="radio"
                                name={`${method.name}_${provider.connectId}_radio`}
                                checked={activeProvider.connectId === provider.connectId}
                                onChange={() => this.onProviderChange(provider)} />
                            <span className="custom-control-indicator"></span>
                            <span className="custom-control-description"></span>
                            <div className="img-card"><img className="img-fluid" src={provider.logo} /></div>
                            <span className="prov">{provider.name}</span>
                        </label>
                    })}
                </div>
                <p className="step">2. Bấm <strong>"THANH TOÁN"</strong> để chuyển hướng về cổng thanh toán và tiến hành đặt cọc.</p>
                <p className="step">3. Nhập thông tin trên thẻ.</p>
                <p className="step">4. Sau khi thanh toán, bạn sẽ nhận được thông báo đặt xe thành công và thông tin chủ xe qua tin nhắn và qua ứng dụng/website Mioto.</p>
            </div>
            <div className="summary-bill">
                <div className="summary">
                    <h4>TỔNG TIỀN </h4>
                    <p><NumberFormat value={trip.priceSummary.deposit} displayType={'text'} thousandSeparator={" "} decimalSeparator={","} suffix={"đ"} /></p>
                </div>
                <div className="link-payment">
                    <button className="btn btn--m btn-primary btn-payment" onClick={() => this.onDepositBtnClick(trip.id, activeProvider.linkPayment)}>THANH TOÁN</button>
                </div>
            </div>
            {err === commonErr.LOADING && <LoadingOverlay />}
            <MessageBox show={errMsg !== ""} error={err} message={errMsg} hideModal={this.hideMessageBox} />
        </div>
    }
}

class TranferMethod extends React.Component {
    constructor(props) {
        super();

        this.state = {
            err: commonErr.INNIT,
            errMsg: "",

            isReserved: props.trip.reserved === 1
        }

        this.hideMessageBox = this.hideMessageBox.bind(this);
        this.onReserveBtnClick = this.onReserveBtnClick.bind(this);
    }

    hideMessageBox() {
        this.setState({
            err: commonErr.INNIT,
            errMsg: ""
        })
    }

    onReserveBtnClick(params) {
        this.setState({
            err: commonErr.LOADING
        })

        reserveTrip(params).then(paymentResp => {
            const { error, errorMessage } = paymentResp.data;
            var { isReserved } = this.state;
            var errorMsg = errorMessage;
            if (error >= commonErr.SUCCESS) {
                isReserved = !isReserved;
                this.props.onReserveBtnClick(params);
                errorMsg = isReserved ? `Đặt chỗ thành công, bạn vui lòng chuyển tiền vào tài khoản Mioto trong vòng ${this.props.expiredText} kể từ lúc đặt chỗ.`: errorMessage;
            }

            this.setState({
                err: error,
                errMsg: errorMsg,

                isReserved: isReserved
            });

            this.props.countDownReserve(params.tripId);     
        })
    }

	
    render() {
        const { trip, method, expiredText } = this.props;
        const { isReserved, err, errMsg } = this.state;

        return <div className="content-payment">
            <div className="step-by-step">
                <p className="step">1. Bấm <strong>"ĐẶT CHỖ"</strong> để xác nhận lựa chọn hình thức thanh toán chuyển khoản ngân hàng.</p>
                <p className="step">2. Bạn vui lòng chuyển tiền vào tài khoản Mioto trong vòng <span> {expiredText}</span> kể từ lúc đặt chỗ.</p>
                <div className="cash-content">
                    <div className="left"> <img className="img-fluid" src={vietcombank} /></div>
                    <div className="right">
                        <p>Số tài khoản: <span> 0721-0006-52087</span></p>
                        <p>Chủ tài khoản: <span>CT CP MIOTO VIỆT NAM</span></p>
                        <p>Chi nhánh: <span>KỲ ĐỒNG - TPHCM</span></p>
                        <p>Nội dung chuyển khoản: <span>Số điện thoại đặt xe</span></p>
                    </div>
                </div>
                <p className="step">3. Chụp lại màn hình <strong> "Chuyển khoản thành công" </strong> và gửi đến <a href="https://www.messenger.com/t/mioto.vn" className="link" target="_blank">Mioto fanpage </a>hoặc <a href="mailto:contact@mioto.vn" className="link"> contact@mioto.vn </a>. Sau khi nhận được hình chụp từ bạn hoặc có tin nhắn tiền đã vào tài khoản, Mioto sẽ gửi thông báo đặt cọc thành công và thông tin chủ xe qua tin nhắn sms và ứng dụng/website Mioto.</p>
            </div>
            <div className="summary-bill">
                <div className="img-store">
                </div>
                <div className="summary-countdown"> 
                    {isReserved && <span className="countdown"><label>Thời gian giữ chỗ</label><span>{this.props.countdown}</span></span>}
                    <div className="d-flex">
                        <div className="summary">
                            <h4>TỔNG TIỀN </h4>
                            <p><NumberFormat value={trip.priceSummary.deposit} displayType={'text'} thousandSeparator={" "} decimalSeparator={","} suffix={"đ"} /></p>
                        </div>
                        <div className="link-payment ">
                            <button className={`btn btn--m ${isReserved ? "btn-secondary" : "btn-primary"}  btn-payment`} onClick={() => this.onReserveBtnClick({ tripId: trip.id, paymentMed: method.method, isCancel: isReserved })}>{isReserved ? "HỦY ĐẶT CHỖ" : "ĐẶT CHỖ"}</button>
                        </div>
                    </div>
                </div>
            </div>
            {err === commonErr.LOADING && <LoadingOverlay />}
            <MessageBox show={errMsg !== ""} error={err} message={errMsg} hideModal={this.hideMessageBox} />
        </div>
    }
}

class StoreMethod extends React.Component {
    constructor(props) {
        super();
        this.state = {
            err: commonErr.INNIT,
            errMsg: "",
            codeAtStore: props.trip.reservedCode,
            isReserved: props.trip.reserved === 1,
            countdown: props.countdown
        }

        this.hideMessageBox = this.hideMessageBox.bind(this);
        this.onReserveBtnClick = this.onReserveBtnClick.bind(this);
    }

    // componentDidMount() {
    //     const tripId = this.props.trip.tripId;
    //     if(this.props.trip.reserved === 1) {
    //         this.countDownReserve(tripId);
    //     }
    // }

    hideMessageBox() {
        this.setState({
            err: commonErr.INNIT,
            errMsg: ""
        })
    }

    onReserveBtnClick(params, linkPayment) {
        this.setState({
            err: commonErr.LOADING
        })

        requestPaymentWithUrl(linkPayment, params.tripId).then(paymentResp => {
            const { data, error, errorMessage } = paymentResp.data
            if (error >= commonErr.SUCCESS && data) {
                params.code = data.codeAtStore;
                reserveTrip(params).then(resp => {
                    const { error, errorMessage } = resp.data;
                    var { isReserved } = this.state;
                    if (error >= commonErr.SUCCESS) {
                        isReserved = !isReserved;
                        this.props.onReserveBtnClick(params);
                        const errMsg = isReserved ? `Đặt chỗ thành công, mã thanh toán của bạn là ${params.code}, vui lòng đến cửa hàng tiện lợi gần nhà để thanh toán trong vòng ${this.props.expiredText} kể từ lúc đặt chỗ.`: errorMessage
                        this.setState({
                            err: error,
                            errMsg: errMsg,
                            codeAtStore: params.code,
                            isReserved: isReserved,
                        });
                    } else {
                        this.setState({
                            err: error,
                            errMsg: errorMessage
                        });
                    }
                    this.props.countDownReserve(params.tripId);                        
                })
            
            } else {
                this.setState({
                    err: error,
                    errMsg: errorMessage
                });
            }
        });   
    }

    render() {
        const { trip, method, expiredText } = this.props;
        const { isReserved, codeAtStore, err, errMsg } = this.state;
        return <div className="content-payment">
            <div className="step-by-step">
                {isReserved ? <p className="step">1. Mã thanh toán hóa đơn Payoo: <span className="codeAtStore">{codeAtStore}</span></p> : <p className="step">1. Bấm <strong>"ĐẶT CHỖ"</strong> để nhận mã code "Thanh toán hóa đơn Payoo" (qua website và tin nhắn sms).</p>}
                <p className="step">2. Chụp / ghi lại mã code và đến thanh toán tại bất kì cửa hàng tiện lợi nào gần nhất trong vòng <span> {expiredText}</span> kể từ lúc đặt chỗ.</p>
                <p className="step">3. Sau khi thanh toán, bạn sẽ nhận được thông báo đặt xe thành công và thông tin chủ xe qua tin nhắn và qua ứng dụng/website Mioto.</p>
                <p>Bấm vào <a href="https://payoo.vn/map/public/?verify=true" target="_blank" style={{color: "#00a550", textDecoration: "underline"}}>đây</a>  để tìm cửa hàng gần nhất chấp nhận thanh toán Payoo</p>
            </div>
            <div className="summary-bill">
                <div className="img-store">
                    <a target="_blank" href="https://payoo.vn/map/public/?verify=true"><img className="img-fluid" src="https://n1-cstg.mioto.vn/m/store.png"/></a>
                </div>
                <div className="summary-countdown"> 
                 
                        
                    {isReserved && <span className="countdown"><label>Thời gian giữ chỗ</label><span>{this.props.countdown}</span></span>}
              
                    
                    <div className="d-flex">
                        <div className="summary"> 
                            <h4>TỔNG TIỀN </h4>
                            <p><NumberFormat value={trip.priceSummary.deposit} displayType={'text'} thousandSeparator={" "} decimalSeparator={","} suffix={"đ"} /> </p>
                        </div>
                        <div className="link-payment ">
                            <button className={`btn btn--m ${isReserved ? "btn-secondary" : "btn-primary"}  btn-payment`} onClick={() => this.onReserveBtnClick({ tripId: trip.id, paymentMed: method.method, isCancel: isReserved }, method.providers[0].linkPayment)}>{isReserved ? "HỦY ĐẶT CHỖ" : "ĐẶT CHỖ"}</button>
                        </div>
                    </div>
                </div>
            </div>
            {err === commonErr.LOADING && <LoadingOverlay />}
            <MessageBox show={errMsg !== ""} error={err} message={errMsg} hideModal={this.hideMessageBox} />
        </div>
    }
}

class OfficeMethod extends React.Component {
    constructor(props) {
        super();

        this.state = {
            err: commonErr.INNIT,
            errMsg: "",

            isReserved: props.trip.reserved === 1
        }

        this.hideMessageBox = this.hideMessageBox.bind(this);
        this.onReserveBtnClick = this.onReserveBtnClick.bind(this);
    }

    hideMessageBox() {
        this.setState({
            err: commonErr.INNIT,
            errMsg: ""
        })
    }

    onReserveBtnClick(params) {
        this.setState({
            err: commonErr.LOADING
        })

        reserveTrip(params).then(paymentResp => {
            const { error, errorMessage } = paymentResp.data;
            var { isReserved } = this.state;
            var errorMsg = errorMessage;
            if (error >= commonErr.SUCCESS) {
                isReserved = !isReserved;
                this.props.onReserveBtnClick(params);
                errorMsg = isReserved ? `Đặt chỗ thành công, bạn vui lòng đặt cọc trực tiếp tại văn phòng Mioto trong vòng ${this.props.expiredText} kể từ lúc đặt chỗ.`: errorMessage;
            }

            this.setState({
                err: error,
                errMsg: errorMsg,

                isReserved: isReserved
            });

            this.props.countDownReserve(params.tripId);     
        })
    }

    render() {
        const { trip, method, expiredText } = this.props;
        const { isReserved, err, errMsg } = this.state;

        return <div className="content-payment">
            <div className="step-by-step">
                <p className="step">1. Bấm <strong>"ĐẶT CHỖ"</strong> để xác nhận lựa chọn hình thức thanh toán tiền mặt tại văn phòng Mioto.</p>
                <p className="step">2. Bạn vui lòng đặt cọc trực tiếp tại văn phòng Mioto trong vòng <span>{expiredText}</span> kể từ lúc đặt chỗ.</p>
                <div className="cash-content">
                    <div className="left">
                        <div className="main-logo"> <img className="img-fluid" src={logo} /></div>
                    </div>
                    <div className="right">
                        <p>Công ty CP Mioto Việt Nam</p>
                        <p>Địa chỉ: 305/4 Lê Văn Sỹ, Phường 1, Tân Bình, TPHCM</p>
                        <p>Giờ làm việc: 9:00-17:00 (Thứ 2 - Thứ 7)</p>
                    </div>
                </div>
                <p className="step">3. Sau khi nhận được tiền đặt cọc, Mioto sẽ thông báo đặt cọc thành công và thông tin chủ xe qua tin nhắn sms và qua ứng dụng/website Mioto.</p>
            </div>
            <div className="summary-bill">
                <div className="img-store">
                </div>
                <div className="summary-countdown"> 
                    {isReserved && <span className="countdown"><label>Thời gian giữ chỗ</label><span>{this.props.countdown}</span></span>}
                    <div className="d-flex">
                        <div className="summary">
                            <h4>TỔNG TIỀN </h4>
                            <p><NumberFormat value={trip.priceSummary.deposit} displayType={'text'} thousandSeparator={" "} decimalSeparator={","} suffix={"đ"} /></p>
                        </div>
                        <div className="link-payment ">
                            <button className={`btn btn--m ${isReserved ? "btn-secondary" : "btn-primary"}  btn-payment`} onClick={() => this.onReserveBtnClick({ tripId: trip.id, paymentMed: method.method, isCancel: isReserved })}>{isReserved ? "HỦY ĐẶT CHỖ" : "ĐẶT CHỖ"}</button>
                        </div>
                    </div>
                </div>
            </div>
            {err === commonErr.LOADING && <LoadingOverlay />}
            <MessageBox show={errMsg !== ""} error={err} message={errMsg} hideModal={this.hideMessageBox} />
        </div>
    }
}

const buildMethodContent = (trip, method, onReserveBtnClick, countDownReserve, countdown, cards) => {
    var timeRemain = getCountDownTime(trip.timeExpired);
    var dateTimeObj = convertToDateTimeObj(timeRemain);
    var expiredText = (dateTimeObj.hours > 0 ? (dateTimeObj.hours + " tiếng ") : "")
                    + (dateTimeObj.minutes > 0 ? (dateTimeObj.minutes + " phút ") : "");
	switch (method.method) {
				case paymentMethods.MY_CARD:
					return <MyCardMethod trip={trip} method={method} cards={cards} />
		
        case paymentMethods.INTERNATIONAL_CARD:
            return <InternationalMethod trip={trip} method={method} />

        case paymentMethods.DOMESTIC_BANK:
            return <DomesticBankMethod trip={trip} method={method} />

        case paymentMethods.STORE:
            return <StoreMethod trip={trip} method={method} onReserveBtnClick={onReserveBtnClick} countDownReserve={countDownReserve} countdown={countdown} expiredText={getCountDownTime(trip.timeExpired)/1000 < method.data.expiredInSeconds ? expiredText : method.data.expiredText}/>

        case paymentMethods.TRANFER:
            return <TranferMethod trip={trip} method={method} onReserveBtnClick={onReserveBtnClick} countDownReserve={countDownReserve} countdown={countdown} expiredText={getCountDownTime(trip.timeExpired)/1000 < method.data.expiredInSeconds ? expiredText : method.data.expiredText}/>

        case paymentMethods.OFFICE:
            return <OfficeMethod trip={trip} method={method} onReserveBtnClick={onReserveBtnClick} countDownReserve={countDownReserve} countdown={countdown} expiredText={getCountDownTime(trip.timeExpired)/1000 < method.data.expiredInSeconds ? expiredText : method.data.expiredText}/>

        default:
            return <div className="content-payment">
                <p>Không tìm thấy phương thức thanh toán</p>
            </div>
    }
}

export default class Payment extends React.Component {
    constructor() {
        super();

        this.state = {
            step: 1,
            err: commonErr.INNIT,
            errMsg: "",

            methods: [],
						methodBoxes: [],
						
						cards: [],
						
				
        }

        this.hideMessageBox = this.hideMessageBox.bind(this);
        this.tooglePaymentMethodBox = this.tooglePaymentMethodBox.bind(this);
        this.onReserveBtnClick = this.onReserveBtnClick.bind(this);
				this.countDownReserve = this.countDownReserve.bind(this);
	

    }

    componentDidMount() {
				window.scrollTo(0, 0);

        this.setState({
            err: commonErr.LOADING
        });

        const { tripId } = this.props.match.params;
        if (!tripId) {
            this.setState({
                err: commonErr.FAIL,
                errMsg: "Không tìm thấy thông tin"
            })
        } else {
            getTripDetail(tripId).then(resp => {
                const { data, error } = resp.data;
                if (error >= commonErr.SUCCESS && data) {
                    const methodBoxes = [];
                    if (data.trip.reservedMethod) {
                        methodBoxes[data.trip.reservedMethod] = true;
                    }
                    this.setState({
                        err: error,
                        trip: data.trip,
                        methodBoxes: methodBoxes
                    });
                    getListPaymentMethod(tripId).then(pResp => {
											if (pResp.data.data) {
														var methods = pResp.data.data;
														const myCardMethods = {
															logo: card_method,
															method: "MyCard",
															name: "Thanh toán qua thẻ của tôi",
														}
														methods.unshift(myCardMethods);
                            this.setState({
                                err: pResp.error,
                                methods: methods,
                            })
                        } else {
                            this.setState({
                                err: commonErr.FAIL,
                                errMsg: "Không tìm thấy thông tin"
                            })
                        }
                    });
                    getListCard().then(myCardResp => {
                        if (myCardResp.data.error >= commonErr.SUCCESS && myCardResp.data.data.length > 0) {
                            if(this.state.trip && this.state.trip.reserved !== 1){
                                this.tooglePaymentMethodBox(paymentMethods.MY_CARD);                // auto open method MY_CARD 
                            }
                            this.setState({
                                err: myCardResp.data.error,
                                cards: myCardResp.data.data
                            });
                        } else {
                            this.setState({
                                err: myCardResp.data.error,
                                cards: null
                            })
                        }
                    });
                } else {
                    this.setState({
                        err: commonErr.FAIL,
                        errMsg: "Không tìm thấy thông tin"
                    })
                }
            })
            this.countDownReserve(tripId);
			}
    }

		// toggleMyCardBox() {
		// 	const { trip } = this.state; 
	
		// 	if (trip.reserved === 1) {
		// 		this.setState({
		// 			err: commonErr.FAIL,
		// 			errMsg: "Bạn cần HỦY ĐẶT CHỖ trước nếu muốn thay đổi phương thức thanh toán.", 
				
		// 		}) 
		// 	} else {
		// 		this.setState({
		// 			isOpenMyCard: !this.state.isOpenMyCard,
		// 		})
		// 	}
		// }
	
    tooglePaymentMethodBox(box) {
        const { methods, methodBoxes, trip, isOpenMyCard } = this.state;

        if (trip.reserved === 1 && box !== trip.reservedMethod) {
            this.setState({
                err: commonErr.FAIL,
                errMsg: "Bạn cần HỦY ĐẶT CHỖ trước nếu muốn thay đổi phương thức thanh toán."
            })
        } else {
            if(box)
                for (var i = 0; i < methods.length; ++i) {
                    if (methods[i].method !== box) {
                        methodBoxes[methods[i].method] = false;
                    }
                }

            methodBoxes[box] = !methodBoxes[box]
					
					
            this.setState({
                methodBoxes: {
                    ...methodBoxes
                }
            })
        }
    }

    hideMessageBox() {
        this.setState({
            err: commonErr.INNIT,
            errMsg: ""
        })
    }

    onReserveBtnClick(params) {
        const { isCancel, paymentMed } = params;
        const trip = {
            ...this.state.trip,
            reserved: isCancel ? 0 : 1,
            reservedMethod: paymentMed
        }
        this.setState({
					trip: trip,
					isOpenMyCard: false
        })
    }

    countDownReserve(tripId){
        getTripDetail(tripId).then(resp => {
            if (resp.data.error >= commonErr.SUCCESS && resp.data.data) {
                var reservedTimeExpired = resp.data.data.trip.reservedTimeExpired;
                if (reservedTimeExpired > 0) {
                    var countDownDate = reservedTimeExpired;

                    // Update the count down every 1 second
                    var x = setInterval(() => {

                        // Get todays date and time
                        var timeRemain = getCountDownTime(countDownDate);
                        var dateTimeObj = convertToDateTimeObj(timeRemain);
                        // Display the result in the element with id="demo"
                        this.setState({
                            countdownInterval: x,
                            countdown: (dateTimeObj.hours > 0 ? (dateTimeObj.hours + ":") : "")
                            + (dateTimeObj.minutes + ":")
                            + dateTimeObj.seconds  
                        })

                        // If the count down is finished, write some text 
                        if (timeRemain < 0) {
                            clearInterval(this.state.countdownInterval);
                            this.setState({
                                countdown: "Quá hạn"
                            })
                        }
                    }, 1000);
                } else {
                    clearInterval(this.state.countdownInterval);
                    this.setState({
                        countdown: ""
                    })
                }
            }  
        })
    }
		

	
    render() {
				var content;
	
        if (this.state.err === commonErr.LOADING) {
            content = <LoadingOverlay />
        } else {
						const { trip, methods, err, errMsg, countdown, cards } = this.state;

            content = <div className="module-register min-height">
                <div className="register-container payment-container">
                    <div className="body-promo new-payments">
										<h4 className="title textAlign-center">Chọn phương thức thanh toán</h4>
											{/* <div className="box-promo"  onClick={() => { this.toggleMyCardBox() }}>
												<div className="left">
													<img src={card_method} />
												</div>
												<div className="center center-payment">
														<p className="code">Thanh toán qua thẻ của tôi</p>
												</div>
												<div className="right right-payment"><i className={`ic ic-chevron-${isOpenMyCard ? "up" : "down"}`}></i></div>
											</div> */}
											{/* {isOpenMyCard && <MyCardMethod onAddCard={() => this.onAddCardBtn(`trip/detail/${trip.id}`)} trip={trip} cards={cards} />} */}
									
											{methods.map((method, i) => {
													const isOpen = this.state.methodBoxes[method.method];
													
													return <div key={i}>
															<div className="box-promo" onClick={() => { this.tooglePaymentMethodBox(method.method) }}>
																	<div className="left"> <img src={method.logo} /></div>
																	<div className="center center-payment">
																			<p className="code">{method.name}</p>
																	</div>
																	<div className="right right-payment"><i className={`ic ic-chevron-${isOpen ? "up" : "down"}`}> </i></div>
															</div>
															{isOpen && buildMethodContent(trip, method, this.onReserveBtnClick, this.countDownReserve, countdown, cards)}
													</div>
                        })}
                        <div className="note-payment"> 
                            <p>(*Trường hợp nhiều khách đặt xe cùng một thời điểm, hệ thống sẽ ưu tiên khách hàng thanh toán sớm nhất)</p>
                        </div>
                    </div>
                    <div className="link-manage">
                        <a className="btn btn-primary btn--m" href={`/trip/detail/${trip ? trip.id : ""}`} >Quản lý chuyến</a>
                    </div>
                    <p className="note-payment">*Gọi <a href="tel:19009217">19009217 (9AM-6PM T2-T7) </a>hoặc Chat với Mioto tại <a target="_blank" href="https://www.messenger.com/t/mioto.vn">Mioto fanpage </a>nếu bạn gặp khó khăn trong quá trình thanh toán</p>
                    <MessageBox show={errMsg !== ""} error={err} message={errMsg} hideModal={this.hideMessageBox} />
                </div>
            </div>
        }

        return <div className="mioto-layout">
            <Header />
            {content}
            <Footer />
        </div>
    }
}