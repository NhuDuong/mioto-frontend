import React from "react"
import moment from "moment"
import PlacesAutocomplete from 'react-places-autocomplete'
import cookie from "react-cookies"
import queryString from "query-string"
import { connect } from "react-redux"
import { geocodeByAddress, getLatLng } from "react-places-autocomplete"
import { Modal } from "react-bootstrap"
import DateRangePicker from 'react-bootstrap-daterangepicker'
import { isIOS } from "react-device-detect"

import Filter from "./filter"
import ListView from "./listview"
import MapView from "./mapview"
import Header from "../common/header"
import FilterSidebar from "../car/filtersidebar"
import CarDetailPopup from "./cardetailpopup"
import { commonErr } from "../common/errors"
import { LoadingInlineSmall } from "../common/loading"
import { MessageBox, MessageLine } from "../common/messagebox"
import { setFilter, searchCarList, searchCarListMore, searchCarMapV2, searchCarMapMoreV2 } from "../../actions/carFindingAct"
import { init, getFilterConfig, getVehicleType, getVehicleMake } from "../../model/car"
import { formatTitleInUrl } from "../common/common";

const viewModes = {
		LIST: 0,
		MAP: 1
}

const defPriceRange = {
		min: 500,
		max: 10000,
		step: 100
};

const defLimitKMRange = {
		min: 0,
		max: 550,
		step: 50
};

const defLimitKMPriceRange = {
		min: 0,
		max: 15,
		step: 5
};

const defDPriceRange = {
		min: 0,
		max: 50,
		step: 5
};

const defFuelCRange = {
		min: 0,
		max: 30,
		step: 5
};

const defYearRange = {
		min: 2000,
		max: moment().year().valueOf(),
		step: 5
};

const defSeatRange = {
		min: 2,
		max: 16,
		step: 2
};

const defRadiusRange = {
		min: 5,
		max: 50,
		step: 3
}

const calendarTemplate = '<div class="daterangepicker dropdown-menu">' +
		'<div class="calendar left">' +
		'<div class="daterangepicker_input hidden">' +
		'<input class="input-mini form-control" type="text" name="daterangepicker_start" value="" />' +
		'<i class="fa fa-calendar glyphicon glyphicon-calendar"></i>' +
		'<div class="calendar-time">' +
		'<div></div>' +
		'<i class="fa fa-clock-o glyphicon glyphicon-time"></i>' +
		'</div>' +
		'</div>' +
		'<div class="calendar-table"></div>' +
		'</div>' +
		'</div>' +
	'</div>';

const bookingTimes = [];
for (var i = 0; i < 1440; i = i + 30) {
		const time = i;
		const hour = (time - time % 60) / 60;
		const hourLabel = hour < 10 ? `0${hour}` : `${hour}`;
		const min = time % 60;
		const minLabel = min < 10 ? `0${min}` : `${min}`;
		bookingTimes.push({ value: time, label: `${hourLabel}:${minLabel}` });
}

const placesAutocompleteStyle = {
	autocompleteContainer: {
		position: 'absolute',
		top: '100%',
		backgroundColor: 'rgba(0, 0, 0, 0.7)',
		width: '100%',
		border: 'none',
	
	},
	autocompleteItem: {
		backgroundColor: 'rgba(0, 0, 0, 0.7)',
		padding: '10px',
		color: '#f0f0f0',
		cursor: 'pointer',
		display: 'flex', 
		justifyContent: 'flex-start',
		flexWrap: 'wrap'
	},
	autocompleteItemActive: {
			backgroundColor: '#6c6c6e',
			color: '#fffff',
	},
}

const preventDefault = e => e.preventDefault();

class CarFinding extends React.Component {
		constructor(props) {
				super(props);

				this.state = {
						err: commonErr.INNIT,
						errMsg: "",
						errMsgBox: "",
						addressPlaceHolder: "Vị trí trên bản đồ.",

						viewMode: viewModes.LIST,
						isOpenFilterBox: false,
						showAdvance: false,
						isShowPopup: false,
						popupCarId: "",
						isOpenSearching: false,
						showSearchTime: true,

						location: props.location,
						oriUrl: window.location.href
				}

				this.toogleFilterBox = this.toogleFilterBox.bind(this);
				this.setListView = this.setListView.bind(this);
				this.setMapView = this.setMapView.bind(this);
				this.setFilter = this.setFilter.bind(this);
				this.resetFilter = this.resetFilter.bind(this);
				this.searchCar = this.searchCar.bind(this);
				this.showMessageBox = this.showMessageBox.bind(this);
				this.hideMessageBox = this.hideMessageBox.bind(this);
				this.setAddressPlaceHolder = this.setAddressPlaceHolder.bind(this);
				this.tooglePopup = this.tooglePopup.bind(this);
				this.hideAdvance = this.hideAdvance.bind(this);
				this.showAdvance = this.showAdvance.bind(this);
				this.onStartTimeChange = this.onStartTimeChange.bind(this);
				this.toogleSearching = this.toggleSearching.bind(this);
			

		}

		setGAFindingEvent(lat, lng) {
				const geocoder = new window.google.maps.Geocoder();
				geocoder.geocode({
						'location': {
								lat: lat,
								lng: lng
						}
				}, (results, status) => {
						if (status === window.google.maps.GeocoderStatus.OK) {
								if (results && results.length > 0) {
										const addrComponents = results[0].address_components;
										if (addrComponents && addrComponents.length > 0) {
												for (var i = 0; i < addrComponents.length; ++i) {
														const addrComponentTypes = addrComponents[i].types;
														if (addrComponentTypes && addrComponentTypes.length > 0) {
																const addrComponentType = addrComponentTypes[0];
																if (addrComponentType === "administrative_area_level_1") {
																		const city = addrComponents[i].short_name;
																		const utmSrc = cookie.load("_utm_src") || "web_directly";
																		var action = "CAR_FINDING";
																		if (this.props.location && this.props.location.search) {
																				const query = queryString.parse(this.props.location.search);
																				if (!query.address) {
																						action = "CAR_FINDING_DEF";
																				}
																		}
																		window.ga('send', 'event', utmSrc, action, city);
																		break;
																}
														}
												}
										}
								}
						}
				})
		}

		componentDidMount() {
				var startDate;
				var endDate;
				var startTime;
				var endTime;
				var lat;
				var lng;
				var address;

				if(this.props.carFinding.filter.isOpenSearching){
					this.setState({
						isOpenSearching: this.props.carFinding.filter.isOpenSearching
					})
				}

				//overide filter by query params
				if (this.props.location && this.props.location.search) {
						const query = queryString.parse(this.props.location.search);

						if (query.startDate) {
								startDate = moment(query.startDate * 1);
								startTime = startDate.get("hour") * 60 + startDate.get("minute");
						}
						if (query.endDate) {
								endDate = moment(query.endDate * 1);
								endTime = endDate.get("hour") * 60 + endDate.get("minute");
						}
						if (query.lat) {
								lat = query.lat * 1;
						}
						if (query.lng) {
								lng = query.lng * 1;
						}
						if (query.address) {
								address = query.address;
						}

						//GA tracking
						if (query.utm_source) {
								cookie.save("_utm_src", query.utm_source, { path: '/' });
								cookie.save("_utm_src", query.utm_source, { path: '/', domain: '.mioto.vn' });
						}
						if (query.utm_ext) {
								cookie.save("_utm_ext", query.utm_ext, { path: '/' });
								cookie.save("_utm_ext", query.utm_ext, { path: '/', domain: '.mioto.vn' });
						} else {
								cookie.remove("_utm_ext", { path: '/' });
								cookie.remove("_utm_ext", { path: '/', domain: '.mioto.vn' });
						}          
				}

				//use cookies for missing params
				if (!address) {
						const cAddress = cookie.load("_maddr");
						if (cAddress) {
								address = cAddress;
						}
				}

				// check valid cookie before set
				if(cookie.load("_mstartts") < moment().add(2, "hours").valueOf() || cookie.load("_mendts") < moment().add(2, "hours").valueOf()){
						cookie.remove("_mstartts", { path: '/' });
						cookie.remove("_mendts", { path: '/' });
				}

				const cStartTs = cookie.load("_mstartts");
				if (cStartTs) {
					const cStartDate = moment(cStartTs * 1);
						startDate = cStartDate;
						startTime = startDate.get("hour") * 60 + startDate.get("minute");
				}

				const cEndTs = cookie.load("_mendts");
				if (cEndTs) {
						endDate = moment(cEndTs * 1);
						endTime = endDate.get("hour") * 60 + endDate.get("minute");
				}
						

				if (address && address !== "") {
						if (lat && lng) {
								if (startDate && endDate) {
										const filter = {
												...this.props.carFinding.filter,
												address: address,
												lat: lat,
												lng: lng,
												startDate: startDate,
												endDate: endDate,
												startTime: startTime,
												endTime: endTime
										}
										this.setState({
												startDate: startDate,
												endDate: endDate,
												startTime: startTime,
												endTime: endTime,
												baseFilter: { ...filter }
										});
										this.setFilter(filter);
										this.searchCar(filter);
								} else {
										init().then(resp => {
												const timeInit = resp.data.data;
												if (timeInit) {
														const filter = {
																...this.props.carFinding.filter,
																address: address,
																lat: lat,
																lng: lng,
																startDate: moment(timeInit.startTime),
																endDate: moment(timeInit.endTime),
																startTime: moment(timeInit.startTime).get("hour") * 60 + moment(timeInit.startTime).get("minute"),
																endTime: moment(timeInit.endTime).get("hour") * 60 + moment(timeInit.endTime).get("minute"),
														}
														this.setState({
																baseFilter: { ...filter },
																addressBanner: address != "" && address
														});
														this.setFilter(filter);
														this.searchCar(filter);
												} else {
														this.setState({
																err: commonErr.FAIL,
																errMsg: "Thời gian tìm kiếm không hợp lệ."
														});
												}
										})
								}
						} else {
								geocodeByAddress(address)
										.then(results => getLatLng(results[0]))
										.then(latLng => {
												lat = latLng.lat;
												lng = latLng.lng;

												if (startDate && endDate) {
														const filter = {
																...this.props.carFinding.filter,
																address: address,
																lat: lat,
																lng: lng,
																startDate: startDate,
																endDate: endDate
														}
														this.setState({
																baseFilter: { ...filter },
																addressBanner: address != "" && address,
																startDate: startDate,
																endDate: endDate,
																startTime: startTime,
																endTime: endTime,
														});
														this.setFilter(filter);
														this.searchCar(filter);
												} else {
														init().then(resp => {
																const timeInit = resp.data.data;
																if (timeInit) {
																		const filter = {
																				...this.props.carFinding.filter,
																				address: address,
																				lat: lat,
																				lng: lng,
																				startDate: moment(timeInit.startTime),
																				endDate: moment(timeInit.endTime)
																		}
																		this.setState({
																				baseFilter: { ...filter },
																				addressBanner: address != "" && address
																		});
																		this.setFilter(filter);
																		this.searchCar(filter);
																} else {
																		this.setState({
																				err: commonErr.FAIL,
																				errMsg: "Thời gian tìm kiếm không hợp lệ."
																		});
																}
														})
												}
										})
										.catch(error => {
												this.setState({
														err: commonErr.FAIL,
														errMsg: "Địa chỉ tìm kiếm không hợp lệ.",
														startDate: startDate,
														endDate: endDate,
														startTime: startTime,
														endTime: endTime
												});
										});
						}
				} else {
						this.setState({
								err: commonErr.FAIL,
								errMsg: "Địa chỉ tìm kiếm không hợp lệ."
						});
				}

				getFilterConfig().then(resp => {
						if (resp.data.data) {
								this.setState({
										customFilterConfig: resp.data.data
								});
						}
				});

				getVehicleType().then(resp => {
						if (resp.data.data) {
								this.setState({
										vehicleTypes: resp.data.data.vehicleTypes
								});
						}
				});

				getVehicleMake().then(resp => {
						if (resp.data.data) {
								this.setState({
										vehicleMakes: resp.data.data.vehicleMakes,
								});
						}
				});
				window.addEventListener("resize", this.updateDimensions.bind(this));
				
				this.hideMainHeader();
		}

		componentWillReceiveProps(props) {
			this.setState({
				isOpenSearching: props.carFinding.filter.isOpenSearching
			}, () => {
				this.checkTouchMove();				
			})
		}

		checkTouchMove() {
			if (this.state.isOpenSearching) {
				window.addEventListener('touchmove', preventDefault, {
					passive: false
			 	});
			} else {
				window.removeEventListener('touchmove', preventDefault, {
					passive: false
			 	})
			}
		}
			//common
		updateDimensions() {
				this.hideMainHeader();
		}
	
		hideMainHeader() {
			if (window.innerWidth <= 991) {
					this.setState({
							hideHeader: true,
					});
			} else {
				this.setState({
					hideHeader: false
				})
			}
		}
	
		setListView() {
				this.setState({
						viewMode: viewModes.LIST
				});
				this.searchCarList(this.props.carFinding.filter);
		}

		setMapView() {
				this.setState({
						viewMode: viewModes.MAP
				});
		}

		toogleFilterBox() {
				this.setState({
						isOpenFilterBox: !this.state.isOpenFilterBox
				});
		}

		setFilter(filter) {
				this.props.dispatch(setFilter(filter));
		}

		resetFilter() {
				const { startDate, endDate, address, lat, lng, minMiles } = this.props.carFinding.filter;
				const filter = {
						...this.state.baseFilter,
						startDate: startDate,
						endDate: endDate,
						address: address,
						lat: lat,
						lng: lng,
						minMiles: minMiles,
				}
				
				this.setFilter(filter);
				const allFeatures = this.state.customFilterConfig.allFeatures;
				
				for (var i = 0; i < allFeatures.length; ++i) {
						allFeatures[i].checked = false;
				}
			this.searchCar(filter);
		
		}

		searchCarList(f) {
				const filter = this.standarizeFilter(f, viewModes.LIST);

				this.props.dispatch(searchCarList(filter));
		}

		searchCarListMore(f) {
				const filter = this.standarizeFilter(f, viewModes.LIST);
				this.props.dispatch(searchCarListMore(filter));
		}

		searchCarMap(f) {
				const filter = this.standarizeFilter(f, viewModes.MAP);
				// this.props.dispatch(searchCarMap(filter));
				this.props.dispatch(searchCarMapV2(filter));
		}

		searchCarMapMore(f) {
				const filter = this.standarizeFilter(f, viewModes.MAP);
				// this.props.dispatch(searchCarMapMore(filter));
				this.props.dispatch(searchCarMapMoreV2(filter));
		}

		standarizeFilter(filter, viewMode) {
				const customFilterConfig = this.state.customFilterConfig;

				const maxPrice = (customFilterConfig && customFilterConfig.maxPrice) ? customFilterConfig.maxPrice / 1000 : defPriceRange.max;
				const minLimitKM = (customFilterConfig && customFilterConfig.minLimitKM) ? customFilterConfig.minLimitKM : defLimitKMRange.min;
				const maxLimitKM = (customFilterConfig && customFilterConfig.maxLimitKM) ? customFilterConfig.maxLimitKM + 50 : defLimitKMRange.max;
				const maxLimitKMPrice = (customFilterConfig && customFilterConfig.maxLimitKMPrice) ? customFilterConfig.maxLimitKMPrice / 1000 + 5 : defLimitKMPriceRange.max;
				const maxDPrice = (customFilterConfig && customFilterConfig.maxDPrice) ? customFilterConfig.maxDPrice / 1000 : defDPriceRange.max;
				const minFuelC = (customFilterConfig && customFilterConfig.minFuelC) ? customFilterConfig.minFuelC : defFuelCRange.min;
				const maxFuelC = (customFilterConfig && customFilterConfig.maxFuelC) ? customFilterConfig.maxFuelC : defFuelCRange.max;
				const minSeat = (customFilterConfig && customFilterConfig.seatMin) ? customFilterConfig.seatMin : defSeatRange.min;
				const maxSeat = (customFilterConfig && customFilterConfig.seatMax) ? customFilterConfig.seatMax : defSeatRange.max;
				const minYear = (customFilterConfig && customFilterConfig.yearMin) ? customFilterConfig.yearMin : defYearRange.min;
				const maxYear = (customFilterConfig && customFilterConfig.yearMax) ? customFilterConfig.yearMax : defYearRange.max;
				const maxRadius = (customFilterConfig && customFilterConfig.maxRadius) ? customFilterConfig.maxRadius : defRadiusRange.max;

				return {
						...filter,
						maxPrice: (filter.maxPrice < maxPrice) ? filter.maxPrice : 0,
						dPrice: (filter.directDelivery && filter.dPrice < maxDPrice) ? filter.dPrice : -1,
						limitKM: (filter.limitKM < maxLimitKM) ? filter.limitKM : -1,
						limitKMPrice: (filter.limitKM > minLimitKM && filter.limitKMPrice < maxLimitKMPrice) ? filter.limitKMPrice : -1,
						fuelC: (filter.fuelC > minFuelC && filter.fuelC < maxFuelC) ? filter.fuelC : 0,
						minSeat: (filter.minSeat > minSeat && filter.minSeat < maxSeat) ? filter.minSeat : 0,
						maxSeat: (filter.maxSeat > minSeat && filter.maxSeat < maxSeat) ? filter.maxSeat : 0,
						minYear: (filter.minYear > minYear && filter.minYear < maxYear) ? filter.minYear : 0,
						maxYear: (filter.maxYear > minYear && filter.maxYear < maxYear) ? filter.maxYear : 0,
						radius: (viewMode === viewModes.LIST && filter.radius * 1.60934 >= maxRadius) ? 0 : filter.radius
				}
		}

		searchCar(filter) {
				//GA finding event
				// this.setGAFindingEvent(filter.lat, filter.lng);

				//save search params cookies
				cookie.save("_maddr", filter.address, { path: '/' });
				cookie.save("_mstartts", filter.startDate.valueOf(), { path: '/' });
				cookie.save("_mendts", filter.endDate.valueOf(), { path: '/' });

				if (this.state.viewMode === viewModes.MAP) {
						this.searchCarMap(filter);
				} else {
						this.searchCarList(filter);
				}
		}

		setAddressPlaceHolder(msg) {
				this.setState({
						addressPlaceHolder: msg
				});
		}

		onAddressChange(address) {
				const filter = {
						...this.props.carFinding.filter,
						address: address,
						isOpenSearching: true				
				}
				this.setFilter(filter);
		}

		onAddressFocus(e) {
				this.setState({
						addressPlaceHolder: "Nhập địa chỉ bạn muốn tìm kiếm."
				});
				const filter = {
						...this.props.carFinding.filter,
						address: "",
						isOpenSearching: true
				}
				this.setFilter(filter);
		}

		onSelectFromAddr(address) {
				if (address.err < 0) {
						var errMsg = "";
						if (address.err === -200) {
								errMsg = "Trình duyệt không được cho phép truy cập vị trí hiện tại. Vui lòng cài đặt cho phép hoặc nhập địa chỉ.";
						} else {
								errMsg = "Không thể tìm thấy vị trí hiện tại của thiết bị. Vui lòng thử lại hoặc nhập địa chỉ.";
						}
						this.setState({
								err: commonErr.FAIL,
								errMsg: errMsg
						});
						this.showMessageBox(errMsg);
				} else if (address.err === commonErr.LOADING) {
						this.setState({
								err: commonErr.LOADING,
						});
				} else {
						this.setState({
								err: commonErr.LOADING,
						});

						geocodeByAddress(address.address)
								.then(results => getLatLng(results[0]))
								.then(latLng => {
										this.setState({
												err: commonErr.SUCCESS,
												addressBanner: address.address
										});

										const filter = {
												...this.props.carFinding.filter,
												address: address.address,
												lat: latLng.lat,
												lng: latLng.lng,
												isOpenSearching: true
										}

										this.setFilter(filter);
										this.searchCar(filter);
								}).catch(error => {
										this.setState({
												err: commonErr.FAIL,
												errMsg: "Địa chỉ không hợp lệ. " + error
										});
								})
				}
		}

		onStartDateChange(event, picker) {
				event.preventDefault();
				if(picker.startDate){
						const startDate = picker.startDate;
						var endDate = this.state.endDate ? this.state.endDate : this.props.carFinding.filter.endDate;
						var endTime = endDate.get("hour") * 60 + endDate.get("minute");
						var hour = this.state.startTime ? (this.state.startTime -  this.state.startTime % 60) / 60 : (this.props.carFinding.filter.startTime -  this.props.carFinding.filter.startTime % 60) / 60;
						var minute = this.state.startTime ? this.state.startTime % 60 : this.props.carFinding.filter.startTime % 60;
						startDate.set({
								hour: hour,
								minute: minute,
								second: 0,
								milliseconds: 0
						});

						if (endDate <= startDate) {
								endDate = startDate.clone();
								endDate.add(23, "hour");
								endTime = moment(endDate).get("hour") * 60 + moment(endDate).get("minute");
						}

						//save cookies
						cookie.save("_mstartts", startDate.valueOf(), { path: '/' });
						cookie.save("_mendts", endDate.valueOf(), { path: '/' });
						
						this.setState({
										startDate: startDate,
										endDate: endDate,
										endTime: endTime,
						})
						
						this.toogleSearchTime();					
						const filter = {
								...this.props.carFinding.filter,
								startDate: startDate,
								endDate: endDate,
								isOpenSearching: true
						}

						this.setFilter(filter);
						this.toogleSearchTime();
						this.searchCar(filter);
				}				
		}

		onEndDateChange(event, picker) {
				event.preventDefault();
				if(picker.startDate && picker.endDate){
						const startDate = picker.startDate;
						startDate.set({
								hour: (this.state.startTime -  this.state.startTime % 60) / 60,
								minute: this.state.startTime % 60,
								second: 0,
								milliseconds: 0
						});
						const endDate = picker.endDate;
						endDate.set({
								hour: (this.state.endTime -  this.state.endTime % 60) / 60,
								minute: this.state.endTime % 60,
								second: 0,
								milliseconds: 0
						});
						if (endDate < startDate) {
										return;
						}

						//save cookies
						cookie.save("_mstartts", startDate.valueOf(), { path: '/' });
						cookie.save("_mendts", endDate.valueOf(), { path: '/' });

						this.setState({
										startDate: startDate,
										endDate: endDate
						})
						
						const filter = {
								...this.props.carFinding.filter,
								startDate: startDate,
								endDate: endDate,
								isOpenSearching: true
						}
		
						this.setFilter(filter);
						this.searchCar(filter);
				}		
		}

		onDateRangeChange(event, picker) {
				event.preventDefault();
				const startDate = picker.startDate.startOf("minute");
				const endDate = picker.endDate.startOf("minute");

				if (startDate > endDate
						|| (startDate.isSame(this.props.carFinding.filter.startDate) && endDate.isSame(this.props.carFinding.filter.endDate))) {
						return;
				}

				const filter = {
						...this.props.carFinding.filter,
						startDate: startDate,
						endDate: endDate
				}

				this.setFilter(filter);
				this.searchCar(filter);
		}

		onStartTimeChange(event) {
				const time = event.target.value;
				const hour = (time - time % 60) / 60;
				const min = time % 60;
				var startDate = moment(this.state.startDate ? this.state.startDate : this.this.props.carFinding.filter.startDate).set({
						hour: hour,
						minute: min,
						second: 0,
						milliseconds: 0
				});

				var endDate = this.state.endDate ? this.state.endDate : this.props.carFinding.filter.endDate;
				if (endDate && endDate.isValid() && endDate <= startDate) {
						endDate = startDate.clone().add(23, "hour");						
				}

				cookie.save("_mstartts", startDate.valueOf(), { path: '/' });
				cookie.save("_mendts", endDate.valueOf(), { path: '/' });

				this.setState({
					startDate: startDate,
					startTime: startDate.get("hour") * 60 + startDate.get("minute"),
					endDate: endDate,								
					endTime: endDate.get("hour") * 60 + endDate.get("minute"),
					err: commonErr.SUCCESS,
					errMsg: ""
				});

				const filter = {
						...this.props.carFinding.filter,
						startDate: startDate,
						endDate: endDate,
						isOpenSearching: true
				}

				this.setFilter(filter);
				this.searchCar(filter);
		}

		onEndTimeChange(event) {
				const time = event.target.value;
				const hour = (time - time % 60) / 60;
				const min = time % 60;
				var endDate = moment(this.state.endDate ? this.state.endDate : this.props.carFinding.filter.endDate).set({
					hour: hour,
					minute: min,
					second: 0,
					milliseconds: 0
				});

				var startDate = this.state.startDate ? this.state.startDate : this.props.carFinding.filter.starstartDatetTime;
				if (startDate && startDate.isValid() && endDate <= startDate) {
					startDate = endDate.clone().add(-23, "hour");
				}

				cookie.save("_mstartts", startDate.valueOf(), { path: '/' });
				cookie.save("_mendts", endDate.valueOf(), { path: '/' });

				this.setState({
					startDate: startDate,
					startTime: startDate.get("hour") * 60 + startDate.get("minute"),
					endDate: endDate,								
					endTime: endDate.get("hour") * 60 + endDate.get("minute"),
					err: commonErr.SUCCESS,
					errMsg: ""
				});

				const filter = {
						...this.props.carFinding.filter,
						startDate: startDate,
						endDate: endDate,
						isOpenSearching: true
				}

				this.setFilter(filter);
				this.searchCar(filter);
		}

		showMessageBox(msg) {
				this.setState({
						errMsgBox: msg
				});
		}

		hideMessageBox() {
				this.setState({
						errMsgBox: ""
				});
		}

		tooglePopup(car, isUpdate) {
				if (isUpdate === true) {
						window.history.pushState("", "", `/car/${formatTitleInUrl(car.name)}/${car.id}`);
						this.setState({
								popupCarId: car.id
						});
				} else {
						if (!this.state.isShowPopup) {
								window.history.pushState("", "", `/car/${formatTitleInUrl(car.name)}/${car.id}`);
						} else {
								window.history.pushState("", "", this.state.oriUrl);
						}
						this.setState({
								isShowPopup: !this.state.isShowPopup,
								popupCarId: car.id
						});
				}
		}

		showAdvance() {
				this.setState({
						showAdvance: true
				});
		}

		hideAdvance() {
				this.setState({
						showAdvance: false
				});
		}

		hasFilter(filter, customFilterConfig) {
				if(filter === undefined || customFilterConfig === undefined) {
						return false;
				}
				const maxPrice = (customFilterConfig && customFilterConfig.maxPrice) ? customFilterConfig.maxPrice / 1000 : defPriceRange.max;
				const maxLimitKM = (customFilterConfig && customFilterConfig.maxLimitKM) ? customFilterConfig.maxLimitKM + 50 : defLimitKMRange.max;
				const maxLimitKMPrice = (customFilterConfig && customFilterConfig.maxLimitKMPrice) ? customFilterConfig.maxLimitKMPrice / 1000 + 5 : defLimitKMPriceRange.max;
				const maxDPrice = (customFilterConfig && customFilterConfig.maxDPrice) ? customFilterConfig.maxDPrice / 1000 : defDPriceRange.max;
				const minFuelC = (customFilterConfig && customFilterConfig.minFuelC) ? customFilterConfig.minFuelC : defFuelCRange.min;
				const minSeat = (customFilterConfig && customFilterConfig.seatMin) ? customFilterConfig.seatMin : defSeatRange.min;
				const maxSeat = (customFilterConfig && customFilterConfig.seatMax) ? customFilterConfig.seatMax : defSeatRange.max;
				const minYear = (customFilterConfig && customFilterConfig.yearMin) ? customFilterConfig.yearMin : defYearRange.min;
				const maxYear = (customFilterConfig && customFilterConfig.yearMax) ? customFilterConfig.yearMax : defYearRange.max;
				const maxRadius = (customFilterConfig && customFilterConfig.maxRadius) ? customFilterConfig.maxRadius : defRadiusRange.max;
				const sortValue = (customFilterConfig && customFilterConfig.sort) ? customFilterConfig.sort : "op";

				return filter.maxPrice < maxPrice
						|| filter.vehicleType != "0" || filter.vehicleMake != "0" || filter.vehicleModel != "0"
						|| filter.minSeat > minSeat || filter.maxSeat < maxSeat
						|| filter.minYear > minYear || filter.maxYear < maxYear
						|| filter.instantBooking === true || filter.directDelivery === true
						|| filter.dPrice < maxDPrice
						|| filter.transmission != "0"
						|| filter.features.length > 0
						|| filter.fuel != "0" || filter.fuelC > minFuelC
						|| filter.limitKM < maxLimitKM
						|| filter.limitKMPrice < maxLimitKMPrice
						|| filter.radius * 1.60934 < maxRadius
						|| filter.pVerified === true
						|| filter.sort != sortValue[0].id
	}
	
		toggleSearching() {
			if(this.state.isOpenSearching){
				if(this.props.carFinding.filter.address == ""){
					const filter = {
						...this.props.carFinding.filter,
						address: this.state.addressBanner,
						isOpenSearching: false
					}
					this.setFilter(filter);
				} 
			}
			
			const filter = {
				...this.props.carFinding.filter,
				isOpenSearching: !this.state.isOpenSearching
			}
			this.setFilter(filter);

			this.setState({
				isOpenSearching: !this.state.isOpenSearching,
			}, () => {
				this.checkTouchMove();	
				// this.checkOpenSearchBox();			
			});
		}

		toogleSearchTime() {
			this.setState({
				showSearchTime: !this.state.showSearchTime,
			})
		}
		
		checkOpenSearchBox(){
			const filter = {
				...this.props.carFinding.filter,
				isOpenSearching: this.state.isOpenSearching
			}
			this.setFilter(filter);
		}
	
		render() {
				var content;
				if (this.state.err >= commonErr.SUCCESS) {
						if (this.state.viewMode === viewModes.MAP) {
								content = <MapView searchCarMapMore={this.searchCarMapMore.bind(this)} setAddressPlaceHolder={this.setAddressPlaceHolder} tooglePopup={this.tooglePopup} />
						} else {
								content = <ListView searchCarListMore={this.searchCarListMore.bind(this)} tooglePopup={this.tooglePopup} />
						}
				} else {
						content = <div className="module-map">
								<div className="has-list">
										<div className="map-container">
												<MessageLine message={this.state.errMsg !== "" ? this.state.errMsg : "Đã có lỗi xảy ra. Vui lòng thử lại."} />
										</div>
								</div>
						</div>
				}
				return <div className="mioto-layout bg-gray">
								{!this.state.hideHeader && <Header />}
								<section className="body has-filter no-pd-on-mobile">
								<div className="header lg-header-search show-on-lg-and-down">
									<div className="header-container">
										<div className="logo-container"><a href="/">Mioto</a></div>
										<div className="location-datetime">
											<div id="searchingBox" onClick={this.toogleSearching}>
												<h4 className="location fontWeight-5">{this.props.carFinding.filter.address !== "" ? this.props.carFinding.filter.address : (this.state.addressPlaceHolder.includes("Nhập địa chỉ") ? this.state.addressBanner : "Vị trí trên bản đồ.")}</h4>
												<span className="datetime">{`${this.props.carFinding.filter.startDate.format("DD/MM/YYYY HH:mm")} - ${this.props.carFinding.filter.endDate.format("DD/MM/YYYY HH:mm")}`}</span><i className="i-chevron-down" />
											</div>
									{this.state.isOpenSearching && <div>
											<div className="searching-backdrop" onClick={this.toogleSearching}/>
											<div className="searching-box" id="home-box">
												<div className="form-default form-finding">
													<div className="line-form">
														<label className="label">Địa điểm</label>
														<div className="wrap-input home-datetimerange">
																<PlacesAutocomplete
																		classNames={{ input: (!this.props.carFinding.filter.address || this.props.carFinding.filter.address === "") ? "" : "" }}
																		inputProps={{
																				value: this.props.carFinding.filter.address,
																				onChange: this.onAddressChange.bind(this),
																				onFocus: this.onAddressFocus.bind(this),
																				placeholder: this.state.addressPlaceHolder
																		}}
																		styles={placesAutocompleteStyle}
																		onSelect={this.onSelectFromAddr.bind(this)}
																		isUseCurrentLocation={true}
																		googleLogo={false}
																		options={{ componentRestrictions: { country: ['vn'] } }} />
														</div>
													</div>
													<div className="line-form has-timer">
														<label className="label">Bắt đầu</label>
																<div className="d-flex">
																<div className="wrap-input home-datetimerange">
																		<DateRangePicker
																				startDate={this.state.startDate ? this.state.startDate : this.props.carFinding.filter.startDate}
																				endDate={this.state.startDate ? this.state.startDate : this.props.carFinding.filter.startDate}
																				minDate={moment()}
																				maxDate={moment().add(3, "months")}
																				timePicker={false}
																				singleDatePicker={true}
																				singleSelect={true}
																				autoApply={true}
																				isIOs={isIOS}
																				timePickerIncrement={30}
																				linkedCalendars={false}
																				timePicker24Hour={true}
																				onHide={this.onStartDateChange.bind(this)}
																				buttonClasses={["hidden"]}
																				opens={"center"}
																				template={calendarTemplate}
																				parentEl={"#home-box"}>
																				<span className="value placeholder">{this.state.startDate ? this.state.startDate.format("DD/MM/YYYY") : (this.props.carFinding.filter.startDate ? this.props.carFinding.filter.startDate.format("DD/MM/YYYY") : "Chọn ngày")} <i className="i-chevron-down"></i></span>
																		</DateRangePicker>
																</div>
																<div className="wrap-select home-select">
																		<select onChange={this.onStartTimeChange.bind(this)} value={this.state.startTime ? this.state.startTime : this.props.carFinding.filter.startDate.get("hour") * 60 + this.props.carFinding.filter.startDate.get("minute")}>
																				{bookingTimes.map(time => <option key={`s_${time.value}`} value={time.value}>{time.label} </option>)}
																		</select>
																	
																</div>		
														</div>
													</div>
													<div className="line-form has-timer">
														<label className="label">Kết thúc</label>
														<div className="d-flex">
																<div className="wrap-input home-datetimerange">
																		<DateRangePicker
																						startDate={this.props.carFinding.filter.startDate}
																						endDate={this.props.carFinding.filter.endDate}
																						minDate={moment(this.props.carFinding.filter.startDate.valueOf())}
																						maxDate={moment().add(3, "months")}
																						timePicker={false}
																						singleDateRangePicker={true}
																						singleSelect={true}
																						autoApply={true}
																						isIOs={isIOS}
																						timePickerIncrement={30}
																						linkedCalendars={false}
																						timePicker24Hour={true}
																						pickingEndDateOnly={true}
																						onHide={this.onEndDateChange.bind(this)}
																						buttonClasses={["hidden"]}
																						opens={"center"}
																						template={calendarTemplate}
																						parentEl={"#home-box"}>
																						<span className="value placeholder">{this.state.endDate ? this.state.endDate.format("DD/MM/YYYY") : (this.props.carFinding.filter.endDate ? this.props.carFinding.filter.endDate.format("DD/MM/YYYY") : "Chọn ngày")} <i className="i-chevron-down"></i></span>
																				</DateRangePicker>
																</div>		
																<div className="wrap-select home-select">
																		<select onChange={this.onEndTimeChange.bind(this)} value={this.state.endTime ? this.state.endTime : this.props.carFinding.filter.endDate.get("hour") * 60 + this.props.carFinding.filter.endDate.get("minute")}>
																				{bookingTimes.map(time => <option key={`e_${time.value}`} value={time.value}>{time.label} </option>)}
																		</select>
																	
																</div>	
														</div>
													</div>
													<div className="wrap-btn"><a onClick={this.toogleSearching} className="btn btn--m btn-primary">Tìm xe ngay</a></div>
												</div>
											</div>
										</div>}
										</div>
									</div>
								</div>

								<div className="finding-control hide-on-lg form-default" id="calendar-default">
										<div className="wrapper-find" id="car-finding">
												<div className="tab-mode">
														<ul>
																<li>
																		<a className={this.state.viewMode === viewModes.LIST ? "active" : "deactive"} onClick={this.setListView.bind(this)}><i className={this.state.viewMode === viewModes.LIST ? "ic ic-list-fill" : "ic ic-list"}></i> Danh sách</a>
																</li>
																<li>
																		<a className={this.state.viewMode === viewModes.MAP ? "active" : "deactive"} onClick={this.setMapView.bind(this)}><i className={this.state.viewMode === viewModes.MAP ? "ic ic-map-fill" : "ic ic-map"}></i> Bản đồ</a>
																</li>
														</ul>
												</div>
												<div className="line-form location">
														<label className="label">Địa điểm</label>
														{this.state.err === commonErr.LOADING && <div className="wrap-input wrap-input-transparent">
																<LoadingInlineSmall />
														</div>}
														<div className={`wrap-input wrap-input-transparent ${this.state.err === commonErr.LOADING ? "hidden" : ""}`}>
																<PlacesAutocomplete
																		classNames={{ input: (!this.props.carFinding.filter.address || this.props.carFinding.filter.address === "") ? "" : "" }}
																		inputProps={{
																				value: this.props.carFinding.filter.address,
																				onChange: this.onAddressChange.bind(this),
																				onFocus: this.onAddressFocus.bind(this),
																				placeholder: this.state.addressPlaceHolder
																		}}
																		onSelect={this.onSelectFromAddr.bind(this)}
																		isUseCurrentLocation={true}
																		googleLogo={false}
																		options={{ componentRestrictions: { country: ['vn'] } }} />
														</div>
												</div>
												{this.state.showSearchTime && <div className="line-form has-timer">
														<label className="label">Bắt đầu</label>
														<div className="wrap-input wrap-input-transparent">
																		<DateRangePicker
																				startDate={this.state.startDate ? this.state.startDate : this.props.carFinding.filter.startDate}
																				endDate={this.state.startDate ? this.state.startDate : this.props.carFinding.filter.startDate}
																				minDate={moment()}
																				maxDate={moment().add(3, "months")}
																				timePicker={false}
																				singleDatePicker={true}
																				singleSelect={true}
																				autoApply={true}
																				isIOs={isIOS}
																				timePickerIncrement={30}
																				linkedCalendars={false}
																				timePicker24Hour={true}
																				onHide={this.onStartDateChange.bind(this)}
																				buttonClasses={["hidden"]}
																				opens={"center"}
																				template={calendarTemplate}
																				parentEl={"#calendar-default"}>
																				<span className="value placeholder">{this.state.startDate ? this.state.startDate.format("DD/MM/YYYY") : (this.props.carFinding.filter.startDate ? this.props.carFinding.filter.startDate.format("DD/MM/YYYY") : "Chọn ngày")} <i className="i-chevron-down"></i></span>
																		</DateRangePicker>
																</div>
														<div className="wrap-select">
															<select onChange={this.onStartTimeChange.bind(this)} value={this.state.startTime ? this.state.startTime : this.props.carFinding.filter.startDate.get("hour") * 60 + this.props.carFinding.filter.startDate.get("minute")}>
																	{bookingTimes.map(time => <option key={`s_${time.value}`} value={time.value}>{time.label} </option>)}
															</select>
										
														</div>		
												</div>}
												{this.state.showSearchTime && <div className="line-form has-timer">
														<label className="label">Kết thúc</label>
														<div className="wrap-input wrap-input-transparent">
																	<DateRangePicker
																		startDate={this.props.carFinding.filter.startDate}
																		endDate={this.props.carFinding.filter.endDate}
																		minDate={moment(this.props.carFinding.filter.startDate.valueOf())}
																		maxDate={moment().add(3, "months")}
																		timePicker={false}
																		singleDateRangePicker={true}
																		singleSelect={true}
																		autoApply={true}
																		isIOs={isIOS}
																		timePickerIncrement={30}
																		linkedCalendars={false}
																		timePicker24Hour={true}
																		pickingEndDateOnly={true}
																		onHide={this.onEndDateChange.bind(this)}
																		buttonClasses={["hidden"]}
																		opens={"center"}
																		template={calendarTemplate}
																		parentEl={"#calendar-default"}>
																		<span className="value placeholder">{this.state.endDate ? this.state.endDate.format("DD/MM/YYYY") : (this.props.carFinding.filter.endDate ? this.props.carFinding.filter.endDate.format("DD/MM/YYYY") : "Chọn ngày")} <i class="i-chevron-down"></i></span>
																</DateRangePicker>
														</div>
														<div className="wrap-select">
															<select onChange={this.onEndTimeChange.bind(this)} value={this.state.endTime ? this.state.endTime : this.props.carFinding.filter.endDate.get("hour") * 60 + this.props.carFinding.filter.endDate.get("minute")}>
																{bookingTimes.map(time => <option key={`e_${time.value}`} value={time.value}>{time.label} </option>)}
															</select>
												
														</div>		
												</div>}
												{/* <div className="switch-search show-on-small">
														<DateRangePicker
																startDate={this.props.carFinding.filter.startDate}
																endDate={this.props.carFinding.filter.endDate}
																minDate={moment()}
																maxDate={moment().add(3, "months")}
																timePicker={true}
																autoApply={true}
																buttonClasses={["hidden"]}
																isIOs={isIOS}
																timePickerIncrement={30}
																opens={"left"}
																linkedCalendars={true}
																timePicker24Hour={true}
																parentEl={"#car-finding"}
																onHide={this.onDateRangeChange.bind(this)}>
																<i className="ic ic-datepicker" />
														</DateRangePicker>
												</div> */}
										</div>
								</div>
								<div className="tab-mode show-on-lg">
									<div className="tab-box">
										{this.state.viewMode === viewModes.LIST && <a className="view-mode" onClick={this.setMapView.bind(this)}><i className="ic ic-map-black"></i> Bản đồ</a>}
										{this.state.viewMode === viewModes.MAP && <a className="view-mode" onClick={this.setListView.bind(this)}><i className="ic ic-list-black"></i> Danh sách</a>}
										<a className={`f-filter ${this.hasFilter(this.props.carFinding.filter, this.state.customFilterConfig) ? "has-dot-red" : ""}`} onClick={this.toogleFilterBox}><i className="ic ic-filter-black"></i> Bộ lọc</a>
									</div>
								</div>
								{content}
								<Filter
										viewModes={viewModes}
										defPriceRange={defPriceRange}
										defLimitKMRange={defLimitKMRange}
										defLimitKMPriceRange={defLimitKMPriceRange}
										defYearRange={defYearRange}
										defSeatRange={defSeatRange}
										defRadiusRange={defRadiusRange}
										filter={this.props.carFinding.filter}
										counters={this.props.carFinding.counters}
										customFilterConfig={this.state.customFilterConfig}
										vehicleTypes={this.state.vehicleTypes}
										vehicleMakes={this.state.vehicleMakes}
										viewMode={this.state.viewMode}
										defFuelCRange={defFuelCRange}
										defDPriceRange={defDPriceRange}
										setFilter={this.setFilter}
										resetFilter={this.resetFilter}
										searchCar={this.searchCar}
										isShowAdvance={this.state.showAdvance}
										showMessageBox={this.showMessageBox}
										hideMessageBox={this.hideMessageBox}
										showAdvance={this.showAdvance}
										hideAdvance={this.hideAdvance} />
						</section>
						<Modal
								show={this.state.isOpenFilterBox}
								onHide={this.toogleFilterBox}
								dialogClassName="modal-sm modal-dialog">
								<Modal.Header closeButton={true} closeLabel={""}>
										<Modal.Title>Bộ lọc</Modal.Title>
								</Modal.Header>
								<Modal.Body>
										<FilterSidebar
												viewModes={viewModes}
												defPriceRange={defPriceRange}
												defLimitKMRange={defLimitKMRange}
												defLimitKMPriceRange={defLimitKMPriceRange}
												defYearRange={defYearRange}
												defSeatRange={defSeatRange}
												defRadiusRange={defRadiusRange}
												filter={this.props.carFinding.filter}
												counters={this.props.carFinding.counters}
												customFilterConfig={this.state.customFilterConfig}
												vehicleTypes={this.state.vehicleTypes}
												vehicleMakes={this.state.vehicleMakes}
												viewMode={this.state.viewMode}
												defFuelCRange={defFuelCRange}
												defDPriceRange={defDPriceRange}
												setFilter={this.setFilter}
												resetFilter={this.resetFilter}
												searchCar={this.searchCar}
												isShowAdvance={this.state.showAdvance}
												toogleFilterBox={this.toogleFilterBox}
												showMessageBox={this.showMessageBox}
												hideMessageBox={this.hideMessageBox}
												showAdvance={this.showAdvance}
												hideAdvance={this.hideAdvance} />
								</Modal.Body>
						</Modal>
						<MessageBox show={this.state.errMsgBox !== ""} hideModal={this.hideMessageBox.bind(this)} message={this.state.errMsgBox} />
						{this.state.isShowPopup && <CarDetailPopup tooglePopup={this.tooglePopup} carId={this.state.popupCarId} />}
				</div>
		}
}

function mapState(state) {
		return {
				carFinding: state.carFinding
		}
}

CarFinding = connect(mapState)(CarFinding);
export default CarFinding;