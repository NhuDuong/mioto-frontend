import React from "react"
import { Link } from "react-router-dom"

import bg from "../../static/images/homev2/owner.jpg"

function Explorer() {
    return <div className="owner__sect" style={{ backgroundImage: `url(${bg})` }}>
        <div className="ex-container">
            <div className="inner__wrap">
                <h2>Bạn muốn cho thuê xe tự lái </h2>
                <p>Trở thành đối tác của chúng tôi để có cơ hội kiếm thêm thu nhập hàng tháng.</p>
                <div class="has-2btn">
                    <Link className="btn btn-secondary btn--m" to="/howitwork#owner">Tìm hiểu ngay </Link>
                    <Link className="btn btn-primary btn--m" to="/owner/register">Đăng ký xe </Link>
                </div>
            </div>
        </div>
    </div>
}

export default Explorer;