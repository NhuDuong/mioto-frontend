import React from "react"
import { Link } from "react-router-dom"

import step1 from "../../static/images/homev2/step-1.svg"
import step2 from "../../static/images/homev2/step-2.svg"
import step3 from "../../static/images/homev2/step-3.svg"
import step4 from "../../static/images/homev2/step-4.svg"

function Tutorial() {
    return <div className="tutorial__sect">
        <div className="ex-container">
            <h3 className="n-title">Hướng dẫn thuê xe</h3>
            <Link to="/howitwork">
                <div className="step-box__wrap">
                    <div className="step-box__item">
                        <div className="step-img">
                            <div className="fix-img"> <img src={step1} alt="Mioto - Thuê xe tự lái"/></div>
                        </div>
                        <div className="step-detail">
                            <h3>Đặt xe với Mioto</h3>
                        </div>
                    </div>
                    <div className="step-box__item">
                        <div className="step-img">
                            <div className="fix-img"> <img src={step2} alt="Mioto - Thuê xe tự lái"/></div>
                        </div>
                        <div className="step-detail">
                            <h3>Nhận xe hoặc giao tận nơi</h3>
                        </div>
                    </div>
                    <div className="step-box__item">
                        <div className="step-img">
                            <div className="fix-img"> <img src={step3} alt="Mioto - Thuê xe tự lái"/></div>
                        </div>
                        <div className="step-detail">
                            <h3>Trải nghiệm chuyến đi</h3>
                        </div>
                    </div>
                    <div className="step-box__item">
                        <div className="step-img">
                            <div className="fix-img"> <img src={step4} alt="Mioto - Thuê xe tự lái"/></div>
                        </div>
                        <div className="step-detail">
                            <h3>Kết thúc giao dịch</h3>
                        </div>
                    </div>
                </div>
            </Link>
            <div className="s-all">
                <Link to="/howitwork">
                    <p className="see-all">Xem thêm <i className="ic ic-chevron-right" /></p>
                </Link>
            </div>
        </div>
    </div>
}

export default Tutorial;