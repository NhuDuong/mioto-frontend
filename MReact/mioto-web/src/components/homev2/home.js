import React from "react"
import cookie from "react-cookies"
import queryString from 'query-string'

import { getHomePage } from "../../model/common"
import Header from "../common/header"
import Footer from "../common/footer"
import Cover from "./cover"
import NewCars from "./newcars"
import RecentCars from "./recentcars"
import FeatureCars from "./featurecars"
import Destination from "./destination"
import DealCars from "./dealcars"
import Tutorial from "./tutorial"
import Explorer from "./explorer"
import AppAds from "./appads"
import AppInstall from "./appinstall"
import Blogs from "./blogs"
import News from "./news"

class HomeV2 extends React.Component {
    constructor() {
        super();
        this.state = {
            dealArounds : null,
            topDests: null,
            featureCars: null,
            newCars: null
        }
    }

    componentDidMount() {
        window.scrollTo(0, 0);

        getHomePage().then(resp => {
            this.setState({
                dealArounds: resp.data.data.dealArounds,
                featureCars: resp.data.data.featureCars,
                recentCars: resp.data.data.recentViews,
                newCars: resp.data.data.newCars,
                topDests: resp.data.data.topDest,
            })
        });

        //GA
        if (this.props.location && this.props.location.search) {
            const query = queryString.parse(this.props.location.search);

            if (query.utm_source) {
                cookie.save("_utm_src", query.utm_source, { path: '/' });
                cookie.save("_utm_src", query.utm_source, { path: '/', domain: '.mioto.vn' });
            }
            if (query.utm_ext) {
                cookie.save("_utm_ext", query.utm_ext, { path: '/' });
                cookie.save("_utm_ext", query.utm_ext, { path: '/', domain: '.mioto.vn' });
            } else {
                cookie.remove("_utm_ext", { path: '/' });
                cookie.remove("_utm_ext", { path: '/', domain: '.mioto.vn' });
            }
        }

        const utmSrc = cookie.load("_utm_src") || "web_directly";
        const utmExt = cookie.load("_utm_ext") || "no_label";
        window.ga('send', 'event', utmSrc, "VIEW_HOME", utmExt);
    }

    render() {
        return <div className="mioto-layout">
            <Header isReloadable={true} />
            <section className="body">
                <Cover />
                {this.state.dealArounds && <DealCars cars={this.state.dealArounds} />}
                <Tutorial />
                {this.state.recentCars && <RecentCars cars={this.state.recentCars} />}
                {this.state.topDests && <Destination topDests={this.state.topDests} />}
                {this.state.featureCars && <FeatureCars cars={this.state.featureCars} />}
                <Explorer />
                {this.state.newCars && <NewCars cars={this.state.newCars} />}
                <News />
                <Blogs />
                <AppAds />
                <AppInstall />
            </section>
            <Footer />
        </div>
    }
}

export default HomeV2;