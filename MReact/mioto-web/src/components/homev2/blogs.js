import React from "react"
import moment from "moment"
import { Link } from 'react-router-dom'

import { getBlogPage } from "../../model/blog"
import { commonErr } from "../common/errors"
import { formatTitleInUrl } from "../common/common"

import defThumb1 from "../../static/images/homev2/blog-1.jpg"
import defThumb2 from "../../static/images/homev2/blog-2.jpg"
import bg from "../../static/images/homev2/bg-blog.png"

function BlogOdd(props) {
    const blog = props.blog;

    return <div className="swiper-slide blog__item">
        <Link to={`/blog/${formatTitleInUrl(blog.title)}/${blog.id}`}>
            <div className="blog-img">
                <div className="fix-img">
                    <img src={blog.thumb ? blog.thumb : defThumb1} alt="Mioto - Thuê xe tự lái" />
                </div>
                <div className="title-blog">
                    <h3>{blog.title}</h3>
                </div>
            </div>
            <div className="blog-detail">
                <div className="bar-line" />
                <div className="content">
                    <p>{blog.desc}</p>
                </div>
                <div className="group-info">
                    <div className="time-post">
                        <i className="ic ic-clock" />
                        <span>{moment(blog.timeCreated).fromNow()}</span>
                    </div>
                    <div className="s-all">
                        <p className="see-all">Xem tiếp</p>
                        <i className="ic ic-chevron-right" />
                    </div>
                </div>
            </div>
        </Link>
    </div>
}

function BlogEven(props) {
    const blog = props.blog;

    return <div className="swiper-slide blog__item">
        <Link to={`/blog/${formatTitleInUrl(blog.title)}/${blog.id}`}>
            <div className="blog-detail">
                <div className="content">
                    <p>{blog.desc}</p>
                </div>
                <div className="group-info">
                    <div className="time-post">
                        <i className="ic ic-clock" />
                        <span>{moment(blog.timeCreated).fromNow()}</span>
                    </div>
                    <div className="s-all">
                        <p className="see-all">Xem tiếp </p>
                        <i className="ic ic-chevron-right" />
                    </div>
                </div>
                <div className="bar-line" />
            </div>
            <div className="blog-img">
                <div className="fix-img">
                    <img src={blog.thumb ? blog.thumb : defThumb2} alt="Mioto - Thuê xe tự lái" />
                </div>
                <div className="title-blog">
                    <h3>{blog.title}</h3>
                </div>
            </div>
        </Link>
    </div>
}

class BlogSider extends React.Component {
    componentDidMount() {
        this.swiper = new window.Swiper(this.refs.swiperBlogs, {
            slidesPerView: 3,
            spaceBetween: 24,
            threshold: 15,
            speed: 600,
            navigation: {
                nextEl: '.next-blog',
                prevEl: '.prev-blog',
            },
            pagination: {
                el: '.swiper-pagination',
            },
            loop: true,
            slidesOffsetBefore: 0,
            slidesOffsetAfter: 0,
            preventClicks: false,
            preventClicksPropagation: false,
            breakpoints: {
                992: {
                    slidesPerView: 3,
                    spaceBetween: 30
                },
                768: {
                    slidesPerView: 3,
                    spaceBetween: 30
                },
                480: {
                    slidesPerView: 1,
                    spaceBetween: 15
                }
            },
        });
    }

    render() {
        return <div ref="swiperBlogs" className="swiper-container swiper-blog">
            <div className="swiper-wrapper blog__wrap">
                {this.props.blogs.map((blog, i) => {
                    if (i % 2 === 0) {
                        return <BlogEven key={i} blog={blog} />
                    } else {
                        return <BlogOdd key={i} blog={blog} />
                    }
                })}
            </div>
            <div className="swiper-pagination"></div>
        </div>
    }
}


export default class Blogs extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            err: commonErr.INNIT,
            errMsg: "",
            blogPage: null,
            city: props.city
        }
    }

    componentDidMount() {
        getBlogPage(0, this.state.city).then((resp) => {
            this.setState({
                err: resp.data.error,
                errMsg: resp.data.errorMessage,
                blogs: resp.data.data,
            });
        });
    }

    next() {
        getBlogPage(this.state.blogs.curPage + 1, this.state.city).then((resp) => {
            this.setState({
                err: resp.data.error,
                errMsg: resp.data.errorMessage,
                blogs: resp.data.data
            });
        });
    }

    pre() {
        getBlogPage(this.state.blogs.curPage - 1, this.state.city).then((resp) => {
            this.setState({
                err: resp.data.error,
                errMsg: resp.data.errorMessage,
                blogs: resp.data.data
            });
        });
    }

    render() {
        if (!this.state.blogs || !this.state.blogs.blogs) {
            return null
        } else {
            return <div className="blog__sect" style={{ backgroundImage: `url(${bg})` }}>
                <div className="m-container">
                    <h3 className="n-title">Blogs </h3>
                    <div className="swiper-button-next next-blog">
                        <i className="i-arr" />
                    </div>
                    <div className="swiper-button-prev prev-blog">
                        <i className="i-arr" />
                    </div>
                    {this.state.blogs && this.state.blogs.blogs && <BlogSider blogs={this.state.blogs.blogs} />}
                </div>
            </div>
        }
    }
}