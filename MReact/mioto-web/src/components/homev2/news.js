import React from "react"

import new1 from "../../static/images/homev2/news-1.png"
import new2 from "../../static/images/homev2/news-2.jpg"
import new3 from "../../static/images/homev2/news-3.png"
import vnex from "../../static/images/homev2/vn-express.png"
import dantri from "../../static/images/homev2/dan-tri.png"
import genk from "../../static/images/homev2/genk.png"
import bg from "../../static/images/homev2/bg-news.png"

class NewsSlider extends React.Component {
    componentDidMount() {
        this.swiper = new window.Swiper(this.refs.swiperNews, {
            slidesPerView: 3,
            spaceBetween: 24,
            threshold: 15,
            speed: 600,
            pagination: {
                el: '.pagi-news',
            },
            loop: true,
            slidesOffsetBefore: 0,
            slidesOffsetAfter: 0,
            preventClicks: false,
            preventClicksPropagation: false,
            breakpoints: {
                992: {
                    slidesPerView: 3,
                    spaceBetween: 30
                },
                768: {
                    slidesPerView: 3,
                    spaceBetween: 30
                },
                480: {
                    slidesPerView: 1,
                    spaceBetween: 15
                }
            },
        });
    }

    render() {
        return <div ref="swiperNews" className="swiper-container swiper-news">
            <div className="swiper-wrapper news-grid__wrap">
                <div className="swiper-slide news__item">
                    <div className="news-img">
                        <a href="https://startup.vnexpress.net/tin-tuc/hanh-trinh-khoi-nghiep/mo-hinh-airbnb-trong-linh-vuc-thue-xe-oto-tai-viet-nam-3764274.html" target="_blank">
                            <div className="fix-img"><img src={new1} /></div>
                            <div className="news-logo"> <img src={vnex} /></div>
                        </a>
                    </div>
                </div>
                <div className="swiper-slide news__item">
                    <div className="news-img">
                        <a href="http://dantri.com.vn/thi-truong/ung-dung-mioto-mo-hinh-airbnb-trong-linh-vuc-thue-xe-o-to-tai-viet-nam-20180614204504383.htm" target="_blank">
                            <div className="fix-img"><img src={new2} /></div>
                            <div className="news-logo"><img src={dantri} /></div>
                        </a>
                    </div>

                </div>
                <div className="swiper-slide news__item">
                    <div className="news-img">
                        <a href="http://genk.vn/ung-dung-mioto-mo-hinh-airbnb-trong-linh-vuc-thue-xe-o-to-tai-viet-nam-20180618112653382.chn" target="_blank">
                            <div className="fix-img"><img src={new3} /></div>
                            <div className="news-logo"><img src={genk} /></div>
                        </a>
                    </div>
                </div>
            </div>
            <div className="swiper-pagination pagi-news"></div>
        </div>
    }
}

function News() {
    return <div className="newspaper__sect" style={{ backgroundImage: `url(${bg})` }}>
        <div className="m-container">
            <h3 className="n-title">Báo chí nói về chúng tôi</h3>
            <NewsSlider />
        </div>
    </div>
}

export default News;