import React from "react"
import cookie from "react-cookies"

import bg from "../../static/images/homev2/bg-app.png"
import images_phones from "../../static/images/homev2/app-4.png"
import images_appstore from "../../static/images/appstore.png"
import images_googleplay from "../../static/images/googleplay.png"

class AppAds extends React.Component {
    onIOSAppClick() {
        //GA
        const utmSrc = cookie.load("_utm_src") || "web_directly";
        const utmExt = cookie.load("_utm_ext") || "no_label";
        window.ga('send', 'event', utmSrc, "INSTALL_APP_IOS", utmExt);
    }

    onAndrAppClick() {
        //GA
        const utmSrc = cookie.load("_utm_src") || "web_directly";
        const utmExt = cookie.load("_utm_ext") || "no_label";
        window.ga('send', 'event', utmSrc, "INSTALL_APP_ANDR", utmExt);
    }

    render() {
        return <div className="down-app__sect" style={{ backgroundImage: `url(${bg})` }}>
            <div className="ex-container">
                <div className="app-content">
                    <h2>Ứng dụng cho điện thoại </h2>
                    <p>Tải ngay ứng dụng tại App Store hoặc Google Play</p>
                    <div className="store">
                        <a className="func-app" target="_blank" onClick={this.onIOSAppClick.bind(this)} href="https://itunes.apple.com/vn/app/mioto-thu%C3%AA-xe-t%E1%BB%B1-l%C3%A1i/id1316420500?l=vi&mt=8">
                            <img className="responsive-img" src={images_appstore} />
                        </a>
                        <a className="func-app" target="_blank" onClick={this.onAndrAppClick.bind(this)} href="https://play.google.com/store/apps/details?id=com.mioto.mioto&hl=vi">
                            <img className="responsive-img" src={images_googleplay} />
                        </a>
                    </div>
                </div>
                <div className="app-img">
                    <img src={images_phones} />
                </div>
            </div>
        </div>
    }
}

export default AppAds;