import React from "react"

import ra from "../../static/images/homev2/right-arrow.png"
import la from "../../static/images/homev2/left-arrow.png"

function DestinationItem(props) {
    const dest = props.dest;

    return <div className="swiper-slide dest-img">
        <a href={`/city/${dest.city}`}>
            <div className="fix-img">
                <img src={dest.image} alt={`Cho thuê xe tự lái ${dest.name}`} />
                <h3>{dest.name}</h3>
            </div>
        </a>
    </div>
}

class Destination extends React.Component {
    componentDidMount() {
        this.swiper = new window.Swiper(this.refs.swiper, {
            slidesPerView: 5,
            spaceBetween: 15,
            threshold: 15,
            speed: 400,
            navigation: {
                nextEl: '.next-dest',
                prevEl: '.prev-dest',
            },
            loop: true,
            slidesOffsetBefore: 0,
            slidesOffsetAfter: 0,
            preventClicks: false,
            preventClicksPropagation: false,
            breakpoints: {
                992: {
                    slidesPerView: 3,
                    spaceBetween: 40
                },
                768: {
                    slidesPerView: 3,
                    spaceBetween: 40
                },
                480: {
                    slidesPerView: 1.4,
                    centeredSlides: true
                }
            }
        })
    }

    render() {
        return <div className="destination__sect">
            <div className="m-container">
                <h3 className="n-title">Địa điểm nổi bật</h3>
                <div className="swiper-button-next next-dest">
                    <i className="i-arr" style={{ backgroundImage: `url(${ra})` }}></i>
                </div>
                <div className="swiper-button-prev prev-dest">
                    <i className="i-arr" style={{ backgroundImage: `url(${la})` }}></i>
                </div>
                <div ref="swiper" className="swiper-container swiper-destination">
                    <div className="swiper-wrapper dest-item">
                        {this.props.topDests.map(function (dest, i) {
                            return <DestinationItem key={i} dest={dest} />
                        })}
                    </div>
                </div>
            </div>
        </div>
    }
}

export default Destination;