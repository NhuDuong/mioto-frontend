import React from "react"
import { Link } from 'react-router-dom'
// import Select from 'react-select';
import PlacesAutocomplete from 'react-places-autocomplete'
import moment from 'moment'
import cookie from 'react-cookies'
import DateRangePicker from 'react-bootstrap-daterangepicker'
import { connect } from "react-redux"
import { isIOS } from "react-device-detect"

import { commonErr } from "../common/errors"
import { LoadingInline } from "../common/loading"
import { MessageBox } from "../common/messagebox"

import bg from "../../static/images/homev2/banner-1.jpg"
import 'bootstrap-daterangepicker/daterangepicker.css'
import ad_monthly from "../../static/images/banners/monthly-promo.jpg"
import ad_dn from "../../static/images/banners/promo-dn.jpg"
import ad_dl from "../../static/images/banners/promo-dl.jpg"

import mic from "../../static/images/homev2/mic.png"


const calendarTemplate = '<div class="daterangepicker dropdown-menu">' +
    '<div class="calendar left">' +
    '<div class="daterangepicker_input hidden">' +
    '<input class="input-mini form-control" type="text" name="daterangepicker_start" value="" />' +
    '<i class="fa fa-calendar glyphicon glyphicon-calendar"></i>' +
    '<div class="calendar-time">' +
    '<div></div>' +
    '<i class="fa fa-clock-o glyphicon glyphicon-time"></i>' +
    '</div>' +
    '</div>' +
    '<div class="calendar-table"></div>' +
    '</div>' +
    '</div>' +
	'</div>';
	
const bookingTimes = [];
for (var i = 0; i < 1440; i = i + 30) {
    const time = i;
    const hour = (time - time % 60) / 60;
    const hourLabel = hour < 10 ? `0${hour}` : `${hour}`;
    const min = time % 60;
    const minLabel = min < 10 ? `0${min}` : `${min}`;
    bookingTimes.push({ value: time, label: `${hourLabel}:${minLabel}` });
}

const placesAutocompleteStyle = {
		autocompleteContainer: {
				position: 'absolute',
				top: '100%',
				backgroundColor: 'rgba(0, 0, 0, 0.7)',
				width: '100%',
				border: 'none'
		},
		autocompleteItem: {
				backgroundColor: 'rgba(0, 0, 0, 0.7)',
				padding: '10px',
				color: '#f0f0f0',
				cursor: 'pointer'
		},
		autocompleteItemActive: {
				backgroundColor: '#6c6c6e',
				color: '#fffff',
		},
}

class AdBaners extends React.Component {
		componentDidMount() {
				this.swiper = new window.Swiper(this.refs.swiperAdBanner, {
						slidesPerView: 1,
						spaceBetween: 15,
						threshold: 15,
						speed: 500,
						loop: true,
						slidesOffsetBefore: 0,
						slidesOffsetAfter: 0,
						preventClicks: false,
						preventClicksPropagation: false,
						autoplay: {
								delay: 6000,
						},
						breakpoints: {
								991: {
										slidesPerView: 1,
										spaceBetween: 10,
								},
								767: {
										slidesPerView: 1,
										spaceBetween: 15,
								},
								480: {
										slidesPerView: 1,
										spaceBetween: 10,
								}
						}
				});
		}

		render() {
				return <div ref="swiperAdBanner" className="swiper-container swiper-banner banner-promo__wrap">
						<div className="swiper-wrapper">
							<div className="swiper-slide">
								<div className="fix-img"> <Link to={{
                        pathname: "/find/filter",
                        search: `address=${this.props.address === "" ? 'Hồ Chí Minh' : this.props.address}`
                    }}><img src={ad_monthly} /></Link></div>
							</div>
							<div className="swiper-slide">
								<div className="fix-img"> <a href={`/city/DaLat`}><img src={ad_dl} /></a></div>
							</div>
							<div className="swiper-slide">
								<div className="fix-img"> <a href={`/city/DaNang`}><img src={ad_dn} /></a></div>
							</div>
						</div>
				</div>
		}
}

class Cover extends React.Component {
		constructor() {
				super();
				this.state = {
						startDate: moment().set({
							hour: 21,
							minute: 0,
							second: 0,
							milliseconds: 0
						}),
						endDate: moment().add(1, "days").set({
							hour: 20,
							minute: 0,
							second: 0,
							milliseconds: 0
						}),
						err: commonErr.INNIT,
						errMsg: "",
						address: "",
						isOpenSearching: true,
				}

				this.findCar = this.findCar.bind(this);
				this.onStartDateChange = this.onStartDateChange.bind(this);
				this.onDateRangeChange = this.onDateRangeChange.bind(this);
				this.onAddressChange = this.onAddressChange.bind(this);
				this.onAddressFocus = this.onAddressFocus.bind(this);
				this.onSelectFromAddr = this.onSelectFromAddr.bind(this);
				this.hideMessageBox = this.hideMessageBox.bind(this);
				this.onStartTimeChange = this.onStartTimeChange.bind(this);
				this.onEndTimeChange = this.onEndTimeChange.bind(this);
				this.toogleSearching = this.toggleSearching.bind(this);
		}

		componentDidMount() {
				const address = cookie.load("_maddr") || "";
				this.setState({
						address: address,
						isHighlight: false
				})

				var startDate;
				var endDate;
				var startTime;
				var endTime;

				// check valid cookie before set
				if(cookie.load("_mstartts") < moment().add(2, "hours").valueOf() || cookie.load("_mendts") < moment().add(2, "hours").valueOf()){
					cookie.remove("_mstartts", { path: '/' });
					cookie.remove("_mendts", { path: '/' });
				}

				const cStartTs = cookie.load("_mstartts");
				const cEndTs = cookie.load("_mendts");
				if (cStartTs && cEndTs) {
						const cStartDate = moment(cStartTs * 1);
						const cEndDate = moment(cEndTs * 1);
						startDate = cStartDate;
						endDate = cEndDate;
						startTime = cStartDate.get("hour") * 60 + startDate.get("minute");
						endTime = cEndDate.get("hour") * 60 + endDate.get("minute");
				}

				if (startDate && endDate) {
					this.setState({
							startDate: startDate,
							endDate: endDate,
							startTime: startTime,
							endTime: endTime,
					})
				} else {
					if(this.props.appInit.err === commonErr.SUCCESS)
						this.setState({
								startDate: moment(this.props.appInit.data.startTime),
								endDate: moment(this.props.appInit.data.endTime),
								startTime: moment(this.props.appInit.data.startTime).get("hour") * 60 + moment(this.props.appInit.data.startTime).get("minute"),
								endTime: moment(this.props.appInit.data.endTime).get("hour") * 60 + moment(this.props.appInit.data.endTime).get("minute"),
						});
				}
		}

		componentWillReceiveProps(props) {
			// if(!cookie.load("_mstartts") || !cookie.load("_mendts"))
			if(cookie.load("_mstartts") > moment().add(2, "hours").valueOf() && cookie.load("_mendts") > moment().add(2, "hours").valueOf()){
				const cStartTs = cookie.load("_mstartts") * 1;
				const cEndTs = cookie.load("_mendts") * 1;
				if (cStartTs && cEndTs) {
					this.setState({
						startDate: moment(cStartTs),
						endDate: moment(cEndTs),
						startTime: moment(cStartTs).get("hour") * 60 + moment(cStartTs).get("minute"),
						endTime: moment(cEndTs).get("hour") * 60 + moment(cEndTs).get("minute")
					})
				}
			} else {
				if (props.appInit.data.startTime !== this.props.appInit.data.startTime
						&& props.appInit.data.endTime !== this.props.appInit.data.endTime) {
						this.setState({
								startDate: moment(props.appInit.data.startTime),
								endDate: moment(props.appInit.data.endTime),
								startTime: moment(props.appInit.data.startTime).get("hour") * 60 + moment(props.appInit.data.startTime).get("minute"),
                				endTime: moment(props.appInit.data.endTime).get("hour") * 60 + moment(props.appInit.data.endTime).get("minute"),
						});
				}
			}
		}

		onStartDateChange(event, picker) {
			if(picker.startDate){
				const startDate = picker.startDate;
				var endDate = this.state.endDate;
				var endTime = this.state.endTime;
				startDate.set({
					hour: (this.state.startTime -  this.state.startTime % 60) / 60,
					minute: this.state.startTime % 60,
					second: 0,
					milliseconds: 0
				});

				if (endDate <= startDate) {
					endDate = startDate.clone();
					endDate.add(23, "hour");
					endTime = moment(endDate).get("hour") * 60 + moment(endDate).get("minute");
				}

				//save cookies
				cookie.save("_mstartts", startDate.valueOf(), { path: '/' });
				cookie.save("_mendts", endDate.valueOf(), { path: '/' });

				this.toggleSearching();
				this.setState({
						startDate: startDate,
						endDate: endDate,
						endTime: endTime,
				})
				this.toggleSearching();
			}				
		}

		onDateRangeChange(event, picker) {
			if(picker.startDate && picker.endDate){
				const startDate = picker.startDate;
				startDate.set({
					hour: (this.state.startTime -  this.state.startTime % 60) / 60,
					minute: this.state.startTime % 60,
					second: 0,
					milliseconds: 0
				});
				const endDate = picker.endDate;
				endDate.set({
					hour: (this.state.endTime -  this.state.endTime % 60) / 60,
					minute: this.state.endTime % 60,
					second: 0,
					milliseconds: 0
				});
				if (endDate < startDate) {
						return;
				}

				//save cookies
				cookie.save("_mstartts", startDate.valueOf(), { path: '/' });
				cookie.save("_mendts", endDate.valueOf(), { path: '/' });
				this.setState({
						startDate: startDate,
						endDate: endDate
				})
			}				
		}

		toggleSearching() {
			this.setState({
				isOpenSearching: !this.state.isOpenSearching
			})
		}

		onStartTimeChange(event) {
			const time = event.target.value;
			const hour = (time - time % 60) / 60;
			const min = time % 60;
			const startDate = moment(this.state.startDate).set({
				hour: hour,
				minute: min,
				second: 0,
				milliseconds: 0
			});
	
			if (this.state.endDate && this.state.endDate.isValid() && this.state.endDate <= startDate) {
				this.setState({
					startDate: this.state.endDate,
					endDate: startDate,
					startTime: this.state.endTime,
					endTime: time,
					err: commonErr.FAIL,
					errMsg: "Thời gian thuê xe không hợp lệ."
				});
			} else {
				cookie.save("_mstartts", startDate.valueOf(), { path: '/' });
				this.setState({
					startDate: startDate,
					startTime: time,
					err: commonErr.SUCCESS,
					errMsg: ""
				});
			}
		}
	
		onEndTimeChange(event) {
			const time = event.target.value;
			if (this.state.endDate && this.state.endDate.isValid()) {
				const hour = (time - time % 60) / 60;
				const min = time % 60;
				const endDate = moment(this.state.endDate).set({
					hour: hour,
					minute: min,
					second: 0,
					milliseconds: 0
				})
				if (endDate <= this.state.startDate) {
					this.setState({
						endDate: this.state.startDate,
						startDate: endDate,
						endTime: this.state.startTime,
						startTime: time,
						err: commonErr.FAIL,
						errMsg: "Thời gian thuê xe không hợp lệ."
					});
				} else {
					cookie.save("_mendts", endDate.valueOf(), { path: '/' });
					this.setState({
						endDate: endDate,
						endTime: time,
						err: commonErr.SUCCESS,
						errMsg: ""
					});
				}
			} else {
				this.setState({
					endTime: time,
					err: commonErr.SUCCESS,
					errMsg: ""
				});
			}
		}
	

		findCar(e) {
				if (!this.state.address || this.state.address === "") {
						this.setState({
								err: commonErr.FAIL,
								errMsg: "Vui lòng nhập địa chỉ để tìm kiếm."
						});
						e.preventDefault();
						return false;
				}
		}

		onAddressFocus(e) {
				this.setState({
						address: "",
						isHighlight: false
				});
		}

		onAddressChange(address) {
				this.setState({
						address: address
				})
		}

		onSelectFromAddr(address) {
				if (address.err < 0) {
						var errMsg = "";
						if (address.err === -200) {
								errMsg = "Bạn đã không cho phép trình duyệt truy cập vị trí hiện tại. Vui lòng cài đặt cho phép hoặc nhập địa chỉ.";
						} else {
								errMsg = "Không thể tìm thấy vị trí hiện tại của thiết bị. Vui lòng thử lại hoặc nhập địa chỉ.";
						}
						this.setState({
								err: commonErr.FAIL,
								errMsg: errMsg
						});
				} else if (address.err === commonErr.LOADING) {
						this.setState({
								err: commonErr.LOADING,
						});
				} else {
						this.setState({
								err: commonErr.SUCCESS,
								address: address.address
						})
				}
		}

		hideMessageBox() {
				this.setState({
						errMsg: ""
				});
		}

		render() {
			return <div className="banner-home__sect" style={{ backgroundImage: `url(${bg})` }}>
						<div className="insurance-partner">
							<p className="lstitle">ĐỐI TÁC BẢO HIỂM</p>
							<img class="img-fluid" src={mic} />
						</div>
						<div className="container" id="home-box">
								<h1 className="slogan">Thuê xe tự lái <span className="dash">-</span> <span className="break">Tải ngay Mioto</span></h1>
								
								{this.state.isOpenSearching && <div className="inside d-flex inside-new">
										{this.state.err === commonErr.LOADING && <div className="form-search location">
											<label className="home-label">Địa điểm</label>
											<div style={{height: '24px'}}></div>
											<LoadingInline />
										</div>}
										<div className={`form-search location has-placeholder ${this.state.err === commonErr.LOADING ? "hidden" : ""}`}>
											<label className="home-label">Địa điểm</label>
											<PlacesAutocomplete
													classNames={{ input: this.state.isHighlight ? "has-error" : "" }}
													inputProps={{
															value: this.state.address,
															onChange: this.onAddressChange,
															onFocus: this.onAddressFocus,
															placeholder: "Nhập thành phố, quận, địa chỉ..."
													}}
													styles={placesAutocompleteStyle}
													onSelect={this.onSelectFromAddr}
													isUseCurrentLocation={true}
													googleLogo={false}
													options={{ componentRestrictions: { country: ['vn'] } }} />
										</div>
										<div className="form-search has-timer">
											<label className="home-label">Bắt đầu</label>
											<div className="d-flex">
											<div className="wrap-input home-datetimerange">
												<DateRangePicker
													startDate={this.state.startDate}
													endDate={this.state.startDate}
													minDate={moment()}
													maxDate={moment().add(3, "months")}
													timePicker={false}
													singleDatePicker={true}
													singleSelect={true}
													autoApply={true}
													isIOs={isIOS}
													timePickerIncrement={30}
													linkedCalendars={false}
													timePicker24Hour={true}
													onHide={this.onStartDateChange.bind(this)}
													buttonClasses={["hidden"]}
													opens={"center"}
													template={calendarTemplate}
													parentEl={"#home-box"}>
													<span className="value">{this.state.startDate ? this.state.startDate.format("DD/MM/YYYY") : "Chọn ngày"} <i className="i-chevron-down"></i></span>
												</DateRangePicker>
											</div>
												<div className="wrap-select home-select">
													<select onChange={this.onStartTimeChange.bind(this)} value={this.state.startTime}>
														{bookingTimes.map(time => <option key={`s_${time.value}`} value={time.value}>{time.label} </option>)}
													</select>
											
												</div>		
											</div>
										</div>
										<div className="form-search has-timer">
											<label className="home-label">Kết thúc</label>
											<div className="d-flex">
												<div className="wrap-input home-datetimerange">
													<DateRangePicker
														startDate={this.state.startDate}
														endDate={this.state.endDate}
														minDate={moment(this.state.startDate.valueOf())}
														maxDate={moment().add(3, "months")}
														timePicker={false}
														singleDateRangePicker={true}
														singleSelect={true}
														autoApply={true}
														isIOs={isIOS}
														timePickerIncrement={30}
														linkedCalendars={false}
														timePicker24Hour={true}
														pickingEndDateOnly={true}
														onHide={this.onDateRangeChange.bind(this)}
														buttonClasses={["hidden"]}
														opens={"center"}
														template={calendarTemplate}
														parentEl={"#home-box"}>
														<span className="value">{this.state.endDate ? this.state.endDate.format("DD/MM/YYYY") : "Chọn ngày"} <i className="i-chevron-down"></i></span>
													</DateRangePicker>	
												</div>
												<div className="wrap-select home-select">
													<select onChange={this.onEndTimeChange.bind(this)} value={this.state.endTime}>
														{bookingTimes.map(time => <option key={`e_${time.value}`} value={time.value}>{time.label} </option>)}
													</select>
												
												</div>		
											</div>
										</div>
										{/* <div className="form-search has-timer">
												<i className="ic ic-datepicker" />
												<DateRangePicker
														startDate={this.state.startDate}
														endDate={this.state.endDate}
														minDate={moment()}
														maxDate={moment().add(3, "months")}
														timePicker={true}
														autoApply={true}
														buttonClasses={["hidden"]}
														isIOs={isIOS}
														timePickerIncrement={30}
														opens={"left"}
														linkedCalendars={true}
														timePicker24Hour={true}
														parentEl={"#home-box"}
														onHide={this.onDateRangeChange}>
														<span style={{ width: "100%", display: "inline-block", cursor: "pointer" }}>{this.state.startDate.format("HH:mm DD/MM/YYYY")} - {this.state.endDate.format("HH:mm DD/MM/YYYY")}</span>
												</DateRangePicker>
										</div> */}
										<div className="fn-search">
												<Link className="btn-search" onClick={this.findCar} to={{
														pathname: `/find/filter`,
														search: `startDate=${this.state.startDate.valueOf()}&endDate=${this.state.endDate.valueOf()}&address=${this.state.address}`
												}}>
														<i className="ic ic-search" /> <span>Tìm xe ngay</span>
												</Link>
										</div>
										<MessageBox show={this.state.errMsg !== ""} hideModal={this.hideMessageBox} message={this.state.errMsg} />
								</div>}
						<AdBaners address={this.state.address}/>
						</div>
				</div >
		}
}

function mapState(state) {
		return {
				appInit: state.appInit
		}
}

Cover = connect(mapState)(Cover);

export default Cover;