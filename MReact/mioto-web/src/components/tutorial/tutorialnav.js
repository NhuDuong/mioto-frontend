import React from "react"
import { NavLink } from "react-router-dom"

export default function TutorialNav() {
    return <div className="sidebar">
        <ul>
            <li><NavLink activeClassName="active" to="/aboutus">Giới thiệu về Mioto</NavLink></li>
            <li><NavLink activeClassName="active" to="/features">Tính năng nổi bật</NavLink></li>
            <li><NavLink activeClassName="active" to="/renterbenef">Lợi ích thuê xe tự lái</NavLink></li>
            <li><NavLink activeClassName="active" to="/ownerbenef">Lợi ích chủ xe</NavLink></li>
            <li><NavLink activeClassName="active" to="/carregishowto">Cách đăng kí xe cho thuê</NavLink></li>
            <li><NavLink activeClassName="active" to="/renterhowto">Quy trình thuê xe tự lái</NavLink></li>
            <li><NavLink activeClassName="active" to="/ownerhowto">Quy trình cho thuê xe tự lái</NavLink></li>
        </ul>
    </div>
}