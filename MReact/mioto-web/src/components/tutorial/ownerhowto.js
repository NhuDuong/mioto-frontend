import React from "react"

import Header from "../common/header"
import Footer from "../common/footer"
import TutorialNav from "./tutorialnav"

export default class OwnerHowTo extends React.Component {
    componentDidMount() {
        window.scrollTo(0, 0);
    }

    render() {
        return <div className="mioto-layout">
            <Header />
            <section className="body">
                <div className="body-container">
                    <TutorialNav />
                    <div className="content">
                        <h1 className="title">Quy trình cho thuê xe tự lái</h1>
                        <div className="content-container">
                            <h5>Bước 1: Cập nhật thông tin xe và thiết lập điều khoản cho thuê </h5>
                            <p>Chỉ với vài thao tác đơn giản, bạn đã có thể đưa thông tin, mô tả và đăng tải hình ảnh xe của bạn lên Mioto. Cập nhật thời gian cho thuê, mức giá mong muốn và các yêu cầu khác của bạn đối với khách thuê. </p>
                            <div className="space l"></div>
                            <h5>Bước 2: Nhận và phản hồi yêu cầu thuê xe</h5>
                            <p>
                                Bạn sẽ nhận được thông báo từ Mioto khi có khách gửi yêu cầu thuê xe đến bạn.
                            Kiểm tra thông tin cá nhân khách thuê và xác nhận cho thuê xe sớm nhất có thể.
                            Đợi khách thuê chuyển tiền đặt cọc để xác nhận hoàn thành việc đặt xe.
                        </p>
                            <div className="space l"></div>
                            <h5>Bước 3: Gặp khách thuê và bàn giao xe</h5>
                            <p>
                                Bạn và khách thuê sắp xếp với nhau để quyết định thời gian và địa điểm giao xe.
                            Kiểm tra giấy phép lái xe, các giấy tờ liên quan và tài sản đặt cọc của khách.
                            Kiểm tra xe, kí hợp đồng, biên bản bàn giao và gửi chìa khóa xe của bạn cho vị khách đáng tin cậy.
                        </p>
                            <div className="space l"></div>
                            <h5>Bước 4: An tâm và nhận lợi nhuận</h5>
                            <p>
                                Mioto với đội ngũ chăm sóc khách hàng nhiệt tình, luôn bên cạnh hỗ trợ bạn khi xảy ra vấn đề.
                            Mioto với tính năng GPS, cho phép chủ xe có thể dễ dàng theo dõi hiện trạng và vị trí xe của mình ngay trên ứng dụng. (Tính năng dự kiến ra mắt trong năm 2018)
                        </p>
                            <div className="space l"></div>
                            <h5>Bước 5: Nhận lại xe của bạn</h5>
                            <p>
                                Gặp lại khách thuê, kiểm tra xe, kí biên bản bàn giao và nhận lại xe của bạn như thỏa thuận ban đầu.
                            Và cũng đừng quên cho điểm rating khách thuê và gợi ý họ cho điểm bạn trên ứng dụng Mioto nhé. Điều này sẽ rất có lợi đối với uy tín của bạn trong cộng đồng thuê xe tự lái Mioto!
                        </p>
                        </div>
                    </div>
                </div>
                <div className="np-bottom">
                    <div className="np-bottom-container">
                        <div className="np-main">
                            <div className="np-prev"><span>Trở Về</span><a href="/renterhowto"><em>Quy trình thuê xe</em></a></div>
                        </div>
                    </div>
                </div>
            </section>
            <Footer />
        </div>
    }
}