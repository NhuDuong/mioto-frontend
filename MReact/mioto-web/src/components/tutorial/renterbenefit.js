import React from "react"

import Header from "../common/header"
import Footer from "../common/footer"
import TutorialNav from "./tutorialnav"

export default class RenterBenefit extends React.Component {
    componentDidMount() {
        window.scrollTo(0, 0);
    }

    render() {
        return <div className="mioto-layout">
            <Header />
            <section className="body">
                <div className="body-container">
                    <TutorialNav />
                    <div className="content">
                        <h2 className="title">Khách thuê xe - <br />Tại sao nên thuê xe tại Mioto?</h2>
                        <div className="content-container">
                            <ul className="list-type2">
                                <li><span className="w-ic"><i className="ict ict-coin"></i></span>
                                    <h4>Tiết kiệm</h4>
                                    <p>Giá thuê xe bình quân thấp hơn dao động từ 10-20% so với thị trường.</p>
                                </li>
                                <li><span className="w-ic"><i className="ict ict-run"></i></span>
                                    <h4>Nhanh chóng và tiện lợi</h4>
                                    <p>Dễ dàng đặt xe mọi lúc, mọi nơi với quy trình đơn giản và nhanh chóng chỉ mất từ 5-10 phút.</p>
                                </li>
                                <li><span className="w-ic"><i className="ict ict-wheel"></i></span>
                                    <h4>Trải nghiệm mới lạ</h4>
                                    <p>Thoải mái tìm kiếm và lựa chọn nhiều dòng xe khác nhau phù hợp với sở thích và ngân sách của bạn.</p>
                                </li>
                                <li><span className="w-ic"><i className="ict ict-gift"></i></span>
                                    <h4>Chương trình khuyến mãi</h4>
                                    <p>Nhiều chương trình khuyến mãi hấp dẫn khác dành cho khách thuê.</p>
                                </li>
                                <li><span className="w-ic"><i className="ict ict-shield"></i></span>
                                    <h4>An toàn và uy tín</h4>
                                    <p>Các xe đăng kí được xác minh bởi Mioto về tính xác thực của giấy tờ pháp lý và hình ảnh xe.</p>
                                    <p>Dễ dàng kiểm tra thông tin và lịch sử rating để lựa chọn chủ xe uy tín trước khi thực hiện yêu cầu đặt xe.</p>
                                </li>
                                <li><span className="w-ic"><i className="ict ict-star"></i></span>
                                    <h4>Tiện ích vượt trội</h4>
                                    <p>Dễ dàng tìm kiếm và đặt xe trên cả 2 nền tảng Website Mioto.vn và ứng dụng Mioto.</p>
                                    <p>Xem trước lịch trình xe và báo giá thuê xe theo từng ngày.</p>
                                    <p>Cho phép đánh giá xe và chủ xe với tính năng "Rating".</p>
                                </li>
                                <li><span className="w-ic"><i className="ict ict-support"></i></span>
                                    <h4>Đội ngũ hỗ trợ</h4>
                                    <p>Bộ phận CSKH của Mioto luôn bên cạnh đễ hỗ trợ bạn trong mọi tình huống.</p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div className="np-bottom">
                    <div className="np-bottom-container">
                        <div className="np-main">
                            <div className="np-prev"><span>Trở về</span><a href="/features"><em>Tính năng nổi bật</em></a></div>
                            <div className="np-prev np-next"><span>Kế Tiếp</span><a href="/ownerbenef"><em>Lợi ích chủ xe</em></a></div>
                        </div>
                    </div>
                </div>
            </section>
            <Footer />
        </div>
    }
}