import React from "react"

import Header from "../common/header"
import Footer from "../common/footer"
import TutorialNav from "./tutorialnav"

export default class Features extends React.Component {
    componentDidMount() {
        window.scrollTo(0, 0);
    }

    render() {
        return <div className="mioto-layout">
            <Header />
            <section className="body">
                <div className="body-container">
                    <TutorialNav />
                    <div className="content">
                        <h2 className="title">Tính năng nổi bật</h2>
                        <div className="content-container">
                            <h4>Tính năng Calendar</h4>
                            <p>Hỗ trợ chủ xe thuận tiện quản lí, theo dõi lịch trình xe một cách khoa học và hoàn toàn chủ động trong việc thiết lập và điều chỉnh giá thuê xe. Cho phép khách thuê xem trước lịch trình xe và nhận báo giá thuê xe theo từng ngày.</p>
                            <p>Chính vì thế, tính năng Calendar sẽ giúp loại bỏ hoàn toàn công đoạn liên lạc bằng điện thoại giữa chủ xe và khách thuê trong việc trao đổi thời gian và giá thuê xe, qua đó rút ngắn đáng kể thời gian đặt xe.</p>
                            <div className="space l"></div>
                            <h4>Tính năng đánh giá</h4>
                            <p>Cho phép khách thuê xe chấm điểm và gửi nhận xét đến chủ xe cũng như xe được thuê. Nhờ đó, khách thuê xe có thông tin làm cơ sở để có thể lựa chọn các chủ xe uy tín, các xe có chất lượng tốt trước khi quyết định đặt xe.</p>
                            <p>Tính năng Rating của Mioto cũng cho phép chủ xe có thể gửi các phản hồi và chấm điểm ngược lại đối với khách thuê xe. Từ đó giúp nâng cao hơn nữa chất lượng thành viên trong cộng đồng thuê xe tự lái Mioto.</p>
                            <div className="space l"></div>
                            <h4>Đặt xe nhanh và giao nhận xe tận nơi</h4>
                            <p>Bạn không có nhiều thời gian, bạn muốn thuê xe nhanh chóng không cần qua bước xét duyệt online của chủ xe?
                            Bạn muốn xe đươc đem đến tận nơi trước cửa nhà của bạn hay đón bạn trực tiếp tại phi trường?
                            Tính năng “Đặt xe nhanh” và “Giao xe tận nơi” của Mioto sẽ đáp ứng hoàn hảo các yêu cầu này của bạn.</p>
                            <p>Tại Mioto, chúng tôi có một danh sách dài các chủ xe sẵn sàng cung cấp dịch vụ giao xe tận nơi và cho phép bạn thuê xe không cần xét duyệt online (chỉ cần kiểm tra trực tiếp khi bàn giao xe).</p>
                            <div className="space l"></div>
                            <h4>GPS mọi nẻo đường</h4>
                            <p>Mioto biết rằng xe ô tô là tài sản lớn của bạn và kinh doanh cho thuê xe tự lái sẽ luôn tiềm ẩn rủi ro. Vì thế, bên cạnh viêc hỗ trợ bạn với công đoạn xác minh trước những thông tin cá nhân quan trọng của khách hàng (Giấy phép lái xe, CMND, Sổ hộ khẩu/Giấy tạm trú KT3 và điểm rating), chúng tôi đang phát triển thêm tính năng GPS tích hợp trực tiếp trên ứng dụng.</p>
                            <p>Với tính năng GPS, chủ xe có thể dễ dàng theo dõi hiện trạng và vị trí xe của mình ngay trên ứng dụng, bất cứ lúc nào và bất cứ đâu để bạn có thể hoàn toàn an tâm về chiếc xe của mình. (Mioto dự kiến triển khai tính năng GPS trong quý 2 năm 2019, hỗ trợ cài đặt cho chủ xe với giá gốc ước tính 500,000đ/thiết bị so với giá thị trường dao động 1-3 triệu đồng).</p>
                        </div>
                    </div>
                </div>
                <div className="np-bottom">
                    <div className="np-bottom-container">
                        <div className="np-main">
                            <div className="np-prev"><span>Trở Về</span><a href="/aboutus"><em>Về chúng tôi</em></a></div>
                            <div className="np-prev np-next"><span>Kế Tiếp</span><a href="/renterbenef"><em>Lợi ích thuê Xe</em></a></div>
                        </div>
                    </div>
                </div>
            </section>
            <Footer />
        </div>
    }
}