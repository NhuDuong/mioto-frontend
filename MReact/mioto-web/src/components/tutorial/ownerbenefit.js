import React from "react";

import Header from "../common/header";
import Footer from "../common/footer";
import TutorialNav from "./tutorialnav";

export default class OwnerBenefit extends React.Component {
    componentDidMount() {
        window.scrollTo(0, 0);
    }

    render() {
        return <div className="mioto-layout">
            <Header />
            <section className="body">
                <div className="body-container">
                    <TutorialNav />
                    <div className="content">
                        <h2 className="title">Chủ xe - Tại sao nên đăng kí xe tại ứng dụng Mioto?</h2>
                        <div className="content-container">
                            <ul className="list-type2">
                                <li><span className="w-ic"><i className="ict ict-cash"></i></span>
                                    <h4>Doanh thu cao</h4>
                                    <p>Tiếp cận kênh thuê xe online với số lượng lớn khách hàng tiềm năng của Mioto giúp gia tăng đáng kể thu nhập từ xe của Bạn (trung bình 5-15 triệu đồng/tháng, tùy thuộc dòng xe và số ngày cho thuê).</p>
                                </li>
                                <li><span className="w-ic"><i className="ict ict-coin"></i></span>
                                    <h4>Chi phí thấp</h4>
                                    <p>Không tốn phí đăng kí và phí duy trì. Chỉ phát sinh phí vận hành khi có giao dịch cho thuê (12%/giá thuê).</p>
                                </li>
                                <li><span className="w-ic"><i className="ict ict-coin"></i></span>
                                    <h4>Linh hoạt</h4>
                                    <p>Cho thuê xe bất cứ khi nào bạn muốn. Toàn quyền sử dụng xe khi cần.</p>
                                </li>
                                <li><span className="w-ic"><i className="ict ict-coin"></i></span>
                                    <h4>Chủ động</h4>
                                    <p>Toàn quyền thiết lập giá thuê xe và các yêu cầu riêng.</p>
                                </li>
                                <li><span className="w-ic"><i className="ict ict-shield"></i></span>
                                    <h4>An toàn</h4>
                                    <p>Dễ dàng kiểm tra lịch sử thuê xe của khách trên ứng dụng Mioto trước khi quyết định cho thuê.</p>
                                </li>
                                <li><span className="w-ic"><i className="ict ict-star"></i></span>
                                    <h4>Tiện ích vượt trội</h4>
                                    <p>Dễ dàng đăng kí và cho thuê xe trên cả 2 nền tảng Website Mioto.vn và Ứng dụng Mioto.</p>
                                    <p>Thuận tiện quản lí và theo dõi lịch trình xe với tính năng "Calendar".</p>
                                    <p>Cho phép phản hồi và đánh giá khách thuê với tính năng "Rating".</p>
                                    <p>* Dể dàng theo dõi xe của bạn với tính năng "GPS" (Tính năng dự kiến ra mắt trong năm 2018. Mioto hỗ trợ cài đặt cho chủ xe với giá gốc ước tính 500,000đ/thiết bị)</p>
                                </li>
                                <li><span className="w-ic"><i className="ict ict-gift"></i></span>
                                    <h4>Chính sách ưu đãi</h4>
                                    <p>Giá ưu đãi dành riêng cho chủ xe khi thuê xe trong cộng đồng Mioto.</p>
                                    <p>Các chương trình khuyến mãi hấp dẫn khác dành riêng cho chủ xe.</p>
                                </li>
                                <li><span className="w-ic"><i className="ict ict-support"></i></span>
                                    <h4>Đội ngũ hỗ trợ</h4>
                                    <p>Bộ phận CSKH của Mioto luôn bên cạnh đễ hỗ trợ bạn trong mọi tình huống.</p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div className="np-bottom">
                    <div className="np-bottom-container">
                        <div className="np-main">
                            <div className="np-prev"><span>Trở Về</span><a href="/renterbenef"><em>Lợi ích thuê xe</em></a></div>
                            <div className="np-prev np-next"><span>Kế Tiếp</span><a href="/carregishowto"><em>Cách Đăng Ký Xe</em></a></div>
                        </div>
                    </div>
                </div>
            </section>
            <Footer />
        </div>
    }
}