import React from "react"
import { NavLink } from "react-router-dom"

export default function SupportNav() {
    return <div className="mioto-layout">
        <section className="body">
            <div className="body-container">
                <div className="content">
                    <div className="content-container">
                        <div className="group-check active">
                            <div className="g-head">
                                <h2>
                                    <i className="ic ic-verify"></i> Giới thiệu
                                </h2>
                                <p>Thông tin cơ bản về Mioto</p>
                            </div>
                            <div className="g-body">
                                <ul>
                                    <li><span><NavLink activeClassName="active" to="/aboutus">Giới thiệu về Mioto</NavLink></span></li>
                                    <li><span><NavLink activeClassName="active" to="/features">Tính năng nổi bật</NavLink></span></li>
                                    <li><span><NavLink activeClassName="active" to="/renterbenef">Lợi ich thuê xe tự lái</NavLink></span></li>
                                    <li><span><NavLink activeClassName="active" to="/ownerbenef">Lợi ích chủ xe</NavLink></span></li>
                                    <li><span><NavLink activeClassName="active" to="/carregishowto">Cách đăng kí xe cho thuê</NavLink></span></li>
                                    <li><span><NavLink activeClassName="active" to="/renterhowto">Quy trình thuê xe tự lái</NavLink></span></li>
                                    <li><span><NavLink activeClassName="active" to="/ownerhowto">Quy trình cho thuê xe tự lái</NavLink></span></li>
                                </ul>
                            </div>
                        </div>
                        <div className="group-check active">
                            <div className="g-head">
                                <h2>
                                    <i className="ic ic-verify"></i> Chính sách & quy định
                                </h2>
                                <p>Nguyên tắc hoạt động và các quy định khi sử dụng dịch vụ tại Mioto</p>
                            </div>
                            <div className="g-body">
                                <ul>
                                    <li><span><NavLink activeClassName="active" to="/terms">Nguyên tắc chung</NavLink></span></li>
                                    <li><span><NavLink activeClassName="active" to="/regu">Quy chế hoạt động</NavLink></span></li>
                                    <li><span><a href="/privacy#canceltrip">Chính sách huỷ chuyến</a></span></li>
                                    <li><span><a href="/privacy#price">Chính sách giá</a></span></li>
                                    <li><span><a href="/privacy#payment">Chính sách thanh toán </a></span></li>
                                    <li><span><a href="/privacy#transaction">Chính sách giao nhận</a></span></li>
                                    <li><span><a href="/privacy#responsibility">Trách nhiệm </a></span></li>
                                </ul>
                            </div>
                        </div>
                        <div className="group-check active">
                            <div className="g-head">
                                <h2>
                                    <i className="ic ic-verify"></i> Câu hỏi & trả lời
                                </h2>
                                <p>Các thắc mắc thường gặp khi sử dụng dịch vụ của Mioto</p>
                            </div>
                            <div className="g-body">
                                <ul>
                                    <li><span><a href="/faqs#renter">Dành cho khách thuê xe</a></span></li>
                                    <li><span><a href="/faqs#owner">Dành cho chủ xe</a></span></li>
                                    <li><span><a href="/faqs#both">Dành chung </a></span></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
}