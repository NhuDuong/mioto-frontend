import React from "react"

import Header from "../common/header"
import Footer from "../common/footer"
import TutorialNav from "./tutorialnav"

import img_tuotrial_1 from "../../static/images/tutorial_1.png"
import img_tuotrial_2 from "../../static/images/tutorial_2.png"
import img_tuotrial_3 from "../../static/images/tutorial_3.png"

export default class RenterHowTo extends React.Component {
    componentDidMount() {
        window.scrollTo(0, 0);
    }

    render() {
        return <div className="mioto-layout">
            <Header />
            <section className="body">
                <div className="body-container">
                    <TutorialNav />
                    <div className="content">
                        <h1 className="title">Quy trình thuê xe tự lái</h1>
                        <div className="content-container">
                            <h5>Bước 1: Đăng kí tài khoản thành viên Mioto </h5>
                            <p>Truy cập vào website <b>Mioto.vn</b> hoặc tải <b>ứng dụng Mioto</b> để đăng kí thành viên bằng cách sử dụng số điện thoại di động hoặc tài khoản Facebook, Google và Email. Ngay lúc này, bạn đã có thể trải nghiêm các tính năng mới lạ và tham khảo hàng trăm mẫu mã xe trên ứng dụng Mioto.</p>
                            <p><img src={img_tuotrial_1} alt="Mioto - Thuê xe tự lái" /></p>
                            <p>*** Để có thể thuê xe từ các chủ xe trong cộng đồng Mioto, bạn chỉ cần xác thực số điện thoại di động, đảm bảo bạn có đủ các giấy tờ cá nhân, tài sản thế chấp (nếu chủ xe yêu cầu) và tiến hành đặt chiếc xe bạn yêu thích.</p>
                            <div className="space l"></div>
                            <h5>Bước 2: Tìm kiếm những chiếc xe phù hợp</h5>
                            <p>Nhập lịch trình chuyến đi và địa điểm thuê xe, Mioto sẽ nhanh chóng tìm kiếm và đề xuất các xe phù hợp đang sẵn sàng phục vụ trong khu vực xung quanh bạn.</p>
                            <p><img src={img_tuotrial_2} alt="Mioto - Thuê xe tự lái" /></p>
                            <div className="space l"></div>
                            <h5>Bước 3: Đặt xe</h5>
                            <p>Lựa chọn chiếc xe bạn yêu thích và gửi yêu cầu thuê xe đến chủ xe. Các chủ xe sẽ nhanh chóng nhận được yêu cầu, xem xét thông tin cá nhân của bạn và xác nhận đồng ý hoặc từ chối cho thuê xe trong vòng 10-15 phút. Trong thời gian chờ đợi, bạn có thể gửi thêm Yêu cầu thuê xe đến các chủ xe khác. (Bạn được phép gửi tối đa không quá 3 Yêu cầu thuê xe cho cùng một lịch trình đi và về)</p>
                            <p><img src={img_tuotrial_3} alt="Mioto - Thuê xe tự lái" /></p>
                            <p>Nếu bạn ngại chờ đợi, bạn có thể lựa chọn các xe có tính năng “Đặt xe nhanh” để đặt xe trực tiếp mà không cần thông qua sự xét duyệt từ phía chủ xe. Sau khi nhận được sự đồng ý từ phía chủ xe, bạn chỉ cần đặt cọc trước 30% bằng hình thức chuyển khoản, ví điện tử hoặc thanh toán tiền mặt trực tiếp tại văn phòng Mioto là bạn đã hoàn thành bước đặt xe.</p>
                            <div className="space l"></div>
                            <h5>Bước 4: Nhận xe</h5>
                            <p>Bạn và chủ xe liên hệ gặp nhau để nhận xe. Ở Mioto, có nhiều chủ xe sẵn sàng đem xe đến tận nơi cho bạn. Kiểm tra tình trạng và giấy tờ xe, xuất trình bản gốc các giấy tờ của bạn, kí hợp đồng, biên bản bàn giao xe, nhận chìa khóa và lái xe bắt đầu hành trình của bạn.</p>
                            <div className="space l"></div>
                            <h5>Bước 5: Sử dụng</h5>
                            <p>Trải nghiệm cảm giác lái xe như một ông chủ thật sự, tận hưởng khoảng thời gian tuyệt vời bên cạnh những người thân yêu cùng chiếc ô tô bạn yêu thích. </p>
                            <div className="space l"></div>
                            <h5>Bước 6: Trả xe</h5>
                            <p>Hết thời gian thuê, bạn hoàn trả xe giống như tình trạng và các thỏa thuận ban đầu. Kí xác nhận biên bản bàn giao, nhận lại giấy tờ để hoàn thành chuyến đi tuyệt vời của bạn. Và cũng đừng quên cho điểm rating cũng như gửi nhận xét của bạn đến chủ xe để nâng cao chất lượng phục vụ cho những hành trình sắp tới cùng Mioto nhé !</p>
                        </div>
                    </div>
                </div>
                <div className="np-bottom">
                    <div className="np-bottom-container">
                        <div className="np-main">
                            <div className="np-prev"><span>Trở Về</span><a href="/carregishowto"><em>Cách đăng kí xe cho thuê</em></a></div>
                            <div className="np-prev np-next"><span>Kế Tiếp</span><a href="/ownerhowto"><em>Quy trình cho thuê xe</em></a></div>
                        </div>
                    </div>
                </div>
            </section>
            <Footer />
        </div>
    }
}