import React from "react"

import Header from "../common/header"
import Footer from "../common/footer"
import PrivacyNav from "./privacynav"

export default class Faqs extends React.Component {
    constructor() {
		super();

		this.state = {
            isMobile: false
        }
	}

    componentDidMount() {
        window.scrollTo(0, 0);
        const isMobile = this.props.location.pathname.includes("/mobile");
		this.setState({
			isMobile: isMobile
		});
    }

    render() {
        return <div className="mioto-layout">
            {!this.state.isMobile && <Header />}
            <section className="body">
                <div className="body-container">
                    {!this.state.isMobile && <PrivacyNav />}
                    <div className="content">
                        <div className="rent-car">
                            <h2 className="title">Hỏi và trả lời</h2>
                            <div className="content-container">
                                <h4 id="renter">Dành cho khách thuê xe</h4>
                                <div className="slstitle">1. Tôi phải làm thế nào nếu muốn hủy chuyến đi sau khi đã đặt cọc tiền? Chi phí phát sinh được tính như thế nào?</div>
                                <p>- Bạn có thể hủy bỏ chuyến đi đã đặt bằng cách gửi yêu cầu hủy chuyến thông qua trang web hoặc ứng dụng di động của Mioto.</p>
                                <p>- Xem thêm chi tiết cách hủy chuyến đi tại <a href="/privacy#rentercanceltrip"> "Chính sách hủy chuyến" </a></p>
                                <div className="slstitle">2. Quy trình thanh toán của Mioto như thế nào? Có yêu cầu đặt cọc tiền trước hay không?</div>
                                <p>- Xem chi tiết quy trình thanh toán tại <a href="/privacy#payment">"Chính sách thanh toán"</a></p>
                                <div className="slstitle">3. Giá thuê xe bao gồm những gì và được tính như thế nào?</div>
                                <p>- Xem thêm chi tiết về giá thuê xe tại <a href="/privacy#price">"Chính sách giá"</a></p>
                                <div className="slstitle">4. Tôi có thể gửi yêu cầu đặt xe cho nhiều chủ xe cùng 1 lúc?</div>
                                <p>- Trong cùng 1 lúc, bạn có thể gửi nhiều yêu cầu đặt xe đến các chủ xe khác nhau.</p>
                                <div className="slstitle">5. Tôi cần lưu ý những gì khi nhận xe từ chủ xe để hạn chế các rủi ro phát sinh?</div>
                                <p>- Kiểm tra kĩ càng tình trạng xe: độ an toàn, vệ sinh xe, thực tế xe có giống như miêu tả trên ứng dụng. Bạn có quyền từ chối nhận xe nếu có bằng chứng cho thấy rằng xe thuê đang trong tình trạng thiếu an toàn.</p>
                                <p>- Kiểm tra kĩ càng và đầy đủ giấy tờ pháp lí của xe thuê bao gồm cà vẹt xe, giấy đăng kiểm còn hiệu lực, bảo hiểm xe còn hiệu lực.</p>
                                <p>- Yêu cầu chủ xe gửi lại cho bạn bản photo công chứng cà vẹt xe, bản gốc hoặc bản photo công chứng của giấy đăng kiểm và bảo hiểm xe.</p>
                                <p>- Ràng buộc chặt chẽ với chủ xe về mặt thủ tục, giấy tờ, pháp lí bằng cách giao kết hợp đồng cho thuê xe tự lái và kí kết biên bàn giao khi nhận xe cũng như khi hoàn trả xe.</p>
                                <p>- Chụp hình tình trạng xe khi nhận xe (từ 10-15 tấm, bên ngoài và nội thất bên trong) để làm bằng chứng đối chiếu trong trường hợp phát sinh tranh chấp.</p>
                                <p>- Bạn có thể ưu tiên lựa chọn các xe có logo "Ảnh đã xác thực" để yên tâm hơn về chất lượng xe.</p>
                                <p>- Bạn cũng có thể xem các đánh giá và nhận xét của các khách thuê xe trước đó về chất lượng dịch vụ của chủ xe và xe cho thuê.</p>
                                <h4 id="owner">Dành cho chủ xe</h4>
                                <div className="slstitle">1. Nếu muốn hủy chuyến xe đã xác nhận cho thuê và nhận đặt cọc từ khách thuê, tôi phải làm như thế nào? Tôi có phải chịu mất phí hay không?</div>
                                <p>- Nếu muốn hủy bỏ chuyến đi đã phê duyệt, bạn phải thông báo cho các khách thuê của bạn càng sớm càng tốt. Truy cập trang web hoặc ứng dụng di động của Mioto để thực hiện hủy bỏ chuyến đi, yêu cầu của Bạn sẽ lập tức được phê duyệt và sẽ được hoàn trả khoản phí bỏ ra trước đó đầy đủ.</p>
                                <p>- Xem thêm chi tiết cách hủy chuyến tại <a href="/privacy#ownercanceltrip"> "Chính sách hủy chuyến"</a> </p>
                                <div className="slstitle">2. Tôi muốn đăng kí xe lên Mioto cần những điều kiện gì, quy trình thế nào và tôi cần chuẩn bị những gì?</div>
                                <p>Rất nhanh chóng và đơn giản. </p>
                                <p>- Trước tiên, bạn cần chắc chắn rằng xe của bạn có đầy đủ giấy tờ pháp lí: cà vẹt xe, giấy đăng kiểm và bảo hiểm xe ô tô còn hiệu lực.</p>
                                <p>- Đăng kí tài khoản Mioto.</p>
                                <p>- Vào thự mục "Xe của tôi", nhập các thông tin theo yêu cầu như: thông số xe, mô tả và đăng tải hình ảnh xe của bạn.</p>
                                <p>- Thiết lập thời gian cho thuê, mức giá mong muốn và các yêu cầu khác của bạn đối với khách thuê. </p>
                                <p>Tìm hiểu them cách thức đăng kí xe trên Mioto tại: link hướng dẫn đăng kí xe</p>
                                <div className="slstitle">3. Thu nhập của tôi từ cho thuê xe trên Mioto được tính như thế nào ?</div>
                                <p>Thu nhập của bạn từ ứng dụng cho thuê xe Mioto được tính như sau:</p>
                                <p>Tổng thu nhập = (Giá thuê x Số ngày cho thuê - Chiết khấu + Phí vận chuyển) x (1- 12%)</p>
                                <p>* 12%: Phí vận hành hệ thống (đã bao gồm VAT)</p>
                                <p>- Đối với tất cả các giao dịch thuê xe qua ứng dụng, khách hàng sẽ đặt cọc 30% tổng tiền thuê xe cho Mioto, 70% số tiền còn lại khách hàng sẽ gửi trực tiếp cho đối tác chủ xe ngay khi nhận xe. Hàng tháng, Mioto sẽ gửi báo cáo doanh thu cho đối tác chủ xe cũng như quyết toán phần chênh lệch giữa số tiền đối tác thực nhận và số tiền trong báo cáo doanh thu.</p>
                                <div className="slstitle">4. Trường hợp xe của tôi bị sự cố gây thiệt hại trong khoảng thời gian khách sử dụng xe thì xử lí như thế nào?</div>
                                <p>- Bạn và khách thuê xe tự thỏa thuận và thương lượng mức đền bù với nhau trên tinh thần hợp tác, dựa trên các điều khoản trong hợp đồng cho thuê xe tự lái mà hai bên đã kí kết.</p>
                                <p>- Bên cạnh đó, bạn cần liên hệ ngay với đơn vị cung cấp dịch vụ bảo hiểm để thông báo về tình hình và nhận sự hướng dẫn cần thiết để làm thủ tục bồi thường (hãy chắc chắn rằng hợp đồng bảo hiểm đã kí có quy định rõ các điều khoản xe của bạn được bảo vệ và bồi thường khi kinh doanh cho thuê xe).</p>
                                <p>- Ngoài ra, trong trường hợp bạn cần sự hỗ trợ, Mioto có liên kết với các đối tác garage xe có uy tín ở TPHCM để bạn có thể yên tâm đem xe đến giám định mức độ tổn thất và xác định số tiền bồi thường với khách thuê một cách nhanh chóng và hợp lí nhất.</p>
                                <div className="slstitle">5. Sau khi kết thúc hợp đồng thì tôi nhận được giấy báo phạt nguội do lỗi của khách thuê xe, vậy tôi cần xử lí trường hợp này thế nào ?</div>
                                <p>- Hiện nay, các lỗi phạt nguội thông thường sẽ được cập nhật lên website của Cục đăng kiểm Việt Nam hoặc Sở giao thông vận tải TPHCM trong vòng 1-2 tuần kể từ thời điểm xe vi phạm. Sau đó, giấy báo phạt sẽ được gửi về cơ quan công an địa phương để chuyển đến địa chỉ của bạn. Vì thế, bạn có thể dể dàng kiểm tra xe của mình có bị vi phạm hay không bằng cách truy cập thông tin trên website của <a href="http://www.vr.org.vn/ptpublic_web/ThongTinPTPublic.aspx">Cục đăng kiểm Việt Nam</a> hoặc website của <a href="http://giaothong.hochiminhcity.gov.vn/tracuuvipham/">Sở giao thông thành phố Hồ Chí Minh</a></p>
                                <p>- Trong trường hợp phát hiện xe của bạn có lỗi vi phạm được đăng trên trang web hoặc nhận được giấy báo từ phía cơ quan công an, bạn cần tuần tự làm theo các bước sau:</p>
                                <p>+ Kiểm tra kĩ thời gian và địa điểm vi phạm. Xác định chắc chắn ràng trong khoảng thời gian này bạn có đang cho thuê xe hay không, nếu đang cho thuê thì khách hàng nào đang thuê xe của bạn (bạn có thể đối chiếu lại với thời gian trong hợp đồng thuê xe đã kí kết hoặc thời gian trong biên bản bàn giao)</p>
                                <p>+ Bước tiếp theo, bạn cần liên lạc với khách thuê xe để thông báo về sự việc, lỗi vi phạm, số tiền phạt, thời gian và địa điểm vi phạm cụ thể.</p>
                                <p>+ Bạn cần gửi đường link trên website hoặc hình ảnh giấy báo vi phạm và các thông tin liên quan (hợp đồng thuê xe, biên bản bàn giao) qua email hoặc các kênh liên lạc khác cho khách thuê xe để làm bằng chứng cho sự việc.</p>
                                <p>+ Khách hàng đồng ý xác nhận. Bạn và khách thuê xe tự thỏa thuận phương thức thanh toán với nhau.</p>
                                <p>+ Trong trường hợp bạn có đầy đủ bằng chứng cho thấy lỗi vi phạm thuộc về khách thuê nhưng không thể liên lạc được với khách thuê, hoặc khách thuê không đồng ý thanh toán tiền vi phạm, bạn có thể liên hệ với bộ phận chăm sóc khách hàng của Mioto để thông báo về sự việc và gửi kèm toàn bộ bằng chứng và thông tin liên quan cho Mioto. Mioto sẽ tiến hành kiểm tra và liên lạc với khách thuê xe để xác minh sự việc bạn cung cấp, nếu chứng minh được khách thuê cố tình không hợp tác hoặc không thanh toán khoản vi phạm phát sinh do lỗi của mình, Mioto sẽ khóa vĩnh viễn tài khoản thành viên của khách thuê trên ứng dụng.</p>
                                <p>*** Mioto khuyến nghị các chủ xe nên cập nhật định kì ít nhất 1 lần/1 tuần các thông tin về các khoản phat nguội trên website của cục đăng kiểm để có thể thông báo đến khách thuê trong thời gian sớm nhất. Quá 30 ngày kể từ khi kết thúc hợp đồng thuê xe, khách thuê sẽ không có trách nhiệm và nghĩa vụ thanh toán các khoản tiền phạt phát sinh nếu có cho chủ xe. Mioto đã quy định rõ điều này trong mẫu biên bản bàn giao đã gửi qua email cho các chủ xe khi đăng kí xe thành công lần đầu.</p>
                                <div className="slstitle">6. Tôi phải làm gì trong trường hợp xấu nhất là xe bị mất cắp hoặc bị đem cầm cố, thế chấp?</div>
                                <p>- Chúng tôi biết rằng xe ô tô là tài sản lớn của bạn và kinh doanh cho thuê xe tự lái sẽ luôn tiềm ẩn rủi ro và Mioto luôn cố gắng hoàn thiện các quy trình cho thuê để hỗ trợ tốt nhất cho các chủ xe. Tuy nhiên, những rủi ro xe bị mất cắp hoặc bị đem đi cầm cố thế chấp cũng không thể nào loại bỏ hoàn toàn. Khi trường hợp trên không may xảy ra, các chủ xe của Mioto cần:</p>
                                <p>+ Xác nhận chắc chắn rằng đã quá thời gian cho thuê và bạn đã không thể liên lạc được với khách thuê xe, hoặc bạn có đầy đủ bằng chứng chứng minh xe của bạn đang trong tình trạng bị mất cắp hoặc đang cầm cố.</p>
                                <p>+ Trình báo ngay với cơ quan công an khu vực bạn đang sống. Trình bày rõ ràng về sự việc, gửi các bằng chứng cho cơ quan công an gồm hợp đồng cho thuê xe đã được kí kết, biên bản bàn giao, các giấy tờ liên quan và tài sản đặt cọc của khách thuê xe để nhờ sự hỗ trợ.</p>
                                <p>+ Thông báo ngay cho bên bảo hiểm của bạn về sự cố xảy ra để nhận sự hướng dẫn về các trình tự xác minh cũng như các thủ tục bồi thường.</p>
                                <p>+ Liên hệ ngay với bộ phận hỗ trợ khách hàng của Mioto để thông báo về tình hình cũng như nhận sự tư vấn và trợ giúp kịp thời.</p>
                                <p>Để tự bảo vệ tài sản của bản thân, các chủ xe cần nghiêm túc thực hiện các lưu ý sau: </p>
                                <p>- Xác nhận chắc chắn với đại lí cung cấp dịch vụ bảo hiểm rằng xe của bạn được bảo vệ và bồi thường khi kinh doanh cho thuê xe.</p>
                                <p>- Ràng buộc chặt chẽ về mặt pháp lí với khách thuê xe bằng việc giao kết bằng văn bản "Hợp đồng cho thuê xe tự lái" và kí kết "Biên bản bàn giao" trước và sau khi giao xe.</p>
                                <p>Tuyệt đối không giao cà vẹt xe bản chính cho khách thuê.</p>
                                <p>- Kiểm tra kĩ càng giấy tờ pháp lí, giữ lại sổ hộ khẩu / KT3 và tài sản đặt cọc của khách thuê xe. Bạn có toàn quyền từ chối giao xe nếu khách thuê xe không xuất trình đầy đủ các giấy tờ pháp lí, tài sản thế chấp hoặc không đồng ý kí hợp đồng hoặc biên bản bàn giao.</p>
                                <div className="slstitle">7. Trách nhiệm của Mioto trong các trường hợp xảy ra sự cố ngoài ý muốn như xe bị cầm cố, thế chấp, bị bắt giữ khi được dùng để vận chuyển ma túy, hàng quốc cấm, hoặc gây tai nạn?</div>
                                <p>Hiện tại, Mioto chỉ đóng vai trò là sàn giao dịch thương mại điện tử về cho thuê xe ô tô, là cầu nối giữa các chủ xe và khách hàng có nhu cầu thuê xe. Về cơ bản, mọi thủ tục và toàn bộ các vấn đề phát sinh liên quan đến giao dịch cho thuê xe giữa chủ xe và khách thuê sẽ do hai bên tự thỏa thuận, kí hợp đồng và chịu trách nhiệm với nhau.</p>
                                <p>Trong trường hợp có xảy ra sự cố ngoài ý muốn như xe bị cầm cố, thế chấp, bị bắt khi được dùng để vận chuyển ma túy, hàng quốc cấm hoặc gây ra tai nạn, Mioto sẽ cố gắng hỗ trợ tốt nhất các chủ xe trong khả năng của mình, giới hạn ở việc hướng dẫn chủ xe các thủ tục cần thiết để trình báo với cơ quan công an và các cơ quan có thẩm quyền, cung cấp các thông tin nếu có liên quan đến thành viên thuê xe hoặc các thông tin khác nếu có yêu cầu từ cơ quan chức năng, và tiến hành khóa vĩnh viễn tài khoản thành viên vi phạm.</p>
                                <p>Để bảo vệ tốt nhất tài sản của mình và giảm thiểu tối đa các rủi ro có thể xảy ra, các chủ xe cần ràng buộc chặt chẽ về mặt pháp lí bằng việc giao kết bằng văn bản "Hợp đồng cho thuê xe tự lái", "Biên bản bàn giao xe" (có thể tham khảo sử dụng mẫu hợp đồng và biên bản bàn giao do Mioto đề xuất hoặc có thể sử dụng mẫu riêng của nhà xe), xác minh đầy đủ thông tin và các giấy tờ cá nhân của khách thuê, giữ lại tài sản đặt cọc và các giấy tờ cần thiết (và tuyệt đối không giao cà vẹt xe bản chính cho khách thuê). Các chủ xe cũng cần trang bị cho xe thiết bị định vị GPS để có thể theo dõi và kiểm tra vị trí xe thường xuyên nhằm có các phương án xử lí kịp thời. </p>
                                <p>Thời gian tới, nhằm bảo vệ một cách toàn diện nhất quyền lợi của các chủ xe cũng như khách thuê, Mioto đang xúc tiến làm việc với các đối tác bảo hiểm để triển khai các sản phẩm bảo hiểm đối với dịch vụ thuê xe tự lái theo ngày, áp dụng cho tất cả các giao dịch cho thuê được thực hiện thông qua ứng dụng Mioto. </p>
                                <p>Mioto hy vọng sẽ sớm hoàn thiện và triển khai gói giải pháp này trong thời gian sớm nhất.</p>
                                <h4 id="both">Dành chung cho chủ xe và khách thuê</h4>
                                <div className="slstitle">1. Chức năng "Đặt xe nhanh" là gì ?</div>
                                <p>- Trong cộng đồng Mioto, có một số chủ xe luôn có sẵn xe để phục vụ và muốn cho thuê mọi lúc, họ sẽ thiết lập chức năng này trên ứng dụng và đưa vào danh sách xe "Đặt xe nhanh" trên ứng dụng.</p>
                                <p>- Thông thường, quy trình đặt xe sẽ trải qua tuần tự các bước “Tìm kiếm” xe -> Khách hàng gửi “Yêu cầu thuê xe” -> Chủ xe “Phê duyệt” -> Khách thuê xe “Đặt cọc” và hoàn tất. Nhờ tính năng “Đặt xe nhanh”, sau khi khách thuê gửi “Yêu cầu thuê xe” sẽ được chuyển ngay sang bước “Đặt cọc” mà không cần đợi sự phê duyệt từ phía chủ xe. Chính vì vậy, sẽ giúp rút ngắn đáng kể thời gian đặt xe của khách thuê.</p>
                                <p>- Đối với chủ xe: Để đăng kí chức năng này, bạn cần truy cập tài khoản Mioto, vào “Xe của tôi”, vào trang thiết lập và chọn mục "Đặt xe nhanh".</p>
                                <p>- Đối với khách thuê xe: Nếu không muốn tốn thời gian chờ đợi sự phê duyệt từ chủ xe, bạn có thể dễ dàng sử dụng chức năng này. Trong danh sách xe đề xuất tại bước tìm kiếm, bạn chỉ cần lựa chọn các xe có logo "Đặt xe nhanh", gửi “Yêu cầu đặt xe” và bạn sẽ được chuyển ngay sang bước “Đặt cọc”.</p>
                                <div className="slstitle">2. Chức năng "Ảnh đã xác thực" là gì?</div>
                                <p>- Mioto biết rằng những hình ảnh miêu tả cụ thể, phản ánh thật tình trạng của xe sẽ giúp giao dịch, thỏa thuận thuê xe giữa khách thuê và chủ xe diễn ra dễ dàng và thuận lợi hơn. Mioto mong muốn mang đến những giao dịch uy tín, đem lại tin tưởng cho khách hàng khi thuê xe, chính vì vậy, Mioto có đội ngũ chuyên viên thực hiện việc xác nhận các hình ảnh và tình trạng xe mà Chủ xe cung cấp có thực sự đúng như thực tế. Những hình ảnh xe đã được Mioto kiểm tra sẽ được gắn logo “Ảnh đã xác thực” để giúp khách thuê xe dễ dàng nhận biết và có sự chọn lựa tốt hơn.</p>
                                <div className="slstitle">3. Chức năng GPS của Mioto hoạt động như thế nào?</div>
                                <p>- GPS là tính năng Mioto đang phát triển và dự kiến triển khai trong quý 2/2019. Mioto sẽ hỗ trợ cài đặt GPS cho chủ xe với giá ước tính ~500,000đ/thiết bị.</p>
                                <p>- Với tính năng GPS, chủ xe có thể dễ dàng theo dõi vị trí xe của mình ngay trên ứng dụng bất cứ lúc nào và bất cứ đâu. Qua đó, Mioto mong muốn nâng cao hơn nữa mức độ an toàn đối với công việc kinh doanh cho thuê xe của khách hàng.</p>
                                <div className="slstitle">4. Sau khi đặt xe thành công, chủ xe và khách cần chuẩn bị các giấy tờ gì khi tiến hành bàn giao xe ?</div>
                                <p>- Sau khi đã hoàn thành các bước và được xác nhận thuê xe thành công, chủ xe sẽ liên lạc với khách thuê để thỏa thuận thời gian, cách thức giao xe.</p>
                                <p>- Khách thuê cần chuẩn bị đầy đủ các giấy tờ pháp lí gồm: CMND, sổ hộ khẩu / KT3, bằng lái xe, tài sản đặt cọc để thuê xe.</p>
                                <p>-Khi nhận xe, khách thuê sẽ đặt cọc cho chủ xe Hộ khẩu/KT3, tiền mặt (15 triệu đồng hoặc tùy thỏa thuận với chủ xe) hoặc tài sản có giá trị tương đương (xe máy và cà vẹt xe) trước khi nhận xe.</p>
                                <p>- Chủ xe cần chuẩn bị đầy đủ giấy tờ để giao cho khách thuê bao gồm bản photo công chứng cà vẹt xe, bản gốc hoặc bản photo công chứng giấy đăng kiểm và bảo hiểm xe còn hiệu lực, hợp đồng cho thuê xe và biên bản bàn giao xe.</p>
                                <p>- Lưu ý: khách và chủ xe cần giao kết hợp đồng thuê xe tự lái và kí biên bản bàn giao xe (tình trạng xe lúc giao và lúc hoàn trả) để phân định rõ ràng quyền và trách nhiệm của mỗi bên cũng như dùng làm bằng chứng để giải quyết các tranh chấp có thể xảy ra giữa 2 bên (chủ xe có thể tham khảo sử dụng mẫu "Hợp đồng cho thuê xe tự lái" và "Biên bản bàn giao xe" của Mioto được gửi tự động qua email của chủ xe khi đăng kí xe thành công lần đầu tiên).</p>
                                <p>- Mọi thắc mắc có thể liên hệ trực tiếp bộ chăm sóc khách hàng hoặc bộ phận kinh doanh của Mioto được tư vấn và hỗ trợ.</p>
                                <div className="slstitle">5. Thời gian hoạt động của Mioto như thế nào?</div>
                                <p>- Giờ làm việc của Mioto là từ 8h30 đến 18 giờ các ngày trong tuần từ thứ 2 đến thứ 7. CN nghỉ.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            {!this.state.isMobile && <Footer />}
        </div>
    }
}