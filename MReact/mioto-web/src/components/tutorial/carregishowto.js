import React from "react"

import Header from "../common/header"
import Footer from "../common/footer"
import TutorialNav from "./tutorialnav"

import img_tuotrial from "../../static/images/tutorial_1.png"

export default class CarRegisHowTo extends React.Component {
    componentDidMount() {
        window.scrollTo(0,0);
    }
    
    render() {
        return (
            <div className="mioto-layout">
                <Header />
                <section className="body">
                    <div className="body-container">
                        <TutorialNav />
                        <div className="content">
                            <h2 className="title">Tôi đăng kí xe cho thuê như thế nào?</h2>
                            <div className="content-container">
                                <h5>Bước 1: Đăng nhập </h5>
                                <p>Đăng nhập vào tài khoản của mình, nếu bạn chưa có tài khoản thì có thể đăng ký tạo tài khoản bằng Email, Facebook hoặc tài khoản Google.</p>
                                <p><img src={img_tuotrial} alt="Mioto - Thuê xe tự lái" /></p>
                                <div className="space l"></div>
                                <h5>Bước 2: Đăng kí xe</h5>
                                <p>Chọn mục "Đăng kí xe" và điền các thông tin về chiếc xe bạn muốn đăng kí. </p>
                                <p>(gồm hình ảnh xe, dòng xe, số km đã đi, bảo hiểm xe, các giấy tờ xe liên quan, các tiện ích đi kèm, bảng giá cho thuê, hình thức khuyến mãi, phương thức và phí giao nhận xe, danh sách các ngày cho thuê)</p>
                                <div className="space l"></div>
                                <h5>Bước 3: Chờ phê duyệt</h5>
                                <p>Trong thời gian 1 ngày làm việc. Nếu thỏa mãn các tiêu chí, xe của bạn sẽ được chấp thuận đăng kí trên sàn giao dịch.</p>
                                <p>Trường hợp các xe không đáp ứng tiêu chí tuyển lựa, công ty sẽ gửi email thông báo lí do đến bạn.</p>
                            </div>
                        </div>
                    </div>
                    <div className="np-bottom">
                        <div className="np-bottom-container">
                            <div className="np-main">
                                <div className="np-prev"><span>Trở Về</span><a href="/ownerbenef"><em>Lợi ích chủ xe</em></a></div>
                                <div className="np-prev np-next"><span>Kế tiếp</span><a href="/renterhowto"><em>Quy trình thuê xe</em></a></div>
                            </div>
                        </div>
                    </div>
                </section>
                <Footer />
            </div>
        )
    }
}