import React from "react"

import Header from "../common/header"
import Footer from "../common/footer"


import web_payment_1 from "../../static/images/payment-guide/web-payment-1.png"
import web_payment_2 from "../../static/images/payment-guide/web-payment-2.png"
import web_payment_3 from "../../static/images/payment-guide/web-payment-3.png"
import web_payment_4 from "../../static/images/payment-guide/web-payment-4.png"
import web_payment_5 from "../../static/images/payment-guide/web-payment-5.png"
import web_payment_6 from "../../static/images/payment-guide/web-payment-6.png"

import app_payment_1 from "../../static/images/payment-guide/app-payment-1.png"
import app_payment_2 from "../../static/images/payment-guide/app-payment-2.png"
import app_payment_3 from "../../static/images/payment-guide/app-payment-3.png"
import app_payment_4 from "../../static/images/payment-guide/app-payment-4.png"
import app_payment_5 from "../../static/images/payment-guide/app-payment-5.png"
import app_payment_6 from "../../static/images/payment-guide/app-payment-6.png"


export default class PaymentHowTo extends React.Component {
	constructor() {
		super();

		this.state = {
            isMobile: false
        }
	}

	componentDidMount() {
		window.scrollTo(0, 0);
		const isMobile = this.props.location.pathname.includes("/mobile");
		this.setState({
			isMobile: isMobile
		});
	}
	render() {
		return (
			<div className="mioto-layout">
				{!this.state.isMobile && <Header />}
				<section className="body">
					<div className="payment-guide__sect">
						<div className="m-container"> 
							<h3 className="n-title">Hướng Dẫn Thanh Toán </h3>
							<div className="payment-guide-content"> 
								<p>Để hoàn tất quá trình đặt xe, bạn cần thanh toán đặt cọc 30% giá trị chuyến đi theo một trong các hình thức sau:</p>
								<ul className="list-method">
									<li><a href="#visaMethod">Thanh toán qua thẻ tín dụng/thẻ ghi nợ VISA, Master</a></li>
									<li><a href="#atmMethod">Thanh toán qua thẻ ATM đã đăng kí thanh toán trực tuyến</a></li>
									<li><a href="#storeMethod">Thanh toán tiền mặt tại cửa hàng tiện lợi gần nhà (có chấp nhận thanh toán qua Payoo)</a></li>
									<li><a href="#transferMethod">Thanh toán bằng hình thức chuyển khoản ngân hàng</a></li>
									<li><a href="#officeMethod">Thanh toán bằng tiền mặt tại văn phòng Mioto</a></li>
								</ul>
								<p className="note">Hiện tại, trường hợp nhiều khách đặt xe cùng một thời điểm, hệ thống sẽ ưu tiên khách hàng thanh toán sớm, vì vậy nên bạn vui lòng đặt cọc trong thời gian sớm nhất bạn nhé.</p>
							</div>
							<div className="payment-guide-detail"> 
								<div className="box-classify d-flex">
									<div className="left"> 
										<h4 className="ls-title">WEB </h4><img className="img-fluid" src={web_payment_1}/>
									</div>
									<div className="right"> 
										<h4 className="ls-title">APP </h4><img className="img-fluid" src={app_payment_1} />
									</div>
								</div>
								<div className="content-guide"> 
									<div className="method-item" id="visaMethod">
										<h4 className="method-name">Thanh toán qua thẻ tín dụng/thẻ ghi nợ VISA, Master</h4>
										<p>Bạn cần có thẻ Visa, Master để thanh toán bằng hình thức này. Các bược thực hiện khá đơn giản:</p>
										<p>Chọn 1 trong 2 cổng thanh toán Payoo hoặc VTC Pay - Bấm thanh toán để chuyển đến cổng thanh toán - Nhập các thông tin trên thẻ và hoàn tất quá trình thanh toán.</p>
										<div className="d-flex"> 
											<div className="left"> <img className="img-fluid" src={web_payment_2}/></div>
											<div className="right"><img className="img-fluid" src={app_payment_2} /></div>
										</div>
									</div>
									<div className="method-item" id="atmMethod">
										<h4 className="method-name">Thanh toán qua thẻ ATM đã đăng kí thanh toán trực tuyến</h4>
										<p>Thẻ của bạn phải đăng ký dịch vụ thanh toán trực tuyến với ngân hàng để thực hiện thanh toán bằng hình thức này.</p>
										<p>Chọn 1 trong 2 cổng thanh toán Payoo hoặc VTC Pay - Bấm thanh toán để chuyển đến cổng thanh toán - Nhập các thông tin trên thẻ và hoàn tất quá trình thanh toán.</p>
										<div className="d-flex"> 
											<div className="left"> <img className="img-fluid" src={web_payment_3} /></div>
											<div className="right"><img className="img-fluid" src={app_payment_3} /></div>
										</div>
									</div>
									<div className="method-item" id="storeMethod">
										<h4 className="method-name">Thanh toán tiền mặt tại cửa hàng tiện lợi gần nhà (có chấp nhận thanh toán qua Payoo)</h4>
										<p>Trước tiên, bạn cần lấy mã code thanh toán payoo bằng cách bấm "Đặt chỗ". Sau đó, bạn chỉ cần chụp lại mã code và đem đến cửa hàng tiện lợi (có chấp nhận thanh toán payoo - vui lòng xem link đính kèm để xem các địa điểm gần nhất) và tiến hành thanh toán bạn nhé.</p>
										<div className="d-flex"> 
											<div className="left"> <img className="img-fluid" src={web_payment_4} /></div>
											<div className="right"><img className="img-fluid" src={app_payment_4} /></div>
										</div>
									</div>
									<div className="method-item" id="transferMethod">
										<h4 className="method-name">Thanh toán bằng hình thức chuyển khoản ngân hàng</h4>
										<p>Trước tiên, bạn cần bấm "Đặt chỗ" để xác nhận hình thức thanh toán chuyển khoản. Sau đó, tiến hành chuyển khoản qua tài khoản ngân hàng của Mioto trong thời gian sớm nhất bạn nhé.</p>
										<div className="d-flex"> 
											<div className="left"> <img className="img-fluid" src={web_payment_5} /></div>
											<div className="right"><img className="img-fluid" src={app_payment_5} /></div>
										</div>
									</div>
									<div className="method-item" id="officeMethod">
										<h4 className="method-name">Thanh toán bằng tiền mặt tại văn phòng Mioto</h4>
										<p>Trước tiên, bạn cần bấm "Đặt chỗ" để xác nhận hình thức thanh toán tại văn phòng Mioto. Sau đó, tiến hành lên văn phòng thanh toán trực tiếp bạn nhé.</p>
										<p>Lưu ý hiện tại, Mioto chỉ đang có văn phòng tại TPHCM nên hình thức thanh toán này tạm thời chỉ áp dụng cho các khách hàng tại khu vực TPHCM.</p>
										<div className="d-flex"> 
											<div className="left"> <img className="img-fluid" src={web_payment_6} /></div>
											<div className="right"><img className="img-fluid" src={app_payment_6} /></div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				{!this.state.isMobile && <Footer />}
			</div>
		)
	}
}