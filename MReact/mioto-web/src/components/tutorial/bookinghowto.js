import React from "react"

import Header from "../common/header"
import Footer from "../common/footer"

import web_booking_1 from "../../static/images/tutorial-booking/login.png"
import web_booking_2 from "../../static/images/tutorial-booking/sign-up.png"
import web_booking_3 from "../../static/images/tutorial-booking/search-home.png"
import web_booking_4 from "../../static/images/tutorial-booking/destination.png"
import web_booking_5 from "../../static/images/tutorial-booking/feature-cars.png"
import web_booking_6 from "../../static/images/tutorial-booking/car-finding.png"
import web_booking_7 from "../../static/images/tutorial-booking/filter-1.png"
import web_booking_8 from "../../static/images/tutorial-booking/filter-2.png"
import web_booking_9 from "../../static/images/tutorial-booking/detail-cars.png"
import web_booking_10 from "../../static/images/tutorial-booking/location.png"
import web_booking_11 from "../../static/images/tutorial-booking/info-car.jpg"
import web_booking_12 from "../../static/images/tutorial-booking/booking-sidebar.png"
import web_booking_13 from "../../static/images/tutorial-booking/promotion.png"
import web_booking_14 from "../../static/images/tutorial-booking/confirm-booking.jpg"


import app_booking_1 from "../../static/images/tutorial-booking/app-login.png"
import app_booking_2 from "../../static/images/tutorial-booking/app-signup.png"
import app_booking_3 from "../../static/images/tutorial-booking/app-search-home.PNG"
import app_booking_4 from "../../static/images/tutorial-booking/app-destination.png"
import app_booking_5 from "../../static/images/tutorial-booking/app-feature-cars.png"
import app_booking_6 from "../../static/images/tutorial-booking/app-filter-1.png"
import app_booking_7 from "../../static/images/tutorial-booking/app-filter-2.png"
import app_booking_8 from "../../static/images/tutorial-booking/app-detail-cars.png"
import app_booking_9 from "../../static/images/tutorial-booking/app-location.png"
import app_booking_10 from "../../static/images/tutorial-booking/app-info.png"
import app_booking_11 from "../../static/images/tutorial-booking/app-booking-sidebar.png"
import app_booking_12 from "../../static/images/tutorial-booking/app-promotion.png"
import app_booking_13 from "../../static/images/tutorial-booking/app-confirm-booking.png"




export default class BookingHowTo extends React.Component {
	constructor() {
		super();

		this.state = {
            isMobile: false
        }
	}

	componentDidMount() {
		const isMobile = this.props.location.pathname.includes("/mobile");
		this.setState({
			isMobile: isMobile
		});

		this.swiper = new window.Swiper(this.refs.swiper, {
			slidesPerView: 5,
      	spaceBetween: 24,
      	threshold: 15,
      	loop: false,
      	speed: 600,
      	slidesOffsetBefore: 0,
      	slidesOffsetAfter: 0,
      	preventClicksPropagation: true,
      	preventClicks: true,
      	simulateTouch: 0,
				breakpoints: {
				991: {
					slidesPerView: 3.5,
					spaceBetween: 15,
					simulateTouch: 1,
					preventClicksPropagation: false,
					preventClicks: false,
					autoplay: 2000,
				},
				767: {
					slidesPerView: 2.5,
					spaceBetween: 15,
					autoplay: 2000,
				},
				480: {
					slidesPerView: 1.5,
					spaceBetween: 10,
					autoplay: 2000,
				}
			}
		})
		
		window.scrollTo(0, 0);
	}
	render() {

		return (
			<div className="mioto-layout">
				{!this.state.isMobile && <Header />}
				<section className="body">
					<div className="payment-guide__sect"> 
						<div className="m-container"> 
							<h3 className="n-title">Hướng Dẫn Đặt Xe</h3>
							<div className="tutorial swiper-container step-box__wrap" ref="swiper">
								<div className="swiper-wrapper">
									<div className="step-box__item swiper-slide">
										<div className="step-detail">
											<h4 className="title">Khách thuê</h4>
											<p className="short-desc">Gửi yêu cầu thuê xe</p>
										</div>
										<div className="step-img">
											<div className="box-desc">
												<p>Đặt xe qua ứng dụng</p>
											</div>
											<div className="ict ict-mioto" />
										</div>
									</div>
									<div className="step-box__item swiper-slide">
										<div className="step-detail">
											<h4 className="title">Chủ xe</h4>
											<p className="short-desc">Phê duyệt "Yêu cầu thuê xe"</p>
										</div>
										<div className="step-img">
											<div className="box-desc">
												<p>Phê duyệt qua ứng dụng</p>
											</div>
											<div className="ict-large ict-allow" />
										</div>
									</div>
									<div className="step-box__item swiper-slide">
										<div className="step-detail">
											<h4 className="title">Khách thuê</h4>
											<p className="short-desc">Nhận phản hồi "Yêu cầu thuê xe"</p>
										</div>
										<div className="step-img">
											<div className="box-desc">
												<p>Nhận thông báo qua Ứng dụng &amp; Tin nhắn SMS</p>
											</div>
											<div className="ict ict-notif" />
										</div>
									</div>
									<div className="step-box__item swiper-slide">
										<div className="step-detail">
											<h4 className="title">Khách thuê</h4>
											<p className="short-desc">Tiến hành Đặt cọc</p>
										</div>
										<div className="step-img">
											<div className="box-desc">
												<p>Đặt cọc 30% chuyến đi qua Ứng dụng</p>
											</div>
											<div className="ict-large ict-deposit" />
										</div>
									</div>
									<div className="step-box__item swiper-slide">
										<div className="step-detail">
											<h4 className="title">Chủ xe và Khách thuê</h4>
											<p className="short-desc">Hoàn thành đặt xe</p>
										</div>
										<div className="step-img">
											<div className="box-desc">
												<p>Nhận thông tin SĐT - Liên hệ xác minh thủ tục</p>
											</div>
											<div className="ict-large ict-send-car" />
										</div>
									</div>
								</div>
							</div>
							<div className="link-step-by-step">
								<a className="text-primary" href="#loginMioto">1. Đăng nhập hoặc đăng ký tài khoản</a>
								<a className="text-primary" href="#searchCars">2. Tìm kiếm xe</a>
								<a className="text-primary" href="#useFilter">3. Sử dụng bộ lọc để tìm kiếm xe mong muốn</a>
								<a className="text-primary" href="#sendRequest">4. Lựa chọn xe mong muốn và gửi yêu cầu thuê xe</a>
								<a className="text-primary" href="#deposit">5. Thanh toán đặt cọc</a>
							</div>
							<div className="payment-guide-content"> 
								<p>Bạn có thể đặt xe trực tuyến qua website Mioto.vn hoặc ứng dụng Mioto thông qua các bước đặt xe cơ bản</p>
								<p>Vui lòng tham khảo thông tin chi tiết về từng bước tìm kiếm và đặt xe như sau:</p>
							</div>
							<div className="payment-guide-detail"> 
								<div className="content-guide"> 
									<div className="method-item" id="loginMioto">
										<h4 className="method-name">1. Đăng nhập hoặc đăng ký tài khoản</h4>
										<p>Bạn vui lòng đăng nhập bằng tài khoản đã có ở Mioto hoặc đăng nhập thông qua Facebook/Google. Trong trường hợp chưa đăng ký tài khoản, bạn có thể chọn dòng "Đăng kí ngay" để tạo tài khoản tại Mioto.vn</p>
										<p>Sau khi đã hoàn tất, bạn có thể bấm vào chữ "Tạo tài khoản" để hoàn tất quá trình </p>
										<div className="box-classify d-flex"> 
											<div className="left"> 
												<h4 className="ls-title">WEB </h4><img className="img-fluid" src={web_booking_1} /><img className="img-fluid" src={web_booking_2} />
											</div>
											<div className="right">
												<h4 className="ls-title">APP </h4><img className="img-fluid" src={app_booking_1} /><img className="img-fluid" src={app_booking_2} />
											</div>
										</div>
									</div>
									<div className="method-item" id="searchCars">
										<h4 className="method-name">2. Tìm kiếm xe</h4>
										<p>Bạn có thể tìm xe theo 3 cách sau:</p>
										<p className="method-title">a. Tìm theo địa chỉ bạn nhập ở thanh tìm kiếm</p>
										<div className="box-classify d-flex"> 
											<div className="left"> 
												<h4 className="ls-title">WEB </h4><img className="img-fluid" src={web_booking_3} />
											</div>
											<div className="right">
												<h4 className="ls-title">APP</h4><img className="img-fluid" src={app_booking_3} />
											</div>
										</div>
										<p className="method-title">b. Tìm theo danh mục các địa điểm nổi bật</p>
										<div className="box-classify d-flex"> 
											<div className="left"> 
												<h4 className="ls-title">WEB </h4><img className="img-fluid" src={web_booking_4} />
											</div>
											<div className="right">
												<h4 className="ls-title">APP</h4><img className="img-fluid" src={app_booking_4} />
											</div>
										</div>
										<p className="method-title">c. Tìm theo danh mục các xe nổi bật được thuê nhiều trên Mioto</p>
										<div className="box-classify d-flex"> 
											<div className="left"> 
												<h4 className="ls-title">WEB </h4><img className="img-fluid" src={web_booking_5} />
											</div>
											<div className="right">
												<h4 className="ls-title">APP</h4><img className="img-fluid" src={app_booking_5} />
											</div>
										</div>
									</div>
									<div className="method-item" id="useFilter">
										<h4 className="method-name">3. Sử dụng bộ lọc để tìm kiếm xe mong muốn</h4>
										<p> <span className="fontWeight-5">a. Sắp xếp: </span>Hệ thống đang mặc định sắp xếp các xe theo chế độ tối ưu. Bạn có thể lựa chọn sắp xếp xe theo giá thấp đến giá cao, khoảng cách gần nhất hoặc các xe có điểm đánh giá cao nhất.</p>
										<p> <span className="fontWeight-5">b. Mức giá: </span>Hệ thống đang mặc định thể hiện xe tại mọi mức giá khác nhau. Bạn có thể thu gọn danh sách các xe nằm trong vùng giá mong muốn bằng cách di chuyển thanh mức giá.</p>
										<p> <span className="fontWeight-5">c. Loại xe: </span>Hệ thống đang mặc định hiển thị tất cá xe 4 chỗ và 7 chỗ. Bạn có thể lựa chọn chỉ xem riêng danh sách xe 4 chỗ hoặc danh sách xe 7 chỗ bằng cách bấm vào ô tương ứng.</p>
										<p> <span className="fontWeight-5">d. Hãng xe:</span> Hệ thống đang mặc định hiển thị tất cả xe của các hãng khác nhau. bạn có thể lựa chọn chỉ xem riêng danh sách xe của một hãng yêu thích bằng cách bấm vào tên của hãng xe. </p>
										<div className="box-classify d-flex"> 
											<div className="left"> 
												<h4 className="ls-title">WEB </h4><img className="img-fluid" src={web_booking_6} />
											</div>
											<div className="right"> 
												<h4 className="ls-title">APP </h4><img className="img-fluid" src={app_booking_6} />
											</div>
										</div>
										<p> <span className="fontWeight-5">Nâng cao: </span>Thể hiện các tính năng tìm kiếm nâng cao (số ghế ngồi trên xe, năm sản xuất, các xe đang ở chế độ cho phép "Giao xe tận nhà", hoặc cho phép "Đặt xe nhanh" (không cần chủ xe phê duyệt), truyền động (số sàn hay số tự động), loại nhiên liệu(xe máy dầu hay máy xăng) và nhiều điều kiện lọc nâng cao khác...) để dễ dàng tìm đúng dòng xe bạn mong muốn.</p>
										<div className="box-classify d-flex"> 
											<div className="left filter">
												<h4 className="ls-title">WEB </h4><img className="img-fluid" src={web_booking_7} /><img className="img-fluid" src={web_booking_8} />
											</div>
											<div className="right"> 
												<h4 className="ls-title">APP </h4><img className="img-fluid" src={app_booking_6}/><img className="img-fluid" src={app_booking_7} />
											</div>
										</div>
									</div>
									<div className="method-item" id="sendRequest">
										<h4 className="method-name">4. Lựa chọn xe mong muốn và gửi yêu cầu thuê xe</h4>
										<p><strong>a. Kiểm tra thông tin xe:</strong>  Hình ảnh xe, số chuyến đi đã thực hiện, đánh giá của các khách thuê, mô tả xe và các tính năng liên quan.</p>
										<div className="box-classify d-flex"> 
											<div className="left"> 
												<h4 className="ls-title">WEB </h4><img className="img-fluid" src={web_booking_9} />
											</div>
											<div className="right"> 
												<h4 className="ls-title">APP </h4><img className="img-fluid" src={app_booking_8} />
											</div>
										</div>
										<p> <strong>b. Kiểm tra vị trí xe:</strong>  Hệ thống sẽ khoanh vùng tọa độ trên bản đồ. Địa chỉ xe chính xác sẽ được hiển thị sau khi khách hàng tiến hành thanh toán đặt cọc.</p>
										<div className="box-classify d-flex"> 
											<div className="left"> 
												<h4 className="ls-title">WEB </h4><img className="img-fluid" src={web_booking_10} />
											</div>
											<div className="right"> 
												<h4 className="ls-title">APP </h4><img className="img-fluid" src={app_booking_9} />
											</div>
										</div>
										<p> <strong>c. Kiểm tra yêu cầu giấy tờ thuê xe và xem thông tin chủ xe:</strong></p>
										<div className="content-detail">
											<p>1. Các giấy tờ bắt buộc: Để đảm bảo an toàn, các chủ xe cần khách thuê cung cấp CMND bản gốc, Bằng lái xe ô tô bản gốc, Hộ khẩu hoặc Sổ tạm trú (KT3) bản gốc tại TPHCM (nếu thuê xe tại Hà Nội/ Đà Nẵng/ Đà Lạt thì cần HK/KT3 tại cùng địa phương).</p>
											<p>2. Điều khoản các yêu cầu thuê xe khác: Chính sách riêng của chủ xe về các giấy tờ thay thế trong trường hợp khách thuê không có HK/KT3 (có thể thay thế bằng passport, GPKD công ty, hoặc nhờ người thân có HK đứng tên hợp đồng), tài sản đặt cọc khi thuê xe (xe máy hoặc tiền mặt có giá trị tương đương).</p>
											<ul className="list-method">
												<li className="text-primary">Khách thuê lưu ý khi giao nhận xe, các chủ xe sẽ giữ lại HK/KT3 bản gốc + tài sản đặt cọc và sẽ hoàn trả lại sau khi bạn hoàn trả lại xe như nguyên trạng ban đầu.</li>
												<li className="text-primary">Bên cạnh đó, bạn vui lòng kiểm tra kĩ phần giấy tờ yêu cầu của chủ xe để đảm bảo đủ điều kiện thuê xe, hạn chế các trường hợp hủy chuyến sau khi đã đặt cọc vì không đáp ứng đủ giấy tờ.</li>
												<li className="text-primary">Trường hợp bạn không có đầy đủ các giấy tờ yêu cầu, vui lòng ghi rõ trong mục lời nhắn gửi đến chủ xe hoặc liên hệ bộ phận CSKH của Mioto tại <u> 19009217 </u> để được linh hoạt hỗ trợ tìm kiếm các xe phù hợp.</li>
											</ul>
											<p>3. Thông tin chủ xe: Mục này sẽ thể hiện điểm đánh giá dành cho chủ xe, nhận xét của khách thuê và thời gian phản hồi của chủ xe đối với các yêu cầu thuê xe...</p>
											<p>Bạn có thể ưu tiên chọn các chủ xe có điểm đánh giá cao, thời gian phản hồi nhanh chóng và có nhiều nhận xét tích cực từ khách thuê. </p>
										</div>
										<div className="box-classify d-flex"> 
											<div className="left"> 
												<h4 className="ls-title">WEB </h4><img className="img-fluid" src={web_booking_11} />
											</div>
											<div className="right"> 
												<h4 className="ls-title">APP </h4><img className="img-fluid" src={app_booking_10} />
											</div>
										</div>
										<p className="fontWeight-5">d. Kiểm tra giá, thời gian thuê, lựa chọn địa điểm giao nhận xe, thông tin giới hạn quãng đường/ngày:</p>
										<div className="content-detail">
											<p>1. Thời gian thuê</p>
											<p>- Thời gian thuê xe được tính theo ngày, hệ thống mặc định thời gian nhận xe từ 21h hôm nay và trả xe vào 20h hôm sau.</p>
											<p>- Bạn có thể linh hoạt tùy chỉnh thời gian nhận và trả xe. Nếu tổng thời gian dưới 24h sẽ làm tròn là 1 ngày. Theo thông lệ, đa phần các chủ xe trên Mioto chỉ giao xe từ 5h sáng - 10h tối hàng ngày nên bạn cần điều chỉnh thời gian cho phù hợp để dễ dàng thuê xe bạn nhé. </p>
											<p>2. Địa điểm giao nhận xe</p>
											<p>Bạn có thể lựa chọn 1 trong 2 hình thức giao nhận xe:</p>
											<p>- Giao nhận tại địa điểm của chủ xe: Địa chỉ nhận xe sẽ được hiển thị chính xác sau khi bạn tiến hành thanh toán đặt cọc thành công trên hệ thống.</p>
											<p>- Giao nhận tận nơi: Bạn có thể yêu cầu chủ xe giao đến địa chỉ nhà của mình và sẽ thanh toán thêm phí giao nhận xe (hệ thống sẽ tự động xác định khoảng cách từ vị trí chủ xe đến địa điểm giao xe để tính phí giao nhận xe).</p>
											<p>3. Giới hạn quãng đường</p>
											<p>Nếu cần di chuyển xa, bạn cần kiểm tra kĩ số km được phép di chuyển tối đa trong một ngày và số tiền phụ phí/km nếu vượt giới hạn. Mỗi chủ xe sẽ có các yêu cầu khác nhau về giới hạn quãng đường di chuyển và phụ phí.</p>
										</div>
										<div className="box-classify d-flex"> 
											<div className="left"> 
												<h4 className="ls-title">WEB </h4><img className="img-fluid" src={web_booking_12} />
											</div>
											<div className="right"> 
												<h4 className="ls-title">APP </h4><img className="img-fluid" src={app_booking_11} />
											</div>
										</div>
										<p className="fontWeight-5">e. Nhập mã khuyến mãi (nếu có): Hàng tháng Mioto đều triển khai các chương trình khuyến mãi đến người dùng, bạn đừng quên nhập mã KM để được giảm giá xe nhé.</p>
										<div className="box-classify d-flex"> 
											<div className="left"> 
												<h4 className="ls-title">WEB </h4><img className="img-fluid" src={web_booking_13} />
											</div>
											<div className="right"> 
												<h4 className="ls-title">APP </h4><img className="img-fluid" src={app_booking_12} />
											</div>
										</div>
										<p className="fontWeight-5">f. Kiểm tra lại yêu cầu thuê xe:</p>
										<div className="content-detail">
											<p>Bạn cần kiểm tra lại toàn bộ các thông tin trong yêu cầu thuê xe (thông tin xe, thời gian thuê, địa điểm nhận xe, khuyến mãi, tổng tiền thuê, các giấy tờ bắt buộc và yêu cầu khác).</p>
											<p>Bạn có thể gửi lời nhắn đến chủ xe tại mục "Lời nhắn" để giới thiệu về lộ trình di chuyển, các giấy tờ bạn có hoặc các yêu cầu khác liên quan đến việc thuê xe... để chủ xe ra quyết định cho thuê nhanh chóng và admin Mioto dễ dàng hỗ trợ bạn hơn nhé.</p>
											<p>Cuối cùng, bạn gửi yêu cầu thuê xe đến chủ xe bằng cách nhấn vào phím "Đặt xe". Các chủ xe sẽ nhận được yêu cầu đặt xe từ bạn và sẽ phản hồi đến bạn (Đồng ý/ Từ chối cho thuê) trong thời gian sớm nhất.</p>
											<p className="text-danger"> 
												<u>Lưu ý:</u>  Để hỗ trợ bạn đặt xe nhanh hơn, hệ thống cho phép bạn gửi một lúc nhiều yêu cầu thuê xe đến nhiều chủ xe khác nhau và bạn có thể ưu tiên lựa chọn các chủ xe có phản hồi sớm bạn nhé. 
											</p>
										</div>
										<div className="box-classify d-flex"> 
											<div className="left"> 
												<h4 className="ls-title">WEB </h4><img className="img-fluid" src={web_booking_14} />
											</div>
											<div className="right"> 
												<h4 className="ls-title">APP </h4><img className="img-fluid" src={app_booking_13} />
											</div>
										</div>
									</div>
									<div className="method-item" id="deposit">
										<h4 className="method-name">5. Thanh toán đặt cọc</h4>
										<div className="content-detail payment-guide-content">
											<p>Sau khi nhận được phản hồi đồng ý từ chủ xe (qua cả 2 hình thức: tin nhắn sms + thông báo trên website/ứng dụng), bạn vui lòng tiến hành thanh toán đặt cọc 30% tiền thuê xe trong thời gian sớm nhất để hoặc tất quá trình đặt xe (phần tiền 70% còn lại bạn sẽ thanh toán trực tiếp cho chủ xe khi nhận xe).</p>
											<p>Các hình thức đặt cọc tại Mioto:</p>
											<ul className="list-method">
												<li><a href="/paymenthowto#visaMethod" target="_blank">Thanh toán qua thẻ tín dụng/thẻ ghi nợ VISA, Master</a></li>
												<li><a href="/paymenthowto#atmMethod" target="_blank">Thanh toán qua thẻ ATM đã đăng kí thanh toán trực tuyến</a></li>
												<li><a href="/paymenthowto#storeMethod" target="_blank">Thanh toán tiền mặt tại cửa hàng tiện lợi gần nhà (có chấp nhận thanh toán qua Payoo)</a></li>
												<li><a href="/paymenthowto#transferMethod" target="_blank">Thanh toán bằng hình thức chuyển khoản ngân hàng</a></li>
												<li><a href="/paymenthowto#officeMethod" target="_blank">Thanh toán bằng tiền mặt tại văn phòng Mioto</a></li>
											</ul>
											<p>Để được hướng dẫn rõ hơn, bạn vui lòng vào trang <a className="text-primary fontWeight-5" href="/paymenthowto" target="_blank">Hướng dẫn thanh toán </a> nhé.</p>
											<p>Sau khi thanh toán đặt cọc thành công, bạn sẽ nhận số điện thoại và địa chỉ chính xác của chủ xe. Bạn vui lòng liên hệ sớm với chủ xe để xác nhận lại lần nữa về lịch trình và các giấy tờ yêu cầu để đảm bảo chuyến đi của mình được diễn ra suôn sẽ và tốt đẹp. Bất cứ các vấn đề gì cần thắc mắc bạn có thể liên hệ <a className="text-primary" href="tel:19009217">19009217 (9AM - 6PM T2-T7)</a> hoặc <a className="text-primary" href="https://www.facebook.com/mioto.vn/" target="_blank">  Mioto Fanpage </a>để được hỗ trợ.</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				{!this.state.isMobile && <Footer />}
			</div>
		)
	}
}