import React from "react"

import Header from "../common/header"
import Footer from "../common/footer"
import TutorialNav from "./tutorialnav"

export default class AboutUs extends React.Component {
		constructor() {
			super();
			this.state = {
							isMobile: false
					}
		}
		componentDidMount() {
			window.scrollTo(0, 0);
			const isMobile = this.props.location.pathname.includes("/mobile");
			this.setState({
				isMobile: isMobile
			});
		}
    render() {
        return <div className="mioto-layout">
          	{!this.state.isMobile && <Header />}
            <section className="body">
                <div className="body-container">
										{!this.state.isMobile && <TutorialNav />}
                    <div className="content">
                        <h1 className="title">Về chúng tôi</h1>
                        <div className="content-container">
                            <p>Công ty Cổ phần Mioto Việt Nam hoạt động trên nền tảng ứng dụng cho thuê xe tự lái 4-7 chỗ, theo mô hình kinh tế sẻ chia.</p>
                            <p>Mioto được thành lập với sứ mệnh mang đến nền tảng công nghệ hiện đại kết nối chủ xe ô tô và hành khách theo cách <b>Nhanh Nhất, An Toàn Nhất và Tiết Kiệm Nhất.</b></p>
                            <p>Mioto hướng tới việc xây dựng một cộng đồng chia sẻ ô tô văn minh với nhiều tiện ích thông qua ứng dụng trên di động, nhằm nâng cao chất lượng cuộc sống của cộng đồng.</p>
                            <div className="space l"></div>
                            <h4>Lợi thế của MiOto</h4>
                            <ul className="list-type1">
                                <li><i className="ict ict-click"></i>Đặt xe bằng 1 Click</li>
                                <li><i className="ict ict-location"></i>Mioto đã có mặt ở hầu hết các quận huyện TPHCM và các tỉnh thành lớn như Hà Nội, Hải Phòng, Đà Nẵng, Đà Lạt, Long An và đang tiếp tục mở rộng sang các tỉnh thành khác.</li>
                                <li><i className="ict ict-car"></i>Hơn 100 mẫu xe</li>
                                <li><i className="ict ict-price"></i>Rẻ hơn 10% xe truyền thống</li>
                                <li><i className="ict ict-salary"></i>Thu nhập 10 - 15 triệu/tháng</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div className="np-bottom">
                    <div className="np-bottom-container">
                        <div className="np-main">
                            <div className="np-prev np-next"><span>Kế Tiếp</span><a href="/features"><em>Tính năng nổi bật</em></a></div>
                        </div>
                    </div>
                </div>
            </section>
						{!this.state.isMobile && <Footer />}
        </div>
    }
}