import React from "react"
import { NavLink } from "react-router-dom"

export default function PrivacyNav() {
    return <div class="sidebar">
        <div className="rent-car has-scroll">
            <div className="scroll-inner">
                <ul>
                    <li>
                        <NavLink activeClassName="active" to="/terms">Nguyên tắc chung</NavLink>
                    </li>
                    <li>
                        <NavLink activeClassName="active" to="/privacy">Chính sách và quy định</NavLink>
                        <ul>
                            <li><a href="/privacy#responsibility">1. Trách nhiệm của khách thuê và chủ xe </a></li>
                            <li><a href="/privacy#mioto-responsibility">2. Trách nhiệm của Mioto khi có sự cố </a></li>
                            <li><a href="/privacy#canceltrip">3. Chính sách huỷ chuyến</a></li>
                            <li><a href="/privacy#price">4. Chính sách giá</a></li>
                            <li><a href="/privacy#payment">5. Chính sách thanh toán </a></li>
                            <li><a href="/privacy#transaction">6. Chính sách giao nhận</a></li>
                            <li><a href="/privacy#quickcancel">7. Chính sách kết thúc sớm chuyến</a></li>
                        </ul>
                    </li>
                    <li>
                        <NavLink activeClassName="active" to="/faqs">Câu hỏi và trả lời</NavLink>
                        <ul>
                            <li><a href="/faqs#renter">Dành cho khách thuê xe</a></li>
                            <li><a href="/faqs#owner">Dành cho chủ xe</a></li>
                            <li><a href="/faqs#both">Dành chung </a></li>
                        </ul>
                    </li>
                    <li>
                        <NavLink activeClassName="active" to="/regu">Quy chế hoạt động</NavLink>
                    </li>
                    <li>
                        <NavLink activeClassName="active" to="/personalinfo">Bảo mật thông tin cá nhân</NavLink>
                    </li>
                    <li>
                        <NavLink activeClassName="active" to="/resolveconflic">Giải quyết khiếu nại</NavLink>
                    </li>
                </ul>
                <div className="space m"></div>
            </div>
        </div>
    </div>
}