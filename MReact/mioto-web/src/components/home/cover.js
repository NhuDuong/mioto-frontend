import React from "react"
import { Link } from 'react-router-dom'
import PlacesAutocomplete from 'react-places-autocomplete'
import moment from 'moment'
import cookie from 'react-cookies'
import DateRangePicker from 'react-bootstrap-daterangepicker'
import { connect } from "react-redux"

import { MessageBox } from "../common/messagebox"

import banner_home from "../../static/images/banner_home.jpg"
import { commonErr } from "../common/errors"
import { LoadingInlineSmall } from "../common/loading"

import 'bootstrap-daterangepicker/daterangepicker.css'

const bannerStyle = {
    backgroundImage: `url(${banner_home})`
}

class Cover extends React.Component {
    constructor() {
        super();
        this.state = {
            startDate: moment(),
            endDate: moment().add(2, "days"),
            err: commonErr.INNIT,
            errMsg: "",
            address: ""
        }
    }

    componentDidMount() {
        const address = cookie.load("_maddr") || "";
        this.setState({
            address: address,
            isHighlight: false
        })

        var startDate;
        var endDate;

        const cStartTs = cookie.load("_mstartts");
        const cEndTs = cookie.load("_mendts");
        if (cStartTs && cEndTs) {
            const cStartDate = moment(cStartTs * 1);
            const cEndDate = moment(cEndTs * 1);
            if (cStartDate.isAfter(moment().add(1, "days"), "hour")) {
                startDate = cStartDate;
                endDate = cEndDate;
            }
        }

        if (startDate && endDate) {
            this.setState({
                startDate: startDate,
                endDate: endDate,
            })
        } else {
            this.setState({
                startDate: moment(this.props.appInit.data.startTime),
                endDate: moment(this.props.appInit.data.endTime)
            });
        }
    }

    componentWillReceiveProps(props) {
        if (props.appInit.data.startTime !== this.props.appInit.data.startTime
            && props.appInit.data.endTime !== this.props.appInit.data.endTime) {
            this.setState({
                startDate: moment(props.appInit.data.startTime),
                endDate: moment(props.appInit.data.endTime)
            });
        }
    }

    onDateRangeChange(event, picker) {
        const startDate = picker.startDate.startOf("minute");
        const endDate = picker.endDate.startOf("minute");
        if (endDate < startDate) {
            return;
        }

        //save cookies
        cookie.save("_mstartts", startDate.valueOf(), { path: '/' });
        cookie.save("_mendts", endDate.valueOf(), { path: '/' });

        this.setState({
            startDate: startDate,
            endDate: endDate
        })
    }

    findCar(e) {
        if (!this.state.address || this.state.address === "") {
            this.setState({
                isHighlight: true
            });
            e.preventDefault();
            return false;
        }
    }

    onAddressFocus(e) {
        this.setState({
            address: "",
            isHighlight: false
        });
    }

    onAddressChange(address) {
        this.setState({
            address: address
        })
    }

    onSelectFromAddr(address) {
        if (address.err < 0) {
            var errMsg = "";
            if (address.err === -200) {
                errMsg = "Bạn đã không cho phép trình duyệt truy cập vị trí hiện tại. Vui lòng cài đặt cho phép hoặc nhập địa chỉ.";
            } else {
                errMsg = "Không thể tìm thấy vị trí hiện tại của thiết bị. Vui lòng thử lại hoặc nhập địa chỉ.";
            }
            this.setState({
                err: commonErr.FAIL,
                errMsg: errMsg
            });
        } else if (address.err === commonErr.LOADING) {
            this.setState({
                err: commonErr.LOADING,
            });
        } else {
            this.setState({
                err: commonErr.SUCCESS,
                address: address.address
            })
        }
    }

    hideMessageBox() {
        this.setState({
            errMsg: ""
        });
    }

    render() {
        return <div className="cover">
            <div className="slogan">
                <h1>Thuê xe tự lái - Tải ngay Mioto!</h1>
            </div>
            <div className="cover-container" style={bannerStyle}>
                <div className="inside form-default">
                    <div className="rent-car">
                        <div className="lstitle">
                            <i className="ic ic-car"></i>
                            <span>Bạn cần thuê xe?</span>
                        </div>
                        <div className="line-form">
                            <label className="label">Địa điểm</label>
                            {this.state.err === commonErr.LOADING && <div className="wrap-input has-ico">
                                <i className="ic ic-location-f" />
                                <LoadingInlineSmall />
                            </div>}
                            <div className={`wrap-input has-ico ${this.state.err === commonErr.LOADING ? "hidden" : ""}`}>
                                <i className="ic ic-location-f" />
                                <PlacesAutocomplete
                                    classNames={{ input: this.state.isHighlight ? "has-error" : "" }}
                                    inputProps={{
                                        value: this.state.address,
                                        onChange: this.onAddressChange.bind(this),
                                        onFocus: this.onAddressFocus.bind(this),
                                        placeholder: "Nhập thành phố, quận, địa chỉ..."
                                    }}
                                    onSelect={this.onSelectFromAddr.bind(this)}
                                    isUseCurrentLocation={true}
                                    googleLogo={false}
                                    options={{ componentRestrictions: { country: ['vn'] } }} />
                            </div>
                        </div>
                        <div className="line-form has-timer">
                            <label className="label">Thời gian</label>
                            <div className="wrap-input">
                                <DateRangePicker
                                    startDate={this.state.startDate}
                                    endDate={this.state.endDate}
                                    minDate={moment()}
                                    maxDate={moment().add(3, "months")}
                                    timePicker={true}
                                    autoApply={true}
                                    buttonClasses={["hidden"]}
                                    timePickerIncrement={30}
                                    opens={"left"}
                                    linkedCalendars={false}
                                    timePicker24Hour={true}
                                    onHide={this.onDateRangeChange.bind(this)}>
                                    <span style={{ fontSize: "13px", width: "100%", padding: "13px 20px", display: "inline-block", cursor: "pointer" }}>{this.state.startDate.format("HH:mm DD/MM/YYYY")} - {this.state.endDate.format("HH:mm DD/MM/YYYY")}</span>
                                </DateRangePicker>
                            </div>
                        </div>
                        <div className="line-action">
                            <Link className="cta-main" onClick={this.findCar.bind(this)} to={{
                                pathname: `/find/filter`,
                                search: `startDate=${this.state.startDate}&endDate=${this.state.endDate}&address=${this.state.address}`
                            }}>TÌM XE NGAY</Link>
                            <i className="i-arr"></i>
                        </div>
                        <MessageBox show={this.state.errMsg !== ""} hideModal={this.hideMessageBox.bind(this)} message={this.state.errMsg} />
                    </div>
                </div>
            </div>
        </div>
    }
}

function mapState(state) {
    return {
        appInit: state.appInit
    }
}

Cover = connect(mapState)(Cover);

export default Cover;