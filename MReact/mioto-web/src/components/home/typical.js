import React from "react"
import { Link } from "react-router-dom"

import banner_def from "../../static/images/img_car.jpg"

// import promo_cover from "../../static/images/banner-7.png"
// import promo_cover from "../../static/images/cover-web.png"
// import banner_c1 from "../../static/images/banner-c1-2.png"

function Typical() {
    return <div className="not-your-typical" style={{ backgroundImage: `url(${banner_def})` }}>
        <div className="wrap-desc">
            <div className="inside"><span className="title-desc">Chỉ cần 5 phút!</span>
                <p>Thuê xe nhanh chóng, dễ dàng, mọi lúc mọi nơi.</p>
                <Link className="btn btn-primary btn--m" to="/howitwork">Tìm hiểu ngay</Link>
            </div>
        </div>
    </div>

    // return <div className="not-your-typical bg-promo" style={{ backgroundImage: `url(${promo_cover})` }}>
    //     <div className="wrapper__pr m-container">
    //         <div className="wrap-promo">
    //             <h4 className="title-desc">VUI HÈ THÁNG 7</h4>
    //             <p className="dt-code">Giảm ngay 10% (tối đa 100.000đ/chuyến) khi nhập mã </p>
    //             <p className="dt-code">Bằng mã code <span>MI7</span> từ Mioto  </p>
    //             <p className="note">* Áp dụng đến hết ngày 31/07/2018</p>
    //         </div>
    //     </div>
    // </div>

    // return <div className="not-your-typical bg-promo" style={{ backgroundImage: `url(${promo_cover})` }}>
    //     <div className="wrapper__pr">
    //         <h4 className="title-desc">VUI HÈ RỰC RỠ</h4>
    //         <div className="wrap-promo">
    //             <p className="dt-code">Giảm <span>10% </span>(tối đa 100.000đ/chuyến) khi nhập mã <span>HEVE </span></p>
    //             <p className="note">Áp dụng đến hết ngày 30/06/2018</p>
    //         </div>
    //     </div>
    // </div>

    // return <div className="not-your-typical bg-promo" style={{ backgroundImage: `url(${promo_cover})` }}>
    //     <div className="wrapper__pr">
    //         <div className="wrap-promo">
    //             <h4 className="title-desc">"NHẬN MÃ LIỀN TAY <span>LỄ NÀY VI VU XẾ HỘP"</span></h4>
    //             <p className="dt-code">Giảm 10% khi nhập mã<span>MIOTO</span></p>
    //         </div>
    //     </div>
    //     <div className="note-promo">
    //         <p>* Chương trình chỉ áp dụng cho khách hàng đặt xe qua ứng dụng hoặc website, kiểm tra ngay để thấy bất ngờ </p>
    //     </div>
    // </div>

    // return <div className="not-your-typical bg-promo" style={{ backgroundImage: `url(${banner_c1})` }}>
    //     <div className="wrapper__pr">
    //         <div className="wrap-promo">
    //             <h4 className="title-desc">HÒA NHỊP BÓNG ĐÁ CHUNG KẾT C1 </h4>
    //             <h4 className="title-desc">REAL MARID FC - LIVERPOOL FC</h4>
    //         </div>
    //         <p className="dt-code">Giảm 10% khi nhập mã<span>C1</span></p>
    //     </div>
    //     <div className="note-promo"><a href="https://www.facebook.com/mioto.vn/" target="_blank">*Tham gia dự đoán tỉ số trận chung kết cúp C1 trên fanpage Mioto để có cơ hội nhận nhiều quà tặng hấp dẫn </a></div>
    // </div>
}

export default Typical;