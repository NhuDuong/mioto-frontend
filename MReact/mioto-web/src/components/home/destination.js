import React from "react"
import { Link } from 'react-router-dom'

function DestinationItem(props) {
    const dest = props.dest;
    return <div className="swiper-slide" style={{ padding: "10px" }}>
        <Link className="item-destination" to={{
            pathname: "/find/filter",
            search: `lat=${dest.loc.lat}&lng=${dest.loc.lon}&address=${dest.name}`
        }}>
            <div className="img-destination">
                <div className="fix-img"> <img src={dest.image} alt={`Cho thuê xe tự lái ${dest.name}`} /></div>
                <h2>{dest.name}</h2>
            </div>
        </Link>
    </div>
}

class Destination extends React.Component {
    componentDidMount() {
        this.swiper = new window.Swiper(this.refs.swiper, {
            slidesPerView: 4,
            spaceBetween: 15,
            threshold: 15,
            speed: 400,
            navigation: {
                nextEl: '.swiper-button-next-destination',
                prevEl: '.swiper-button-prev-destination',
            },
            slidesOffsetBefore: 0,
            slidesOffsetAfter: 0,
            preventClicks: false,
            preventClicksPropagation: false,
            breakpoints: {
                992: {
                    slidesPerView: 3,
                    spaceBetween: 40
                },
                768: {
                    slidesPerView: 3,
                    spaceBetween: 40
                },
                480: {
                    slidesPerView: 1.2,
                }
            },
            loop: true
        })
    }

    render() {
        return <div className="module-destination">
            <span className="title">ĐỊA ĐIỂM NỔI BẬT</span>
            <div className="swiper-button-next swiper-button-next-destination">
                <i className="i-arr"></i>
            </div>
            <div className="swiper-button-prev swiper-button-prev-destination">
                <i className="i-arr"></i>
            </div>
            <div ref="swiper" className="swiper-container swiper-destination">
                <div className="swiper-wrapper">
                    {this.props.topDests.map(function (dest, i) {
                        return <DestinationItem key={i} dest={dest} />
                    })}
                </div>
            </div>
        </div>
    }
}

export default Destination;