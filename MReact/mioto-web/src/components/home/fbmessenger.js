import React from "react"

class FbMessenger extends React.Component {
    componentDidMount() {
        //FB Messenger plugin
        var js, fjs = document.getElementsByTagName("script")[0];
        var id = "facebook-jssdk";
        if (document.getElementById(id)) {
            return;
        }
        js = document.createElement("script");
        js.id = id;
        js.src = "https://connect.facebook.net/vi_VN/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);

        window.fbAsyncInit = function () {
            window.FB.init({
                appId: '1760356927595709',
                autoLogAppEvents: true,
                xfbml: true,
                version: 'v2.12'
            });
        };
    }

    render() {
        return <div className="fb-customerchat"
            page_id="122050578497254"
            minimized="true"
            logged_in_greeting="Chat ngay với Mioto để được hỗ trợ."
            logged_out_greeting="Chat ngay với Mioto để được hỗ trợ.">
        </div>
    }
}

export default FbMessenger;