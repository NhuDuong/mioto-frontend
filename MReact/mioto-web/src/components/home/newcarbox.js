import React from "react"
import StarRatings from "react-star-ratings"
import { Link } from "react-router-dom"

import { formatPrice, formatTitleInUrl } from "../common/common"

import carPhotoDefault from "../../static/images/upload/car_1.png"

function DealItem(props) {
    var car = props.featureCar;
    return <div className="item-car">
        <Link to={{ pathname: `/car/${formatTitleInUrl(car.name)}/${car.id}` }}>
            <span className="img-car">
                <div className="fix-img">
                    <img src={car.photos ? car.photos[0].thumbUrl : carPhotoDefault} alt={`Cho thuê xe tự lái ${car.name}`} />
                </div>
            </span>
            <div className="desc-car">
                <StarRatings
                    rating={car.rating.avg || 0}
                    starRatedColor="#00a550"
                    starDimension="17px"
                    starSpacing="1px"
                />
                <h2>{car.name}</h2>
                <p>
                    <span><i className="ic ic-clock"></i> {car.totalTrips} chuyến</span>
                    <span><i className="ic ic-tag"></i> {formatPrice(car.price)}</span>
                </p>
            </div>
        </Link>
    </div>
}

class NewCarBox extends React.Component {
    componentDidMount() {
        this.swiper = new window.Swiper(this.refs.swiperNew, {
            slidesPerView: 4,
            spaceBetween: 24,
            threshold: 15,
            speed: 600,
            navigation: {
                nextEl: '.swiper-button-next-new-box',
                prevEl: '.swiper-button-prev-new-box',
            },
            slidesOffsetBefore: 0,
            slidesOffsetAfter: 0,
            preventClicks: false,
            preventClicksPropagation: false,
            breakpoints: {
                992: {
                    slidesPerView: 3,
                    spaceBetween: 30
                },
                768: {
                    slidesPerView: 3,
                    spaceBetween: 30
                },
                480: {
                    slidesPerView: 1.2,
                    spaceBetween: 15
                }
            },
        });
    }

    render() {
        return <div className="module-deal">
            <div className="space m"></div>
            <div className="space m"></div>
            <span className="title">XE MỚI ĐĂNG KÍ</span>
            <div className="swiper-button-next swiper-button-next-new-box">
                <i className="i-arr"></i>
            </div>
            <div className="swiper-button-prev swiper-button-prev-new-box">
                <i className="i-arr"></i>
            </div>
            <div ref="swiperNew" className="swiper-container swiper-new">
                <div className="swiper-wrapper">
                    {
                        this.props.cars.map(function (car, i) {
                            return <div className="swiper-slide" key={i}>
                                <DealItem featureCar={car} />
                            </div>
                        })
                    }
                </div>
            </div>
        </div >
    }
}

export default NewCarBox;