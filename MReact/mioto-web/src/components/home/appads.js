import React from "react"
import cookie from 'react-cookies'

import images_appstore from "../../static/images/appstore.png"
import images_googleplay from "../../static/images/googleplay.png"
import images_iphone from "../../static/images/iphone.png"

class AppAds extends React.Component {
    onIOSAppClick() {
        //GA
        const utmSrc = cookie.load("_utm_src") || "web_directly";
        const utmExt = cookie.load("_utm_ext") || "no_label";
        window.ga('send', 'event', utmSrc, "INSTALL_APP_IOS", utmExt);
    }

    onAndrAppClick() {
        //GA
        const utmSrc = cookie.load("_utm_src") || "web_directly";
        const utmExt = cookie.load("_utm_ext") || "no_label";
        window.ga('send', 'event', utmSrc, "INSTALL_APP_ANDR", utmExt);
    }

    render() {
        return <div className="module-app">
            <div className="app-container">
                <div className="inside">
                    <h4>Ứng dụng cho điện thoại</h4>
                    <p>Tải ngay ứng dụng tại App Store hoặc Google Play.</p>
                    <a className="func-app" onClick={this.onIOSAppClick.bind(this)} href="https://itunes.apple.com/vn/app/mioto-thu%C3%AA-xe-t%E1%BB%B1-l%C3%A1i/id1316420500?l=vi&mt=8">
                        <img className="responsive-img" src={images_appstore} alt="Mioto - Thuê xe tự lái" />
                    </a>
                    <a className="func-app" onClick={this.onAndrAppClick.bind(this)} href="https://play.google.com/store/apps/details?id=com.mioto.mioto&hl=vi">
                        <img className="responsive-img" src={images_googleplay} alt="Mioto - Thuê xe tự lái" />
                    </a>
                </div>
                <div className="img">
                    <img className="responsive-img" src={images_iphone} alt="Mioto - Thuê xe tự lái" />
                </div>
            </div>
        </div>
    }
}

export default AppAds;