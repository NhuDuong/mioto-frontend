import React from "react"
import cookie from "react-cookies"
import { isIOS, isAndroid } from "react-device-detect"

import images_googleplay from "../../static/images/googleplay.png"
import images_appstore from "../../static/images/appstore.png"

export default class AppInstall extends React.Component {
    onIOSAppClick() {
        //GA
        const utmSrc = cookie.load("_utm_src") || "web_directly";
        const utmExt = cookie.load("_utm_ext") || "no_label";
        window.ga('send', 'event', utmSrc, "INSTALL_APP_IOS", utmExt);
    }

    onAndrAppClick() {
        //GA
        const utmSrc = cookie.load("_utm_src") || "web_directly";
        const utmExt = cookie.load("_utm_ext") || "no_label";
        window.ga('send', 'event', utmSrc, "INSTALL_APP_ANDR", utmExt);
    }

    render() {
        var content = null;
        if (isIOS) {
            content = <a className="app-link" onClick={this.onIOSAppClick.bind(this)} href="https://itunes.apple.com/vn/app/mioto-thu%C3%AA-xe-t%E1%BB%B1-l%C3%A1i/id1316420500?l=vi&mt=8">
                <div className="app-footer">
                    <div className="left">
                        <div className="fix-img"> <img src={images_appstore} alt="Mioto - Thuê xe tự lái"/></div>
                    </div>
                    <p>Tải ngay ứng dụng Mioto</p>
                </div>
            </a>
        } else if (isAndroid) {
            content = <a className="app-link" onClick={this.onAndrAppClick.bind(this)} href="https://play.google.com/store/apps/details?id=com.mioto.mioto&hl=vi">
                <div className="app-footer">
                    <div className="left">
                        <div className="fix-img"> <img src={images_googleplay} alt="Mioto - Thuê xe tự lái"/></div>
                    </div>
                    <p>Tải ngay ứng dụng Mioto</p>
                </div>
            </a>
        }
        return content;
    }

}