import React from "react"
import banner_owner from "../../static/images/bg-explore.jpg"

function Explorer() {
    return <div className="module-explorer" style={{ "backgroundImage": `url(${banner_owner})` }}>
        <div className="explorer-container">
            <div className="inside">
                <h2>Bạn muốn cho thuê xe tự lái</h2>
                <p>Trở thành đối tác của chúng tôi để có cơ hội kiếm thêm thu nhập hàng tháng.</p>
                <a className="btn btn-primary btn--m" href="/howitwork#owner">Tìm hiểu ngay</a>
            </div>
        </div>
    </div>
}

export default Explorer;