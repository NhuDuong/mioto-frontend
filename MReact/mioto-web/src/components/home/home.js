import React from "react"
import cookie from "react-cookies"
import queryString from 'query-string'

import Header from "../common/header"
import Footer from "../common/footer"
import Cover from "./cover"
import DealBox from "./dealbox"
import NewCarBox from "./newcarbox"
import Typycal from "./typical"
import Destination from "./destination"
import Explorer from "./explorer"
import AppAds from "./appads"
import AppInstall from "./appinstall"
import { getHomePage } from "../../model/common"

class Home extends React.Component {
    constructor() {
        super();
        this.state = {
            topDests: null,
            featureCars: null,
            newCars: null
        }
    }

    componentDidMount() {
        window.scrollTo(0, 0);

        getHomePage().then(resp => {
            this.setState({
                featureCars: resp.data.data.featureCars,
                newCars: resp.data.data.newCars,
                topDests: resp.data.data.topDest,
            })
        });

        //GA
        if (this.props.location && this.props.location.search) {
            const query = queryString.parse(this.props.location.search);

            if (query.utm_source) {
                cookie.save("_utm_src", query.utm_source, { path: '/' });
                cookie.save("_utm_src", query.utm_source, { path: '/', domain: '.mioto.vn' });
            }
            if (query.utm_ext) {
                cookie.save("_utm_ext", query.utm_ext, { path: '/' });
                cookie.save("_utm_ext", query.utm_ext, { path: '/', domain: '.mioto.vn' });
            } else {
                cookie.remove("_utm_ext", { path: '/' });
                cookie.remove("_utm_ext", { path: '/', domain: '.mioto.vn' });
            }
        }

        const utmSrc = cookie.load("_utm_src") || "web_directly";
        const utmExt = cookie.load("_utm_ext") || "no_label";
        window.ga('send', 'event', utmSrc, "VIEW_HOME", utmExt);
    }

    render() {
        return <div className="mioto-layout">
            <Header />
            <section className="body">
                <Cover />
                {this.state.topDests && <Destination topDests={this.state.topDests} />}
                <Typycal />
                {this.state.featureCars && <DealBox featureCars={this.state.featureCars} />}
                <Explorer />
                {this.state.newCars && <NewCarBox cars={this.state.newCars} />}
                <AppAds />
                <AppInstall />
            </section>
            <Footer />
        </div>
    }
}

export default Home;