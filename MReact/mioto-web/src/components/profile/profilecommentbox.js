import React from "react";

import { commonErr } from "../common/errors";
import { getOwnerReviews, getTravelerReviews } from "../../model/car";
import CommentItem from "../common/commentitem";

export default class ProfileCommentBox extends React.Component {
    constructor() {
        super();
        this.state = {
            ownerReviews: {
                err: commonErr.INNIT,
                reviews: null,
                profile: null,
                more: 0
            },
            renterReviews: {
                err: commonErr.INNIT,
                reviews: null,
                profile: null,
                more: 0
            }
        }
    }

    componentDidMount() {
        this.setState({
            ownerReviews: {
                err: commonErr.LOADING
            },
            renterReviews: {
                err: commonErr.LOADING
            }
        });

        getOwnerReviews(this.props.profile.uid, 0, 0, 0).then(resp => {
            if (resp.data.error >= commonErr.SUCCESS && resp.data.data) {
                this.setState({
                    ownerReviews: {
                        err: resp.data.error,
                        reviews: resp.data.data.reviews,
                        profiles: resp.data.data.profiles,
                        more: resp.data.data.more
                    }
                })
            }
        });

        getTravelerReviews(this.props.profile.uid, 0, 0, 0).then(resp => {
            if (resp.data.error >= commonErr.SUCCESS && resp.data.data) {
                this.setState({
                    renterReviews: {
                        err: resp.data.error,
                        reviews: resp.data.data.reviews,
                        profiles: resp.data.data.profiles,
                        more: resp.data.data.more
                    }
                })
            }
        });
    }

    render() {
        var content;
        var reviews;
        var profiles;
        if (this.props.reviewType === 0) {
            if (this.state.renterReviews) {
                reviews = this.state.renterReviews.reviews;
                profiles = this.state.renterReviews.profiles;
            }
        } else {
            if (this.state.ownerReviews) {
                reviews = this.state.ownerReviews.reviews;
                profiles = this.state.ownerReviews.profiles;
            }
        }

        if (reviews && profiles) {
            content = <ul className="list-comments">
                {reviews.map(review => {
                    var ownerProfile;
                    if (profiles) {
                        if (profiles.length > 0) {
                            for (var i = 0; i < profiles.length; ++i) {
                                if (profiles[i].uid === review.uid) {
                                    ownerProfile = profiles[i];
                                    break;
                                }
                            }
                        }
                    }
                    return <li><CommentItem review={review} profile={ownerProfile} /></li>
                })}
            </ul>
        } else {
            content = <p className="no-result">Chưa có nhận xét nào từ {this.props.reviewType === 1 ? "chủ xe." : "khách thuê."}</p>
        }

        return <div className="body-content">
            {content}
        </div>
    }
}