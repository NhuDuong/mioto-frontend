import React from "react"
import StarRatings from "react-star-ratings"
import { Link } from "react-router-dom"

import { formatPrice, formatTitleInUrl } from "../common/common"

import carPhotoDefault from "../../static/images/upload/car_1.png"

function CarItem(props) {
    var car = props.featureCar;
    return <div className="item-car">
        <Link to={`/car/${formatTitleInUrl(car.name)}/${car.id}`}>
            <span className="img-car">
                <div className="fix-img">
                    <img src={car.photos ? car.photos[0].thumbUrl : carPhotoDefault} alt={`Cho thuê xe tự lái ${car.name}`} />
                </div>
                <span className="label-pos">
                    {car.totalDiscountPercent > 0 && <span className="discount">Giảm {car.totalDiscountPercent}%</span>}
                    {car.instant > 0 && <span className="rent"><i className="ic ic-sm-thunderbolt-wh" /> Đặt xe nhanh</span>}
                    {car.deliveryEnable === 1 && car.deliveryPrice === 0 && <span className="free">Miễn phí giao nhận xe</span>}
                </span>
            </span>
            <div className="desc-car">
                <div className="group">
                    <span className="star">
                        <StarRatings
                            rating={car.rating.avg || 0}
                            starRatedColor="#00a550"
                            starDimension="17px"
                            starSpacing="1px"
                        />
                    </span>
                    <span className="price">{formatPrice(car.price)}</span>
                </div>
                <h2>{car.name}</h2>
                <p>
										{car.distance && <span><i className="ic ic-map" /> {car.distance}</span>}
                    <span><i className="ic ic-clock" /> {car.totalTrips} chuyến</span>
                </p>
            </div>
        </Link>
    </div>
}

class PropsCars extends React.Component {
    componentDidMount() {
        this.swiper = new window.Swiper(this.refs.swiperPropsCars, {
            slidesPerView: 4,
            spaceBetween: 24,
            threshold: 15,
            speed: 600,
            pagination: {
                el: '.pagi-props-car',
            },
            navigation: {
                nextEl: '.swiper-button-next-props-cars',
                prevEl: '.swiper-button-prev-props-cars'
            },
            slidesOffsetBefore: 0,
            slidesOffsetAfter: 0,
            breakpoints: {
                992: {
                    slidesPerView: 3,
                    spaceBetween: 30
                },
                768: {
                    slidesPerView: 3,
                    spaceBetween: 30
                },
                480: {
                    slidesPerView: 1,
                    spaceBetween: 15
                }
            }
        });
    }

    render() {
        return <div className="props-body">
            <div className="swiper-button-next swiper-button-next-props-cars">
                <i className="i-arr" />
            </div>
            <div className="swiper-button-prev swiper-button-prev-props-cars">
                <i className="i-arr" />
            </div>
            <div ref="swiperPropsCars" className="swiper-container swiper-similar">
                <div className="swiper-wrapper">
                    {this.props.featureCars.map((featureCar, i) => {
                        return <div className="swiper-slide" key={i}>
                            <CarItem isNewTab={this.props.isNewTab} featureCar={featureCar} tooglePopup={this.props.tooglePopup} />
                        </div>
                    })}
                </div>
                <div className="swiper-pagination pagi-news pagi-props-car" />
            </div>
        </div>
    }
}

export default PropsCars;