import React from "react"
import StarRatings from "react-star-ratings"

import { commonErr } from "../common/errors"
import { getUserCars } from "../../model/car"
import { MessageLine } from "../common/messagebox"
import { formatPrice, formatTitleInUrl } from "../common/common"

import carPhotoDefault from "../../static/images/upload/car_1.png"

function CarItem(props) {
    var car = props.car;
    return <div className="item-car">
        <a href={`/car/${formatTitleInUrl(car.name)}/${car.id}`}>
            <span className="img-car">
                <div className="fix-img"> <img src={car.photos ? car.photos[0].thumbUrl : carPhotoDefault} /></div>
            </span>
            <div className="desc-car">
                <StarRatings
                    rating={car.rating.avg || 0}
                    starRatedColor="#00a550"
                    starDimension="17px"
                    starSpacing="1px"
                />
                <h2>{car.name}</h2>
                <p>
                    <span><i className="ic ic-clock"></i> {car.totalTrips} chuyến</span>
                    <span><i className="ic ic-tag"></i> {formatPrice(car.price)}</span>
                </p>
            </div>
        </a>
    </div>
}

class PropertiesBoxV2 extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            err: commonErr.INNIT,

            total: props.total || 0,
            cars: props.cars || [],
            more: (props.total && props.cars && props.total > props.cars.length) ? 1 : 0
        }
    }

    componentDidMount() {
        if (this.props.total === 0 || !this.props.cars || this.props.cars.length === 0) {
            getUserCars(this.props.profile.uid, 0, 0, 0, 0, true).then(resp => {
                if (resp.data.error >= commonErr.SUCCESS && resp.data.data.cars) {
                    this.setState({
                        cars: resp.data.data.cars,
                        more: resp.data.data.more,
                        err: resp.data.error
                    });
                } else {
                    this.setState({
                        err: resp.data.error
                    });
                }
            });
        }
    }


    getUserCarsMore() {
        getUserCars(this.props.profile.uid, 0, 0, this.state.cars.length, 0, true).then(resp => {
            if (resp.data.error >= commonErr.SUCCESS && resp.data.data.cars) {
                this.setState({
                    cars: this.state.cars.concat(resp.data.data.cars),
                    more: resp.data.data.more,
                    err: resp.data.error
                });
            } else {
                this.setState({
                    err: resp.data.error
                });
            }
        });
    }


    render() {
        var content;
        if (this.state.cars && this.state.cars.length > 0) {
            content = this.state.cars.map(car =>
                <CarItem key={car.id} car={car} />
            );
        } else {
            content = <MessageLine message="Bạn chưa đăng kí xe nào" />
        }
        return <div className="properties">
            <div className="properties__wrap">
                <div className="props-header"><span className="lstitle">Xe của {this.props.profile.name} ({this.state.cars.length} xe)</span></div>
                {content}
                {this.state.more === 1 && <div className="s-all">
                    <p className="see-all" onClick={this.getUserCarsMore.bind(this)}>Xem thêm <i className="ic ic-chevron-up" /></p>
                </div>}
            </div>
        </div>
    }
}

export default PropertiesBoxV2;