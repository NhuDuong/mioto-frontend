import React from "react"
import { connect } from "react-redux"
import { Modal } from "react-bootstrap"
import moment from "moment"

import Header from "../common/header"
import Footer from "../common/footer"
import { LoadingPage } from "../common/loading"
import { getLoggedProfile } from "../../actions/sessionAct"
import { commonErr } from "../common/errors"
import PropertiesBoxV3 from "../profile/propertiesv3"
import { getProfile } from "../../model/profile"
import { getUserReportReasons, reportUser } from "../../model/car"
import ProfileCommentBoxV2 from "../profile/profilecommentboxv2"
import Login from "../login/login"
import ImageLightBox from "../common/imagelightbox"
import { MessagePage } from "../common/messagebox"

import avatar_default from "../../static/images/avatar_default.png"
import def_profile_cover from "../../static/images/background/car-9.jpg"

class UserReport extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            err: commonErr.INNIT,
            errMsg: "",
            reasons: null,

            ip_reason: "",
            ip_comment: ""
        }
    }

    handleInputChange = (event) => {
        this.setState({
            [event.target.name]: event.target.value,
            err: commonErr.INNIT,
            errMsg: ""
        });
    }

    reportUser() {
        if (this.state.ip_reason === ""
            || this.state.ip_reason === "0"
            || (this.state.ip_reason === "u0" && (!this.state.ip_comment || this.state.ip_comment === ""))) {
            this.setState({
                err: commonErr.FAIL,
                errMsg: "Vui lòng chọn lý do hoặc ghi rõ lý do của bạn."
            });
        } else {
            reportUser(this.props.profile.uid, this.state.ip_reason, this.state.ip_comment).then(resp => {
                if (resp.data.error >= commonErr.SUCCESS) {
                    this.setState({
                        err: resp.data.error,
                        errMsg: resp.data.errorMessage
                    });
                    this.props.hideModal();
                } else {
                    this.setState({
                        err: resp.data.error,
                        errMsg: resp.data.errorMessage
                    });
                }
            });
        }
    }

    componentDidMount() {
        getUserReportReasons(this.props.profile.uid).then(resp => {
            if (resp.data.error >= commonErr.SUCCESS) {
                this.setState({
                    err: resp.data.error,
                    errMsg: resp.data.errorMessage,
                    reasons: resp.data.data.reasons
                });
            } else {
                this.setState({
                    err: resp.data.error,
                    errMsg: resp.data.errorMessage
                });
            }
        });
    }

    render() {
        return <Modal
            show={this.props.show}
            onHide={this.props.hideModal}
            dialogClassName="modal-sm modal-dialog"
        >
            <Modal.Header closeButton={true} closeLabel={""}>
                <Modal.Title>Báo xấu tài khoản</Modal.Title>
            </Modal.Header>
            {this.state.err < commonErr.SUCCESS && this.state.errMsg !== "" && <p className="textAlign-center" style={{ color: "red" }}>{this.state.errMsg}</p>}
            <Modal.Body>
                <div className="form-default form-s">
                    <div className="line-form">
                        <div className="wrap-select">
                            <select name="ip_reason" onChange={this.handleInputChange.bind(this)}>
                                <option value="0" disabled selected>Chọn lý do</option>
                                {this.state.reasons && this.state.reasons.map(
                                    reason => <option key={reason.reason} value={reason.reason}>{reason.desc}</option>
                                )}
                            </select>
                        </div>
                    </div>
                    <div className="line-form">
                        <div className="wrap-input has-ico">
                            <input type="text" placeholder="Lời nhắn" name="ip_comment" onChange={this.handleInputChange.bind(this)} />
                        </div>
                    </div>
                    <div className="clear" />
                    <button className="btn btn-primary btn--m" type="button" name="cancelTripBtn" onClick={this.reportUser.bind(this)}>Báo xấu</button>
                </div>
            </Modal.Body>
        </Modal>
    }
}

class Profile extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            err: commonErr.INNIT,
            errMsg: "",
            data: null,
            reviewType: "0",
            showLoginForm: false,
            showReportForm: false,
            isShowQrLightBox: false
        }

        this.showQrLightBox = this.showQrLightBox.bind(this);
        this.hideQrLightBox = this.hideQrLightBox.bind(this);
    }

    componentDidMount() {
        getProfile(this.props.match.params.userId, "pd" /*view count for server*/).then(resp => {
            this.setState({
                err: resp.data.error,
                errMsg: resp.data.errorMessage,
                data: resp.data.data
            });
        });
    }

    showLoginForm() {
        this.setState({
            showLoginForm: true
        });
    }

    openLoginForm() {
        this.setState({
            showLoginForm: true
        });
    }

    closeLoginForm() {
        this.setState({
            showLoginForm: false
        });
    }

    showReportForm() {
        this.setState({
            showReportForm: true
        });
    }

    closeReportForm() {
        this.setState({
            showReportForm: false
        });
    }

    getLoggedProfile() {
        this.props.dispatch(getLoggedProfile());
    }

    reportUserBtnClick() {
        if (this.props.session.profile.info && this.props.session.profile.info.uid) {
            this.showReportForm();
        } else {
            this.showLoginForm();
        }
    }

    reviewTypeChange(event) {
        this.setState({
            reviewType: event.target.value
        });
    }

    showQrLightBox(i) {
        this.setState({
            isShowQrLightBox: true
        })
    }

    hideQrLightBox() {
        this.setState({
            isShowQrLightBox: false
        })
    }

    render() {
        var content;
        if (this.state.err === commonErr.INNIT || this.state.err === commonErr.LOADING) {
            content = <LoadingPage />
        } else if (this.state.err === commonErr.SUCCESS && this.state.data) {
            const profile = this.state.data.profile;
            const totalTrip = profile.traveler.totalTrips + profile.owner.totalTrips;
            content = content = <section className="body min-height">
                <div className="cover-profile new-profile" style={{ backgroundImage: `url(${def_profile_cover})` }}>
                    <div className="qr-car" onClick={this.showQrLightBox}><i className="ic ic-qr-code" /></div>
                </div>
                <div className="profile__sect">
                    <div className="content-profile--new">
                        <div className="desc-profile">
                            <div className="avatar-box">
                                <div className="avatar avatar--xl">
                                    <div className="avatar-img" style={{ backgroundImage: `url(${(profile.avatar && profile.avatar.fullUrl) ? profile.avatar.fullUrl : avatar_default})` }}></div>
                                </div>
                            </div>
                            <div className="snippet">
                                <div className="item-title">
                                    <p>{profile.name}</p>
                                </div>
                                <div className="d-flex">
                                    <span className="join">Tham gia: {moment(profile.timeCreated).format("DD/MM/YYYY")}</span>
                                    <div className="bar-line" /><span className="sum-trips">{totalTrip > 0 ? `${totalTrip} chuyến` : "Chưa có chuyến"} </span>
                                </div>
                            </div>
                        </div>
                        <div className="desc-profile">
                            {profile.owner && profile.owner.responseRate !== "-" && <div className="wr-info">
                                <div className="s-info"><span className="lstitle">Tỉ lệ phản hồi</span><span className="ctn">{profile.owner.responseRate}</span></div>
                                <div className="s-info"><span className="lstitle">Thời gian phản hồi</span><span className="ctn">{profile.owner.responseTime}</span></div>
                                <div className="s-info"><span className="lstitle">Tỉ lệ đồng ý</span><span className="ctn">{profile.owner.acceptRate}</span></div>
                            </div>}
                            <div className={`information ${(!profile.owner || profile.owner.responseRate === "-") ? "information--acc" : ""}`}>
                                <div className="inside">
                                    <ul>
                                        <li>
                                            <span className="label">Điện thoại</span>
                                            {profile.phoneVerified === 1 ? <span className="ctn"><i className="ic ic-verify" />Đã xác thực</span> : <span className="ctn"><i className="ic ic-error" />Chưa xác thực</span>}
                                        </li>
                                        <li>
                                            <span className="label">Email</span>
                                            {profile.emailVerified === 1 ? <span className="ctn"><i className="ic ic-verify" />Đã xác thực</span> : <span className="ctn"><i className="ic ic-error" />Chưa xác thực</span>}
                                        </li>
                                        <li>
                                            <span className="label">Facebook</span>
                                            {profile.facebookName ? <span className="ctn"><i className="ic ic-verify" />Đã liên kết {profile.facebookFr > 0 && <span>(có {profile.facebookFr} bạn)</span>}</span> : <span className="ctn"><i className="ic ic-error" />Chưa liên kết</span>}
                                        </li>
                                        <li>
                                            <span className="label">Google</span>
                                            {profile.googleName ? <span className="ctn"><i className="ic ic-verify" />Đã liên kết</span> : <span className="ctn"><i className="ic ic-error" />Chưa liên kết</span>}
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="profile__wrap">
                        <div className="review__sect">
                            <div className="review-container">
                                <ProfileCommentBoxV2
                                    profile={profile}
                                    reviewType={0} />
                                <ProfileCommentBoxV2
                                    profile={profile}
                                    reviewType={1} />
                            </div>
                        </div>
                        <PropertiesBoxV3 profile={profile} />
                    </div>
                </div>
                <Login show={this.state.showLoginForm} showModal={this.openLoginForm.bind(this)} hideModal={this.closeLoginForm.bind(this)} getLoggedProfile={this.getLoggedProfile.bind(this)} />
                <UserReport show={this.state.showReportForm} hideModal={this.closeReportForm.bind(this)} profile={profile} />
                <ImageLightBox isOpen={this.state.isShowQrLightBox} images={[{ id: 0, thumbUrl: profile.profileQR, fullUrl: profile.profileQR }]} hideLightBox={this.hideQrLightBox} />
            </section>
        } else {
            content = <MessagePage message="Không tìm thấy thông tin." />
        }

        return <div className="mioto-layout">
            <Header />
            {content}
            <Footer />
        </div>
    }
}

function mapSate(state) {
    return {
        session: state.session
    }
}

Profile = connect(mapSate)(Profile);

export default Profile;