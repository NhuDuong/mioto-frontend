import React from "react"

import { commonErr } from "../common/errors"
import { getUserCars } from "../../model/car"
import { MessageLine } from "../common/messagebox"
import PropsCars from "./propscars"

class PropertiesBoxV3 extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            cars: [],
            err: commonErr.INNIT
        }
    }

    componentDidMount() {
        getUserCars(this.props.profile.uid, 0, 0, 0, 0, true).then(resp => {
            if (resp.data.error >= commonErr.SUCCESS && resp.data.data.cars) {
                this.setState({
                    cars: resp.data.data.cars,
                    err: resp.data.error
                });
            } else {
                this.setState({
                    err: resp.data.error
                });
            }
        });
    }

    render() {
        var content;
        if (this.state.cars && this.state.cars.length > 0) {
            content = <PropsCars featureCars={this.state.cars} />
        } else {
            content = <MessageLine message={"Không tìm thấy xe nào."} />
        }
        if (this.state.cars && this.state.cars.length > 0) {
            return <div className="properties">
                <div className="properties__wrap">
                    <div className="props-header">
                        <span className="lstitle">Xe của {this.props.profile.name} ({this.state.cars.length} xe)</span>
                    </div>
                    {content}
                </div>
            </div>
        } else {
            return null;
        }
    }
}

export default PropertiesBoxV3;