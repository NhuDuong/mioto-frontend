import React from "react"
import StarRatings from "react-star-ratings"

import { commonErr } from "../common/errors"
import { getOwnerReviews, getTravelerReviews } from "../../model/car"
import CommentItemV2 from "../common/commentitemv2"

export default class ProfileCommentBoxV2 extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            err: commonErr.INNIT,
            reviews: props.reviews || null,
            profiles: props.profiles || null,
            more: props.more ? 1 : 0
        }
    }

    componentDidMount() {
        if (!this.props.reviews || this.props.reviews.length === 0) {
            if (this.props.reviewType === 0) {
                getOwnerReviews(this.props.profile.uid, 0, 0, 0).then(resp => {
                    if (resp.data.error >= commonErr.SUCCESS && resp.data.data) {
                        this.setState({
                            err: resp.data.error,
                            reviews: resp.data.data.reviews,
                            profiles: resp.data.data.profiles,
                            totalReviews: resp.data.data.totalReviews,
                            more: resp.data.data.more
                        })
                    }
                });
            } else {
                getTravelerReviews(this.props.profile.uid, 0, 0, 0).then(resp => {
                    if (resp.data.error >= commonErr.SUCCESS && resp.data.data) {
                        this.setState({
                            err: resp.data.error,
                            reviews: resp.data.data.reviews,
                            profiles: resp.data.data.profiles,
                            totalReviews: resp.data.data.totalReviews,
                            more: resp.data.data.more
                        })
                    }
                });
            }
        }
    }

    getReviewsMore() {
        if (this.props.reviewType === 0) {
            getOwnerReviews(this.props.profile.uid, 0, this.state.reviews.length, 0).then(resp => {
                if (resp.data.error >= commonErr.SUCCESS && resp.data.data) {
                    this.setState({
                        err: resp.data.error,
                        reviews: this.state.reviews.concat(resp.data.data.reviews),
                        profiles: this.state.profiles.concat(resp.data.data.profiles),
                        more: resp.data.data.more
                    })
                }
            });
        } else {
            getTravelerReviews(this.props.profile.uid, 0, this.state.reviews.length, 0).then(resp => {
                if (resp.data.error >= commonErr.SUCCESS && resp.data.data) {
                    this.setState({
                        err: resp.data.error,
                        reviews: this.state.reviews.concat(resp.data.data.reviews),
                        profiles: this.state.profiles.concat(resp.data.data.profiles),
                        more: resp.data.data.more
                    })
                }
            });
        }
    }

    render() {
        var reviews;
        var profiles;
        var profile = this.props.profile;
        var rating;
        if (this.state.err >= commonErr.SUCCESS) {
            if (this.props.reviewType === 1) {
                reviews = this.state.reviews;
                profiles = this.state.profiles;
                if (profile.owner && this.state.totalReviews > 0) {
                    rating = <div className="rating">
                        <StarRatings
                            rating={profile.owner.rating.avg}
                            starRatedColor="#00a550"
                            starDimension="17px"
                            starSpacing="1px"
                        />
                        <div className="bar-line" />
                        <div className="trips">{this.state.totalReviews} đánh giá</div>
                    </div>
                }
            } else {
                reviews = this.state.reviews;
                profiles = this.state.profiles;
                if (profile.traveler && this.state.totalReviews > 0) {
                    rating = <div className="rating">
                        <StarRatings
                            rating={profile.traveler.rating.avg}
                            starRatedColor="#00a550"
                            starDimension="17px"
                            starSpacing="1px"
                        />
                        <div className="bar-line" />
                        <div className="trips">{this.state.totalReviews} đánh giá</div>
                    </div>
                }
            }
        }

        if (reviews) {
            return <div className="review review__wrap">
                <div className="head-content">
                    <span className="lstitle">{this.props.reviewType === 1 ? "Nhận xét từ khách thuê" : "Nhận xét từ chủ xe"}</span>
                    {rating}
                </div>
                {reviews.map(review => {
                    if (review.name && review.name !== "") {
                        return <CommentItemV2 key={review.id} review={review} />
                    } else if (profiles) {
                        var ownerProfile;
                        if (profiles.length > 0) {
                            for (var i = 0; i < profiles.length; ++i) {
                                if (profiles[i].uid === review.uid) {
                                    ownerProfile = profiles[i];
                                    break;
                                }
                            }
                        }
                        return <CommentItemV2 key={review.id} review={review} profile={ownerProfile} />
                    }
                })}
                {this.state.more === 1 && <div className="s-all">
                    <p className="see-all" onClick={this.getReviewsMore.bind(this)}>Xem thêm <i className="ic ic-chevron-up" /></p>
                </div>}
            </div>
        } else {
            return null;
        }
    }
}