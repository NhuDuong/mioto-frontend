import React from "react"
import StarRatings from "react-star-ratings"

import { commonErr } from "../common/errors"
import { getUserCars } from "../../model/car"
import { MessageLine } from "../common/messagebox"
import { formatPrice, formatTitleInUrl } from "../common/common"

import carPhotoDefault from "../../static/images/upload/car_1.png"

function CarItem(props) {
    var car = props.car;
    return <div className="item-car">
        <a href={`/car/${formatTitleInUrl(car.name)}/${car.id}`}>
            <span className="img-car" style={{ backgroundImage: `url(${car.photos ? car.photos[0].thumbUrl : carPhotoDefault})` }}></span>
            <div className="desc-car">
                <StarRatings
                    rating={car.rating.avg || 0}
                    starRatedColor="#00a550"
                    starDimension="17px"
                    starSpacing="1px"
                />
                <h2>{car.name}</h2>
                <p>
                    <span><i className="ic ic-clock"></i> {car.totalTrips} chuyến</span>
                    <span><i className="ic ic-tag"></i> {formatPrice(car.price)}</span>
                </p>
            </div>
        </a>
    </div>
}

class PropertiesBox extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            cars: [],
            err: commonErr.INNIT
        }
    }

    componentDidMount() {
        getUserCars(this.props.profile.uid, 0, 0, 0, 0, true).then(resp => {
            if (resp.data.error >= commonErr.SUCCESS && resp.data.data.cars) {
                this.setState({
                    cars: resp.data.data.cars,
                    err: resp.data.error
                });
            } else {
                this.setState({
                    err: resp.data.error
                });
            }
        });
    }

    render() {
        var content;
        if (this.state.cars && this.state.cars.length > 0) {
            content = this.state.cars.map(car =>
                <CarItem key={car.id} car={car} />
            );
        } else {
            content = <MessageLine message={"Không tìm thấy xe nào."} />
        }
        return <div className="properties">
            <span className="title title--s">Xe của {this.props.profile.name}</span>
            {content}
        </div>
    }
}

export default PropertiesBox;