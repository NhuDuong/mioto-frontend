import React from "react"
import StarRatings from "react-star-ratings"
import { connect } from "react-redux"
import { Modal } from "react-bootstrap"

import Header from "../common/header"
import Footer from "../common/footer"
import { LoadingPage } from "../common/loading"
import { getLoggedProfile } from "../../actions/sessionAct"
import { commonErr } from "../common/errors"
import PropertiesBox from "../profile/properties"
import { getProfile } from "../../model/profile"
import { getUserReportReasons, reportUser } from "../../model/car"
import ProfileCommentBox from "../profile/profilecommentbox"
import Login from "../login/login"
import ImageLightBox from "../common/imagelightbox"

import avatar_default from "../../static/images/avatar_default.png"
import def_profile_cover from "../../static/images/def_profile_cover.jpg"
import { MessagePage } from "../common/messagebox"

class UserReport extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            err: commonErr.INNIT,
            errMsg: "",
            reasons: null,

            ip_reason: "",
            ip_comment: ""
        }
    }

    handleInputChange = (event) => {
        this.setState({
            [event.target.name]: event.target.value,
            err: commonErr.INNIT,
            errMsg: ""
        });
    }

    reportUser() {
        const self = this;
        if (self.state.ip_reason === ""
            || self.state.ip_reason === "0"
            || (self.state.ip_reason === "u0" && (!self.state.ip_comment || self.state.ip_comment === ""))) {
            self.setState({
                err: commonErr.FAIL,
                errMsg: "Vui lòng chọn lý do hoặc ghi rõ lý do của bạn."
            });
        } else {
            reportUser(self.props.profile.uid, self.state.ip_reason, self.state.ip_comment).then(function (resp) {
                if (resp.data.error >= commonErr.SUCCESS) {
                    self.setState({
                        err: resp.data.error,
                        errMsg: resp.data.errorMessage
                    });
                    self.props.hideModal();
                } else {
                    self.setState({
                        err: resp.data.error,
                        errMsg: resp.data.errorMessage
                    });
                }
            });
        }
    }

    componentDidMount() {
        const self = this;
        getUserReportReasons(this.props.profile.uid).then(function (resp) {
            if (resp.data.error >= commonErr.SUCCESS) {
                self.setState({
                    err: resp.data.error,
                    errMsg: resp.data.errorMessage,
                    reasons: resp.data.data.reasons
                });
            } else {
                self.setState({
                    err: resp.data.error,
                    errMsg: resp.data.errorMessage
                });
            }
        });
    }

    render() {
        return <Modal
            show={this.props.show}
            onHide={this.props.hideModal}
            dialogClassName="modal-sm modal-dialog"
        >
            <Modal.Header closeButton={true} closeLabel={""}>
                <Modal.Title>Báo xấu tài khoản</Modal.Title>
            </Modal.Header>
            {this.state.err < commonErr.SUCCESS && this.state.errMsg !== "" && <p className="textAlign-center" style={{ color: "red" }}>{this.state.errMsg}</p>}
            <Modal.Body>
                <div className="form-default form-s">
                    <div className="line-form">
                        <div className="wrap-select">
                            <select name="ip_reason" onChange={this.handleInputChange.bind(this)}>
                                <option value="0" disabled selected>Chọn lý do</option>
                                {this.state.reasons && this.state.reasons.map(
                                    reason => <option key={reason.reason} value={reason.reason}>{reason.desc}</option>
                                )}
                            </select>
                        </div>
                    </div>
                    <div className="line-form">
                        <div className="wrap-input has-ico">
                            <input type="text" placeholder="Lời nhắn" name="ip_comment" onChange={this.handleInputChange.bind(this)} />
                        </div>
                    </div>
                    <div className="clear"></div>
                    <button className="btn btn-primary btn--m" type="button" name="cancelTripBtn" onClick={this.reportUser.bind(this)}>Báo xấu</button>
                </div>
            </Modal.Body>
        </Modal>
    }
}

class Profile extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            err: commonErr.INNIT,
            errMsg: "",
            data: null,
            reviewType: "0",
            showLoginForm: false,
            showReportForm: false,
            isShowQrLightBox: false
        }

        this.showQrLightBox = this.showQrLightBox.bind(this);
        this.hideQrLightBox = this.hideQrLightBox.bind(this);
    }

    componentDidMount() {
        window.scrollTo(0, 0);

        const self = this;
        getProfile(self.props.match.params.userId, "pd" /*view count for server*/).then(function (resp) {
            self.setState({
                err: resp.data.error,
                errMsg: resp.data.errorMessage,
                data: resp.data.data
            });
        });
    }

    showLoginForm() {
        this.setState({
            showLoginForm: true
        });
    }

    openLoginForm() {
        this.setState({
            showLoginForm: true
        });
    }

    closeLoginForm() {
        this.setState({
            showLoginForm: false
        });
    }

    showReportForm() {
        this.setState({
            showReportForm: true
        });
    }

    closeReportForm() {
        this.setState({
            showReportForm: false
        });
    }

    getLoggedProfile() {
        this.props.dispatch(getLoggedProfile());
    }

    reportUserBtnClick() {
        if (this.props.session.profile.info && this.props.session.profile.info.uid) {
            this.showReportForm();
        } else {
            this.showLoginForm();
        }
    }

    reviewTypeChange(event) {
        this.setState({
            reviewType: event.target.value
        });
    }

    showQrLightBox(i) {
        this.setState({
            isShowQrLightBox: true
        })
    }

    hideQrLightBox() {
        this.setState({
            isShowQrLightBox: false
        })
    }

    render() {
        var content;
        if (this.state.err === commonErr.INNIT || this.state.err === commonErr.LOADING) {
            content = <LoadingPage />
        } else if (this.state.err === commonErr.SUCCESS && this.state.data) {
            const profile = this.state.data.profile;
            content = <section className="body">
                <div className="cover-profile" style={{ backgroundImage: `url(${def_profile_cover})` }}>
                    <div className="qr-car" onClick={this.showQrLightBox}> <i className="ic ic-qr-code"></i></div>
                </div>
                <div className="module-profile">
                    <div className="profile-container">
                        <div className="content-profile">
                            <div className="avatar avatar--xl" title="title name">
                                <div className="avatar-img" style={{ backgroundImage: `url(${(profile && profile.avatar && profile.avatar.fullUrl) ? profile.avatar.fullUrl : avatar_default})` }}></div>
                            </div>
                            <div className="desc-top">
                                <h2 className="name-car">HỒ SƠ</h2>
                                <div className="snippet">
                                    <div className="item-title"><span>{profile ? profile.name : ""}<a className="func-edit" onClick={this.reportUserBtnClick.bind(this)} title="Báo xấu"><i className="ic ic-error-gray"></i></a></span></div>
                                </div>
                                <StarRatings
                                    rating={this.state.reviewType === "0" ? profile.owner.rating.avg : profile.traveler.rating.avg}
                                    starRatedColor="#00a550"
                                    starDimension="17px"
                                    starSpacing="1px"
                                />
                                <div className="wr-s">
                                    <div className="s-info"><span className="lstitle">Chủ xe đánh giá</span>{profile.traveler.totalReviewsFromOwner > 0 ? <span className="ctn">{profile.traveler.totalReviewsFromOwner} nhận xét</span> : "_"}</div>
                                    <div className="s-info"><span className="lstitle">Khách thuê đánh giá</span>{profile.owner.totalReviewsFromTraveler > 0 ? <span className="ctn">{profile.owner.totalReviewsFromTraveler} nhận xét</span> : "_"}</div>
                                </div>
                            </div>
                            <div className="clear"></div>
                            <div className="wr-s-info">
                                <div className="owner-response">
                                    <div className="response-desc">
                                        <p>Tỉ lệ phản hồi</p><span className="rate">{profile.owner.responseRate} </span>
                                    </div>
                                    <div className="response-desc">
                                        <p>Thời gian phản hồi</p><span className="rate">{profile.owner.responseTime} </span>
                                    </div>
                                    <div className="response-desc">
                                        <p>Tỉ lệ đồng ý</p><span className="rate">{profile.owner.acceptRate} </span>
                                    </div>
                                </div>
                            </div>
                            <div className="information">
                                <ul>
                                    <li><span className="label">Điện thoại</span><span className="ctn">{profile.phoneVerified ? <span> <i className="ic ic-verify"></i> Đã xác thực</span> : <span> <i className="ic ic-error"></i> Chưa xác thực</span>}</span></li>
                                    <li><span className="label">Email</span><span className="ctn">{profile.emailVerified ? <span> <i className="ic ic-verify"></i> Đã xác thực</span> : <span> <i className="ic ic-error"></i> Chưa xác thực</span>}</span></li>
                                    <li><span className="label">Facebook</span><span className="ctn">{profile.facebook > 0 ? <span> <i className="ic ic-verify"></i> Đã liên kết {profile.facebookFr > 0 && `(có ${profile.facebookFr} bạn)`}</span> : <span> <i className="ic ic-error"></i> Chưa liên kết</span>}</span></li>
                                    <li><span className="label">Google</span><span className="ctn">{profile.google > 0 ? <span> <i className="ic ic-verify"></i> Đã liên kết</span> : <span> <i className="ic ic-error"></i> Chưa liên kết</span>}</span></li>
                                </ul>
                            </div>
                            <div className="clear"></div>
                            <PropertiesBox profile={profile} />
                        </div>
                        <div className="sidebar-profile">
                            <div className="head-content">
                                <span className="lstitle">Nhận xét</span>
                                <span className="wrap-select">
                                    <select defaultValue={this.state.reviewType} onChange={this.reviewTypeChange.bind(this)}>
                                        <option value="0">Từ khách thuê</option>
                                        <option value="1">Từ chủ xe</option>
                                    </select>
                                </span>
                            </div>
                            <ProfileCommentBox profile={this.state.data.profile} reviewType={this.state.reviewType} />
                        </div>
                        <div className="clear"></div>
                        <Login show={this.state.showLoginForm} showModal={this.openLoginForm.bind(this)} hideModal={this.closeLoginForm.bind(this)} getLoggedProfile={this.getLoggedProfile.bind(this)} />
                        <UserReport show={this.state.showReportForm} hideModal={this.closeReportForm.bind(this)} profile={profile} />
                        <ImageLightBox isOpen={this.state.isShowQrLightBox} images={[{ id: 0, thumbUrl: profile.profileQR, fullUrl: profile.profileQR }]} hideLightBox={this.hideQrLightBox} />
                    </div>
                </div>
            </section>
        } else {
            content = <MessagePage message="Không tìm thấy thông tin hồ sơ." />
        }

        return <div className="mioto-layout">
            <Header />
            {content}
            <Footer />
        </div>
    }
}

function mapSate(state) {
    return {
        session: state.session
    }
}

Profile = connect(mapSate)(Profile);

export default Profile;