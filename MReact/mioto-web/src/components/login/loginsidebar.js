import React from 'react'
import { connect } from "react-redux"

import { getLoggedProfile, logout } from "../../actions/sessionAct"
import {getUserNotify, seenUserNotify, seenSystemNotify} from "../../model/profile"

import { commonErr } from "../common/errors"
import { LoadingInline } from "../common/loading"
import Login from "../login/login"
import ChangePassword from "../account/changepw"
// import { seenSystemNotify } from "../../model/profile"

import avatar_default from "../../static/images/avatar_default.png"

class GuestMenu extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      showModal: false
    }
  }

  render() {
    return <div className="sidebar-body" style={{ background: "white", width: "100vw", height: "100vh" }}>
      <ul>
        <li><a onClick={this.props.openLoginForm}>Đăng nhập</a></li>
        <li><a href="/signup">Đăng kí</a></li>
      </ul>
    </div>
  }
}

class LoggedMenu extends React.Component {
  constructor() {
    super();
    this.state = {
			showChangePassword: false,
			hasNewUp: false,
			hasPromo: false,
		}
		this.onPromoLinkClick = this.onPromoLinkClick.bind(this);
  }
  componentWillReceiveProps(props) {
		const snotis = props.appInit.data.snotifs;
		if (snotis) {
        const unoti = snotis.filter(n => {
          return (n.type === 2 /*UPCOMING NOTI*/ && n.reddot > 0)
        });
				const snoti = snotis.filter(n => {
						return (n.type === 3 /*PROMO NOTI*/ && n.reddot > 0)
        });
				if (snoti && snoti.length > 0) {
						this.setState({
								hasPromo: true
						})
        }
        if (unoti && unoti.length > 0) {
          this.setState({
              hasNewUp: true
          })
        }
			}
	}
	onMyTripsClick() {
		const snotis = this.props.appInit.data.snotifs;
		if (snotis) {
			const snoti = snotis.filter(n => {
				return (n.type === 2 /*UPCOMMING NOTI*/ && n.reddot > 0)
			});
			if (snoti && snoti.length > 0) {
				seenSystemNotify(snoti[0].id, snoti[0].type);
				window.location.href = "/mytrips";
				return;
			}
		}

		window.location.href = "/mytrips";
	}
	

  onPromoLinkClick() {
    const snotis = this.props.appInit.data.snotifs;
    if (snotis) {
      const snoti = snotis.filter(n => {
        return (n.type === 3 /*UPCOMMING NOTI*/ && n.reddot > 0)
      });
      if (snoti && snoti.length > 0) {
        seenSystemNotify(snoti.id, snoti.type).then(resp => {
          window.location.href = "/promo";
          return;
        });
      }
    }
    window.location.href = "/promo";
  }

  render() {
    return <div className="sidebar-header">
      <div className="avatar avatar--m" title="title name">
        <a href="/account">
          <div className="avatar-img" style={{ "backgroundImage": `url(${(this.props.profile.avatar && this.props.profile.avatar.thumbUrl) ? this.props.profile.avatar.thumbUrl : avatar_default})` }}></div>
        </a>
      </div>
      <div className="snippet">
        <a href={`/account`}>
          <div className="item-title">
            <span>{this.props.profile.name}</span>
          </div>
        </a>
      </div>
      <div className="space m"></div>
      <div className="sidebar-body">
        <ul>
					<li><a href="/account">Tài khoản</a></li>
          <li><a href="/myfavs">Xe yêu thích</a></li>
          <li><a href="/mycars">Xe của tôi </a></li>
          <li><a onClick={this.onMyTripsClick.bind(this)}>
						<span className={this.state.hasNewUp ? "has-dot-red" : ""}>Chuyến của tôi</span></a>
					</li>
					{/* <li><a href="/myrentaltrips">Chuyến cho thuê</a></li> */}
					<li><a href="/mycard">Thẻ của tôi </a></li>
          <li><a onClick={this.onPromoLinkClick}><span className={this.state.hasPromo ? "has-dot-red" : ""}>Khuyến mãi</span></a></li>
          <li><a href="/sharedcode">Giới thiệu bạn bè</a></li>
          <li><a onClick={this.props.showChangePassword}>Đổi mật khẩu</a></li>
          <li><a onClick={this.props.logout}>Đăng xuất</a></li>
        </ul>
      </div>
      <div className="space m"></div>
    </div>
  }
}

function mapAppInitState(state) {
  return {
    appInit: state.appInit
  }
}

LoggedMenu = connect(mapAppInitState)(LoggedMenu);

class LoginSidebar extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      showModal: false,
      showChangePassword: false
    }

    this.getLoggedProfile = this.getLoggedProfile.bind(this);
    this.logout = this.logout.bind(this);
  }

  getLoggedProfile() {
    this.props.dispatch(getLoggedProfile());
  }

  logout() {
    this.props.dispatch(logout());
  }

  openLoginForm() {
    this.setState({
      showModal: true
    });
  }

  closeLoginForm() {
    this.setState({
      showModal: false
    });
  }

  showChangePassword() {
    this.setState({
      showChangePassword: true
    });
  }

  hideChangePassword() {
    this.setState({
      showChangePassword: false
    });
  }

  render() {
    var menuCont;
    if (this.props.session.profile.err === commonErr.LOADING) {
      menuCont = <LoadingInline />;
    } else if (this.props.session.profile.info && this.props.session.profile.info.uid) {
      menuCont = <LoggedMenu logout={this.logout} profile={this.props.session.profile.info} showChangePassword={this.showChangePassword.bind(this)} />;
    } else {
      menuCont = <GuestMenu getLoggedProfile={this.getLoggedProfile} openLoginForm={this.openLoginForm.bind(this)} />;
    }

    return <div className="sidebar-container" style={{ background: "white", width: "100vw", height: "100vh" }}>
      {menuCont}
      <Login show={this.state.showModal} showModal={this.openLoginForm.bind(this)} hideModal={this.closeLoginForm.bind(this)} getLoggedProfile={this.getLoggedProfile} />
      <ChangePassword show={this.state.showChangePassword} hideModal={this.hideChangePassword.bind(this)} />
    </div>
  }
}

function mapState(state) {
  return {
    session: state.session
  }
}

LoginSidebar = connect(mapState)(LoginSidebar);

export default LoginSidebar;