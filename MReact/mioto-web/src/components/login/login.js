import React from "react"
import axios from "axios"
import { Modal } from "react-bootstrap"
import validator from "validator"
import cookie from 'react-cookies'

import Login3rd from "../login/login3rd"
import { commonErr } from "../common/errors"
import { fbappInfo } from "../common/fbdeveloper"
import { login, resetPasswordByPhone, checkPhone, checkEmail } from "../../model/profile"
import { LoadingInline } from "../common/loading"
import AccountKit from "../common/accountkit"
import ResetPwByEmail from "../account/resetpwbyemail"

class ResetPwForm extends React.Component {
	constructor() {
		super();
		this.state = {
			err: commonErr.INNIT,
			errMsg: "",
			ip_reset_pw: "",
			ip_reset_pw_confirm: ""
		}

		this.handleInputChange = this.handleInputChange.bind(this);
	}

	handleInputChange(event) {
		this.setState({
			[event.target.name]: event.target.value
		});
	}

	onResetPwBtnClick() {
		const self = this;
		if (!self.state.ip_reset_pw || !self.state.ip_reset_pw_confirm
			|| self.state.ip_reset_pw === "" || self.state.ip_reset_pw_confirm === "") {
			self.setState({
				err: commonErr.FAIL,
				errMsg: "Mật khẩu không hợp lệ."
			});
		} else if (self.state.ip_reset_pw !== self.state.ip_reset_pw_confirm) {
			self.setState({
				err: commonErr.FAIL,
				errMsg: "Mật khẩu lặp lại không đúng."
			});
		} else {
			resetPasswordByPhone(self.props.resetPwPhone, self.props.resetAccessTok, self.state.ip_reset_pw).then(function (resp) {
				self.setState({
					err: resp.error,
					errMsg: resp.errorMessage
				});
			});
		}
	}

	onFinishButtonClick() {
		this.props.hideModal();
		this.props.hideForgotPasswordForm();
		this.props.showLoginModal();
	}

	render() {
		var content;
		if (this.state.err === commonErr.INNIT || this.state.err < commonErr.SUCCESS) {
			content = <div className="form-default form-s">
				{this.state.err < commonErr.SUCCESS && <div className="textAlign-center"><i className="ic ic-error"></i> {this.state.errMsg}<div className="space m"></div></div>}
				<div className="line-form">
					<div className="wrap-input has-ico">
						<i className="ic ic-lock-fill"></i>
						<input type="password" placeholder="Mật khẩu mới" name="ip_reset_pw" onChange={this.handleInputChange} />
					</div>
				</div>
				<div className="line-form">
					<div className="wrap-input has-ico">
						<i className="ic ic-lock-fill"></i>
						<input type="password" placeholder="Lặp lại mật khẩu" name="ip_reset_pw_confirm" onChange={this.handleInputChange} />
					</div>
				</div>
				<div className="clear"></div>
				<button className="btn btn-primary btn--m" type="button" name="resetPw" onClick={this.onResetPwBtnClick.bind(this)}>Cập nhật mật khẩu</button>
			</div>
		} else {
			content = <div className="form-default form-s">
				<div className="textAlign-center"><i className="ic ic-verify"></i> Mật khẩu đã được làm mới thành công</div>
				<div className="clear"></div>
				<div className="space m"></div>
				<button className="btn btn-primary btn--m" type="button" name="resetPw" onClick={this.onFinishButtonClick.bind(this)}>Đăng nhập</button>
			</div>
		}
		return <Modal
			show={this.props.show}
			onHide={this.props.hideModal}
			dialogClassName="modal-sm modal-dialog"
		>
			<Modal.Header closeButton={true} closeLabel={""}>
				<Modal.Title>Đặt lại mật khẩu</Modal.Title>
			</Modal.Header>
			<Modal.Body>
				{content}
			</Modal.Body>
		</Modal>
	}
}

class ForgotPasswordForm extends React.Component {
	constructor() {
		super();
		this.state = {
			err: commonErr.INNIT,
			errMsg: "",
			phoneEmail: "",
			isPhone: true
		}
	}

	onPhoneEmailChange(event) {
		const data = event.target.value;
		if (validator.isNumeric(data)) {
			this.setState({
				err: commonErr.INNIT,
				errMsg: "",
				phoneEmail: data,
				isPhone: true
			});
		} else {
			this.setState({
				err: commonErr.INNIT,
				errMsg: "",
				phoneEmail: data,
				isPhone: false
			});
		}
	}

	checkInput() {
		if (this.state.isPhone) {
			this.checkPhone();
		} else {
			this.checkEmail();
		}
	}

	checkPhone() {
		const self = this;
		const data = self.state.phoneEmail;
		if (!validator.isNumeric(data)) {
			self.setState({
				err: commonErr.FAIL,
				errMsg: "Số điện thoại không hợp lệ."
			});
		}

		self.setState({
			err: commonErr.LOADING,
			errMsg: ""
		});

		checkPhone(data).then(function (resp) {
			if (!resp.data || !resp.data.data || resp.data.data.exist !== 1) {
				self.setState({
					err: commonErr.FAIL,
					errMsg: "Không tìm thấy tài khoản với số điện thoại bạn cung cấp. Vui lòng kiểm tra lại hoặc đăng kí tài khoản mới."
				});
			} else {
				self.setState({
					err: commonErr.SUCCESS,
					errMsg: ""
				});
				self.props.hide();
				self.accountKitElement.click();
			}
		});
	}

	checkEmail() {
		const self = this;
		const data = self.state.phoneEmail;

		if (!validator.isEmail(data)) {
			self.setState({
				err: commonErr.FAIL,
				errMsg: "Email không hợp lệ."
			});
		} else {
			self.setState({
				err: commonErr.LOADING,
				errMsg: ""
			});

			checkEmail(data).then(function (resp) {
				if (!resp.data || !resp.data.data || resp.data.data.exist !== 1) {
					self.setState({
						err: commonErr.FAIL,
						errMsg: "Không tìm thấy tài khoản với địa chỉ email bạn cung cấp. Vui lòng kiểm tra lại hoặc đăng kí tài khoản mới."
					});
				} else {
					self.setState({
						err: commonErr.SUCCESS,
						errMsg: ""
					});
					self.props.hide();
					self.props.showVerifyEmail(data);
				}
			});
		}
	}

	render() {
		return <Modal
			show={this.props.isShow}
			onHide={this.props.hide}
			dialogClassName="modal-sm modal-dialog"
		>
			<Modal.Header closeButton={true} closeLabel={""}>
				<Modal.Title>Quên mật khẩu</Modal.Title>
			</Modal.Header>
			<Modal.Body>
				<div className="form-default form-s">
					{this.state.err === commonErr.LOADING && <LoadingInline />}
					{this.state.err < commonErr.SUCCESS && <div className="textAlign-center"><i className="ic ic-error"></i> {this.state.errMsg}<div className="space m"></div></div>}
					<div className="line-form">
						<div className="wrap-input has-ico">
							<input type="text" placeholder="Điện thoại hoặc email" value={this.state.phoneEmail} onChange={this.onPhoneEmailChange.bind(this)} />
						</div>
					</div>
					<div className="clear"></div>
					<AccountKit
						appId={fbappInfo.appId}
						csrf={fbappInfo.csrf}
						version={fbappInfo.version}
						language={fbappInfo.language}
						phoneNumber={this.state.phoneEmail.replace("+84", "")}
						onResponse={(resp) => this.props.onAccountKitLoginResp(resp)}
					>
						{p => <a ref={link => this.accountKitElement = link} {...p} className="hidden"></a>}
					</AccountKit>
					<button disabled={this.state.phoneEmail === ""} className="btn btn-primary btn--m" type="button" onClick={this.checkInput.bind(this)}>Tiếp tục</button>
				</div>
			</Modal.Body>
		</Modal>
	}
}

class Login extends React.Component {
	constructor(props) {
		super(props);
		cookie.save("_mcookietest", "_mcookietest", { path: '/' });
		const mcookietest = cookie.load("_mcookietest");
		cookie.remove("_mcookietest");
		this.state = {
			ip_username: "",
			ip_password: "",
			err: commonErr.INNIT,
			errMsg: "",

			resetPwPhone: "",
			resetAccessTok: "",
			showResetPwForm: false,
			isCookieAllowed: (mcookietest !== undefined && mcookietest !== null && mcookietest !== "")
		};

		this.onAccountKitLoginResp = this.onAccountKitLoginResp.bind(this);
	}

	handleInputChange = (event) => {
		this.setState({
			[event.target.name]: event.target.value
		});
	}

	onLoginBtnClick = (event) => {
		const username = this.state.ip_username;
		const password = this.state.ip_password;
		const self = this;
		if (username !== "" && password !== "") {
			this.setState({ err: commonErr.LOADING });
			login(username, password).then(function (resp) {
				self.setState({
					err: resp.data.error,
					errMsg: resp.data.errorMessage
				});
				if (resp.data.error >= commonErr.SUCCESS) {
					self.props.getLoggedProfile();
					self.props.hideModal();
				}
			});
		}
	}

	showForgotPasswordForm() {
		this.setState({
			isShowForgotPasswordForm: true,
		});
		this.props.hideModal();
	}

	hideForgotPasswordForm() {
		this.setState({
			isShowForgotPasswordForm: false
		});
	}

	hideResetPwForm() {
		this.setState({
			showResetPwForm: false,
			resetPwPhone: "",
			resetAccessTok: ""
		});
	}

	onAccountKitLoginResp(resp) {
		const self = this;
		self.props.hideModal();

		if (resp.code) {
			axios({
				method: "GET",
				url: `${fbappInfo.getAccessTokBaseDomain}?grant_type=authorization_code&code=${resp.code}&access_token=AA|${fbappInfo.appId}|${resp.state}`
			}).then(function (tokenData) {
				if (tokenData.data) {
					var resetAccessTok = tokenData.data.access_token;
					if (resetAccessTok) {
						axios({
							method: "GET",
							url: `${fbappInfo.getLoggedInfoBaseDomain}/?access_token=${resetAccessTok}`
						}).then(function (accountData) {
							if (accountData.data && accountData.data.phone) {
								var resetPwPhone = accountData.data.phone.number;
								if (resetPwPhone) {
									self.setState({
										showResetPwForm: true,
										resetPwPhone: resetPwPhone,
										resetAccessTok: resetAccessTok
									})
								}
							}
						})
					}
				}
			})
		}
	}

	showVerifyEmail(email) {
		this.setState({
			isShowVerifyEmail: true,
			isShowForgotPasswordForm: false,
			emailForReset: email
		});
	}

	hideVerifyEmail() {
		this.setState({
			isShowVerifyEmail: false,
		});
	}

	onBackBtnClick() {
		this.setState({
			err: commonErr.INNIT,
			errMsg: ""
		})
	}

	render() {
		var content;
		if (!this.state.isCookieAllowed) {
			content = <div className="form-default form-s">
				<div className="textAlign-center"><i className="ic ic-error"></i> Bạn cần cài đặt trình duyệt cho phép sử dụng cookies mới có thể đăng nhập hệ thống.</div>
			</div>
		} else if (this.state.errMsg && this.state.errMsg !== "") {
			content = <div className="form-default form-s">
				<div className="textAlign-center"><i className="ic ic-error"></i> {this.state.errMsg}</div>
				<div className="clear"></div>
				<div className="space m"></div>
				<button className="btn btn-primary btn--m" type="button" name="backBtn" onClick={this.onBackBtnClick.bind(this)}>Thử lại</button>
			</div>
		} else {
			content = <div className="form-default form-s">
				<div className="line-form">
					<div className="wrap-input has-ico">
						<i className="ic ic-email-fill"></i>
						<input type="text" placeholder="Số điện thoại hoặc email" name="ip_username" onChange={this.handleInputChange.bind(this)} />
					</div>
				</div>
				<div className="line-form">
					<div className="wrap-input has-ico">
						<i className="ic ic-lock-fill"></i>
						<input type="password" placeholder="Mật Khẩu" name="ip_password" onChange={this.handleInputChange.bind(this)} />
					</div>
				</div>
				<a className="forgot-password" onClick={this.showForgotPasswordForm.bind(this)}>Quên mật khẩu?</a>
				<div className="clear"></div>
				<a className="btn btn-primary btn--m" onClick={this.onLoginBtnClick.bind(this)}>Đăng nhập {this.state.err === commonErr.LOADING && <LoadingInline />}</a>
				<p>Bạn chưa là thành viên? <a href="/signup">Hãy đăng kí ngay!</a></p><span className="line"></span>
				<p className="textAlign-center">Hoặc đăng nhập bằng tài khoản</p>
				<Login3rd />
			</div>
		}
		return <div>
			<Modal
				show={this.props.show}
				onHide={this.props.hideModal}
				dialogClassName="modal-sm modal-dialog"
			>
				<Modal.Header closeButton={true} closeLabel={""}>
					<Modal.Title>Đăng nhập</Modal.Title>
				</Modal.Header>
				<Modal.Body>
					{content}
				</Modal.Body>
			</Modal>
			<ForgotPasswordForm isShow={this.state.isShowForgotPasswordForm} hide={this.hideForgotPasswordForm.bind(this)} onAccountKitLoginResp={this.onAccountKitLoginResp.bind(this)} showVerifyEmail={this.showVerifyEmail.bind(this)} />
			<ResetPwForm show={this.state.showResetPwForm} hideModal={this.hideResetPwForm.bind(this)} hideForgotPasswordForm={this.hideForgotPasswordForm.bind(this)} showLoginModal={this.props.showModal} resetPwPhone={this.state.resetPwPhone} resetAccessTok={this.state.resetAccessTok} />
			{this.state.isShowVerifyEmail && <ResetPwByEmail show={this.state.isShowVerifyEmail} hideModal={this.hideVerifyEmail.bind(this)} showLoginModal={this.props.showModal} email={this.state.emailForReset} />}
		</div>
	}
}

export default Login;