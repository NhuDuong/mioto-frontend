import React from "react";

export default class Login3rd extends React.Component {
    openFbLogin = () => {
        window.miotoLogin.openPopup('facebook');
        return false;
    }

    openGgLogin = () => {
        window.miotoLogin.openPopup('google');
        return false;
    }

    render() {
        return <div className="wr-btn-share">
            <div className="wr-btn">
                <a className="btn btn-facebook btn--m" type="button" onClick={this.openFbLogin}><i className="ic ic-facebook"></i> Facebook</a>
            </div>
            <div className="wr-btn">
                <a className="btn btn-google btn--m" type="button" onClick={this.openGgLogin}><i className="ic ic-google"></i> Google</a>
            </div>
        </div>
    }
}