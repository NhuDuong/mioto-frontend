import React from "react"
import StarRatings from "react-star-ratings"
import moment from 'moment'
import avatar_default from "../../static/images/avatar_default.png"

import { getCityReviews } from "../../model/car"

class Review extends React.Component {
    constructor() {
        super();
        this.state = {
            reviews: [],
            profiles: [],
            more: false
        }

        this.getMoreReviews = this.getMoreReviews.bind(this);
    }

    componentDidMount() {
        const reviews = this.props.reviews;
        const profiles = this.props.profiles;
        const more = this.props.hasMore;
        this.setState({
            reviews: this.state.reviews.concat(reviews),
            profiles: this.state.reviews.concat(profiles),
            more: more,
        })
    }

    getMoreReviews() {
        getCityReviews(this.props.city.city, 0, this.state.reviews.length, 0).then(resp => {
            if (resp.data.data && resp.data.data.reviews && resp.data.data.profiles) {
                this.setState({
                    err: resp.data.error,
                    reviews: this.state.reviews.concat(resp.data.data.reviews),
                    profiles: this.state.profiles.concat(resp.data.data.profiles),
                    more: resp.data.data.more
                })
            }
        })
    }

    render() {
        return <div className="avg-ratings__sect review">
            <div className="m-container">
                <h3 className="title">Những nhận xét tại {this.props.city.name}</h3>
                <hr className="line" />
                <div className="list-cmt-wrap">
                    {this.state.reviews.map(review => {
                        var profile;
                        if (this.state.profiles.length > 0) {
                            for (var i = 0; i < this.state.profiles.length; ++i) {
                                if (this.state.profiles[i].uid === review.uid) {
                                    profile = this.state.profiles[i];
                                    break;
                                }
                            }
                        }
                        return <div className="list-comments">
                            <div className="list-wrap">
                                <div className="left">
                                    <div className="avatar avatar--s">
                                        {!review.avt && profile && <a href={`/profile/${profile.uid}`} target="_blank">
                                            <img className="img-fluid" src={(profile.avatar && profile.avatar.thumbUrl) ? profile.avatar.thumbUrl : avatar_default} alt="Mioto - Thuê xe tự lái" />
                                        </a>}
                                        {review.avt && review.avt !== "" && <img className="img-fluid" src={review.avt} alt="Mioto - Thuê xe tự lái" />}
                                    </div>
                                </div>
                                <div className="right">
                                    {!review.name && profile && <a href={`/profile/${profile.uid}`} target="_blank">
                                        <h4 className="name">{profile.name}</h4>
                                    </a>}
                                    {review.name && review.name !== "" && <h4 className="name">{review.name}</h4>}
                                    <div className="cmt-box">
                                        <div className="group">
                                            <StarRatings
                                                rating={review.rating}
                                                starRatedColor="#00a550"
                                                starDimension="17px"
                                                starSpacing="1px"
                                            />
                                            <p className="date">{moment(review.timeCreated).fromNow()}</p>
                                        </div>
                                        <p className="desc">{review.comment}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    })}
                </div>
                {this.state.more == true && <div className="s-all">
                    <a onClick={this.getMoreReviews} className="see-all">Xem thêm<i className="ic ic-chevron-up"></i></a>
                </div>}
            </div>
        </div>
    }
}

export default Review;