import React from "react"
import { Link } from 'react-router-dom'

class FeatureDes extends React.Component {
    render() {
        return <div className="nearby-car__sect">
            <div className="m-container">
                <h3 className="title">Địa điểm nổi bật tại {this.props.city}</h3>
                <ul className="list-areas">
                    {this.props.des.map(des => {
                        return <li className="area">
                            <Link to={{
                                pathname: "/find/filter",
                                search: `lat=${des.lat}&lng=${des.lon}&address=${des.name}`
                            }}><p>{des.name}</p></Link>
                        </li>
                    })}
                </ul>
            </div>
        </div>
    }
}

export default FeatureDes;