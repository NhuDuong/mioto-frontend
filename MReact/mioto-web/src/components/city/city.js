import React from "react"
import cookie from "react-cookies"
import queryString from 'query-string'
import { Redirect } from "react-router-dom"

import { getCity } from "../../model/common"
import Header from "../common/header"
import Footer from "../common/footer"
import FeatureCars from "./featurecars"
import Cover from "./cover"
import DealCars from "./dealcars"
import FeatureDes from "./featuredes"
import Discover from "./discover"
import Review from "./review"
import Blogs from "../homev2/blogs"
import AppInstall from "../homev2/appinstall"
import { formatTitleInUrl } from "../common/common"
import { commonErr } from "../common/errors"
import { LoadingPage } from "../common/loading"

class City extends React.Component {
    constructor() {
        super();
        this.state = {
            err: commonErr.INNIT
        }
    }

    componentDidMount() {
        const city = this.props.match.params.city;

        //GA
        if (this.props.location && this.props.location.search) {
            const query = queryString.parse(this.props.location.search);
            if (query.utm_source) {
                cookie.save("_utm_src", query.utm_source, { path: '/' });
                cookie.save("_utm_src", query.utm_source, { path: '/', domain: '.mioto.vn' });
            }
            if (query.utm_ext) {
                cookie.save("_utm_ext", query.utm_ext, { path: '/' });
                cookie.save("_utm_ext", query.utm_ext, { path: '/', domain: '.mioto.vn' });
            } else {
                cookie.remove("_utm_ext", { path: '/' });
                cookie.remove("_utm_ext", { path: '/', domain: '.mioto.vn' });
            }
        }

        getCity(city).then(resp => {
            if (resp.data.data) {
                this.setState({
                    err: resp.data.error,
                    city: resp.data.data.city,
                    dealArounds: resp.data.data.dealArounds,
                    featureCars: resp.data.data.featureCars,
                    destArounds: resp.data.data.destArounds,
                    reviews: resp.data.data.reviews,
                    moreReview: resp.data.data.moreReview,
                    profiles: resp.data.data.profiles,
                })
            } else {
                this.setState({
                    err: resp.data.error
                })
            }
        });

        const utmSrc = cookie.load("_utm_src") || "web_directly";
        const utmExt = cookie.load("_utm_ext") || "no_label";
        window.ga('send', 'event', utmSrc, "VIEW_HOME", utmExt);
    }

    render() {
        if (this.state.err < commonErr.SUCCESS) {
            return <Redirect to="/notfound" />
        } else if (this.state.err === commonErr.INIT) {
            return <LoadingPage />
        } else {
            return <div className="mioto-layout">
                <Header isReloadable={true} />
                {this.state.city && <section className="body">
                    <Cover city={this.state.city}/>
                    {this.state.dealArounds && <DealCars city={this.state.city.name} cars={this.state.dealArounds} />}
                    {this.state.featureCars && <FeatureCars city={this.state.city.name} cars={this.state.featureCars} />}
                    {this.state.reviews && <Review city={this.state.city} reviews={this.state.reviews} profiles={this.state.profiles} hasMore={this.state.moreReview} />}
                    {this.state.destArounds && <FeatureDes city={this.state.city.name} des={this.state.destArounds} />}
                    <Blogs city={this.state.city.city} />
										<Discover city={this.state.city.name} lat={this.state.city.lat} lon={this.state.city.lon} />
										<AppInstall/>
                </section>}
                <Footer />
            </div>
        }
    }
}

export default City;