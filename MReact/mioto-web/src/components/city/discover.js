import React from "react"
import { Link } from 'react-router-dom'

import bg from "../../static/images/discover/discover.png"

class Discover extends React.Component {
    render() {
        return <div className="start-adventure__sect" style={{ backgroundImage: `url(${bg})` }}>
            <div className="m-container">
                <div className="start-adventure__wrap">
                    <h3 className="n-title">Bắt đầu chuyến đi của bạn </h3>
                    <Link to={{
                        pathname: "/find/filter",
                        search: `address=${this.props.city}&lat=${this.props.lat}&lng=${this.props.lon}`
                    }} className="btn btn--l btn-primary">Tìm xe ở {this.props.city}</Link>
                </div>
            </div>
        </div>
    }
}

export default Discover;