import React from "react"
import StarRatings from "react-star-ratings"

import { formatPrice, formatTitleInUrl } from "../common/common"

import carPhotoDefault from "../../static/images/upload/car_1.png"

class Deals extends React.Component {
    constructor() {
        super();
        this.state = {

        }
    }

    componentDidMount() {
        this.swiperMain = new window.Swiper(this.refs.swiperMain, {
            nextButton: ".swiper-button-next",
            prevButton: ".swiper-button-prev",
            spaceBetween: 15,
            loopedSlides: 50,
            slidesPerView: 1,
            loop: true
        })

        this.swiperList = new window.Swiper(this.refs.swiperList, {
            spaceBetween: 5,
            slidesPerView: "auto",
            touchRatio: 0.2,
            slideToClickedSlide: true,
            loopedSlides: 50,
            direction: 'vertical',
            centeredSlides: true,
            loop: true
        })

        this.swiperMain.controller.control = this.swiperList;
        this.swiperList.controller.control = this.swiperMain;
    }

    render() {
        return <div className="deal-area__sect">
            <div className="m-container">
                <h3 className="title-car">Xe giảm giá tại {this.props.city}</h3>
                <div className="deal-car__wrap">
                    <div ref="swiperMain" className="swiper-container gallery-top">
                        <div className="swiper-wrapper">
                            {this.props.deals.map(car => {
                                return <div className="swiper-slide common-box box-lg-car__item">
                                    <a href={`/car/${formatTitleInUrl(car.name)}/${car.id}`}>
                                        <img className="img" src={car.photos ? car.photos[0].fullUrl : carPhotoDefault} alt={`Cho thuê xe tự lái ${car.name}`} />
                                        <div className="price-car">
                                            {car.priceOrigin !== car.price && <span className="real">{formatPrice(car.priceOrigin)}</span>}
                                            {formatPrice(car.price)}
                                        </div>
                                        <div className="desc-car">
                                            <div className="ratings">
                                                <span className="star">
                                                    <StarRatings
                                                        rating={car.rating.avg || 0}
                                                        starRatedColor="#00a550"
                                                        starDimension="17px"
                                                        starSpacing="1px"
                                                    />
                                                </span>
                                                <div className="bar-line"> </div>
                                                <p className="trips">{car.totalTrips} chuyến </p>
                                            </div>
                                            <h2>{car.name}</h2>
                                        </div>
                                    </a>
                                </div>
                            })}
                        </div>
                    </div>
                    <div ref="swiperList" className="swiper-container gallery-thumbs">
                        <div className="swiper-wrapper box-sm-car__wrap">
                            {this.props.deals.map(car => {
                                return <div className="swiper-slide">
                                    <div className="common-box box-sm-car__item">
                                        <div className="left">
                                            <div className="car-img">
                                                <div className="fix-img"><img src={car.photos ? car.photos[0].thumbUrl : carPhotoDefault} alt={`Cho thuê xe tự lái ${car.name}`} /></div>
                                            </div>
                                            <div className="price-car">
                                                {car.priceOrigin !== car.price && <span className="real">{formatPrice(car.priceOrigin)}</span>}
                                                {formatPrice(car.price)}
                                            </div>
                                        </div>
                                        <div className="right">
                                            <div className="desc-car">
                                                <h2>{car.name}</h2>
                                                <div className="ratings"> <span className="star">
                                                    <StarRatings
                                                        rating={car.rating.avg || 0}
                                                        starRatedColor="#00a550"
                                                        starDimension="17px"
                                                        starSpacing="1px"
                                                    />
                                                </span>
                                                    <div className="bar-line"> </div>
                                                    <p className="trips">{car.totalTrips} chuyến </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            })}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    }
}

export default Deals;