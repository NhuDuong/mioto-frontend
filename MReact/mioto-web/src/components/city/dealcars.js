import React from "react"
import StarRatings from "react-star-ratings"
import { Link } from "react-router-dom"

import { formatPrice, formatTitleInUrl } from "../common/common"

import carPhotoDefault from "../../static/images/upload/car_1.png"

function getRandom(arr, n) {
    var result = new Array(n),
        len = arr.length,
        taken = new Array(len);
    if (n > len)
        n = len;
    while (n--) {
        var x = Math.floor(Math.random() * len);
        result[n] = arr[x in taken ? taken[x] : x];
        taken[x] = --len in taken ? taken[len] : len;
    }
    return result;
}

function CarItem(props) {
    const car = props.car;
    const city = props.city;

    return <div className="swiper-slide box-car__item">
        <Link to={`/car/${formatTitleInUrl(car.name)}/${car.id}`}>
            <div className="img-car">
                <div className="fix-img"> <img src={car.photos ? car.photos[0].thumbUrl : carPhotoDefault} alt={`Cho thuê xe tự lái ${car.name} ${city}`} /></div>
                <div className="price-car">
                    {car.priceOrigin !== car.price && <span className="real">{formatPrice(car.priceOrigin)}</span>}
                    {formatPrice(car.price)}
                </div>
                <span className="label-pos">
                    {car.totalDiscountPercent > 0 && <span className="discount">Giảm {car.totalDiscountPercent}%</span>}
                </span>
            </div>
            <div className="desc-car">
                <div className="ratings">
                    <span className="star">
                        <StarRatings
                            rating={car.rating.avg || 0}
                            starRatedColor="#00a550"
                            starDimension="17px"
                            starSpacing="1px"
                        />
                    </span>
                    <div className="bar-line"> </div>
                    <p className="trips">{car.totalTrips} chuyến </p>
                </div>
                <h2>{car.name}</h2>
            </div>
        </Link>
    </div>
}

class dealCars extends React.Component {
    componentDidMount() {
        this.swiper = new window.Swiper(this.refs.swiperDeal, {
            slidesPerView: 4,
            spaceBetween: 24,
            threshold: 15,
            speed: 600,
            pagination: {
                el: '.pagi-feature-car',
            },
            navigation: {
                nextEl: '.next-deal-car',
                prevEl: '.prev-deal-car',
            },
            loop: true,
            slidesOffsetBefore: 0,
            slidesOffsetAfter: 0,
            preventClicks: false,
            preventClicksPropagation: false,
            breakpoints: {
                992: {
                    slidesPerView: 3,
                    spaceBetween: 30
                },
                768: {
                    slidesPerView: 3,
                    spaceBetween: 30
                },
                480: {
                    slidesPerView: 1,
                    spaceBetween: 15
                }
            },
        });
    }

    render() {
        const cars = getRandom(this.props.cars, 20);
        return <div className="car-area__sect">
            <div className="m-container">
                <h3 className="title-car">Xe giảm giá tại {this.props.city}</h3>
                <div className="swiper-button-next next-deal-car"> <i className="i-arr"></i></div>
                <div className="swiper-button-prev prev-deal-car"> <i className="i-arr"></i></div>
                <div ref="swiperDeal" className="swiper-container swiper-perfect-box">
                    <div className="swiper-wrapper box-car__wrap">
                        {cars.map((car, i) => {
                            return <CarItem key={i} car={car} city={this.props.city} />
                        })}
                    </div>
                    <div className="swiper-pagination pagi-news pagi-feature-car"></div>
                </div>
            </div>
        </div>
    }
}

export default dealCars;