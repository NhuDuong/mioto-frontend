import React from "react"

export default class CheckBoxList extends React.Component {
	
	render() {

        return <div>
            {this.props.options && this.props.options.map(option =>
                <div key={option.id} className="squaredFour have-label">
                    <div className="squaredFour have-label">
                        <input id={`${this.props.id}-${option.id}`} type="checkbox" checked={option.checked} onChange={this.props.onOptionChange.bind(this)} value={option.id} />
                        <label htmlFor={`${this.props.id}-${option.id}`}> <img style={{ width: "16px", height: "16px" }} className="img-ico" src={option.logo} alt="Mioto - Thuê xe tự lái" /> {option.name}</label>
                    </div>
                </div>)}
        </div>
    }
}
