import React from "react"

import { formatPrice, formatTitleInUrl } from "../common/common"

import car_photo from "../../static/images/upload/car_1.png"

function CarItemMap(props) {
    return <div className="item-car">
        <a href={`/car/${formatTitleInUrl(props.car.name)}/${props.car.id}`}>
            <span className="img-car" style={{ backgroundImage: `url(${props.car.photos ? props.car.photos[0].thumbUrl : car_photo})` }}>
                <span className="label-pos">
                    {props.car.instant > 0 && <span className="rent"><i className="ic ic-sm-thunderbolt-wh" /> Đặt xe nhanh</span>}
                    {props.car.deliveryEnable === 1 && props.car.deliveryPrice === 0 && <span className="free">Miễn phí giao nhận xe</span>}
                </span>
            </span>
        </a>
        <div className="desc-car">
            <h2>{props.car.name}</h2>
            <p>
                <span><i className="ic ic-clock"></i> {props.car.totalTrips} chuyến</span>
                <span>{props.car.priceOrigin !== props.car.price && <span className="real">{formatPrice(props.car.priceOrigin)}</span>}
                    <i className="ic ic-tag"></i> {formatPrice(props.car.price)}
                </span>
            </p>
        </div>
    </div>
}

export default CarItemMap;