import React from "react"

import { Link } from "react-router-dom"

import vtc from "../../static/images/vtc.png"
import mastercard from "../../static/images/mastercard-logo.png"
import visa from "../../static/images/visa.png"
import bocongthuong from "../../static/images/bo-cong-thuong.png"
import pjico from "../../static/images/pjico.png"
import liberty from "../../static/images/liberty-logo.png"
import paydoo from "../../static/images/payoo-logo.png"

function Footer() {
    return <section className="footer">
        <div className="footer-container">
            <div className="t-footer">
                <div className="col-1">
                    <a className="logo-footer" href="/">Mioto</a>
                    <div className="space l" />
                    <a className="func-social" href="https://www.facebook.com/mioto.vn/"><i className="ic ic-facebook"></i></a>
                    <a className="func-social" href="https://www.instagram.com/miotovn/"><i className="ic ic-instagram"></i></a>
                    <a className="func-social" href="https://twitter.com/miotovietnam"><i className="ic ic-twitter"></i></a>
                    <a className="func-social" href="https://plus.google.com/u/0/112340450598977299795"><i className="ic ic-google"></i></a>
                    <a className="func-social" href="https://www.linkedin.com/company/mioto/"><i className="ic ic-linkedin"></i></a>
                    <a className="func-social" href="https://www.youtube.com/channel/UCUbuFZt8RcIZUX4qC_BaV4Q"><i className="ic ic-youtube"></i></a>
                    <a className="func-social" href="https://www.pinterest.com/miotovietnam/"><i className="ic ic-pinterest"></i></a>
                </div>
                <div className="col-2">
                    <div className="f-part">
                        <h5>Chính sách</h5>
                        <ul>
                            <li>
                                <Link to="/aboutus">Giới thiệu về Mioto</Link>
                            </li>
                            <li>
                                <Link to="/privacy">Chính sách và quy định</Link>
                            </li>
                            <li>
                                <Link to="/regu">Quy chế hoạt động</Link>
                            </li>
                            <li>
                                <Link to="/personalinfo">Bảo mật thông tin</Link>
                            </li>
                            <li>
                                <Link to="/resolveconflic">Giải quyết tranh chấp</Link>
                            </li>
                        </ul>
                    </div>
                    <div className="f-part">
                        <h5>Tìm hiểu thêm</h5>
                        <ul>
                            <li>
                                <Link to="/howitwork">Hướng dẫn chung</Link>
                                
                            </li>
                            <li>
                                <Link to="/bookinghowto">Hướng dẫn đặt xe</Link>
                            </li>
                            <li>
                                <Link to="/ownerguide">Hướng dẫn dành cho chủ xe</Link>
                            </li>
                            <li>
                                <Link to="/paymenthowto">Hướng dẫn thanh toán</Link>
                            </li>
                            {/* <li>
                                <Link to="/ownerbenef">Trở thành chủ xe</Link>
                            </li> */}
                            <li>
                                <Link to="/owner/register">Đăng ký chủ xe Mioto</Link>
                            </li>
                            <li>
                                <Link to="/faqs">Hỏi và trả lời</Link>
                            </li>
                            <li>
                                <Link to="/blogs">Mioto blog</Link>
                            </li>
                        </ul>
                    </div>
                    <div className="clear"></div>
                </div>
            </div>
            <div className="module-payment">
                <div className="payment-wrapper">
                    <div className="left">
                        <h4 className="title">CÁCH THỨC THANH TOÁN</h4>
                        <div className="logo-payment">
                            <div className="payment-wrap">
                                <div className="fix-img"><img src={vtc} alt="Mioto - Thuê xe tự lái" /></div>
                            </div>
                            <div className="payment-wrap">
                                <div className="fix-img"><img src={paydoo} alt="Mioto - Thuê xe tự lái"/></div>
                            </div>
                            <div className="payment-wrap">
                                <div className="fix-img"><img src={mastercard} alt="Mioto - Thuê xe tự lái" /></div>
                            </div>
                            <div className="payment-wrap">
                                <div className="fix-img"><img src={visa} alt="Mioto - Thuê xe tự lái" /></div>
                            </div>
                        </div>
                    </div>
                    <div className="center">
                        <h4 className="title">ĐỐI TÁC</h4>
                        <div className="insurance-wrapper">
                            <div className="logo-insurance">
                                <a href="https://www.pjico.com.vn/" target="_blank" rel="noopener noreferrer"><div className="fix-img"> <img src={pjico} alt="Mioto - Thuê xe tự lái" /></div></a>
                            </div>
                            <div className="logo-liberty">
                                <a href="https://www.libertyinsurance.com.vn/Direct/trang-chu/?utm_source=googlebrand" target="_blank" rel="noopener noreferrer"><div className=" fix-img"><img src={liberty} alt="Mioto - Thuê xe tự lái" /></div></a>
                            </div>
                        </div>
                    </div>
                    <div className="right">
                        <h4 className="title">CHỨNG NHẬN
                    <div className="certify-logo">
                                <div className="fix-img">
                                    <a href="http://online.gov.vn/HomePage/WebsiteDisplay.aspx?DocId=41067" target="_blank" rel="noopener noreferrer"><img src={bocongthuong} alt="Mioto - Thuê xe tự lái" /></a>
                                </div>
                            </div>
                        </h4>
                    </div>
                </div>
            </div>
            <div className="cpr">
                <div className="col-1">
                    <span className="site">© Công ty Cổ Phần Mioto Việt Nam</span>
                </div>
                <div className="col-2">
                    <ul className="nav-footer">
                        <li><a>Địa chỉ: 305/4 Lê Văn Sỹ, Phường 1, Quận Tân Bình, Thành phố Hồ Chí Minh.</a></li>
                        <li><a>Mã số thuế: 0314714661</a></li>
                        <li><a>Email: contact@mioto.vn</a></li>
                        <li><a href="tel:19009217">Điện thoại: 1900 9217 (9AM-6PM T2-T7)</a></li>
                        <li><a>Tên TK: CT CP MIOTO VIỆT NAM </a></li>
                        <li><a>Số TK: 0721-0006-52087 </a></li>
                        <li><a>Ngân hàng Vietcombank - CN Kỳ Đồng</a></li>
                    </ul>
                </div>
            </div>
        </div >

    </section >
}

export default Footer;