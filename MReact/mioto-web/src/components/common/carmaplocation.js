import React from "react"

import car_icon from "../../static/images/marker_car.png"

export default class CarMapLocation extends React.Component {
    componentDidMount() {
        if (this.props.location) {
            setTimeout(() => {
                this.initMap(this.refs, this.props.location.lat, this.props.location.lng);
            }, 100)
        }
    }

    componentWillUpdate(props) {
        if (props.address === "" || this.props.location.lat !== props.location.lat || this.props.location.lng !== props.location.lng) {
            this.pMarker.setMap(null);
            if (props.address !== "") {
                const pos = new window.google.maps.LatLng(props.location.lat, props.location.lng);
                this.pMarker = new window.google.maps.Marker({
                    icon: {
                        url: car_icon,
                        scaledSize: new window.google.maps.Size(34, 44)
                    },
                    position: pos,
                    map: this.map
                });

                this.map.setCenter(pos);
            }
        }
    }

    initMap(refs, lat, lng) {
        const map = this.map = new window.google.maps.Map(refs.map, {
            center: {
                lat: lat,
                lng: lng
            },
            zoom: 16,
            mapTypeControl: false,
            scrollwheel: false,
            clickableIcons: false,
            streetViewControl: false,
            zoomControl: true,
            zoomControlOptions: {
                position: window.google.maps.ControlPosition.RIGHT_BOTTOM
            }
        })

        this.pMarker = new window.google.maps.Marker({
            icon: {
                url: car_icon,
                scaledSize: new window.google.maps.Size(34, 44)
            },
            position: new window.google.maps.LatLng(this.props.location.lat, this.props.location.lng),
            map: map
        });
    }

    render() {
        return <div ref="map" className="fix-map" style={{ maxWidth: "100%", height: "200px" }}></div>
    }
}