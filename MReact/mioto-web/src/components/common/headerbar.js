import React from 'react'
import { connect } from "react-redux"
import { Link } from "react-router-dom"

import Login from "../login/login"
import ChangePassword from "../account/changepw"
import Notifications from "../account/notifications"
import { commonErr } from "../common/errors"
import { getLoggedProfile, logout } from "../../actions/sessionAct"
import { LoadingInline } from "../common/loading"
import {getUserNotify, seenUserNotify, seenSystemNotify} from "../../model/profile"

import avatar_default from "../../static/images/avatar_default.png"


class GuestMenu extends React.Component {
		constructor(props) {
				super(props);

				this.state = {
					showModal: false,
				}

				this.closeLoginForm = this.closeLoginForm.bind(this);
				this.openLoginForm = this.openLoginForm.bind(this);
		}

		openLoginForm() {
				this.setState({
						showModal: true
				})
		}

		closeLoginForm() {
				this.setState({
						showModal: false
				})
		}

		render() {
				return <div className="menu-container">
						<ul>
								<li className="has-magr">
										<a href="/howitwork"><i className="ic ic-info-wh" /> Hướng dẫn</a>
								</li>
								<li>
										<a onClick={this.openLoginForm}>Đăng nhập</a>
								</li>
								<li>
										<a className="btn btn-transparent btn--m" title="" href="/signup">Đăng kí</a>
								</li>
						</ul>
						<Login show={this.state.showModal} showModal={this.openLoginForm.bind(this)} hideModal={this.closeLoginForm.bind(this)} getLoggedProfile={this.props.getLoggedProfile} />
				</div>
		}
}

class LoggedMenu extends React.Component {
		constructor(props) {
				super(props);
				this.state = {
						showChangePassword: false,
						hasPromo: false,
						hasNewUp: false
				}

				this.setWrapperRef = this.setWrapperRef.bind(this);
				this.handleClickOutside = this.handleClickOutside.bind(this);
				this.onPromoLinkClick = this.onPromoLinkClick.bind(this);
				this.onMyTripsClick = this.onMyTripsClick.bind(this);
		}

	componentDidMount() {
				document.addEventListener('mousedown', this.handleClickOutside);
		}

		componentWillUnmount() {
				document.removeEventListener('mousedown', this.handleClickOutside);
		}

	componentWillReceiveProps(props) {
		const snotis = props.appInit.data.snotifs;
		if (snotis) {
			const snoti = snotis.filter(n => {
					return (n.type === 3 /*PROMO NOTI*/ && n.reddot > 0)
			});
			const unoti = snotis.filter(n => {
				return (n.type === 2 /*PROMO NOTI*/ && n.reddot > 0)
			});
			if (snoti && snoti.length > 0) {
					this.setState({
							hasPromo: true
					})
			}
			if (unoti && unoti.length > 0) {
				this.setState({
						hasNewUp: true
				})
			}
		}
	}

	onMyTripsClick() {
		const snotis = this.props.appInit.data.snotifs;
		if (snotis) {
			const snoti = snotis.filter(n => {
				return (n.type === 2 /*UPCOMMING NOTI*/ && n.reddot > 0)
			});
			if (snoti && snoti.length > 0) {
				seenSystemNotify(snoti[0].id, snoti[0].type);
				window.location.href = "/mytrips";
				return;
			}
		}

		window.location.href = "/mytrips";
	}
	
		onPromoLinkClick() {
				const snotis = this.props.appInit.data.snotifs;
				if (snotis) {
						const snoti = snotis.filter(n => {
								return (n.type === 3 /*UPCOMMING NOTI*/ && n.reddot > 0)
						});
						if (snoti && snoti.length > 0) {
								seenSystemNotify(snoti.id, snoti.type).then(resp => {
										window.location.href = "/promo";
										return;
								})
						}
				}
				window.location.href = "/promo";
		}

		showChangePassword() {
				this.setState({
					showChangePassword: true
				});
		}

		hideChangePassword() {
				this.setState({
					showChangePassword: false
				});
		}

		/**
		* Set the wrapper ref
		*/
		setWrapperRef(node) {
				this.wrapperRef = node;
		}

		/**
		 * Alert if clicked on outside of element
		 */
		handleClickOutside(event) {
				if (this.wrapperRef && !this.wrapperRef.contains(event.target)) {
						this.props.closeDropdown();
				}
		}

		render() {
				return <div className="menu-container">
						<ul>
								<li className="has-magr">
										<a href="/howitwork"><i className="ic ic-info-wh"></i> Hướng dẫn</a>
								</li>
								<li ref={this.setWrapperRef} className={`dropdown ${this.props.isShowDropdown ? "open" : ""}`}>
										<div onClick={this.props.toggleDropdown} id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
												<div className="avatar avatar--s">
														<div className="avatar-img" style={{ "backgroundImage": `url(${(this.props.profile.avatar && this.props.profile.avatar.thumbUrl) ? this.props.profile.avatar.thumbUrl : avatar_default})` }}></div>
												</div>
												<div className="snippet">
														<div className="item-title"><span>{this.props.profile.name}</span></div>
												</div>
										</div>
										<div className="dropdown-menu" aria-labelledby="dropdownMenuButton" style={{ minWidth: "10vw" }}>
												<ul>
														<li><a href="/account">Tài khoản</a></li>
														<li><a href="/myfavs">Xe yêu thích</a></li>
														<li><a href="/mycars">Xe của tôi </a></li>
														<li><a onClick={this.onMyTripsClick}>
															<span className={this.state.hasNewUp ? "has-dot-red" : ""}>Chuyến của tôi</span></a></li>
														<li><a href="/mycard">Thẻ của tôi </a></li>
														<li>
																<a onClick={this.onPromoLinkClick}>
																		<span className={this.state.hasPromo ? "has-dot-red" : ""}>Khuyến mãi</span>
																</a>
														</li>
														<li><a href="/sharedcode">Giới thiệu bạn bè</a></li>
														<li><a onClick={this.showChangePassword.bind(this)}>Đổi mật khẩu</a></li>
														<li><a onClick={this.props.logout}>Đăng xuất</a></li>
												</ul>
										</div>
								</li>
								<ChangePassword errMsg="" show={this.state.showChangePassword} hideModal={this.hideChangePassword.bind(this)} />
						</ul>
				</div>
		}
}

function mapAppInitState(state) {
		return {
				appInit: state.appInit
		}
}

LoggedMenu = connect(mapAppInitState)(LoggedMenu);

class HeaderBar extends React.Component {
		constructor(props) {
				super(props);
				this.state = {
						isShowDropdown: false,
						isShowNotify: false,
						err: commonErr.INNIT,
						notis: null
				}

				this.getLoggedProfile = this.getLoggedProfile.bind(this);
				this.logout = this.logout.bind(this);
				this.toggleNotify = this.toggleNotify.bind(this);
		}

		getLoggedProfile() {
				this.props.dispatch(getLoggedProfile());
		}

		logout() {
				this.props.dispatch(logout());
		}

		toggleDropdown() {
				this.setState({
						isShowDropdown: !this.state.isShowDropdown
				});
		}

		toggleNotify() {
				this.setState({
						isShowNotify: !this.state.isShowNotify
				});
		}

		closeDropdown() {
				this.setState({
						isShowDropdown: false
				});
		}

		closeNotify() {
				this.setState({
						isShowNotify: false
				});
		}

		render() {
				var menuCont;
				if (this.props.session.profile.err === commonErr.LOADING) {
						menuCont = <div className="menu-container"><LoadingInline /></div>
				} else if (this.props.session.profile && this.props.session.profile.info && this.props.session.profile.info.uid) {
						menuCont = <LoggedMenu isShowDropdown={this.state.isShowDropdown} toggleDropdown={this.toggleDropdown.bind(this)} closeDropdown={this.closeDropdown.bind(this)} logout={this.logout} profile={this.props.session.profile.info} />;
				} else {
						menuCont = <GuestMenu getLoggedProfile={this.getLoggedProfile} />
				}

				return <section className="header">
								<div className="header-container">
									
									<div className="header-icon">
										<div className="js-toggle-right-slidebar js-menu-mobile">
												<div className="trigger-menu">
														<span className="three-bars-icon" onClick={this.props.openSidebar}></span>
												</div>
										</div>
										<Notifications isLogged={this.props.session.profile && this.props.session.profile.info && this.props.session.profile.info.uid} isShowNotify={this.state.isShowNotify} toggleNotify={this.toggleNotify} closeNotify={this.closeNotify.bind(this)} />
										<Link className="item-app show-on-med-and-down" to="/howitwork"><i className="ic ic-info-wh" /></Link>
								</div>
								<div className="logo-container">
									<a href="/">Mioto</a>
								</div>
								<div className="info-container">
										<span>
												<i className="ic ic-phone" />
												<a style={{ color: "white" }} href="tel:19009217">1900 9217</a>
										</span>
										<span><i className="ic ic-email" /> contact@mioto.vn</span>
										<span><a style={{ color: "white" }} href="https://www.messenger.com/t/mioto.vn" target="_blank"><i className="ic ic-messenger"></i> Facebook</a></span>
								</div>
								{menuCont}
						</div>
		
				</section>
		}
}

function mapState(state) {
		return {
				session: state.session
		}
}

HeaderBar = connect(mapAppInitState)(HeaderBar);
HeaderBar = connect(mapState)(HeaderBar);
export default HeaderBar;