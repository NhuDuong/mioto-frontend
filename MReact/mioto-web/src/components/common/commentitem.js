import React from "react"
import moment from "moment"
import StarRatings from "react-star-ratings"

import avatar_default from "../../static/images/avatar_default.png"

export default class CommentItem extends React.Component {
    render() {
        const profile = this.props.profile;
        const review = this.props.review;
        return (
            <div className="item-comment">
                <div className="avatar avatar--s" title="title name">
                    <div className="avatar-img" style={{ backgroundImage: `url(${profile.avatar ? profile.avatar.thumbUrl : avatar_default})` }}></div>
                </div>
                <div className="desc">
                    <h4><a href={`/profile/${profile.uid}`}>{profile.name}</a></h4>
                    <div className="cmt">
                        <p>
                            <span className="timer">{moment(review.timeCreated).fromNow()}</span>
                            <StarRatings
                                rating={review.rating}
                                starRatedColor="#00a550"
                                starDimension="17px"
                                starSpacing="1px"
                            />
                        </p>
                        <p>{review.comment}</p>
                    </div>
                </div>
            </div>
        );
    }
}