import React from "react"

export default class CallBtn extends React.Component {
    render() {
        return <a className="func-filter call" href="tel:19009217"><i className="ic ic-filter ic-call"></i></a>
    }
}