import React from "react"
import ImageUploader from 'react-images-upload'

export default class ImageUpload extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            pictures: []
        };
    }

    onDrop(picture) {
        this.setState({
            pictures: this.state.pictures.concat(picture),
        });

        this.props.onChange(picture);
    }

    render() {
        return <ImageUploader
            withIcon={false}
            buttonText='Chọn hình'
            onChange={this.onDrop.bind(this)}
            withLabel={false}
            withPreview={this.props.withPreview}
            accept={"accept=image/*"}
            buttonClassName={"btn btn-primary btn--m"}
            fileSizeError={" vượt quá kích thươc tối đa 5Mb."}
            fileTypeError={" không đúng định dạng cho phép."}
            imgExtension={['.jpg', '.png', '.JPG', '.PNG', '.jpeg', '.JPEG', ".gif", ".GIF"]}
            maxFileSize={5242880}
        />
    }
}