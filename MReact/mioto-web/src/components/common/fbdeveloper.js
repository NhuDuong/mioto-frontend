export const fbappInfo = {
    appId: "1760356927595709",
    csrf: "313ce2ca118f7dab94b37cc33c02a7be",
    version: "v1.1",
    language: "vi_VN",
    getAccessTokBaseDomain: "https://graph.accountkit.com/v1.2/access_token",
    getLoggedInfoBaseDomain: "https://graph.accountkit.com/v1.2/me"
}