import React from "react"

export function Stars(props) {
    if (!props.avrg || props.avrg <= 0) {
        return <span className="star"><i /><i /><i /><i /><i /></span>
    } else if (props.avrg <= 0.5) {
        return <span className="star"><i className="half" /><i /><i /><i /><i /></span>
    } else if (props.avrg <= 1) {
        return <span className="star"><i className="full" /><i /><i /><i /><i /></span>
    } else if (props.avrg <= 1.5) {
        return <span className="star"><i className="full" /><i className="half" /><i /><i /><i /></span>
    } else if (props.avrg <= 2) {
        return <span className="star"><i className="full" /><i className="full" /><i /><i /><i /></span>
    } else if (props.avrg <= 2.5) {
        return <span className="star"><i className="full" /><i className="full" /><i className="half" /><i /><i /></span>
    } else if (props.avrg <= 3) {
        return <span className="star"><i className="full" /><i className="full" /><i className="full" /><i /><i /></span>
    } else if (props.avrg <= 3.5) {
        return <span className="star"><i className="full" /><i className="full" /><i className="full" /><i className="half" /><i /></span>
    } else if (props.avrg <= 4) {
        return <span className="star"><i className="full" /><i className="full" /><i className="full" /><i className="full" /><i /></span>
    } else if (props.avrg <= 4.5) {
        return <span className="star"><i className="full" /><i className="full" /><i className="full" /><i className="full" /><i className="half" /></span>
    } else if (props.avrg <= 5) {
        return <span className="star"><i className="full" /><i className="full" /><i className="full" /><i className="full" /><i className="full" /></span>
    }
}