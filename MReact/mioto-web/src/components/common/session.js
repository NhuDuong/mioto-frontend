import React from "react"
import { connect } from "react-redux"
import { Redirect } from "react-router-dom"

import { commonErr } from "../common/errors"

class Session extends React.Component {
    render() {
        if (this.props.session.profile.err !== commonErr.INNIT
            && this.props.session.profile.err !== commonErr.LOADING
            && (!this.props.session.profile
                || !this.props.session.profile.info
                || !this.props.session.profile.info.uid)) {
            return <Redirect to="/" />
        } else {
            return null
        }
    }
}

function mapState(state) {
    return {
        session: state.session
    }
}

Session = connect(mapState)(Session)

export default Session;