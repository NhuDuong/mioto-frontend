import React from "react"

export default class CheckBoxSelector extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            show: false
        }
    }

    toggleBox() {
        this.setState({
            show: !this.state.show
        })
    }

    render() {
        return (
            <div className="wrap-input has-dropdown">
                <span onClick={this.toggleBox.bind(this)} className="value placeholder"> {this.props.name}</span>
                <ul className={`dropdown-m has-checkbox ${this.state.show ? "" : "hidden"}`}>
                    {this.props.options && this.props.options.map(option =>
                        <li key={option.id}>
                            <div className="squaredFour have-label">
                                <input id={`${this.props.id}-${option.id}`} type="checkbox" onChange={this.props.onOptionChange.bind(this)} value={option.id} />
                                <label htmlFor={`${this.props.id}-${option.id}`}> <img className="img-ico" src={option.logo} alt="Mioto - Thuê xe tự lái"/>{option.name}</label>
                            </div>
                        </li>
                    )}
                </ul>
            </div>
        );
    }
}
