import React from "react"
import { Map, InfoWindow, Marker, GoogleApiWrapper } from 'google-maps-react';
import CarItem from "./caritem";

class MapContainer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            activeMarker: {},
            selectedPlace: {},
            showingInfoWindow: true,
        }

        // binding this to event-handler functions
        this.onMarkerClick = this.onMarkerClick.bind(this);
        this.onMapClicked = this.onMapClicked.bind(this);
    }

    onMarkerClick(props, marker, e) {
        this.setState({
            activeMarker: marker,
            selectedPlace: props,
            showingInfoWindow: true
        });
    }

    onMapClicked(props) {
        if (this.state.showingInfoWindow) {
            this.setState({
                showingInfoWindow: false,
                activeMarker: {},
                selectedPlace: {}
            })
        }
    }

    render() {
        const cars = this.props.cars;
        return (
            <Map
                initialCenter={{
                    lat: 10.8,
                    lng: 106.7
                }}
                google={this.props.google}
                onClick={this.onMapClicked}>

                {cars.map(car => <Marker onClick={this.onMarkerClick}
                    name={<CarItem car={car} />}
                    position={{ lat: car.location.lat, lng: car.location.lon }} />)}

                <InfoWindow
                    marker={this.state.activeMarker}
                    visible={this.state.showingInfoWindow}>
                    <div>
                        <h1>{this.state.selectedPlace.name}</h1>
                    </div>
                </InfoWindow>
            </Map>
        )
    }
}


export default GoogleApiWrapper({
    apiKey: ("AIzaSyB34E-5CM0lBY4GgbIi5w3osRwK8BIyNjs")
})(MapContainer)