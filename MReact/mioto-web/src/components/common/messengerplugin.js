import React from "react"

export default class MessengerPlugin extends React.Component {
    render() {
        return <div className="fb-customerchat"
            page_id="122050578497254"
            theme_color="#00a550"
            logged_in_greeting="Chat ngay với Mioto để được hỗ trợ."
            logged_out_greeting="Mioto hẹn gặp lại quý khách hàng.">
        </div>
    }
}