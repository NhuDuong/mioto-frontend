import React from "react"
import moment from "moment"
import StarRatings from "react-star-ratings"

import avatar_default from "../../static/images/avatar_default.png"

export default class CommentItemV2 extends React.Component {
    render() {
        const profile = this.props.profile;
        const review = this.props.review;
        return <div className="list-comments">
            <div className="left">
                <div className="fix-avatar">
                    {!review.avt && profile && <a href={`/profile/${profile.uid}`}>
                        <img src={`${profile.avatar && profile.avatar.thumbUrl ? profile.avatar.thumbUrl : avatar_default}`} />
                    </a>}
                    {review.avt && review.avt !== "" && <img src={review.avt} />}
                </div>
            </div>
            <div className="right">
                <div className="group">
                    {!review.name && profile && <h4 className="name"><a href={`/profile/${profile.uid}`}>{profile.name}</a></h4>}
                    {review.name && review.name !== "" && <h4 className="name">{review.name}</h4>}
                </div>
                <div className="cmt-box">
                    <div className="group">
                        <span className="star">
                            <StarRatings
                                rating={review.rating}
                                starRatedColor="#00a550"
                                starDimension="17px"
                                starSpacing="1px"
                            />
                        </span>
                        <p className="date">{moment(review.timeCreated).fromNow()}</p>
                    </div>
                    <p className="desc">{review.comment}</p>
                </div>
            </div>
        </div>
    }
}