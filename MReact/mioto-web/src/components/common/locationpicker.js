import React from "react"
import { Modal } from "react-bootstrap"
import Geocode from "react-geocode"
import PlacesAutocomplete, { geocodeByAddress, getLatLng } from 'react-places-autocomplete'
import { LoadingOverlay } from "./loading"
import { commonErr } from "../common/errors"
import { MessageLine } from "./messagebox"

class MapLocation extends React.Component {
    componentDidMount() {
        setTimeout(() => {
            this.initMap(this.refs, this.props.location.lat, this.props.location.lng);
        }, 100)
    }

    componentWillReceiveProps(props) {
        if (this.props.location.lat !== props.location.lat
            || this.props.location.lng !== props.location.lng) {
            const position = new window.google.maps.LatLng(props.location.lat, props.location.lng);
            this.upDateMarker(this.map, position);
            if (this.props.address !== props.address) {
                const position = new window.google.maps.LatLng(props.location.lat, props.location.lng);
                this.map.setCenter(position);
            }
        }
    }

    upDateMarker(map, position) {
        this.pMarker.setMap(null);
        this.pMarker = new window.google.maps.Marker({
            position: position,
            map: map
        });
    }

    initMap(refs, lat, lng) {
        const map = this.map = new window.google.maps.Map(refs.map, {
            center: {
                lat: lat,
                lng: lng
            },
            zoom: 17,
            mapTypeControl: false,
            scrollwheel: false,
            clickableIcons: false,
            streetViewControl: false,
            zoomControl: true,
            zoomControlOptions: {
                position: window.google.maps.ControlPosition.RIGHT_BOTTOM
            }
        })

        this.pMarker = new window.google.maps.Marker({
            position: new window.google.maps.LatLng(this.props.location.lat, this.props.location.lng),
            map: map
        });

        map.addListener('click', e => {
            this.props.onMarkerUpdate({
                lat: e.latLng.lat(),
                lng: e.latLng.lng()
            });
            // Geocode.fromLatLng(e.latLng.lat(), e.latLng.lng()).then(
            //     response => {
            //         const address = response.results[0].formatted_address;
            //         if (address) {
            //             this.props.onMarkerUpdate(address, {
            //                 lat: e.latLng.lat(),
            //                 lng: e.latLng.lng()
            //             });
            //         }
            //     },
            //     error => {
            //         this.props.onAddressUpdateError(error);
            //     }
            // );
        });

        map.addListener('drag', () => {
            this.pMarker.setPosition(map.getCenter());
        });

        map.addListener('dragend', () => {
            const center = map.getCenter();
            this.props.onMarkerUpdate({
                lat: center.lat(),
                lng: center.lng()
            });
            // Geocode.fromLatLng(center.lat(), center.lng()).then(
            //     response => {
            //         const address = response.results[0].formatted_address;
            //         if (address) {
            //             this.props.onMarkerUpdate(address, {
            //                 lat: center.lat(),
            //                 lng: center.lng()
            //             });
            //         }
            //     },
            //     error => {
            //         this.props.onAddressUpdateError(error);
            //     }
            // );
        });
    }

    render() {
        return <div ref="map" className="fix-map" style={{ maxWidth: "100%", height: "300px" }}></div>
    }
}

export default class LocationPikcer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            err: commonErr.INNIT,
            errMsg: "",
            addressPalceHolder: "Nhập chi tiết địa chỉ của bạn"
        }

        this.onSearchBtnClick = this.onSearchBtnClick.bind(this);
        this.onAddressChange = this.onAddressChange.bind(this);
        this.onAddressRemove = this.onAddressRemove.bind(this);
        this.onMarkerUpdate = this.onMarkerUpdate.bind(this);
        this.onAddressUpdateError = this.onAddressUpdateError.bind(this);
        this.onAddressFocus = this.onAddressFocus.bind(this);
        this.onAddressSelected = this.onAddressSelected.bind(this);
        this.onUpdate = this.onUpdate.bind(this);
        this.onHide = this.onHide.bind(this);
    }

    componentDidMount() {
        this.setState({
            address: this.props.address,
            location: this.props.location
        });
    }

    componentWillReceiveProps(props) {
        this.setState({
            address: props.address,
            location: props.location
        });
    }

    onSearchBtnClick() {
        const address = this.state.address;
        if (!address || address === "") {
            return;
        }

        this.setState({
            err: commonErr.LOADING,
            errMsg: ""
        });

        Geocode.fromAddress(address).then(
            response => {
                const location = response.results[0].geometry.location;
                this.setState({
                    location: location,
                    err: commonErr.SUCCESS,
                    errMsg: ""
                });
            },
            error => {
                this.setState({
                    err: commonErr.FAIL,
                    errMsg: "Không thể tìm thấy vị trí của bạn."
                });
            }
        );
    }

    onAddressChange(address) {
        this.setState({
            address: address
        });
        if (!address || address === "") {
            this.setState({
                location: null
            });
        }
    }

    onMarkerUpdate(location) {
        this.setState({
            location: location
        });
    }

    onAddressUpdateError(error) {
        this.setState({
            address: "",
            addressPalceHolder: "Không tìm thấy địa chỉ"
        });
    }

    onAddressFocus() {
    }

    onAddressRemove() {
        this.setState({
            address: "",
            location: null
        });
    }

    onAddressSelected(address) {
        if (address.err < 0) {
            var errMsg = "";
            if (address.err === -200) {
                errMsg = "Bạn đã không cho phép trình duyệt truy cập vị trí hiện tại. Vui lòng cài đặt cho phép hoặc nhập địa chỉ.";
            } else {
                errMsg = "Không thể tìm thấy vị trí hiện tại của thiết bị. Vui lòng thử lại hoặc nhập địa chỉ.";
            }
            this.setState({
                err: commonErr.FAIL,
                errMsg: errMsg
            });
        } else if (address.err === commonErr.LOADING) {
            this.setState({
                err: commonErr.LOADING,
                errMsg: ""
            });
        } else {
            this.setState({
                err: commonErr.LOADING,
                errMsg: ""
            });
            geocodeByAddress(address.address)
                .then(results => getLatLng(results[0]))
                .then(latLng => {
                    this.setState({
                        address: address.address,
                        location: {
                            lat: latLng.lat,
                            lng: latLng.lng
                        },
                        err: commonErr.SUCCESS,
                        errMsg: ""
                    });
                })
        }
    }

    onUpdate() {
        this.props.onUpdate(this.state.address, this.state.location);
        this.props.onHide();
    }

    onHide() {
        this.setState({
            err: commonErr.INNIT,
            errMsg: ""
        })
        this.props.onHide();
    }

    isValidAddress() {
        return this.state.err >= commonErr.SUCCESS && this.state.location
            && this.state.location.lat && this.state.location.lat !== 0
            && this.state.location.lng && this.state.location.lng !== 0;
    }

    render() {
        return <Modal
            show={this.props.show}
            onHide={this.onHide}
            dialogClassName="modal-sm modal-dialog"
        >
            <Modal.Header closeButton={true} closeLabel={""}>
                <Modal.Title>Tùy chỉnh địa chỉ</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <div className="form-default">
                    <div className="line-form">
                        <div className="wrap-input has-ico-search">
                            <i className="ic ic-remove" onClick={this.onAddressRemove}></i>
                            <PlacesAutocomplete
                                inputProps={{
                                    value: this.state.address,
                                    onChange: this.onAddressChange,
                                    onFocus: this.onAddressFocus,
                                    placeholder: this.state.addressPalceHolder
                                }}
                                onSelect={this.onAddressSelected}
                                isUseCurrentLocation={true}
                                googleLogo={false}
                                options={{ componentRestrictions: { country: ['vn'] } }} />
                        </div>
                    </div>
                    {this.state.err === commonErr.LOADING && <LoadingOverlay />}
                    {this.state.err <= commonErr.FAIL && <MessageLine error={this.state.err} message={this.state.errMsg} />}
                    {this.isValidAddress() === true && <div>
                        <MapLocation address={this.state.address} location={this.state.location} onMarkerUpdate={this.onMarkerUpdate} onAddressUpdateError={this.onAddressUpdateError} />
                    </div>}
                </div>
                <div className="wr-btn" style={{ padding: "40px 10px 20px 10px" }}>
                    <a onClick={this.onHide} style={{ width: "48%", float: "left", margin: "1%" }} className="btn btn-secondary btn--m">Hủy bỏ</a>
                    <a disabled={!this.state.address || !this.isValidAddress()} onClick={this.onUpdate} style={{ width: "48%", float: "left", margin: "1%" }} className="btn btn-primary btn--m" >Áp dụng</a>
                </div>
            </Modal.Body>
        </Modal>
    }
}