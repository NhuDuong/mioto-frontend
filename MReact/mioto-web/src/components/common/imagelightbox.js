import React, { Component } from 'react'
import Lightbox from 'react-image-lightbox'

export default class ImageLightbox extends Component {
    constructor(props) {
        super(props);

        this.state = {
            photoIndex: props.photoIndex || 0,
        };
    }

    render() {
        const {
            photoIndex,
        } = this.state;
        const images = this.props.images;

        return this.props.isOpen && <Lightbox
            ariaHideApp={true}
            mainSrc={images[photoIndex].fullUrl}
            nextSrc={images.length > 1 ? images[(photoIndex + 1) % images.length].fullUrl : false}
            prevSrc={images.length > 1 ? images[(photoIndex + images.length - 1) % images.length].fullUrl : false}
            imageCaption={images.length > 1 ? `Hình ${photoIndex + 1} / ${images.length}` : false}

            onMovePrevRequest={() => this.setState({
                photoIndex: (photoIndex + images.length - 1) % images.length,
            })}
            onMoveNextRequest={() => this.setState({
                photoIndex: (photoIndex + 1) % images.length,
            })}

            onCloseRequest={this.props.hideLightBox}
        />
    }
}