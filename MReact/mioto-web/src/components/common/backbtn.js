import React from 'react'
import { Link } from "react-router-dom"
import { withLastLocation } from 'react-router-last-location'

const BackBtn = ({ lastLocation }) => (
    <Link className="func-page-back" to={lastLocation ? lastLocation : { pathname: "/" }}><i></i></Link>
);

export default withLastLocation(BackBtn);