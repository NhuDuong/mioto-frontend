import React from "react";

import Header from "../common/header";
import Footer from "../common/footer";
import { MessagePage } from "../common/messagebox";

class NotFound extends React.Component {
    componentDidMount() {
        window.scrollTo(0, 0);
    }

    render() {
        return (
            <div className="mioto-layout">
                <Header />
                <section className="body">
                    <MessagePage message={"Page not found"} />
                </section>
                <Footer />
            </div>
        );
    }
}

export default NotFound;