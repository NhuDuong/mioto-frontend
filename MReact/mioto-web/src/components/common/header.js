import React from 'react'
import { slide as Menu } from 'react-burger-menu'
import { connect } from "react-redux"

import HeaderBar from "./headerbar"
import LoginSidebar from "../login/loginsidebar"
import { initApp } from "../../actions/appInitAct"
import { getLoggedProfile } from "../../actions/sessionAct"
import { commonErr } from './errors'



class Header extends React.Component {
    constructor() {
        super();
        this.state = {
            isOpenSidebar: false
        }
    }

    componentDidMount() {
        if (this.props.appInit.err === commonErr.INNIT) {
            this.props.dispatch(initApp());
        }
        if (this.props.session.profile.err === commonErr.INNIT) {
            this.props.dispatch(getLoggedProfile());
        }
    }

    openSidebar() {
        this.setState({
            isOpenSidebar: true
        });
    }

    onSidebarStateChange(state) {
        this.setState({
            isOpenSidebar: state.isOpen
        });
    }

    render() {
        return <div>
            <Menu width={'100vw'} isOpen={this.state.isOpenSidebar} onStateChange={this.onSidebarStateChange.bind(this)}>
                <LoginSidebar />
            </Menu>
            <HeaderBar isReloadable={this.props.isReloadable} openSidebar={this.openSidebar.bind(this)} />
        </div>
    }
}

function mapInitState(state) {
    return {
        appInit: state.appInit
    }
}

function mapSessionState(state) {
    return {
        session: state.session
    }
}

Header = connect(mapInitState)(Header);
Header = connect(mapSessionState)(Header);

export default Header;