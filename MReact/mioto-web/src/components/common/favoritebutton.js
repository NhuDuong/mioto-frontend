import React from "react"

import { commonErr } from "../common/errors"
import { LoadingInlineSmall } from "../common/loading"
import { addFavoriteCar, removeFavoriteCar } from "../../model/car"

class FavoriteButton extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            buttonState: 0, //0: unfavorite 1: adding favorite 2: added favorite
        }
    }

    componentDidMount() {
        if (this.props.car.fav === 1) {
            this.setState({
                buttonState: 2
            });
        }
    }

    addToFavorites() {
        if (!this.props.isLogged) {
            this.props.showLoginForm();
        } else {
            this.setState({
                buttonState: 1
            });
            addFavoriteCar(this.props.car.id).then(resp => {
                if (resp.data.error >= commonErr.SUCCESS) {
                    this.setState({
                        buttonState: 2
                    });
                }
            });
        }
    }

    removeFromFavorites() {
        if (!this.props.isLogged) {
            this.props.showLoginForm();
        } else {
            this.setState({
                buttonState: 1
            });
            removeFavoriteCar(this.props.car.id).then(resp => {
                if (resp.data.error >= commonErr.SUCCESS) {
                    this.setState({
                        buttonState: 0
                    });
                }
            });
        }
    }

    render() {
        var content;
        var state = this.state.buttonState;
        if (state === 2) {
            content = <a className="btn btn-primary btn--m" onClick={this.removeFromFavorites.bind(this)}>Xoá khỏi yêu thích</a>
        } else if (state === 1) {
            content = <a className="btn btn-primary btn--m"><LoadingInlineSmall /></a>
        } else {
            content = <a className="btn btn-primary btn--m" onClick={this.addToFavorites.bind(this)}>Thêm vào yêu thích</a>
        }

        return content;
    }
}

export default FavoriteButton;