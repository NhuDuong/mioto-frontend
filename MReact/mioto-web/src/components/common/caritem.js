import React from "react"
import StarRatings from "react-star-ratings"

import { formatPrice, formatTitleInUrl } from "../common/common"

import car_photo from "../../static/images/upload/car_1.png"

function CarItem(props) {
    return <a href={`/car/${formatTitleInUrl(props.car.name)}/${props.car.id}`} onClick={(e) => { e.preventDefault(); return false; }}>
        <div className="item-car" style={props.style} onClick={() => props.tooglePopup(props.car)}>
            <span className="img-car" style={{ backgroundImage: `url(${props.car.photos ? props.car.photos[0].thumbUrl : car_photo})` }}>
                <span className="label-pos">
                    {props.car.totalDiscountPercent > 0 && <span className="discount">Giảm {props.car.totalDiscountPercent}%</span>}
                    {props.car.instant > 0 && <span className="rent"><i className="ic ic-sm-thunderbolt-wh" /> Đặt xe nhanh</span>}
                    {props.car.pp === 1 && <span className="free"><i className="ic ic-passport" />Chấp nhận passport</span>}
                </span>
                {/* {props.car.photosVerified === 1 && <div className="status-verify">
                    <i className="ic ic-verify-stroke" /> Ảnh đã xác thực
            </div>} */}
            </span>
            <div className="desc-car">
                <StarRatings
                    rating={props.car.rating.avg}
                    starRatedColor="#00a550"
                    starDimension="17px"
                    starSpacing="1px"
                />
                <span className="km">
                    {props.car.priceOrigin !== props.car.price && <span className="real">{formatPrice(props.car.priceOrigin)}</span>}
                    <strong>{formatPrice(props.car.price)}</strong>
                </span>
								<h2>{props.car.name}</h2>
                <p>
                    <span><i className="ic ic-clock" /> {props.car.totalTrips} chuyến</span>
                    {(props.car.distance && props.car.distance !== 0) && <span><i className="ic ic-map" /> {props.car.distance}</span>}
								</p>
								
								<div className={`group-label marginTop-s ${props.car.deliveryEnable !== 1 ? "item-min-height" : ""}`}>
									{props.car.deliveryEnable === 1 && <span>Giao xe tận nơi</span>}
									{props.car.deliveryEnable === 1 && props.car.deliveryPrice === 1 && <span>Miễn phí giao xe</span>}
								</div>
            </div>
        </div>
    </a>
}

export default CarItem;