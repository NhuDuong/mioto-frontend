import React from "react"
import { connect } from "react-redux"
import cookie from 'react-cookies'

import Header from "../common/header"
import Footer from "../common/footer"
import Login3rd from "../login/login3rd"
import { commonErr } from "../common/errors"
import { quickSignup } from "../../model/profile"
import { MessageBox } from "../common/messagebox"
import { getLoggedProfile } from "../../actions/sessionAct"
import { LoadingOverlay } from "../common/loading"


class SignUp extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            ip_name: "",
            ip_phone: "",
            ip_password: "",
            ip_password_confirm: "",
            err: commonErr.INNIT,
            errMsg: ""
        }

        this.handleInputChange = this.handleInputChange.bind(this);
        this.getLoggedProfile = this.getLoggedProfile.bind(this);
    }

    handleInputChange = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        });
    }

    hideMessageBox() {
        this.setState({
            errMsg: ""
        });
    }

    getLoggedProfile() {
        this.props.dispatch(getLoggedProfile());
    }

    onSignUpBtnClick = (event) => {
        const name = this.state.ip_name;
        const phone = this.state.ip_phone;
        const password = this.state.ip_password;
        const passwordConfirm = this.state.ip_password_confirm;

        const self = this;
        if (name !== "" && phone !== "" && password !== "") {
            if (password !== passwordConfirm) {
                self.setState({
                    err: commonErr.FAIL,
                    errMsg: "Mật khẩu không khớp"
                });
            } else {
                this.setState({ err: commonErr.LOADING });
                quickSignup(phone, name, password).then(function (resp) {
                    self.setState({ err: resp.data.error, errMsg: resp.data.errorMessage });
                    if (resp.data.error >= commonErr.SUCCESS) {
                        self.getLoggedProfile();

                        //GA
                        const utmSrc = cookie.load("_utm_src") || "web_directly";
                        const utmExt = cookie.load("_utm_ext") || "no_label";
                        window.ga('send', 'event', utmSrc, "ACCOUNT_REGISTER", utmExt);
                    }
                });
            }
        } else {
            self.setState({
                err: commonErr.FAIL,
                errMsg: "Thông tin không hợp lệ"
            });
        }
    }

    readPolicyChange(event) {
        this.setState({
            readPolicy: event.target.checked
        });
    }

    showAdvance() {
        this.setState({
            showAdvance: true
        });
    }

    hideAdvance() {
        this.setState({
            showAdvance: false
        });
    }

    render() {
        var content;
        if (this.state.err === commonErr.SUCCESS) {
            content = <div className="form-default form-s min-height">
                <h2 className="title-form">Đăng kí tài khoản</h2>
                <p className="textAlign-center"> <i className="ic ic-verify" ></i> Bạn đã đăng kí tài khoản thành công. Hệ thống sẽ tự động đăng nhập. Hãy cập nhật <a href="/account">tài khoản</a> ngay hoặc quay lại <a href="/">trang chủ</a> để tiếp tục sử dụng dịch vụ của Mioto.</p>
            </div>
        } else {
            content = <div className="form-default form-s">
                <h2 className="title-form">Đăng kí tài khoản</h2>
                <div className="line-form">
                    <div className="wrap-input has-ico"><i className="ic ic-phone-fill"></i>
                        <input type="text" name="ip_phone" placeholder="Điện thoại hoặc email" onChange={this.handleInputChange} />
                    </div>
                </div>
                <div className="line-form">
                    <div className="wrap-input has-ico"><i className="ic ic-user-fill"></i>
                        <input type="text" name="ip_name" placeholder="Tên hiển thị" onChange={this.handleInputChange} />
                    </div>
                </div>
                <div className="line-form has-2input">
                    <div className="wrap-input">
                        <input type="password" name="ip_password" placeholder="Mật khẩu" onChange={this.handleInputChange} />
                    </div>
                    <div className="wrap-input">
                        <input type="password" name="ip_password_confirm" placeholder="Xác nhận mật khẩu" onChange={this.handleInputChange} />
                    </div>
                </div>

                {!this.state.showAdvance && <a style={{ float: "right" }} className="func-more block" onClick={this.showAdvance.bind(this)}><i className="i-arrow-down"></i></a>}
                {this.state.showAdvance && <a style={{ float: "right" }} className="func-more block" onClick={this.hideAdvance.bind(this)}><i className="i-arrow-up"></i></a>}
                <div className="space m"></div>
                <div className="space m"></div>
                <div className="clear"></div>
                {this.state.showAdvance && <div>
                    <div className="line-form">
                        <label className="label">Khách hàng cá nhân</label>
                        <div className="wrap-input">
                            <input type="text" placeholder="CMND hoặc Mã số thế" />
                        </div>
                    </div>
                    <div className="line-form">
                        <label className="label">Khách hàng doanh nghiệp</label>
                        <div className="wrap-input">
                            <input type="text" placeholder="Chứng nhận ĐKKD" />
                        </div>
                    </div>
                    <div className="line-form has-2input">
                        <div className="wrap-input">
                            <input type="text" placeholder="Ngày cấp" />
                        </div>
                        <div className="wrap-input">
                            <input type="text" placeholder="Nơi cấp" />
                        </div>
                    </div>
                </div>}
                <div className="squaredFour have-label">
                    <input id="read_policy" type="checkbox" onChange={this.readPolicyChange.bind(this)} />
                    <label className="label" htmlFor="read_policy"><p>Tôi đã đọc và đồng ý với <a href="/privacy" target="_blank">chính sách</a> của Mioto</p></label>
                </div>
                <div className="space m"></div>
                <div className="space m"></div>
                <div className="clear"></div>
                <button disabled={!this.state.readPolicy} className="btn btn-primary btn--m" type="button" name="signUpBtn" onClick={this.onSignUpBtnClick.bind(this)}>Đăng Kí</button>
                <div className="space"></div>
                <span className="line"></span>
                <p className="textAlign-center">Hoặc đăng nhập bằng tài khoản</p>
                <Login3rd />
                {this.state.errMsg && this.state.errMsg !== "" && <MessageBox error={this.state.err} message={this.state.errMsg} show={this.state.errMsg && this.state.errMsg !== ""} hideModal={this.hideMessageBox.bind(this)} />}
            </div>
        }

        return <div className="mioto-layout">
            <Header />
            <section className="body">
                <div className="body-container">
                    {content}
                </div>
                {this.state.err === commonErr.LOADING && <LoadingOverlay />}
            </section>
            <Footer />
        </div>
    }
}

function mapState(state) {
    return {
        session: state.session
    }
}

SignUp = connect(mapState)(SignUp);

export default SignUp;