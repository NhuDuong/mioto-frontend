import React from "react"
import axios from "axios"
import { Modal } from "react-bootstrap"
import AccountKit from "../common/accountkit"

import { fbappInfo } from "../common/fbdeveloper"
import { commonErr } from "../common/errors"
import { verifyPhone } from "../../model/profile"

export default class VerifyPhone extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            err: commonErr.INNIT,
            errMsg: "",
            step: 0
        }
    }

    closeForm() {
        this.props.hideModal();
    }

    onAccountKitPhoneLoginResp(resp) {
        const self = this;

        var verifyPhoneAccessTok;
        var verifyPhoneNumber;

        if (resp.code) {
            axios({
                method: "GET",
                url: `${fbappInfo.getAccessTokBaseDomain}?grant_type=authorization_code&code=${resp.code}&access_token=AA|${fbappInfo.appId}|${resp.state}`
            }).then(function (tokenData) {
                if (tokenData.data) {
                    verifyPhoneAccessTok = tokenData.data.access_token;
                    if (verifyPhoneAccessTok) {
                        axios({
                            method: "GET",
                            url: `${fbappInfo.getLoggedInfoBaseDomain}/?access_token=${verifyPhoneAccessTok}`
                        }).then(function (accountData) {
                            if (accountData.data && accountData.data.phone) {
                                verifyPhoneNumber = accountData.data.phone.number;
                                if (verifyPhoneNumber) {
                                    verifyPhone(verifyPhoneNumber, verifyPhoneAccessTok, 1).then(function (resp) {
                                        self.setState({
                                            err: resp.data.error,
                                            errMsg: resp.data.errorMessage,
                                            step: 1
                                        });
                                    });
                                }
                            }
                        })
                    }
                }
            })
        }
    }

    retry() {
        this.setState({
            err: commonErr.INNIT,
            errMsg: "",
            step: 0
        });
    }

    finish() {
        this.setState({
            err: commonErr.INNIT,
            errMsg: "",
            step: 0
        });
        this.props.hideModal();
        this.props.getProfile();
    }

    render() {
        const profile = this.props.profile;
        var content;
        if (this.state.step === 1) {
            if (this.state.err >= commonErr.SUCCESS) {
                content = <Modal.Body>
                    <p className="textAlign-center"><i className="ic ic-verify"></i> Xác minh số điện thoại thành công.</p>
                    <div className="space m"></div>
                    <div className="textAlign-center"><a className="btn btn-primary btn--m" type="button" onClick={this.finish.bind(this)}> Hoàn tất</a></div>
                </Modal.Body>
            } else {
                content = <Modal.Body>
                    <p style={{ color: "red" }} className="textAlign-center"><i className="ic ic-warning"></i> Xác minh số điện thoại thất bại. Vui lòng kiểm tra và thử lại.</p>
                    <div className="space m"></div>
                    <div className="textAlign-center"><a className="btn btn-primary btn--m" type="button" onClick={this.retry.bind(this)}> Xác minh lại</a></div>
                </Modal.Body>
            }
        } else {
            content = <Modal.Body>
                <p className="textAlign-center">Bạn cần xác minh số điện thoại mới có thể đăng kí xe.</p>
                <div className="space m"></div>
                <AccountKit
                    appId={fbappInfo.appId}
                    csrf={fbappInfo.csrf}
                    version={fbappInfo.version}
                    language={fbappInfo.language}
                    phoneNumber={profile.phoneNumber ? profile.phoneNumber.replace("+84", "") : null}
                    onResponse={(resp) => this.onAccountKitPhoneLoginResp(resp)}
                >
                    {p => <div className="textAlign-center"><a className="btn btn-primary btn--m" type="button" {...p}> Xác minh ngay</a></div>}
                </AccountKit>
            </Modal.Body>
        }
        return <Modal
            show={this.props.show}
            onHide={this.finish.bind(this)}
            dialogClassName="modal-sm modal-dialog"
        >
            <Modal.Header closeButton={true} closeLabel={""}>
                <Modal.Title>Xác minh số điện thoại</Modal.Title>
            </Modal.Header>
            {content}
        </Modal>
    }
}