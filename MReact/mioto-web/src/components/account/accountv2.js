import React from "react"
import axios from "axios"
import moment from "moment"
import { connect } from "react-redux"
import { Redirect } from "react-router-dom"

import Header from "../common/header"
import Footer from "../common/footer"
import AccountKit from "../common/accountkit"
import VerifyEmail from "./verifyemail"
import ChangeInfo from "./changeinfo"
import ChangePhone from "./changephone"
import ChangeEmail from "./changeemail"
import ChangeAvatar from "./changeavatar"
import ProfileCommentBoxV2 from "../profile/profilecommentboxv2"
import LinkFacebook from "../account/linkfacebook"
import UnLinkFacebook from "../account/unlinkfacebook"
import UnLinkGoogle from "../account/unlinkgoogle"
import LinkGoogle from "../account/linkgoogle"
import PropertiesBoxV3 from "../profile/propertiesv3"
import { MessageBox } from "../common/messagebox"
import { fbappInfo } from "../common/fbdeveloper"
import { logout, getLoggedProfile } from "../../actions/sessionAct"
import { verifyPhone } from "../../model/profile"
import { LoadingPage } from "../common/loading"
import { commonErr } from "../common/errors"
import { genderMap } from "../common/common"
import { getProfile } from "../../model/profile"

import avatar_default from "../../static/images/avatar_default.png"
import def_profile_cover from "../../static/images/background/car-9.jpg"

class AccountV2 extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            err: commonErr.INNIT,
            errMsg: "",

            reviewType: 0,
            verifyPhoneNumber: "",
            verifyPhoneAccessTok: "",
            showChangeInfo: false,
            showChangeAvatar: false,
            showMessageBox: false,
            showChangePhone: false,
            showChangeEmail: false,
            showVerifyEmail: false
        }

        this.logout = this.logout.bind(this);
        this.getProfile = this.getProfile.bind(this);
        this.showChangeInfo = this.showChangeInfo.bind(this);
        this.hideChangeInfo = this.hideChangeInfo.bind(this);
        this.showChangePhone = this.showChangePhone.bind(this);
        this.hideChangePhone = this.hideChangePhone.bind(this);
        this.showChangeEmail = this.showChangeEmail.bind(this);
        this.hideChangeEmail = this.hideChangeEmail.bind(this);
        this.showVerifyEmail = this.showVerifyEmail.bind(this);
        this.hideVerifyEmail = this.hideVerifyEmail.bind(this);
        this.showMessageBox = this.showMessageBox.bind(this);
        this.hideMessageBox = this.hideMessageBox.bind(this);
        this.showChangeAvatar = this.showChangeAvatar.bind(this);
        this.hideChangeAvatar = this.hideChangeAvatar.bind(this);
        this.hideChangePaper = this.hideChangePaper.bind(this);
        this.onAccountKitPhoneLoginResp = this.onAccountKitPhoneLoginResp.bind(this);
    }

    componentWillReceiveProps(props) {
        if (props.session.profile.info && props.session.profile.info.uid) {
            if (!this.props.session.profile.info || !this.props.session.profile.info.uid
                || this.props.session.profile.info.uid !== props.session.profile.info.uid) {
                this.setState({
                    err: commonErr.LOADING
                });
                getProfile(props.session.profile.info.uid, "pd" /*view count for server*/).then(resp => {
                    this.setState({
                        err: resp.data.error,
                        errMsg: resp.data.errorMessage,
                        profile: {
                            ...props.session.profile.info,
                            owner: resp.data.data.profile.owner,
                            traveler: resp.data.data.profile.traveler,
                            timeCreated: resp.data.data.profile.timeCreated
                        },
                        totalOwnerCars: resp.data.data.totalOwnerCars,
                        cars: resp.data.data.cars,
                        moreReviewFromOwner: resp.data.data.moreReviewFromOwner,
                        moreReviewFromTraveler: resp.data.data.moreReviewFromTraveler,
                        reviews: resp.data.data.reviews,
                        profiles: resp.data.data.profiles
                    });
                });
            } else if (this.state.profile) {
                this.setState({
                    profile: {
                        ...props.session.profile.info,
                        owner: this.state.profile.owner,
                        traveler: this.state.profile.traveler,
                        timeCreated: this.state.profile.timeCreated
                    }
                });
            }
        }
    }

    getProfile() {
        this.props.dispatch(getLoggedProfile());
    }

    logout() {
        this.props.dispatch(logout());
    }

    showChangeInfo() {
        this.setState({
            showChangeInfo: true
        });
    }

    hideChangeInfo() {
        this.setState({
            showChangeInfo: false
        });
    }

    showChangeAvatar() {
        this.setState({
            showChangeAvatar: true
        });
    }

    hideChangeAvatar() {
        this.setState({
            showChangeAvatar: false
        });
    }

    showChangePaper() {
        this.setState({
            showChangePaper: true
        });
    }

    hideChangePaper() {
        this.setState({
            showChangePaper: false
        });
    }

    showChangePhone() {
        this.setState({
            showChangePhone: true
        });
    }

    hideChangePhone() {
        this.setState({
            showChangePhone: false
        });
    }

    showChangeEmail() {
        this.setState({
            showChangeEmail: true
        });
    }

    hideChangeEmail() {
        this.setState({
            showChangeEmail: false
        });
    }

    showVerifyEmail() {
        this.setState({
            showVerifyEmail: true
        });
    }

    hideVerifyEmail() {
        this.setState({
            showVerifyEmail: false
        });
    }

    reviewTypeChange(event) {
        this.setState({
            reviewType: event.target.value
        });
    }

    showMessageBox() {
        this.setState({
            showMessageBox: true
        });
    }

    hideMessageBox() {
        this.setState({
            showMessageBox: false
        });
    }

    onAccountKitPhoneLoginResp(resp) {
        var verifyPhoneAccessTok;
        var verifyPhoneNumber;

        if (resp.code) {
            axios({
                method: "GET",
                url: `${fbappInfo.getAccessTokBaseDomain}?grant_type=authorization_code&code=${resp.code}&access_token=AA|${fbappInfo.appId}|${resp.state}`
            }).then(tokenData => {
                if (tokenData.data) {
                    verifyPhoneAccessTok = tokenData.data.access_token;
                    if (verifyPhoneAccessTok) {
                        axios({
                            method: "GET",
                            url: `${fbappInfo.getLoggedInfoBaseDomain}/?access_token=${verifyPhoneAccessTok}`
                        }).then(accountData => {
                            if (accountData.data && accountData.data.phone) {
                                verifyPhoneNumber = accountData.data.phone.number;
                                if (verifyPhoneNumber) {
                                    verifyPhone(verifyPhoneNumber, verifyPhoneAccessTok, 1).then(resp => {
                                        this.setState({
                                            err: resp.data.error,
                                            errMsg: resp.data.errorMessage,
                                            showMessageBox: true
                                        });
                                    });
                                }
                            }
                        })
                    }
                }
            })
        }
    }

    updatePaper(paper) {
        this.setState({
            paper: paper
        });
        this.showChangePaper();
    }

    render() {
        var content;
        const profile = this.state.profile;
        if (this.props.session.profile.err === commonErr.INNIT
            || this.props.session.profile.err === commonErr.LOADING
            || this.state.err === commonErr.LOADING) {
            content = <LoadingPage />
        } else if (profile && profile.uid) {
            const totalTrip = profile.traveler.totalTrips + profile.owner.totalTrips;
            content = <section className="body">
                <div className="cover-profile new-profile" style={{ backgroundImage: `url(${def_profile_cover})` }} />
                <div className="profile__sect">
                    <div className="content-profile--new">
                        <div className="desc-profile desc-account">
                            <div className="avatar-box">
                                <div className="avatar avatar--xl has-edit" onClick={this.showChangeAvatar}>
                                    <div className="avatar-img" style={{ backgroundImage: `url(${(profile.avatar && profile.avatar.fullUrl) ? profile.avatar.fullUrl : avatar_default})` }}></div>
                                </div>
                            </div>
                            <div className="snippet">
                                <div className="item-title">
                                    <p>{profile.name}</p>
                                    <a className="func-edit" title="Chỉnh sửa" onClick={this.showChangeInfo}><i className="ic ic-edit" /></a>
                                </div>
                                <div className="d-flex">
                                    <span className="join">Tham gia: {moment(profile.timeCreated).format("DD/MM/YYYY")}</span>
                                    <div className="bar-line" />
                                    <span className="sum-trips">{totalTrip > 0 ? `${totalTrip} chuyến` : "Chưa có chuyến"}</span>
                                </div>
                                <div className="line-info">
                                    <div className="d-flex">
                                        <div className="info"><span className="label">Ngày sinh </span><span className="ctn">{profile.dob !== 0 ? moment(profile.dob).format("DD/MM/YYYY") : ""}</span></div>
                                        <div className="info"><span className="label">Giới tính </span><span className="ctn">{profile.gender ? genderMap[profile.gender] : "Nam"}</span></div>
                                    </div>
                                    {profile.owner && profile.owner.responseRate !== "-" && <div className="wr-info">
                                        <div className="s-info"><span className="lstitle">Tỉ lệ phản hồi</span><span className="ctn">{profile.owner.responseRate}</span></div>
                                        <div className="s-info"><span className="lstitle">Thời gian phản hồi</span><span className="ctn">{profile.owner.responseTime}</span></div>
                                        <div className="s-info"><span className="lstitle">Tỉ lệ đồng ý</span><span className="ctn">{profile.owner.acceptRate}</span></div>
                                    </div>}
                                </div>
                            </div>
                        </div>
                        <div className="desc-profile">
                            <div className={`information information--acc`}>
                                <div className="inside">
                                    <ul>
                                        <li>
                                            <span className="label">Điện thoại</span>
                                            <span className="ctn">{profile.phoneVerified === 1 && <i className="ic ic-verify" />}{profile.phoneNumber ? profile.phoneNumber : ""}
                                                {profile.phoneNumber && profile.phoneVerified !== 1 && <AccountKit
                                                    appId={fbappInfo.appId}
                                                    csrf={fbappInfo.csrf}
                                                    version={fbappInfo.version}
                                                    language={fbappInfo.language}
                                                    phoneNumber={profile.phoneNumber ? profile.phoneNumber.replace("+84", "") : null}
                                                    onResponse={(resp) => this.onAccountKitPhoneLoginResp(resp)}
                                                >
                                                    {p => <a {...p} className="verify btn btn--s">Xác minh</a>}
                                                </AccountKit>}
                                                <a className="func-edit" onClick={this.showChangePhone} title="Edit"><i className="ic ic-edit" /></a></span>
                                        </li>
                                        <li>
                                            <span className="label">Email</span>
                                            <span className="ctn">{profile.emailVerified === 1 && <i className="ic ic-verify" />}{profile.email ? profile.email : ""}
                                                {profile.email && profile.emailVerified !== 1 && <a className="verify btn btn--s">Xác minh</a>}
                                                <a className="func-edit" onClick={this.showChangeEmail} title="Edit"><i className="ic ic-edit" /></a>
                                            </span>
                                        </li>
                                        <li>
                                            <span className="label">Facebook</span>
                                            <span className="ctn">{profile.facebookName ? <span>{profile.facebookName} {profile.facebookFr && <span>(có {profile.facebookFr} bạn)</span>} <UnLinkFacebook getProfile={this.getProfile} /> </span> : <LinkFacebook />}</span>
                                        </li>
                                        <li>
                                            <span className="label">Google</span>
                                            <span className="ctn">{profile.googleName ? <span>{profile.googleName} <UnLinkGoogle getProfile={this.getProfile} /> </span> : <LinkGoogle />}</span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="profile__wrap">
                        <div className="review__sect">
                            <div className="review-container">
                                <ProfileCommentBoxV2 profile={profile} reviewType={0} />
                                <ProfileCommentBoxV2 profile={profile} reviewType={1} />
                            </div>
                        </div>
                        <PropertiesBoxV3 profile={profile} />
                    </div>
                </div>
                <ChangePhone show={this.state.showChangePhone} hideModal={this.hideChangePhone} getProfile={this.getProfile} />
                <ChangeEmail show={this.state.showChangeEmail} hideModal={this.hideChangeEmail} getProfile={this.getProfile} />
                <VerifyEmail email={profile.email} show={this.state.showVerifyEmail} hideModal={this.hideVerifyEmail} getProfile={this.getProfile} />
                <ChangeAvatar show={this.state.showChangeAvatar} getProfile={this.getProfile} hideModal={this.hideChangeAvatar} />
                <ChangeInfo show={this.state.showChangeInfo} getProfile={this.getProfile} profile={this.props.session.profile} hideModal={this.hideChangeInfo} />
                <MessageBox show={this.state.showMessageBox} hideModal={this.hideMessageBox} message={this.state.errMsg} />
            </section>
        } else {
            content = <Redirect to="/" />
        }

        return <div className="mioto-layout">
            <Header />
            {content}
            <Footer />
        </div>
    }
}

function mapState(state) {
    return {
        session: state.session
    }
}

AccountV2 = connect(mapState)(AccountV2)

export default AccountV2;