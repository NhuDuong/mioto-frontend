import React from "react"
import { connect } from "react-redux"
import { CopyToClipboard } from 'react-copy-to-clipboard'
import {
    FacebookShareButton,
    GooglePlusShareButton,
    LinkedinShareButton,
    TwitterShareButton,
    EmailShareButton,
    FacebookIcon,
    TwitterIcon,
    GooglePlusIcon,
    LinkedinIcon,
    EmailIcon,
} from 'react-share'

import Header from "../common/header"
import Footer from "../common/footer"
import { commonErr } from "../common/errors"
import { LoadingPage } from "../common/loading"
import { getLoggedProfile } from "../../actions/sessionAct"
import { MessagePage, MessageBox } from "../common/messagebox"
import Session from "../common/session"

import img_sharedcode from "../../static/images/sharecode.png"

class SharedCode extends React.Component {
    constructor() {
        super();
        this.state = {
            err: commonErr.INNIT,
            copied: false
        }
    }

    componentDidMount() {
        const self = this;
        if (!self.props.session.profile.info || !self.props.session.profile.info.uid) {
            self.getProfile();
        }
    }

    getProfile() {
        this.props.dispatch(getLoggedProfile());
    }

    hideCopyBox() {
        this.setState({
            copied: false
        });
    }

    render() {
        const err = this.props.session.profile.err;
        const profile = this.props.session.profile.info;
        var content;

        if (err === commonErr.INNIT
            || err === commonErr.LOADING) {
            content = <LoadingPage />
        } else if (profile && profile.uid) {
            content = <section className="body">
                <div className="scode_wrapper min-height">
                    <div className="m-container">
                        <h3>GIỚI THIỆU BẠN BÈ</h3>
                        <div className="scode_grid">
                            <div>
                                <p className="desc">Chia sẻ mã giới thiệu với bạn bè để cả hai đều được giảm giá <span>{profile.discountPercent}%</span> (tối đa <span>{profile.discountMax}</span> VNĐ)</p>
                                <div className="space m" />
                                <p className="desc">Mã giới thiệu</p>
                                <div className="form-default">
                                    <div className="line-form">
                                        <div className="wrap-input has-ico right">
                                            <CopyToClipboard text={profile.shareCode}
                                                onCopy={() => this.setState({ copied: true })}>
                                                <span><i className="ic ic-copy"> </i></span>
                                            </CopyToClipboard>
                                            <input type="text" value={profile.shareCode} disabled />
                                        </div>
                                    </div>
                                    <div className="space m" />
                                    <div className="line-form d-flex" style={{ cursor: "pointer" }}>
                                        <FacebookShareButton style={{ padding: "2px" }} url={"https://www.mioto.vn/"} children={<FacebookIcon size={40} round={true} />} />
                                        <GooglePlusShareButton style={{ padding: "2px" }} url={"https://www.mioto.vn/"} children={<GooglePlusIcon size={40} round={true} />} />
                                        <LinkedinShareButton style={{ padding: "2px" }} url={"https://www.mioto.vn/"} children={<LinkedinIcon size={40} round={true} />} />
                                        <TwitterShareButton style={{ padding: "2px" }} url={"https://www.mioto.vn/"} children={<TwitterIcon size={40} round={true} />} />
                                        <EmailShareButton style={{ padding: "2px" }} url={"https://www.mioto.vn/"} children={<EmailIcon size={40} round={true} />} />
                                    </div>
                                </div>
                            </div>
                            <div className="img-share"><img src={img_sharedcode} alt="Mioto - Thuê xe tự lái"/></div>
                        </div>
                        <MessageBox error={commonErr.SUCCESS} show={this.state.copied} hideModal={this.hideCopyBox.bind(this)} message="Đã sao chép mã giảm giá vào bộ nhớ. Hãy chia sẻ ngay với bạn bè của mình." />
                    </div>
                </div>
            </section>
        } else {
            content = <MessagePage message="Vui lòng đăng nhập để sử dụng tính năng này." />
        }

        return <div className="mioto-layout">
            <Session />
            <Header />
            {content}
            <Footer />
        </div>
    }
}

function mapState(state) {
    return {
        session: state.session
    }
}

SharedCode = connect(mapState)(SharedCode)

export default SharedCode;