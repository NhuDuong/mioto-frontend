import React from "react"
import { Modal } from "react-bootstrap"

import { unLinkFb } from "../../model/profile"
import { commonErr } from "../common/errors"

export default class UnLinkFacebook extends React.Component {
    constructor() {
        super();
        this.state = {
            step: 0, //0: show button 1: show confirm 2: show unlink result
            isShowModal: false,
            err: commonErr.INNIT,
            errMsg: ""
        }
    }

    onBtnUnLinkFbClick() {
        this.setState({
            isShowModal: true,
            step: 1
        });
    }

    onContinueBtnClick() {
        const self = this;
        unLinkFb().then(function (resp) {
            self.setState({
                step: 2,
                err: resp.data.error,
                errMsg: resp.data.errorMessage
            });
        });
    }

    onFinishBtnClick() {
        this.setState({
            isShowModal: false
        });
        if (this.state.err >= commonErr.SUCCESS) {
            this.props.getProfile();
        }
    }

    hideModal() {
        this.setState({
            step: 0,
            isShowModal: false,
            err: commonErr.INNIT,
            errMsg: ""
        });
        if (this.state.step === 2 && this.state.err >= commonErr.SUCCESS) {
            this.props.getProfile();
        }
    }

    render() {
        var modalContent;
        if (this.state.step === 1) {
            modalContent = <div className="form-default form-s textAlign-center">
                <div className="line-form">Bạn có chắc muốn bỏ liên kết tài khoản của mình với tài khoản Facebook hiện tại?</div>
                <div className="clear"></div>
                <div className="space m"></div>
                <button className="btn btn-primary btn--m" type="button" onClick={this.onContinueBtnClick.bind(this)}>Tiếp tục</button>
            </div>
        } else {
            var modalMessage;
            if (this.state.err >= commonErr.SUCCESS) {
                modalMessage = <div className="line-form textAlign-center"><i className="ic ic-verify"></i> Bỏ liên kết tài khoản thành công </div>;
            } else {
                modalMessage = <div className="line-form textAlign-center"><i className="ic ic-warning"></i> Bỏ liên kết thất bại. ${this.state.errMsg}</div>;
            }
            modalContent = <div className="form-default form-s">
                {modalMessage}
                <div className="clear"></div>
                <button className="btn btn-primary btn--m" type="button" onClick={this.onFinishBtnClick.bind(this)}>Hoàn tất</button>
            </div >
        }

        return <span>
            <a onClick={this.onBtnUnLinkFbClick.bind(this)} className="func-edit"><i className="ic ic-remove"></i></a>
            <Modal
                show={this.state.isShowModal}
                onHide={this.hideModal.bind(this)}
                dialogClassName="modal-sm modal-dialog"
            >
                <Modal.Header closeButton={true} closeLabel={""}>
                    <Modal.Title>Xác nhận</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    {modalContent}
                </Modal.Body>
            </Modal>
        </span>
    }
}