import React from "react"
import moment from "moment"
import { Modal } from "react-bootstrap"
import NumberFormat from "react-number-format"
import StarRatings from "react-star-ratings"

import { commonErr } from "../common/errors"
import { getCarRequestInfo, responseCarRequest } from "../../model/car"
import { LoadingPage } from "../common/loading"
import { getRejectTripReasonsOwner } from "../../model/car"

import avatar_default from "../../static/images/avatar_default.png"
import car_photo from "../../static/images/upload/car_1.png"

class CarMapLocation extends React.Component {
    componentDidMount() {
        if (this.props.location) {
            setTimeout(() => {
                this.initMap(this.refs, this.props.location.lat, this.props.location.lon);
            }, 100)
        }
    }

    initMap(refs, lat, lng) {
        const map = new window.google.maps.Map(refs.map, {
            center: {
                lat: lat,
                lng: lng
            },
            zoom: 14,
            mapTypeControl: false,
            scrollwheel: false,
            clickableIcons: false,
            streetViewControl: false,
            zoomControl: true,
            zoomControlOptions: {
                position: window.google.maps.ControlPosition.RIGHT_BOTTOM
            }
        })

        this.marker = new window.google.maps.Marker({
            map: map,
            position: new window.google.maps.LatLng(this.props.location.lat, this.props.location.lon)
        });
    }

    render() {
        return <div ref="map" className="fix-map"></div>
    }
}

export default class BookingRequestBox extends React.Component {
    constructor() {
        super();
        this.state = {
            err: commonErr.INNIT,
            errMsg: "",
            step: 1,
            isAccept: false,
            respMessage: ""
        }
    }

    componentDidMount() {
        this.setState({
            err: commonErr.LOADING
        });

        //get request detail
        getCarRequestInfo(this.props.id).then(resp => {
            this.setState({
                err: resp.data.error,
                errMsg: resp.data.errorMessage,
                detail: resp.data.data
            });
        });
    }

    onAcceptBtnClick() {
        this.setState({
            step: 2,
            isAccept: true,
            cancelReason: null,
        });
    }

    onRejectBtnClick() {
        this.setState({
            err: commonErr.LOADING
        });
        getRejectTripReasonsOwner().then(resp => {
            this.setState({
                err: resp.data.error,
                errMsg: resp.data.errorMessage,
                cancelReasons: resp.data.data.reasons,

                step: 2,
                isAccept: false,
                cancelReason: "r0"
            });
        });
    }

    onBackBtnClick() {
        this.setState({
            step: 1
        });
    }

    onRespBtnClick() {
        this.setState({
            err: commonErr.LOADING
        });

        //send response
        responseCarRequest(this.props.id, this.state.isAccept, this.state.respMessage, this.state.cancelReason).then(resp => {
            this.setState({
                err: resp.data.error,
                errMsg: resp.data.errorMessage,
                step: 3
            });
        });
    }

    onRespMessageChange(event) {
        this.setState({
            respMessage: event.target.value
        });
    }

    onRetryBtnClick() {
        if (this.state.step === 1) {
            this.setState({
                err: commonErr.LOADING
            });

            //get request detail
            this.setState({
                err: commonErr.SUCCESS
            });
        } else if (this.state.step === 3) {
            this.setState({
                err: commonErr.LOADING
            });

            //send response
            responseCarRequest(this.props.id, this.state.isAccept, this.state.respMessage, this.state.cancelReason).then(resp => {
                this.setState({
                    err: resp.data.error
                });
            })
        }
    }

    onReasonChange = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        });
    }

    onCloseBtnClick() {
        this.props.hide();
    }

    isValidCancelTrip() {
        if (this.state.isAccept) {
            return true;
        }
        if (!this.state.cancelReason || this.state.cancelReason === "" || this.state.cancelReason === "0") {
            return false;
        }
        if (this.state.cancelReason === "r0" && (!this.state.respMessage || this.state.respMessage === "")) {
            return false;
        }
        return true;
    }

    render() {
        var content;
        if (this.state.err === commonErr.INNIT || this.state.err === commonErr.LOADING) {
            content = <LoadingPage />
        } else if (this.state.step === 3) {
            if (this.state.err >= commonErr.SUCCESS) {
                content = <Modal.Body>
                    <div className="module-register" style={{ background: "none", padding: "0px" }}>
                        <div className="register-container">
                            <div className="content-register">
                                <div className="content-booking" style={{ padding: "0px" }}>
                                    <div className="info-car--more" style={{ paddingTop: "0px" }}>
                                        <div className="full-wrapper">
                                            <div className="group-7">
                                                <div className="require-paper">
                                                    <div className="ctn-desc-new">
                                                        <p className="textAlign-center">{this.state.isAccept ? "Bạn đã đồng ý yêu cầu đặt xe." : "Bạn đã từ chối yêu cầu đặt xe."}</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="wrap-btn has-2btn">
                                                <div className="wr-btn"><a className="btn btn-secondary btn--m" href="/mycars">Quản lý xe </a></div>
                                                <div className="wr-btn"><a className="btn btn-primary btn--m" onClick={this.onCloseBtnClick.bind(this)}>Hoàn tất </a></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </Modal.Body>
            } else {
                content = <Modal.Body>
                    <div className="module-register" style={{ background: "none", padding: "0px" }}>
                        <div className="register-container">
                            <div className="content-register">
                                <div className="content-booking" style={{ padding: "0px" }}>
                                    <div className="info-car--more" style={{ paddingTop: "0px" }}>
                                        <div className="group-7">
                                            <div className="require-paper">
                                                <div className="ctn-desc-new">
                                                    <p className="textAlign-center">{this.state.errMsg}</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="full-wrapper">
                                            <div className="wrap-btn has-2btn">
                                                <div className="wr-btn"><a className="btn btn-secondary btn--m" onClick={this.onCloseBtnClick.bind(this)}>Hủy bỏ </a></div>
                                                <div className="wr-btn"><a className="btn btn-primary btn--m" onClick={this.onRetryBtnClick.bind(this)}>Thử lại </a></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </Modal.Body>
            }
        } else if (this.state.step === 2) {
            content = <Modal.Body>
                <div className="module-register" style={{ background: "none", padding: "0px" }}>
                    <div className="register-container">
                        <div className="content-register">
                            <div className="content-booking" style={{ padding: "0px" }}>
                                <div className="info-car--more" style={{ paddingTop: "0px" }}>
                                    <div className="full-wrapper">
                                        {!this.state.isAccept && <div className="line-form">
                                            <h6>Lý do từ chối</h6>
                                            <div className="wrap-select">
                                                <select name="cancelReason" onChange={this.onReasonChange.bind(this)}>
                                                    {this.state.cancelReasons && this.state.cancelReasons.map(
                                                        (reason, i) => <option key={i} value={reason.reason}>{reason.desc}</option>
                                                    )}
                                                </select>
                                            </div>
                                        </div>}
                                        <div className="group-info">
                                            <h6>Lời nhắn </h6>
                                            <textarea className="textarea" onChange={this.onRespMessageChange.bind(this)} value={this.state.respMessage} />
                                        </div>
                                        <div className="wrap-btn has-2btn">
                                            <div className="wr-btn"><a className="btn btn-secondary btn--m" onClick={this.onBackBtnClick.bind(this)}>Quay lại </a></div>
                                            <div className="wr-btn"><a className={`btn btn-primary btn--m ${this.isValidCancelTrip() ? "" : "disabled"}`} onClick={this.onRespBtnClick.bind(this)}>Gửi trả lời</a></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </Modal.Body>
        } else {
            if (this.state.err < commonErr.SUCCESS) {
                content = <Modal.Body>
                    <div className="module-register" style={{ background: "none", padding: "0px" }}>
                        <div className="register-container">
                            <div className="content-register">
                                <div className="content-booking" style={{ padding: "0px" }}>
                                    <div className="info-car--more" style={{ paddingTop: "0px" }}>
                                        <div className="group-7">
                                            <div className="require-paper">
                                                <div className="ctn-desc-new">
                                                    <p className="textAlign-center">Không tìm thấy thông tin</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="full-wrapper">
                                            <div className="wrap-btn has-2btn">
                                                <div className="wr-btn"><a className="btn btn-secondary btn--m" onClick={this.onCloseBtnClick.bind(this)}>Hủy bỏ </a></div>
                                                <div className="wr-btn"><a className="btn btn-primary btn--m" onClick={this.onRetryBtnClick.bind(this)}>Thử lại </a></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </Modal.Body>
            } else {
                const detail = this.state.detail;
                const { car, carRequest, traveler, travelerPapers, travelerPapersOther } = detail;
                const priceSummary = car.priceSummary;

                content = <Modal.Body>
                    <div className="module-register" style={{ background: "none", padding: "0px" }}>
                        <div className="register-container trip-container">
                            <div className="content-register">
                                {carRequest.statusT && <div className="stepbystep">
                                    <div className="booking-status status-trips">
                                        <p> <span className="status orange-dot"> </span><span>{carRequest.statusT}</span></p>
                                    </div>
                                </div>}
                                <div className="content-booking">
                                    <div className="info-car--more" style={{ paddingTop: "0px" }}>
                                        <div className="group-1">
                                            <div className="car-img">
                                                <div className="fix-img"> <img src={car.photos ? car.photos[0].thumbUrl : car_photo} /></div>
                                            </div>
                                        </div>
                                        <div className="group-2">
                                            <div className="group-info">
                                                <p className="license-plate">Biển số xe: <span>{car.licensePlate}</span></p>
                                                <h6 className="name">{car.name}</h6>
                                            </div>
                                        </div>
                                        {traveler && <div className="group-3">
                                            <div className="profile-mini-v2">
                                                <div className="avatar avatar-new" title="title name">
                                                    <div className="avatar-img" style={{ backgroundImage: `url(${traveler.avatar ? traveler.avatar.thumbUrl : avatar_default})` }}></div>
                                                </div>
                                                <div className="desc-v2">
                                                    <span className="lstitle">Khách thuê </span>
                                                    <h2><a href={`/profile/${traveler.uid}`}>{traveler.name}</a></h2>
                                                    <StarRatings
                                                        rating={traveler.owner.rating.avg}
                                                        starRatedColor="#00a550"
                                                        starDimension="17px"
                                                        starSpacing="1px"
                                                    />
                                                </div>
                                            </div>
                                        </div>}
                                        <div className="group-4">
                                            <div className="group-info">
                                                <h6>Thời gian thuê xe <a href={`/carsetting/${car.id}#calendarsetting`} target="_blank">(Lịch xe)</a></h6>
                                                <div className="form-default grid-form">
                                                    <div className="line-form has-timer">
                                                        <p>Bắt đầu: {moment(carRequest.tripDateFrom).format("dddd, HH:mm, MM/DD/YYYY")}</p>
                                                    </div>
                                                    <div className="line-form has-timer">
                                                        <p>Kết thúc: {moment(carRequest.tripDateTo).format("dddd, HH:mm, MM/DD/YYYY")}</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="group-info">
                                                <h6>Địa điểm giao nhận xe</h6>
                                                <div className="address-car">
                                                    <div className="line-form"><span className="value">{carRequest.dAddr ? carRequest.dAddr : "Địa chỉ xe"}</span></div>
                                                    {carRequest.dLoc && <CarMapLocation location={carRequest.dLoc} />}
                                                </div>
                                            </div>
                                        </div>
                                        <div className="group-5">
                                            <div className="group-info">
                                                <h6>Bảng tính giá</h6>
                                                <div className="form-default no-mg">
                                                    <div className="line-form has-timer">
                                                        {priceSummary.discountPercent > 0 && <div style={{ width: "50%" }} className="wrap-input has-dropdown date has-value"><span className="value" style={{ textDecoration: "line-through", color: "#aeaeb0" }}>Đơn giá thuê</span></div>}
                                                        {priceSummary.discountPercent > 0 && <div style={{ width: "50%" }} className="wrap-input has-dropdown time has-value"><span className="value"><NumberFormat style={{ textDecoration: "line-through", color: "#aeaeb0" }} value={priceSummary.priceOrigin} displayType={'text'} thousandSeparator={" "} decimalSeparator={","} suffix={" /ngày"} /></span></div>}
                                                        <div style={{ width: "50%" }} className="wrap-input has-dropdown date has-value"><span className="value">Đơn giá thuê {priceSummary.discountPercent > 0 && ` (giảm ${priceSummary.discountPercent}%)`}</span></div>
                                                        <div style={{ width: "50%" }} className="wrap-input has-dropdown time has-value"><span className="value"><NumberFormat value={priceSummary.price} displayType={'text'} thousandSeparator={" "} decimalSeparator={","} /> /ngày</span></div>
                                                        <div style={{ width: "50%" }} className="wrap-input has-dropdown date has-value"><span className="value">Tổng phí thuê xe</span></div>
                                                        <div style={{ width: "50%" }} className="wrap-input has-dropdown time has-value"><span className="value"><NumberFormat value={priceSummary.totalPerDay} displayType={'text'} thousandSeparator={" "} decimalSeparator={","} /> x <strong>{priceSummary.totalDays}</strong></span></div>
                                                        {priceSummary.deliveryFee > 0 && <div style={{ width: "50%" }} className="wrap-input has-dropdown date has-value"><span className="value">Tổng phí giao nhận xe</span></div>}
                                                        {priceSummary.deliveryFee > 0 && <div style={{ width: "50%" }} className="wrap-input has-dropdown time has-value"><span className="value"><NumberFormat value={priceSummary.deliveryFee > 0 ? priceSummary.deliveryFee : 0} displayType={'text'} thousandSeparator={" "} decimalSeparator={","} /> {(priceSummary.deliveryDistance && priceSummary.deliveryDistance) > 0 && <NumberFormat value={priceSummary.deliveryDistance} displayType={'text'} thousandSeparator={" "} decimalSeparator={","} prefix={" ("} suffix={" km)"} decimalScale={1} />}</span></div>}
                                                        {priceSummary.promotionDiscount > 0 && <div style={{ width: "50%" }} className="wrap-input has-dropdown date has-value"><span className="value">Tổng giảm giá</span></div>}
                                                        {priceSummary.promotionDiscount > 0 && <div style={{ width: "50%" }} className="wrap-input has-dropdown time has-value"><span className="value"><NumberFormat value={priceSummary.promotionDiscount > 0 ? priceSummary.promotionDiscount : 0} displayType={'text'} thousandSeparator={" "} decimalSeparator={","} /></span></div>}
                                                        <div style={{ width: "50%" }} className="wrap-input has-dropdown date has-value"><span className="value"><h6>Tổng chi phí</h6></span></div>
                                                        <div style={{ width: "50%" }} className="wrap-input has-dropdown time has-value"><span className="value"><h6><NumberFormat value={priceSummary.priceTotal} displayType={'text'} thousandSeparator={" "} decimalSeparator={","} /></h6></span></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="group-6">
                                            <div className="require-paper">
                                                <h6>Giấy tờ khách thuê cung cấp</h6>
                                                <div className="ctn-desc-new">
                                                    <ul className="required">
                                                        {(travelerPapers && travelerPapers.length > 0) && travelerPapers.map((paper, i) => <li key={i}><img className="img-ico" src={paper.logo} style={{ width: "20px", height: "20px" }} />{paper.name}</li>)}
                                                        {(!travelerPapers || travelerPapers.length === 0) && <p>Không có giấy tờ</p>}
                                                    </ul>
                                                </div>
                                            </div>
                                            {travelerPapersOther && <div className="require-paper">
                                                <h6>Giấy tờ khác</h6>
                                                <div className="ctn-desc-new">
                                                    <p>{travelerPapersOther}</p>
                                                </div>
                                            </div>}
                                        </div>
                                        {carRequest.msgFromAdmin && <div className="group-7">
                                            <div className="require-paper">
                                                <h6>Lời nhắn</h6>
                                                <div className="ctn-desc-new">
                                                    <p>{carRequest.msgFromAdmin}</p>
                                                </div>
                                            </div>
                                        </div>}
                                        {carRequest.status <= 1 && <div className="full-wrapper">
                                            <div className="wrap-btn has-2btn">
                                                <div className="wr-btn"><a className="btn btn-secondary btn--m" onClick={this.onRejectBtnClick.bind(this)}>Từ chối </a></div>
                                                <div className="wr-btn"><a className="btn btn-primary btn--m" onClick={this.onAcceptBtnClick.bind(this)}>Đồng ý </a></div>
                                            </div>
                                        </div>}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </Modal.Body>
            }
        }

        return <Modal
            show={this.props.isShow}
            onHide={this.props.hide}
            dialogClassName="modal-dialog modal-lg modal-dialog modal-confirm modal-request-detail"
        >
            <Modal.Header closeButton={true} closeLabel={""} >
                <Modal.Title>Yêu cầu thuê xe từ Admin</Modal.Title>
            </Modal.Header>
            {content}
        </Modal>
    }
}