import React from "react"
import moment from "moment"
import { connect } from "react-redux"
import HtmlToReactParser from "html-to-react"
import { Modal } from "react-bootstrap"
import cookie from 'react-cookies'

import { commonErr } from "../common/errors"
import { getTripStatus, getTripTime } from "../common/common"
import {
    getUserNotify, getUserNotifyDetail, getPopupNofify, seenUserNotify, seenSystemNotify,
    getUserNotifyCouter, resetUsernotifyCounter
} from "../../model/profile"
import { getUpcommingTrip } from "../../model/car"
import NotificationBox from "../account/notificationbox"
import BookingRequestBox from "./bookingreq"
import { LoadingInline } from "../common/loading"

import avatar_default from "../../static/images/avatar_default.png"

const AnnouceBox = (props) => {
    const isShow = props.isShow;
    const noti = props.noti;
    const htmlToReactParser = new HtmlToReactParser.Parser();
    const content = htmlToReactParser.parse(noti);
    return <Modal
        show={isShow}
        onHide={props.hide}
        dialogClassName="modal-sm modal-dialog notify-container-fluid"
    >
        <Modal.Header closeButton={true} closeLabel={""} />
        {content}
    </Modal>
}

const SysNotiBox = (props) => {
    const isShow = props.isShow;
    const noti = props.noti;
    const htmlToReactParser = new HtmlToReactParser.Parser();
    const content = htmlToReactParser.parse(noti.content);
    return <Modal
        show={isShow}
        onHide={props.hide}
        dialogClassName="modal-sm modal-dialog notify-container-fluid"
    >
        <Modal.Header closeButton={true} closeLabel={""} />
        {content}
    </Modal>
}

class Notifications extends React.Component {
    constructor(props) {
        super(props);
        cookie.save("_mcookietest", "_mcookietest", { path: '/' });
        const mcookietest = cookie.load("_mcookietest");
        cookie.remove("_mcookietest");

        this.state = {
            err: commonErr.INNIT,
            notis: [],
            newNotis: [],
            hasNewAct: false,
            hasNewUp: false,
            hasMoreNoti: 0,
            upComings: [],
            hasMoreUp: 0,
            active: 0, //:noti 1: upcomming
            isReset: false,
            needUpdateNewAct: true,
            isResetNewAct: false,
            isResetNewUp: false,
            isShowAnnouce: false,
            isShowSysNoti: false,
            isShowBookingReq: false,
            isCookieAllowed: (mcookietest !== undefined && mcookietest !== null && mcookietest !== "")
        }

        this.seenNotify = this.seenNotify.bind(this);
        this.toggleNotify = this.toggleNotify.bind(this);
        this.setWrapperRef = this.setWrapperRef.bind(this);
        this.handleClickOutside = this.handleClickOutside.bind(this);
        this.setHasNewAct = this.setHasNewAct.bind(this);
        this.setNeedUpdateNewAct = this.setNeedUpdateNewAct.bind(this);
    }

    componentWillUnmount() {
        document.removeEventListener('mousedown', this.handleClickOutside);
    }

    buildPopupSystemNotis(notis) {
        if (this.state.isCookieAllowed) {
            //filter system noti
            var snotis = notis.filter(n => {
                return (n.type === 1)
            })

            if (snotis.length > 0) {
                //filter unalerted system noti
                var alertedNotis = cookie.load("_msysnotis");
                if (alertedNotis && alertedNotis !== "") {
                    const alertedNotisId = alertedNotis.split(";");
                    if (alertedNotisId.length > 0) {
                        snotis = snotis.filter(n => {
                            var isAlerted = false;
                            for (var i = 0; i < alertedNotisId.length; ++i) {
                                if (alertedNotisId[i] === n.id) {
                                    isAlerted = true;
                                    break;
                                }
                            }
                            return !isAlerted;
                        });

                        //keep max size alerted id
                        if (alertedNotisId.length >= 50) {
                            alertedNotis = alertedNotis.replace((alertedNotisId[0] + ";"), "");
                        }
                    }
                }

                if (snotis.length > 0) {
                    const popupNoti = snotis[0]; //only popup 1 noti each page load
                    getPopupNofify(popupNoti.id).then(resp => {
                        if (resp.data.data && resp.data.data.notification) {
                            this.setState({
                                isShowSysNoti: true,
                                snoti: {
                                    id: popupNoti.id,
                                    type: popupNoti.type,
                                    title: resp.data.data.notification.title,
                                    content: resp.data.data.notification.content
                                }
                            });

                            if (alertedNotis) {
                                alertedNotis += ";" + popupNoti.id;
                            } else {
                                alertedNotis = popupNoti.id;
                            }

                            cookie.save("_msysnotis", alertedNotis, { path: '/', expires: moment().add(1, "months").toDate() });
                        }
                    })
                }
            }
        }
    }
    

    componentDidMount() {
        //user notis
        if (this.props.isLogged) {
            getUserNotify(0, 0, 0).then(resp => {
                if (resp.data.data) {
                    this.setState({
                        err: resp.data.error,
                        notis: resp.data.data.notifications,
                        hasMoreNoti: resp.data.data.more
                    });
                }
            });

            // check counter
            this.checkNotifyCounter();

            getUpcommingTrip(0, 0, 0).then(resp => {
                if (resp.data.data) {
                    this.setState({
                        err: resp.data.error,
                        upComings: resp.data.data,
                        hasMoreUp: resp.data.data.more
                    });
                }
            });
        }

        //system notis
        const notis = this.props.appInit.data.snotifs;
        if (notis) {
            //system popup notis
            this.buildPopupSystemNotis(notis);

            //filter upcoming notis
            const unoti = notis.filter(n => {
                return (n.type === 2 && n.reddot > 0)
            })
            if (unoti && unoti.length > 0) {
                this.setState({
                    hasNewUp: true
                })
            }
        }

        document.addEventListener('mousedown', this.handleClickOutside);
    }

    componentWillReceiveProps(props) {
        //user notis
        if (!this.props.isLogged && props.isLogged) {
            this.checkNotifyCounter();
        }

        const notis = props.appInit.data.snotifs;
        if (notis === this.props.appInit.data.snotifs) {
            return;
        }

        //system notis
        if (notis) {
            //system popup notis
            this.buildPopupSystemNotis(notis);

            //upcoming notis
            const unoti = notis.filter(n => {
                return (n.type === 2 && n.reddot > 0) //UPCOMMING NOTI
            });
            if (unoti && unoti.length > 0) {
                this.setState({
                    hasNewUp: true
                })
            }
        }
    }

    checkNotifyCounter(){
        getUserNotifyCouter().then(resp => {
            if (resp.data && resp.data.data && resp.data.data.counter > 0) {
                this.setState({
                    hasNewAct: true
                });
            }
        });
    }

    toggleNotify() {
        if (!this.props.isShowNotify) {
            if (this.state.active === 0) {
                if (this.state.needUpdateNewAct) {
                    this.setState({
                        err: commonErr.LOADING
                    });
                    getUserNotify(0, 0, 0).then(resp => {
                        if (resp.data.data) {
                            this.setState({
                                err: resp.data.error,
                                notis: resp.data.data.notifications,
                                hasMoreNoti: resp.data.data.more
                            });
                            if (resp.data.error >= commonErr.SUCCESS && !this.state.isResetNewAct && this.state.hasNewAct) {
                                resetUsernotifyCounter();
                                this.setState({
                                    isResetNewAct: true,
                                    hasNewAct: false
                                });
                            }
                        }
                    });
                    this.setState({
                        needUpdateNewAct: false
                    });
                }
            }

            if (this.state.active === 1) {
                this.setState({
                    err: commonErr.LOADING
                });
                getUpcommingTrip(0, 0, 0).then(resp => {
                    if (resp.data.data) {
                        this.setState({
                            err: resp.data.error,
                            upComings: resp.data.data,
                            hasMoreUp: resp.data.data.more
                        });
                    }
                });
                if (!this.state.isResetNewUp && this.state.hasNewUp) {
                    const snotis = this.props.appInit.data.snotifs;
                    if (snotis) {
                        const snoti = snotis.filter(n => {
                            return (n.type === 2 /*UPCOMMING NOTI*/ && n.reddot > 0)
                        });
                        if (snoti && snoti.length > 0) {
                            seenSystemNotify(snoti[0].id, snoti[0].type);
                        }
                    }
                    this.setState({
                        isResetNewUp: true,
                        hasNewUp: false
                    });
                }
            }
        }

        this.props.toggleNotify();
    }

    activeNotis() {
        if (this.state.needUpdateNewAct) {
            this.setState({
                err: commonErr.LOADING
            });
            getUserNotify(0, 0, 0).then(resp => {
                if (resp.data.data) {
                    this.setState({
                        err: resp.data.error,
                        notis: resp.data.data.notifications,
                        hasMoreNoti: resp.data.data.more
                    });
                    if (resp.data.error >= commonErr.SUCCESS && !this.state.isResetNewAct && this.state.hasNewAct) {
                        resetUsernotifyCounter();
                        this.setState({
                            isResetNewAct: true,
                            hasNewAct: false
                        });
                    }
                }
            });
            this.setState({
                needUpdateNewAct: false
            });
        }
        this.setState({
            active: 0
        });
    }

    activeUpcomings() {
        this.setState({
            err: commonErr.LOADING
        });
        getUpcommingTrip(0, 0, 0).then(resp => {
            if (resp.data.data) {
                this.setState({
                    err: resp.data.error,
                    upComings: resp.data.data,
                    hasMoreUp: resp.data.data.more
                });
            }
        });

        if (!this.state.isResetNewUp && this.state.hasNewUp) {
            const snotis = this.props.appInit.data.snotifs;
            if (snotis) {
                const snoti = snotis.filter(n => {
                    return (n.type === 2 /*UPCOMMING NOTI*/ && n.reddot > 0)
                });
                if (snoti && snoti.length > 0) {
                    seenSystemNotify(snoti[0].id, snoti[0].type);
                }
            }

            this.setState({
                isResetNewUp: true,
                hasNewUp: false
            });
        }

        this.setState({
            active: 1
        });
    }

    showAnnounceNotify(noti) {
        this.setState({
            isShowAnnouce: true,
            annouceNoti: noti
        });
    }

    hideAnnounceNotify() {
        this.setState({
            isShowAnnouce: false,
            annouceNoti: null
        });
    }

    hideSysNotiBox() {
        seenSystemNotify(this.state.snoti.id, this.state.snoti.type);
        this.setState({
            isShowSysNoti: false,
            snoti: null
        });
    }

    hideBookingReq() {
        this.setState({
            isShowBookingReq: false
        });
    }

    seenNotify(noti) {
        if (!noti.seen) {
            const newNotis = [];
            for (var i = 0; i < this.state.notis.length; ++i) {
                const newNoti = {
                    ...this.state.notis[i]
                }
                if (newNoti.id === noti.id) {
                    newNoti.seen = true;
                }
                newNotis.push(newNoti);
            }
            this.setState({
                notis: newNotis
            })
            this.checkNotifyCounter();
            seenUserNotify(noti.id).then(resp => {
                switch (noti.type) {
                    case 1:
                        if (!noti.alFull || noti.alFull === 0) {
                            getUserNotifyDetail(noti.id).then(resp => {
                                if (resp.data.data && resp.data.data.notification) {
                                    this.showAnnounceNotify(resp.data.data.notification.msgFull || noti.msg);
                                }
                            });
                        }
                        break;
                    case 20:
                    case 21:
                        window.location.href = `/trip/detail/${noti.objIds[0]}`;
                        break;
                    case 40:
                        window.location.href = `/mycars`;
                        break;
                    case 41:
                        window.location.href = `/car/${noti.objIds[0]}`;
                        break;
                    case 60:
                        window.location.href = `/profile/${noti.objIds[0]}`;
                        break;
                    case 80:
                        this.setState({
                            isShowBookingReq: true,
                            bookingReqId: noti.objIds[0]
                        });
                        break;
                    default:
                        break;
                }
            });
        } else {
            switch (noti.type) {
                case 1:
                    if (!noti.alFull || noti.alFull === 0) {
                        getUserNotifyDetail(noti.id).then(resp => {
                            if (resp.data.data && resp.data.data.notification) {
                                this.showAnnounceNotify(resp.data.data.notification.msgFull || noti.msg);
                            }
                        });
                    }
                    break;
                case 20:
                case 21:
                    window.location.href = `/trip/detail/${noti.objIds[0]}`;
                    break;
                case 40:
                    window.location.href = `/mycars`;
                    break;
                case 41:
                    window.location.href = `/car/${noti.objIds[0]}`;
                    break;
                case 60:
                    window.location.href = `/profile/${noti.objIds[0]}`;
                    break;
                case 80:
                    this.setState({
                        isShowBookingReq: true,
                        bookingReqId: noti.objIds[0]
                    });
                    break;
                default:
                    break;
            }
        }
    }

    getNotifyMore() {
        const pos = this.state.notis.length;
        getUserNotify(0, pos, 0).then(resp => {
            if (resp.data.data) {
                var notis = resp.data.data.notifications;
                var more = resp.data.data.more;
                this.setState({
                    notis: this.state.notis.concat(notis),
                    hasMoreNoti: more
                });
            }
            this.setState({
                err: commonErr.SUCCESS
            });
        })
    }

    getUpcommingTripMore() {
        const pos = this.state.upComings.length;
        getUpcommingTrip(0, pos, 0).then(resp => {
            const err = resp.data.error;
            if (resp.data.data) {
                var upComings = resp.data.data;
                var more = resp.data.data.more;
                this.setState({
                    err: err,
                    upComings: {
                        trips: this.state.upComings.trips.concat(upComings.trips),
                        cars: this.state.upComings.cars.concat(upComings.cars),
                        profiles: this.state.upComings.profiles.concat(upComings.profiles)
                    },
                    hasMoreUp: more
                });
            }
        });
    }

    setHasNewAct() {
        if (!this.props.isShowNotify || this.state.active === 1) {
            this.setState({
                hasNewAct: true
            });
        }
    }

    setNeedUpdateNewAct() {
        this.setState({
            needUpdateNewAct: true
        });
    }

    /**
    * Set the wrapper ref
    **/
    setWrapperRef(node) {
        this.wrapperRef = node;
    }

    handleClickOutside(event) {
        if (this.wrapperRef && !this.wrapperRef.contains(event.target)) {
            if (!this.state.isShowAnnouce && !this.state.isShowSysNoti && !this.state.isShowBookingReq) {
                this.props.closeNotify();
            }
        }
    }

    render() {
        var content;
        if (this.state.err === commonErr.LOADING) {
            content = <LoadingInline />
        } else if (this.state.active === 1) {
            const upComings = this.state.upComings;
            if (upComings) {
                const trips = upComings.trips;
                const cars = upComings.cars;
                const profiles = upComings.profiles;
                const loginId = this.props.session.profile.info.uid;

                if (trips && cars && profiles) {
                    var subContent = trips.map(trip => {
                        var car;
                        var owner;
                        var traveler;

                        for (var c = 0; c < cars.length; ++c) {
                            if (cars[c].id === trip.carId) {
                                car = cars[c];
                            }
                        }
                        for (var p = 0; p < profiles.length; ++p) {
                            if (trip.ownerId === profiles[p].uid) {
                                owner = profiles[p];
                            }
                            if (trip.travelerId === profiles[p].uid) {
                                traveler = profiles[p];
                            }
                        }
                        if (trip && car && owner && traveler) {
                            const profile = (loginId === owner.uid) ? traveler : owner;
                            return <li key={trip.id}>
                                <a href={`/trip/detail/${trip.id}`}>
                                    <div className="trip-box trip--upcoming">
                                        <div className="trip-footer">
                                            <div className="status-trips">
                                                <p><span className={getTripStatus(trip.status).class}> </span>{getTripStatus(trip.status).name}</p>
                                            </div>
                                            <div className="time">
                                                <p>{moment(getTripTime(trip)).fromNow()}</p>
                                            </div>
                                        </div>
                                        <div className="trip-header">
                                            <div className="left">
                                                <div className="car-img">
                                                    <div className="fix-img"> <img src={(car.photos && car.photos[0].thumbUrl) ? car.photos[0].thumbUrl : avatar_default} alt="Mioto - Thuê xe tự lái" /></div>
                                                </div>
                                            </div>
                                            <div className="right">
                                                <div className="wrapper">
                                                    <div className="avatar">
                                                        <div className="avatar-img"><img src={`${profile.avatar ? profile.avatar.thumbUrl : avatar_default}`} /></div>
                                                    </div>
                                                    <span>{loginId === owner.uid ? "Khách thuê" : "Chủ xe"} </span>
                                                    <div className="lstitle">{profile.name}</div>
                                                </div>
                                            </div>
                                            <h4>{car.name}</h4>
                                        </div>
                                        <div className="trip-body">
                                            <div className="left">
                                                <p>Bắt đầu: <span>{moment(trip.tripDateFrom).format("HH:mm, dddd, DD/MM/YYYY")}</span></p>
                                                <p>Kết thúc: <span>{moment(trip.tripDateTo).format("HH:mm, dddd, DD/MM/YYYY")}</span></p>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </li>
                        }
                        return null;
                    });

                    if (subContent && subContent.length > 0) {
                        content = <ul className="upcoming">
                            {subContent}
                            {this.state.hasMoreUp === 1 && <li onClick={this.getUpcommingTripMore.bind(this)}>
                                <a className="textAlign-center"><div className="title-label"></div><em className="timer">Xem thêm</em></a>
                            </li>}
                        </ul>
                    } else {
                        content = <ul>
                            <li>
                                <a className="read textAlign-center">Bạn không có thông báo sắp tới nào.</a>
                            </li>
                        </ul>
                    }
                } else {
                    content = <ul>
                        <li>
                            <a className="read textAlign-center">Bạn không có thông báo sắp tới nào.</a>
                        </li>
                    </ul>
                }
            } else {
                content = <ul>
                    <li>
                        <a className="read textAlign-center">Bạn không có thông báo sắp tới nào.</a>
                    </li>
                </ul>
            }
        } else {
            const notis = this.state.notis;
            var newNotifs = [];
            var notifs = [];
            var subContentNew, subContent;
            
            if (notis && notis.length > 0) {
                for(var i = 0; i < notis.length; ++i){
                    if(notis[i].n){
                        newNotifs.push(notis[i]);
                    } else {
                        notifs.push(notis[i]);
                    }
                }

                subContentNew = newNotifs.map(noti =>
                    <li key={noti.id}>
                        <a onClick={() => this.seenNotify(noti)} className={noti.seen ? "" : "read"} >
                            <div className="cover-avatar">
															<img src={noti.iconUrl ? noti.iconUrl : avatar_default} alt="Mioto - Thuê xe tự lái" />
															{noti.subIconUrl && <div className="sub-avatar">
																<img src={noti.subIconUrl} />
															</div>}
                            </div>
                            <span className="desc">
                                <span className="lstitle">{noti.title}</span>
                                <span className="customContentNotify" dangerouslySetInnerHTML={{__html: noti.msg}} />
                                <em className="timer">{moment(noti.timeCreated).fromNow()}</em>
                            </span>
                        </a>
                    </li>
                );

                subContent = notifs.map(noti =>
                    <li key={noti.id}>
                        <a onClick={() => this.seenNotify(noti)} className={noti.seen ? "" : "read"} >
                            <div className="cover-avatar">
                                <img src={noti.iconUrl ? noti.iconUrl : avatar_default} alt="Mioto - Thuê xe tự lái" />
																{noti.subIconUrl && <div className="sub-avatar">
																		<img src={noti.subIconUrl} />
																	</div>}
														</div>
                            <span className="desc">
                                <span className="lstitle">{noti.title}</span>
                                <span className="customContentNotify" dangerouslySetInnerHTML={{__html: noti.msg}} />
                                <em className="timer">{moment(noti.timeCreated).fromNow()}</em>
                            </span>
                        </a>
                    </li>
                )
            } else {
                subContent = <li><a className="read" style={{ textAlign: "center" }}>Bạn không có thông báo nào.</a></li>
            }
            content = <ul>
                {subContent}
                {this.state.hasMoreNoti === 1 && <li onClick={this.getNotifyMore.bind(this)}>
                    <a className="textAlign-center"><div className="title-label" /><em className="timer">Xem thêm</em></a>
                </li>}
            </ul>
        }

        return <div ref={this.setWrapperRef}>
            {this.props.isLogged && <div className={`item-notify ${(this.state.hasNewAct) ? "new" : ""} ${this.props.isShowNotify ? "open" : ""}`}><i onClick={this.toggleNotify} className="ic ic-notify"></i>
                <div className="dropdown-menu">
                    {/* <div className="tabs-notify">
                        <a onClick={this.activeNotis.bind(this)} className={`${this.state.active === 0 ? "active" : ""} ${this.state.hasNewAct ? "new-act" : ""}`}>Hoạt động</a>
                        <a onClick={this.activeUpcomings.bind(this)} className={`${this.state.active === 1 ? "active" : ""} ${this.state.hasNewUp ? "new-coming" : ""}`}>Sắp tới</a>
                    </div> */}
                    {newNotifs && newNotifs.length > 0 && <div className="title-notify"><h6>MỚI</h6></div>} <ul>{subContentNew}</ul>

                    {(newNotifs && newNotifs.length > 0 && this.state.active === 0) && <div className="title-notify"> 
                        <h6>TRƯỚC ĐÓ</h6>
                    </div>}                                                        
                    {content}
                </div>
                {this.state.annouceNoti && <AnnouceBox isShow={this.state.isShowAnnouce} noti={this.state.annouceNoti} hide={this.hideAnnounceNotify.bind(this)} />}
                {this.state.isShowBookingReq && <BookingRequestBox isShow={this.state.isShowBookingReq} hide={this.hideBookingReq.bind(this)} id={this.state.bookingReqId} />}
            </div>}
            {this.state.snoti && <SysNotiBox isShow={this.state.isShowSysNoti} noti={this.state.snoti} hide={this.hideSysNotiBox.bind(this)} />}
            <NotificationBox isAllowedShow={!this.state.isShowAnnouce && !this.state.isShowSysNoti && !this.state.isShowBookingReq} setHasNewAct={this.setHasNewAct} setNeedUpdateNewAct={this.setNeedUpdateNewAct} />
        </div>
    }
}

function mapState(state) {
    return {
        appInit: state.appInit
    }
}

function mapSession(state) {
    return {
        session: state.session
    }
}

Notifications = connect(mapSession)(Notifications);
Notifications = connect(mapState)(Notifications);

export default Notifications;