import React from "react";

export default class LinkFacebook extends React.Component {
    openFbLink = () => {
        window.miotoLink.openPopup('facebook');
        return false;
    }

    render() {
        return (
            <a onClick={this.openFbLink} className="func-edit"><i className="ic ic-link"></i></a>
        );
    }
}