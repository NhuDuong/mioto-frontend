import React from "react"
import { Modal } from "react-bootstrap"

import { commonErr } from "../common/errors"
import { updateEmail, generateEmailOtp, verifyEmail } from "../../model/profile"
import { LoadingInline } from "../common/loading"

const otpSuccessMsg = "Mioto vừa gởi mã OTP vào email của bạn. Vui lòng nhập mã đã nhận vào ô bên dưới để xác minh."

export default class ChangeEmail extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            step: 0, //0: show update 1: show update result 2: show confirm 3: show confirm result
            ip_newemail: null,
            ip_email_otp: "",
            token: "",
            err: commonErr.INNIT,
            errMsg: ""
        }

        this.handleInputChange = this.handleInputChange.bind(this);
        this.closeForm = this.closeForm.bind(this);
    }

    handleInputChange = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        });
    }

    onSubmitUpdateBtnClick = (event) => {
        const self = this;
        const newEmail = this.state.ip_newemail;
        this.setState({
            err: commonErr.LOADING,
            errMsg: ""
        });
        updateEmail(newEmail).then(function (resp) {
            self.setState({
                step: 1,
                err: resp.data.error,
                errMsg: resp.data.errorMessage
            });
        });
    }

    onConfirmBtnClick = (event) => {
        const self = this;
        const newEmail = this.state.ip_newemail;
        self.setState({
            err: commonErr.LOADING,
            errMsg: ""
        });
        generateEmailOtp(newEmail).then(function (resp) {
            if (resp.data.data) {
                self.setState({
                    step: 2,
                    err: resp.data.error,
                    token: resp.data.data.code
                });
            } else {
                self.setState({
                    step: 2,
                    err: resp.data.error,
                    errMsg: resp.data.errorMessage
                });
            }
        });
    }

    onSubmitConfirmBtnClick = (event) => {
        const self = this;
        const newEmail = this.state.ip_newemail;
        const token = this.state.token;
        const otp = this.state.ip_email_otp;
        this.setState({
            err: commonErr.LOADING,
            errMsg: ""
        });
        verifyEmail(newEmail, token, otp).then(function (resp) {
            self.setState({
                step: 3,
                err: resp.data.error,
                errMsg: resp.data.errorMessage
            });
        });
    }

    onBackUpdateBtnClick() {
        this.setState({
            step: 0,
            err: commonErr.INNIT,
            errMsg: ""
        });
    }

    onRetryBtnClick() {
        this.setState({
            step: 2,
            err: commonErr.INNIT,
            errMsg: "",
        });
    }

    onBackBtnClick() {
        this.setState({
            step: 0,
            err: commonErr.INNIT,
            errMsg: ""
        });
    }

    closeForm() {
        this.setState({
            step: 0,
            err: commonErr.INNIT,
            errMsg: ""
        });
        this.props.getProfile();
        this.props.hideModal();
    }

    render() {
        var content;
        if (this.state.step === 3) {
            if (this.state.err >= commonErr.SUCCESS) {
                content = <div className="form-default form-s">
                    <div className="line-form">
                        <div className="textAlign-center"><i className="ic ic-verify"></i> Xác minh email thành công.</div>
                    </div>
                    <div className="clear"></div>
                    <div className="space m"></div>
                    <button className="btn btn-primary btn--m" type="button" onClick={this.closeForm}>Hoàn tất</button>
                </div>
            } else {
                content = <div className="form-default form-s">
                    {this.state.err === commonErr.LOADING && <LoadingInline />}
                    <div className="line-form">
                        <div className="textAlign-center"><i className="ic ic-error"></i> Xác minh email thất bại. {this.state.errMsg}</div>
                    </div>
                    <div className="clear"></div>
                    <div className="space m"></div>
                    <a className="btn btn-primary btn--m" type="button" onClick={this.onRetryBtnClick.bind(this)}>Thử lại</a>
                </div>
            }
        } else if (this.state.step === 2) {
            if (this.state.err >= commonErr.SUCCESS) {
                content = <div className="form-default form-s">
                    {this.state.err === commonErr.LOADING && <LoadingInline />}
                    <div className="line-form">
                        <div className="textAlign-center"><i className="ic ic-verify"></i> {otpSuccessMsg}</div>
                    </div>
                    <div className="line-form">
                        <div className="wrap-input">
                            <input type="text" name="ip_email_otp" placeholder="Nhập mã OTP từ email" onChange={this.handleInputChange} />
                        </div>
                    </div>
                    <div className="clear"></div>
                    <div className="space m"></div>
                    <p className="textAlign-center has-more-btn">
                        <a className="btn btn-primary btn--m" type="button" onClick={this.onSubmitConfirmBtnClick.bind(this)}>Xác minh</a>
                    </p>
                    <p style={{ float: "right" }}>Không nhận được mã OTP. <a onClick={this.onConfirmBtnClick.bind(this)}>Yêu cầu gởi lại</a></p>
                </div>
            } else {
                content = <div className="form-default form-s">
                    <div className="line-form">
                        <div className="textAlign-center"><i className="ic ic-error"></i> Không gởi được OTP. {this.state.errMsg}</div>
                    </div>
                    <div className="clear"></div>
                    <div className="space m"></div>
                    <a className="btn btn-primary btn--m" type="button" onClick={this.onConfirmBtnClick.bind(this)}>Gởi lại OTP</a>
                </div>
            }
        } else if (this.state.step === 1) {
            if (this.state.err >= commonErr.SUCCESS) {
                content = <div className="form-default form-s">
                    {this.state.err === commonErr.LOADING && <LoadingInline />}
                    <div className="line-form">
                        <div className="textAlign-center"><i className="ic ic-verify"></i> Cập nhật email thành công.</div>
                    </div>
                    <div className="clear"></div>
                    <div className="space m"></div>
                    <a className="btn btn-primary btn--m" type="button" onClick={this.onConfirmBtnClick.bind(this)}>Xác minh ngay</a>
                </div>
            } else {
                content = <div className="form-default form-s">
                    <div className="line-form">
                        <div className="textAlign-center"><i className="ic ic-error"></i> Cập nhật thất bại. {this.state.errMsg}</div>
                    </div>
                    <div className="clear"></div>
                    <div className="space m"></div>
                    <button className="btn btn-primary btn--m" type="button" onClick={this.onBackBtnClick.bind(this)}>Thử lại</button>
                </div>
            }
        } else {
            content = <div className="form-default form-s">
                {this.state.err === commonErr.LOADING && <LoadingInline />}
                <div className="line-form">
                    <div className="wrap-input">
                        <input type="text" name="ip_newemail" placeholder="Email mới" onChange={this.handleInputChange} />
                    </div>
                </div>
                <div className="clear"></div>
                <div className="space m"></div>
                <button className="btn btn-primary btn--m" type="button" onClick={this.onSubmitUpdateBtnClick.bind(this)}>Cập nhật</button>
            </div>
        }

        return <Modal
            show={this.props.show}
            onHide={this.closeForm}
            dialogClassName="modal-sm modal-dialog"
        >
            <Modal.Header closeButton={true} closeLabel={""}>
                <Modal.Title>Cập nhật email</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                {content}
            </Modal.Body>
        </Modal>
    }
}
