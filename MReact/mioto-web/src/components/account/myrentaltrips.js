import React from "react"
import moment from 'moment'
import { Link } from 'react-router-dom'
import InfiniteScroll from "react-infinite-scroller"
import { StickyContainer, Sticky } from 'react-sticky'

import Header from "../common/header"
import { LoadingPage, LoadingInline } from "../common/loading"
import { commonErr } from "../common/errors"
import { getOwnerTrips } from "../../model/car"
import { MessagePage } from "../common/messagebox"
import { formatPrice, getTripStatus } from "../common/common"
import Session from "../common/session"

import avatar_default from "../../static/images/avatar_default.png"

class TripItem extends React.Component {
    render() {
        const car = this.props.car;
        const trip = this.props.trip;
        const renter = this.props.renter;

        const status = getTripStatus(trip.status);

        return <div className="trip-box">
            <div className="trip-header">
                <div className="left">
                    <div className="car-img">
                        <Link to={{ pathname: `/trip/detail/${trip.id}` }}>
                            <div className="fix-img"> <img src={`${car.photos ? car.photos[0].thumbUrl : ""}`} alt={`Cho thuê xe tự lái ${car.name}`} /></div>
                        </Link>
                    </div>
                </div>
                <div className="right">
                    <h4>{car.name}</h4>
                </div>
            </div>
            <div className="space m"></div>
            <div className="trip-body">
                <div className="left">
                    <p className="total-price">
                        <i className="ic ic-total-price" />Tổng tiền {formatPrice(trip.priceSummary.priceTotal)}</p>
                    <p> Bắt đầu: {moment(trip.tripDateFrom).format("HH:mm dddd, DD/MM/YYYY")}</p>
                    <p> Kết thúc: {moment(trip.tripDateTo).format("HH:mm dddd, DD/MM/YYYY")}</p>
                </div>
                <div className="right">
                    <div className="avatar">
                        <div className="avatar-img">
                            <Link to={`/profile/${renter.uid}`}>
                                <img src={`${(renter.avatar && renter.avatar.thumbUrl) ? renter.avatar.thumbUrl : avatar_default}`} alt="Mioto - Thuê xe tự lái" />
                            </Link>
                        </div>
                    </div>
                    <div className="lstitle">{renter.name}</div>
                </div>
            </div>
            <Link to={{ pathname: `/trip/detail/${trip.id}` }}>
                <div className="trip-footer">
                    <div className="status-trips">
                        <p><span className={status.class} /> {status.name}</p>
                    </div>
                    <div className="time">
                        <p>{moment(trip.timeBooked).fromNow()}</p>
                    </div>
                </div>
            </Link>
        </div>
    }
}

class MyRentalTrips extends React.Component {
    constructor() {
        super();
        this.state = {
            err: commonErr.INNIT,
            filterStatus: 0,
            trips: [],
            cars: [],
            profiles: []
        }
    }

    getTrips(stt) {
        const self = this;
        self.setState({
            err: commonErr.LOADING
        });
        getOwnerTrips(stt, 0, 0, 0).then(function (resp) {
            if (resp.data.error >= commonErr.SUCCESS && resp.data.data.trips) {
                self.setState({
                    trips: resp.data.data.trips,
                    cars: resp.data.data.cars,
                    profiles: resp.data.data.profiles,
                    more: resp.data.data.more,
                    err: resp.data.error
                });
            } else {
                self.setState({
                    err: resp.data.error,
                    trips: null,
                    more: 0
                });
            }
        });
    }

    componentDidMount() {
        window.scrollTo(0, 0);
        //
        this.setState({
            err: commonErr.LOADING
        });
        this.getTrips(0);
    }

    getTripsMore() {
        const nextPos = this.state.trips.length;
        getOwnerTrips(this.state.filterStatus, 0, nextPos, 0).then((resp) => {
            if (resp.data.error >= commonErr.SUCCESS && resp.data.data.trips
                && resp.data.data.cars && resp.data.data.profiles) {
                this.setState({
                    trips: this.state.trips.concat(resp.data.data.trips),
                    cars: this.state.cars.concat(resp.data.data.cars),
                    profiles: this.state.profiles.concat(resp.data.data.profiles),
                    more: resp.data.data.more,
                    err: resp.data.error
                });
            } else {
                this.setState({
                    err: resp.data.error,
                    more: 0
                });
            }
        });
    }

    onStatusFilterChange(event) {
        window.scrollTo(0, 0);
        //
        const filterStatus = event.target.value;
        this.setState({
            filterStatus: filterStatus
        });
        this.getTrips(filterStatus);
    }

    onStatusFilterReset() {
        window.scrollTo(0, 0);
        //
        this.setState({
            filterStatus: 0
        });
        this.getTrips(0);
    }

    render() {
        var content;
        if (this.state.err === commonErr.LOADING) {
            content = <LoadingPage />
        } else if (this.state.err >= commonErr.SUCCESS
            && this.state.trips && this.state.trips.length > 0
            && this.state.cars && this.state.cars.length > 0
            && this.state.profiles && this.state.profiles.length > 0) {

            const trips = this.state.trips;
            const cars = this.state.cars;
            const profiles = this.state.profiles;

            var i, j, k;
            const tripsInfo = [];

            for (i = 0; i < trips.length; ++i) {
                const trip = trips[i];
                for (j = 0; j < cars.length; ++j) {
                    const car = cars[j];
                    if (car.id === trip.carId) {
                        for (k = 0; k < profiles.length; ++k) {
                            const profile = profiles[k];
                            if (profile.uid === trip.travelerId) {
                                tripsInfo.push({ trip, car, profile });
                                break;
                            }
                        }
                        break;
                    }
                }
            }

            content = <InfiniteScroll pageStart={0}
                loadMore={this.getTripsMore.bind(this)}
                hasMore={this.state.more === 1}
                loader={<ul key={0}><LoadingInline /></ul>}>
                {tripsInfo.map(tripInfo => <TripItem key={tripInfo.trip.id} trip={tripInfo.trip} car={tripInfo.car} renter={tripInfo.profile} />)}
            </InfiniteScroll>
        } else {
            content = <MessagePage message={"Không tìm thấy chuyến nào."} />
        }

        return <div className="mioto-layout bg-gray">
            <Session />
            <Header />
            <StickyContainer>
                <section className="body">
                    <Sticky disableCompensation>
                        {({
                            isSticky,
                            wasSticky,
                            style,
                            distanceFromTop,
                            distanceFromBottom,
                            calculatedHeight
                        }) => {
                            return <div className="filter-trips show-on-small" style={{ ...style }}>
                                <div className="content-filter">
                                    <div className="rent-car">
                                        <div className="line-form">
                                            <label className="label">Trạng Thái</label>
                                            <div className="wrap-select">
                                                <select onChange={this.onStatusFilterChange.bind(this)} value={this.state.filterStatus}>
                                                    <option value="0">Tất cả</option>
                                                    <option value="1">Chờ duyệt</option>
                                                    <option value="2">Từ chối</option>
                                                    <option value="3">Chờ đặt cọc</option>
                                                    <option value="4">Đã đặt cọc</option>
                                                    <option value="5">Đã huỷ</option>
                                                    <option value="6">Đang cho thuê</option>
                                                    <option value="7">Đã kết thúc</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        }}
                    </Sticky>
                    <div className="body has-filter">
                        <Sticky disableCompensation>
                            {({
                                isSticky,
                                wasSticky,
                                style,
                                distanceFromTop,
                                distanceFromBottom,
                                calculatedHeight
                            }) => {
                                return <div className="filter-trips" style={{ ...style, width: "325px" }}>
                                    <div className="content-filter">
                                        <div className="rent-car">
                                            <div className="line-form"><label className="label">Trạng thái</label>
                                                <div className="wrap-select">
                                                    <select onChange={this.onStatusFilterChange.bind(this)} value={this.state.filterStatus}>
                                                        <option value="0">Tất cả</option>
                                                        <option value="1">Chờ duyệt</option>
                                                        <option value="2">Từ chối</option>
                                                        <option value="3">Chờ đặt cọc</option>
                                                        <option value="4">Đã đặt cọc</option>
                                                        <option value="5">Đã huỷ</option>
                                                        <option value="6">Đang cho thuê</option>
                                                        <option value="7">Đã kết thúc</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            }}
                        </Sticky>
                        <div className="module-map module-trips min-height-no-footer">
                            {content}
                        </div>
                    </div>
                </section>
            </StickyContainer>
        </div>
    }
}

export default MyRentalTrips;