import React from "react"

import { Modal } from "react-bootstrap"
import HtmlToReactParser from "html-to-react"
import moment from 'moment'

import Header from "../common/header"
import Footer from "../common/footer"
import { commonErr } from "../common/errors"
import { formatPrice } from "../common/common"
import { getMyPromo, searchPromo } from "../../model/promo"
import { LoadingInline } from "../common/loading"
import Session from "../common/session"

import promo_cover from "../../static/images/img_car.jpg"

export default class MyPromo extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            err: commonErr.INNIT,
            searchCode: props.match.params.code || ""
        }
    }

    componentDidMount() {
        this.setState({
            err: commonErr.LOADING
        });
        getMyPromo(0, 0, 0).then(myCodeResp => {
            if (myCodeResp.data.error >= commonErr.SUCCESS) {
                const searchCode = this.state.searchCode;
                if (searchCode && searchCode !== "") {
                    searchPromo(searchCode, 0).then(searchResp => {
                        if (searchResp.data.error >= commonErr.SUCCESS) {
                            this.setState({
                                searchCode: searchCode,
                                err: searchResp.data.error,
                                codes: searchResp.data.data.codes,
                                myCodes: myCodeResp.data.data.codes
                            });
                        } else {
                            this.setState({
                                searchCode: searchCode,
                                err: searchResp.data.error,
                                myCodes: myCodeResp.data.data.codes,
                                codes: null
                            });
                        }
                    });
                } else {
                    this.setState({
                        err: myCodeResp.data.error,
                        myCodes: myCodeResp.data.data.codes
                    });
                }
            } else {
                this.setState({
                    err: myCodeResp.data.error,
                    myCodes: null
                });
            }
        });
    }

    onSearchCodeChange(event) {
        clearTimeout(this.timer);
        const searchCode = event.target.value;
        this.setState({
            searchCode: searchCode
        });
        this.timer = setTimeout(() => this.searchCode(searchCode), 500);
    }

    searchCode(searchCode) {
        if (!searchCode || searchCode === "") {
            return;
        }

        this.setState({
            err: commonErr.LOADING
        });
        searchPromo(searchCode, 0).then(resp => {
            if (resp.data.error >= commonErr.SUCCESS) {
                this.setState({
                    searchCode: searchCode,
                    err: resp.data.error,
                    codes: resp.data.data.codes
                });
            } else {
                this.setState({
                    searchCode: searchCode,
                    err: resp.data.error,
                    codes: null
                });
            }
        });
    }

    showPromoCodeDetail(code) {
        this.setState({
            activeCode: code
        });
    }

    hidePromoCodeDetail() {
        this.setState({
            activeCode: null
        });
    }

    render() {
        var content;
        var htmlToReactParser = new HtmlToReactParser.Parser();

        if (this.state.err === commonErr.INNIT || this.state.err === commonErr.LOADING) {
            content = <LoadingInline />
        } else if (!this.state.searchCode || this.state.searchCode === "") {
            if (!this.state.myCodes || this.state.myCodes.length === 0) {
                content = <p style={{ color: "gray" }}>Không tìm thấy khuyến mãi.</p>
            } else {
                content = this.state.myCodes.map(code => <div onClick={() => this.showPromoCodeDetail(code)} key={code.id} className="box-promo d-flex">
                    <div className="top">
                        <img className="img-promo" src={code.thumb} alt="Mioto - Thuê xe tự lái" />
                    </div>
                    {code.discountPercent > 0 ? <div className="center">
                        <p className="code">{code.code}</p>
                        <p className="desc">Giảm<span> {code.discountPercent}%</span></p>
                        <p className="desc">(tối đa<span> {formatPrice(code.discountMax)}</span>)</p>
                    </div> : <div className="center">
                            <p className="code">{code.code}</p>
                            <p className="desc">Giảm<span> {formatPrice(code.discountMax)}</span></p>
                        </div>}
                </div>)
            }
        } else {
            if (!this.state.codes || this.state.codes.length === 0) {
                content = <p style={{ color: "gray" }}>Không tìm thấy mã khuyến mãi.</p>
            } else {
                content = this.state.codes.map(code => <div onClick={() => this.showPromoCodeDetail(code)} key={code.id} className="box-promo d-flex">
                    <div className="top">
                        <img className="img-promo" src={code.thumb} alt="Mioto - Thuê xe tự lái" />
                    </div>
                    {code.discountPercent > 0 ? <div className="center">
                        <p className="code">{code.code}</p>
                        <p className="desc">Giảm<span> {code.discountPercent}%</span></p>
                        <p className="desc">(tối đa<span> {formatPrice(code.discountMax)}</span>)</p>
                    </div> : <div className="center">
                            <p className="code">{code.code}</p>
                            <p className="desc">Giảm<span> {formatPrice(code.discountMax)}</span></p>
                        </div>}
                </div>)
            }
        }

        return <div className="mioto-layout">
            <Session />
            <Header />
            <section className="body min-height">
                <div className="section_promo-title">
                    <div className="m-container">
                        <h4>Khuyến mãi của tôi</h4>
                        <div className="form-default">
                            <div className="line-form">
                                <div className="wrap-input has-ico-search"> <i className="ic ic-search"></i>
                                    <input type="text" placeholder="Nhập mã khuyến mãi" value={this.state.searchCode} onChange={this.onSearchCodeChange.bind(this)} style={{ textTransform: "uppercase" }} />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="module-promo m-container d-flex">
                    {content}
                </div>
                {this.state.activeCode && <Modal
                    show={this.state.activeCode}
                    onHide={this.hidePromoCodeDetail.bind(this)}
                    dialogClassName="modal-sm modal-dialog"
                >
                    <Modal.Header closeButton={true} closeLabel={""}>
                        <Modal.Title>Mã khuyến mãi</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <div className="body-promo">
                            {this.state.activeCode.desc && <div class="promo-details">
                                <div class="promo-header">
                                    <div class="cover-promo" style={{ backgroundImage: `url(${promo_cover})` }}> </div>
                                    <div class="cover-detail">
                                        <div class="img-promo"> <img src={this.state.activeCode.thumb} alt="Mioto - Thuê xe tự lái" /></div>
                                        <div class="desc">
                                            <h4>{this.state.activeCode.code}</h4>
                                            <p> Áp dụng từ <span class="green">{moment(this.state.activeCode.validDateFrom).format("DD/MM/YYYY")} </span>đến <span class="green">{moment(this.state.activeCode.validDateTo).format("DD/MM/YYYY")} </span></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="promo-body">
                                    {htmlToReactParser.parse(this.state.activeCode.desc)}
                                </div>
                            </div>}
                            {!this.state.activeCode.desc && <div>Không có mô tả</div>}
                        </div>
                    </Modal.Body>
                </Modal>}
            </section>
            <Footer />
        </div>
    }
}