import React from "react"
import { Modal } from "react-bootstrap"

import { commonErr } from "../common/errors"
import { generateEmailOtpForgot, resetPasswordByEmail } from "../../model/profile"
import { LoadingInline } from "../common/loading"

const otpSuccessMsg = "Mioto vừa gởi mã OTP vào email của bạn. Vui lòng nhập mã đã nhận vào ô bên dưới để xác minh.";

export default class ResetPwByEmail extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            step: 0,
            ip_email: props.email,
            ip_otp: "",
            token: "",
            err: commonErr.INNIT,
            errMsg: ""
        }

        this.handleInputChange = this.handleInputChange.bind(this);
    }

    handleInputChange = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        });
    }

    componentDidMount() {
        const self = this;
        const email = this.props.email;
        if (email) {
            this.setState({
                err: commonErr.LOADING,
                errMsg: "",
                ip_email: email
            });
            generateEmailOtpForgot(email).then(function (resp) {
                if (resp.data.data) {
                    self.setState({
                        step: 0,
                        err: resp.data.error,
                        token: resp.data.data.code
                    });
                } else {
                    self.setState({
                        step: 0,
                        err: resp.data.error,
                        errMsg: resp.data.errorMessage
                    });
                }
            });
        }
    }

    onConfirmBtnClick() {
        const email = this.props.email;
        this.setState({
            err: commonErr.LOADING,
            errMsg: ""
        });
        generateEmailOtpForgot(email).then(resp => {
            if (resp.data.data) {
                this.setState({
                    step: 0,
                    err: resp.data.error,
                    token: resp.data.data.code
                });
            } else {
                this.setState({
                    step: 0,
                    err: resp.data.error,
                    errMsg: resp.data.errorMessage
                });
            }
        });
    }

    onRetryBtnClick() {
        this.setState({
            step: 0,
            err: commonErr.INNIT,
            errMsg: "",
        });
    }

    onSubmitConfirmBtnClick = () => {
        const email = this.state.ip_email;
        const otp = this.state.ip_otp;
        const token = this.state.token;
        const pw = this.state.ip_pw;
        const pwConfirm = this.state.ip_pw_confirm;

        if (!otp || otp === "") {
            this.setState({
                err: commonErr.FAIL,
                errMsg: "Bạn chưa nhập OTP."
            });
        } else if (!pw || pw === "") {
            this.setState({
                err: commonErr.FAIL,
                errMsg: "Bạn chưa nhập mật khẩu."
            });
        } else if (pw !== pwConfirm) {
            this.setState({
                err: commonErr.FAIL,
                errMsg: "Mật khẩu lặp lại không đúng."
            });
        } else {
            this.setState({
                err: commonErr.LOADING,
                errMsg: ""
            });
            resetPasswordByEmail(email, token, otp, pw).then(function (resp) {
                this.setState({
                    step: 1,
                    err: resp.data.error,
                    errMsg: resp.data.errorMessage
                });
            });
        }
    }

    closeForm() {
        this.props.hideModal();
        this.props.showLoginModal();
    }

    render() {
        var content;
        if (this.state.step === 1) {
            if (this.state.err < commonErr.SUCCESS) {
                content = <div className="form-default form-s">
                    {this.state.err === commonErr.LOADING && <LoadingInline />}
                    <div className="line-form">
                        <div className="textAlign-center"><i className="ic ic-error"></i> Cập nhật thất bại. {this.state.errMsg}</div>
                    </div>
                    <div className="clear"></div>
                    <div className="space m"></div>
                    <a className="btn btn-primary btn--m" type="button" onClick={this.onRetryBtnClick.bind(this)}>Thử lại</a>
                </div>
            } else {
                content = <div className="form-default form-s">
                    {this.state.err === commonErr.LOADING && <LoadingInline />}
                    <div className="line-form">
                        <div className="textAlign-center"><i className="ic ic-verify"></i> Mật khẩu mới đã được cập nhật thành công.</div>
                    </div>
                    <div className="clear"></div>
                    <div className="space m"></div>
                    <a className="btn btn-primary btn--m" type="button" onClick={this.closeForm.bind(this)}>Đăng nhập</a>
                </div>
            }
        } else {
            if (this.state.err === commonErr.LOADING) {
                content = <div className="form-default form-s">
                    <LoadingInline />
                </div>
            } else {
                content = <div className="form-default form-s">
                    {this.state.err >= commonErr.SUCCESS && <div className="line-form">
                        <div className="textAlign-center"><i className="ic ic-verify"></i> {otpSuccessMsg}</div>
                    </div>}
                    {this.state.err < commonErr.SUCCESS && <div className="line-form">
                        <div className="textAlign-center"><i className="ic ic-error"></i> {this.state.errMsg}</div>
                    </div>}
                    <div className="line-form">
                        <div className="wrap-input">
                            <input type="text" name="ip_otp" value={this.state.ip_otp} placeholder="Nhập mã OTP từ email" onChange={this.handleInputChange} />
                        </div>
                    </div>
                    <div className="line-form">
                        <div className="wrap-input has-ico">
                            <i className="ic ic-lock-fill"></i>
                            <input type="password" name="ip_pw" value={this.state.ip_pw} placeholder="Mật khẩu mới" onChange={this.handleInputChange} />
                        </div>
                    </div>
                    <div className="line-form">
                        <div className="wrap-input has-ico">
                            <i className="ic ic-lock-fill"></i>
                            <input type="password" name="ip_pw_confirm" value={this.state.ip_pw_confirm} placeholder="Lặp lại mật khẩu mới" onChange={this.handleInputChange} />
                        </div>
                    </div>
                    <div className="clear"></div>
                    <div className="space m"></div>
                    <p className="textAlign-center has-more-btn">
                        <a className="btn btn-primary btn--m" type="button" onClick={this.onSubmitConfirmBtnClick.bind(this)}>Cập nhật</a>
                    </p>
                    <p style={{ float: "right" }}>Bạn không nhận được mã OTP. <a onClick={this.onConfirmBtnClick.bind(this)}>Yêu cầu gởi lại</a></p>
                </div>
            }
        }

        return <Modal
            show={this.props.show}
            onHide={this.props.hideModal}
            dialogClassName="modal-sm modal-dialog"
        >
            <Modal.Header closeButton={true} closeLabel={""}>
                <Modal.Title>Quên mật khẩu</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                {content}
            </Modal.Body>
        </Modal>
    }
}