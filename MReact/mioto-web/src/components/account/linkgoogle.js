import React from "react";

export default class LinkGoogle extends React.Component {
    openGgLink = () => {
        window.miotoLink.openPopup('google');
        return false;
    }

    render() {
        return (
            <a onClick={this.openGgLink} className="func-edit"><i className="ic ic-link"></i></a>
        );
    }
}