import React from "react"
import axios from "axios"
import moment from "moment"
import { connect } from "react-redux"
import { Redirect } from "react-router-dom"

import Header from "../common/header"
import Footer from "../common/footer"
import AccountKit from "../common/accountkit"
import VerifyEmail from "./verifyemail"
import ChangeInfo from "./changeinfo"
import ChangePhone from "./changephone"
import ChangeEmail from "./changeemail"
import ChangeAvatar from "./changeavatar"
import ChangePaper from "./changepaper"
import ProfileCommentBox from "../profile/profilecommentbox"
import LinkFacebook from "../account/linkfacebook"
import UnLinkFacebook from "../account/unlinkfacebook"
import UnLinkGoogle from "../account/unlinkgoogle"
import LinkGoogle from "../account/linkgoogle"
import PropertiesBox from "../profile/properties"
import { MessageBox } from "../common/messagebox"
import { fbappInfo } from "../common/fbdeveloper"
import { logout, getLoggedProfile } from "../../actions/sessionAct"
import { verifyPhone } from "../../model/profile"
import { LoadingPage } from "../common/loading"
import { commonErr } from "../common/errors"
import { genderMap } from "../common/common"

import avatar_default from "../../static/images/avatar_default.png"
import def_profile_cover from "../../static/images/def_profile_cover.jpg"

class Account extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showChangeInfo: false,
            showChangeAvatar: false,
            showChangePaper: false,
            showMessageBox: false,
            showChangePhone: false,
            showChangeEmail: false,
            showVerifyEmail: false,
            err: commonErr.INNIT,
            errMsg: "",
            reviewType: 0,
            verifyPhoneNumber: "",
            verifyPhoneAccessTok: ""
        }

        this.getProfile = this.getProfile.bind(this);
        this.logout = this.logout.bind(this);
        this.showChangeInfo = this.showChangeInfo.bind(this);
        this.hideChangeInfo = this.hideChangeInfo.bind(this);
        this.showChangePhone = this.showChangePhone.bind(this);
        this.hideChangePhone = this.hideChangePhone.bind(this);
        this.showChangeEmail = this.showChangeEmail.bind(this);
        this.hideChangeEmail = this.hideChangeEmail.bind(this);
        this.showVerifyEmail = this.showVerifyEmail.bind(this);
        this.hideVerifyEmail = this.hideVerifyEmail.bind(this);
        this.showMessageBox = this.showMessageBox.bind(this);
        this.hideMessageBox = this.hideMessageBox.bind(this);
        this.showChangeAvatar = this.showChangeAvatar.bind(this);
        this.hideChangeAvatar = this.hideChangeAvatar.bind(this);
        this.showChangePaper = this.showChangePaper.bind(this);
        this.hideChangePaper = this.hideChangePaper.bind(this);
        this.onAccountKitPhoneLoginResp = this.onAccountKitPhoneLoginResp.bind(this);
        this.updatePaper = this.updatePaper.bind(this);
    }

    componentDidMount() {
        if (!this.props.session.profile.info || !this.props.session.profile.info.uid) {
            this.getProfile();
        }
        window.scrollTo(0, 0);
    }

    getProfile() {
        this.props.dispatch(getLoggedProfile());
    }

    logout() {
        this.props.dispatch(logout());
    }

    showChangeInfo() {
        this.setState({
            showChangeInfo: true
        });
    }

    hideChangeInfo() {
        this.setState({
            showChangeInfo: false
        });
    }

    showChangeAvatar() {
        this.setState({
            showChangeAvatar: true
        });
    }

    hideChangeAvatar() {
        this.setState({
            showChangeAvatar: false
        });
    }

    showChangePaper() {
        this.setState({
            showChangePaper: true
        });
    }

    hideChangePaper() {
        this.setState({
            showChangePaper: false
        });
    }

    showChangePhone() {
        this.setState({
            showChangePhone: true
        });
    }

    hideChangePhone() {
        this.setState({
            showChangePhone: false
        });
    }

    showChangeEmail() {
        this.setState({
            showChangeEmail: true
        });
    }

    hideChangeEmail() {
        this.setState({
            showChangeEmail: false
        });
    }

    showVerifyEmail() {
        this.setState({
            showVerifyEmail: true
        });
    }

    hideVerifyEmail() {
        this.setState({
            showVerifyEmail: false
        });
    }

    reviewTypeChange(event) {
        this.setState({
            reviewType: event.target.value
        });
    }

    showMessageBox() {
        this.setState({
            showMessageBox: true
        });
    }

    hideMessageBox() {
        this.setState({
            showMessageBox: false
        });
    }

    onAccountKitPhoneLoginResp(resp) {
        var verifyPhoneAccessTok;
        var verifyPhoneNumber;

        if (resp.code) {
            axios({
                method: "GET",
                url: `${fbappInfo.getAccessTokBaseDomain}?grant_type=authorization_code&code=${resp.code}&access_token=AA|${fbappInfo.appId}|${resp.state}`
            }).then(tokenData => {
                if (tokenData.data) {
                    verifyPhoneAccessTok = tokenData.data.access_token;
                    if (verifyPhoneAccessTok) {
                        axios({
                            method: "GET",
                            url: `${fbappInfo.getLoggedInfoBaseDomain}/?access_token=${verifyPhoneAccessTok}`
                        }).then(accountData => {
                            if (accountData.data && accountData.data.phone) {
                                verifyPhoneNumber = accountData.data.phone.number;
                                if (verifyPhoneNumber) {
                                    verifyPhone(verifyPhoneNumber, verifyPhoneAccessTok, 1).then(resp => {
                                        this.setState({
                                            err: resp.data.error,
                                            errMsg: resp.data.errorMessage,
                                            showMessageBox: true
                                        });
                                    });
                                }
                            }
                        })
                    }
                }
            })
        }
    }

    updatePaper(paper) {
        this.setState({ paper: paper });
        this.showChangePaper();
    }

    render() {
        var content;
        const profile = this.props.session.profile.info;
        if (this.props.session.profile.err === commonErr.INNIT
            || this.props.session.profile.err === commonErr.LOADING) {
            content = <LoadingPage />
        } else if (profile && profile.uid) {
            content = <section className="body min-height">
                <div className="cover-profile" style={{ backgroundImage: `url(${def_profile_cover})` }}></div>
                <div className="module-profile">
                    <div className="profile-container">
                        <div className="content-profile">
                            <div onClick={this.showChangeAvatar} className="avatar avatar--xl has-edit" title="title name">
                                <div className="avatar-img" style={{ backgroundImage: `url(${(profile && profile.avatar && profile.avatar.fullUrl) ? profile.avatar.fullUrl : avatar_default})` }}></div>
                            </div>
                            <div className="desc-top">
                                <h2 className="name-car">TÀI KHOẢN CỦA TÔI</h2>
                                <div className="snippet">
                                    <div className="item-title">
                                        <span>{profile ? profile.name : ""}<a className="func-edit" onClick={this.showChangeInfo} title="Edit"><i className="ic ic-edit"></i></a></span>
                                    </div>
                                </div>
                            </div>
                            <div className="clear" />
                            <div className="wr-s-info">
                                <div className="s-info"><span className="lstitle">Tổng chuyến</span>{profile.totalTrips ? <span className="ctn">{profile.totalTrips} chuyến</span> : "_"}</div>
                                <div className="s-info"><span className="lstitle">Đánh giá</span>{profile.totalReview ? <span className="ctn">{profile.totalReview} nhận xét</span> : "_"}</div>
                            </div>
                            <div className="information">
                                <ul>
                                    <li><span className="label">Ngày sinh</span><span className="ctn">{profile.dob !== 0 ? moment(profile.dob).format("DD/MM/YYYY") : ""}</span></li>
                                    <li><span className="label">Giới tính</span><span className="ctn">{profile.gender ? genderMap[profile.gender] : "Nam"}</span></li>
                                    <li><div className="space m"></div></li>
                                    <li>
                                        <span className="label">Điện thoại</span><span className="ctn">{profile.phoneNumber && (profile.phoneVerified !== 0)
                                            && <a> <i className="ic ic-verify"></i> </a>} {profile.phoneNumber ? profile.phoneNumber : ""}
                                            {profile.phoneNumber && (profile.phoneVerified) === 0 && <AccountKit
                                                appId={fbappInfo.appId}
                                                csrf={fbappInfo.csrf}
                                                version={fbappInfo.version}
                                                language={fbappInfo.language}
                                                phoneNumber={profile.phoneNumber ? profile.phoneNumber.replace("+84", "") : null}
                                                onResponse={(resp) => this.onAccountKitPhoneLoginResp(resp)}
                                            >
                                                {p => <a {...p}> <p>[Click để xác minh]</p></a>}
                                            </AccountKit>}
                                            <a className="func-edit" ><i className="func-edit ic ic-edit" onClick={this.showChangePhone}></i></a>
                                            <ChangePhone show={this.state.showChangePhone} hideModal={this.hideChangePhone} getProfile={this.getProfile} />
                                        </span>
                                    </li>
                                    <li>
                                        <span className="label">Email</span><span className="ctn">{profile.email && (profile.emailVerified !== 0) && <a> <i className="ic ic-verify"></i></a>} {profile.email ? profile.email : ""}
                                            {profile.email && (profile.emailVerified === 0) && <a onClick={this.showVerifyEmail}> <p>[Click để xác minh]</p></a>}
                                            <a className="func-edit"><i className="ic ic-edit" onClick={this.showChangeEmail}></i></a>
                                            <ChangeEmail show={this.state.showChangeEmail} hideModal={this.hideChangeEmail} getProfile={this.getProfile} />
                                            {this.showVerifyEmail && <VerifyEmail email={profile.email} show={this.state.showVerifyEmail} hideModal={this.hideVerifyEmail} getProfile={this.getProfile} />}
                                        </span>
                                    </li>
                                    <li><span className="label">Facebook</span><span className="ctn">{profile && profile.facebookName ? <span> {profile.facebookName} <UnLinkFacebook getProfile={this.getProfile} /> </span> : <LinkFacebook />}  {profile.facebookFr > 0 && <p>{`(có ${profile.facebookFr} bạn)`}</p>}</span></li>
                                    <li><span className="label">Google</span><span className="ctn">{profile && profile.googleName ? <span> {profile.googleName} <UnLinkGoogle getProfile={this.getProfile} /> </span> : <LinkGoogle />} </span></li>
                                    <li><div className="space m" /></li>
                                </ul>
                            </div>
                            <div className="clear"></div>
                            <PropertiesBox profile={profile} />
                        </div>
                        <div className="sidebar-profile">
                            <div className="head-content">
                                <span className="lstitle">Nhận xét</span>
                                <span className="wrap-select">
                                    <select defaultValue="0" onChange={this.reviewTypeChange.bind(this)}>
                                        <option value="0">Từ khách thuê</option>
                                        <option value="1">Từ chủ xe</option>
                                    </select>
                                </span>
                            </div>
                            <ProfileCommentBox profile={profile} reviewType={this.state.reviewType} />
                        </div>
                        <div className="clear"></div>
                    </div>
                </div>
                <MessageBox show={this.state.showMessageBox} hideModal={this.hideMessageBox} message={this.state.errMsg} />
            </section>
        } else {
            content = <Redirect to="/" />
        }

        return <div className="mioto-layout">
            <Header />
            {content}
            {this.props.session.profile.info && this.props.session.profile.info.uid && <ChangeInfo show={this.state.showChangeInfo} getProfile={this.getProfile} profile={this.props.session.profile} hideModal={this.hideChangeInfo} />}
            <ChangeAvatar show={this.state.showChangeAvatar} getProfile={this.getProfile} hideModal={this.hideChangeAvatar} />
            {this.state.paper && <ChangePaper paper={this.state.paper} show={this.state.showChangePaper} getProfile={this.getProfile} hideModal={this.hideChangePaper} />}
            <Footer />
        </div>
    }
}

function mapState(state) {
    return {
        session: state.session
    }
}

Account = connect(mapState)(Account)

export default Account;