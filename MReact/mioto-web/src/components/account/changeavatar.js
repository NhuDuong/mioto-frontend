import React from "react"
import { Modal } from "react-bootstrap"
import ReactCrop, { makeAspectCrop } from 'react-image-crop'

import { commonErr } from "../common/errors"
import ImageUpload from "../common/imageupload"
import { uploadAvatar } from "../../model/profile"
import { LoadingInline } from "../common/loading"

export default class ChangeAvatar extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            err: commonErr.INNIT,
            errMsg: ""
        }

        this.closeForm = this.closeForm.bind(this);
    }

    onSubmitBtnClick = (event) => {
        const photo = this.dataURItoBlob(this.state.resizeImage);
        const self = this;
        if (photo) {
            self.setState({
                err: commonErr.LOADING
            });
            uploadAvatar(photo).then(function (resp) {
                self.setState({
                    err: resp.data.error,
                    errMsg: resp.data.errorMessage
                });
            });
        }
    }

    onRetryBtnClick() {
        this.setState({
            err: commonErr.INNIT,
            errMsg: ""
        });
    }

    hanldePhotoInputChange(photos) {
        const reader = new FileReader();

        reader.onload = (e2) => {
            this.setState({
                dataUrl: e2.target.result
            });
        };

        reader.readAsDataURL(photos[0]);
    }

    closeForm() {
        if (this.state.err >= commonErr.SUCCESS && this.state.err !== commonErr.INNIT && this.state.err !== commonErr.LOADING) {
            this.props.getProfile();
        }
        this.setState({
            err: commonErr.INNIT,
            errMsg: ""
        });
        this.props.hideModal();
    }

    dataURItoBlob(dataURI) {
        // convert base64/URLEncoded data component to raw binary data held in a string
        var byteString;
        if (dataURI.split(',')[0].indexOf('base64') >= 0)
            byteString = atob(dataURI.split(',')[1]);
        else
            byteString = unescape(dataURI.split(',')[1]);
        // separate out the mime component
        var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

        // write the bytes of the string to a typed array
        var ia = new Uint8Array(byteString.length);
        for (var i = 0; i < byteString.length; i++) {
            ia[i] = byteString.charCodeAt(i);
        }

        return new Blob([ia], { type: mimeString });
    }

    onImageLoaded = (image) => {
        const crop = makeAspectCrop({
            x: 0,
            y: 0,
            aspect: 4 / 4,
            width: 80,
        }, image.naturalWidth / image.naturalHeight);

        var bufferCanvas = document.createElement('canvas');
        var bufferContext = bufferCanvas.getContext('2d');
        bufferCanvas.width = image.naturalWidth;
        bufferCanvas.height = image.naturalHeight;
        bufferContext.drawImage(image, 0, 0);

        const width = crop.width * image.naturalWidth / 100;
        const height = crop.height * image.naturalHeight / 100;

        var tnCanvas = document.createElement('canvas');
        var tnCanvasContext = tnCanvas.getContext('2d');
        tnCanvas.width = width;
        tnCanvas.height = height;

        tnCanvasContext.drawImage(bufferCanvas, crop.x, crop.y, width, height, 0, 0, width, height);

        this.setState({
            crop: crop,
            image,
            resizeImage: tnCanvas.toDataURL("image/jpeg")
        });
    }

    onCropComplete = (crop, pixelCrop) => {
        const image = this.state.image;

        var bufferCanvas = document.createElement('canvas');
        var bufferContext = bufferCanvas.getContext('2d');
        bufferCanvas.width = image.naturalWidth;
        bufferCanvas.height = image.naturalHeight;
        bufferContext.drawImage(image, 0, 0);

        var tnCanvas = document.createElement('canvas');
        var tnCanvasContext = tnCanvas.getContext('2d');
        tnCanvas.width = pixelCrop.width;
        tnCanvas.height = pixelCrop.height;

        tnCanvasContext.drawImage(bufferCanvas, pixelCrop.x, pixelCrop.y, pixelCrop.width, pixelCrop.height, 0, 0, pixelCrop.width, pixelCrop.height);

        this.setState({
            resizeImage: tnCanvas.toDataURL("image/jpeg")
        });
    }

    onCropChange = (crop) => {
        if (crop.width < 10) {
            return;
        }

        this.setState({ crop });
    }

    render() {
        var content;
        if (this.state.err !== commonErr.INNIT) {
            if (this.state.err === commonErr.SUCCESS) {
                content = <div className="form-default form-s">
                    <div className="textAlign-center"><i className="ic ic-verify"></i> Cập nhật ảnh đại diện thành công.</div>
                    <div className="space m"></div>
                    <div className="space m"></div>
                    <button className="btn btn-primary btn--m" type="button" onClick={this.closeForm.bind(this)}>Hoàn tất</button>
                </div>
            } else if (this.state.err < commonErr.SUCCESS) {
                content = <div className="form-default form-s">
                    <div className="textAlign-center"><i className="ic ic-error"></i> Cập nhật ảnh đại diện thất bại, vui lòng thử lại.</div>
                    <div className="space m"></div>
                    <div className="space m"></div>
                    <button className="btn btn-primary btn--m" type="button" onClick={this.onRetryBtnClick.bind(this)}>Thử lại</button>
                </div>
            }
        } else {
            content = <div className="form-default form-s">
                <ImageUpload withPreview={false} onChange={this.hanldePhotoInputChange.bind(this)} className="form-control" />
                <div className="space m"></div>
                <div className="space m"></div>
                {this.state.dataUrl && <ReactCrop
                    {...this.state}
                    src={this.state.dataUrl}
                    onImageLoaded={this.onImageLoaded}
                    onComplete={this.onCropComplete}
                    onChange={this.onCropChange}
                />}
                <div className="space m"></div>
                <div className="space m"></div>
                {this.state.err !== commonErr.LOADING && this.state.resizeImage && <button className="btn btn-primary btn--m" type="button" onClick={this.onSubmitBtnClick.bind(this)}>Cập nhật</button>}
            </div>
        }

        return <Modal
            show={this.props.show}
            onHide={this.closeForm}
            dialogClassName="modal-sm modal-dialog"
        >
            <Modal.Header closeButton={true} closeLabel={""}>
                <Modal.Title>Cập nhật ảnh đại diện</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                {this.state.err === commonErr.LOADING && <LoadingInline />}
                {content}
            </Modal.Body>
        </Modal>
    }
}
