import React from "react"
import moment from 'moment'
import { Link } from 'react-router-dom'
import InfiniteScroll from "react-infinite-scroller"
import { StickyContainer, Sticky } from 'react-sticky'
import { Modal } from "react-bootstrap"

import Header from "../common/header"
import { LoadingPage, LoadingInline } from "../common/loading"
import { commonErr } from "../common/errors"
import { getTrips } from "../../model/car"
import { MessagePage } from "../common/messagebox"
import { formatPrice, getTripStatus } from "../common/common"
import Session from "../common/session"
import { TripBar } from "../account/mytrips"



class TripItem extends React.Component {
    render() {
        const car = this.props.car;
        const trip = this.props.trip;
        const owner = this.props.owner;
        const status = getTripStatus(trip.status);

        return <div className="trip-box new-box">
							<div className="trip-header">
								<h4 className="car-name">
									<span>{car.name}</span>
								</h4>
								
            	</div>
            {/* <div className="space m"></div> */}
						<div className="trip-body trip-header">
							<div className="left">
												<div className="car-img">
														<Link to={{ pathname: `/trip/detail/${trip.id}` }}>
																<div className="fix-img"><img src={`${car.photos ? car.photos[0].thumbUrl : ""}`} alt={`Cho thuê xe tự lái ${car.name}`} /></div>
														</Link>
												</div>
										</div>
									
                <div className="right">
                    <p> Bắt đầu: {moment(trip.tripDateFrom).format("HH:mm dddd, DD/MM/YYYY")}</p>
										<p> Kết thúc: {moment(trip.tripDateTo).format("HH:mm dddd, DD/MM/YYYY")}</p>
										<p className="total-price">Tổng tiền {formatPrice(trip.priceSummary.priceTotal)}</p>
                </div>
               
            </div>
            <Link to={{ pathname: `/trip/detail/${trip.id}` }}>
                <div className="trip-footer">
                    <div className="status-trips">
                        <p><span className={status.class} /> {status.name}</p>
                    </div>
                    <div className="time">
                        <p>{moment(trip.timeBooked).fromNow()}</p>
                    </div>
                </div>
            </Link>
        </div>
    }
}



class FilterTripHistory extends React.Component {
	render() {
		return <div className="content-filter">
				<div className="rent-car" style={{paddingTop: "0px"}}>
					<span className="slstitle">Sắp xếp</span>
					<div className="line-form">
						<div className="wrap-select">
							<select>
								<option value="0" selected>Mặc định</option>
								<option value="1">Ưu tiên thời gian đặt</option>
								<option value="2">Ưu tiên thời gian khởi hành</option>
							</select>
						</div>
					</div>
					<span className="slstitle">Lọc theo</span>
					<div className="line-form">
						<div className="wrap-select">
							<select>
								<option value="0" selected>Tất cả</option>
								<option value="1">Chuyến thuê</option>
								<option value="2">Chuyến cho thuê</option>
							</select>
						</div>
					</div>
					<span className="slstitle">Trạng thái</span>
					<div className="line-form">
						<div className="wrap-select">
							<select>
								<option value="0">Tất cả</option>
								<option value="1">Chờ duyệt</option>
								<option value="2">Đã duyệt</option>
								<option value="3">Đã đặt cọc</option>
								<option value="4">Đang thuê</option>
								<option value="5">Đã kết thúc</option>
							</select>
						</div>
					</div>
					<span className="slstitle">Bắt đầu</span>
					<div className="line-form">
						<div class="wrap-input has-dropdown"><span class="value placeholder">03 / 09 / 2017</span></div>
					</div>
					<span className="slstitle">Kết thúc</span>
					<div className="line-form">
						<div class="wrap-input has-dropdown"><span class="value placeholder">03 / 09 / 2017</span></div>
					</div>
					<span className="slstitle">Theo xe</span>
					<div className="line-form">
						<div className="wrap-select">
							<select>
								<option value="0">Tất cả</option>
								<option value="1">HYUNDAI SONATA 2016</option>
								<option value="2">MAZDA 3 2017</option>
								<option value="3">CHEVROLET SPARK 2017</option>
							</select>
						</div>
					</div>
					<div className="space m"></div>
					<a className="btn btn-default btn--m block has-dot-red btn-reset" href="#!"><i className="ic ic-reset"></i><span>Bỏ lọc</span></a>
				</div>
			</div> 
		
	}
}

class MyHistory extends React.Component {
    constructor() {
        super();
        this.state = {
            err: commonErr.INNIT,
            filterStatus: 0,
            trips: [],
            cars: [],
						profiles: [], 
						isOpenFilterTrip: false, 
				}
				this.toggleFilterTrip = this.toggleFilterTrip.bind(this);
    }

    getTrips(stt) {
        const self = this;
        self.setState({
            err: commonErr.LOADING
        });

        getTrips(stt, 0, 0, 0).then(function (resp) {
            if (resp.data.error >= commonErr.SUCCESS && resp.data.data.trips) {
                self.setState({
                    trips: resp.data.data.trips,
                    cars: resp.data.data.cars,
                    profiles: resp.data.data.profiles,
                    more: resp.data.data.more,
                    err: resp.data.error
                });
            } else {
                self.setState({
                    err: resp.data.error,
                    trips: null,
                    more: 0
                });
            }
        });
    }

    componentDidMount() {
        window.scrollTo(0, 0);
        //
        const self = this;
        self.setState({
            err: commonErr.LOADING
        });
        this.getTrips(0);
    }

    getTripsMore() {
        const self = this;

        const stt = this.state.filterStatus;
        const nextPos = this.state.trips.length;

        getTrips(stt, 0, nextPos, 0).then(function (resp) {
            if (resp.data.error >= commonErr.SUCCESS && resp.data.data.trips
                && resp.data.data.cars && resp.data.data.profiles) {
                self.setState({
                    trips: self.state.trips.concat(resp.data.data.trips),
                    cars: self.state.cars.concat(resp.data.data.cars),
                    profiles: self.state.profiles.concat(resp.data.data.profiles),
                    more: resp.data.data.more,
                    err: resp.data.error
                });
            } else {
                self.setState({
                    err: resp.data.error,
                    more: 0
                });
            }
        });
    }

    onStatusFilterChange(event) {
        window.scrollTo(0, 0);
        //
        const filterStatus = event.target.value;
        this.setState({
            filterStatus: event.target.value
        });
        this.getTrips(filterStatus);
    }

    onStatusFilterReset() {
        window.scrollTo(0, 0);
        //
        this.setState({
            filterStatus: 0
        });
        this.getTrips(0);
    }

		toggleFilterTrip() {
			this.setState({
				isOpenFilterTrip: !this.state.isOpenFilterTrip
			})
			console.log(this.state.isOpenFilterTrip)
		}
	
    render() {
        var content;
        if (this.state.err === commonErr.LOADING) {
            content = <LoadingPage />
        } else if (this.state.err >= commonErr.SUCCESS 
            && this.state.trips && this.state.trips.length > 0
            && this.state.cars && this.state.cars.length > 0 
            && this.state.profiles && this.state.profiles.length > 0) {

            const trips = this.state.trips;
            const cars = this.state.cars;
            const profiles = this.state.profiles;

            var i, j, k;
            const tripsInfo = [];

            for (i = 0; i < trips.length; ++i) {
                const trip = trips[i];
                for (j = 0; j < cars.length; ++j) {
                    const car = cars[j];
                    if (car.id === trip.carId) {
                        for (k = 0; k < profiles.length; ++k) {
                            const profile = profiles[k];
                            if (profile.uid === trip.ownerId) {
                                tripsInfo.push({ trip, car, profile });
                                break;
                            }
                        }
                        break;
                    }
                }
            }

            content = <InfiniteScroll pageStart={0}
                loadMore={this.getTripsMore.bind(this)}
                hasMore={this.state.more === 1}
                loader={<ul key={0}><LoadingInline /></ul>}>
                {tripsInfo.map(tripInfo => <TripItem key={tripInfo.trip.id} trip={tripInfo.trip} car={tripInfo.car} owner={tripInfo.profile} />)}
            </InfiniteScroll>
        } else {
            content = <MessagePage message={"Không tìm thấy chuyến nào."} />
        }

        return <div className="mioto-layout bg-gray">
            <Session />
            <Header />
						<section className="body no-pd-on-mobile">
							<TripBar openFilter={this.toggleFilterTrip.bind(this)}></TripBar>
										<div className="module-map module-trips min-height-no-footer">
											<div className="trip-container"><div className="has-trip">
											{content}
											</div>
										</div>
									</div>
								</section>
								<Modal
										show={this.state.isOpenFilterTrip}
										onHide={this.toggleFilterTrip}
										dialogClassName="modal-sm modal-dialog">
										<Modal.Header closeButton={true} closeLabel={""}>
												<Modal.Title>Bộ lọc</Modal.Title>
										</Modal.Header>
										<Modal.Body>
											
											<FilterTripHistory />
											
										</Modal.Body>
								</Modal>
        		</div>
    }
}

export default MyHistory;