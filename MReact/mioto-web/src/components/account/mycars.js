import React from "react"
import { StickyContainer, Sticky } from 'react-sticky'
import InfiniteScroll from "react-infinite-scroller"
import StarRatings from "react-star-ratings"
import NumberFormat from "react-number-format"

import Header from "../common/header"
import Session from "../common/session"
import { commonErr } from "../common/errors"
import { formatPrice, CarStatus, CarStatusColor, formatTitleInUrl } from "../common/common"
import { getOwnerCars, removeCar } from "../../model/car"
import { getBalance } from '../../model/wallet'
import { LoadingPage, LoadingInline } from "../common/loading"
import { MessagePage } from "../common/messagebox"
import { Modal } from "react-bootstrap"

import car_photo from "../../static/images/upload/car_1.png"

class CarItem extends React.Component {
    render() {
        const props = this.props;

        return <div className="trip-box">
            <a className="func-remove" onClick={() => { props.showConfirmRemove(props.car.id) }}><i className="ic ic-remove"></i></a>
            <div className="box-wrap">
                <div className="item-car status-trips">
                    <p className="status">
                        <span className={`status ${CarStatusColor[props.car.status]}-dot`}></span>{CarStatus[props.car.status]}
                    </p>
                    <div className="car-img">
                        <div className="fix-img">
                            <a href={`/car/${formatTitleInUrl(props.car.name)}/${props.car.id}`}>
                                <img src={props.car.photos ? props.car.photos[0].thumbUrl : car_photo} alt={`Cho thuê xe tự lái ${props.car.name}`} />
                            </a>
                        </div>
                        <span className="price">{formatPrice(props.car.price)}</span>
                    </div>
                </div>
                <div className="desc-car">
                    <h2>{props.car.name}</h2>
                    <div className="wrap-line">
                        <StarRatings
                            rating={props.car.rating.avg}
                            starRatedColor="#00a550"
                            starDimension="17px"
                            starSpacing="1px"
                        />
                        <div className="bar-line"></div>
                        <p>{props.car.totalTrips} chuyến </p>
                    </div>
                    <span className="price">{formatPrice(props.car.price)}</span>
                    <hr className="line-m" />
                    <a className={"btn btn-secondary btn--m"} href={`/car/${formatTitleInUrl(props.car.name)}/${props.car.id}`}>Xem chi tiết</a>
                    <a className={"btn btn-primary btn--m"} href={`/carsetting/${props.car.id}#infosetting`}>Quản lý xe</a>
                </div>
            </div>
            <div className="trip-footer">
                <a className={"btn btn-secondary btn--m"} href={`/car/${formatTitleInUrl(props.car.name)}/${props.car.id}`}>Xem chi tiết</a>
                <a className={"btn btn-primary btn--m"} href={`/carsetting/${props.car.id}#infosetting`}>Quản lý xe</a>
            </div>
        </div>
    }
}

class MyCars extends React.Component {
    constructor() {
        super();
        this.state = {
            cars: [],
            err: commonErr.INNIT,
            errMsg: "",
            isShowConfirmRemove: false,
            activeCarId: 0,
            filterStatus: 0,

        }
    }

    componentDidMount() {
        window.scrollTo(0, 0);

        this.setState({
            err: commonErr.LOADING
        });

        getOwnerCars(0, 0, 0, 0, true).then((resp) => {
            if (resp.data.error >= commonErr.SUCCESS && resp.data.data.cars) {
                this.setState({
                    cars: resp.data.data.cars,
                    more: resp.data.data.more * 1,
                    err: resp.data.error
                });
            } else {
                this.setState({
                    err: resp.data.error
                });
            }
        });
     
        getBalance().then((resp) => {
					if (resp.data.error === commonErr.SUCCESS) {
						this.setState({
							balance: resp.data.data.balance
						})
					} else {
						this.setState({
							err: resp.data.error,
						})
					}
				})
    }

    getOwnerCarsMore() {
        const nextPos = this.state.cars.length;
        getOwnerCars(0, 0, nextPos, 0).then((resp) => {
            if (resp.data.error >= commonErr.SUCCESS && resp.data.data.cars) {
                this.setState({
                    cars: this.state.cars.concat(resp.data.data.cars),
                    more: resp.data.data.more * 1,
                    err: resp.data.error
                });
            } else {
                this.setState({
                    err: resp.data.error
                });
            }
        });
    }

    showConfirmRemove(carId) {
        this.setState({
            isShowConfirmRemove: true,
            activeCarId: carId
        });
    }

    hideConfirmRemove() {
        this.setState({
            isShowConfirmRemove: false,
            activeCarId: 0,
            err: commonErr.INNIT,
            errMsg: ""
        });
    }

    removeCar() {
        this.setState({
            err: commonErr.LOADING
        });

        removeCar(this.state.activeCarId).then((resp) => {
            this.hideConfirmRemove();
            if (resp.data.error >= commonErr.SUCCESS) {
                getOwnerCars(0, 0, 0, 0).then((resp) => {
                    if (resp.data.error >= commonErr.SUCCESS && resp.data.data.cars) {
                        this.setState({
                            cars: resp.data.data.cars,
                            more: resp.data.data.more * 1,
                            err: resp.data.error
                        });
                    } else {
                        this.setState({
                            err: resp.data.error
                        });
                    }
                });
            } else {
                this.setState({
                    err: resp.data.error,
                    errMsg: resp.data.errorMessage
                });
            }
        });
    }

    onFilterStatusChange(event) {
        this.setState({
            filterStatus: event.target.value * 1
        });
    }

    render() {
        var propertiesCont;
        if (this.state.err === commonErr.LOADING) {
            propertiesCont = <LoadingPage />
        } else {
            var properties = this.state.cars.slice();
            if (properties) {
                if (this.state.filterStatus !== 0) {
                    properties = properties.filter((car) => {
                        return car.status === this.state.filterStatus;
                    });
                }
                if (properties.length > 0) {
                    propertiesCont = <InfiniteScroll pageStart={0}
                        loadMore={this.getOwnerCarsMore.bind(this)}
                        hasMore={this.state.more === 1}
                        loader={<ul><LoadingInline /></ul>}>
                        <div className="listing-car">
                            {properties.map(car => <CarItem
                                key={car.id}
                                car={car}
                                id={car.id}
                                showConfirmRemove={this.showConfirmRemove.bind(this)} />)}
                        </div>
                    </InfiniteScroll>
                } else {
                    propertiesCont = <MessagePage message={"Không tìm thấy xe nào."} />
                }
            } else {
                propertiesCont = <MessagePage message={"Bạn chưa có xe nào. Hãy đăng kí xe ngay để có cơ hội kiếm thêm thu nhập hàng tháng."} />
            }
        }

        return <div className="mioto-layout bg-gray">
            <Session />
            <Header />
            <StickyContainer>
                <section className="body">
                    <div className="sidebar-control z-2">
                        <div className="sidebar-settings general-settings">
                            <ul>
                                <li> <a className="active" href="/mycars"><i className="ic ic-cars"></i><span className="label">Danh sách xe</span></a></li>
                                <li> <a href="/calendars"><i className="ic ic-setting-calendar"></i><span className="label">Lịch chung</span></a></li>
                                <li> <a href="/gps"><i className="ic ic-gps"></i><span className="label">GPS</span><sup class="new-func">BETA</sup></a></li>
                                <li> <a href="/commonsetting"><i className="ic ic-setting-rent"> </i><span className="label">Cài đặt chung</span></a></li>
                                <li> <a href="/carregister"><i className="ic ic-plus-black"></i><span className="label">Đăng ký xe </span></a></li>
                                <li> <a href="/mywallet"><i className="ic ic-owner-wallet"></i><span className="label">Số dư: <span className="fontWeight-6 font-16"><NumberFormat value={this.state.balance} displayType={'text'} thousandSeparator={" "} decimalSeparator={","} suffix={"đ"} /></span></span></a></li>
                            </ul>
                        </div>
                    </div>
                    <Sticky disableCompensation>
                        {({
                            isSticky,
                            wasSticky,
                            style,
                            distanceFromTop,
                            distanceFromBottom,
                            calculatedHeight
                        }) => {
                            return <div className="filter-trips show-on-small">
                                <div className="content-filter">
                                    <div className="rent-car">
                                        <div className="line-form">
                                            <label className="label">Trạng Thái </label>
                                            <div className="wrap-select">
                                                <select onChange={this.onFilterStatusChange.bind(this)}>
                                                    <option value="0">Tất cả</option>
                                                    <option value="2">Đang hoạt động</option>
                                                    <option value="1">Đang chờ duyệt</option>
                                                    <option value="3">Đã bị từ chối </option>
                                                    <option value="5">Đang tạm ngừng</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            // <div className="sidebar-control">
                            //         <div className="sidebar-settings show-on-med-and-down" style={{ ...style }}>
                            //             <ul>
                            //                 <li><a href="/commonsetting"><i className="ic ic-setting-rent"></i></a></li>
                            //                 <li><a href="/calendars"><i className="ic ic-setting-calendar"></i></a></li>
                            //                 <li><a href="/carregister"><i className="ic ic-plus-black"></i></a></li>
                            //                 <li><a href="/gps"><i className="ic ic-gps"></i></a></li>
                            //             </ul>
                            //         </div>
                            //     </div>
                        }}
                    </Sticky>

                    <div className="body has-filter">
                        <Sticky disableCompensation>
                            {({
                                isSticky,
                                wasSticky,
                                style,
                                distanceFromTop,
                                distanceFromBottom,
                                calculatedHeight
                            }) => {
                                return <div className="filter-trips" style={{ ...style, width: "325px" }}>
                                    <div className="content-filter">
                                        <div className="rent-car">
                                            <div className="line-form">
                                                <label className="label">Trạng Thái </label>
                                                <div className="wrap-select">
                                                    <select onChange={this.onFilterStatusChange.bind(this)}>
                                                        <option value="0">Tất cả</option>
                                                        <option value="2">Đang hoạt động</option>
                                                        <option value="1">Đang chờ duyệt</option>
                                                        <option value="3">Đã bị từ chối </option>
                                                        <option value="5">Đang tạm ngừng</option>
                                                    </select>
                                                </div>
                                            </div>
                                            {/* <div className="space-m"> </div>
                                            <div className="btn-group">
                                                <div className="wrap-btn-filter">
                                                    <a className="btn btn-primary btn--m" href="/carregister">Đăng ký xe</a>
                                                </div>
                                                <div className="wrap-btn-filter">
                                                    <a className="btn btn-secondary btn--m" href="/commonsetting">Giấy tờ chung</a>
                                                    <div className="space m"></div>
                                                    <a className="btn btn-secondary btn--m" href="/calendars">Lịch chung</a>
                                                    <div className="space m"></div>
                                                    <a className="btn btn-secondary btn--m" href="/gps">GPS</a>
                                                </div>
                                            </div> */}
                                        </div>
                                    </div>
                                </div>
                            }}
                        </Sticky>
                        <div className="module-map module-car min-height-no-footer">
                            {propertiesCont}
                        </div>
                        <Modal
                            show={this.state.isShowConfirmRemove}
                            onHide={this.hideConfirmRemove.bind(this)}
                            dialogClassName="modal-sm modal-dialog"
                        >
                            <Modal.Header closeButton={true} closeLabel={""}>
                                <Modal.Title>Xác nhận xoá xe</Modal.Title>
                            </Modal.Header>
                            <Modal.Body>
                                <div className="form-default form-s">
                                    <div className="line-form textAlignCenter">
                                        {(!this.state.errMsg || this.state.errMsg === "") && <div className="textAlign-center"><i className="ic ic-error"></i> Bạn có chắc là muốn xoá xe này?</div>}
                                        {this.state.errMsg !== "" && <div className="textAlign-center"><i className="ic ic-error"></i> {this.state.errMsg}</div>}
                                    </div>
                                    <div className="clear"></div>
                                    <div className="space m"></div>
                                    <p className="textAlign-center has-more-btn">
                                        <a className="btn btn-primary btn--m" onClick={this.removeCar.bind(this)}>Xoá</a>
                                    </p>
                                </div>
                            </Modal.Body>
                        </Modal>
                    </div>
                </section>
            </StickyContainer>
        </div>
    }
}

export default MyCars;