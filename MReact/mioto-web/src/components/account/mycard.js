import React from "react"

import { Modal } from "react-bootstrap"


import Header from "../common/header"
import { LoadingPage } from "../common/loading"
import { commonErr } from "../common/errors"
import { MessageBox } from "../common/messagebox"

import Session from "../common/session"
import Footer from "../common/footer"


import { getListCard, requestAddCard, removeCard } from "../../model/payment"


import credit_card from "../../static/images/payments/credit-card.svg"
import visa from "../../static/images/payments/visa.svg"
import master from "../../static/images/payments/mastercard.svg"
import jcb from "../../static/images/payments/jcb.svg"

import my_card from "../../static/images/payments/my-card.svg"

class CardItem extends React.Component {
	constructor(props) {
		super();
		this.buildImgPayment = this.buildImgPayment.bind(this)
	}
	buildImgPayment() {
		const cardBrand = this.props.card.cardBrand;
		switch (cardBrand) {
			case "VISA": 
				return <img src={visa} />
			case "MASTERCARD": 
				return <img src={master} />
			case "JCB": 
				return <img src={jcb} />
			default:
				return <img src={credit_card} />
		}
	
	}

	render() {
		const props = this.props;
		const card = this.props.card;
		return <div className="box-card__item d-flex">
			<a className="func-remove show-on-sm">
				<i className="ic ic-remove" />
			</a>
			<div className="logo-card">
				{this.buildImgPayment()}
			</div>
			<div className="info-card">
				<p className="card-number">•••• •••• •••• {card.cardNumber.slice(12)}
					{card.cardType && <span className="card-type" style={{ textTransform: "uppercase" }}>{card.cardType}</span>}
				</p>
				<p className="card-name" >{card.cardName}</p>
				<p className="valid-card">Hiệu lực thẻ: {card.cardExpiredMonth}/{card.cardExpiredYear}</p>
			</div>
			<div className="remove-card">
				<a className="btn btn--m btn-secondary hide-on-sm" onClick={() => { props.showConfirmRemove(card.cardId) }}>Hủy liên kết thẻ</a>
			</div>
		</div >
	}
}

export default class MyCard extends React.Component {
	constructor(props) {
		super(props);
			this.state = {
				err: commonErr.INNIT,
				errMsg: "",

				cards: [],
				activeCarId: 0,
		
				isShowConfirmRemove: false
			}
		
		this.onAddCardBtn = this.onAddCardBtn.bind(this);
		this.hideMessageBox = this.hideMessageBox.bind(this);
	}
	componentDidMount() { 
		window.scrollTo(0, 0);

		this.setState({
			err: commonErr.LOADING
		});

		getListCard().then(myCardResp => {
			if (myCardResp.data.error >= commonErr.SUCCESS && myCardResp.data.data) {
				this.setState({
					err: myCardResp.data.error,
					cards: myCardResp.data.data
				});
			} else {
				this.setState({
					err: myCardResp.data.error,
					cards: null
				})
			}
		
		})
	
	}
	componentWillReceiveProps(props) {
		this.setState({
			cards: props.cards
		})
	}




	onAddCardBtn(callbackPaths) {
		// this.setState({
		// 	err: commonErr.LOADING
		// });

		requestAddCard(callbackPaths).then(cardResp => {
			const { data, error, errorMessage } = cardResp.data
			if (error >= commonErr.SUCCESS && data) {
				window.location.assign(data.linkUrl);
			} else {
				this.setState({
					err: error,
					errMsg: errorMessage
				});
			}
		});
	}

	showConfirmRemove(cardId) {
		this.setState({
				isShowConfirmRemove: true,
				activeCardId: cardId
		});
	}

	hideConfirmRemove() {
			this.setState({
					isShowConfirmRemove: false,
					activeCarId: 0,
					err: commonErr.INNIT,
					errMsg: ""
			});
	}
	 removeCard() {
        this.setState({
            err: commonErr.LOADING
        });

        removeCard(this.state.activeCardId).then((resp) => {
            this.hideConfirmRemove();
            if (resp.data.error >= commonErr.SUCCESS) {
							getListCard().then(myCardResp => {
								if (myCardResp.data.error >= commonErr.SUCCESS && myCardResp.data.data.length > 0) {
									this.setState({
										err: myCardResp.data.error,
										cards: myCardResp.data.data
									});
								} else {
									this.setState({
										err: myCardResp.data.error,
										cards: null
									})
								}
							
							})
						
            } else {
                this.setState({
                    err: resp.data.error,
                    errMsg: resp.data.errorMessage
                });
            }
        });
    }

		hideMessageBox() {
			this.setState({
					err: commonErr.INNIT,
					errMsg: ""
			})	
		}

	render() {

		var content;
		const path = "mycard";
		const { err, errMsg } = this.state;

		if (this.state.err === commonErr.LOADING) {
			content = <LoadingPage />
		} else if (this.state.err >= commonErr.SUCCESS && this.state.cards && this.state.cards.length > 0) {
			content = this.state.cards.map(card => <CardItem key={card.cardId} card={card} showConfirmRemove={this.showConfirmRemove.bind(this)}></CardItem>)
		} else {
				content = <div style={{ textAlign: "center" }}>
				<img src={my_card} className="img-fluid img-default" style={{width: "50%", marginTop: "50px"}} />
				<p className="fontWeight-6 font-14">Bạn chưa có thẻ nào</p>
				</div>
		}
		
		return <div className="mioto-layout bg-gray">
			<Session />
			<Header />
			<section className="body no-pd-on-mobile">
				<div className="my-card__sect">
					<div className="m-container">
						<div className="my-card__wrap">
							<h3 className="n-title">THẺ CỦA TÔI</h3>
							<a className="btn btn--m btn-add-card hide-on-med" onClick={() => this.onAddCardBtn(path)}><i className="ic ic-plus" />  Thêm thẻ</a>
							<a className="func-filter plus show-on-med" onClick={() => this.onAddCardBtn(path)}><i className="ic plus-o" /></a>
						</div>
				
						<MessageBox show={errMsg !== ""} error={err} message={errMsg} hideModal={this.hideMessageBox} />
					</div>
				</div>
				<div className="list-card__sect module-trips module-map">
						<div className="trip-container">
							<div className="box-card__wrap"> 
							{content}
							
							</div>
						</div>
				</div>
			</section>
			<Modal
					show={this.state.isShowConfirmRemove}
					onHide={this.hideConfirmRemove.bind(this)}
					dialogClassName="modal-sm modal-dialog"
			>
					<Modal.Header closeButton={true} closeLabel={""}>
							<Modal.Title>Xác nhận xoá thẻ</Modal.Title>
					</Modal.Header>
					<Modal.Body>
							<div className="form-default form-s">
									<div className="line-form textAlignCenter">
											{(!this.state.errMsg || this.state.errMsg === "") && <div className="textAlign-center"><i className="ic ic-error"></i> Bạn có chắc là muốn hủy liên kết với thẻ này?</div>}
											{this.state.errMsg !== "" && <div className="textAlign-center"><i className="ic ic-error"></i> {this.state.errMsg}</div>}
									</div>
									<div className="clear"></div>
									<div className="space m"></div>
									<p className="textAlign-center has-more-btn">
											<a className="btn btn-primary btn--m" onClick={this.removeCard.bind(this)}>Xoá</a>
									</p>
							</div>
					</Modal.Body>
			</Modal>
			<Footer />
			</div>
	}
}