import React from "react"
import moment from "moment"
import InputMask from "react-input-mask"
import { Modal } from "react-bootstrap"

import { commonErr } from "../common/errors"
import { updateInfo } from "../../model/profile"
import { LoadingInline } from "../common/loading"

export default class ChangeInfo extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            ip_displayname: props.profile.info.name,
            ip_gender: props.profile.info.gender,
            ip_dob: props.profile.info.dob > 0 ? moment(props.profile.info.dob).format("DD-MM-YYYY") : "01-01-1950",
            //
            err: commonErr.INNIT,
            errMsg: ""
        }
        this.handleInputChange = this.handleInputChange.bind(this);
    }

    handleInputChange = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        });
    }

    onSubmitBtnClick = (event) => {
        if (!moment(`${this.state.ip_dob}`, "DD-MM-YYYY").isValid()) {
            this.setState({ errMsg: "Ngày sinh không hợp lệ." });
        } else {
            const displayName = this.state.ip_displayname;
            const dateOfBirth = moment(`${this.state.ip_dob}`, "DD-MM-YYYY").valueOf();
            const gender = this.state.ip_gender;

            if (displayName !== "") {
                this.setState({
                    err: commonErr.LOADING
                });
                updateInfo(displayName, gender, dateOfBirth).then(resp => {
                    this.setState({
                        err: resp.data.error,
                        errMsg: resp.data.errorMessage
                    });
                    if (resp.data.error === commonErr.SUCCESS) {
                        this.props.getProfile();
                        this.props.hideModal();
                        this.setState({
                            err: commonErr.INNIT,
                            errMsg: ""
                        })
                    }
                });
            }
        }
    }

    onBackBtnClick() {
        this.setState({
            err: commonErr.INNIT,
            errMsg: ""
        })
    }

    render() {
        var content;
        if (this.state.errMsg && this.state.errMsg !== "") {
            content = <div className="form-default form-s">
                <div className="textAlign-center"><i className="ic ic-error"></i> Cập nhật thất bại. {this.state.errMsg}</div>
                <div className="clear"></div>
                <div className="space m"></div>
                <div className="space m"></div>
                <button className="btn btn-primary btn--m" type="button" name="backBtn" onClick={this.onBackBtnClick.bind(this)}>Thử lại</button>
            </div>
        } else {
            content = <div className="form-default form-s">
                <div className="line-form">
                    <div className="wrap-input has-ico"><i className="ic ic-user-fill"></i>
                        <input type="text" name="ip_displayname" placeholder="Tên hiển thị" onChange={this.handleInputChange} defaultValue={this.state.ip_displayname} />
                    </div>
                </div>
                <div className="line-form">
                    <div className="wrap-input has-ico"><i className="ic ic-calendar-fill"></i>
                        <InputMask name="ip_dob" mask="99-99-9999" defaultValue={`${this.state.ip_dob}`} onChange={this.handleInputChange} />
                    </div>
                </div>
                <div className="line-form">
                    <div className="wrap-select">
                        <select name="ip_gender" defaultValue={this.state.ip_gender} onChange={this.handleInputChange}>
                            <option value="1">Nam</option>
                            <option value="2">Nữ</option>
                        </select>
                    </div>
                </div>
                <div className="clear"></div>
                <a className="btn btn-primary btn--m" type="button" onClick={this.onSubmitBtnClick.bind(this)}>Cập nhật {this.state.err === commonErr.LOADING && <LoadingInline />}</a>
            </div>
        }

        return (
            <Modal
                show={this.props.show}
                onHide={this.props.hideModal}
                dialogClassName="modal-sm modal-dialog"
            >
                <Modal.Header closeButton={true} closeLabel={""}>
                    <Modal.Title>Cập nhật thông tin</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    {content}
                </Modal.Body>
            </Modal>
        );
    }
}
