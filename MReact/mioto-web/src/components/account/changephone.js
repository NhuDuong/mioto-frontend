import React from "react"
import axios from "axios"
import { Modal } from "react-bootstrap"

import { fbappInfo } from "../common/fbdeveloper"
import { commonErr } from "../common/errors"
import { updatePhone } from "../../model/profile"
import AccountKit from '../common/accountkit'
import { verifyPhone } from "../../model/profile"

export default class ChangePhone extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            step: 0, //0: show update 1: show update message 2: show confirm message
            ip_newphone: null,
            err: commonErr.INNIT,
            errMsg: ""
        }

        this.handleInputChange = this.handleInputChange.bind(this);
        this.onAccountKitLoginResp = this.onAccountKitLoginResp.bind(this);
        this.closeForm = this.closeForm.bind(this);
    }

    handleInputChange = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        });
    }

    onSubmitBtnClick = (event) => {
        const newPhone = this.state.ip_newphone;
        const self = this;
        updatePhone(newPhone).then(function (resp) {
            self.setState({
                step: 1,
                err: resp.data.error,
                errMsg: resp.data.errorMessage
            });
        });
    }

    onBackBtnClick() {
        this.setState({
            step: 0,
            err: commonErr.INNIT,
            errMsg: ""
        });
    }

    closeForm() {
        this.setState({
            step: 0,
            err: commonErr.INNIT,
            errMsg: ""
        });
        if (this.state.err >= commonErr.SUCCESS && this.state.err !== commonErr.INNIT && this.state.err !== commonErr.LOADING) {
            this.props.getProfile();
        }
        this.props.hideModal();
    }

    onAccountKitLoginResp(resp) {
        const self = this;

        var verifyPhoneAccessTok;
        var verifyPhoneNumber;

        if (resp.code) {
            axios({
                method: "GET",
                url: `${fbappInfo.getAccessTokBaseDomain}?grant_type=authorization_code&code=${resp.code}&access_token=AA|${fbappInfo.appId}|${resp.state}`
            }).then(function (tokenData) {
                if (tokenData.data) {
                    verifyPhoneAccessTok = tokenData.data.access_token;
                    if (verifyPhoneAccessTok) {
                        axios({
                            method: "GET",
                            url: `${fbappInfo.getLoggedInfoBaseDomain}/?access_token=${verifyPhoneAccessTok}`
                        }).then(function (accountData) {
                            if (accountData.data && accountData.data.phone) {
                                verifyPhoneNumber = accountData.data.phone.number;
                                if (verifyPhoneNumber) {
                                    verifyPhone(verifyPhoneNumber, verifyPhoneAccessTok, 1).then(function (resp) {
                                        self.setState({
                                            step: 2,
                                            err: resp.data.error,
                                            errMsg: resp.data.errorMessage
                                        });
                                    });
                                }
                            } else {
                                self.setState({
                                    step: 2,
                                    err: resp.data.error,
                                    errMsg: resp.data.errorMessage
                                });
                            }
                        })
                    }
                }
            })
        }
    }

    render() {
        var content;
        if (this.state.step === 2) {
            if (this.state.err >= commonErr.SUCCESS) {
                content = <div className="form-default form-s">
                    <div className="line-form">
                        <div className="textAlign-center"><i className="ic ic-verify"></i> Xác minh thành công.</div>
                    </div>
                    <div className="clear"></div>
                    <div className="space m"></div>
                    <button className="btn btn-primary btn--m" type="button" onClick={this.closeForm}>Hoàn tất</button>
                </div>
            } else {
                content = <div className="form-default form-s">
                    <div className="line-form">
                        <div className="textAlign-center"><i className="ic ic-error"></i> Xác minh thất bại. {this.state.errMsg}</div>
                    </div>
                    <div className="clear"></div>
                    <div className="space m"></div>
                    <AccountKit
                        appId={fbappInfo.appId}
                        csrf={fbappInfo.csrf}
                        version={fbappInfo.version}
                        language={fbappInfo.language}
                        phoneNumber={this.state.ip_newphone ? this.state.ip_newphone.replace("+84", "") : null}
                        onResponse={(resp) => this.onAccountKitLoginResp(resp)}
                    >
                        {p => <button {...p} className="btn btn-primary btn--m" type="button">Xác minh lại</button>}
                    </AccountKit>
                </div>
            }
        } else if (this.state.step === 1) {
            if (this.state.err >= commonErr.SUCCESS) {
                content = <div className="form-default form-s">
                    <div className="line-form">
                        <div className="textAlign-center"><i className="ic ic-verify"></i> Cập nhật thành công.</div>
                    </div>
                    <div className="clear"></div>
                    <div className="space m"></div>
                    <AccountKit
                        appId={fbappInfo.appId}
                        csrf={fbappInfo.csrf}
                        version={fbappInfo.version}
                        language={fbappInfo.language}
                        phoneNumber={this.state.ip_newphone ? this.state.ip_newphone.replace("+84", "") : null}
                        onResponse={(resp) => this.onAccountKitLoginResp(resp)}
                    >
                        {p => <button {...p} className="btn btn-primary btn--m" type="button">Xác minh ngay</button>}
                    </AccountKit>
                </div>
            } else {
                content = <div className="form-default form-s">
                    <div className="line-form">
                        <div className="textAlign-center"><i className="ic ic-error"></i> Cập nhật thất bại. {this.state.errMsg}</div>
                    </div>
                    <div className="clear"></div>
                    <div className="space m"></div>
                    <button className="btn btn-primary btn--m" type="button" onClick={this.onBackBtnClick.bind(this)}>Thử lại</button>
                </div>
            }
        } else {
            content = <div className="form-default form-s">
                <div className="line-form">
                    <div className="wrap-input">
                        <input type="text" name="ip_newphone" placeholder="Số điện thoại" onChange={this.handleInputChange} />
                    </div>
                </div>
                <div className="clear"></div>
                <div className="space m"></div>
                <button className="btn btn-primary btn--m" type="button" onClick={this.onSubmitBtnClick.bind(this)}>Cập nhật</button>
            </div>
        }

        return <Modal
            show={this.props.show}
            onHide={this.closeForm}
            dialogClassName="modal-sm modal-dialog"
        >
            <Modal.Header closeButton={true} closeLabel={""}>
                <Modal.Title>Cập nhật số điện thoại</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                {content}
            </Modal.Body>
        </Modal>
    }
}
