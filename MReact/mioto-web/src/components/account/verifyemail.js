import React from "react"
import { Modal } from "react-bootstrap"

import { commonErr } from "../common/errors"
import { generateEmailOtp, verifyEmail } from "../../model/profile"
import { LoadingInline } from "../common/loading"

const otpSuccessMsg = "Mioto vừa gởi mã OTP vào email của bạn. Vui lòng nhập mã đã nhận vào ô bên dưới để xác minh.";

export default class VerifyEmail extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            step: 0, //0: show email 1:show confirm 2: show confirm message
            ip_email: props.email,
            ip_otp: "",
            token: "",
            err: commonErr.INNIT,
            errMsg: ""
        }

        this.handleInputChange = this.handleInputChange.bind(this);
    }

    handleInputChange = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        });
    }

    onConfirmBtnClick() {
        const self = this;
        const email = this.props.email;
        this.setState({
            err: commonErr.LOADING,
            errMsg: ""
        });
        generateEmailOtp(email).then(function (resp) {
            if (resp.data.data) {
                self.setState({
                    step: 1,
                    err: resp.data.error,
                    token: resp.data.data.code
                });
            } else {
                self.setState({
                    step: 1,
                    err: resp.data.error,
                    errMsg: resp.data.errorMessage
                });
            }
        });
    }

    onSubmitConfirmBtnClick = (event) => {
        const self = this;
        const email = this.state.ip_email;
        const otp = this.state.ip_otp;
        const token = this.state.token;

        this.setState({
            err: commonErr.LOADING,
            errMsg: ""
        });
        verifyEmail(email, token, otp).then(function (resp) {
            self.setState({
                step: 2,
                err: resp.data.error,
                errMsg: resp.data.errorMessage
            });
        });
    }

    onRetryBtnClick() {
        this.setState({
            step: 1,
            err: commonErr.INNIT,
            errMsg: "",
        });
    }

    closeForm() {
        this.setState({
            step: 0,
            err: commonErr.INNIT,
            errMsg: ""
        });
        this.props.getProfile();
        this.props.hideModal();
    }

    render() {
        var content;
        if (this.state.step === 2) {
            if (this.state.err >= commonErr.SUCCESS) {
                content = <div className="form-default form-s">
                    <div className="line-form">
                        <div className="textAlign-center"><i className="ic ic-verify"></i> Xác minh email thành công.</div>
                    </div>
                    <div className="clear"></div>
                    <div className="space m"></div>
                    <button className="btn btn-primary btn--m" type="button" onClick={this.closeForm.bind(this)}>Hoàn tất</button>
                </div>
            } else {
                content = <div className="form-default form-s">
                    <div className="line-form">
                        <div className="textAlign-center"><i className="ic ic-error"></i> Xác minh email thất bại. {this.state.errMsg}</div>
                    </div>
                    <div className="clear"></div>
                    <div className="space m"></div>
                    <a className="btn btn-primary btn--m" type="button" onClick={this.onRetryBtnClick.bind(this)}>Thử lại</a>
                </div>
            }
        } else if (this.state.step === 1) {
            if (this.state.err >= commonErr.SUCCESS) {
                content = <div className="form-default form-s">
                    {this.state.err === commonErr.LOADING && <LoadingInline />}
                    <div className="line-form">
                        <div className="textAlign-center"><i className="ic ic-verify"></i> {otpSuccessMsg}</div>
                    </div>
                    <div className="line-form">
                        <div className="wrap-input">
                            <input type="text" name="ip_otp" value={this.state.ip_otp} placeholder="Nhập mã OTP từ email" onChange={this.handleInputChange} />
                        </div>
                    </div>
                    <div className="clear"></div>
                    <div className="space m"></div>
                    <p className="textAlign-center has-more-btn">
                        <a className="btn btn-primary btn--m" type="button" onClick={this.onSubmitConfirmBtnClick.bind(this)}>Xác minh</a>
                    </p>
                    <p style={{ float: "right" }}>Không nhận đuọc mã OTP. <a onClick={this.onConfirmBtnClick.bind(this)}>Yêu cầu gởi lại</a></p>
                </div>
            } else {
                content = <div className="form-default form-s">
                    <div className="line-form">
                        <div className="textAlign-center"><i className="ic ic-error"></i> Không gởi được OTP. {this.state.errMsg}</div>
                    </div>
                    <div className="clear"></div>
                    <div className="space m"></div>
                    <a className="btn btn-primary btn--m" type="button" onClick={this.onConfirmBtnClick.bind(this)}>Gởi lại OTP</a>
                </div>
            }
        } else {
            content = <div className="form-default form-s">
                {this.state.err === commonErr.LOADING && <LoadingInline />}
                <div className="line-form">
                    <div className="wrap-input">
                        <input type="text" disabled={true} name="ip_email" placeholder="Email cần xác thực" defaultValue={this.props.email} onChange={this.handleInputChange} />
                    </div>
                </div>
                <div className="clear"></div>
                <div className="space m"></div>
                <p className="textAlign-center has-more-btn">
                    <a className="btn btn-primary btn--m" type="button" onClick={this.onConfirmBtnClick.bind(this)}>Xác minh ngay</a>
                </p>
            </div>
        }

        return <Modal
            show={this.props.show}
            onHide={this.props.hideModal}
            dialogClassName="modal-sm modal-dialog"
        >
            <Modal.Header closeButton={true} closeLabel={""}>
                <Modal.Title>Xác minh email</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                {content}
            </Modal.Body>
        </Modal>
    }
}