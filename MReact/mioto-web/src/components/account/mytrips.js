import React from "react"
import moment from 'moment'
import { Link } from 'react-router-dom'
import InfiniteScroll from "react-infinite-scroller"
import { Modal } from "react-bootstrap"

import Header from "../common/header"
import { LoadingPage, LoadingInline } from "../common/loading"
import { commonErr } from "../common/errors"
import { getMyTrips, getTripHistories } from "../../model/car"
import { MessagePage, MessageNoTrips } from "../common/messagebox"
import { formatPrice, getTripStatus } from "../common/common"
import UserTripsFilter from "./usertripsfilter"
import Session from "../common/session"
import { getOwnerCars } from "../../model/car"

import trip from "../../static/images/trips.svg"

const viewMode = {
    TRIPS: 0,
    HISTORY: 1
}

const defaultFilter = {
	sort: 0,
	userType: 0,
	status: 0,
	carId: "",
	fromTime: 0,
	toTime: 0
}

class TripItem extends React.Component {
    render() {
        const car = this.props.car;
        const trip = this.props.trip;
        const status = getTripStatus(trip.status);

        return <div className="trip-box new-box">
							<div className="trip-header">
								<h4 className="car-name">
									<span>{car.name}</span>
								</h4>
								
            	</div>
            {/* <div className="space m"></div> */}
						<div className="trip-body trip-header">
							<div className="left">
												<div className="car-img">
														<Link to={{ pathname: `/trip/detail/${trip.id}` }}>
																<div className="fix-img"><img src={`${car.photos ? car.photos[0].thumbUrl : ""}`} alt={`Cho thuê xe tự lái ${car.name}`} /></div>
														</Link>
												</div>
										</div>
									
                <div className="right">
                    <p> Bắt đầu: {moment(trip.tripDateFrom).format("HH:mm dddd, DD/MM/YYYY")}</p>
										<p> Kết thúc: {moment(trip.tripDateTo).format("HH:mm dddd, DD/MM/YYYY")}</p>
										<p className="total-price">Tổng tiền {formatPrice(trip.priceSummary.priceTotal)}</p>
                </div>
               
            </div>
            <Link to={{ pathname: `/trip/detail/${trip.id}` }}>
                <div className="trip-footer">
                    <div className="status-trips">
                        <p><span className={status.class} /> {status.name}</p>
                    </div>
                    <div className="time">
                        <p>{moment(trip.timeBooked).fromNow()}</p>
                    </div>
                </div>
            </Link>
        </div>
    }
}

export class TripBar extends React.Component {
	constructor() {
		super();
		this.state = {
				hasFilter: false
		}
}
	componentWillReceiveProps(props) {
		const hasFilter = props.checkHasFilter();
		this.setState({
			hasFilter: hasFilter
		})
	}
	render() {
		return <div className="finding-control form-default shadow">
			<div className="wrapper-find wrapper-new-trips">
				<div className="tab-trips">
					<ul>
						<li><a className={this.props.mode === viewMode.TRIPS ? "active" : "deactive"} onClick={this.props.viewMyTrips}>Chuyến hiện tại</a></li>
						<li><a className={this.props.mode === viewMode.HISTORY ? "active" : "deactive"} onClick={this.props.viewTripHistories}>Lịch sử chuyến</a></li>
					</ul>
				</div>
				<div className="tab-filter"><a className={this.state.hasFilter ? "btn btn--m has-dot-red" : "btn btn--m"} onClick={this.props.openFilter} href="#!"><i className="ic ic-filter-black"></i><span>Bộ lọc</span></a></div>
			</div>
		</div>
	}
} 

class MyTrips extends React.Component {
    constructor() {
        super();
        this.state = {
            err: commonErr.INNIT,
            mode: 0,
            trips: [],
						cars: [],
						carsForFilter: [],
            profiles: [], 
            filter: [],
            isOpenFilterTrip: false, 
        }
        this.toggleFilterTrip = this.toggleFilterTrip.bind(this);
    }

    componentDidMount() {
        window.scrollTo(0, 0);
        //
        const self = this;
        self.setState({
            err: commonErr.LOADING
        });

        getOwnerCars(0, 0, 0, 0, true).then(function (resp) {
            if (resp.data.error >= commonErr.SUCCESS && resp.data.data.cars) {
                self.setState({
                    carsForFilter: resp.data.data.cars,
                    err: resp.data.error
                });
            } else {
                self.setState({
                    err: resp.data.error
                });
            }
        });
        this.getTrips(0);
    }

    viewMyTrips(){
        this.setState({
            mode: viewMode.TRIPS
        }, () => { 
            this.resetFilter();
        })
        
    }

    viewTripsHistory(){
        this.setState({
            mode: viewMode.HISTORY
        }, () => {
            this.resetFilter();
        })
    }

    setFilter(filter){
        this.setState({
            filter: filter
        })
    }

    resetFilter(){
        this.setState({
            filter: []
        }, () => {
            this.getTrips(this.state.filter)
        })
    }

    getTrips(filter){
        if (this.state.mode === viewMode.TRIPS) {
            this.getMyTrips(filter);
        } else {
            this.getTripHistories(filter);
        }
    }

    getMyTrips(filter) {
        const self = this;
        self.setState({
            err: commonErr.LOADING
        });

        getMyTrips(filter, 0, 0, 0).then(function (resp) {
            if (resp.data.error >= commonErr.SUCCESS && resp.data.data.trips) {
                self.setState({
                    trips: resp.data.data.trips,
									profiles: resp.data.data.profiles,
										cars: resp.data.data.cars,
                    more: resp.data.data.more,
                    err: resp.data.error
                });
            } else {
                self.setState({
                    err: resp.data.error,
                    trips: null,
                    more: 0
                });
            }
        });
    }

    getTripHistories(filter) {
        const self = this;
        self.setState({
            err: commonErr.LOADING
        });

        getTripHistories(filter, "", 0, 0).then(function (resp) {
            if (resp.data.error >= commonErr.SUCCESS && resp.data.data.trips) {
                self.setState({
                    trips: resp.data.data.trips,
									profiles: resp.data.data.profiles,
										cars: resp.data.data.cars,
                    more: resp.data.data.more,
                    lastId: resp.data.data.lastId,
                    err: resp.data.error
                });
            } else {
                self.setState({
                    err: resp.data.error,
                    trips: null,
                    more: 0
                });
            }
        });
    }

    getTripsMore() {
        const self = this;
        const filter = self.state.filter;
        const nextPos = this.state.mode == viewMode.TRIPS ? this.state.trips.length : this.state.lastId;
        if(this.state.mode == viewMode.TRIPS){
            getMyTrips(filter, nextPos, 0, 0).then(function (resp) {
                if (resp.data.error >= commonErr.SUCCESS && resp.data.data.trips
                    && resp.data.data.cars && resp.data.data.profiles) {
                    self.setState({
                        trips: self.state.trips.concat(resp.data.data.trips),
											profiles: self.state.profiles.concat(resp.data.data.profiles),
												cars: self.state.cars.concat(resp.data.data.cars),
                        more: resp.data.data.more,
                        err: resp.data.error
                    });
                } else {
                    self.setState({
                        err: resp.data.error,
                        more: 0
                    });
                }
            });
        } else {
            getTripHistories(filter, nextPos, 0, 0).then(function (resp) {
                if (resp.data.error >= commonErr.SUCCESS && resp.data.data.trips
                    && resp.data.data.cars && resp.data.data.profiles) {
                    self.setState({
                        trips: self.state.trips.concat(resp.data.data.trips),
											profiles: self.state.profiles.concat(resp.data.data.profiles),
											cars: self.state.cars.concat(resp.data.data.cars),
                        more: resp.data.data.more,
                        lastId: resp.data.data.lastId,
                        err: resp.data.error
                    });
                } else {
                    self.setState({
                        err: resp.data.error,
                        more: 0
                    });
                }
            });
        }
    }

    toggleFilterTrip() {
        this.setState({
            isOpenFilterTrip: !this.state.isOpenFilterTrip
        })
    }

    checkHasFilter(){
        const sortValue = this.state.filter && this.state.filter.sort > 0 ? this.state.filter.sort : defaultFilter.sort;
				const userTypeValue = this.state.filter && this.state.filter.userType > 0 ? this.state.filter.userType : defaultFilter.userType;
				const carId = this.state.filter && this.state.filter.carId ? this.state.filter.carId : "";
				const status = this.state.filter && this.state.filter.status > 0 ? this.state.filter.status : defaultFilter.status;
				const fromTime = this.state.filter && this.state.filter.fromTime > 0 ? this.state.filter.fromTime : defaultFilter.fromTime;
				const toTime = this.state.filter && this.state.filter.fromTime > 0 ? this.state.filter.fromTime : defaultFilter.fromTime;

		return sortValue > defaultFilter.sort 
			|| userTypeValue > defaultFilter.userType
			|| carId != defaultFilter.carId
			|| status > defaultFilter.status
			|| fromTime > defaultFilter.fromTime
			|| toTime > defaultFilter.toTime
    }
	
    render() {
        var content;
        var trips = [];
        var cars = [];
			var profiles = [this.state.profiles];
			const hasFilter = this.checkHasFilter();
        if (this.state.err === commonErr.LOADING) {
            content = <LoadingPage />
        } else if (this.state.err >= commonErr.SUCCESS 
					&& this.state.trips && this.state.cars && this.state.profiles) {
						
					trips = this.state.trips;
					cars = this.state.cars;
					profiles = this.state.profiles;

            var i, j, k;
            const tripsInfo = [];

					for (i = 0; i < trips.length; ++i) {
                const trip = trips[i];
                for (j = 0; j < cars.length; ++j) {
                    const car = cars[j];
									if (car.id === trip.carId) {
                        for (k = 0; k < profiles.length; ++k) {
                            const profile = profiles[k];
													if (profile.uid === trip.ownerId) {
                                tripsInfo.push({ trip, car, profile });
                                break;
                            }
                        }
                        break;
                    }
                }
            }
            content = <InfiniteScroll  >
                {tripsInfo.map(tripInfo => <TripItem key={tripInfo.trip.id} trip={tripInfo.trip} car={tripInfo.car} owner={tripInfo.profile} />)}
            </InfiniteScroll>
				} else {
					if (hasFilter) {
            content = <MessagePage message={"Không tìm thấy chuyến nào."} />					 
					} else {
						content = <MessageNoTrips message={"Bạn chưa có chuyến nào, hãy thuê ngay một chiếc xe để trải nghiệm dịch vụ của Mioto"} img={trip} />
					 }
        }

        return <div className="mioto-layout bg-gray">
            <Session />
            <Header />
						<section className="body no-pd-on-mobile">
                            <TripBar openFilter={this.toggleFilterTrip.bind(this)}
                                     mode={this.state.mode}
                                     viewMode={viewMode}
                                     viewMyTrips={this.viewMyTrips.bind(this)}
                                     viewTripHistories={this.viewTripsHistory.bind(this)}
                                     checkHasFilter={this.checkHasFilter.bind(this)}></TripBar>
                    {/* <Sticky disableCompensation>
                        {({
                            isSticky,
                            wasSticky,
                            style,
                            distanceFromTop,
                            distanceFromBottom,
                            calculatedHeight
                        }) => {
                            return <div className="filter-trips show-on-small" style={{ ...style }}>
                                <div className="content-filter">
                                    <div className="rent-car">
                                        <div className="line-form">
                                            <label className="label">Trạng Thái</label>
                                            <div className="wrap-select">
                                                <select onChange={this.onStatusFilterChange.bind(this)} value={this.state.filterStatus}>
                                                    <option value="0">Tất cả</option>
                                                    <option value="1">Chờ duyệt</option>
                                                    <option value="2">Đã từ chối</option>
                                                    <option value="3">Chờ đặt cọc</option>
                                                    <option value="4">Đã đặt cọc</option>
                                                    <option value="5">Đã huỷ</option>
                                                    <option value="6">Đang thuê</option>
                                                    <option value="7">Đã kết thúc</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        }}
                    </Sticky>
                    <div className="body has-filter">
                        <Sticky disableCompensation>
                            {({
                                isSticky,
                                wasSticky,
                                style,
                                distanceFromTop,
                                distanceFromBottom,
                                calculatedHeight
                            }) => {
                                return <div className="filter-trips" style={{ ...style, width: "325px" }}>
                                    <div className="content-filter">
                                        <div className="rent-car">
                                            <div className="line-form"><label className="label">Trạng thái</label>
                                                <div className="wrap-select">
                                                    <select onChange={this.onStatusFilterChange.bind(this)} value={this.state.filterStatus}>
                                                        <option value="0">Tất cả</option>
                                                        <option value="1">Chờ duyệt</option>
                                                        <option value="2">Đã từ chối</option>
                                                        <option value="3">Chờ đặt cọc</option>
                                                        <option value="4">Đã đặt cọc</option>
                                                        <option value="5">Đã huỷ</option>
                                                        <option value="6">Đang thuê</option>
                                                        <option value="7">Đã kết thúc</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            }}
                        </Sticky> */}
										<div className="module-map module-trips min-height-no-trips">
											<div className="trip-container"><div className="has-trip">
											{content}
											</div>
										</div>
									</div>
								</section>
								<Modal
										show={this.state.isOpenFilterTrip}
										onHide={this.toggleFilterTrip}
										dialogClassName="modal-sm modal-dialog">
										<Modal.Header closeButton={true} closeLabel={""}>
												<Modal.Title>Bộ lọc</Modal.Title>
										</Modal.Header>
										<Modal.Body>
											
                                            <UserTripsFilter 
                                                viewMode={viewMode}
                                                filter={this.state.filter}
                                                mode={this.state.mode}
                                                resetFilter={this.resetFilter.bind(this)}
                                                setFilter={this.setFilter.bind(this)}
                                                getTrips={this.getTrips.bind(this)}
                                                defaultFilter={defaultFilter}
                                                carsForFilter={this.state.carsForFilter}
                                                checkHasFilter={this.checkHasFilter.bind(this)}/>
										</Modal.Body>
								</Modal>
        		</div>
    }
}

export default MyTrips;