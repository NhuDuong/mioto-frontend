import React from "react"
import { connect } from "react-redux"
import { Modal } from "react-bootstrap"
import ReactInterval from "react-interval"
import ReactStars from "react-stars"
import HtmlToReactParser from "html-to-react"

import { commonErr } from "../common/errors"
import BookingRequestBox from "./bookingreq"
import { reviewTripByOwner, reviewTripByTraveler, removePendingReview, getTripDetail } from "../../model/car"
import { getUserNotify, getUserNotifyCouter, getUserNotifyDetail, seenUserNotify } from "../../model/profile"

class NotificationBox extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            noti: null,
            rating: 5,
            comment: "",
            timer: 3000
        }

        this.seenUserNotify = this.seenUserNotify.bind(this);
        this.close = this.close.bind(this);
        this.getNoti = this.getNoti.bind(this);
    }

    getNoti() {
        if (!this.props.isAllowedShow) {
            return;
        }

        if (!this.state.noti) {
            getUserNotifyCouter().then(resp => {
                if (resp.data && resp.data.data) {
                    if (resp.data.data.counter > 0) {
                        getUserNotify(0, 0, 0).then(resp => {
                            if (resp.data.data) {
                                const notis = resp.data.data.notifications;
                                if (notis.length > 0) {
                                    const noti = notis[0];
                                    if (!noti.seen) {
                                        if (noti.type === 1 && (!noti.alFull || noti.alFull === 0)) { //ANNOUNCE
                                            getUserNotifyDetail(noti.id).then(resp => {
                                                if (resp.data.data && resp.data.data.notification) {
                                                    this.setState({
                                                        noti: noti,
                                                        content: resp.data.data.notification.msgFull
                                                    });
                                                }
                                            });
                                        } else {
                                            this.setState({
                                                noti: noti
                                            })
                                        }
                                        this.props.setHasNewAct();
                                        this.props.setNeedUpdateNewAct();
                                    }
                                }
                            }
                        })
                    }
                }
            })

            var timer = this.state.timer;
            if (timer >= 40000) {
                timer = 3000;
            } else {
                timer += 3000;
            }
            this.setState({
                timer: timer
            });
        }
    }

    seenUserNotify() {
        const noti = this.state.noti;
        if (noti) {
            if (!noti.seen) {
                seenUserNotify(noti.id, true).then(resp => {
                    switch (noti.type) {
                        case 20:
                        case 21:
                            window.location.href = `/trip/detail/${noti.objIds[0]}`;
                            break;
                        case 40:
                            window.location.href = `/mycars`;
                            break;
                        case 41:
                            window.location.href = `/car/${noti.objIds[0]}`;
                            break;
                        case 60:
                            window.location.href = `/profile/${noti.objIds[0]}`;
                            break;
                        default:
                            break;
                    }
                });
            } else {
                switch (noti.type) {
                    case 20:
                    case 21:
                        window.location.href = `/trip/detail/${noti.objIds[0]}`;
                        break;
                    case 40:
                        window.location.href = `/mycars`;
                        break;
                    case 41:
                        window.location.href = `/car/${noti.objIds[0]}`;
                        break;
                    case 60:
                        window.location.href = `/profile/${noti.objIds[0]}`;
                        break;
                    default:
                        break;
                }
            }

            this.setState({
                noti: null
            })
        }
    }

    close() {
        const noti = this.state.noti;
        if (noti) {
            if (!noti.seen) {
                seenUserNotify(noti.id, true);
            }
            this.setState({
                noti: null
            })
        }
    }

    onRatingChange(rating) {
        this.setState({
            rating: rating
        });
    }

    onCommentChange(e) {
        this.setState({
            comment: e.target.value
        });
    }

    review() {
        const tripId = this.state.noti.objIds[0];
        getTripDetail(tripId).then(resp => {
            if (resp.data.error >= commonErr.SUCCESS && resp.data.data) {
                const trip = resp.data.data.trip;
                var loginId = this.props.session.profile.info.uid;
                var isOwner = (loginId === trip.ownerId);
                if (isOwner) {
                    reviewTripByOwner(tripId, this.state.rating, this.state.comment).then(resp => {
                        removePendingReview(tripId);
                        this.close();
                    });
                } else {
                    reviewTripByTraveler(tripId, this.state.rating, this.state.comment).then(resp => {
                        removePendingReview(tripId);
                        this.close();
                    });
                }
            }
        })
    }

    render() {
        const noti = this.state.noti;
        var modal;
        if (noti) {
            if (noti.type === 80) {
                modal = <BookingRequestBox isShow={noti !== null} hide={this.close} id={noti.objIds[0]} />
            } else if (noti.type === 1) {
                var msg = noti.msg;
                if (this.state.content && this.state.content !== "") {
                    const htmlToReactParser = new HtmlToReactParser.Parser();
                    msg = htmlToReactParser.parse(this.state.content);
                }
                modal = <Modal
                    show={noti !== null}
                    onHide={this.close}
                    dialogClassName="modal-sm modal-dialog"
                >
                    <Modal.Header closeButton={true} closeLabel={""}>
                        <Modal.Title>Thông báo</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>{msg}</Modal.Body>
                </Modal>
            } else {
                var content;
                if (noti.type === 21) {
                    content = <div className="module-register" style={{ background: "none", padding: "5px" }}>
                        <div className="textAlign-center">{noti.msg}</div>
                        <div className="space m" />
                        <div className="form-default form-s" style={{ width: "160px", margin: "auto" }}>
                            <ReactStars
                                count={5}
                                size={30}
                                value={this.state.rating}
                                half={false}
                                onChange={this.onRatingChange.bind(this)}
                                color1={'#efefef'}
                                color2={'#00a550'} />
                        </div>
                        <div className="form-default form-s">
                            <div className="space m" />
                            <div className="line-form">
                                <div className="wrap-input has-ico">
                                    <textarea className="textarea" onChange={this.onCommentChange.bind(this)} placeholder="Nhập nội dung đánh giá" value={this.state.comment}></textarea>
                                </div>
                            </div>
                            <div className="clear" />
                            <div className="space m" />
                            <div className="space m" />
                            <div className="space m" />
                            <div className="space m" />
                            <div className="wrap-btn has-2btn">
                                <div className="wr-btn">
                                    <a className="btn btn-secondary btn--m" type="button" onClick={this.seenUserNotify}>Chi tiết</a>
                                </div>
                                <div className="wr-btn">
                                    <a className="btn btn-primary btn--m" type="button" onClick={this.review.bind(this)}>Đánh giá</a>
                                </div>
                            </div>
                        </div>
                    </div>
                } else {
                    content = <div className="module-register" style={{ background: "none", padding: "0" }}>
                        <div className="form-default form-s">
                            <div className="textAlign-center customContentNotify" dangerouslySetInnerHTML={{__html: noti.msg}} />
                            <div className="space m" />
                            <div className="space m" />
                            <div className="wrap-btn has-2btn">
                                <div className="wr-btn">
                                    <a className="btn btn-secondary btn--m" onClick={this.close}>Bỏ qua</a>
                                </div>
                                <div className="wr-btn">
                                    <a className="btn btn-primary btn--m" onClick={this.seenUserNotify}>Chi tiết</a>
                                </div>
                            </div>
                        </div>
                    </div>
                }
                modal = <Modal
                    show={noti !== null}
                    onHide={this.close}
                    dialogClassName="modal-sm modal-dialog"
                >
                    <Modal.Header closeButton={true} closeLabel={""}>
                        <Modal.Title>Thông báo</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>{content}</Modal.Body>
                </Modal>
            }
        }
        return <div>
            <ReactInterval timeout={this.state.timer} enabled={true} callback={this.getNoti} />
            {modal}
        </div>
    }
}

function mapSession(state) {
    return {
        session: state.session
    }
}

NotificationBox = connect(mapSession)(NotificationBox);

export default NotificationBox;