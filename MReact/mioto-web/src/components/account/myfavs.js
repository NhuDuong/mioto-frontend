import React from "react"
import StarRatings from "react-star-ratings"
import { Link } from "react-router-dom"

import Header from "../common/header"
import { commonErr } from "../common/errors"
import { formatPrice, formatTitleInUrl } from "../common/common"
import { getFavoriteCars, removeFavoriteCar } from "../../model/car"
import { LoadingPage } from "../common/loading"
import { MessagePage } from "../common/messagebox"
import Session from "../common/session"

import car_photo from "../../static/images/upload/car_1.png"

class FavoriteCarItem extends React.Component {
    removeFromFavorites() {
        this.props.removeFromFavorites(this.props.car.id);
    }

    render() {
        return <div class="box-car__item"> 
                <Link to={{ pathname: `/car/${formatTitleInUrl(this.props.car.name)}/${this.props.car.id}` }}>
                    <div class="img-car"> 
                        <div class="fix-img"> 
                            <img src={this.props.car.photos ? this.props.car.photos[0].thumbUrl : car_photo}></img>                            
                        </div>
                        {this.props.car.photosVerified === 1 && <div className="status-verify">
                                    <i className="ic ic-verify-stroke" /> Ảnh đã xác thực
                                </div>}
                        <div class="price-car">{formatPrice(this.props.car.price)}</div>
                        <span className="label-pos">
                            {this.props.car.totalDiscountPercent > 0 && <span className="discount">Giảm {this.props.car.totalDiscountPercent}%</span>}
                            {this.props.car.instant > 0 && <span className="rent"><i className="ic ic-sm-thunderbolt-wh" /> Đặt xe nhanh</span>}
                            {this.props.car.deliveryEnable === 1 && this.props.car.deliveryPrice === 0 && <span className="free">Miễn phí giao nhận xe</span>}
                        </span>
                    </div>
                </Link>
                    <div class="desc-car"> 
                    <div class="ratings"> <StarRatings
                            rating={this.props.car.rating.avg}
                            starRatedColor="#00a550"
                            starDimension="17px"
                            starSpacing="1px"
                        />
                        <div class="bar-line"> </div>
                        <p class="trips">{this.props.car.totalTrips} chuyến </p>
                    </div>
                    <h2>{this.props.car.name}</h2>
                    </div>
                    <div class="wr-btn-share"><a class="btn btn-secondary btn--m" onClick={this.removeFromFavorites.bind(this)}>Bỏ thích </a><Link class="btn btn-primary btn--m" to={{ pathname: `/car/${formatTitleInUrl(this.props.car.name)}/${this.props.car.id}` }}>Xem chi tiết</Link></div>
                </div>
    }
}

class MyFavs extends React.Component {
    constructor() {
        super();
        this.state = {
            cars: null,
            err: commonErr.INNIT
        }
    }

    componentDidMount() {
        window.scrollTo(0, 0);

        this.setState({
            err: commonErr.LOADING
        });
        getFavoriteCars(0, 0, 0).then(resp => {
            if (resp.data.error >= commonErr.SUCCESS && resp.data.data) {
                this.setState({
                    cars: resp.data.data.cars,
                    err: resp.data.error
                });
            } else {
                this.setState({
                    err: resp.data.error
                });
            }
        });
    }

    removeFromFavorites(carId) {
        removeFavoriteCar(carId).then(resp => {
            if (resp.data.error >= commonErr.SUCCESS) {
                this.setState({
                    buttonState: 0
                });
            }
            var cars = this.state.cars.filter(car => {
                return (car.id !== carId)
            });
            this.setState({
                cars: cars
            });
        });
    }

    render() {
        var favoritesCont;

        if (this.state.err === commonErr.LOADING) {
            favoritesCont = <LoadingPage />
        } else {
            const favorites = this.state.cars;
            if (favorites) {
                favoritesCont =  <div class="my-favs__sect">
                                    <div class="m-container"> 
                                        <div class="heading-title">
                                            <h4>DANH SÁCH XE YÊU THÍCH</h4>
                                        </div>
                                        <hr class="l-gray"></hr>
                                        <div class="car-area__sect"> 
                                            <div class="box-car__wrap"> 
                                            {favorites.map(car =>
                                                <FavoriteCarItem removeFromFavorites={this.removeFromFavorites.bind(this)} car={car} />
                                            )}
                                            </div>
                                        </div>
                                    </div>
                                </div>
            } else {
                favoritesCont = <MessagePage message={"Không có xe yêu thích nào."} />
            }
        }

        return <div className="mioto-layout">
            <Session />
            <Header />
            <section className="body">
                {favoritesCont}
            </section>
        </div>
    }
}

export default MyFavs;