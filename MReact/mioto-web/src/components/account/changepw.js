import React from "react"
import { Modal } from "react-bootstrap"

import { commonErr } from "../common/errors"
import { updatePassword } from "../../model/profile"

export default class ChangePassword extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            ip_oldpassword: "",
            ip_newpassword: "",
            ip_newpassword_confirm: "",
						show: this.props.show,
            err: commonErr.INNIT,
            errMsg: this.props.errMsg
        }

        this.handleInputChange = this.handleInputChange.bind(this);
	}
	
	componentWillReceiveProps(props) {
		this.setState({
			show: props.show,
			errMsg: props.errMgs
		})
	}

    handleInputChange = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        });
    }

    retry() {
        this.setState({
            ip_oldpassword: "",
            ip_newpassword: "",
            ip_newpassword_confirm: "",

            err: commonErr.INNIT,
            errMsg: ""
        });
    }

    onSubmitBtnClick = (event) => {
        const oldPassword = this.state.ip_oldpassword;
        const newPassword = this.state.ip_newpassword;
        const newPasswordConfirm = this.state.ip_newpassword_confirm;
        const self = this;
        if (oldPassword !== "" && newPassword !== "") {
            if (newPassword === newPasswordConfirm) {
                updatePassword(oldPassword, newPassword).then(function (resp) {
                    self.setState({
                        err: resp.data.error,
                        errMsg: resp.data.errorMessage
                    });
                });
            } else {
                this.setState({
                    err: commonErr.FAIL,
                    errMsg: "Mật khẩu mới không khớp."
                });
            }
        }
    }

    render() {
        var content;
        if (this.state.errMsg && this.state.errMsg !== "") {
            if (this.state.err >= commonErr.SUCCESS) {
                content = <div className="form-default form-s">
                    <div className="line-form">
                        <div className="textAlign-center"><i className="ic ic-verify"></i> Cập nhật thành công.</div>
                    </div>
                    <div className="clear"></div>
                    <div className="space m"></div>
                    <button className="btn btn-primary btn--m" type="button" onClick={this.props.hideModal}>Hoàn tất</button>
                </div>
            } else {
                content = <div className="form-default form-s">
                    <div className="line-form">
                        <div className="textAlign-center"><i className="ic ic-error"></i> Cập nhật thất bại. ({this.state.errMsg})</div>
                    </div>
                    <div className="clear"></div>
                    <div className="space m"></div>
                    <button className="btn btn-primary btn--m" type="button" onClick={this.retry.bind(this)}>Thử lại</button>
                </div>
            }
        } else {
            content = <div className="form-default form-s">
                <div className="line-form">
                    <div className="wrap-input has-ico"><i className="ic ic-lock-fill"></i>
                        <input type="password" name="ip_oldpassword" placeholder="Mật khẩu hiện tại" onChange={this.handleInputChange} />
                    </div>
                </div>
                <div className="line-form">
                    <div className="wrap-input has-ico"><i className="ic ic-lock-fill"></i>
                        <input type="password" name="ip_newpassword" placeholder="Mật khẩu mới" onChange={this.handleInputChange} />
                    </div>
                </div>
                <div className="line-form">
                    <div className="wrap-input has-ico"><i className="ic ic-lock-fill"></i>
                        <input type="password" name="ip_newpassword_confirm" placeholder="Xác nhận mật khẩu mới" onChange={this.handleInputChange} />
                    </div>
                </div>
                <div className="clear"></div>
                <button className="btn btn-primary btn--m" type="button" onClick={this.onSubmitBtnClick.bind(this)}>Cập nhật</button>
            </div>
        }
        return <Modal
            show={this.state.show}
            onHide={this.props.hideModal}
            dialogClassName="modal-sm modal-dialog"
        >
            <Modal.Header closeButton={true} closeLabel={""}>
                <Modal.Title>Đổi mật khẩu</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                {content}
            </Modal.Body>
        </Modal>
    }
}
