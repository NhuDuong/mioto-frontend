import React from "react"
import { Modal } from "react-bootstrap"
import { connect } from "react-redux"

import { commonErr } from "../common/errors"
import ImageUpload from "../common/imageupload"
import { uploadPaper } from "../../model/profile"
import { removeAccountPaper } from "../../model/car"
import { LoadingInline } from "../common/loading"
import { getLoggedProfile } from "../../actions/sessionAct"

export class ChangePaper extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            photo: null,
            err: commonErr.INNIT,
            errMsg: ""
        }
        this.closeForm = this.closeForm.bind(this);
        this.removePaperPhoto = this.removePaperPhoto.bind(this);
    }

    removePaperPhoto(paperId, photoId) {
        const self = this;
        self.setState({ err: commonErr.LOADING });
        removeAccountPaper(paperId, photoId).then(function (resp) {
            self.setState({
                err: resp.data.error,
                errMsg: resp.data.errorMessage
            });
            if (self.state.err !== commonErr.INNIT && self.state.err >= commonErr.SUCCESS) {
                self.props.dispatch(getLoggedProfile());
            }
        });
    }

    onRetryBtnClick() {
        this.setState({
            err: commonErr.INNIT,
            errMsg: ""
        });
    }

    hanldeFileInputChange(photos) {
        const self = this;
        self.setState({ err: commonErr.LOADING });
        uploadPaper(self.props.paper.id, photos[0]).then(function (resp) {
            self.setState({
                err: resp.data.error,
                errMsg: resp.data.errorMessage
            });
            if (self.state.err !== commonErr.INNIT && self.state.err >= commonErr.SUCCESS) {
                self.props.dispatch(getLoggedProfile());
            }
        });
    }

    closeForm() {
        this.setState({
            photo: null,
            err: commonErr.INNIT,
            errMsg: ""
        });
        this.props.hideModal();
    }

    render() {
        var paper;
        const papers = this.props.session.profile.info.travelerPapers;
        for (var i = 0; i < papers.length; ++i) {
            if (this.props.paper.id === papers[i].id) {
                paper = papers[i];
            }
        }

        var content = <div className="form-default form-s">
            <div className="list-photos">
                <ul>
                    <li>
                        <a className="obj-photo">
                            <span className="ins">
                                <ImageUpload withPreview={false} onChange={this.hanldeFileInputChange.bind(this)} className="form-control" />
                            </span>
                        </a>
                    </li>
                    {paper.photos && paper.photos.reverse().map(photo => <li>
                        <div className="obj-photo" style={{ backgroundImage: `url(${photo.url})` }}><a className="func-remove" onClick={() => {
                            this.removePaperPhoto(this.props.paper.id, photo.id)
                        }}><i className="ic ic-remove"></i></a></div>
                    </li>)}
                </ul>
            </div>

            <div className="space m"></div>
            <div className="space m"></div>
            {<button className="btn btn-primary btn--m" type="button" onClick={this.closeForm.bind(this)}>Hoàn tất</button>}
        </div>
        return (
            <Modal
                show={this.props.show}
                onHide={this.closeForm}
                dialogClassName="modal-sm modal-dialog"
            >
                <Modal.Header closeButton={true} closeLabel={""}>
                    <Modal.Title>Cập nhật {paper.name}</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <div className="form-default form-s">
                        {this.state.err < commonErr.SUCCESS && this.state.errMsg !== "" && <p><i className="ic ic-error"></i> Cập nhật thất bại. {this.state.errMsg}</p>}
                    </div>
                    {this.state.err === commonErr.LOADING && <LoadingInline />}
                    {content}
                </Modal.Body>
            </Modal>
        );
    }
}

function mapState(state) {
    return {
        session: state.session
    }
}

ChangePaper = connect(mapState)(ChangePaper)

export default ChangePaper;
