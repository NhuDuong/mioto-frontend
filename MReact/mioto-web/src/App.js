import React from "react"
import { Provider } from "react-redux"
import { BrowserRouter, Switch, Route } from 'react-router-dom'
import { LastLocationProvider } from 'react-router-last-location'
import moment from "moment"

import store from "./store"
import Home from "./components/homev2/home"
import CarFinding from "./components/car/carfinding"
import TripDetail from "./components/car/tripdetail"
import Login from "./components/login/login"
import Profile from "./components/profile/profilev2"
import AccountV2 from "./components/account/accountv2"
import CarDetail from "./components/car/cardetail"
import SignUp from "./components/account/signup"
import CarRegister from "./components/carowner/carregister"
import HowItWorks from "./components/tutorial/howitworks"
import PaymentHowTo from "./components/tutorial/paymenthowto"
import BookingHowTo from "./components/tutorial/bookinghowto"
import OwnerGuide from "./components/tutorial/ownerguide"
import AboutUs from "./components/tutorial/aboutus"
import Features from "./components/tutorial/features"
import CarRegisHowTo from "./components/tutorial/carregishowto"
import RenterHowTo from "./components/tutorial/renterhowto"
import RenterBenefit from "./components/tutorial/renterbenefit"
import OwnerBenefit from "./components/tutorial/ownerbenefit"
import OwnerHowTo from "./components/tutorial/ownerhowto"
import CommingSoon from "./components/commingsoon"
import NotFound from "./components/common/notfound"
import MyCars from "./components/account/mycars"
import MyFavs from "./components/account/myfavs"
import MyTrips from "./components/account/mytrips"
import MyPromo from "./components/account/mypromo"
import MyCard from "./components/account/mycard"
// import MyRentalTrips from "./components/account/myrentaltrips"
import Privacy from "./components/tutorial/privacy"
import Terms from "./components/tutorial/terms"
import Faqs from "./components/tutorial/faqs"
import Regulation from "./components/tutorial/regulation"
import PersonalInfo from "./components/tutorial/personalinfo"
import ResolveConflic from "./components/tutorial/resolveconflic"
import SupportNav from "./components/tutorial/supportnav"
import Blogs from "./components/blog/blogs"
import Blog from "./components/blog/blog"
import CarSetting from "./components/carowner/carsetting"
import CommonSetting from "./components/carowner/commonsetting"
import Calendars from "./components/carowner/calendars"
import SharedCode from "./components/account/sharedcode"
import City from "./components/city/city"
import Payment from "./components/car/payment"
import CarsGPS from "./components/carowner/carsgps"
import OwnerRegister from "./components/carowner/ownerregister"
import MyWallet from "./components/carowner/mywallet"
import MyMonthlyReport from "./components/carowner/mymonthlyreport"

const App = () => {
  //localize the calendar in app
  moment.locale('vn', {
    months: 'Tháng 1_Tháng 2_Tháng 3_Tháng 4_Tháng 5_Tháng 6_Tháng 7_Tháng 8_Tháng 9_Tháng 10_Tháng 11_Tháng 12'.split('_'),
    monthsShort: 'Th. 1_Th. 2_Th. 3_Th. 4_Th. 5_Th. 6_Th. 7_Th. 8_Th. 9_TH. 10_Th. 11._Th. 12.'.split('_'),
    monthsParseExact: true,
    weekdays: 'Chủ nhật_Thứ 2_Thứ 3_Thứ 4_Thứ 5_Thứ 6_Thứ 7'.split('_'),
    weekdaysShort: 'CN_T. 2_T. 3_T. 4_T. 5_T. 6._T. 7'.split('_'),
    weekdaysMin: 'CN_T.2_T.3_T.4_T.5_T.6_T.7'.split('_'),
    weekdaysParseExact: true,
    longDateFormat: {
      LT: 'HH:mm',
      LTS: 'HH:mm:ss',
      L: 'DD/MM/YYYY',
      LL: 'D MMMM YYYY',
      LLL: 'D MMMM YYYY HH:mm',
      LLLL: 'dddd D MMMM YYYY HH:mm'
    },
    calendar: {
      sameDay: '[Cùng ngày] LT',
      nextDay: '[Ngày mai] LT',
      nextWeek: 'dddd [Tuần tới] LT',
      lastDay: '[Hôm qua] LT',
      lastWeek: 'dddd [Tuần rồi] LT',
      sameElse: 'L'
    },
    relativeTime: {
      future: '%s tới',
      past: '%s trước',
      s: '%d giây',
      m: ' 1 phút',
      mm: '%d phút',
      h: ' 1 giờ',
      hh: '%d giờ',
      d: ' 1 ngày',
      dd: '%d ngày',
      M: ' 1 tháng',
      MM: '%d tháng',
      y: ' 1 năm',
      yy: '%d năm'
    },
    dayOfMonthOrdinalParse: /\d{1,2}(er|e)/,
    ordinal: function (number) {
      return number + (number === 1 ? 'er' : 'e');
    },
    meridiemParse: /PD|MD/,
    isPM: function (input) {
      return input.charAt(0) === 'M';
    },
    // In case the meridiem units are not separated around 12, then implement
    // this function (look at locale/id.js for an example).
    // meridiemHour : function (hour, meridiem) {
    //     return /* 0-23 hour, given meridiem token and hour 1-12 */ ;
    // },
    meridiem: function (hours, minutes, isLower) {
      return hours < 12 ? 'AM' : 'PM';
    },
    week: {
      dow: 1, // Monday is the first day of the week.
      doy: 4  // The week that contains Jan 4th is the first week of the year.
    }
  });

  return <Provider store={store}>
    <BrowserRouter>
      <LastLocationProvider watchOnlyPathname={true}>
        <Switch>
          <Route exact path='/' component={Home} />
          <Route path='/home' component={Home} />

          <Route path='/find' render={props => <CarFinding {...props} />} />
          <Route path='/login' component={Login} />
          <Route path='/signup' component={SignUp} />
          <Route path='/cms' component={CommingSoon} />
          <Route path='/owner/register' component={OwnerRegister} />

          <Route path='/profile/:userId' component={Profile} />
          <Route path='/account' component={AccountV2} />
          <Route exact path='/car/:carId' render={props => <CarDetail {...props} />} />
          <Route exact path='/car/:carName/:carId' render={props => <CarDetail {...props} />} />
          <Route path="/mycars" component={MyCars} />
          <Route path="/myfavs" component={MyFavs} />
          <Route path="/mytrips" component={MyTrips} />
          <Route path="/mycard" component={MyCard} />
          {/* <Route path="/myrentaltrips" component={MyRentalTrips} /> */}
          <Route path="/mywallet" component={MyWallet} />
          <Route path="/mymonthlyreport/:month" render={props => <MyMonthlyReport {...props} />} />
          <Route exact path="/promo" component={MyPromo} />
          <Route path="/promo/:code" render={props => <MyPromo {...props} />} />
          <Route path="/payment/:tripId" render={props => <Payment {...props} />} />
          <Route path="/sharedcode" component={SharedCode} />
          <Route exact path='/trip/detail/:tripId' render={props => <TripDetail {...props} />} />
          <Route path="/carregister" component={CarRegister} />
          <Route path="/carsetting/:carId" render={props => <CarSetting {...props} />} />
          <Route path="/commonsetting" component={CommonSetting} />
          <Route path="/calendars" component={Calendars} />
          <Route path="/gps" component={CarsGPS} />
          

          <Route path="/howitwork" component={HowItWorks} />
					<Route path="/howitwork/mobile" component={HowItWorks} />
					<Route path="/paymenthowto" component={PaymentHowTo} />
          <Route path="/paymenthowto/mobile" component={PaymentHowTo} />
					<Route path="/bookinghowto" component={BookingHowTo} />
          <Route path="/bookinghowto/mobile" component={BookingHowTo} />
					<Route path="/ownerguide" component={OwnerGuide} />
          <Route path="/ownerguide/mobile" component={OwnerGuide} />
          <Route path="/support" component={SupportNav} />
          <Route path="/aboutus" component={AboutUs} />
          <Route path="/aboutus/mobile" component={AboutUs} />
          <Route path="/features" component={Features} />
          <Route path="/ownerhowto" component={OwnerHowTo} />
          <Route path="/carregishowto" component={CarRegisHowTo} />
          <Route path="/ownerbenef" component={OwnerBenefit} />
          <Route path="/renterhowto" component={RenterHowTo} />
          <Route path="/renterbenef" component={RenterBenefit} />
          <Route path="/privacy" component={Privacy} />
          <Route path="/privacy/mobile" component={Privacy} />
          <Route path="/terms" component={Terms} />
          <Route path="/terms/mobile" component={Terms} />
          <Route path="/faqs" component={Faqs} />
          <Route path="/faqs/mobile" component={Faqs} />
          <Route path="/regu" component={Regulation} />
          <Route path="/regu/mobile" component={Regulation} />
          <Route path="/personalinfo" component={PersonalInfo} />
          <Route path="/resolveconflic" component={ResolveConflic} />
          <Route path='/blogs' component={Blogs} />
          <Route exact path='/blog/:blogId' component={Blog} />
          <Route exact path='/blog/:blogTitle/:blogId' component={Blog} />
          <Route path="/city/:city" render={props => <City {...props} />} />

          <Route component={NotFound} />
        </Switch>
      </LastLocationProvider>
    </BrowserRouter>
  </Provider>
}

export default App;