import { login as Login, logout as Logout, getLoggedProfile as GetLoggedProfile } from "../model/profile";

export function login(username, password) {
    return dispatch => {
        return dispatch({
            type: "LOG_IN",
            payload: Login(username, password)
        }).then(() => dispatch(getLoggedProfile()));
    };
}

export function logout() {
    return {
        type: "LOG_OUT",
        payload: Logout()
    }
}

export function getLoggedProfile() {
    return {
        type: "GET_LOGGED_PROFILE",
        payload: GetLoggedProfile()
    }
}