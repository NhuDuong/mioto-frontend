import { searchCarForList, searchCarForMap, searchCarForMapV2 } from "../model/car"

export const setViewMode = (mode) => {
    return {
        type: "SET_VIEW_MODE",
        payload: mode
    }
}

export const setFilter = (filter) => {
    return {
        type: "SET_FILTER",
        payload: filter
    }
}

export const searchCarList = (filter) => {
    return {
        type: "SEARCH_CAR_LIST",
        payload: searchCarForList(filter)
    }
}

export const searchCarListMore = (filter) => {
    return {
        type: "SEARCH_CAR_LIST_MORE",
        payload: searchCarForList(filter)
    }
}

export const searchCarMap = (filter) => {
    return {
        type: "SEARCH_CAR_MAP",
        payload: searchCarForMap(filter)
    }
}

export const searchCarMapMore = (filter) => {
    return {
        type: "SEARCH_CAR_MAP_MORE",
        payload: searchCarForMap(filter)
    }
}

export const searchCarMapV2 = (filter) => {
    return {
        type: "SEARCH_CAR_MAP_V2",
        payload: searchCarForMapV2(filter)
    }
}

export const searchCarMapMoreV2 = (filter) => {
    return {
        type: "SEARCH_CAR_MAP_MORE_V2",
        payload: searchCarForMapV2(filter)
    }
}