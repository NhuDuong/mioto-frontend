import { init } from "../model/car"

export function initApp() {
    return {
        type: "INIT",
        payload: init()
    }
}