import { combineReducers } from "redux"
import carFinding from "./carFindingRed"
import session from "./sessionRed"
import appInit from "./appInitRed"

export default combineReducers({
    carFinding,
    session,
    appInit
});