#!/bin/sh

SERVER="root@mioto.dev.1"

ENTRY_PATH=`readlink -f $0`
CURR_PATH=`dirname $ENTRY_PATH`
CURR_FOLDER_NAME=${CURR_PATH##*/}
PRJ_NAME="mioto-web-dev"
DEPLOY_PATH=/data/www/$PRJ_NAME/

#sync
rsync -arv --delete --no-perms --no-owner --no-group build/ $SERVER:$DEPLOY_PATH --exclude='.svn'
