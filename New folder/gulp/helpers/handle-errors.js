const notify = require('gulp-notify');

module.exports = function() {
  const args = Array.prototype.slice.call(arguments);
  notify.onError({
    title: 'Task Failed [<%= error.message %>'
  }).apply(this, args);
  this.emit('end');
}