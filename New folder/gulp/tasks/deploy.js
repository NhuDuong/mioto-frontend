const gulp        = require('gulp');
const runSequence = require('run-sequence');

gulp.task('deploy', function() {
  runSequence(
    'clean',
    'iconfont',
    [
      'copy',
      'imagemin',
      'scripts:deploy',
      'styles:deploy',
      'views:deploy'
    ]
  );
});