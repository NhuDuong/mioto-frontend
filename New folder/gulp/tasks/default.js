var gulp        = require('gulp');
var runSequence = require('run-sequence');

gulp.task('default', function () {
  runSequence(
    'clean',
    'iconfont',
    [
      'browser-sync:dev',
      'copy',
      'imagemin',
      'scripts:dev',
      'styles:dev',
      'views:dev',
      'watch:dev'
    ]
  );
});