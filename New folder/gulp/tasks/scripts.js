const browserSync = require('browser-sync');
const cached = require('gulp-cached');
const concat = require('gulp-concat');
const config = require('../config');
const gulp = require('gulp');
const handleErrors = require('../helpers/handle-errors');
const jsPath = config.paths.scripts;
const jshint = require('gulp-jshint');
const newer = require('gulp-newer');
const plumber = require('gulp-plumber');
const remember = require('gulp-remember');
const stripDebug = require('gulp-strip-debug');
const stylish = require('jshint-stylish');
const uglify = require('gulp-uglify');

gulp.task('scripts:dev', ['scripts-vendor:dev'], () => {
    return gulp.src(jsPath.src)
        .pipe(plumber({ errorHandler: handleErrors }))
        .pipe(cached('scripts'))
        .pipe(jshint('./gulp/lints/.jshintrc'))
        .pipe(jshint.reporter(stylish))
        .pipe(remember('scripts'))
        .pipe(concat(config.names.js.app))
        .pipe(gulp.dest(jsPath.dest))
        .pipe(browserSync.reload({ stream: true }));
});

gulp.task('scripts:deploy', ['scripts-vendor:deploy'], () => {
    return gulp.src(jsPath.src)
        .pipe(plumber({ errorHandler: handleErrors }))
        .pipe(concat(config.names.js.appMin))
        .pipe(stripDebug())
        .pipe(uglify())
        .pipe(gulp.dest(jsPath.dest));
});

gulp.task('scripts-vendor:dev', function() {
    return gulp.src(jsPath.vendorSrc)
        .pipe(plumber({ errorHandler: handleErrors }))
        .pipe(newer(jsPath.dest + '/' + config.names.js.vendor))
        .pipe(concat(config.names.js.vendor))
        .pipe(gulp.dest(jsPath.dest));
});

gulp.task('scripts-vendor:deploy', () => {
    return gulp.src(jsPath.vendorSrc)
        .pipe(plumber({ errorHandler: handleErrors }))
        .pipe(concat(config.names.js.vendorMin))
        .pipe(uglify())
        .pipe(gulp.dest(jsPath.dest));
});

