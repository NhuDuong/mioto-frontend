const gulp                   = require('gulp');
const imagemin               = require('gulp-imagemin');
const imageminJpegRecompress = require('imagemin-jpeg-recompress');
const imageminPngquant       = require('imagemin-pngquant');
const newer                  = require('gulp-newer');
const plumber                = require('gulp-plumber');
const config                 = require('../config');
const imgPath                = config.paths.images;
const imgDest                = imgPath.dest;

gulp.task('imagemin', () => {
  gulp.src(imgPath.src)
    .pipe(plumber())
    .pipe(newer(imgDest))
    .pipe(imagemin([
      imagemin.gifsicle({ interlaced: true }),
      imageminJpegRecompress({
        progressive: true,
        max: 80,
        min: 70
      }),
      imageminPngquant({ quality: '75-85' }),
      imagemin.svgo({ plugins: [{ removeViewBox: false }] })
    ]))
    .pipe(gulp.dest(imgDest));
});