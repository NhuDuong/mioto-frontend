const config  = require('../config');
const gulp    = require('gulp');
const newer   = require('gulp-newer');
const paths   = config.paths;
const plumber = require('gulp-plumber');

gulp.task('data:copy', () => {
  return gulp.src(paths.data.src)
    .pipe(plumber())
    .pipe(newer(paths.data.dest))
    .pipe(gulp.dest(paths.data.dest));
});

gulp.task('fonts:copy', () => {
  return gulp.src(paths.fonts.src)
    .pipe(plumber())
    .pipe(newer(paths.fonts.dest))
    .pipe(gulp.dest(paths.fonts.dest));
});

gulp.task('media:copy', () => {
  return gulp.src(paths.media.src)
    .pipe(plumber())
    .pipe(newer(paths.media.dest))
    .pipe(gulp.dest(paths.media.dest));
});

gulp.task('copy', ['data:copy', 'fonts:copy', 'media:copy']);