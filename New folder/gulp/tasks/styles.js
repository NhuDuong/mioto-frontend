const autoprefixer = require('autoprefixer');
const browserSync  = require('browser-sync');
const cached       = require('gulp-cached');
const cssnano      = require('gulp-cssnano');
const gulp         = require('gulp');
const mqpacker     = require('css-mqpacker');
const plumber      = require('gulp-plumber');
const postcss      = require('gulp-postcss');
const rename       = require('gulp-rename');
const sass         = require('gulp-sass');
const sassGlob     = require('gulp-sass-glob');
const sasslint     = require('gulp-sass-lint');
const sourcemaps   = require('gulp-sourcemaps');
const handleErrors = require('../helpers/handle-errors');
const config       = require('../config');
const cssPath      = config.paths.styles;

// gulp.task('styles-lint:dev', () => {
//   return gulp.src(cssPath.lint)
//     .pipe(cached('stylesLint'))
//     .pipe(sasslint())
//     .pipe(sasslint.format())
//     .pipe(sasslint.failOnError());
// });

gulp.task('styles:dev', () => {
  return gulp.src(cssPath.src)
    .pipe(sassGlob())
    .pipe(plumber({
      errorHandler: handleErrors
    }))
    .pipe(sourcemaps.init())
    .pipe(sass({
      includePaths: cssPath.src,
      errLogToConsole: true,
      outputStyle: 'expanded'
    }))
    .pipe(postcss([
      autoprefixer({
        browsers: config.autoPrefixBrowserList
      }),
      mqpacker({
        sort: true
      }),
    ]))
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest(cssPath.dest))
    .pipe(browserSync.reload({ stream: true }));
});

gulp.task('styles:deploy', () => {
  return gulp.src(cssPath.src)
    .pipe(sassGlob())
    .pipe(plumber({ errorHandler: handleErrors }))
    .pipe(sass())
    .pipe(postcss([
      autoprefixer({
        browsers: config.autoPrefixBrowserList
      }),
      mqpacker({
        sort: true
      }),
    ]))
    .pipe(cssnano({ safe: true }))
    .pipe(rename(config.names.css))
    .pipe(gulp.dest(cssPath.dest));
});
