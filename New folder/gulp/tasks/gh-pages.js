const config  = require('../config');
const ghPages = require('gulp-gh-pages');
const gulp    = require('gulp');

gulp.task('gh-pages', () => {
  return gulp.src(config.paths.ghPages.src)
    .pipe(ghPages());
});