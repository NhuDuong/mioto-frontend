module.exports = {
    autoPrefixBrowserList: ['last 2 version'],
    paths: {
        dest: 'dist/',
        data: {
            src: 'app/data/*',
            dest: 'dist/data'
        },
        media: {
            src: 'app/media/*',
            dest: 'dist/media'
        },
        fonts: {
            src: ['app/fonts/*','app/fonts/**/*'],
            dest: 'dist/fonts'
        },
        iconfont: {
            fontPath: '../fonts/',
            iconTemplate: 'gulp/template/_icons.scss',
            src: 'app/iconfont/*',
            templateDest: 'app/styles/_components',
            dest: 'dist/fonts'
        },
        images: {
            src: ['app/images/*', 'app/images/**/*'],
            dest: 'dist/images'
        },
        styles: {
            all: ['app/styles/*.scss', 'app/styles/**/*.scss', 'app/styles/*.sass', 'app/styles/**/*.sass'],
            include: 'app/styles/',
            lint: 'app/styles/**/*.scss',
            src: 'app/styles/*.scss',
            dest: 'dist/css'
        },
        scripts: {
            dest: 'dist/js',
            src: ['app/scripts/*.js', 'app/scripts/_plugins/*.js', 'app/scripts/_pages/*.js'],
            vendorSrc: ['app/scripts/_vendors/*.js', 'app/scripts/_vendors/**/*.js'],
            jquerySrc: ['app/scripts/jquery.min.js'],
            swiperSrc: ['app/scripts/swiper.min.js'],
            rangesliderSrc: ['app/scripts/rangeslider.min.js'],
            modernizrSrc: ['app/scripts/jquery.min.js'],
            watch: ['app/scripts/*.js', 'app/scripts/**/*.js']
        },
        views: {
            all: ['app/views/*.pug', 'app/views/**/*.pug', 'app/views/**/**/*.pug'],
            src: 'app/views/*.pug',
            dest: 'dist'
        },
        ghPages: {
            src: './'
        }
    },
    names: {
        iconfont: 'icons',
        css: 'app.min.css',
        js: {
            app: 'main.js',
            appMin: 'app.min.js',
            vendor: 'vendor.js',
            vendorMin: 'vendor.min.js',
            swiper: 'swiper.js',
            swiperMin: 'swiper.min.js',
            jquery: 'jquery.js',
            jqueryMin: 'jquery.min.js',
            rangeslider: 'rangeslider.js',
            rangesliderMin: 'rangeslider.min.js',
            modernizr: 'modernizr.js',
            modernizrMin: 'modernizr.min.js',
        }
    }
};